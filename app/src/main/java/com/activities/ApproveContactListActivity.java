package com.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.adapter.ApproveContactListAdapter;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.facebook.login.LoginManager;
import com.quickkonnect.LoginActivity;
import com.quickkonnect.PojoClasses;
import com.quickkonnect.R;
import com.quickkonnect.SharedPreference;
import com.utilities.Constants;
import com.utilities.QKPreferences;
import com.utilities.Utilities;
import com.volleyapirequest.VolleyApiRequest;
import com.volleyapirequest.VolleyCallBack;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ApproveContactListActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private static SharedPreferences mSharedPreferences;
    private RecyclerView.LayoutManager mLayoutManager;
    private ApproveContactListAdapter adapter;
    private String userid, sender_id;
    private ArrayList<PojoClasses.ContactInfo> mlist;
    private TextView tv_no_record_found;
    private QKPreferences qkPreferences;
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Utilities.changeStatusbar(ApproveContactListActivity.this, getWindow());
        setContentView(R.layout.activity_approve_contact_list);
        Toolbar toolbar = findViewById(R.id.toolbar);
        //toolbar.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        toolbar.setBackgroundResource(R.drawable.toolbar_gradient);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mSharedPreferences = getSharedPreferences(SharedPreference.PREF_NAME, 0);
        qkPreferences = new QKPreferences(this);
        userid = qkPreferences.getLoggedUser().getData().getId() + "";
        //userid = mSharedPreferences.getString(SharedPreference.CURRENT_USER_ID, "");
        tv_no_record_found = findViewById(R.id.tv_no_record_found);

        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);
        Bundle extras = getIntent().getExtras();

        context = ApproveContactListActivity.this;

        if (extras != null) {
            sender_id = extras.getString("scan_user_id");
        }
        if (!Utilities.isNetworkConnected(this))
            Toast.makeText(this, Constants.NO_INTERNET_CONNECTION, Toast.LENGTH_LONG).show();
        else
            getApproveContactsList();

    }

    private void getApproveContactsList() {

        new VolleyApiRequest(context, true, VolleyApiRequest.REQUEST_BASEURL + "api/approve_user_contacts/" + userid,
                new VolleyCallBack() {
                    @Override
                    public void onResponse(JSONObject response) {


                        try {
                            String responseStatus = response.getString("status");
                            String msg = response.getString("msg");
                            if (responseStatus.equals("200")) {

                                JSONArray jsnArr = response.getJSONArray("data");

                                Log.d("jsnArr", jsnArr.toString());

                                recyclerView.setVisibility(View.VISIBLE);
                                mlist = new ArrayList<PojoClasses.ContactInfo>();
                                for (int i = 0; i < jsnArr.length(); i++) {

                                    JSONObject obj = jsnArr.getJSONObject(i);
                                    Log.i("Success obj--> ", obj.toString());
                                    JSONArray user_contact_detail = obj.getJSONArray("user_contact_detail");

                                    for (int j = 0; j < user_contact_detail.length(); j++) {
                                        JSONObject obj1 = user_contact_detail.getJSONObject(j);
                                        mlist.add(new PojoClasses.ContactInfo(obj1.getInt("contact_id"), obj1.getString("contact_type"), obj1.getString("email"), obj1.getString("phone")));
                                    }

                                }
                                adapter = new ApproveContactListAdapter(ApproveContactListActivity.this, mlist);
//                        //4. set adapter
                                recyclerView.setAdapter(adapter);

                                Log.d("mListrrr", mlist.toString());
                            } else if (responseStatus.equals("400")) {
                                if (msg.equalsIgnoreCase("No user found")) {
                                    SharedPreferences.Editor e = mSharedPreferences.edit();
                                    e.clear();
                                    e.remove(SharedPreference.CURRENT_USER_ID);
                                    e.remove(SharedPreference.CURRENT_USER_NAME);
                                    e.commit();
                                    e.apply();
                                    LoginManager.getInstance().logOut();

                                    Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    startActivity(intent);
                                    finish();
                                } else {
                                    recyclerView.setVisibility(View.GONE);
                                    tv_no_record_found.setVisibility(View.VISIBLE);
                                    tv_no_record_found.setText(msg);
                                }

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyApiRequest.showVolleyError(context, error);
                    }
                }).enqueRequest(null, Request.Method.GET);
    }

}