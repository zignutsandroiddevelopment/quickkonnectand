package com.activities;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.models.logindata.LanguageDetail;
import com.models.logindata.ModelLoggedUser;
import com.quickkonnect.R;
import com.retrofitservice.ApiUtils;
import com.retrofitservice.SOService;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;
import com.utilities.Constants;
import com.utilities.QKPreferences;
import com.utilities.Utilities;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UpdateLanguageActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener, View.OnClickListener {

    private SOService soService;
    private String userid;
    private SearchableSpinner sp_language;
    private String prevLanguage = "", prevProficiency = "";
    private String lang;
    private RatingBar ratingBar;
    private Button btn_save;
    private TextView tv_delete;
    private RelativeLayout view;
    private QKPreferences qkPreferences;
    private ModelLoggedUser modelUserProfile;
    private LanguageDetail languageDetail;
    private Context context;
    boolean isEdit = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_language);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close_white_24dp);

        Utilities.ChangeStatusBar(UpdateLanguageActivity.this);

        soService = ApiUtils.getSOService();

        try {
            Bundle b = getIntent().getExtras();
            if (b.getString(Constants.KEY_UPDATE).equalsIgnoreCase(Constants.ADD_DATA)) {
                getSupportActionBar().setTitle(getResources().getString(R.string.addlanguage));
            } else if (b.getString(Constants.KEY_UPDATE).equalsIgnoreCase(Constants.EDIT_DATA)) {
                getSupportActionBar().setTitle(getResources().getString(R.string.updatelanguage));
                isEdit = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        context = UpdateLanguageActivity.this;
        qkPreferences = new QKPreferences(context);
        userid = "" + qkPreferences.getLoggedUser().getData().getId();

        modelUserProfile = qkPreferences.getLoggedUser();

        sp_language = findViewById(R.id.sp_language);
        ratingBar = findViewById(R.id.rating);
        btn_save = findViewById(R.id.btn_save);

        tv_delete = findViewById(R.id.tv_delete);
        view = findViewById(R.id.r1);
        tv_delete.setOnClickListener(this);


//        String language[] = {"Abkhaz", "Afar", "Afrikaans", "Akan", "Albanian", "Amharic", "Arabic", "Aragonese", "Aragonese"
//                , "Armenian", "Avestan", "Avaric", "Aymara", "Azerbaijani", "Bambara", "Bashkir", "Avaric", "Basque", "Belarusian", "Bengali"
//                , "Bihari", "Bislama", "Bosnian", "Breton", "Bulgarian", "Burmese", "Catalan", "Chamorro", "Chechen", "Chichewa", "Chinese"
//                , "Chuvash", "Cornish", "Cree", "Croatian", "Croatian", "Danish", "Danish", "Dutch", "English", "Esperanto", "Estonian"
//                , "Ewe", "Faroese", "Fijian", "Finnish", "French", "Fula", "Galician", "Georgian", "German", "Greek", "Guaraní", "Gujarati", "Haitian"
//                , "Hausa", "Hebrew", "Herero", "Hindi", "Hiri Motu", "Hungarian", "Interlingua", "Irish", "Igbo", "Inupiaq", "Ido", "Icelandic"
//                , "Italian", "Inuktitut", "Japanese", "Javanese", "Kalaallisut", "Kannada", "Kanuri", "Kashmiri", "Kazakh", "Khmer"
//                , "Kikuyu", "Kinyarwanda", "Kirghiz", "Komi", "Kongo", "Korean", "Kurdish", "Kwanyama", "Latin", "Luxembourgish", "Luganda"
//                , "Limburgish", "Lingala", "Lao", "Lithuanian", "Luba-Katanga", "Latvian", "Manx", "Macedonian", "Malagasy"
//                , "Malay", "Malayalam", "Maltese", "Māori", "Marathi", "Marshallese", "Mongolian", "Nauru"
//                , "Navajo", "Nauru", "Norwegian Bokmål", "North Ndebele", "Nepali", "Ndonga", "Norwegian Nynorsk", "Norwegian"
//                , "Nuosu", "South Ndebele", "Occitan", "Ojibwe", "Old Church Slavonic", "Oromo", "Oriya", "Ossetian", "Panjabi"
//                , "Pāli", "Persian", "Polish", "Pashto", "Portuguese", "Quechua", "Romansh", "Kirundi", "Romanian", "Russian"
//                , "Sanskrit", "Sardinian", "Sindhi", "Northern Sami", "Samoan", "Sango", "Serbian", "Scottish", "Shona", "Sinhala", "Slovak"
//                , "Slovene", "Somali", "Slovak", "Southern Sotho", "Spanish", "Sundanese", "Swati", "Swedish", "Tamil", "Telugu", "Tajik"
//                , "Thai", "Tigrinya", "Tibetan Standard", "Tagalog", "Tagalog", "Tonga", "Turkish", "Tsonga", "Tatar", "Twi", "Tahitian"
//                , "Uighur", "Ukrainian", "Urdu", "Uzbek", "Venda", "Vietnamese", "Walloon", "Tahitian", "Welsh", "Wolof"
//                , "Western Frisian", "Xhosa", "Yiddish", "Yoruba", "Zhuang"};

        String language[] = getResources().getStringArray(R.array.country);

        sp_language.setTitle(getResources().getString(R.string.selectlanguage));
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this, R.layout.spinner_item, language);
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
        sp_language.setAdapter(spinnerArrayAdapter);

        sp_language.setOnItemSelectedListener(this);
        btn_save.setOnClickListener(this);


        Bundle extras = getIntent().getExtras();

        if (isEdit) {

            languageDetail = extras.getParcelable("language");

            if (languageDetail != null && languageDetail.getRating() != null) {
                prevProficiency = languageDetail.getRating();
                ratingBar.setRating(Float.parseFloat(languageDetail.getRating()));
            }

            if (languageDetail != null && languageDetail.getLangaugeName() != null) {

                prevLanguage = languageDetail.getLangaugeName();
                for (int i = 0; i < language.length; i++) {
                    if (sp_language.getItemAtPosition(i).toString().equals(languageDetail.getLangaugeName())) {
                        sp_language.setSelection(i);
                        break;
                    }
                }
            }

            btn_save.setText(getResources().getString(R.string.update));
            tv_delete.setVisibility(View.VISIBLE);
        }
    }


    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        lang = adapterView.getItemAtPosition(i).toString();
        Log.d("lang", "onItemSelected: " + lang);
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {


    }

    @Override
    public void onClick(final View view) {

        switch (view.getId()) {
            case R.id.tv_delete:
                new AlertDialog.Builder(this)
                        .setMessage(getResources().getString(R.string.delete_confirmation_message_language))
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int whichButton) {

                                if (!Utilities.isNetworkConnected(UpdateLanguageActivity.this))
                                    Utilities.showToast(context, getResources().getString(R.string.no_internat_connection));

                                else {
                                    Utilities.hideKeyboard(UpdateLanguageActivity.this);
                                    deleteLanguage(languageDetail.getLangaugeId());
                                }
                            }
                        })
                        .setNegativeButton(android.R.string.no, null).show();
                break;
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.custom_save_update, menu);
        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {

            case android.R.id.home:
                exitfromActivity();
                return true;

            case R.id.ic_save_update:

                Utilities.hideKeyboard(UpdateLanguageActivity.this);

                String rat = String.valueOf(ratingBar.getRating());
                if (!Utilities.isEmpty(rat) || rat.equalsIgnoreCase("0.0"))
                    Utilities.showToast(context, getResources().getString(R.string.proficiencyrequired));

                else if (!Utilities.isEmpty(lang) || lang.equals(getResources().getString(R.string.selectlanguage)))
                    Utilities.showToast(context, getResources().getString(R.string.languagerequired));

                else {

                    if (!Utilities.isNetworkConnected(this))
                        Utilities.showToast(this, getResources().getString(R.string.no_internat_connection));

                    else {

                        if (languageDetail != null && languageDetail.getLangaugeId() != null)
                            editLanguage(languageDetail.getLangaugeId(), lang, rat);

                        else
                            addLanguage(userid, lang, rat);
                    }
                }

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }


    /**
     * Add new language
     *
     * @param user_id
     * @param langauge_name
     * @param rating
     */
    public void addLanguage(String user_id, final String langauge_name, final String rating) {
        Map<String, String> headerParams = Utilities.getHeaderParameter(context);
        final ProgressDialog progressDialog = Utilities.showProgress(context);
        soService.addLanguage(headerParams, user_id, langauge_name, rating).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Utilities.hideProgressDialog(progressDialog);
                try {

                    if (response == null || response.body() == null) {
                        Utilities.showToast(context, getResources().getString(R.string.unknownerror));
                        return;
                    }

                    JSONObject jsonObject = new JSONObject(response.body().string());

                    String status = jsonObject.getString("status");
                    String message = jsonObject.getString("msg");

                    if (status.equals(ApiUtils.RESPONSE_CODE_SUCCESS)) {

                        List<LanguageDetail> languageDetailList = modelUserProfile.getData().getLanguageDetail();
                        if (languageDetailList == null)
                            languageDetailList = new ArrayList<>();

                        LanguageDetail languageDetail = new LanguageDetail();
                        JSONObject object = jsonObject.getJSONObject("data");

                        languageDetail.setLangaugeId(object.getInt("langauge_id"));
                        languageDetail.setLangaugeName(object.getString("langauge_name"));
                        languageDetail.setRating(object.getString("rating"));

                        languageDetailList.add(languageDetail);

                        modelUserProfile.getData().setLanguageDetail(languageDetailList);

                        qkPreferences.storeLoggedUser(modelUserProfile);

                        setResult(RESULT_OK);
                        UpdateLanguageActivity.this.finish();

                    } else Utilities.showToast(context, message);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Utilities.hideProgressDialog(progressDialog);
                Utilities.showToast(context, t.getLocalizedMessage());
            }
        });
    }

    /**
     * Edit in existing language
     *
     * @param langauge_id
     * @param langauge_name
     * @param rating
     */
    public void editLanguage(int langauge_id, final String langauge_name, final String rating) {
        final ProgressDialog progressDialog = Utilities.showProgress(context);
        Map<String, String> headerParams = Utilities.getHeaderParameter(context);
        soService.editLanguage(headerParams, langauge_id, langauge_name, rating).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Utilities.hideProgressDialog(progressDialog);
                try {

                    if (response == null && response.body() == null) {
                        Utilities.showToast(context, getResources().getString(R.string.unknownerror));
                        return;
                    }

                    JSONObject jsonObject = new JSONObject(response.body().string());

                    String responseStatus = jsonObject.getString("status");

                    String msg = jsonObject.getString("msg");

                    if (responseStatus.equals(ApiUtils.RESPONSE_CODE_SUCCESS)) {

                        if (modelUserProfile != null) {

                            List<LanguageDetail> languageDetailList = modelUserProfile.getData().getLanguageDetail();
                            int index = 0;
                            for (LanguageDetail languageDetails: languageDetailList) {
                                if (languageDetails.getLangaugeId() != null &&
                                        languageDetails.getLangaugeId().equals(languageDetail.getLangaugeId())) {
                                    index = languageDetailList.indexOf(languageDetails);
                                    break;
                                }
                            }

                            JSONObject object = jsonObject.getJSONObject("data");

                            languageDetail.setLangaugeName(object.getString("langauge_name"));
                            languageDetail.setRating(object.getString("rating"));

                            languageDetailList.set(index, languageDetail);

                            modelUserProfile.getData().setLanguageDetail(languageDetailList);

                            qkPreferences.storeLoggedUser(modelUserProfile);

                            setResult(RESULT_OK);

                            UpdateLanguageActivity.this.finish();

                        }

                    } else
                        Utilities.showToast(context, msg);


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Utilities.hideProgressDialog(progressDialog);
                Utilities.showToast(context, t.getLocalizedMessage());
            }
        });
    }

    /**
     * Delete existing language
     *
     * @param language_id
     */
    public void deleteLanguage(int language_id) {

        final ProgressDialog progressDialog = Utilities.showProgress(context);
        Map<String, String> headerParam = Utilities.getHeaderParameter(context);
        soService.deleteLanguage(headerParam, language_id).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Utilities.hideProgressDialog(progressDialog);
                try {

                    if (response == null || response.body() == null) {
                        Utilities.showToast(context, getResources().getString(R.string.unknownerror));
                        return;
                    }

                    JSONObject jsonObject = new JSONObject(response.body().string());

                    String responseStatus = jsonObject.getString("status");

                    String msg = jsonObject.getString("msg");

                    if (responseStatus.equals(ApiUtils.RESPONSE_CODE_SUCCESS)) {

                        List<LanguageDetail> languageDetailList = modelUserProfile.getData().getLanguageDetail();

                        for (int i = 0; i < languageDetailList.size(); i++) {
                            if (languageDetailList.get(i).getLangaugeId().equals(languageDetail.getLangaugeId())) {
                                languageDetailList.remove(i);
                                break;
                            }
                        }

                        modelUserProfile.getData().setLanguageDetail(languageDetailList);

                        qkPreferences.storeLoggedUser(modelUserProfile);

                        setResult(RESULT_OK);
                        UpdateLanguageActivity.this.finish();

                    } else
                        Utilities.showToast(context, msg);


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Utilities.hideProgressDialog(progressDialog);
                Utilities.showToast(context, t.getLocalizedMessage());
            }
        });
    }

    @Override
    public void onBackPressed() {
        exitfromActivity();
    }

    public void exitfromActivity() {
        if (isFoundanyupdate()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(UpdateLanguageActivity.this);
            builder.setTitle(getResources().getString(R.string.alert));
            builder.setMessage(getResources().getString(R.string.exitwithoutsave));
            builder.setPositiveButton(getResources().getString(R.string.exit), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    Utilities.hideKeyboard(UpdateLanguageActivity.this);
                    setResult(RESULT_CANCELED);
                    UpdateLanguageActivity.this.finish();
                }
            });
            builder.setNegativeButton(getResources().getString(R.string.close), null);
            builder.show();

        } else {

            Utilities.hideKeyboard(UpdateLanguageActivity.this);
            setResult(RESULT_CANCELED);
            UpdateLanguageActivity.this.finish();
        }
    }

    public boolean isFoundanyupdate() {

        String rat = String.valueOf(ratingBar.getRating());

        if (prevLanguage != null && !prevLanguage.equals(lang)) {
            return true;

        } else if (prevProficiency != null && !prevProficiency.equals(rat)) {
            return true;

        }

        return false;
    }
}