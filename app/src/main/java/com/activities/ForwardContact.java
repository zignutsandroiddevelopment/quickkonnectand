package com.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.Chat.ChatMessage;
import com.Chat.NewChatActivity;
import com.adapter.NewContactListAdapter;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.interfaces.OnContactClick;
import com.models.Conversation;
import com.models.contacts.Datum;
import com.quickkonnect.PojoClasses;
import com.quickkonnect.R;
import com.quickkonnect.SimpleDividerItemDecoration;
import com.realmtable.Contacts;
import com.realmtable.RealmController;
import com.services.ForwardMessageService;
import com.utilities.Constants;
import com.utilities.QKPreferences;
import com.utilities.RecyclerItemClickListener;
import com.utilities.Utilities;

import java.util.ArrayList;

import io.realm.RealmResults;

public class ForwardContact extends AppCompatActivity {

    private TextView tv_no_record_found_Contacts;
    private RecyclerView recycleContacts;
    private ImageView back_toolbar, forward_to_chat;
    boolean isMultiSelect = false;
    private QKPreferences qkPreferences;
    private ArrayList<String> selectedmessages;

    private String message = "";
    ProgressDialog progressBar;

    private DatabaseReference messageChatDatabase, conversationDB;

    public ArrayList<Datum> mlist_multiselect;
    public ArrayList<Datum> mlist;
    public NewContactListAdapter newContactListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Utilities.changeStatusbar(ForwardContact.this, getWindow());

        setContentView(R.layout.activity_forward_contact);
        qkPreferences = new QKPreferences(ForwardContact.this);
        selectedmessages = new ArrayList<>();
        try {
            Bundle b = getIntent().getExtras();
            //message = b.getString("MESSAGE");
            assert b != null;
            selectedmessages = (ArrayList<String>) b.getStringArrayList("MESSAGE");
            //ReceiverId = b.getString("RECEIVERID");
            //type = b.getString("TYPE");
        } catch (Exception e) {
            e.printStackTrace();
        }
        initView();
    }

    public void initView() {
        tv_no_record_found_Contacts = findViewById(R.id.tv_no_record_found_forward);
        recycleContacts = findViewById(R.id.recycle_forward_contact);
        back_toolbar = findViewById(R.id.back_forward_to_chat);
        forward_to_chat = findViewById(R.id.forward_to_chat);

        back_toolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        forward_to_chat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (mlist_multiselect.size() > 1) {
                    try {
                        Intent intent = new Intent(ForwardContact.this, ForwardMessageService.class);
                        intent.putExtra("mlist_multiselect", mlist_multiselect);
                        intent.putExtra("selected_messages", selectedmessages);
                        intent.putExtra("mlist_multiselect", (ArrayList<Datum>) mlist_multiselect);
                        intent.putExtra("selected_messages", (ArrayList<String>) selectedmessages);
                        startService(intent);

                        progressBar = Utilities.showProgress(ForwardContact.this);
                        Handler h = new Handler();
                        h.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                if (progressBar != null && progressBar.isShowing())
                                    progressBar.dismiss();
                                ForwardContact.this.finish();
                            }
                        }, 900);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    //new AsyncTaskForward().execute();
                } else if (mlist_multiselect.size() == 1) {

                    String user_username = qkPreferences.getValues(QKPreferences.USER_FULLNAME) + "";
                    String user_uniqueid = qkPreferences.getValues(QKPreferences.USER_UNIQUECODE) + "";

                    String username = mlist_multiselect.get(0).getScanUserName();
                    String profileUrl = mlist_multiselect.get(0).getScanUserProfileUrl();
                    String uniquecode = mlist_multiselect.get(0).getScanUserUniqueCode();

                    Intent chatIntent = new Intent(ForwardContact.this, NewChatActivity.class);
                    chatIntent.putExtra(Constants.EXTRA_CURRENT_USER_ID, user_uniqueid + "");
                    chatIntent.putExtra(Constants.EXTRA_RECIPIENT_ID, uniquecode + "");
                    chatIntent.putExtra(Constants.EXTRA_CHAT_REF, uniquecode + "");
                    chatIntent.putExtra(Constants.EXTRA_BLOCK_USER_ID, "0");
                    chatIntent.putExtra(Constants.EXTRA_CURRENT_USER_NAME, user_username + "");
                    chatIntent.putExtra(Constants.EXTRA_RECIPIENT_IMAGE, profileUrl + "");
                    chatIntent.putExtra(Constants.EXTRA_RECIPIENT_NAME, username + "");
                    chatIntent.putExtra(Constants.EXTRA_IS_FORWARD, "1");
                    chatIntent.putExtra(Constants.EXTRA_FORWARD_MESSAGE, selectedmessages + "");
                    startActivity(chatIntent);
                    finish();
                } else {
                    Utilities.showToast(ForwardContact.this, "Please select at list one contact.");
                }
            }
        });

        recycleContacts.addItemDecoration(new SimpleDividerItemDecoration(ForwardContact.this));
        recycleContacts.setLayoutManager(new LinearLayoutManager(ForwardContact.this));
        setupLongClickListner();
        showContacts("name");
    }

    public void showContacts(String filterBy) {

        String unique_code = qkPreferences.getValues(QKPreferences.USER_UNIQUECODE);

        RealmResults<Contacts> contacts = RealmController.with(ForwardContact.this).getContacts();

        mlist = new ArrayList<>();
        mlist_multiselect = new ArrayList<>();

        if (contacts != null && contacts.size() > 0) {

            for (Contacts contacts1 : contacts) {

                if (!String.valueOf(unique_code).equals(contacts1.getUnique_code()) && contacts1.getContact_type() != null && contacts1.getContact_type().equals(Constants.TYPE_CONTACTS)) {

                    Datum datum = new Datum();

                    datum.setEmail(contacts1.getEmail());
                    datum.setPhone(contacts1.getPhone());
                    datum.setScannedBy(contacts1.getScanned_by());
                    datum.setScanUserId(Integer.parseInt(contacts1.getUser_id().replace(".0", "")));

                    //String cap = contacts1.getUsername().substring(0, 1).toUpperCase() + contacts1.getUsername().substring(1);
                    String cap = contacts1.getUsername();
                    datum.setScanUserName(cap);

                    datum.setScanUserProfileUrl(contacts1.getProfile_pic());
                    datum.setScanUserUniqueCode(contacts1.getUnique_code());
                    datum.setScanDate(contacts1.getScan_date());
                    datum.setFirstname(contacts1.getFirstname());
                    datum.setLastname(contacts1.getLastname());
                    mlist.add(datum);
                }
            }
        }
        if (mlist != null && mlist.size() > 0) {
            recycleContacts.setVisibility(View.VISIBLE);
            tv_no_record_found_Contacts.setVisibility(View.GONE);
            //Utilities.shortContact(mlist, filterBy);
            newContactListAdapter = new NewContactListAdapter(ForwardContact.this, mlist, mlist_multiselect,false, onContactClick);
            recycleContacts.setAdapter(newContactListAdapter);
        } else {
            recycleContacts.setVisibility(View.GONE);
            tv_no_record_found_Contacts.setVisibility(View.VISIBLE);
        }
    }

    OnContactClick onContactClick = new OnContactClick() {

        @Override
        public void onLoadMore(String type) {

        }

        @Override
        public void getContact(Datum contactsList, int type) {

        }

        @Override
        public void getFollower(PojoClasses.FollowersList_Contact followerlist) {

        }

        @Override
        public void getFollowing(PojoClasses.FollowingList_Contact followinglist) {

        }
    };

    public void setupLongClickListner() {

        recycleContacts.addOnItemTouchListener(new RecyclerItemClickListener(ForwardContact.this, recycleContacts, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                boolean isTrue = true;
                if (!isMultiSelect) {
                    mlist_multiselect.clear();
                    isMultiSelect = true;
                    forward_to_chat.setVisibility(View.VISIBLE);
                    multi_select(position);
                } else {
                    if (mlist_multiselect != null && mlist_multiselect.size() > 0) {
                        for (int i = 0; i < mlist_multiselect.size(); i++) {
                            if (mlist_multiselect.get(i).getScanUserUniqueCode().equalsIgnoreCase(mlist.get(position).getScanUserUniqueCode())) {
                                try {
                                    //isMultiSelect = false;
                                    mlist_multiselect.remove(i);
                                    isTrue = false;
                                    newContactListAdapter.contactsLists_Selected = mlist_multiselect;
                                    newContactListAdapter.contactsLists = newContactListAdapter.getCurrentList();
                                    newContactListAdapter.notifyDataSetChanged();
                                   /* if(mlist_multiselect.size()<0){
                                        isMultiSelect = false;
                                        forward_to_chat.setVisibility(View.GONE);
                                    }*/
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                        if (isTrue) {
                            multi_select(position);
                        }
                    } else {
                        isMultiSelect = true;
                        multi_select(position);
                    }
                }
                /*if (mlist_multiselect != null && mlist_multiselect.size() == 0) {
                    isMultiSelect = false;
                    mlist_multiselect.clear();
                    forward_to_chat.setVisibility(View.GONE);
                }*/
                /*if (isMultiSelect) {

                }*/
            }

            @Override
            public void onItemLongClick(View view, int position) {
                if (!isMultiSelect) {
                    mlist_multiselect.clear();
                    isMultiSelect = true;
                    forward_to_chat.setVisibility(View.VISIBLE);
                }
                multi_select(position);
            }
        }));
    }

    public void multi_select(int position) {
        try {
            ArrayList<Datum> datumArrayList = newContactListAdapter.getCurrentList();
            if (mlist_multiselect.contains(datumArrayList.get(position))) {
                mlist_multiselect.remove(datumArrayList.get(position));
                if (mlist_multiselect != null && mlist_multiselect.size() == 0) {
                    isMultiSelect = false;
                    //frame_export.startAnimation(fabCloseAnimation);
                    //frame_export.setVisibility(View.GONE);
                    mlist_multiselect.clear();
                }
            } else {
                mlist_multiselect.add(datumArrayList.get(position));
            }
            refreshAdapter();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void refreshAdapter() {
        try {
            newContactListAdapter.contactsLists_Selected = mlist_multiselect;
            newContactListAdapter.contactsLists = newContactListAdapter.getCurrentList();
            newContactListAdapter.notifyDataSetChanged();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private class AsyncTaskForward extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... strings) {

            String user_username = qkPreferences.getValues(QKPreferences.USER_FULLNAME) + "";
            String user_uniqueid = qkPreferences.getValues(QKPreferences.USER_UNIQUECODE) + "";

            if (selectedmessages != null && selectedmessages.size() > 0 && mlist_multiselect != null && mlist_multiselect.size() > 0) {

                for (int i = 0; i < mlist_multiselect.size(); i++) {
                    String username = mlist_multiselect.get(i).getScanUserName();
                    String profileUrl = mlist_multiselect.get(i).getScanUserProfileUrl();
                    String uniquecode = mlist_multiselect.get(i).getScanUserUniqueCode();
                    String chatRef = "";
                    Long currentUserId = null;
                    Long ReceipientID = null;

                    try {
                        currentUserId = Long.parseLong(user_uniqueid);
                        ReceipientID = Long.parseLong(uniquecode);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    try {
                        if (currentUserId != null && ReceipientID != null) {
                            if (currentUserId > ReceipientID) {
                                chatRef = ReceipientID + "-" + currentUserId;
                            } else {
                                chatRef = currentUserId + "-" + ReceipientID;
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    messageChatDatabase = FirebaseDatabase.getInstance().getReference().child("chat").child(chatRef + "");
                    conversationDB = FirebaseDatabase.getInstance().getReference().child("conversation").child(chatRef);

                    for (int j = 0; j < selectedmessages.size(); j++) {
                        Conversation conversation = new Conversation(selectedmessages.get(j) + "", username, profileUrl, "", "", "", "", "");
                        conversationDB.setValue(conversation);
                        ChatMessage newMessage = new ChatMessage(selectedmessages.get(j) + "", user_uniqueid, user_username, "0", uniquecode, username, "", "", "", "");
                        messageChatDatabase.push().setValue(newMessage);
                        RealmController realmController = new RealmController(getApplication());
                        realmController.UpdateTanentListTime(username, selectedmessages.get(j) + "");
                    }
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            // execution of result of Long time consuming operation
            if (progressBar != null && progressBar.isShowing())
                progressBar.dismiss();
            finish();
        }

        @Override
        protected void onPreExecute() {
            progressBar = Utilities.showProgress(ForwardContact.this);
        }
    }
}