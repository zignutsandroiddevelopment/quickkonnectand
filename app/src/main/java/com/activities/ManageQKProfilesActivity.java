package com.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ExpandableListView;
import android.widget.TextView;

import com.adapter.AdpMyProfilesExpand;
import com.google.gson.Gson;
import com.models.profiles.ModelProfileList;
import com.models.profiles.ProfilesList;
import com.quickkonnect.Accept_Employee;
import com.quickkonnect.EmployeeDetails;
import com.quickkonnect.NewScanProfileActivity;
import com.quickkonnect.R;
import com.quickkonnect.UpdateUserProfileActivity;
import com.realmtable.MyProfiles;
import com.realmtable.RealmController;
import com.retrofitservice.ApiUtils;
import com.retrofitservice.SOService;
import com.utilities.Constants;
import com.utilities.QKPreferences;
import com.utilities.Utilities;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.realm.RealmResults;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ManageQKProfilesActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {

    private Context context;
    private QKPreferences qkPreferences;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private TextView tv_no_record_found;
    private AdpMyProfilesExpand adpMyProfilesExpand;
    private ExpandableListView expListView;
    private List<String> listGroupTitles;
    private HashMap<String, List<ProfilesList>> listDataMembers;
    private int RESULT_CODE_PROFILEDATAUPDATE = 100;
    private int RESULT_CODE_FROM_EMPOYEE_VIEW = 101;
    private SOService mService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_manage_qkprofiles);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close_white_24dp);

        if (Build.VERSION.SDK_INT >= 19 && Build.VERSION.SDK_INT < 21) {
            setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, true);
        }
        if (Build.VERSION.SDK_INT >= 19) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }
        if (Build.VERSION.SDK_INT >= 21) {
            setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, false);
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }

        getSupportActionBar().setTitle("Profiles");

        context = ManageQKProfilesActivity.this;

        mService = ApiUtils.getSOService();
        qkPreferences = new QKPreferences(context);

        mSwipeRefreshLayout = findViewById(R.id.swipeRefreshLayout_mqp);
        mSwipeRefreshLayout.setOnRefreshListener(ManageQKProfilesActivity.this);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary,
                android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_blue_dark);

        tv_no_record_found = findViewById(R.id.tv_no_record_found_mqp);

        expListView = findViewById(R.id.lvExp);

        resetAdapter();
    }

    public static void setWindowFlag(Activity activity, final int bits, boolean on) {
        Window win = activity.getWindow();
        WindowManager.LayoutParams winParams = win.getAttributes();
        if (on) {
            winParams.flags |= bits;
        } else {
            winParams.flags &= ~bits;
        }
        win.setAttributes(winParams);
    }


    public void resetAdapter() {

        listGroupTitles = new ArrayList<>();
        listDataMembers = new HashMap<>();

        adpMyProfilesExpand = new AdpMyProfilesExpand(context, listGroupTitles, listDataMembers);
        expListView.setAdapter(adpMyProfilesExpand);
        expListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                return true; // This way the expander cannot be collapsed
            }
        });
        expListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView expandableListView, View view, int i, int i1, long l) {

                List<ProfilesList> myProfiles = listDataMembers.get(listGroupTitles.get(i));

                ProfilesList myProfiles1 = myProfiles.get(i1);

                if (myProfiles1 != null) {

                    if (myProfiles1.getType() == 0) {

                        Intent pdate_user_profile = new Intent(ManageQKProfilesActivity.this, UpdateUserProfileActivity.class);
                        pdate_user_profile.putExtra("dashboard", "dashboard");
                        startActivity(pdate_user_profile);

                    } else if (myProfiles1.getType() == 4) {

                        Intent intent = new Intent(getApplicationContext(), Accept_Employee.class);
                        intent.putExtra(Constants.KEY, Constants.KEY_FROM_MANAGEPROFILES);
                        intent.putExtra("PROFILE_NAME", myProfiles1.getName() + "");
                        intent.putExtra("unique_id", myProfiles1.getUniqueCode() + "");
                        intent.putExtra("profile_id", "" + myProfiles1.getId());
                        //startActivity(intent);
                        //finish();
                        startActivityForResult(intent, RESULT_CODE_FROM_EMPOYEE_VIEW);

                    } else {

                        Intent intent = new Intent(context, NewScanProfileActivity.class);
                        intent.putExtra("unique_code", "" + myProfiles1.getUniqueCode());
                        intent.putExtra(Constants.KEY, Constants.KEY_FROM_MANAGEPROFILES);
                        intent.putExtra("profile_id", "" + myProfiles1.getId());// Profile Id here
                        intent.putExtra("ROLE", "" + myProfiles1.getRole());
                        intent.putExtra("ROLE_NAME", "" + myProfiles1.getRole_name());
                        startActivityForResult(intent, RESULT_CODE_PROFILEDATAUPDATE);
                    }
                }
                return false;
            }
        });

        showData(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.new_add_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                ManageQKProfilesActivity.this.finish();
                break;
            case R.id.ic_create_profile:
                Intent mngQKProfile = new Intent(context, CreateProfileActivity.class);
                startActivityForResult(mngQKProfile, RESULT_CODE_PROFILEDATAUPDATE);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == RESULT_CODE_PROFILEDATAUPDATE) {
            showData(false);
            if (Utilities.isNetworkConnected(context))
                getList(false);
        } else if (requestCode == RESULT_CODE_FROM_EMPOYEE_VIEW) {
            try {
                if (Utilities.isNetworkConnected(context)) {
                    resetAdapter();
                    getList(false);
                } else
                    showData(false);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onRefresh() {
        mSwipeRefreshLayout.setRefreshing(false);
        if (Utilities.isNetworkConnected(context)) {
            resetAdapter();
            getList(true);
        } else
            showData(false);
    }

    ProgressDialog progressDialog = null;

    private void getList(boolean isRefresh) {
        if (isRefresh) {
            progressDialog = Utilities.showProgress(context);
        }
        String token = "";
        if (qkPreferences != null) {
            token = qkPreferences.getApiToken() + "";
        }

        Map<String, String> params = new HashMap<>();
        params.put("Authorization", "Bearer " + token + "");
        params.put("Accept", "application/json");
        params.put("QK_ACCESS_KEY", getResources().getString(R.string.QK_ACCESS_KEY)+"");
        mService.getProfileList(params,qkPreferences.getValuesInt(QKPreferences.USER_ID)).enqueue(new Callback<ModelProfileList>() {
            @Override
            public void onResponse(Call<ModelProfileList> call, Response<ModelProfileList> response) {
                if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();
                Log.e("Profiles-Success", "" + response.body());
                try {
                    if (response != null && response.body() != null) {
                        ModelProfileList modelProfileList = response.body();
                        if (modelProfileList != null)
                            parseResponse(modelProfileList);
                    }
                } catch (Exception e) {
                    Log.e("Profiles-Error", "" + e.getMessage());
                }
            }

            @Override
            public void onFailure(Call<ModelProfileList> call, Throwable t) {
                Log.e("Profiles-Error", "" + t.getLocalizedMessage());
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        showData(true);
    }

    private void showData(boolean isreload) {
        try {
            RealmResults<MyProfiles> myProfiles = RealmController.with(ManageQKProfilesActivity.this).getMyProfiles();
            if (myProfiles != null && myProfiles.size() > 0) {

                List<ProfilesList> typedefault = new ArrayList<>();
                List<ProfilesList> typeCompany = new ArrayList<>();
                List<ProfilesList> typeEnterprise = new ArrayList<>();
                List<ProfilesList> typeCelebrity = new ArrayList<>();
                List<ProfilesList> typeEmployee = new ArrayList<>();

                listGroupTitles.clear();
                listDataMembers.clear();

                for (MyProfiles profiles : myProfiles) {

                    ProfilesList profilesList = RealmController.getRealmtoPojo(profiles);

                    if (isMyProfile(profilesList.getUniqueCode()) && profiles.getType() != null && profiles.getIsOwnProfile() != null && profiles.getIsOwnProfile().equals("true")) {

                        if (profiles.getType().equals("" + Constants.TYPE_PROFILE_DEFAULT))
                            typedefault.add(profilesList);

                        else if (profiles.getType().equals("" + Constants.TYPE_PROFILE_COMPANY))
                            typeCompany.add(profilesList);

                        else if (profiles.getType().equals("" + Constants.TYPE_PROFILE_ENTERPRISE))
                            typeEnterprise.add(profilesList);

                        else if (profiles.getType().equals("" + Constants.TYPE_PROFILE_CELEBRITY))
                            typeCelebrity.add(profilesList);

                        else if (profiles.getType().equals("" + Constants.TYPE_PROFILE_EMPLOYEE)) {

                            String companyProfile = profilesList.getCompany_profile();
                            if (companyProfile != null && !companyProfile.isEmpty()) {
                                try {
                                    JSONObject jsonObject = new JSONObject(companyProfile);
                                    String profile_image = jsonObject.optString("logo");
                                    profilesList.setLogo(profile_image);
                                    String profile_name = jsonObject.optString("name");
                                    profilesList.setName(profile_name);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                            typeEmployee.add(profilesList);
                        }

                    }
                }

                if (typeCompany != null && typeCompany.size() > 0) {
                    listGroupTitles.add("Company");
                    listDataMembers.put("Company", typeCompany);
                }

                if (typeEnterprise != null && typeEnterprise.size() > 0) {
                    listGroupTitles.add("Enterprise");
                    listDataMembers.put("Enterprise", typeEnterprise);
                }

                if (typeCelebrity != null && typeCelebrity.size() > 0) {
                    listGroupTitles.add("Celebrity/Public Figure");
                    listDataMembers.put("Celebrity/Public Figure", typeCelebrity);
                }

                if (typeEmployee != null && typeEmployee.size() > 0) {
                    listGroupTitles.add("Employee");
                    listDataMembers.put("Employee", typeEmployee);
                }

                if (adpMyProfilesExpand != null) {
                    adpMyProfilesExpand.notifyDataSetChanged();
                }

                if (listGroupTitles != null && listGroupTitles.size() > 0) {
                    for (int i = 0; i < listGroupTitles.size(); i++) {
                        expListView.expandGroup(i);
                    }
                }

                if (listDataMembers != null && listDataMembers.size() == 0) {
                    tv_no_record_found.setVisibility(View.VISIBLE);
                } else {
                    tv_no_record_found.setVisibility(View.GONE);
                }

                if (isreload)
                    getList(false);

            } else
                getList(true);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void parseResponse(ModelProfileList modelProfileList) {
        try {

            if (modelProfileList.getStatus() == 200) {

                List<ProfilesList> profilesLists = modelProfileList.getData();

                if (profilesLists != null && profilesLists.size() > 0) {

                    Log.e("Data", "" + modelProfileList);

                    tv_no_record_found.setVisibility(View.GONE);

                    List<String> myprofiles = new ArrayList<>();

                    for (ProfilesList profilesList : profilesLists) {

                        MyProfiles myProfiles = new MyProfiles();

                        myProfiles.setId("" + profilesList.getId());
                        myProfiles.setName(profilesList.getName());
                        myProfiles.setReason(profilesList.getResson());
                        myProfiles.setStatus(profilesList.getStatus());
                        myProfiles.setType("" + profilesList.getType());
                        myProfiles.setLogo("" + profilesList.getLogo());
                        myProfiles.setReason(profilesList.getResson() + "");
                        myProfiles.setUnique_code("" + profilesList.getUniqueCode());
                        myProfiles.setIs_transfer("" + profilesList.getIs_transfer());
                        myProfiles.setTransfer_status("" + profilesList.getTransfer_status());
                        String qktaginfo = new Gson().toJson(profilesList.getQkTagInfo());
                        myProfiles.setQktaginfo(qktaginfo);
                        myProfiles.setIsOwnProfile("true");

                        myprofiles.add(profilesList.getUniqueCode());

                        RealmController.with(ManageQKProfilesActivity.this).updateMyProfiles(myProfiles);
                    }

                    qkPreferences.storeMyProfiles(myprofiles);

                    showData(false);

                } else {
                    tv_no_record_found.setVisibility(View.VISIBLE);
                }
            } else {
                tv_no_record_found.setVisibility(View.VISIBLE);
                Utilities.showToast(context, modelProfileList.getMsg());
            }

        } catch (Exception e) {
            e.printStackTrace();
            tv_no_record_found.setVisibility(View.VISIBLE);
        }
    }

    public boolean isMyProfile(String uniquecode) {
        List<String> myprofiles = qkPreferences.getMyProfilesIds();
        if (myprofiles != null)
            for (String s : myprofiles) {
                if (s.equals(uniquecode))
                    return true;
            }
        return false;
    }
}
