package com.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.adapter.ContactListAdapter;
import com.interfaces.OnContactClick;
import com.models.contacts.Datum;
import com.quickkonnect.PojoClasses;
import com.quickkonnect.R;
import com.quickkonnect.SimpleDividerItemDecoration;
import com.realmtable.BlockList;
import com.realmtable.RealmController;
import com.retrofitservice.ApiUtils;
import com.retrofitservice.SOService;
import com.utilities.Constants;
import com.utilities.QKPreferences;
import com.utilities.Utilities;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Map;
import java.util.TimeZone;

import io.realm.RealmResults;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BlockedContactsActivity extends AppCompatActivity {

    private Context context;
    private QKPreferences qkPreferences;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private ContactListAdapter adapter;
    private SwipeRefreshLayout swipeRefreshLayout;
    private ArrayList<Datum> mlist;
    private TextView tv_no_record_found;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_blocked__profile_);

        Utilities.ChangeStatusBar(BlockedContactsActivity.this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_blocked_profile);
        //toolbar.setBackgroundResource(R.drawable.toolbar_gradient);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        context = BlockedContactsActivity.this;

        getSupportActionBar().setTitle(getResources().getString(R.string.blockedContacts));

        qkPreferences = new QKPreferences(context);

        swipeRefreshLayout = findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary,
                android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_blue_dark);

        tv_no_record_found = (TextView) findViewById(R.id.tv_no_record_found_Blocked_profile);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView_Blocked_profile);
        recyclerView.addItemDecoration(new SimpleDividerItemDecoration(context));
        recyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(mLayoutManager);

        if (Utilities.isNetworkConnected(context))
            getContactsList();

        else getContacts("name");

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                if (Utilities.isNetworkConnected(context))
                    getContactsList();

                else {
                    Utilities.showToast(context, getResources().getString(R.string.no_internat_connection));
                    swipeRefreshLayout.setRefreshing(false);
                }
            }
        });
    }

    private void getContactsList() {
        swipeRefreshLayout.setRefreshing(true);
        SOService soService = ApiUtils.getSOService();
        Map<String, String> headerMap = Utilities.getHeaderParameter(context);
        String user_id = qkPreferences.getLoggedUserid();
        soService.getBlockedContacts(headerMap, user_id, TimeZone.getDefault().getID()).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                swipeRefreshLayout.setRefreshing(false);
                try {
                    JSONObject jsonObject = new JSONObject(response.body().string());
                    parseResponse(jsonObject);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                swipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    public void parseResponse(JSONObject response) {
        try {
            String responseStatus = response.getString("status");
            String msg = response.getString("msg");
            Log.e("responese_block data", response + "");
            RealmController.with(BlockedContactsActivity.this).clearAllBlockedContacts();
            if (responseStatus.equals("200")) {
                JSONArray jsnArr = response.getJSONArray("data");
                recyclerView.setVisibility(View.VISIBLE);
                mlist = new ArrayList<>();
                for (int i = 0; i < jsnArr.length(); i++) {

                    JSONObject obj = jsnArr.getJSONObject(i);

                    BlockList bl = new BlockList();

                    bl.setUser_id(obj.getString("scan_user_id"));
                    bl.setUsername(obj.getString("scan_user_name"));
                    bl.setProfile_pic(obj.getString("scan_user_profile_url"));
                    bl.setFirstname(obj.getString("firstname"));
                    bl.setLastname(obj.getString("lastname"));
                    bl.setUnique_code(obj.getString("scan_user_unique_code"));

                    try {
                        String scanDate = obj.getString("scan_date");
                        if (Utilities.isEmpty(scanDate))
                            bl.setScan_date(Utilities.changeDateFormat("yyyy-MM-dd hh:mm:ss", "EEE, dd MMM yyyy hh:mm", scanDate));

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    RealmController.with(BlockedContactsActivity.this).createUpdateBlockedContacts(bl);
                }

                getContacts("name");

            } else {
                recyclerView.setVisibility(View.GONE);
                tv_no_record_found.setVisibility(View.VISIBLE);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void getContacts(String filterBy) {

        RealmResults<BlockList> blockLists = RealmController.with(BlockedContactsActivity.this).getBlockList();
        if (blockLists != null && blockLists.size() > 0) {
            mlist = new ArrayList<>();
            for (BlockList contacts1: blockLists) {

                Datum datum = new Datum();

                datum.setScanUserId(Integer.parseInt(contacts1.getUser_id().replace(".0", "")));

                String cap = contacts1.getUsername();
                datum.setScanUserName(cap);

                datum.setScanUserProfileUrl(contacts1.getProfile_pic());
                datum.setScanUserUniqueCode(contacts1.getUnique_code());
                datum.setScanDate(contacts1.getScan_date());
                datum.setFirstname(contacts1.getFirstname());
                datum.setLastname(contacts1.getLastname());

                mlist.add(datum);
            }
            Utilities.shortContact(mlist, filterBy);
            adapter = new ContactListAdapter(BlockedContactsActivity.this, mlist, new OnContactClick() {

                @Override
                public void onLoadMore(String type) {

                }

                @Override
                public void getContact(Datum contactsList, int type) {
                    Intent intent = new Intent(context, MatchProfileActivity.class);
                    intent.putExtra("unique_code", contactsList.getScanUserUniqueCode());
                    intent.putExtra(Constants.KEY, Constants.KEY_BLOCKED);
                    startActivityForResult(intent, 101);
                }

                @Override
                public void getFollower(PojoClasses.FollowersList_Contact followerlist) {

                }

                @Override
                public void getFollowing(PojoClasses.FollowingList_Contact followinglist) {

                }
            });
            recyclerView.setAdapter(adapter);
        } else {
            getContactsList();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == 101) {
            getContactsList();
        }
    }
}

