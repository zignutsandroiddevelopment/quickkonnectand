package com.activities;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Selection;
import android.text.Spanned;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.models.logindata.ContactDetail;
import com.models.logindata.ModelLoggedUser;
import com.quickkonnect.R;
import com.rilixtech.CountryCodePicker;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;
import com.utilities.Constants;
import com.utilities.QKPreferences;
import com.utilities.Utilities;
import com.volleyapirequest.VolleyApiRequest;
import com.volleyapirequest.VolleyCallBack;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

public class UpdateContactInfoActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener {

    private static SharedPreferences mSharedPreferences;
    String userid;
    SearchableSpinner sp_type;
    EditText edt_contact_email, edt_contact_phone;
    String contact_type, email, phone;
    private String prevContact = "", prevemail = "", prevphone = "";
    int contact_id;
    TextView tv_delete;
    RelativeLayout view;
    ArrayAdapter<String> spinnerArrayAdapter;
    private Context context;
    private ContactDetail contactDetail;
    private QKPreferences qkPreferences;
    private ModelLoggedUser modelUserProfile;
    private TextView tvTypelbl;
    boolean isEdit = false;
    private CountryCodePicker ccp;
    boolean isOnCreate = true;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_update_contact_info);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Utilities.ChangeStatusBar(UpdateContactInfoActivity.this);

        try {

            Bundle b = getIntent().getExtras();

            if (b.getString(Constants.KEY_UPDATE).equalsIgnoreCase(Constants.ADD_DATA))
                getSupportActionBar().setTitle(getResources().getString(R.string.addcontactinformation));

            else if (b.getString(Constants.KEY_UPDATE).equalsIgnoreCase(Constants.EDIT_DATA)) {
                getSupportActionBar().setTitle(getResources().getString(R.string.updateinformation));
                isEdit = true;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        context = UpdateContactInfoActivity.this;
        qkPreferences = new QKPreferences(context);

        userid = "" + qkPreferences.getLoggedUser().getData().getId();

        modelUserProfile = qkPreferences.getLoggedUser();

        sp_type = findViewById(R.id.sp_type);
        edt_contact_email = findViewById(R.id.edt_contact_email);
        tvTypelbl = findViewById(R.id.tvTypelbl);
        edt_contact_phone = findViewById(R.id.edt_contact_phone);
        tv_delete = findViewById(R.id.tv_delete);
        view = findViewById(R.id.r1);

        tvTypelbl.setText(getResources().getString(R.string.personal));
        contact_type = getResources().getString(R.string.personal);
        prevContact = getResources().getString(R.string.personal);
        sp_type.setTitle(getResources().getString(R.string.selectType));
        //String gender[] = {"Personal", "Business", "Home", "Work", "Mobile", "Main", "Other", "Custom Label"};
        String contactsType[] = getResources().getStringArray(R.array.itemsContactsType);
        spinnerArrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, contactsType);
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
        sp_type.setAdapter(spinnerArrayAdapter);

        ccp = (CountryCodePicker) view.findViewById(R.id.ccp_update);
        ccp.enablePhoneAutoFormatter(true);
        ccp.setVisibility(View.VISIBLE);
        tv_delete.setOnClickListener(this);
        sp_type.setOnItemSelectedListener(this);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close_white_24dp);
        Bundle extras = getIntent().getExtras();

        edt_contact_phone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (edt_contact_phone.getText().toString().length() == 1 && edt_contact_phone.getText().toString().equalsIgnoreCase("0")) {
                    edt_contact_phone.setText("");
                } else {
                    edt_contact_phone.setFilters(new InputFilter[]{filter1, new InputFilter.LengthFilter(15)});
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        if (isEdit) {
            contactDetail = extras.getParcelable("contactInfo");
            if (contactDetail != null && contactDetail.getContactType() != null) {
                //edt_education_school.setText(school_name);
//                sp_type.setSelection(getIndex(sp_type, contactDetail.getContactType()));
                contact_type = contactDetail.getContactType();
                prevContact = contact_type;
                tvTypelbl.setText(contact_type);
            }

            if (contactDetail != null && contactDetail.getEmail() != null && Utilities.isEmpty(contactDetail.getEmail())) {
                prevemail = contactDetail.getEmail();
                edt_contact_email.setText(contactDetail.getEmail());
            }

            if (contactDetail != null && contactDetail.getPhone() != null && Utilities.isEmpty("" + contactDetail.getPhone())) {
                try {
                    String CurrentString = contactDetail.getPhone() + "";
                    String[] separated = CurrentString.split(" ");
                    ccp.setCountryForPhoneCode(Integer.parseInt(separated[0]));

                    if (separated.length > 1) {
                        prevphone = separated[1];
                        edt_contact_phone.setText(separated[1]);

                    } else {
                        prevphone = separated[0];
                        edt_contact_phone.setText(separated[0]);
                    }

                } catch (Exception e) {
                    prevphone = String.valueOf(contactDetail.getPhone());
                    edt_contact_phone.setText("" + contactDetail.getPhone());
                    e.printStackTrace();
                }
            }
            tv_delete.setVisibility(View.VISIBLE);
        }
        SetFontStyle();
    }

    //private method of your class
    private int getIndex(Spinner spinner, String myString) {
        int index = 0;

        for (int i = 0; i < spinner.getCount(); i++) {
            if (spinner.getItemAtPosition(i).toString().equalsIgnoreCase(myString)) {
                index = i;
                break;
            }
        }
        return index;
    }

    @Override
    public void onClick(final View view) {

        switch (view.getId()) {
            case R.id.tv_delete:

                new AlertDialog.Builder(this)
                        .setMessage(getResources().getString(R.string.delete_confirmation_message_contact))
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int whichButton) {
                                if (!Utilities.isNetworkConnected(UpdateContactInfoActivity.this))
                                    Toast.makeText(UpdateContactInfoActivity.this, Constants.NO_INTERNET_CONNECTION, Toast.LENGTH_LONG).show();
                                else {
                                    Utilities.hideKeyboard(UpdateContactInfoActivity.this);
                                    DeleteContact(view, contact_id);
                                }
                            }
                        })
                        .setNegativeButton(android.R.string.no, null).show();


                break;
        }

    }

    public void SetFontStyle() {
        Typeface lato_regular = Typeface.createFromAsset(getAssets(), "fonts/latoregular.ttf");
        Typeface lato_bold = Typeface.createFromAsset(getAssets(), "fonts/latobold.ttf");
        edt_contact_email.setTypeface(lato_regular);
        edt_contact_phone.setTypeface(lato_regular);
        tv_delete.setTypeface(lato_bold);

    }

    // Update
    public void functionUpdateContactInfo(final View view, int contact_id, final String contact_type, final String email, final String phone) {

        JSONObject obj = new JSONObject();
        try {
            obj.put("contact_id", contact_id);
            obj.put("contact_type", contact_type);
            obj.put("email", email);
            obj.put("phone", phone);


        } catch (JSONException e) {
            e.printStackTrace();
        }

        new VolleyApiRequest(context, true, VolleyApiRequest.REQUEST_BASEURL + "api/user_contact_profile_edit",
                new VolleyCallBack() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {

                            String responseStatus = response.getString("status");
                            String msg = response.getString("msg");
                            if (responseStatus.equals("200")) {

                                if (modelUserProfile != null) {

                                    List<ContactDetail> contactDetailList = modelUserProfile.getData().getContactDetail();
                                    int index = 0;
                                    for (ContactDetail contactDetails: contactDetailList) {
                                        if (contactDetails.getContactId() != null &&
                                                contactDetails.getContactId().equals(contactDetail.getContactId())) {
                                            index = contactDetailList.indexOf(contactDetails);
                                            break;
                                        }
                                    }

                                    JSONObject object = response.getJSONObject("data");
                                    // contactDetail.setContactId(object.getString("contact_id"));
                                    contactDetail.setContactType(object.getString("contact_type"));
                                    contactDetail.setEmail(object.getString("email"));
                                    contactDetail.setPhone(object.getString("phone"));

                                    contactDetailList.set(index, contactDetail);

                                    modelUserProfile.getData().setContactDetail(contactDetailList);

                                    qkPreferences.storeLoggedUser(modelUserProfile);

                                    setResult(RESULT_OK);
                                    UpdateContactInfoActivity.this.finish();

                                }

                            } else
                                Utilities.showToast(context, msg);


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyApiRequest.showVolleyError(context, error);
                    }
                }).enqueRequest(obj, Request.Method.POST);


    }

    // Add
    public void functionAddContactInfo(final View view, String user_id, final String contact_type, final String email, final String phone) {

        JSONObject obj = new JSONObject();
        try {
            obj.put("user_id", user_id);
            obj.put("contact_type", contact_type);
            obj.put("email", email);
            obj.put("phone", phone);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        new VolleyApiRequest(context, true, VolleyApiRequest.REQUEST_BASEURL + "api/user_contact_profile_add",
                new VolleyCallBack() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {

                            List<ContactDetail> contactDetails = modelUserProfile.getData().getContactDetail();
                            if (contactDetails == null)
                                contactDetails = new ArrayList<>();

                            ContactDetail contactDetail = new ContactDetail();
                            JSONObject object = response.getJSONObject("data");
                            contactDetail.setContactId(Integer.parseInt(object.getString("contact_id")));
                            contactDetail.setContactType(object.getString("contact_type"));
                            contactDetail.setEmail(object.getString("email"));
                            contactDetail.setPhone(object.getString("phone"));

                            contactDetails.add(contactDetail);

                            modelUserProfile.getData().setContactDetail(contactDetails);

                            qkPreferences.storeLoggedUser(modelUserProfile);

                            setResult(RESULT_OK);
                            UpdateContactInfoActivity.this.finish();

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyApiRequest.showVolleyError(context, error);
                    }
                }).enqueRequest(obj, Request.Method.POST);


    }

    //Delete
    //Contact Delete
    public void DeleteContact(final View view, int contactid) {

        new VolleyApiRequest(context, true, VolleyApiRequest.REQUEST_BASEURL + "api/delete_contact_detail/" + contactDetail.getContactId(),
                new VolleyCallBack() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {

                            String responseStatus = response.getString("status");
                            String msg = response.getString("msg");
                            if (responseStatus.equals("200")) {

                                List<ContactDetail> contactDetailList = modelUserProfile.getData().getContactDetail();

                                for (int i = 0; i < contactDetailList.size(); i++) {
                                    if (contactDetailList.get(i).getContactId().equals(contactDetail.getContactId())) {
                                        contactDetailList.remove(i);
                                        break;
                                    }
                                }

                                modelUserProfile.getData().setContactDetail(contactDetailList);

                                qkPreferences.storeLoggedUser(modelUserProfile);

                                setResult(RESULT_OK);
                                UpdateContactInfoActivity.this.finish();

                            } else
                                Utilities.showToast(context, msg);


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyApiRequest.showVolleyError(context, error);
                    }
                }).enqueRequest(null, Request.Method.GET);


    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        Log.e("Custom", "" + i);
        if (i == 7) {
            // Add Custom Label here
            addCustomeLabel();
        } else {
            if (!isOnCreate) {
                contact_type = adapterView.getItemAtPosition(i).toString();
                tvTypelbl.setText(contact_type);
                Log.d("contact_type", "onItemSelected: " + contact_type);
            } else
                isOnCreate = false;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.custom_save_update, menu);
        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.ic_save_update:

                Utilities.hideKeyboard(UpdateContactInfoActivity.this);

                //userid ="20";

                String email = edt_contact_email.getText().toString().trim();

                String phone = "";

                if (!edt_contact_phone.getText().toString().equalsIgnoreCase("")) {
                    phone = "+" + ccp.getSelectedCountryCode() + " " + edt_contact_phone.getText().toString();
                }

                if (contact_type == null || contact_type == "" || contact_type.equals("") || contact_type.equals("null"))
                    Utilities.showToast(context, getResources().getString(R.string.contacttyperequired));

                else if (!Utilities.isValidEmail(email))
                    Utilities.showToast(context, getResources().getString(R.string.invalidemail));

                else {
                    Utilities.hideKeyboard(UpdateContactInfoActivity.this);

                    if (contactDetail != null && contactDetail.getContactId() != null) {

                        if (!Utilities.isNetworkConnected(this))
                            Utilities.showToast(context, getResources().getString(R.string.no_internat_connection));

                        else
                            functionUpdateContactInfo(view, contactDetail.getContactId(), contact_type, edt_contact_email.getText().toString().trim(), phone);

                    } else {

                        if (!Utilities.isNetworkConnected(this))
                            Toast.makeText(this, Constants.NO_INTERNET_CONNECTION, Toast.LENGTH_LONG).show();
                        else
                            functionAddContactInfo(view, userid, contact_type, edt_contact_email.getText().toString().trim(), phone);
                    }
                }
                return true;

            case android.R.id.home:
                exitfromActivity();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        exitfromActivity();
    }

    public void exitfromActivity() {
        if (isFoundanyupdate()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(UpdateContactInfoActivity.this);
            builder.setTitle(getResources().getString(R.string.alert));
            builder.setMessage(getResources().getString(R.string.exitwithoutsave));
            builder.setPositiveButton(getResources().getString(R.string.exit), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    Utilities.hideKeyboard(UpdateContactInfoActivity.this);
                    setResult(RESULT_CANCELED);
                    UpdateContactInfoActivity.this.finish();
                }
            });
            builder.setNegativeButton(getResources().getString(R.string.close), null);
            builder.show();

        } else {

            Utilities.hideKeyboard(UpdateContactInfoActivity.this);
            setResult(RESULT_CANCELED);
            UpdateContactInfoActivity.this.finish();
        }
    }

    public boolean isFoundanyupdate() {

        String email = edt_contact_email.getText().toString().trim();
        String phone = edt_contact_phone.getText().toString().trim();


        if (prevemail != null && !prevemail.equals(email)) {
            return true;

        } else if (prevContact != null && !prevContact.equals(contact_type)) {
            return true;

        } else if (prevphone != null && !prevphone.equals(phone)) {
            return true;
        }

        return false;
    }

    InputFilter filter1 = new InputFilter() {
        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
            for (int i = start; i < end; ++i) {
                if (!Pattern.compile("[0123456789]*").matcher(String.valueOf(source.charAt(i))).matches()) {
                    return "";
                }
            }
            return null;
        }
    };

    private boolean isValidPhoneNumber(CharSequence phoneNumber) {
        String num = phoneNumber.toString();
        if (num.startsWith("1") || num.startsWith("2") || num.startsWith("3") || num.startsWith("4") || num.startsWith("5") || num.startsWith("6") || num.startsWith("7") || num.startsWith("8") || num.startsWith("9")) {
            return true;
        } else {
            return false;
        }
    }

    public void addCustomeLabel() {

        final Dialog dialog = new Dialog(UpdateContactInfoActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View view = LinearLayout.inflate(UpdateContactInfoActivity.this, R.layout.dialog_addsocialmedia, null);
        dialog.setContentView(view);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        dialog.show();
        dialog.getWindow().setAttributes(lp);
        TextView tvAlert = view.findViewById(R.id.tvAlert);
        TextView tvDone = view.findViewById(R.id.tvDone);
        TextView tvCancel = view.findViewById(R.id.tvCancel);
        CountryCodePicker ccp = (CountryCodePicker) view.findViewById(R.id.ccp);
        LinearLayout lin_cpp = view.findViewById(R.id.lin_cpp);

        view.findViewById(R.id.divider_select_socialmedia).setVisibility(View.GONE);
        ccp.setVisibility(View.GONE);
        tvAlert.setText(getResources().getString(R.string.createcustom));
        final SearchableSpinner spinnerSocialmedia = view.findViewById(R.id.spinnerSocialMedia);
        spinnerSocialmedia.setVisibility(View.GONE);
        final EditText edtSocialmediaLInk = view.findViewById(R.id.edt_socialmedia_link);
        //edtSocialmediaLInk.setText(Url);
        Selection.setSelection(edtSocialmediaLInk.getText(), edtSocialmediaLInk.getText().length());
        tvDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    Utilities.hidekeyboard(UpdateContactInfoActivity.this, view);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (!edtSocialmediaLInk.getText().toString().isEmpty()) {
                    dialog.dismiss();
                    contact_type = edtSocialmediaLInk.getText().toString() + "";
                    tvTypelbl.setText(contact_type);
                } else {
                    Utilities.showToast(UpdateContactInfoActivity.this, "Please enter name");
                }
            }
        });

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.show();

    }
}