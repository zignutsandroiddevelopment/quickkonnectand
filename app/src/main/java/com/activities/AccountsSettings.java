package com.activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.quickkonnect.Blocked_Profile;
import com.quickkonnect.ChangePasswordActivity;
import com.quickkonnect.Introduction_Activity;
import com.quickkonnect.R;
import com.realmtable.RealmController;
import com.retrofitservice.ApiUtils;
import com.utilities.QKPreferences;
import com.utilities.Utilities;
import com.volleyapirequest.VolleyApiRequest;
import com.volleyapirequest.VolleyCallBack;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Zignuts Technolab pvt. ltd on 17-03-2018.
 */

public class AccountsSettings extends AppCompatActivity {//implements View.OnClickListener {

    private Context context;
    //    private LinearLayout linearLayout, lin_change_password;
//    private TextView tv_block_profile, tv_change_password, tv_app_activation, tv_block;
    private QKPreferences qkPreferences;
    private String userid;
    private RecyclerView recycleHelpSettings;
    private AdapterAccountSetting adapterAccountSetting;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.helpaccountsetting);

        Utilities.ChangeStatusBar(AccountsSettings.this);
        try {
            Toolbar toolbar = findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
            android.support.v7.app.ActionBar actionBar = getSupportActionBar();
            actionBar.setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close_white_24dp);
        } catch (Exception e) {
            e.printStackTrace();
        }

        context = AccountsSettings.this;

        qkPreferences = new QKPreferences(AccountsSettings.this);

        userid = qkPreferences.getLoggedUser().getData().getId() + "";

        initWidget();
    }

    public void initWidget() {

//        lin_change_password = findViewById(R.id.lin_change_password);

//        if (qkPreferences.getLoginFrom() != null) {
//            String loginfrom = qkPreferences.getLoginFrom();
//            if (loginfrom.equalsIgnoreCase("SOCIAL"))
//                lin_change_password.setVisibility(View.GONE);
//            else
//                lin_change_password.setVisibility(View.VISIBLE);
//        } else {
//            lin_change_password.setVisibility(View.VISIBLE);
//        }

//        linearLayout = findViewById(R.id.llaccountsetting);
//        linearLayout.setVisibility(View.VISIBLE);

//        tv_block_profile = findViewById(R.id.tv_block_profile);
//        tv_change_password = findViewById(R.id.tv_change_password);
//        tv_app_activation = findViewById(R.id.tv_app_activation);
//        tv_block = findViewById(R.id.tv_block);
//
//        tv_block_profile.setOnClickListener(this);
//        tv_change_password.setOnClickListener(this);
//        tv_app_activation.setOnClickListener(this);
//        tv_block.setOnClickListener(this);

        recycleHelpSettings = findViewById(R.id.recycleHelpSettings);
        recycleHelpSettings.setLayoutManager(new LinearLayoutManager(this));
        setupAccountSettings();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                AccountsSettings.this.finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

//    @Override
//    public void onClick(View view) {
//        switch (view.getId()) {
//
//            case R.id.tv_block:
//                Intent block = new Intent(context, BlockedContactsActivity.class);
//                startActivity(block);
//                break;
//
//            case R.id.tv_block_profile:
//                Intent blockp = new Intent(context, Blocked_Profile.class);
//                startActivity(blockp);
//                break;
//
//            case R.id.tv_change_password:
//                Intent changepassword = new Intent(context, ChangePasswordActivity.class);
//                startActivity(changepassword);
//                break;
//
//            case R.id.tv_app_activation:
//                final CharSequence[] items = {getResources().getString(R.string.deactivateaccount),
//                        getResources().getString(R.string.deleteAccount)};
//                AlertDialog.Builder builder = new AlertDialog.Builder(context);
//                builder.setTitle(getResources().getString(R.string.makeyourselection));
//                builder.setItems(items, new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int item) {
//                        // Do something with the selection
//                        if (item == 0) {
//                            // Deactivate Account
//                            if (!Utilities.isNetworkConnected(AccountsSettings.this))
//                                Utilities.showToast(context, getResources().getString(R.string.no_internat_connection));
//                            else
//                                showDeleteconfirmation(false);
//
//                        } else if (item == 1) {
//                            //Delete  Account
//                            if (!Utilities.isNetworkConnected(AccountsSettings.this))
//                                Utilities.showToast(context, getResources().getString(R.string.no_internat_connection));
//                            else
//                                showDeleteconfirmation(true);
//                        }
//                    }
//                });
//                builder.setPositiveButton(getResources().getString(R.string.cancel), null);
//                builder.show();
//                break;
//        }
//    }

    public void setupAccountSettings() {

        List<ModelAccountSettings> helpSettings = new ArrayList<>();

        ModelAccountSettings modelHelpSettings = new ModelAccountSettings();

        boolean isFromSocial = false;
        if (qkPreferences.getLoginFrom() != null) {
            String loginfrom = qkPreferences.getLoginFrom();
            if (loginfrom.equalsIgnoreCase("SOCIAL"))
                isFromSocial = true;
            else
                isFromSocial = false;
        }

        if (!isFromSocial) {
            modelHelpSettings.setIcon(R.drawable.changepassword);
            modelHelpSettings.setTitle(getResources().getString(R.string.change_password));
            modelHelpSettings.setType(TYPE_CHANGEPASS);
            helpSettings.add(modelHelpSettings);
        }

        modelHelpSettings = new ModelAccountSettings();
        modelHelpSettings.setIcon(R.drawable.blockcontact);
        modelHelpSettings.setTitle(getResources().getString(R.string.blockedContacts));
        modelHelpSettings.setType(TYPE_BLOCKEDCONTACTS);
        helpSettings.add(modelHelpSettings);

        modelHelpSettings = new ModelAccountSettings();
        modelHelpSettings.setIcon(R.drawable.blockuser);
        modelHelpSettings.setTitle(getResources().getString(R.string.blockedProfiles));
        modelHelpSettings.setType(TYPE_BLOCKEDPROFILES);
        helpSettings.add(modelHelpSettings);

        modelHelpSettings = new ModelAccountSettings();
        modelHelpSettings.setIcon(R.drawable.app_activation);
        modelHelpSettings.setTitle(getResources().getString(R.string.delete_account));
        modelHelpSettings.setType(TYPE_DELETEACCOUNT);
        helpSettings.add(modelHelpSettings);

        adapterAccountSetting = new AdapterAccountSetting(context, helpSettings);
        recycleHelpSettings.setAdapter(adapterAccountSetting);
    }


    public class AdapterAccountSetting extends RecyclerView.Adapter<AdapterAccountSetting.AccountSettingHolder> {

        private Context context;
        private List<ModelAccountSettings> helpSettings;

        public AdapterAccountSetting(Context context, List<ModelAccountSettings> helpSettings) {
            this.context = context;
            this.helpSettings = helpSettings;
        }

        @NonNull
        @Override

        public AdapterAccountSetting.AccountSettingHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(context).inflate(R.layout.item_helpsetting, parent, false);
            return new AdapterAccountSetting.AccountSettingHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull AdapterAccountSetting.AccountSettingHolder holder, final int position) {
            holder.switchOnOff.setVisibility(View.GONE);
            holder.imgIcon.setImageResource(helpSettings.get(position).getIcon());
            holder.tvTitle.setText(helpSettings.get(position).getTitle());
            holder.tvTitle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    switch (helpSettings.get(position).getType()) {

                        case TYPE_CHANGEPASS:
                            startActivity(new Intent(context, ChangePasswordActivity.class));
                            break;

                        case TYPE_BLOCKEDCONTACTS:
                            startActivity(new Intent(context, BlockedContactsActivity.class));
                            break;

                        case TYPE_BLOCKEDPROFILES:
                            startActivity(new Intent(context, Blocked_Profile.class));
                            break;

                        case TYPE_DELETEACCOUNT:
                            showDeleteAccountDialog();
                            break;
                    }
                }
            });
        }

        @Override
        public int getItemCount() {
            return helpSettings.size();
        }

        public class AccountSettingHolder extends RecyclerView.ViewHolder {

            ImageView imgIcon;
            TextView tvTitle;
            Switch switchOnOff;

            public AccountSettingHolder(View itemView) {
                super(itemView);
                imgIcon = itemView.findViewById(R.id.imgIcon);
                tvTitle = itemView.findViewById(R.id.tvTitle);
                switchOnOff = itemView.findViewById(R.id.switchOnOff);
            }
        }
    }

    public static final int TYPE_CHANGEPASS = 1;
    public static final int TYPE_BLOCKEDCONTACTS = 2;
    public static final int TYPE_BLOCKEDPROFILES = 3;
    public static final int TYPE_DELETEACCOUNT = 4;

    public class ModelAccountSettings {

        private int icon;
        private String title;
        private int type;

        public int getIcon() {
            return icon;
        }

        public void setIcon(int icon) {
            this.icon = icon;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public int getType() {
            return type;
        }

        public void setType(int type) {
            this.type = type;
        }
    }

    public void showDeleteAccountDialog() {
        final CharSequence[] items = {getResources().getString(R.string.deactivateaccount),
                getResources().getString(R.string.deleteAccount)};
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(getResources().getString(R.string.makeyourselection));
        builder.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                // Do something with the selection
                if (item == 0) {
                    // Deactivate Account
                    if (!Utilities.isNetworkConnected(AccountsSettings.this))
                        Utilities.showToast(context, getResources().getString(R.string.no_internat_connection));
                    else
                        showDeleteconfirmation(false);

                } else if (item == 1) {
                    //Delete  Account
                    if (!Utilities.isNetworkConnected(AccountsSettings.this))
                        Utilities.showToast(context, getResources().getString(R.string.no_internat_connection));
                    else
                        showDeleteconfirmation(true);
                }
            }
        });
        builder.setPositiveButton(getResources().getString(R.string.cancel), null);
        builder.show();
    }


    public void showDeleteconfirmation(final boolean isDelete) {

        AlertDialog.Builder builder = new AlertDialog.Builder(AccountsSettings.this);
        builder.setTitle(getResources().getString(R.string.alert));
        if (isDelete) {
            builder.setMessage(getResources().getString(R.string.deleteMsg));
        } else {
            builder.setMessage(getResources().getString(R.string.disabledAccount));
        }
        builder.setPositiveButton(getResources().getString(R.string.action_continue), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                if (isDelete) {
                    //Delete
                    deleteUserAccount(userid, "P");
                } else {
                    //Deactive
                    deleteUserAccount(userid, "T");
                }
            }
        });
        builder.setNegativeButton(getResources().getString(R.string.cancel), null);
        builder.show();
    }

    // User Account Temporary Delete
    public void deleteUserAccount(String user_id, String delete_type) {

        JSONObject obj = new JSONObject();
        try {
            obj.put("user_id", user_id);
            obj.put("delete_type", delete_type);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.e("Object -> ", obj.toString());

        new VolleyApiRequest(AccountsSettings.this, true, VolleyApiRequest.REQUEST_BASEURL + "api/user_account_delete",
                new VolleyCallBack() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String status = response.getString("status");
                            String message = response.getString("msg");
                            if (status.equals(ApiUtils.RESPONSE_CODE_SUCCESS)) {
                                qkPreferences.clearQKPreference();

                                RealmController.with(AccountsSettings.this).clearChatConversation();
                                RealmController.with(AccountsSettings.this).clearChatuserList();
                                RealmController.with(AccountsSettings.this).clearAllTables();
                                Intent intent = new Intent(AccountsSettings.this, Introduction_Activity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                /* intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);*/
                                startActivity(intent);
                                AccountsSettings.this.finish();
                            } else if (status.equals("400")) {
                                Utilities.showToast(AccountsSettings.this, "" + message);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyApiRequest.showVolleyError(AccountsSettings.this, error);
                    }
                }).enqueRequest(obj, Request.Method.POST);
    }
}
