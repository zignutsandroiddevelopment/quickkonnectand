package com.activities;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.adapter.SchoolListAdapter;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.models.logindata.EducationDetail;
import com.models.logindata.ModelLoggedUser;
import com.models.schoollist.Datum;
import com.models.schoollist.SchoolListPojo;
import com.quickkonnect.R;
import com.realmtable.RealmController;
import com.realmtable.SchoolListTbl;
import com.retrofitservice.ApiUtils;
import com.retrofitservice.SOService;
import com.utilities.Constants;
import com.utilities.QKPreferences;
import com.utilities.Utilities;
import com.volleyapirequest.VolleyApiRequest;
import com.volleyapirequest.VolleyCallBack;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.realm.Realm;
import io.realm.RealmResults;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UpdateEducationActivity extends AppCompatActivity implements View.OnClickListener {

    private String userid;
    private AutoCompleteTextView autoComplete_edt_education_school;
    private EditText /*edt_education_school,*/ edt_education_degree, edt_education_field_of_study, edt_education_start_date, edt_education_end_date, edt_education_activities;
    private TextView tv_delete_education;
    private LinearLayout view;
    private int mYear, mMonth, mDay;
    private Calendar c;
    private QKPreferences qkPreferences;
    private ModelLoggedUser modelUserProfile;
    private EducationDetail educationDetail;
    private Context context;
    boolean isEdit = false, firts = true;
    private ArrayList<SchoolList> schoolLists;
    private String prevschoolname = "", prevDegree = "", prevFiledofStudy = "", prevStartDate = "", prevEndDate = "", prevDesc = "";
    SchoolListAdapter schoolListAdapter;
    SOService mService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_education);

        Utilities.ChangeStatusBar(UpdateEducationActivity.this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mService = ApiUtils.getSOService();

        try {
            Bundle b = getIntent().getExtras();
            if (b.getString(Constants.KEY_UPDATE).equalsIgnoreCase(Constants.ADD_DATA)) {
                getSupportActionBar().setTitle("Add Education");
            } else if (b.getString(Constants.KEY_UPDATE).equalsIgnoreCase(Constants.EDIT_DATA)) {
                getSupportActionBar().setTitle("Update Education");
                isEdit = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close_white_24dp);

        context = UpdateEducationActivity.this;
        qkPreferences = new QKPreferences(context);
        userid = "" + qkPreferences.getLoggedUser().getData().getId();

        getSchoolList();

        modelUserProfile = qkPreferences.getLoggedUser();

        //Education EditText
        autoComplete_edt_education_school = findViewById(R.id.autoComplete_edt_education_school);
        edt_education_degree = findViewById(R.id.edt_education_degree);
        edt_education_field_of_study = findViewById(R.id.edt_education_field_of_study);
        edt_education_start_date = findViewById(R.id.edt_education_start_date);
        edt_education_end_date = findViewById(R.id.edt_education_end_date);
        edt_education_activities = findViewById(R.id.edt_education_activities);
        tv_delete_education = findViewById(R.id.tv_delete_education);


        if (isEdit) {
            autoComplete_edt_education_school.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                }

                @Override
                public void afterTextChanged(Editable s) {
                    if (firts) {
                        firts = false;
                        schoolLists = new ArrayList<>();
                        FillSchoolList(true);
                        schoolListAdapter = new SchoolListAdapter(UpdateEducationActivity.this, R.layout.custom_row, schoolLists);
                        autoComplete_edt_education_school.setThreshold(1);
                        autoComplete_edt_education_school.setAdapter(schoolListAdapter);
                    }
                }
            });
        } else {
            schoolLists = new ArrayList<>();
            FillSchoolList(true);
            schoolListAdapter = new SchoolListAdapter(this, R.layout.custom_row, schoolLists);
            autoComplete_edt_education_school.setThreshold(1);
            autoComplete_edt_education_school.setAdapter(schoolListAdapter);
        }

        tv_delete_education.setOnClickListener(this);

        view = findViewById(R.id.r1);


        // Get Current Date

        c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        edt_education_start_date.setOnClickListener(this);
        edt_education_end_date.setOnClickListener(this);

        try {
            Date c = Calendar.getInstance().getTime();
            SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
            String formattedDate = df.format(c);
            edt_education_end_date.setTag(formattedDate);


        } catch (Exception e) {
            e.printStackTrace();
        }

        Bundle extras = getIntent().getExtras();

        //if (extras != null) {
        if (isEdit) {
            try {
                tv_delete_education.setVisibility(View.VISIBLE);
                getSupportActionBar().setTitle("Update Education");

                educationDetail = extras.getParcelable("education");
                Log.d("Education", "" + educationDetail);

                if (educationDetail.getSchoolName() != null && educationDetail.getSchoolName().length() > 0
                        && !educationDetail.getSchoolName().equals("null") && Utilities.isEmpty(educationDetail.getSchoolName())) {
                    prevschoolname = educationDetail.getSchoolName();
                    autoComplete_edt_education_school.setText(educationDetail.getSchoolName());

                }
                if (educationDetail.getDegree() != null && educationDetail.getDegree().length() > 0
                        && !educationDetail.getDegree().equals("null") && Utilities.isEmpty(educationDetail.getDegree())) {
                    prevDegree = educationDetail.getDegree();
                    edt_education_degree.setText(educationDetail.getDegree());

                }
                if (educationDetail.getFieldOfStudy() != null && educationDetail.getFieldOfStudy().length() > 0
                        && !educationDetail.getFieldOfStudy().equals("null") && Utilities.isEmpty(educationDetail.getFieldOfStudy())) {
                    prevFiledofStudy = educationDetail.getFieldOfStudy();
                    edt_education_field_of_study.setText(educationDetail.getFieldOfStudy());

                }
                if (educationDetail.getStartDate() != null && educationDetail.getStartDate().length() > 0
                        && !educationDetail.getStartDate().equals("null") && Utilities.isEmpty(educationDetail.getStartDate())) {
                    prevStartDate = educationDetail.getStartDate();
                    edt_education_start_date.setText(educationDetail.getStartDate());

                }
                if (educationDetail.getEndDate() != null && educationDetail.getEndDate().length() > 0
                        && !educationDetail.getEndDate().equals("null") && Utilities.isEmpty(educationDetail.getEndDate())) {
                    if (educationDetail.getEndDate().equalsIgnoreCase("Present")) {
                        edt_education_end_date.setText("");
                    } else {
//                        try {
//                            SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
//                            String formattedDate = df.format(educationDetail.getEndDate() + "");
//                            prevEndDate = formattedDate;
//                            edt_education_end_date.setText(formattedDate);
//                        } catch (Exception e) {
//                            edt_education_end_date.setText(educationDetail.getEndDate());
//                            e.printStackTrace();
//                        }

                        try {
                            String formattedDate = educationDetail.getEndDate();
                            if (educationDetail.getEndDate().contains("-")) {
                                String endDate[] = educationDetail.getEndDate().split("-");
                                formattedDate = endDate[2] + "/" + endDate[1] + "/" + endDate[0];
                            }
                            prevEndDate = formattedDate;
                            edt_education_end_date.setText(formattedDate);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }

                if (educationDetail.getActivities() != null && educationDetail.getActivities().length() > 0
                        && !educationDetail.getActivities().equals("null") && Utilities.isEmpty(educationDetail.getActivities())) {
                    prevDesc = educationDetail.getActivities();
                    edt_education_activities.setText(educationDetail.getActivities());
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

        }

    }

    public void getSchoolList() {

        String token = "";
        if (qkPreferences != null) {
            token = qkPreferences.getApiToken() + "";
        }

        Map<String, String> params = new HashMap<>();
        params.put("Authorization", "Bearer " + token + "");
        params.put("Accept", "application/json");
        params.put("QK_ACCESS_KEY", getResources().getString(R.string.QK_ACCESS_KEY) + "");

        mService.getSchoolList(params).enqueue(new Callback<SchoolListPojo>() {
            @Override
            public void onResponse(Call<SchoolListPojo> call, Response<SchoolListPojo> response) {
                Log.e("SchooList:Success", "" + response.body());
                if (response != null && response.body() != null) {
                    SchoolListPojo schoolListPojo = response.body();
                    if (schoolListPojo != null) {

                        if (schoolListPojo.getStatus() == 200) {
                            RealmController.with(UpdateEducationActivity.this).clearSchoolList();
                            List<Datum> datumList = schoolListPojo.getData();
                            for (int i = 0; i < datumList.size(); i++) {
                                SchoolListTbl schoolList = new SchoolListTbl();
                                schoolList.setId(datumList.get(i).getId() + "");
                                schoolList.setName(datumList.get(i).getName() + "");
                                schoolList.setLogo(datumList.get(i).getLogo() + "");
                                Realm realm = Realm.getDefaultInstance();
                                realm.beginTransaction();
                                realm.copyToRealm(schoolList);
                                realm.commitTransaction();
                                FillSchoolList(false);
                            }

                        } else
                            Log.e("SchooList:Error", "Error");

                    } else
                        Log.e("SchooList:Error", "Error");

                } else
                    Log.e("SchooList:Error", "Error");
            }

            @Override
            public void onFailure(Call<SchoolListPojo> call, Throwable t) {
                Log.e("SchooList:Error", "" + t.getLocalizedMessage());
            }
        });

//        new VolleyApiRequest(context, false, VolleyApiRequest.REQUEST_BASEURL + "api/profile/education_profile_list",
//                new VolleyCallBack() {
//                    @Override
//                    public void onResponse(JSONObject response) {
//                        parseResponseCountry(response);
//                    }
//
//                    @Override
//                    public void onErrorResponse(VolleyError error) {
//                        VolleyApiRequest.showVolleyError(context, error);
//                    }
//                }).enqueRequest(null, Request.Method.POST);
    }

    public void parseResponseCountry(JSONObject response) {
        try {
            String status = response.getString("status");
            String message = response.getString("msg");
            if (status.equals("200")) {
                JSONArray jsnArr = response.getJSONArray("data");
                Log.i("Success JSONArray--> ", jsnArr.toString());

                for (int i = 0; i < jsnArr.length(); i++) {
                    JSONObject obj = jsnArr.getJSONObject(i);
                    SchoolListTbl schoolList = new SchoolListTbl();
                    schoolList.setId(obj.getString("id"));
                    schoolList.setName(obj.getString("name"));
                    schoolList.setLogo(obj.getString("logo"));
                    Realm realm = Realm.getDefaultInstance();
                    realm.beginTransaction();
                    realm.copyToRealm(schoolList);
                    realm.commitTransaction();
                    FillSchoolList(false);
                }
            } else if (status.equals("400")) {
                // Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void FillSchoolList(boolean isFillData) {
        try {
            RealmResults<SchoolListTbl> school = RealmController.with(UpdateEducationActivity.this).getSchoolList();
            if (school.size() > 0) {
                for (SchoolListTbl country: school) {
                    SchoolList model = new SchoolList();
                    model.setId(country.getId());
                    model.setName(country.getName());
                    model.setLogo(country.getLogo());
                    schoolLists.add(model);
                    if (schoolListAdapter != null)
                        schoolListAdapter.notifyDataSetChanged();
                }
            } else {
                getSchoolList();
            }
            if (isFillData)
                getSchoolList();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(final View view) {

        switch (view.getId()) {

            case R.id.tv_delete_education:
                new AlertDialog.Builder(this)
                        .setMessage(getResources().getString(R.string.delete_confirmation_message_education))
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int whichButton) {
                                if (!Utilities.isNetworkConnected(UpdateEducationActivity.this))
                                    Toast.makeText(UpdateEducationActivity.this, Constants.NO_INTERNET_CONNECTION, Toast.LENGTH_LONG).show();
                                else {
                                    Utilities.hideKeyboard(UpdateEducationActivity.this);
                                    DeleteEducation(view, educationDetail.getEducationId());
                                }
                            }
                        })
                        .setNegativeButton(android.R.string.no, null).show();
                break;

            case R.id.edt_education_start_date:
                String editextStartDate = edt_education_start_date.getText().toString().trim();
                if (editextStartDate != null && !editextStartDate.isEmpty() && !editextStartDate.equals("null")) {
                    String[] parts = editextStartDate.split("/");
                    mDay = Integer.parseInt(parts[0]);
                    mMonth = Integer.parseInt(parts[1]) - 1;
                    mYear = Integer.parseInt(parts[2]);
                }
                DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                int month = monthOfYear + 1;
                                String fm = "" + month;
                                String fd = "" + dayOfMonth;
                                if (month < 10) {
                                    fm = "0" + month;
                                }
                                if (dayOfMonth < 10) {
                                    fd = "0" + dayOfMonth;
                                }
                                edt_education_start_date.setText(fd + "/" + fm + "/" + year);
                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.getDatePicker().setMaxDate(c.getTimeInMillis());
                datePickerDialog.show();
                break;

            case R.id.edt_education_end_date:
                String editextEndDate = edt_education_end_date.getText().toString().trim();
                if (editextEndDate.equalsIgnoreCase("")) {
                    datePickerDialog = new DatePickerDialog(this,
                            new DatePickerDialog.OnDateSetListener() {

                                @Override
                                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                    int month = monthOfYear + 1;
                                    String fm = "" + month;
                                    String fd = "" + dayOfMonth;
                                    if (month < 10) {
                                        fm = "0" + month;
                                    }
                                    if (dayOfMonth < 10) {
                                        fd = "0" + dayOfMonth;
                                    }
                                    edt_education_end_date.setText(fd + "/" + fm + "/" + year);
                                }
                            }, mYear, mMonth, mDay);
                    datePickerDialog.getDatePicker().setMaxDate(c.getTimeInMillis());
                    datePickerDialog.show();
                } else {
                    if (!editextEndDate.isEmpty() && !editextEndDate.equals("null")) {
                        try {
                            String[] parts = editextEndDate.split("/");
                            mDay = Integer.parseInt(parts[0]);
                            mMonth = Integer.parseInt(parts[1]) - 1;
                            mYear = Integer.parseInt(parts[2]);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    datePickerDialog = new DatePickerDialog(this,
                            new DatePickerDialog.OnDateSetListener() {

                                @Override
                                public void onDateSet(DatePicker view, int year,
                                                      int monthOfYear, int dayOfMonth) {
                                    int month = monthOfYear + 1;
                                    String fm = "" + month;
                                    String fd = "" + dayOfMonth;
                                    if (month < 10) {
                                        fm = "0" + month;
                                    }
                                    if (dayOfMonth < 10) {
                                        fd = "0" + dayOfMonth;
                                    }
                                    edt_education_end_date.setText(fd + "/" + fm + "/" + year);
                                }
                            }, mYear, mMonth, mDay);
                    datePickerDialog.getDatePicker();//.setMaxDate(c.getTimeInMillis());
                    datePickerDialog.show();
                }
                break;
        }
    }

    public void functionAddEducation(final View view, String user_id, final String school_name, final String degree, final String field_of_study, final String start_date, final String end_date, final String activities) {

        JSONObject obj = new JSONObject();
        try {

            obj.put("user_id", user_id);
            obj.put("school_name", school_name);
            obj.put("degree", degree);
            obj.put("field_of_study", field_of_study);
            obj.put("start_date", start_date);
            obj.put("end_date", end_date);
            obj.put("activities", activities);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        new VolleyApiRequest(context, true, VolleyApiRequest.REQUEST_BASEURL + "api/user_education_profile_add",
                new VolleyCallBack() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            List<EducationDetail> educationDetailList = modelUserProfile.getData().getEducationDetail();
                            if (educationDetailList == null)
                                educationDetailList = new ArrayList<>();

                            EducationDetail educationDetail = new EducationDetail();
                            JSONObject object = response.getJSONObject("data");
                            educationDetail.setEducationId(object.getInt("education_id"));
                            educationDetail.setSchoolName(object.getString("school_name"));
                            educationDetail.setDegree(object.getString("degree"));
                            educationDetail.setFieldOfStudy(object.getString("field_of_study"));

//                            if (object.getString("start_date") != null && object.getString("start_date").length() > 0 && !object.getString("start_date").equals("null") && Utilities.isEmpty(object.getString("start_date"))) {
//
//                                String[] separated = object.getString("start_date").split("-");
//                                String year = separated[0]; // this will contain "Fruit"
//                                String month = separated[1];
//                                String day = separated[2];
//                                educationDetail.setStartDate(day + "/" + month + "/" + year);
//                            }
//
//                            if (object.getString("end_date") != null && object.getString("end_date").length() > 0 && !object.getString("end_date").equals("null") && Utilities.isEmpty(object.getString("end_date"))) {
//
//                                String[] separated = object.getString("end_date").split("-");
//                                String year = separated[0]; // this will contain "Fruit"
//                                String month = separated[1];
//                                String day = separated[2];
//                                educationDetail.setEndDate(day + "/" + month + "/" + year);
//                            }

                            educationDetail.setStartDate(object.getString("start_date"));
                            educationDetail.setEndDate(object.getString("end_date"));

                            educationDetail.setActivities(object.getString("activities"));

                            educationDetailList.add(educationDetail);

                            modelUserProfile.getData().setEducationDetail(educationDetailList);

                            qkPreferences.storeLoggedUser(modelUserProfile);

                            setResult(RESULT_OK);
                            UpdateEducationActivity.this.finish();

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyApiRequest.showVolleyError(context, error);
                    }
                }).enqueRequest(obj, Request.Method.POST);
    }

    //Education Update
    public void functionUpdateEducation(final View view, int education_id, final String school_name, final String degree, final String field_of_study, final String start_date, final String end_date, final String activities) {

        JSONObject obj = new JSONObject();
        try {

            obj.put("education_id", educationDetail.getEducationId());
            obj.put("school_name", school_name);
            obj.put("degree", degree);
            obj.put("field_of_study", field_of_study);
            obj.put("start_date", start_date);
            obj.put("end_date", end_date);
            obj.put("activities", activities);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        new VolleyApiRequest(context, true, VolleyApiRequest.REQUEST_BASEURL + "api/user_education_profile_edit",
                new VolleyCallBack() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {

                            String responseStatus = response.getString("status");
                            String msg = response.getString("msg");
                            if (responseStatus.equals("200")) {

                                if (modelUserProfile != null) {

                                    List<EducationDetail> educationDetailList = modelUserProfile.getData().getEducationDetail();
                                    int index = 0;
                                    for (EducationDetail educationDetails: educationDetailList) {
                                        if (educationDetails.getEducationId() != null &&
                                                educationDetails.getEducationId().equals(educationDetail.getEducationId())) {
                                            index = educationDetailList.indexOf(educationDetails);
                                            break;
                                        }
                                    }

                                    JSONObject object = response.getJSONObject("data");
                                    educationDetail.setEducationId(object.getInt("education_id"));
                                    educationDetail.setSchoolName(object.getString("school_name"));
                                    educationDetail.setDegree(object.getString("degree"));
                                    educationDetail.setFieldOfStudy(object.getString("field_of_study"));

//                                    if (object.getString("start_date") != null && object.getString("start_date").length() > 0 && !object.getString("start_date").equals("null")) {
//
//                                        String[] separated = object.getString("start_date").split("-");
//                                        String year = separated[0]; // this will contain "Fruit"
//                                        String month = separated[1];
//                                        String day = separated[2];
//                                        educationDetail.setStartDate(day + "/" + month + "/" + year);
//                                    }
//
                                    if (object.getString("end_date") != null && object.getString("end_date").length() > 0 && !object.getString("end_date").equals("null")) {

                                        String[] separated = object.getString("end_date").split("-");
                                        String year = separated[0]; // this will contain "Fruit"
                                        String month = separated[1];
                                        String day = separated[2];
                                        educationDetail.setEndDate(day + "/" + month + "/" + year);
                                    }

                                    educationDetail.setStartDate(object.getString("start_date"));
                                    //educationDetail.setEndDate(object.getString("end_date"));

                                    educationDetail.setActivities(object.getString("activities"));

                                    educationDetailList.set(index, educationDetail);

                                    modelUserProfile.getData().setEducationDetail(educationDetailList);

                                    qkPreferences.storeLoggedUser(modelUserProfile);

                                    setResult(RESULT_OK);
                                    UpdateEducationActivity.this.finish();

                                }

                            } else
                                Utilities.showToast(context, msg);


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyApiRequest.showVolleyError(context, error);
                    }
                }).enqueRequest(obj, Request.Method.POST);


    }

    //Education Delete
    public void DeleteEducation(final View view, int educationid) {

        new VolleyApiRequest(context, true, VolleyApiRequest.REQUEST_BASEURL + "api/delete_education_detail/" + educationDetail.getEducationId(),
                new VolleyCallBack() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {

                            String responseStatus = response.getString("status");
                            String msg = response.getString("msg");
                            if (responseStatus.equals("200")) {

                                List<EducationDetail> educationDetailList = modelUserProfile.getData().getEducationDetail();

                                for (int i = 0; i < educationDetailList.size(); i++) {
                                    if (educationDetailList.get(i).getEducationId().equals(educationDetail.getEducationId())) {
                                        educationDetailList.remove(i);
                                        break;
                                    }
                                }

                                modelUserProfile.getData().setEducationDetail(educationDetailList);

                                qkPreferences.storeLoggedUser(modelUserProfile);

                                setResult(RESULT_OK);
                                UpdateEducationActivity.this.finish();

                            } else
                                Utilities.showToast(context, msg);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyApiRequest.showVolleyError(context, error);
                    }
                }).enqueRequest(null, Request.Method.GET);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.custom_save_update, menu);
        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {

            case android.R.id.home:
                exitfromActivity();
                return true;

            case R.id.ic_save_update:
                Utilities.hideKeyboard(UpdateEducationActivity.this);
                if (autoComplete_edt_education_school.getText().toString().trim() == "" || autoComplete_edt_education_school.getText().toString().trim().equals("") || autoComplete_edt_education_school.getText().toString().trim().equals("null")) {
                    Utilities.showToast(context, "School name is required");
                } else if (edt_education_degree.getText().toString().trim() == "" || edt_education_degree.getText().toString().trim().equals("") || edt_education_degree.getText().toString().trim().equals("null")) {
                    Utilities.showToast(context, "Degree is required");
                } else if (edt_education_field_of_study.getText().toString().trim() == "" || edt_education_field_of_study.getText().toString().trim().equals("") || edt_education_field_of_study.getText().toString().trim().equals("null")) {
                    Utilities.showToast(context, "Field of study is required");
                }/*else if (edt_education_start_date.getText().toString().trim() == "" || edt_education_start_date.getText().toString().trim().equals("") || edt_education_start_date.getText().toString().trim().equals("null")) {
                    StaticVariable.functionShowMessage(view, "Start date is required");
                }else if (edt_education_end_date.getText().toString().trim() == "" || edt_education_end_date.getText().toString().trim().equals("") || edt_education_end_date.getText().toString().trim().equals("null")) {
                    StaticVariable.functionShowMessage(view, "End date is required");
                }*/ else {

                    String startdate = edt_education_start_date.getText().toString().trim();

                    String finalstartdate = null;
                    String enddate = edt_education_end_date.getText().toString().trim();
                    String finalenddate = null;


                    if ((startdate != null && !startdate.isEmpty() && !startdate.equals("null")) && (enddate.equals("Present") || (enddate != null && !enddate.isEmpty() && !enddate.equals("null")))) {

                        if (enddate.equals("Present")) {
                            enddate = edt_education_end_date.getTag().toString();
                        }

                        if (!Utilities.isDateAfter(startdate, enddate)) {
                            Utilities.showToast(context, "End date should be greater than start date");
                        } else {

                            if (startdate == "" || startdate.equals("") || startdate.equals("null")) {

                                finalstartdate = "";
                            } else {
                                String[] separated = startdate.split("/");
                                String day = separated[0]; // this will contain "Fruit"
                                String month = separated[1];
                                String year = separated[2];

                                finalstartdate = year + "-" + month + "-" + day;
                            }


                            if (enddate == "" || enddate.equals("") || enddate.equals("null")) {
                                finalenddate = "";
                            } else {
                                String[] separated1 = enddate.split("/");
                                String day1 = separated1[0];
                                String month1 = separated1[1];
                                String year1 = separated1[2];

                                finalenddate = year1 + "-" + month1 + "-" + day1;
                                //finalenddate = day1 + "-" + month1 + "-" + year1;
                            }

                            if (enddate.equals("")) {
                                finalenddate = "";
                            }


                            if (educationDetail != null) {
                                if (!Utilities.isNetworkConnected(this))
                                    Toast.makeText(this, Constants.NO_INTERNET_CONNECTION, Toast.LENGTH_LONG).show();
                                else
                                    //Update ModelLoggedData
                                    functionUpdateEducation(view, educationDetail.getEducationId(), autoComplete_edt_education_school.getText().toString().trim(), edt_education_degree.getText().toString().trim(), edt_education_field_of_study.getText().toString().trim(), finalstartdate, finalenddate, edt_education_activities.getText().toString());


                            } else {
                                if (!Utilities.isNetworkConnected(this))
                                    Toast.makeText(this, Constants.NO_INTERNET_CONNECTION, Toast.LENGTH_LONG).show();
                                else
                                    //Save ModelLoggedData
                                    functionAddEducation(view, userid, autoComplete_edt_education_school.getText().toString().trim(), edt_education_degree.getText().toString().trim(), edt_education_field_of_study.getText().toString().trim(), finalstartdate, finalenddate, edt_education_activities.getText().toString());

                            }
                        }
                    } else {


                        if (startdate == "" || startdate.equals("") || startdate.equals("null")) {

                            finalstartdate = "";
                        } else {
                            String[] separated = startdate.split("/");
                            String day = separated[0];
                            String month = separated[1];
                            String year = separated[2];

                            finalstartdate = year + "-" + month + "-" + day;
                        }

                        if (enddate.equals("Present") || enddate.equalsIgnoreCase("") || enddate.equals("null")) {
                            finalenddate = "";
                        } else {
                            String[] separated1 = enddate.split("/");
                            String day1 = separated1[0];
                            String month1 = separated1[1];
                            String year1 = separated1[2];

                            finalenddate = year1 + "-" + month1 + "-" + day1;
                        }
                        if (educationDetail != null) {

                            //Update ModelLoggedData
                            if (!Utilities.isNetworkConnected(this))
                                Toast.makeText(this, Constants.NO_INTERNET_CONNECTION, Toast.LENGTH_LONG).show();
                            else
                                functionUpdateEducation(view, educationDetail.getEducationId(), autoComplete_edt_education_school.getText().toString().trim(), edt_education_degree.getText().toString().trim(), edt_education_field_of_study.getText().toString().trim(), finalstartdate, finalenddate, edt_education_activities.getText().toString());

                        } else {

                            //Save ModelLoggedData
                            if (!Utilities.isNetworkConnected(this))
                                Toast.makeText(this, Constants.NO_INTERNET_CONNECTION, Toast.LENGTH_LONG).show();
                            else
                                functionAddEducation(view, userid, autoComplete_edt_education_school.getText().toString().trim(), edt_education_degree.getText().toString().trim(), edt_education_field_of_study.getText().toString().trim(), finalstartdate, finalenddate, edt_education_activities.getText().toString());

                        }
                    }
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        exitfromActivity();
    }

    public void exitfromActivity() {
        if (isFoundanyupdate()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(UpdateEducationActivity.this);
            builder.setTitle(getResources().getString(R.string.alert));
            builder.setMessage(getResources().getString(R.string.exitwithoutsave));
            builder.setPositiveButton(getResources().getString(R.string.exit), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    Utilities.hideKeyboard(UpdateEducationActivity.this);
                    setResult(RESULT_CANCELED);
                    UpdateEducationActivity.this.finish();
                }
            });
            builder.setNegativeButton(getResources().getString(R.string.close), null);
            builder.show();

        } else {

            Utilities.hideKeyboard(UpdateEducationActivity.this);
            setResult(RESULT_CANCELED);
            UpdateEducationActivity.this.finish();
        }
    }

    public boolean isFoundanyupdate() {

        String schoolname = autoComplete_edt_education_school.getText().toString();
        String degree = edt_education_degree.getText().toString();
        String filedofStudy = edt_education_field_of_study.getText().toString();
        String startdate = edt_education_start_date.getText().toString();
        String enddate = edt_education_end_date.getText().toString();
        String activities = edt_education_activities.getText().toString();

        if (prevschoolname != null && !prevschoolname.equals(schoolname)) {
            return true;

        } else if (prevDegree != null && !prevDegree.equals(degree)) {
            return true;

        } else if (prevFiledofStudy != null && !prevFiledofStudy.equals(filedofStudy)) {
            return true;

        } else if (prevStartDate != null && !prevStartDate.equals(startdate)) {
            return true;

        } else if (prevEndDate != null && !prevEndDate.equals(enddate)) {
            return true;

        } else if (prevDesc != null && !prevDesc.equals(activities)) {
            return true;
        }

        return false;
    }

    public class SchoolList {
        String id;
        String name;
        String logo;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getLogo() {
            return logo;
        }

        public void setLogo(String logo) {
            this.logo = logo;
        }
    }
}

