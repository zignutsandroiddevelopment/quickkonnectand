package com.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.models.logindata.ExperienceDetail;
import com.models.logindata.ModelLoggedUser;
import com.quickkonnect.R;
import com.utilities.Constants;
import com.utilities.QKPreferences;
import com.utilities.Utilities;
import com.volleyapirequest.VolleyApiRequest;
import com.volleyapirequest.VolleyCallBack;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class UpdateExperienceActivity extends AppCompatActivity implements View.OnClickListener {

    String userid;
    EditText edt_experience_title, edt_experience_company, edt_experience_location, edt_experience_startdate, edt_experience_enddate;
    Button btn_experience_save;
    TextView tv_delete;
    RelativeLayout view;
    private int mYear, mMonth, mDay;
    Calendar c;
    private int PLACE_PICKER_REQUEST = 103;
    private QKPreferences qkPreferences;
    private ModelLoggedUser modelUserProfile;
    private String prevtitle = "", prevCompany = "", prevLocation = "", prevStartYear = "", prevEndYear = "";
    private ExperienceDetail experienceDetail;
    private Context context;
    boolean isEdit = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_experience);

        Utilities.ChangeStatusBar(UpdateExperienceActivity.this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close_white_24dp);

        try {

            Bundle b = getIntent().getExtras();

            if (b.getString(Constants.KEY_UPDATE).equalsIgnoreCase(Constants.ADD_DATA)) {
                getSupportActionBar().setTitle(getResources().getString(R.string.addexp));

            } else if (b.getString(Constants.KEY_UPDATE).equalsIgnoreCase(Constants.EDIT_DATA)) {
                getSupportActionBar().setTitle(getResources().getString(R.string.updateexp));
                isEdit = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        context = UpdateExperienceActivity.this;
        qkPreferences = new QKPreferences(context);
        userid = "" + qkPreferences.getLoggedUser().getData().getId();

        modelUserProfile = qkPreferences.getLoggedUser();

        edt_experience_title = findViewById(R.id.edt_experience_title);
        edt_experience_company = findViewById(R.id.edt_experience_company);
        edt_experience_location = findViewById(R.id.edt_experience_location);
        edt_experience_startdate = findViewById(R.id.edt_experience_startdate);
        edt_experience_enddate = findViewById(R.id.edt_experience_enddate);
        btn_experience_save = findViewById(R.id.btn_experience_save);
        tv_delete = findViewById(R.id.tv_delete);
        view = findViewById(R.id.r1);

        btn_experience_save.setOnClickListener(this);
        tv_delete.setOnClickListener(this);

        // Get Current Date
        c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        edt_experience_startdate.setOnClickListener(this);
        edt_experience_enddate.setOnClickListener(this);

        Bundle extras = getIntent().getExtras();

        if (isEdit) {

            experienceDetail = extras.getParcelable("experience");

            if (experienceDetail != null && experienceDetail.getTitle() != null) {
                prevtitle = experienceDetail.getTitle();
                edt_experience_title.setText(experienceDetail.getTitle());
            }

            if (experienceDetail != null && experienceDetail.getCompanyName() != null) {
                prevCompany = experienceDetail.getCompanyName();
                edt_experience_company.setText(experienceDetail.getCompanyName());
            }

            if (experienceDetail != null && experienceDetail.getLocation() != null) {
                prevLocation = experienceDetail.getLocation();
                edt_experience_location.setText(experienceDetail.getLocation());
            }

            if (experienceDetail != null && experienceDetail.getStartDate() != null && !experienceDetail.getStartDate().equals("null")) {
                /*String[] parts = experienceDetail.getStartDate().split("/");
                mDay = Integer.parseInt(parts[0]);
                mMonth = Integer.parseInt(parts[1]);
                mYear = Integer.parseInt(parts[2]);*/
                prevStartYear = experienceDetail.getStartDate();
                edt_experience_startdate.setText(experienceDetail.getStartDate());
            }

            if (experienceDetail != null && experienceDetail.getEndDate() != null && !experienceDetail.getEndDate().equals("null")) {
              /*  String[] parts = experienceDetail.getEndDate().split("/");
                mDay = Integer.parseInt(parts[0]);
                mMonth = Integer.parseInt(parts[1]);
                mYear = Integer.parseInt(parts[2]);*/
                prevEndYear = experienceDetail.getEndDate();
                edt_experience_enddate.setText(experienceDetail.getEndDate());
            }

            btn_experience_save.setText(getResources().getString(R.string.update));

            tv_delete.setVisibility(View.VISIBLE);

            //getSupportActionBar().setTitle("Update Education");

        }
        SetFontStyle();

        edt_experience_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
                    startActivityForResult(builder.build(UpdateExperienceActivity.this), PLACE_PICKER_REQUEST);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == Activity.RESULT_OK) {
                Place place = PlacePicker.getPlace(context, data);
                String toastMsg = "";
                if (place.getAddress() != null && !place.getAddress().toString().equalsIgnoreCase("") && !place.getAddress().toString().equalsIgnoreCase("null"))
                    toastMsg = String.format("%s", place.getAddress());
                else
                    toastMsg = "Unknown";
                edt_experience_location.setText(toastMsg);
                double latitude = place.getLatLng().latitude;
                double longtitude = place.getLatLng().longitude;
                edt_experience_location.setTag("" + latitude + "," + longtitude);
            }
        }
    }

    public void SetFontStyle() {
        Typeface lato_regular = Typeface.createFromAsset(getAssets(), "fonts/latoregular.ttf");
        Typeface lato_bold = Typeface.createFromAsset(getAssets(), "fonts/latobold.ttf");
        edt_experience_title.setTypeface(lato_regular);
        edt_experience_company.setTypeface(lato_regular);
        edt_experience_location.setTypeface(lato_regular);
        edt_experience_startdate.setTypeface(lato_regular);
        edt_experience_enddate.setTypeface(lato_regular);
        edt_experience_enddate.setTypeface(lato_regular);
        tv_delete.setTypeface(lato_bold);

    }

    @Override
    public void onClick(final View view) {

        switch (view.getId()) {
            case R.id.tv_delete:

                new AlertDialog.Builder(this)
                        .setMessage(getResources().getString(R.string.delete_confirmation_message_experience))
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int whichButton) {
                                if (!Utilities.isNetworkConnected(UpdateExperienceActivity.this))
                                    Toast.makeText(UpdateExperienceActivity.this, Constants.NO_INTERNET_CONNECTION, Toast.LENGTH_LONG).show();
                                else {
                                    Utilities.hideKeyboard(UpdateExperienceActivity.this);
                                    DeleteExperience(view, experienceDetail.getExperienceId());
                                }
                            }
                        })
                        .setNegativeButton(android.R.string.no, null).show();

                break;

            case R.id.edt_experience_startdate:

                String editextStartDate = edt_experience_startdate.getText().toString().trim();

                if (editextStartDate != null && !editextStartDate.isEmpty() && !editextStartDate.equals("null")) {


                    try {
                        String[] parts = editextStartDate.split("/");
                        mDay = Integer.parseInt(parts[0]);
                        mMonth = Integer.parseInt(parts[1]) - 1;
                        mYear = Integer.parseInt(parts[2]);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }


                DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {


                                int month = monthOfYear + 1;
                                String fm = "" + month;
                                String fd = "" + dayOfMonth;
                                if (month < 10) {
                                    fm = "0" + month;
                                }
                                if (dayOfMonth < 10) {
                                    fd = "0" + dayOfMonth;
                                }

                                edt_experience_startdate.setText(fd + "/" + fm + "/" + year);


                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.getDatePicker().setMaxDate(c.getTimeInMillis());
                datePickerDialog.show();
                break;

            case R.id.edt_experience_enddate:


                String editextEndDate = edt_experience_enddate.getText().toString().trim();

                if (editextEndDate != null && !editextEndDate.isEmpty() && !editextEndDate.equals("null")) {


                    try {
                        String[] parts = editextEndDate.split("/");
                        mDay = Integer.parseInt(parts[0]);
                        mMonth = Integer.parseInt(parts[1]) - 1;
                        mYear = Integer.parseInt(parts[2]);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }


                datePickerDialog = new DatePickerDialog(this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {


                                int month = monthOfYear + 1;
                                String fm = "" + month;
                                String fd = "" + dayOfMonth;
                                if (month < 10) {
                                    fm = "0" + month;
                                }
                                if (dayOfMonth < 10) {
                                    fd = "0" + dayOfMonth;
                                }

                                edt_experience_enddate.setText(fd + "/" + fm + "/" + year);


                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.getDatePicker().setMaxDate(c.getTimeInMillis());
                datePickerDialog.show();
                break;
        }


    }

    // Add
    public void functionAddExperience(final View view, String user_id, final String title, final String company_name, final String start_date, final String end_date, final String location) {

        JSONObject obj = new JSONObject();
        try {
            obj.put("user_id", user_id);
            obj.put("title", title);
            obj.put("company_name", company_name);
            obj.put("start_date", start_date);
            obj.put("end_date", end_date);
            obj.put("location", location);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        new VolleyApiRequest(context, true, VolleyApiRequest.REQUEST_BASEURL + "api/user_experience_profile_add",
                new VolleyCallBack() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {

                            String responseStatus = response.getString("status");
                            String msg = response.getString("msg");
                            if (responseStatus.equals("200")) {

                                List<ExperienceDetail> experienceDetailList = modelUserProfile.getData().getExperienceDetail();
                                if (experienceDetailList == null)
                                    experienceDetailList = new ArrayList<>();

                                ExperienceDetail experienceDetail = new ExperienceDetail();
                                JSONObject object = response.getJSONObject("data");

                                experienceDetail.setExperienceId(object.getInt("experience_id"));
                                experienceDetail.setTitle(object.getString("title"));
                                experienceDetail.setCompanyName(object.getString("company_name"));
                                experienceDetail.setLocation(object.getString("location"));
                                experienceDetail.setStartDate(object.getString("start_date"));
                                experienceDetail.setEndDate(object.getString("end_date"));

                                experienceDetailList.add(experienceDetail);

                                modelUserProfile.getData().setExperienceDetail(experienceDetailList);

                                qkPreferences.storeLoggedUser(modelUserProfile);

                                setResult(RESULT_OK);
                                UpdateExperienceActivity.this.finish();


                            } else
                                Utilities.showToast(context, msg);


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyApiRequest.showVolleyError(context, error);
                    }
                }).enqueRequest(obj, Request.Method.POST);


    }

    // Update
    public void functionUpdateExperience(final View view, int experience_id, final String title, final String company_name, final String start_date, final String end_date, final String location) {

        JSONObject obj = new JSONObject();
        try {
            obj.put("experience_id", experience_id);
            obj.put("title", title);
            obj.put("company_name", company_name);
            obj.put("start_date", start_date);
            obj.put("end_date", end_date);
            obj.put("location", location);


        } catch (JSONException e) {
            e.printStackTrace();
        }

        new VolleyApiRequest(context, true, VolleyApiRequest.REQUEST_BASEURL + "api/user_experience_profile_edit",
                new VolleyCallBack() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {

                            String responseStatus = response.getString("status");
                            String msg = response.getString("msg");
                            if (responseStatus.equals("200")) {

                                if (modelUserProfile != null) {

                                    List<ExperienceDetail> experienceDetailList = modelUserProfile.getData().getExperienceDetail();
                                    int index = 0;
                                    for (ExperienceDetail experienceDetails: experienceDetailList) {
                                        if (experienceDetails.getExperienceId() != null &&
                                                experienceDetails.getExperienceId().equals(experienceDetail.getExperienceId())) {
                                            index = experienceDetailList.indexOf(experienceDetails);
                                            break;
                                        }
                                    }

                                    JSONObject object = response.getJSONObject("data");

                                    experienceDetail.setExperienceId(object.getInt("experience_id"));
                                    experienceDetail.setTitle(object.getString("title"));
                                    experienceDetail.setCompanyName(object.getString("company_name"));
                                    experienceDetail.setLocation(object.getString("location"));
                                    experienceDetail.setStartDate(object.getString("start_date"));
                                    experienceDetail.setEndDate(object.getString("end_date"));

                                    experienceDetailList.set(index, experienceDetail);

                                    modelUserProfile.getData().setExperienceDetail(experienceDetailList);

                                    qkPreferences.storeLoggedUser(modelUserProfile);

                                    setResult(RESULT_OK);
                                    UpdateExperienceActivity.this.finish();

                                }

                            } else
                                Utilities.showToast(context, msg);


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyApiRequest.showVolleyError(context, error);
                    }
                }).enqueRequest(obj, Request.Method.POST);


    }


    //Experience Delete
    public void DeleteExperience(final View view, int experienceid) {


        new VolleyApiRequest(context, true, VolleyApiRequest.REQUEST_BASEURL + "api/delete_experience_detail/" + experienceDetail.getExperienceId(),
                new VolleyCallBack() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("", "onResponse: " + response);
                        try {

                            String responseStatus = response.getString("status");
                            String msg = response.getString("msg");
                            if (responseStatus.equals("200")) {

                                List<ExperienceDetail> experienceDetailList = modelUserProfile.getData().getExperienceDetail();

                                for (int i = 0; i < experienceDetailList.size(); i++) {
                                    if (experienceDetailList.get(i).getExperienceId().equals(experienceDetail.getExperienceId())) {
                                        experienceDetailList.remove(i);
                                        break;
                                    }
                                }

                                modelUserProfile.getData().setExperienceDetail(experienceDetailList);

                                qkPreferences.storeLoggedUser(modelUserProfile);

                                setResult(RESULT_OK);
                                UpdateExperienceActivity.this.finish();

                            } else
                                Utilities.showToast(context, msg);


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyApiRequest.showVolleyError(context, error);
                    }
                }).enqueRequest(null, Request.Method.GET);


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
       /* MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.custom_save_update, menu);
        return true;*/

        getMenuInflater().inflate(R.menu.custom_save_update, menu);
        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        int id = item.getItemId();

        switch (item.getItemId()) {

            case android.R.id.home:
                exitfromActivity();
                return true;

            case R.id.ic_save_update:

                Utilities.hideKeyboard(UpdateExperienceActivity.this);
                if (edt_experience_title.getText().toString().trim() == "" || edt_experience_title.getText().toString().trim().equals("") || edt_experience_title.getText().toString().trim().equals("null")) {
                    Utilities.showToast(context, "Title is required");
                } else if (edt_experience_company.getText().toString().trim() == "" || edt_experience_company.getText().toString().trim().equals("") || edt_experience_company.getText().toString().trim().equals("null")) {
                    Utilities.showToast(context, "Company is required");
                }

//                else if (edt_experience_location.getText().toString().trim() == "" || edt_experience_location.getText().toString().trim().equals("") || edt_experience_location.getText().toString().trim().equals("null")) {
//                    Utilities.showToast(context, "Location is required");
//                }

                else {
                    String startdate = edt_experience_startdate.getText().toString().trim();
                    String finalstartdate = null;
                    String enddate = edt_experience_enddate.getText().toString().trim();
                    String finalenddate = null;
                    Utilities.hideKeyboard(UpdateExperienceActivity.this);

                    if ((startdate != null && !startdate.isEmpty() && !startdate.equals("null")) && (enddate != null && !enddate.isEmpty() && !enddate.equals("null"))) {

                        if (!isDateAfter(startdate, enddate)) {
                            Utilities.showToast(context, "End date should be greater than start date");
                        } else {

                            if (startdate == "" || startdate.equals("") || startdate.equals("null")) {
                                finalstartdate = "";
                            } else {
                                String[] separated = startdate.split("/");
                                String day = separated[0]; // this will contain "Fruit"
                                String month = separated[1];
                                String year = separated[2];

                                finalstartdate = year + "-" + month + "-" + day;
                            }

                            if (enddate == "" || enddate.equals("") || enddate.equals("null")) {

                                finalenddate = "";
                            } else {
                                String[] separated1 = enddate.split("/");
                                String day1 = separated1[0]; // this will contain "Fruit"
                                String month1 = separated1[1];
                                String year1 = separated1[2];

                                finalenddate = year1 + "-" + month1 + "-" + day1;
                            }

                            if (experienceDetail != null && experienceDetail.getExperienceId() != null) {
                                if (!Utilities.isNetworkConnected(this))
                                    Toast.makeText(this, Constants.NO_INTERNET_CONNECTION, Toast.LENGTH_LONG).show();
                                else
                                    functionUpdateExperience(view, experienceDetail.getExperienceId(), edt_experience_title.getText().toString().trim(), edt_experience_company.getText().toString().trim(), finalstartdate, finalenddate, edt_experience_location.getText().toString().trim());

                            } else {
                                if (!Utilities.isNetworkConnected(this))
                                    Toast.makeText(this, Constants.NO_INTERNET_CONNECTION, Toast.LENGTH_LONG).show();
                                else
                                    functionAddExperience(view, userid, edt_experience_title.getText().toString().trim(), edt_experience_company.getText().toString().trim(), finalstartdate, finalenddate, edt_experience_location.getText().toString().trim());

                            }
                        }
                    } else {

                        if (startdate == "" || startdate.equals("") || startdate.equals("null")) {

                            finalstartdate = "";
                        } else {
                            String[] separated = startdate.split("/");
                            String day = separated[0]; // this will contain "Fruit"
                            String month = separated[1];
                            String year = separated[2];

                            finalstartdate = year + "-" + month + "-" + day;
                        }


                        if (enddate == "" || enddate.equals("") || enddate.equals("null")) {

                            finalenddate = "";
                        } else {
                            String[] separated1 = enddate.split("/");
                            String day1 = separated1[0]; // this will contain "Fruit"
                            String month1 = separated1[1];
                            String year1 = separated1[2];

                            finalenddate = year1 + "-" + month1 + "-" + day1;
                        }
                        if (experienceDetail != null && experienceDetail.getExperienceId() != null) {
                            if (!Utilities.isNetworkConnected(this))
                                Toast.makeText(this, Constants.NO_INTERNET_CONNECTION, Toast.LENGTH_LONG).show();
                            else
                                functionUpdateExperience(view, experienceDetail.getExperienceId(), edt_experience_title.getText().toString().trim(), edt_experience_company.getText().toString().trim(), finalstartdate, finalenddate, edt_experience_location.getText().toString().trim());

                        } else {
                            if (!Utilities.isNetworkConnected(this))
                                Toast.makeText(this, Constants.NO_INTERNET_CONNECTION, Toast.LENGTH_LONG).show();
                            else
                                functionAddExperience(view, userid, edt_experience_title.getText().toString().trim(), edt_experience_company.getText().toString().trim(), finalstartdate, finalenddate, edt_experience_location.getText().toString().trim());

                        }
                    }


                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        exitfromActivity();
    }

    public void exitfromActivity() {
        if (isFoundanyupdate()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(UpdateExperienceActivity.this);
            builder.setTitle(getResources().getString(R.string.alert));
            builder.setMessage(getResources().getString(R.string.exitwithoutsave));
            builder.setPositiveButton(getResources().getString(R.string.exit), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    Utilities.hideKeyboard(UpdateExperienceActivity.this);
                    setResult(RESULT_CANCELED);
                    UpdateExperienceActivity.this.finish();
                }
            });
            builder.setNegativeButton(getResources().getString(R.string.close), null);
            builder.show();

        } else {

            Utilities.hideKeyboard(UpdateExperienceActivity.this);
            setResult(RESULT_CANCELED);
            UpdateExperienceActivity.this.finish();
        }
    }

    public boolean isFoundanyupdate() {

        String title = edt_experience_title.getText().toString();
        String company = edt_experience_company.getText().toString();
        String location = edt_experience_location.getText().toString();
        String startyear = edt_experience_startdate.getText().toString();
        String endyear = edt_experience_enddate.getText().toString();

        if (prevtitle != null && !prevtitle.equals(title)) {
            return true;

        } else if (prevCompany != null && !prevCompany.equals(company)) {
            return true;

        } else if (prevLocation != null && !prevLocation.equals(location)) {
            return true;

        } else if (prevStartYear != null && !prevStartYear.equals(startyear)) {
            return true;

        } else if (prevEndYear != null && !prevEndYear.equals(endyear)) {
            return true;
        }

        return false;
    }


    public static boolean isDateAfter(String startDate, String endDate) {
        try {
            String myFormatString = "dd/MM/yyyy"; // for example
            SimpleDateFormat df = new SimpleDateFormat(myFormatString);
            Date date1 = df.parse(endDate);
            Date startingDate = df.parse(startDate);

            return date1.after(startingDate);
        } catch (Exception e) {

            return false;
        }
    }


}