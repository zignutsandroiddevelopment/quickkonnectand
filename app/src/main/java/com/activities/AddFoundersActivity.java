package com.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputFilter;
import android.text.Spanned;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;

import com.models.createprofile.Founder;
import com.quickkonnect.R;
import com.utilities.Utilities;

/**
 * Created by ZTLAB-10 on 21-02-2018.
 */

public class AddFoundersActivity extends AppCompatActivity {

    Context context;
    EditText edt_firstname, edt_lastname, edt_designation, edt_email;
    Founder founder;
    int pos = -1;
    private String blockCharacterSet = "~#^|$%*!<>/%";

    private InputFilter filter = new InputFilter() {
        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
            if (source != null && blockCharacterSet.contains(("" + source))) {
                return "";
            }
            return null;
        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Utilities.changeStatusbar(AddFoundersActivity.this, getWindow());
        setContentView(R.layout.activity_createfounder);

        Utilities.ChangeStatusBar(AddFoundersActivity.this);

        context = AddFoundersActivity.this;

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        //toolbar.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        toolbar.setBackgroundResource(R.drawable.toolbar_gradient);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close_white_24dp);

        getSupportActionBar().setTitle(getResources().getString(R.string.addfounder));

        initWidget();
    }

    public void initWidget() {

        edt_firstname = findViewById(R.id.edt_firstname);
        edt_lastname = findViewById(R.id.edt_lastname);
        edt_designation = findViewById(R.id.edt_designation);
        edt_email = findViewById(R.id.edt_email);

        edt_firstname.setFilters(new InputFilter[]{filter});
        edt_lastname.setFilters(new InputFilter[]{filter});
        edt_designation.setFilters(new InputFilter[]{filter});

        if (getIntent().getExtras() != null && getIntent().getExtras().getParcelable("data") != null) {
            founder = (Founder) getIntent().getExtras().getParcelable("data");
            pos = getIntent().getExtras().getInt("pos");
            if (founder != null) {
                if (founder.getFirstname() != null && Utilities.isEmpty(founder.getFirstname()))
                    edt_firstname.setText(founder.getFirstname());

                if (founder.getLastname() != null && Utilities.isEmpty(founder.getLastname()))
                    edt_lastname.setText(founder.getLastname());

                if (founder.getDesignation() != null && Utilities.isEmpty(founder.getDesignation()))
                    edt_designation.setText(founder.getDesignation());

                if (founder.getEmail() != null && Utilities.isEmpty(founder.getEmail()))
                    edt_email.setText(founder.getEmail());
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.custom_save_update, menu);
        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                AddFoundersActivity.this.finish();
                break;

            case R.id.ic_save_update:
                try {
                    Utilities.hideKeyboard(AddFoundersActivity.this);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                String firstname = edt_firstname.getText().toString();
                String lastname = edt_lastname.getText().toString();
                String designation = edt_designation.getText().toString();
                String email = edt_email.getText().toString();

                if (!Utilities.isEmpty(firstname))
                    Utilities.showToast(context, getResources().getString(R.string.firstnamenotempty));

                else if (!Utilities.isEmpty(lastname))
                    Utilities.showToast(context, getResources().getString(R.string.lastnamenotempty));

                else {

                    if (!Utilities.isEmpty(email)) {

                        Founder founder = new Founder();
                        founder.setFirstname(firstname);
                        founder.setLastname(lastname);
                        founder.setDesignation(designation);
                        founder.setEmail(email);
                        Intent intent = new Intent();
                        intent.putExtra("index", pos);
                        intent.putExtra("data", founder);
                        setResult(RESULT_OK, intent);
                        AddFoundersActivity.this.finish();

                    } else {

                        if (Utilities.isValidEmail(email)) {

                            Founder founder = new Founder();
                            founder.setFirstname(firstname);
                            founder.setLastname(lastname);
                            founder.setDesignation(designation);
                            founder.setEmail(email);
                            Intent intent = new Intent();
                            intent.putExtra("index", pos);
                            intent.putExtra("data", founder);
                            setResult(RESULT_OK, intent);
                            AddFoundersActivity.this.finish();

                        } else
                            Utilities.showToast(context, getResources().getString(R.string.invalidemailenter));
                    }
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
