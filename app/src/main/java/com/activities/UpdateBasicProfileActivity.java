package com.activities;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.adapter.CountryAdapter;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.interfaces.OnCountrySelect;
import com.models.ModelCountry;
import com.models.ModelStates;
import com.models.logindata.BasicDetail;
import com.models.logindata.ModelLoggedUser;
import com.quickkonnect.R;
import com.realmtable.Country;
import com.realmtable.RealmController;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;
import com.utilities.Constants;
import com.utilities.QKPreferences;
import com.utilities.Utilities;
import com.volleyapirequest.VolleyApiRequest;
import com.volleyapirequest.VolleyCallBack;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;

public class UpdateBasicProfileActivity extends AppCompatActivity implements View.OnClickListener {

    private String userid;
    private EditText edt_basic_profile_birthdate, edt_basic_profile_hometown;
    private String country_id, state_id, city_id;//, gender;
    private ConstraintLayout view;
    private Calendar c;
    private int mYear, mMonth, mDay;
    private SearchableSpinner spin_gender;
    private Context context;
    private CheckBox chk_ishideyear;
    private QKPreferences qkPreferences;
    TextView tvGender;
    private ModelLoggedUser modelUserProfile;
    private CountryAdapter countryAdapter, stateAdapter, cityAdapter;
    private AutoCompleteTextView tv_country_title, tv_state_title, tv_city_title;
    List<ModelCountry> countryList, stateList, citytryList;
    List<ModelStates> modelStates;
    List<ModelStates> modelCities;
    boolean isOnCreate = false;
    private String prevHometown = "", prevBirthdate = "", prevGender = "", prevCountry = "", prevState = "", prevCity = "";
//    private boolean previsHidemy = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_update_basic_profile);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        try {

            Bundle b = getIntent().getExtras();

            if (b.getString(Constants.KEY_UPDATE).equalsIgnoreCase(Constants.ADD_DATA))
                getSupportActionBar().setTitle(getResources().getString(R.string.addbasicprofile));

            else if (b.getString(Constants.KEY_UPDATE).equalsIgnoreCase(Constants.EDIT_DATA))
                getSupportActionBar().setTitle(getResources().getString(R.string.updatebasicprofile));

        } catch (Exception e) {
            e.printStackTrace();
        }

        Utilities.ChangeStatusBar(UpdateBasicProfileActivity.this);

        context = UpdateBasicProfileActivity.this;

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close_white_24dp);

        qkPreferences = new QKPreferences(context);

        userid = "" + qkPreferences.getLoggedUser().getData().getId();

        view = findViewById(R.id.constraint);
        edt_basic_profile_birthdate = findViewById(R.id.edt_basic_profile_birthdate);
        edt_basic_profile_hometown = findViewById(R.id.edt_basic_profile_hometown);

        spin_gender = findViewById(R.id.spin_gender);
        tvGender = findViewById(R.id.tvGender);
        chk_ishideyear = findViewById(R.id.chk_ishideyear);

        tv_country_title = findViewById(R.id.tv_country_title);
        tv_state_title = findViewById(R.id.tv_state_title);
        tv_city_title = findViewById(R.id.tv_city_title);

        edt_basic_profile_birthdate.setOnClickListener(this);

        // Get Current Date
        c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);
        c.add(Calendar.YEAR, -13);

        modelUserProfile = qkPreferences.getLoggedUser();
        BasicDetail basicDetail = modelUserProfile.getData().getBasicDetail();
        if (modelUserProfile != null && basicDetail != null) {

            if (basicDetail.getBirthdate() != null
                    && !basicDetail.getBirthdate().equals("null")) {
                prevBirthdate = basicDetail.getBirthdate();
                edt_basic_profile_birthdate.setText(basicDetail.getBirthdate());
            }

            if (basicDetail.getHometown() != null && !basicDetail.getHometown().equals("null")) {
                prevHometown = basicDetail.getHometown();
                edt_basic_profile_hometown.setText(basicDetail.getHometown());
            }

            isOnCreate = true;

            try {
                if (basicDetail.getIshide_year() != null && !basicDetail.getIshide_year().isEmpty()) {
                    chk_ishideyear.setChecked(Boolean.parseBoolean(basicDetail.getIshide_year()));
                } else {
                    chk_ishideyear.setChecked(qkPreferences.getValuesBoolean(QKPreferences.IS_SHOWBIRTHYEAR));
                }
//                previsHidemy = chk_ishideyear.isChecked();
            } catch (Exception e) {
                e.printStackTrace();
            }

            String items[] = {"Male", "Female", "Other"};
            ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, items);

            spin_gender.setTitle("Select Your Gender");
            spin_gender.setAdapter(spinnerArrayAdapter);
            spin_gender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    if (!isOnCreate) {
                        switch (i) {
                            case 0:
                                tvGender.setText("Male");
                                break;
                            case 1:
                                tvGender.setText("Female");
                                break;
                            case 2:
                                tvGender.setText("Other");
                                break;
                        }
                    } else isOnCreate = false;

                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {
                }
            });


            if (basicDetail.getGender() != null && !basicDetail.getGender().equals("null")) {
                switch (basicDetail.getGender()) {
                    case "M":
                        prevGender = "Male";
                        tvGender.setText("Male");
                        break;
                    case "F":
                        prevGender = "Female";
                        tvGender.setText("Female");
                        break;
                    case "O":
                        prevGender = "Other";
                        tvGender.setText("Other");
                        break;
                    case "U":
                        prevGender = "Unknown";
                        tvGender.setText("Unknown");
                        break;
                    default:
                        tvGender.setText("");
                        break;
                }
            }

            country_id = "" + basicDetail.getCountryId();
            prevCountry = country_id;

            state_id = "" + basicDetail.getStateId();
            prevState = state_id;

            city_id = "" + basicDetail.getCityId();
            prevCity = city_id;


            if (basicDetail.getCityId() != null && Utilities.isEmpty("" + basicDetail.getCountryId()) && !basicDetail.getCountryId().toString().equalsIgnoreCase("0")) {
                tv_country_title.setText(basicDetail.getCountry());
                tv_country_title.setTag("" + basicDetail.getCountryId());
                if (!Utilities.isNetworkConnected(UpdateBasicProfileActivity.this))
                    Toast.makeText(UpdateBasicProfileActivity.this, Constants.NO_INTERNET_CONNECTION, Toast.LENGTH_LONG).show();
                else
                    functionGetState("" + basicDetail.getCountryId(), false);
            }

            if (basicDetail.getStateId() != null && Utilities.isEmpty("" + basicDetail.getStateId()) && !basicDetail.getStateId().toString().equalsIgnoreCase("0")) {
                tv_state_title.setText(basicDetail.getState());
                tv_state_title.setTag("" + basicDetail.getStateId());
                if (!Utilities.isNetworkConnected(UpdateBasicProfileActivity.this))
                    Toast.makeText(UpdateBasicProfileActivity.this, Constants.NO_INTERNET_CONNECTION, Toast.LENGTH_LONG).show();
                else
                    functionGetCity("" + basicDetail.getCountryId(), "" + basicDetail.getStateId(), false);
            }

            if (basicDetail.getCityId() != null && Utilities.isEmpty("" + basicDetail.getCityId()) && !basicDetail.getCityId().toString().equalsIgnoreCase("0")) {
                tv_city_title.setText(basicDetail.getCity());
                tv_city_title.setTag("" + basicDetail.getCityId());
            }
        }

        showSelectionPopup(0);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.custom_save_update, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.ic_save_update:

                Utilities.hideKeyboard(UpdateBasicProfileActivity.this);

                if (!Utilities.isNetworkConnected(UpdateBasicProfileActivity.this))
                    Toast.makeText(UpdateBasicProfileActivity.this, Constants.NO_INTERNET_CONNECTION, Toast.LENGTH_LONG).show();
                else {

                    if (edt_basic_profile_birthdate.getText().toString().equalsIgnoreCase("") || edt_basic_profile_birthdate.getText().toString().equalsIgnoreCase("null")) {
                        Toast.makeText(this, "Please Select Birth date", Toast.LENGTH_LONG).show();
                    } else if (tvGender.getText().toString().equalsIgnoreCase("") || tvGender.getText().toString().equalsIgnoreCase("null")) {
                        Toast.makeText(this, "Please Select Gender", Toast.LENGTH_LONG).show();
                    } else if (tv_country_title.getTag() != null && (tv_state_title.getTag() == null || tv_state_title.getTag() == "")) {
                        Toast.makeText(this, "Please Select State", Toast.LENGTH_LONG).show();
                    } else if (tv_country_title.getTag() != null && tv_state_title.getTag() != null && (tv_city_title.getTag() == "" || tv_city_title.getTag() == null)) {
                        Toast.makeText(this, "Please Select City", Toast.LENGTH_LONG).show();
                    } else {

                        String dobConvert = edt_basic_profile_birthdate.getText().toString().trim();
                        if (dobConvert == "" || dobConvert.equals("") || dobConvert.equals("null")) {
                            dobConvert = "";
                        } else {
                            String[] separated = dobConvert.split("/");
                            String day = separated[0]; // this will contain "Fruit"
                            String month = separated[1];
                            String year = separated[2];
                            dobConvert = year + "-" + month + "-" + day;
                        }
                        if (!Utilities.isNetworkConnected(this))
                            Toast.makeText(this, Constants.NO_INTERNET_CONNECTION, Toast.LENGTH_LONG).show();
                        else {
                            if (tv_country_title.getTag() != null)
                                country_id = tv_country_title.getTag().toString();
                            else
                                country_id = "";

                            if (tv_state_title.getTag() != null)
                                state_id = tv_state_title.getTag().toString();
                            else
                                state_id = "";

                            if (tv_city_title.getTag() != null)
                                city_id = tv_city_title.getTag().toString();
                            else
                                city_id = "";

                            Utilities.hideKeyboard(UpdateBasicProfileActivity.this);
                            functionAddBasicProfile(view, userid, dobConvert, edt_basic_profile_hometown.getText().toString().trim(), country_id, state_id, city_id);
                        }
                    }
                }
                return true;

            case android.R.id.home:
                exitfromActivity();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        exitfromActivity();
    }

    public void exitfromActivity() {
        if (isFoundanyupdate()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(UpdateBasicProfileActivity.this);
            builder.setTitle(getResources().getString(R.string.alert));
            builder.setMessage(getResources().getString(R.string.exitwithoutsave));
            builder.setPositiveButton(getResources().getString(R.string.exit), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    Utilities.hideKeyboard(UpdateBasicProfileActivity.this);
                    setResult(RESULT_CANCELED);
                    UpdateBasicProfileActivity.this.finish();
                }
            });
            builder.setNegativeButton(getResources().getString(R.string.close), null);
            builder.show();

        } else {

            Utilities.hideKeyboard(UpdateBasicProfileActivity.this);
            setResult(RESULT_CANCELED);
            UpdateBasicProfileActivity.this.finish();
        }
    }

    public boolean isFoundanyupdate() {

        String updatehomvetown = edt_basic_profile_hometown.getText().toString();
        String updatebirthdate = edt_basic_profile_birthdate.getText().toString();
        String gender = tvGender.getText().toString();

        if (tv_country_title.getTag() != null)
            country_id = tv_country_title.getTag().toString();
        else
            country_id = "";

        if (tv_state_title.getTag() != null)
            state_id = tv_state_title.getTag().toString();
        else
            state_id = "";

        if (tv_city_title.getTag() != null)
            city_id = tv_city_title.getTag().toString();
        else
            city_id = "";

        if (prevHometown != null && !prevHometown.equals(updatehomvetown)) {
            return true;

        } else if (prevBirthdate != null && !prevBirthdate.equals(updatebirthdate)) {
            return true;

        } else if (prevGender != null && !prevGender.equals(gender)) {
            return true;

        } else if (prevCountry != null && !prevCountry.equals(country_id)) {
            return true;

        } else if (prevState != null && !prevState.equals(state_id)) {
            return true;

        } else if (prevCity != null && !prevCity.equals(city_id)) {
            return true;
        }

        return false;
    }

    // Save/Update Basic Profile
    public void functionAddBasicProfile(final View view, String user_id, final String dob, final String hometown, final String country, final String state, final String city) {

        JSONObject obj = new JSONObject();
        String gender1 = "";
        switch (spin_gender.getSelectedItem().toString()) {
            case "Male":
                gender1 = "M";
                break;
            case "Female":
                gender1 = "F";
                break;
            case "Other":
                gender1 = "O";
                break;
            case "Unknown":
                gender1 = "U";
                break;
        }
        final boolean isHideyear = chk_ishideyear.isChecked();
        try {
            obj.put("user_id", user_id);
            obj.put("dob", dob);
            obj.put("gender", gender1);
            obj.put("hometown", hometown);
            obj.put("country", country);
            obj.put("state", state);
            obj.put("city", city);
            if (isHideyear)
                obj.put("ishide_year", 1);
            else
                obj.put("ishide_year", 0);

        } catch (Exception e) {
            e.printStackTrace();
        }
        Log.d("TAG", "functionAddBasicProfile: " + obj);

        new VolleyApiRequest(context, true, VolleyApiRequest.REQUEST_BASEURL + "api/user_basic_profile",
                new VolleyCallBack() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String status = response.getString("status");
                            String message = response.getString("msg");
                            if (status.equals("200")) {

                                qkPreferences.setValues(QKPreferences.IS_SHOWBIRTHYEAR, isHideyear);

                                JSONObject jsonObject = response.getJSONObject("data");

                                BasicDetail basicDetail = modelUserProfile.getData().getBasicDetail();
                                basicDetail.setHometown(jsonObject.getString("hometown"));
                                basicDetail.setGender(jsonObject.optString("gender"));
                                basicDetail.setCountry(jsonObject.optString("country"));
                                basicDetail.setBirthdate(jsonObject.optString("birthdate"));
                                basicDetail.setIshide_year(String.valueOf(chk_ishideyear.isChecked()));

                                if (jsonObject.optString("country_id") != null && !jsonObject.optString("country_id").equalsIgnoreCase("") && !jsonObject.optString("country_id").equalsIgnoreCase("null"))
                                    basicDetail.setCountryId(Integer.parseInt(jsonObject.optString("country_id")));
                                else
                                    basicDetail.setCountryId(0);

                                basicDetail.setState(jsonObject.optString("state"));
                                if (jsonObject.optString("state_id") != null && !jsonObject.optString("state_id").equalsIgnoreCase("") && !jsonObject.optString("state_id").equalsIgnoreCase("null"))
                                    basicDetail.setStateId(Integer.parseInt(jsonObject.optString("state_id")));
                                else
                                    basicDetail.setStateId(0);

                                basicDetail.setCity(jsonObject.optString("city"));
                                if (jsonObject.optString("city_id") != null && !jsonObject.optString("city_id").equalsIgnoreCase("") && !jsonObject.optString("city_id").equalsIgnoreCase("null"))
                                    basicDetail.setCityId(Integer.parseInt(jsonObject.optString("city_id")));
                                else
                                    basicDetail.setCityId(0);

                                qkPreferences.storeLoggedUser(modelUserProfile);

                                setResult(RESULT_OK);
                                UpdateBasicProfileActivity.this.finish();

                            } else
                                Utilities.showToast(context, message);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyApiRequest.showVolleyError(context, error);
                    }
                }).enqueRequest(obj, Request.Method.POST);
    }

    public void functionGetState(String country_code, boolean isLoading) {
        JSONObject obj = new JSONObject();
        try {
            obj.put("country_code", country_code);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        new VolleyApiRequest(context, isLoading, VolleyApiRequest.REQUEST_BASEURL + "api/get_states",
                new VolleyCallBack() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String status = response.getString("status");
                            String message = response.getString("msg");
                            if (status.equals("200")) {
                                JSONArray jsnArr = response.getJSONArray("data");
                                if (jsnArr != null && jsnArr.length() > 0) {

                                    Type type = new TypeToken<List<ModelStates>>() {
                                    }.getType();
                                    modelStates = new Gson().fromJson(jsnArr.toString(), type);
                                }
                            }
                            Utilities.hideKeyboard(UpdateBasicProfileActivity.this);

                            showSelectionPopup(1);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyApiRequest.showVolleyError(context, error);
                    }
                }).enqueRequest(obj, Request.Method.POST);
    }

    // Get City
    public void functionGetCity(String country_id, String state_id, boolean isLoading) {
        JSONObject obj = new JSONObject();
        try {
            obj.put("country_id", country_id);
            obj.put("state_id", state_id);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        new VolleyApiRequest(context, true, VolleyApiRequest.REQUEST_BASEURL + "api/get_cities",
                new VolleyCallBack() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {

                            String status = response.getString("status");
                            String message = response.getString("msg");
                            if (status.equals("200")) {
                                JSONArray jsnArr = response.getJSONArray("data");
                                if (jsnArr != null && jsnArr.length() > 0) {

                                    Type type = new TypeToken<List<ModelStates>>() {
                                    }.getType();
                                    modelCities = new Gson().fromJson(jsnArr.toString(), type);
                                }

                                showSelectionPopup(2);
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyApiRequest.showVolleyError(context, error);
                    }
                }).enqueRequest(obj, Request.Method.POST);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.edt_basic_profile_birthdate:
                String editextDate = edt_basic_profile_birthdate.getText().toString().trim();
                if (editextDate != null && !editextDate.isEmpty() && !editextDate.equals("null")) {
                    String[] parts = editextDate.split("/");
                    mDay = Integer.parseInt(parts[0]);
                    mMonth = Integer.parseInt(parts[1]) - 1;
                    mYear = Integer.parseInt(parts[2]);
                }
                DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                int month = monthOfYear + 1;
                                String fm = "" + month;
                                String fd = "" + dayOfMonth;
                                if (month < 10) {
                                    fm = "0" + month;
                                }
                                if (dayOfMonth < 10) {
                                    fd = "0" + dayOfMonth;
                                }

                                edt_basic_profile_birthdate.setText(fd + "/" + fm + "/" + year);


                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.getDatePicker().setMaxDate(c.getTimeInMillis());
                //datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
                datePickerDialog.show();
                break;
        }
    }

    public void showSelectionPopup(final int index) {
        try {
            if (index == 0) {
                countryList = new ArrayList<>();
                try {
                    RealmResults<Country> countries = RealmController.with(UpdateBasicProfileActivity.this).getCountry();
                    if (countries.size() > 0) {
                        for (Country country: countries) {
                            ModelCountry modelCountry = new ModelCountry();
                            modelCountry.setCountry_id(country.getCountry_id());
                            modelCountry.setCountry_name(country.getCountry_name());
                            countryList.add(modelCountry);
                        }
                    } else {
                        getCountries();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                countryAdapter =
                        new CountryAdapter(UpdateBasicProfileActivity.this, countryList, new OnCountrySelect() {
                            @Override
                            public void getCountry(ModelCountry modelCountry) {

                                tv_country_title.setText("" + modelCountry.getCountry_name());
                                tv_country_title.setTag("" + modelCountry.getCountry_id());
                                tv_country_title.setSelection(tv_country_title.getText().length());
                                tv_country_title.dismissDropDown();

                                tv_state_title.setHint("Current State");
                                tv_state_title.setText("");
                                tv_state_title.setTag("");

                                tv_city_title.setHint("Current City");
                                tv_city_title.setText("");
                                tv_city_title.setTag("");

                                modelStates = new ArrayList<>();
                                modelCities = new ArrayList<>();

                                Utilities.hideKeyboard(UpdateBasicProfileActivity.this);

                                functionGetState(modelCountry.getCountry_id(), false);

                            }
                        });
                tv_country_title.setAdapter(countryAdapter);
                tv_country_title.setThreshold(1);

            } else if (index == 1) {
                stateList = new ArrayList<>();
                if (modelStates != null && modelStates.size() > 0) {

                    for (ModelStates country: modelStates) {
                        ModelCountry modelCountry = new ModelCountry();
                        modelCountry.setCountry_id("" + country.getId());
                        modelCountry.setCountry_name(country.getName());
                        stateList.add(modelCountry);
                    }
                }

                stateAdapter =
                        new CountryAdapter(UpdateBasicProfileActivity.this, stateList, new OnCountrySelect() {
                            @Override
                            public void getCountry(ModelCountry modelCountry) {

                                tv_state_title.setText("" + modelCountry.getCountry_name());
                                tv_state_title.setTag("" + modelCountry.getCountry_id());
                                tv_state_title.setSelection(tv_state_title.getText().length());
                                tv_state_title.dismissDropDown();

                                tv_city_title.setHint("Current City");
                                tv_city_title.setText("");
                                tv_city_title.setTag("");

                                modelCities = new ArrayList<>();

                                Utilities.hideKeyboard(UpdateBasicProfileActivity.this);

                                functionGetCity(
                                        tv_country_title.getTag().toString(),
                                        modelCountry.getCountry_id(), true);


                            }
                        });
                tv_state_title.setAdapter(stateAdapter);
                tv_state_title.setThreshold(1);

            } else if (index == 2) {
                citytryList = new ArrayList<>();
                if (modelCities != null && modelCities.size() > 0) {

                    for (ModelStates country: modelCities) {
                        ModelCountry modelCountry = new ModelCountry();
                        modelCountry.setCountry_id("" + country.getId());
                        modelCountry.setCountry_name(country.getName());
                        citytryList.add(modelCountry);
                    }
                }

                cityAdapter =
                        new CountryAdapter(UpdateBasicProfileActivity.this, citytryList, new OnCountrySelect() {
                            @Override
                            public void getCountry(ModelCountry modelCountry) {
                                Utilities.hideKeyboard(UpdateBasicProfileActivity.this);
                                tv_city_title.setText("" + modelCountry.getCountry_name());
                                tv_city_title.setTag("" + modelCountry.getCountry_id());
                                tv_city_title.setSelection(tv_city_title.getText().length());
                                tv_city_title.dismissDropDown();
                            }
                        });

                tv_city_title.setAdapter(cityAdapter);
                tv_city_title.setThreshold(1);
            }

//        listView.setAdapter(countryAdapter);
//        dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getCountries() {
        new VolleyApiRequest(context, false, VolleyApiRequest.REQUEST_BASEURL + "api/get_countries",
                new VolleyCallBack() {
                    @Override
                    public void onResponse(JSONObject response) {
                        parseResponseCountry(response);
                    }

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyApiRequest.showVolleyError(context, error);
                    }
                }).enqueRequest(null, Request.Method.GET);
    }

    public void parseResponseCountry(JSONObject response) {
        try {
            String status = response.getString("status");
            String message = response.getString("msg");

            if (status.equals("200")) {

                JSONArray jsnArr = response.getJSONArray("data");
                Log.i("Success JSONArray--> ", jsnArr.toString());

                for (int i = 0; i < jsnArr.length(); i++) {
                    JSONObject obj = jsnArr.getJSONObject(i);
                    Country country = new Country();
                    country.setCountry_id(obj.getString("code"));
                    country.setCountry_name(obj.getString("name"));
                    Realm realm = Realm.getDefaultInstance();
                    realm.beginTransaction();
                    realm.copyToRealm(country);
                    realm.commitTransaction();
                    showSelectionPopup(0);
                }
            } else if (status.equals("400")) {
                Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}
