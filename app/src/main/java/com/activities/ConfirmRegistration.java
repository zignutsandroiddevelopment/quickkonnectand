package com.activities;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Base64;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.cunoraz.gifview.library.GifView;
import com.customdialog.DialogUploadProfilePic;
import com.customviews.BorderDataColorPicker;
import com.interfaces.OnImagePick;
import com.models.logindata.BasicDetail;
import com.models.logindata.ModelLoggedUser;
import com.quickkonnect.AddSocialMediaAccountActivity;
import com.quickkonnect.DashboardActivity;
import com.quickkonnect.R;
import com.realmtable.MyProfiles;
import com.realmtable.RealmController;
import com.services.LoadProfiles;
import com.utilities.CompressImage;
import com.utilities.QKPreferences;
import com.utilities.Utilities;
import com.volleyapirequest.VolleyApiRequest;
import com.volleyapirequest.VolleyCallBack;
import com.yalantis.ucrop.UCrop;

import net.quikkly.android.Quikkly;
import net.quikkly.android.QuikklyBuilder;
import net.quikkly.android.ui.RenderTagView;
import net.quikkly.core.Skin;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import io.reactivex.annotations.NonNull;

public class ConfirmRegistration extends AppCompatActivity {

    private Context context;
    private LinearLayout linearMale, linearFemale, linearOther, linearDOB;
    private TextView tvDob, tvMen, tvWomen, tvOther;
    private ImageView imgFemale, imgMale, imgOther, img_basic_profile_profile_pic;
    private int mYear, mMonth, mDay;
    private EditText editFName, editLName;
    private Calendar c;
    private ModelLoggedUser modelUserProfile;
    private QKPreferences qkPreferences;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Utilities.ChangeStatusBar(ConfirmRegistration.this);
        setContentView(R.layout.activity_confirm_registration);

        Utilities.ChangeStatusBar(ConfirmRegistration.this);

        context = ConfirmRegistration.this;

        new QuikklyBuilder()
                .setApiKey(getResources().getString(R.string.quikkly_key))
                .loadDefaultBlueprintFromLibraryAssets(this)
                .build()
                .setAsDefault();
        Quikkly.getDefaultInstance();

        qkPreferences = new QKPreferences(this);
        modelUserProfile = qkPreferences.getLoggedUser();

        linearMale = findViewById(R.id.linearMale);
        linearFemale = findViewById(R.id.linearFemale);
        linearDOB = findViewById(R.id.linearDOB);
        linearOther = findViewById(R.id.linearOther);

        tvDob = findViewById(R.id.tvDob);
        tvMen = findViewById(R.id.tvMen);
        tvWomen = findViewById(R.id.tvWomen);
        tvOther = findViewById(R.id.tvOther);

        editFName = findViewById(R.id.editName);
        editLName = findViewById(R.id.editLName);

        imgFemale = findViewById(R.id.imgFemale);
        imgMale = findViewById(R.id.imgMale);
        imgOther = findViewById(R.id.imgOther);
        img_basic_profile_profile_pic = findViewById(R.id.img_basic_profile_profile_pic);


        img_basic_profile_profile_pic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DialogUploadProfilePic(context, onImagePick).showImagePickDialog();
            }
        });

        linearMale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setupMaleFemale(1);
            }
        });

        linearFemale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setupMaleFemale(2);
            }
        });

        linearOther.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setupMaleFemale(3);
            }
        });

        setupMaleFemale(1);

        findViewById(R.id.tv_next_emp).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Utilities.isNetworkConnected(context))
                    submitInformation();
                else
                    Utilities.showToast(context, getResources().getString(R.string.no_internat_connection));
            }
        });

        findViewById(R.id.tvClose).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ConfirmRegistration.this.finish();
            }
        });

        c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);
        c.add(Calendar.YEAR, -13);

        linearDOB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String editextDate = tvDob.getText().toString().trim();
                if (editextDate != null && !editextDate.isEmpty() && !editextDate.equals("null")) {
                    String[] parts = editextDate.split("/");
                    mDay = Integer.parseInt(parts[0]);
                    mMonth = Integer.parseInt(parts[1]) - 1;
                    mYear = Integer.parseInt(parts[2]);
                }
                DatePickerDialog datePickerDialog = new DatePickerDialog(ConfirmRegistration.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                int month = monthOfYear + 1;
                                String fm = "" + month;
                                String fd = "" + dayOfMonth;
                                if (month < 10) {
                                    fm = "0" + month;
                                }
                                if (dayOfMonth < 10) {
                                    fd = "0" + dayOfMonth;
                                }
                                tvDob.setText(fd + "/" + fm + "/" + year);

                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.getDatePicker().setMaxDate(c.getTimeInMillis());
                //datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
                datePickerDialog.show();
            }
        });

        if (modelUserProfile != null) {
            String fname = modelUserProfile.getData().getFirstname();
            if (fname != null && !fname.isEmpty())
                editFName.setText(fname);

            String lname = modelUserProfile.getData().getLastname();
            if (lname != null && !lname.isEmpty())
                editLName.setText(lname);
        }

        inputChanged();
    }

    private Skin skin;
    private RenderTagView renderView;
    private String template = "template0015style2";
    private String Base64Image = "";
    private BorderDataColorPicker borderColor;
    private BorderDataColorPicker maskColor;
    private BorderDataColorPicker dataColor;

    private void inputChanged() {

        try {

            renderView = findViewById(R.id.render_tag);
            borderColor = findViewById(R.id.render_border_color);
            maskColor = findViewById(R.id.render_mask_color);
            dataColor = findViewById(R.id.render_data_color);

            String aa = Utilities.convertToString(getResources().getString(R.string.default_data).replace("#", "0xff"));
            String bb = Utilities.convertToString(getResources().getString(R.string.default_back).replace("#", "0xff"));
            String cc = Utilities.convertToString(getResources().getString(R.string.default_border).replace("#", "0xff"));

            int data_color = Integer.parseInt(aa, 16) + 0xFF000000;
            int bg_color = Integer.parseInt(bb, 16) + 0xFF000000;
            int bc_color = Integer.parseInt(cc, 16) + 0xFF000000;

            borderColor.setColor(bc_color);
            maskColor.setColor(bg_color);
            dataColor.setColor(data_color);

            borderColor.setSelected(true);

            skin = new Skin();

            skin.borderColor = borderColor.getColorHtmlHex();
            skin.maskColor = maskColor.getColorHtmlHex();
            skin.dataColor = dataColor.getColorHtmlHex();

            skin.imageFit = 1;
            skin.logoUrl = "";
            skin.logoFit = 1;

            skin.imageUrl = Utilities.readAndBase64EncodeFromAssets(this, "logo_small.png");

            renderView.post(new Runnable() {
                @Override
                public void run() {
                    getViewBitmap(renderView);
                }
            });

            String unique_code = modelUserProfile.getData().getUniqueCode();

            renderView.setAll(template, TextUtils.isEmpty(unique_code) ? BigInteger.ZERO : new BigInteger(unique_code), skin);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public OnImagePick onImagePick = new OnImagePick() {
        @Override
        public void onUploadPhoto() {
            pickFromGallery();
        }

        @Override
        public void onTakePhoto() {
            getPhotoFromCamera();
        }

        @Override
        public void removePhoto() {

        }
    };

    private static final int REQUEST_SELECT_PICTURE = 100;
    static final int REQUEST_STORAGE_READ_ACCESS_PERMISSION = 6;
    private static final int PICK_FROM_CAMERA = 101;
    private File file;
    private Bitmap selectedImage = null;
    private Uri fileUri;
    private static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 5;
    private String strBase64 = "";

    private void pickFromGallery() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(ConfirmRegistration.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                    REQUEST_STORAGE_READ_ACCESS_PERMISSION);

        } else {
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            intent.addCategory(Intent.CATEGORY_OPENABLE);
            startActivityForResult(Intent.createChooser(intent, getString(R.string.label_select_picture)), REQUEST_SELECT_PICTURE);
        }
    }

    public void getPhotoFromCamera() {
        int currentapiVersion = Build.VERSION.SDK_INT;
        if (currentapiVersion >= Build.VERSION_CODES.M) {
            if (checkAndRequestPermissions())
                launchCamera();

        } else
            launchCamera();
    }

    public void launchCamera() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        file = new File(getExternalCacheDir(), String.valueOf(System.currentTimeMillis()) + ".jpg");

        fileUri = Uri.fromFile(file);


        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
            intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
        } else {
            file = new File(file.getPath());
            Uri photoUri = FileProvider.getUriForFile(getApplicationContext(), getApplicationContext().getPackageName() + ".provider", file);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
        }
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        if (intent.resolveActivity(getApplicationContext().getPackageManager()) != null) {
            startActivityForResult(intent, PICK_FROM_CAMERA);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_STORAGE_READ_ACCESS_PERMISSION:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    pickFromGallery();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != RESULT_CANCELED) {

            if (resultCode == RESULT_OK && requestCode == REQUEST_SELECT_PICTURE) {

                fileUri = data.getData();

                file = new File(fileUri.getPath());

                if (fileUri != null) {
                    try {
                        String compressImg = new CompressImage(context).compressImage("" + fileUri);
                        Uri compressUri = Uri.fromFile(new File(compressImg));
                        startCropActivity(compressUri);
                    } catch (Exception e) {
                        e.printStackTrace();
                        startCropActivity(fileUri);
                    }
                } else
                    Utilities.showToast(context, "" + R.string.toast_cannot_retrieve_selected_image);

            } else if (resultCode == RESULT_OK && requestCode == PICK_FROM_CAMERA) {

                if (fileUri != null)
                    startCropActivity(fileUri);

                else
                    Utilities.showToast(context, "" + R.string.toast_cannot_retrieve_selected_image);

            } else if (requestCode == UCrop.REQUEST_CROP) handleCropResult(data);
        }
    }

    private void startCropActivity(@NonNull Uri uri) {

        String destinationFileName = "" + System.currentTimeMillis();
        destinationFileName += ".png";

        UCrop uCrop = UCrop.of(uri, Uri.fromFile(new File(context.getCacheDir(), destinationFileName)));
        uCrop.withAspectRatio(1, 1);
        uCrop.start(ConfirmRegistration.this);

    }

    private void handleCropResult(@NonNull Intent result) {

        if (result != null) {

            final Uri resultUri = UCrop.getOutput(result);
            if (resultUri != null) {

                try {

                    final InputStream imageStream = getContentResolver().openInputStream(resultUri);
                    selectedImage = BitmapFactory.decodeStream(imageStream);

                    int orientation = Utilities.getCameraPhotoOrientation(context, resultUri, file.getPath());
                    img_basic_profile_profile_pic.setImageBitmap(selectedImage);
                    img_basic_profile_profile_pic.setRotation(orientation);

                } catch (IOException e) {
                    e.printStackTrace();
                }

            } else
                Utilities.showToast(context, "" + R.string.toast_cannot_retrieve_cropped_image);
        }
    }


    public void setupMaleFemale(int type) {
        if (type == 1) {//Male

            linearMale.setBackgroundResource(R.drawable.bg_gendar_selected);
            imgMale.setImageResource(R.drawable.men_2);
            tvMen.setTextColor(getResources().getColor(R.color.gendar_selected_text));

            linearFemale.setBackgroundResource(R.drawable.bg_gendar_unselected);
            imgFemale.setImageResource(R.drawable.female_1);
            tvWomen.setTextColor(getResources().getColor(R.color.gendar_unselected_text));

            linearOther.setBackgroundResource(R.drawable.bg_gendar_unselected);
            imgOther.setImageResource(R.drawable.other4);
            tvOther.setTextColor(getResources().getColor(R.color.gendar_unselected_text));

            imgMale.setTag("1");
            imgFemale.setTag("0");
            imgOther.setTag("0");

        } else if (type == 2) {//Female

            linearFemale.setBackgroundResource(R.drawable.bg_gendar_selected);
            imgFemale.setImageResource(R.drawable.female_2);
            tvWomen.setTextColor(getResources().getColor(R.color.gendar_selected_text));

            linearMale.setBackgroundResource(R.drawable.bg_gendar_unselected);
            imgMale.setImageResource(R.drawable.men_1);
            tvMen.setTextColor(getResources().getColor(R.color.gendar_unselected_text));

            linearOther.setBackgroundResource(R.drawable.bg_gendar_unselected);
            imgOther.setImageResource(R.drawable.other4);
            tvOther.setTextColor(getResources().getColor(R.color.gendar_unselected_text));

            imgFemale.setTag("1");
            imgMale.setTag("0");
            imgOther.setTag("0");

        } else if (type == 3) {//Other

            linearOther.setBackgroundResource(R.drawable.bg_gendar_selected);
            imgOther.setImageResource(R.drawable.other3);
            tvOther.setTextColor(getResources().getColor(R.color.gendar_selected_text));

            linearFemale.setBackgroundResource(R.drawable.bg_gendar_unselected);
            imgFemale.setImageResource(R.drawable.female_1);
            tvWomen.setTextColor(getResources().getColor(R.color.gendar_unselected_text));

            linearMale.setBackgroundResource(R.drawable.bg_gendar_unselected);
            imgMale.setImageResource(R.drawable.men_1);
            tvMen.setTextColor(getResources().getColor(R.color.gendar_unselected_text));

            imgOther.setTag("1");
            imgFemale.setTag("0");
            imgMale.setTag("0");
        }
    }

    public void submitInformation() {

        String fname = editFName.getText().toString();
        String lname = editLName.getText().toString();
        String dateofbirth = tvDob.getText().toString();

        String gendar_male = imgMale.getTag().toString();
        String gendar_female = imgFemale.getTag().toString();
        String gendar_other = imgOther.getTag().toString();

        if (fname == null || fname.isEmpty()) {
            Utilities.showToast(context, "Please enter first name");

        } else if (lname == null || lname.isEmpty()) {
            Utilities.showToast(context, "Please enter last name");

        } else if (dateofbirth == null || dateofbirth.isEmpty()) {
            Utilities.showToast(context, "Please enter date of birth");

        } else {

            String gendar_default = "M";

            if (gendar_male != null && gendar_male.equals("1"))
                gendar_default = "M";

            else if (gendar_female != null && gendar_female.equals("1"))
                gendar_default = "F";

            else if (gendar_other != null && gendar_other.equals("1"))
                gendar_default = "O";

            if (dateofbirth != null && dateofbirth.contains("/")) {
                String[] separated = dateofbirth.split("/");
                String day = separated[0]; // this will contain "Fruit"
                String month = separated[1];
                String year = separated[2];
                dateofbirth = year + "-" + month + "-" + day;
            }

            if (selectedImage == null)
                strBase64 = "";

            else {
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                selectedImage.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                byte[] byteArray = stream.toByteArray();
                strBase64 = Base64.encodeToString(byteArray, 0);
            }

            JSONObject obj = new JSONObject();
            try {

                obj.put("user_id", qkPreferences.getLoggedUser().getData().getId());
                obj.put("firstname", fname);
                obj.put("lastname", lname);
                obj.put("profile_picture", strBase64);
                obj.put("dob", dateofbirth);
                obj.put("gender", gendar_default);

            } catch (JSONException e) {
                e.printStackTrace();
            }

            new VolleyApiRequest(context, true, VolleyApiRequest.REQUEST_BASEURL + "api/change_profile_pic",
                    new VolleyCallBack() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                String status = response.getString("status");
                                String message = response.getString("msg");
                                if (status.equals("200")) {

                                    JSONObject object = response.getJSONObject("data");

                                    BasicDetail basicDetail = modelUserProfile.getData().getBasicDetail();

                                    basicDetail.setFirstname(object.getString("firstname"));
                                    basicDetail.setLastname(object.getString("lastname"));
                                    basicDetail.setQkStory(object.getString("qk_story"));
                                    basicDetail.setProfilePic(object.getString("profile_pic"));
                                    basicDetail.setBirthdate(object.getString("dob"));

                                    modelUserProfile.getData().setName(object.getString("firstname") + " " + object.getString("lastname"));

                                    modelUserProfile.getData().setBasicDetail(basicDetail);

                                    modelUserProfile.getData().setProfilePic(object.getString("profile_pic"));

                                    qkPreferences.storeLoggedUser(modelUserProfile);

                                    String unique_code = qkPreferences.getValues(QKPreferences.USER_UNIQUECODE);

                                    MyProfiles contacts = RealmController.with(ConfirmRegistration.this).getMyProfielDataByProfileId(unique_code);

                                    if (contacts != null) {
                                        MyProfiles update_contact = RealmController.getRealmtoRealmMyProfiles(contacts);
                                        update_contact.setName(object.getString("firstname") + " " + object.getString("lastname"));
                                        RealmController.with(ConfirmRegistration.this).updateMyProfiles(update_contact);
                                    }

                                    generateDefaultQKCode();

                                } else
                                    Utilities.showToast(context, "" + message);

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onErrorResponse(VolleyError error) {
                            VolleyApiRequest.showVolleyError(context, error);
                        }
                    }).enqueRequest(obj, Request.Method.POST);
        }
    }

    public void generateDefaultQKCode() {

        String borderColor = skin.borderColor;
        String backgroundColor = skin.maskColor;
        String dataColor = skin.dataColor;

        borderColor = borderColor.replace("#", "0xff");
        backgroundColor = backgroundColor.replace("#", "0xff");
        dataColor = dataColor.replace("#", "0xff");

        JSONObject obj = new JSONObject();

        try {

            obj.put("middle_tag", "R");

            RealmController.with(ConfirmRegistration.this).deleteTag(String.valueOf(qkPreferences.getLoggedUser().getData().getId()));

            obj.put("qr_code", Base64Image);
            obj.put("border_color", borderColor);
            obj.put("background_color", backgroundColor);
            obj.put("pattern_color", dataColor);
            obj.put("user_id", String.valueOf(qkPreferences.getLoggedUser().getData().getId()));
            obj.put("profile_id", String.valueOf(qkPreferences.getLoggedUser().getData().getId()));
            obj.put("type", "1");
            obj.put("background_gradient", "#0BC5AD");

            new VolleyApiRequest(context, true, VolleyApiRequest.REQUEST_BASEURL + "api/user_qr",
                    new VolleyCallBack() {
                        @Override
                        public void onResponse(JSONObject response) {

                            startService(new Intent(ConfirmRegistration.this, LoadProfiles.class));

                            try {
                                String status = response.getString("status");
                                String message = response.getString("msg");

                                if (status.equals("200")) {

                                    parseQKTagGenerateResponse(response);

                                } else
                                    Utilities.showToast(context, message);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onErrorResponse(VolleyError error) {
                            VolleyApiRequest.showVolleyError(context, error);
                        }
                    }).enqueRequest(obj, Request.Method.POST);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void parseQKTagGenerateResponse(JSONObject response) {

        try {
            JSONObject data = response.getJSONObject("data");

            String background_color = data.getString("background_color");
            String border_color = data.getString("border_color");
            String pattern_color = data.getString("pattern_color");
            String qr_code_url = data.getString("url");
            String qr_middle_tag = data.getString("middle_tag");
            String background_gradient = data.getString("background_gradient");

            String unique_code = modelUserProfile.getData().getUniqueCode();

            MyProfiles myProfiles = RealmController.with(ConfirmRegistration.this).getProfileByID(unique_code);

            if (myProfiles != null) {

                JSONObject jsonObject = new JSONObject();

                MyProfiles updateData = new MyProfiles();

                updateData.setId("" + myProfiles.getId());
                updateData.setName(myProfiles.getName());
                updateData.setBackground_gradient(background_gradient);
                updateData.setStatus(myProfiles.getStatus());
                updateData.setType("" + myProfiles.getType());
                updateData.setLogo("" + myProfiles.getLogo());
                updateData.setUnique_code("" + myProfiles.getUnique_code());

                jsonObject.put("border_color", border_color);
                jsonObject.put("pattern_color", pattern_color);
                jsonObject.put("background_color", background_color);
                jsonObject.put("middle_tag", "");
                jsonObject.put("qr_code", qr_code_url);

                updateData.setQktaginfo(jsonObject.toString());

                RealmController.with(ConfirmRegistration.this).updateMyProfiles(updateData);
            }

            ModelLoggedUser modelLoggedUser = qkPreferences.getLoggedUser();
            modelLoggedUser.getData().setQrCode(qr_code_url);
            qkPreferences.storeLoggedUser(modelLoggedUser);

            ShowSuccessDialog("2");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void ShowSuccessDialog(final String type) {
        final Dialog dialog = new Dialog(ConfirmRegistration.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View view = LinearLayout.inflate(ConfirmRegistration.this, R.layout.layout_sucess_dialog, null);
        dialog.setContentView(view);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        dialog.show();
        dialog.getWindow().setAttributes(lp);

        TextView tvmessage = dialog.findViewById(R.id.tvmessage);
        final GifView gifView1 = (GifView) view.findViewById(R.id.image_suc_gif);
        gifView1.setVisibility(View.VISIBLE);
        gifView1.play();
        gifView1.setGifResource(R.drawable.done);
        gifView1.getGifResource();

        try {

            Handler h = new Handler();
            h.postDelayed(new Runnable() {
                @Override
                public void run() {
                    gifView1.pause();
                }
            }, 2000);

        } catch (Exception e) {
            e.printStackTrace();
        }

        tvmessage.setText(getResources().getString(R.string.profilesavesuccessfully));

        TextView textView = dialog.findViewById(R.id.tv_ok);

        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, DashboardActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });
    }

    private Bitmap getViewBitmap(View v) {

        v.clearFocus();

        v.setPressed(false);

        boolean willNotCache = v.willNotCacheDrawing();

        v.setWillNotCacheDrawing(false);

        int color = v.getDrawingCacheBackgroundColor();

        v.setDrawingCacheBackgroundColor(0);

        if (color != 0) {
            v.destroyDrawingCache();
        }
        v.buildDrawingCache();
        Bitmap cacheBitmap = v.getDrawingCache();
        if (cacheBitmap == null) {
            return null;
        }

        Bitmap bitmap = Bitmap.createBitmap(cacheBitmap);

        // Restore the view
        v.destroyDrawingCache();
        v.setWillNotCacheDrawing(willNotCache);
        v.setDrawingCacheBackgroundColor(color);

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream.toByteArray();

        String encoded = Base64.encodeToString(byteArray, Base64.DEFAULT);

        Base64Image = encoded.replaceAll("\\s+", "");

        return bitmap;
    }

    private boolean checkAndRequestPermissions() {
        int permissionSendMessage = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        int locationPermission = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        List<String> listPermissionsNeeded = new ArrayList<>();
        if (locationPermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (permissionSendMessage != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CAMERA);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), REQUEST_ID_MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }
}
