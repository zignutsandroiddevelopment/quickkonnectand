package com.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.customdialog.SuccessDialog;
import com.interfaces.OnSuccessDialog;
import com.models.logindata.ModelLoggedData;
import com.models.logindata.ModelLoggedUser;
import com.quickkonnect.R;
import com.retrofitservice.ApiUtils;
import com.retrofitservice.SOService;
import com.utilities.CompressImage;
import com.utilities.Constants;
import com.utilities.QKPreferences;
import com.utilities.Utilities;
import com.yalantis.ucrop.UCrop;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

import io.reactivex.annotations.NonNull;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Zignuts Technolab Pvt. Ltd on 21-02-2018.
 */

public class ReportaProblemActivity extends AppCompatActivity {

    private Context context;
    private EditText edt_name, edt_msg, edt_email;
    private LinearLayout img_file;
    private ImageView img_file_report_img;
    private QKPreferences qkPreferences;

    public static File file;
    private Uri fileUri;
    public static Bitmap selectedImage = null;

    public static final int REQUEST_STORAGE_READ_ACCESS_PERMISSION = 6;
    public static final int REQUEST_SELECT_PICTURE = 100;
    public static final int PICK_FROM_CAMERA = 101;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report_a_problem);

        context = ReportaProblemActivity.this;

        try {
            Utilities.ChangeStatusBar(ReportaProblemActivity.this);
        } catch (Exception e) {
            e.printStackTrace();
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_report);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close_white_24dp);
        getSupportActionBar().setTitle(getResources().getString(R.string.report_problem));

        qkPreferences = QKPreferences.getInstance(context);

        initWidget();
    }

    public void initWidget() {

        edt_name = findViewById(R.id.edt_name_report);
        edt_msg = findViewById(R.id.edt_msg_report);
        edt_email = findViewById(R.id.edt_email_report);
        img_file = findViewById(R.id.img_file_report);
        img_file_report_img = findViewById(R.id.img_file_report_img);

        Utilities.hidekeyboard(context, edt_name);

        updateInfo();

        img_file.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle(getResources().getString(R.string.chooseOption));
                String pickOptions[] = {getResources().getString(R.string.choose_from_gallery)};
                builder.setItems(pickOptions, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        switch (i) {
                            case 0:
                                pickFromGallery();
                                break;
                        }
                    }
                });
                builder.setNegativeButton(getResources().getString(R.string.cancel), null);
                builder.show();
            }
        });
    }

    public void updateInfo() {

        ModelLoggedUser modelLoggedUser = qkPreferences.getLoggedUser();

        if (modelLoggedUser != null) {

            ModelLoggedData modelLoggedData = modelLoggedUser.getData();
            if (modelLoggedData != null) {

                if (Utilities.isEmpty(modelLoggedData.getFirstname()) && Utilities.isEmpty(modelLoggedData.getLastname()))
                    edt_name.setText(String.valueOf(modelLoggedData.getFirstname())
                            + " " + String.valueOf(modelLoggedData.getLastname()));

                else if (Utilities.isEmpty(modelLoggedData.getFirstname()))
                    edt_name.setText(String.valueOf(modelLoggedData.getFirstname()));

                else
                    edt_name.setText("");

                if (Utilities.isEmpty(modelLoggedData.getEmail()))
                    edt_email.setText(String.valueOf(modelLoggedData.getEmail()));
                else
                    edt_email.setText("");
            }
        }
    }

    /**
     * Send feedback to admin
     *
     * @param name             - Login user name
     * @param msg              - Message to admin
     * @param email            - Login user email
     * @param attachmentBase64 - Photo - in base64 string format
     */
    public void sendReport(String name, String msg, String email, String attachmentBase64) {
        Utilities.hideKeyboard((Activity) context);
        String user_id = qkPreferences.getLoggedUserid();
        final ProgressDialog progressDialog = Utilities.showProgress(context);
        Map<String, String> headerParameter = Utilities.getHeaderParameter(context);
        SOService soService = ApiUtils.getSOService();
        soService.sendReport(headerParameter, user_id, name, msg, email, attachmentBase64).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Utilities.hideProgressDialog(progressDialog);

                try {

                    JSONObject jsonObject = new JSONObject(response.body().string());

                    String status = jsonObject.getString("status");

                    String message = jsonObject.getString("msg");

                    if (status.equalsIgnoreCase(ApiUtils.RESPONSE_CODE_SUCCESS))
                        new SuccessDialog(context, new OnSuccessDialog() {
                            @Override
                            public void onSuccess() {
                                ReportaProblemActivity.this.finish();
                            }
                        }).showSuccessDialog(message);

                    else Utilities.showToast(ReportaProblemActivity.this, message);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Utilities.hideProgressDialog(progressDialog);
                Utilities.showToast(ReportaProblemActivity.this, String.valueOf(t.getLocalizedMessage()));
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.custom_save_update, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                ReportaProblemActivity.this.finish();
                break;
            case R.id.ic_save_update:

                String name = edt_name.getText().toString();
                String msg = edt_msg.getText().toString();
                String email = edt_email.getText().toString();
                String strBase64 = "";

                if (selectedImage != null) {
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    selectedImage.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                    byte[] byteArray = stream.toByteArray();
                    strBase64 = Base64.encodeToString(byteArray, 0);
                    Log.d("ss", strBase64);
                }

                if (!Utilities.isEmpty(name)) {
                    Utilities.showToast(context, getResources().getString(R.string.namenotempty));

                } else if (!Utilities.isEmpty(msg)) {
                    Utilities.showToast(context, getResources().getString(R.string.msgnotempty));

                } else if (!Utilities.isEmpty(email)) {
                    Utilities.showToast(context, getResources().getString(R.string.emailnotempty));

                } else if (!Utilities.isValidEmail(email)) {
                    Utilities.showToast(context, getResources().getString(R.string.invalidemailenter));

                } else {

                    if (!Utilities.isNetworkConnected(ReportaProblemActivity.this))
                        Utilities.showToast(ReportaProblemActivity.this, Constants.NO_INTERNET_CONNECTION);

                    else
                        sendReport(name, msg, email, strBase64);
                }

                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void pickFromGallery() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN
                && ActivityCompat.checkSelfPermission(context, android.Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(ReportaProblemActivity.this, new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE}, REQUEST_STORAGE_READ_ACCESS_PERMISSION);

        } else {
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            intent.addCategory(Intent.CATEGORY_OPENABLE);
            startActivityForResult(Intent.createChooser(intent, getString(R.string.label_select_picture)), REQUEST_SELECT_PICTURE);
        }
    }

//    public void launchCamera() {
//        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//        file = new File(context.getExternalCacheDir(), String.valueOf(System.currentTimeMillis()) + ".jpg");
//
//        fileUri = Uri.fromFile(file);
//
//        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
//            intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
//        } else {
//            file = new File(file.getPath());
//            Uri photoUri = FileProvider.getUriForFile(context.getApplicationContext(), context.getApplicationContext().getPackageName() + ".provider", file);
//            intent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
//        }
//        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
//        if (intent.resolveActivity(context.getApplicationContext().getPackageManager()) != null) {
//            try {
//                startActivityForResult(intent, PICK_FROM_CAMERA);
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
//    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Utilities.hidekeyboard(context, img_file);

        if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_SELECT_PICTURE) {
            fileUri = data.getData();
            file = new File(fileUri.getPath());
            if (fileUri != null) {
                try {
                    String compressImg = new CompressImage(context).compressImage("" + fileUri);
                    Uri compressUri = Uri.fromFile(new File(compressImg));
                    startCropActivity(compressUri);
                } catch (Exception e) {
                    startCropActivity(fileUri);
                    e.printStackTrace();
                }

            } else
                Utilities.showToast(context, getResources().getString(R.string.toast_cannot_retrieve_selected_image));

        } else if (resultCode == Activity.RESULT_OK && requestCode == PICK_FROM_CAMERA) {

            if (fileUri != null)
                startCropActivity(fileUri);

            else
                Utilities.showToast(context, getResources().getString(R.string.toast_cannot_retrieve_selected_image));

        } else if (resultCode == Activity.RESULT_OK && requestCode == UCrop.REQUEST_CROP) {
            handleCropResult(data);
        }
    }

    private void startCropActivity(@NonNull Uri uri) {
        String destinationFileName = "" + System.currentTimeMillis();
        destinationFileName += ".png";
        UCrop uCrop = UCrop.of(uri, Uri.fromFile(new File(ReportaProblemActivity.this.getCacheDir(), destinationFileName)));
        uCrop.withAspectRatio(1, 1);
        uCrop.start(ReportaProblemActivity.this);
    }

    private void handleCropResult(@NonNull Intent result) {
        if (result != null) {
            final Uri resultUri = UCrop.getOutput(result);
            if (resultUri != null) {
                try {
                    final InputStream imageStream = ReportaProblemActivity.this.getContentResolver().openInputStream(resultUri);
                    selectedImage = BitmapFactory.decodeStream(imageStream);
                    int orientation = Utilities.getCameraPhotoOrientation(CreateProfileActivity.context, resultUri, file.getPath());
                    img_file_report_img.setVisibility(View.VISIBLE);
                    img_file_report_img.setImageBitmap(selectedImage);
                    img_file_report_img.setRotation(orientation);

                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                Utilities.showToast(ReportaProblemActivity.this, "" + R.string.toast_cannot_retrieve_cropped_image);
            }
        }
    }
}