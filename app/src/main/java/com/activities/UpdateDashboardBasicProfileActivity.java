package com.activities;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputFilter;
import android.text.Spanned;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.ceylonlabs.imageviewpopup.ImagePopup;
import com.models.logindata.BasicDetail;
import com.models.logindata.ModelLoggedUser;
import com.quickkonnect.CircleTransform;
import com.quickkonnect.R;
import com.realmtable.MyProfiles;
import com.realmtable.RealmController;
import com.squareup.picasso.Picasso;
import com.utilities.CompressImage;
import com.utilities.Constants;
import com.utilities.QKPreferences;
import com.utilities.Utilities;
import com.volleyapirequest.VolleyApiRequest;
import com.volleyapirequest.VolleyCallBack;
import com.yalantis.ucrop.UCrop;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import io.reactivex.annotations.NonNull;

public class UpdateDashboardBasicProfileActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageView img_basic_profile_profile_pic;
    private EditText edt_basic_profile_firstname, edt_basic_profile_lastname, edt_basic_profile_story;
    private Context context;
    //    private static SharedPreferences mSharedPreferences;
    private String userid;
    //    private MarshMallowPermission marshMallowPermission;
    private static final int PICK_FROM_CAMERA = 101;
    private File file;
    private Uri fileUri;
    int rotateImage;
    private File mediaFile = null;
    private File croppedFile = null;
    private String strBase64 = "";
    static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 5;
    private RelativeLayout view;
    private BottomSheetDialog bottomSheetDialog;
    private static final int REQUEST_SELECT_PICTURE = 100;
    static final int REQUEST_STORAGE_READ_ACCESS_PERMISSION = 6;
    //private static final String SAMPLE_CROPPED_IMAGE_NAME = "SampleCropImage";
    private ImagePopup imagePopup;
    private ModelLoggedUser modelUserProfile;
    private QKPreferences qkPreferences;
    private Bitmap selectedImage = null;
    private TextView tv_counter_udbp;
    private String prevFName = "", prevLName = "", prevStory = "";
    private boolean isPhotoupdate = false;
    byte[] bary;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Utilities.ChangeStatusBar(UpdateDashboardBasicProfileActivity.this);
        setContentView(R.layout.activity_update_dashboard_basic_profile);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN | WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        context = UpdateDashboardBasicProfileActivity.this;

        qkPreferences = new QKPreferences(this);

        try {
            bary = getIntent().getExtras().getByteArray("picture");
        } catch (Exception e) {
            e.printStackTrace();
        }


        userid = qkPreferences.getLoggedUser().getData().getId() + "";

        //marshMallowPermission = new MarshMallowPermission(this);

        img_basic_profile_profile_pic = findViewById(R.id.img_basic_profile_profile_pic);
        edt_basic_profile_firstname = findViewById(R.id.edt_basic_profile_firstname);
        edt_basic_profile_lastname = findViewById(R.id.edt_basic_profile_lastname);
        edt_basic_profile_story = findViewById(R.id.edt_basic_profile_story);
        tv_counter_udbp = findViewById(R.id.tv_counter_udbp);
        //view = findViewById(R.id.constraint);

        if (bary != null && bary.length > 0) {
            Bitmap bmp = BitmapFactory.decodeByteArray(bary, 0, bary.length);
            img_basic_profile_profile_pic.setImageBitmap(bmp);
        }

//        edt_basic_profile_firstname.setFilters(new InputFilter[]{filter});
//        edt_basic_profile_lastname.setFilters(new InputFilter[]{filter});

        Utilities.TextWatcher(edt_basic_profile_story, tv_counter_udbp, "400");

        try {
            Utilities.hidekeyboard(getApplicationContext(), edt_basic_profile_firstname);
        } catch (Exception e) {
            e.printStackTrace();
        }
        img_basic_profile_profile_pic.setOnClickListener(this);
        img_basic_profile_profile_pic.setRotation(rotateImage);

        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close_white_24dp);

        modelUserProfile = qkPreferences.getLoggedUser();

        BasicDetail basicDetail = modelUserProfile.getData().getBasicDetail();

        if (basicDetail != null && !basicDetail.equals("null")) {
            if (basicDetail.getProfilePic() != null && Utilities.isEmpty("" + basicDetail.getProfilePic())) {

                Picasso.with(UpdateDashboardBasicProfileActivity.this)
                        .load("" + basicDetail.getProfilePic())
                        .error(R.drawable.ic_default_profile_photo)
                        .placeholder(R.drawable.ic_default_profile_photo)
                        .transform(new CircleTransform())
                        //.networkPolicy(NetworkPolicy.OFFLINE)
                        .fit()
                        .into(img_basic_profile_profile_pic);

                img_basic_profile_profile_pic.setRotation(rotateImage);

            } else {

                Picasso.with(UpdateDashboardBasicProfileActivity.this)
                        .load("http://sss")
                        .placeholder(R.drawable.ic_default_profile_photo)
                        .error(R.drawable.ic_default_profile_photo)
                        .transform(new CircleTransform())
                        .fit()
                        .into(img_basic_profile_profile_pic);

                img_basic_profile_profile_pic.setRotation(rotateImage);

            }

            if (basicDetail.getFirstname() != null && Utilities.isEmpty(basicDetail.getFirstname())) {
                prevFName = basicDetail.getFirstname();
                edt_basic_profile_firstname.setText(basicDetail.getFirstname());
            }

            if (basicDetail.getLastname() != null && Utilities.isEmpty(basicDetail.getLastname())) {
                prevLName = basicDetail.getLastname();
                edt_basic_profile_lastname.setText(basicDetail.getLastname());
            }

            if (basicDetail.getQkStory() != null && Utilities.isEmpty("" + basicDetail.getQkStory())) {
                prevStory = String.valueOf(basicDetail.getQkStory());
                edt_basic_profile_story.setText("" + basicDetail.getQkStory());
            }
        } else {

            Picasso.with(UpdateDashboardBasicProfileActivity.this)
                    .load("http://sss")
                    .placeholder(R.drawable.ic_default_profile_photo)
                    .error(R.drawable.ic_default_profile_photo)
                    .transform(new CircleTransform())
                    .fit()
                    .into(img_basic_profile_profile_pic);
            img_basic_profile_profile_pic.setRotation(rotateImage);


        }
        functionGetUserProfile(userid);
    }

    InputFilter filter = new InputFilter() {
        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
            for (int i = start; i < end; ++i) {
                if (!Pattern.compile("[-ABCDEFGHIJKLMNNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890&_.',?()]*").matcher(String.valueOf(source.charAt(i))).matches()) {
                    if (!String.valueOf(source.charAt(i)).equalsIgnoreCase(" "))
                        return "";
                }
            }
            return null;
        }
    };

    public void initImagepopup(String userUrl, Uri fileUri) {
        imagePopup = new ImagePopup(UpdateDashboardBasicProfileActivity.this);
        imagePopup.setWindowHeight(800); // Optional
        imagePopup.setWindowWidth(800); // Optional
        imagePopup.setBackgroundColor(Color.BLACK); // Optional
        imagePopup.setFullScreen(true); // Optional
        imagePopup.setHideCloseIcon(false);  // Optional
        imagePopup.setImageOnClickClose(true);  // Optional
        if (Utilities.isEmpty(userUrl))
            imagePopup.initiatePopupWithPicasso(userUrl);
        else if (fileUri != null && Utilities.isEmpty(fileUri.getPath()))
            imagePopup.initiatePopupWithPicasso(fileUri);

        imagePopup.viewPopup();
    }

    public void removePhoto() {
        croppedFile = null;
        img_basic_profile_profile_pic.setImageResource(R.drawable.ic_default_profile_photo);
    }

    public void getPhotoFromCamera() {
        int currentapiVersion = Build.VERSION.SDK_INT;
        if (currentapiVersion >= Build.VERSION_CODES.M) {
            if (checkAndRequestPermissions())
                launchCamera();

        } else
            launchCamera();
    }

    public void launchCamera() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        file = new File(getExternalCacheDir(), String.valueOf(System.currentTimeMillis()) + ".jpg");

        fileUri = Uri.fromFile(file);


        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
            intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
        } else {
            file = new File(file.getPath());
            Uri photoUri = FileProvider.getUriForFile(getApplicationContext(), getApplicationContext().getPackageName() + ".provider", file);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
        }
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        if (intent.resolveActivity(getApplicationContext().getPackageManager()) != null) {
            startActivityForResult(intent, PICK_FROM_CAMERA);
        }
    }


//    public void openCamera() {
//        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//        file = new File(getExternalCacheDir(), String.valueOf(System.currentTimeMillis()) + ".jpg");
//        fileUri = Uri.fromFile(file);
//        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
//        startActivityForResult(intent, PICK_FROM_CAMERA);
//    }

    private void pickFromGallery() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(UpdateDashboardBasicProfileActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                    REQUEST_STORAGE_READ_ACCESS_PERMISSION);

        } else {
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            intent.addCategory(Intent.CATEGORY_OPENABLE);
            startActivityForResult(Intent.createChooser(intent, getString(R.string.label_select_picture)), REQUEST_SELECT_PICTURE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_STORAGE_READ_ACCESS_PERMISSION:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    pickFromGallery();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != RESULT_CANCELED) {
            if (resultCode == RESULT_OK && requestCode == REQUEST_SELECT_PICTURE) {
                fileUri = data.getData();
                file = new File(fileUri.getPath());
                if (fileUri != null) {
                    try {
                        isPhotoupdate = true;
                        String compressImg = new CompressImage(context).compressImage("" + fileUri);
                        Uri compressUri = Uri.fromFile(new File(compressImg));
                        startCropActivity(compressUri);
                    } catch (Exception e) {
                        e.printStackTrace();
                        //Uri compressUri = Uri.fromFile(new File(fileUri.toString()));
                        startCropActivity(fileUri);
                    }
                } else {
                    Utilities.showToast(context, "" + R.string.toast_cannot_retrieve_selected_image);
                }
            } else if (resultCode == RESULT_OK && requestCode == PICK_FROM_CAMERA) {

                if (fileUri != null) {
                    isPhotoupdate = true;
                    //String compressImg = new CompressImage(context).compressImage("" + fileUri);
                    //Uri compressUri = Uri.fromFile(new File(compressImg));
                    startCropActivity(fileUri);

//                rotateImage = Utilities.getCameraPhotoOrientation(UpdateDashboardBasicProfileActivity.this, fileUri,
//                        strBase64);
                } else {
                    Utilities.showToast(context, "" + R.string.toast_cannot_retrieve_selected_image);
                }

            } else if (requestCode == UCrop.REQUEST_CROP) {
                handleCropResult(data);
            }
        }
    }

    public void functionGetUserProfile(final String userid) {
        new VolleyApiRequest(UpdateDashboardBasicProfileActivity.this, false, VolleyApiRequest.REQUEST_BASEURL + "api/get_user_detail/" + userid,
                new VolleyCallBack() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String status = response.getString("status");
                            String message = response.getString("msg");
                            if (status.equals("200")) {
                                JSONObject dataObject = response.getJSONObject("data");
                                JSONObject basic_details = dataObject.getJSONObject("basic_detail");
                                String profile_pic = basic_details.getString("profile_pic");
                                /*ModelUserProfile object = new Gson().fromJson(dataObject.toString(), ModelUserProfile.class);*/
                                if (profile_pic != null && !profile_pic.equalsIgnoreCase("")) {
                                    Picasso.with(UpdateDashboardBasicProfileActivity.this)
                                            .load("" + profile_pic)
                                            .error(R.drawable.ic_default_profile_photo)
                                            .placeholder(R.drawable.ic_default_profile_photo)
                                            .transform(new CircleTransform())
                                            .fit()
                                            .into(img_basic_profile_profile_pic);
                                } else {
                                    img_basic_profile_profile_pic.setImageResource(R.drawable.ic_default_profile_photo);
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onErrorResponse(VolleyError error) {
                    }
                }).
                enqueRequest(null, Request.Method.GET);
    }


    private void handleCropResult(@NonNull Intent result) {

        if (result != null) {

            final Uri resultUri = UCrop.getOutput(result);
            if (resultUri != null) {

                try {
                    final InputStream imageStream = getContentResolver().openInputStream(resultUri);
                    selectedImage = BitmapFactory.decodeStream(imageStream);

//                    File imgPath = Utilities.saveBitmap(context, selectedImage);
//                    Log.e("Store Image", "" + imgPath);

//                    Bitmap bitmap = Utilities.getBitmapfromPaht(imgPath.getAbsolutePath());

//                    selectedImage = Utilities.modifyOrientation(selectedImage1, Uri.fromFile(file));
//                    if (!UCropActivity.isTouchDetected) {


                    int orientation = Utilities.getCameraPhotoOrientation(context, resultUri, file.getPath());
                    img_basic_profile_profile_pic.setImageBitmap(selectedImage);
                    img_basic_profile_profile_pic.setRotation(orientation);

//                    } else {
//                        img_basic_profile_profile_pic.setImageBitmap(selectedImage);
//                    }

//                    Bitmap bitmap = Utilities.rotateBitmap(selectedImage, ExifInterface.ORIENTATION_NORMAL);

                    //selectedImage = Utilities.modifyOrientation(selectedImage, Utilities.getRealPathFromURI(UpdateQkTagActivity.this, resultUri));


                    // articleview is ImageView
//                    Picasso.with(UpdateDashboardBasicProfileActivity.this)
//                            .load(resultUri)
//                            .placeholder(R.drawable.ic_default_profile_photo)
//                            .error(R.drawable.ic_default_profile_photo)
//                            .transform(new CircleTransform())
//                            //.networkPolicy(NetworkPolicy.OFFLINE)
//                            .fit()
//                            .into(img_basic_profile_profile_pic);


                } catch (IOException e) {
                    e.printStackTrace();

                }
            } else {
                Utilities.showToast(UpdateDashboardBasicProfileActivity.this,
                        "" + R.string.toast_cannot_retrieve_cropped_image);
            }
        }
    }


    private void startCropActivity(@NonNull Uri uri) {

        String destinationFileName = "" + System.currentTimeMillis();
        destinationFileName += ".png";

        UCrop uCrop = UCrop.of(uri, Uri.fromFile(new File(context.getCacheDir(), destinationFileName)));
        uCrop.withAspectRatio(1, 1);
        uCrop.start(UpdateDashboardBasicProfileActivity.this);

    }

    private boolean checkAndRequestPermissions() {
        int permissionSendMessage = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        int locationPermission = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        List<String> listPermissionsNeeded = new ArrayList<>();
        if (locationPermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (permissionSendMessage != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CAMERA);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), REQUEST_ID_MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.img_basic_profile_profile_pic:
                bottomSheetDialog = new BottomSheetDialog(UpdateDashboardBasicProfileActivity.this);

                View view1 = getLayoutInflater().inflate(R.layout.custom_bottom_sheet_profile_pic, null);

                bottomSheetDialog.setContentView(view1);

                Button btnCancel = view1.findViewById(R.id.buttonSheet1);
                TextView TextViewSheet1 = view1.findViewById(R.id.TextViewSheet1);
                View divider_viewphoto = view1.findViewById(R.id.divider_viewphoto);
                TextView TextViewSheet2 = view1.findViewById(R.id.TextViewSheet2);
                TextView TextViewSheet3 = view1.findViewById(R.id.TextViewSheet3);
                TextView TextViewSheet4 = view1.findViewById(R.id.TextViewSheet4);

                if (modelUserProfile.getData().getBasicDetail().getProfilePic() != null && Utilities.isEmpty("" + modelUserProfile.getData().getBasicDetail().getProfilePic())) {
                    TextViewSheet1.setVisibility(View.VISIBLE);
                    divider_viewphoto.setVisibility(View.VISIBLE);
                } else {
                    TextViewSheet1.setVisibility(View.GONE);
                    divider_viewphoto.setVisibility(View.GONE);
                }

                btnCancel.setOnClickListener(this);
                TextViewSheet1.setOnClickListener(this);
                TextViewSheet2.setOnClickListener(this);
                TextViewSheet3.setOnClickListener(this);
                TextViewSheet4.setOnClickListener(this);

                bottomSheetDialog.show();
                break;

            case R.id.buttonSheet1:
                bottomSheetDialog.hide();
                break;

            case R.id.TextViewSheet1:
                bottomSheetDialog.hide();
                if (selectedImage != null && !selectedImage.equals("null"))
                    initImagepopup(null, fileUri);
                else if (modelUserProfile.getData().getBasicDetail().getProfilePic() != null
                        && Utilities.isEmpty("" + modelUserProfile.getData().getBasicDetail().getProfilePic()))
                    initImagepopup("" + modelUserProfile.getData().getBasicDetail().getProfilePic(), null);
                else
                    Utilities.showToast(context, "Please select profile for view");
                break;

            case R.id.TextViewSheet2:
                bottomSheetDialog.hide();
                pickFromGallery();
                break;

            case R.id.TextViewSheet3:
                bottomSheetDialog.hide();
                getPhotoFromCamera();
                break;

            case R.id.TextViewSheet4:
                bottomSheetDialog.hide();
                removePhoto();
                break;
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.custom_save_update, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.ic_save_update:

                Utilities.hideKeyboard(UpdateDashboardBasicProfileActivity.this);

                String firstname = edt_basic_profile_firstname.getText().toString();
                String lastname = edt_basic_profile_lastname.getText().toString();
                String stories = edt_basic_profile_story.getText().toString().trim();

                if (!Utilities.isEmpty(firstname)) {
                    Utilities.showToast(context, "Firstname is required");

                } else if (!Utilities.isEmpty(lastname)) {
                    Utilities.showToast(context, "Lastname is required");
                } else {
                    if (selectedImage == null)
                        strBase64 = "";
                    else {
                        ByteArrayOutputStream stream = new ByteArrayOutputStream();
                        selectedImage.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                        byte[] byteArray = stream.toByteArray();
                        strBase64 = Base64.encodeToString(byteArray, 0);
                        Log.d("ss", strBase64);
                    }
                    if (!Utilities.isNetworkConnected(this))
                        Utilities.showToast(context, Constants.NO_INTERNET_CONNECTION);

                    else
                        functionUpdateProfile(userid, firstname, lastname, stories, strBase64);

                }
                return true;

            case android.R.id.home:
                exitfromActivity();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        exitfromActivity();
    }

    public void exitfromActivity() {
        if (isFoundanyupdate()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(UpdateDashboardBasicProfileActivity.this);
            builder.setTitle(getResources().getString(R.string.alert));
            builder.setMessage(getResources().getString(R.string.exitwithoutsave));
            builder.setPositiveButton(getResources().getString(R.string.exit), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    Utilities.hideKeyboard(UpdateDashboardBasicProfileActivity.this);
                    setResult(RESULT_CANCELED);
                    UpdateDashboardBasicProfileActivity.this.finish();
                }
            });
            builder.setNegativeButton(getResources().getString(R.string.close), null);
            builder.show();

        } else {

            Utilities.hideKeyboard(UpdateDashboardBasicProfileActivity.this);
            setResult(RESULT_CANCELED);
            UpdateDashboardBasicProfileActivity.this.finish();
        }
    }

    public boolean isFoundanyupdate() {

        String updatefname = edt_basic_profile_firstname.getText().toString();
        String updatelname = edt_basic_profile_lastname.getText().toString();
        String updatestory = edt_basic_profile_story.getText().toString();

        if (prevFName != null && !prevFName.equals(updatefname)) {
            return true;

        } else if (prevLName != null && !prevLName.equals(updatelname)) {
            return true;

        } else if (isPhotoupdate) {
            return true;

        } else if (prevStory != null && !prevStory.equals(updatestory)) {
            return true;
        }

        return false;
    }


    // Save/Update Basic Profile
    public void functionUpdateProfile(final String user_id, String firstname, String lastname, final String qk_story, String profile_pic) {

        JSONObject obj = new JSONObject();
        try {
            obj.put("user_id", user_id);
            obj.put("firstname", firstname);
            obj.put("lastname", lastname);
            obj.put("qk_story", qk_story);
            obj.put("profile_picture", profile_pic);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        new VolleyApiRequest(context, true, VolleyApiRequest.REQUEST_BASEURL + "api/change_profile_pic",
                new VolleyCallBack() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String status = response.getString("status");
                            String message = response.getString("msg");
                            if (status.equals("200")) {

                                JSONObject object = response.getJSONObject("data");

                                BasicDetail basicDetail = modelUserProfile.getData().getBasicDetail();

                                basicDetail.setFirstname(object.getString("firstname"));
                                basicDetail.setLastname(object.getString("lastname"));
                                basicDetail.setQkStory(object.getString("qk_story"));
                                basicDetail.setProfilePic(object.getString("profile_pic"));

                                modelUserProfile.getData().setName(object.getString("firstname")
                                        + " " + object.getString("lastname"));

                                modelUserProfile.getData().setBasicDetail(basicDetail);

                                modelUserProfile.getData().setProfilePic(object.getString("profile_pic"));

                                qkPreferences.storeLoggedUser(modelUserProfile);

                                String unique_code = qkPreferences.getValues(QKPreferences.USER_UNIQUECODE);

                                /*try {
                                    String user_profilepic = qkPreferences.getValues(QKPreferences.USER_PROFILEIMG);
                                    String name = qkPreferences.getValues(QKPreferences.USER_FULLNAME);
                                    String userid = "" + qkPreferences.getValuesInt(QKPreferences.USER_ID);
                                    String token = FirebaseInstanceId.getInstance().getToken() + "";
                                    // Add user to firebase and update user details
                                    ChatUserModel cum = new ChatUserModel(name, user_profilepic, "1", user_id, token);
                                    ChatConfig.addUser(UpdateDashboardBasicProfileActivity.this, unique_code, cum);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }*/

                                MyProfiles contacts = RealmController.with(UpdateDashboardBasicProfileActivity.this).getMyProfielDataByProfileId(unique_code);

                                if (contacts != null) {
                                    MyProfiles update_contact = RealmController.getRealmtoRealmMyProfiles(contacts);
                                    update_contact.setName(object.getString("firstname") + " " + object.getString("lastname"));
                                    RealmController.with(UpdateDashboardBasicProfileActivity.this).updateMyProfiles(update_contact);
                                }

                                setResult(RESULT_OK);
                                UpdateDashboardBasicProfileActivity.this.finish();

                            } else
                                Utilities.showToast(context, "" + message);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyApiRequest.showVolleyError(context, error);
                    }
                }).enqueRequest(obj, Request.Method.POST);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (bottomSheetDialog != null && bottomSheetDialog.isShowing()) {
            bottomSheetDialog.cancel();
        }
    }

}
