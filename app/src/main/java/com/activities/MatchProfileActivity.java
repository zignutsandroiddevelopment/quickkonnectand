package com.activities;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.Chat.NewChatActivity;
import com.adapter.ApproveContactListAdapter1;
import com.adapter.MatchProfileEducationAdapter;
import com.adapter.MatchProfileExperienceAdapter;
import com.adapter.MatchSocialAdapter;
import com.adapter.UpdateLanguageListAdapter;
import com.adapter.UpdatePublicationListAdapter;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.interfaces.SocialmediaClick;
import com.linkedin.platform.DeepLinkHelper;
import com.linkedin.platform.errors.LIDeepLinkError;
import com.linkedin.platform.listeners.DeepLinkListener;
import com.models.ContactDetail;
import com.models.logindata.LanguageDetail;
import com.models.logindata.ModelLoggedData;
import com.models.logindata.ModelLoggedUser;
import com.models.logindata.PublicationDetail;
import com.models.otherusersprofiles.OtherUserProfilesPojo;
import com.quickkonnect.CircleTransform;
import com.quickkonnect.EmployeeDetails;
import com.quickkonnect.PojoClasses;
import com.quickkonnect.R;
import com.realmtable.Contacts;
import com.realmtable.RealmController;
import com.retrofitservice.ApiUtils;
import com.retrofitservice.SOService;
import com.services.LoadMyContacts;
import com.services.LoadMyFollowers;
import com.services.LoadMyFollowing;
import com.squareup.picasso.Picasso;
import com.utilities.Constants;
import com.utilities.QKPreferences;
import com.utilities.Utilities;
import com.volleyapirequest.VolleyApiRequest;
import com.volleyapirequest.VolleyCallBack;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MatchProfileActivity extends AppCompatActivity implements View.OnClickListener, SwipeRefreshLayout.OnRefreshListener {

    private static final String TAG = "MatchProfile";
    public QKPreferences qkPreferences;
    private Context context;
    private SOService mService;
    private TextView tvlblRequestalreadysent1;
    private Button btnSendRequest;
    private LinearLayout card_scanrequest;
    private String opposite_user_id = "";

    ArrayList<PojoClasses.SocialScanResult> socialScanResultArrayList;
    MatchSocialAdapter matchSocialAdapter;

    // Education
    ArrayList<PojoClasses.Education> educationArrayList;
    MatchProfileEducationAdapter educationListAdapter;

    // Experience
    ArrayList<PojoClasses.Experience> experienceArrayList;
    MatchProfileExperienceAdapter experienceListAdapter;

    // Publication
    private ArrayList<PublicationDetail> publicationArrayList;
    private UpdatePublicationListAdapter publicationListAdapter;

    // Languages
    private ArrayList<LanguageDetail> languageArrayList;
    private UpdateLanguageListAdapter languageListAdapter;

    private String visited_user_id = "0";
    String profile_pic = "";

    private RecyclerView recyclerView_education, recyclerView_experience, recyclerView_download_contact, recyclerView_publications, recyclerView_languages;
    private RecyclerView recyclerView_social;
    public RecyclerView.LayoutManager mLayoutManager;
    private LinearLayout card_contacts, lin_msg, lin_un_match, lin_block, llClose, llfollowerblock, llunfollow, llunBlock;
    private TextView tv_education_data_not_found, tv_experiences_data_not_found, tv_publication_data_not_found, tv_languages_data_not_found, tv_social_match_not_found;
    private TextView tv_designation, tv_username, tv_bub;//, tv_followervlock, tv_unfollow;
    private TextView tv_birthDate, tv_location, tv_hometown, tv_education;
    ImageView img_basic_profile_profile_pic, imgblock;
    private Button btn_download_contact;
    private String unique_code;
    private String scan_user_id, userId, login_user_uniquecode;
    //private NestedScrollView scrollView;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private String fullName, ProfileId = "", UserId_Follower = "";
    private boolean isUniqueCode = false;
    private boolean ismyprofile = false;
    private String keyfrom = "";//, touserid = "", touserdevicetoken = "";
    boolean isFromFollower = false;
    boolean isFromFollower_unBlock = false;
    private LinearLayout llDOB, llLocation, llHometown, llEducation;
    private int FORM_EMPLOYEE_DETAIL = 1020;

    // For Employee Detail
    private RelativeLayout lin_emp_company_detils_cmp;
    private TextView tv_compony_name_cmp, tv_compony_address_cmp;
    private ImageView img_company_logo_emp_cmp;
    private String company_id = "0";
    private Intent intent;

    private static final int PERMISSIONS_REQUEST_READ_CONTACTS = 100;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_match_profile);
        Utilities.ChangeStatusBar(MatchProfileActivity.this);
        context = MatchProfileActivity.this;
        qkPreferences = new QKPreferences(this);
        mService = ApiUtils.getSOService();

        intent = getIntent();

        userId = qkPreferences.getValuesInt(QKPreferences.USER_ID) + "";
        login_user_uniquecode = qkPreferences.getValues(QKPreferences.USER_UNIQUECODE) + "";

        llDOB = findViewById(R.id.llDOB);
        llLocation = findViewById(R.id.llLocation);
        llHometown = findViewById(R.id.llHometown);
        llEducation = findViewById(R.id.llEducation);
        card_contacts = findViewById(R.id.card_contacts);
        imgblock = findViewById(R.id.imgblock);
        lin_msg = findViewById(R.id.lin_profile_message);
        lin_un_match = findViewById(R.id.lin_profile_unmatch);
        lin_block = findViewById(R.id.lin_profile_block);
        llfollowerblock = findViewById(R.id.lin_profile_block_follower);
        llunfollow = findViewById(R.id.lin_profile_unfollow);
        llunBlock = findViewById(R.id.lin_profile_unblock);
        tv_bub = findViewById(R.id.tv_block_txt_profile);


        tvlblRequestalreadysent1 = findViewById(R.id.tvlblRequestalreadysent);
        card_scanrequest = findViewById(R.id.card_scanrequest);

        btnSendRequest = findViewById(R.id.btnSendRequest);
        btnSendRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Utilities.isNetworkConnected(context))
                    sendContactRequest();
                else
                    Utilities.showToast(context, getResources().getString(R.string.no_internat_connection));
            }
        });


        //tv_followervlock = findViewById(R.id.tv_block_txt_profile_follower);
        //tv_unfollow = findViewById(R.id.tv_block_txt_profile_follow);
        llClose = findViewById(R.id.llClose);

        lin_emp_company_detils_cmp = findViewById(R.id.lin_emp_company_detils_cmp);
        tv_compony_name_cmp = findViewById(R.id.tv_compony_name_cmp);
        tv_compony_address_cmp = findViewById(R.id.tv_compony_address_cmp);
        img_company_logo_emp_cmp = findViewById(R.id.img_company_logo_emp_cmp);
        lin_emp_company_detils_cmp.setVisibility(View.GONE);

        lin_msg.setOnClickListener(this);
        llfollowerblock.setOnClickListener(this);
        llunfollow.setOnClickListener(this);
        llunBlock.setOnClickListener(this);
        lin_un_match.setOnClickListener(this);
        lin_block.setOnClickListener(this);

        tv_birthDate = findViewById(R.id.tv_birthdate);
        tv_location = findViewById(R.id.tv_location);
        tv_hometown = findViewById(R.id.tv_hometown);
        tv_education = findViewById(R.id.tv_education);

        tv_designation = findViewById(R.id.tv_designation);
        tv_username = findViewById(R.id.tv_username);

        img_basic_profile_profile_pic = findViewById(R.id.img_basic_profile_profile_pic);

        recyclerView_social = findViewById(R.id.recyclerView_social);
        recyclerView_download_contact = findViewById(R.id.recyclerView_download_contact);
        recyclerView_education = findViewById(R.id.recyclerView_education);
        recyclerView_experience = findViewById(R.id.recyclerView_experience);
        recyclerView_publications = findViewById(R.id.recyclerView_publication);
        recyclerView_languages = findViewById(R.id.recyclerView_languages);

        recyclerView_social.setNestedScrollingEnabled(false);
        recyclerView_download_contact.setNestedScrollingEnabled(false);
        recyclerView_education.setNestedScrollingEnabled(false);
        recyclerView_experience.setNestedScrollingEnabled(false);
        recyclerView_publications.setNestedScrollingEnabled(false);
        recyclerView_languages.setNestedScrollingEnabled(false);

        tv_education_data_not_found = findViewById(R.id.tv_education_data_not_found);
        tv_experiences_data_not_found = findViewById(R.id.tv_experiences_data_not_found);
        tv_publication_data_not_found = findViewById(R.id.tv_publication_data_not_found);
        tv_languages_data_not_found = findViewById(R.id.tv_language_data_not_found);
        tv_social_match_not_found = findViewById(R.id.tv_social_match_not_found);

        btn_download_contact = findViewById(R.id.btn_download_contact);

        mSwipeRefreshLayout = findViewById(R.id.swipeRefreshLayout_match_Profile);
        mSwipeRefreshLayout.setOnRefreshListener(MatchProfileActivity.this);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary,
                android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_blue_dark);

        btn_download_contact.setOnClickListener(this);
        llClose.setOnClickListener(this);

        //Education
        recyclerView_education.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        recyclerView_education.setLayoutManager(mLayoutManager);

        // Experience
        recyclerView_experience.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        recyclerView_experience.setLayoutManager(mLayoutManager);

        // Contact
        recyclerView_download_contact.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        recyclerView_download_contact.setLayoutManager(mLayoutManager);

        // Publication
        recyclerView_publications.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        recyclerView_publications.setLayoutManager(mLayoutManager);

        // Language
        recyclerView_languages.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        recyclerView_languages.setLayoutManager(mLayoutManager);

        recyclerView_social.setFocusable(false);
        recyclerView_education.setFocusable(false);
        recyclerView_experience.setFocusable(false);
        recyclerView_download_contact.setFocusable(false);
        recyclerView_publications.setFocusable(false);
        recyclerView_languages.setFocusable(false);

        findViewById(R.id.relativeRoot).requestFocus();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                try {
                    startService(new Intent(getApplicationContext(), LoadMyContacts.class));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    startService(new Intent(getApplicationContext(), LoadMyFollowers.class));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    startService(new Intent(getApplicationContext(), LoadMyFollowing.class));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, 2000);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && checkSelfPermission(Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED)
            requestPermissions(new String[]{Manifest.permission.READ_CONTACTS}, PERMISSIONS_REQUEST_READ_CONTACTS);

        else callApi();
    }

    @Override
    protected void onNewIntent(Intent intent1) {
        super.onNewIntent(intent1);
        if (intent1 != null) {
            this.intent = intent1;
            callApi();
        }
    }

    public void callApi() {

        if (intent != null) {

            Bundle extras = intent.getExtras();

            keyfrom = extras.getString(Constants.KEY);

            if (keyfrom != null) {

                unique_code = extras.getString("unique_code");

//                if (keyfrom.equalsIgnoreCase(Constants.KEY_BLOCKED + "")) {
//
//                    btn_download_contact.setEnabled(false);
//                    btn_download_contact.setClickable(false);
//                    btn_download_contact.setBackground(getResources().getDrawable(R.drawable.draw_round_corner_gray_bg));
//
//                } else {
//
//                    btn_download_contact.setEnabled(true);
//                    btn_download_contact.setClickable(true);
//                    btn_download_contact.setBackground(getResources().getDrawable(R.drawable.button_selector));
//                }

                Contacts contacts = RealmController.with(MatchProfileActivity.this).getUniqueContacts(unique_code);
                if (contacts != null && !keyfrom.equalsIgnoreCase(Constants.KEY_SCANREQUEST)) {
                    displayData(contacts, true);
                } else {
                    loadDatafromServer(true);
                }

            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PERMISSIONS_REQUEST_READ_CONTACTS) {

            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //Utilities.showToast(context, getResources().getString(R.string.permissiongranted));
                callApi();

            } else
                Utilities.showToast(context, getResources().getString(R.string.cannotdisplaynames));
        }
    }

    public void loadDatafromServer(boolean showLoader) {

        if (keyfrom != null && keyfrom.equals(Constants.KEY_SCANQK)) {
            // Unique code is here
            isUniqueCode = true;
            company_id = intent.getExtras().getString("company_id");
            if (!login_user_uniquecode.equals(unique_code)) {
                fnScanUser(unique_code, showLoader);
            } else {
                isUniqueCode = true;
                ismyprofile = true;
                getOtherUserData(showLoader);
            }

        } else if (keyfrom != null && keyfrom.equalsIgnoreCase(Constants.KEY_FOLLOWER)) {
            // if from  follower contact
            isUniqueCode = false;
            isFromFollower = true;
            try {
                ProfileId = intent.getExtras().getString("ProfileId");
                if (ProfileId != null)
                    ProfileId = ProfileId.replace(".0", "");
                UserId_Follower = intent.getExtras().getString("UserId_Follower");
                if (UserId_Follower != null)
                    UserId_Follower = UserId_Follower.replace(".0", "");
            } catch (Exception e) {
                e.printStackTrace();
            }
            getOtherUserData(showLoader);

        } else if (keyfrom != null && keyfrom.equalsIgnoreCase(Constants.KEY_FOLLOWER_UNBlock)) {
            // if from  follower contact
            isUniqueCode = false;
            isFromFollower_unBlock = true;
            try {
                ProfileId = intent.getExtras().getString("ProfileId");
                if (ProfileId != null)
                    ProfileId = ProfileId.replace(".0", "");
                UserId_Follower = intent.getExtras().getString("UserId_Follower");
                if (UserId_Follower != null)
                    UserId_Follower = UserId_Follower.replace(".0", "");
            } catch (Exception e) {
                e.printStackTrace();
            }
            getOtherUserDataBlock(UserId_Follower, ProfileId, showLoader);

        } else {
            // User id is here
            isUniqueCode = false;
            getOtherUserData(showLoader);
        }
    }

    @Override
    public void onRefresh() {
        if (Utilities.isNetworkConnected(context)) {
            isUniqueCode = false;
            getOtherUserData(true);
        } else
            Utilities.showToast(context, getResources().getString(R.string.no_internat_connection));
    }

    ProgressDialog progressDialog = null;

    public void getOtherUserData(boolean showLoader) {
        mSwipeRefreshLayout.setRefreshing(false);
        if (showLoader)
            progressDialog = Utilities.showProgress(context);

        String token = "";
        if (qkPreferences != null) {
            token = qkPreferences.getApiToken() + "";
        }

        Map<String, String> params = new HashMap<>();
        params.put("Authorization", "Bearer " + token + "");
        params.put("Accept", "application/json");
        params.put("QK_ACCESS_KEY", getResources().getString(R.string.QK_ACCESS_KEY) + "");

        mService.getOtherUserProfile(params, unique_code, login_user_uniquecode, TimeZone.getDefault().getID()).enqueue(new Callback<OtherUserProfilesPojo>() {
            @Override
            public void onResponse(Call<OtherUserProfilesPojo> call, Response<OtherUserProfilesPojo> response) {
                if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();

                Log.e("Other-Profiles-Success", "" + response.body());

                try {

                    if (response != null && response.body() != null) {

                        OtherUserProfilesPojo userProfilesPojo = response.body();

                        if (userProfilesPojo != null && userProfilesPojo.getStatus() == 200) {

                            String scan_result = userProfilesPojo.getData().getScan_request();
                            if (scan_result != null && !scan_result.isEmpty() && scan_result.equalsIgnoreCase(Constants.CONTACT_APPROVED)) {
                                card_scanrequest.setVisibility(View.GONE);
                                JSONObject jsonObject = new JSONObject(new Gson().toJson(userProfilesPojo));
                                updateOtherUserData(jsonObject);

                            } else {

                                opposite_user_id = String.valueOf(userProfilesPojo.getData().getBasicDetail().getUserId());

                                JSONObject jsonObject = new JSONObject(new Gson().toJson(userProfilesPojo));
                                JSONObject data = jsonObject.getJSONObject("data");
                                JSONObject user_basic_detail = data.getJSONObject("basic_detail");

                                String firstName = user_basic_detail.getString("firstname");
                                String lastName = user_basic_detail.getString("lastname");

                                if (Utilities.isEmpty(firstName) && Utilities.isEmpty(lastName))
                                    tv_username.setText(firstName + " " + lastName);

                                else if (Utilities.isEmpty(firstName))
                                    tv_username.setText(firstName + " " + lastName);

                                String qk_story = user_basic_detail.getString("qk_story");

                                if (Utilities.isEmpty(qk_story))
                                    tv_designation.setText(qk_story);
                                else
                                    tv_designation.setText("");


                                String profile_pic = user_basic_detail.getString("profile_pic");
                                if (Utilities.isEmpty(profile_pic))
                                    Picasso.with(context)
                                            .load(profile_pic)
                                            .error(R.drawable.ic_default_profile_photo)
                                            .transform(new CircleTransform())
                                            .placeholder(R.drawable.ic_default_profile_photo)
                                            .fit()
                                            .into(img_basic_profile_profile_pic);

                                if (scan_result != null && !scan_result.isEmpty() && scan_result.equalsIgnoreCase(Constants.CONTACT_PENDING)) {

                                    card_scanrequest.setVisibility(View.VISIBLE);
                                    btnSendRequest.setText(getResources().getString(R.string.resendcontactrequest));
                                    tvlblRequestalreadysent1.setVisibility(View.VISIBLE);
                                    tvlblRequestalreadysent1.setText(getResources().getString(R.string.yourcontactreqalreadysent));

                                } else {
                                    card_scanrequest.setVisibility(View.VISIBLE);
                                    btnSendRequest.setText(getResources().getString(R.string.sendcontactrequest));
                                    tvlblRequestalreadysent1.setVisibility(View.GONE);
                                }
                            }

                        } else finishActivity(getResources().getString(R.string.invaliduser));

                    } else finishActivity(getResources().getString(R.string.invaliduser));

                } catch (Exception e) {
                    finishActivity(e.getMessage());
                }
            }

            @Override
            public void onFailure(Call<OtherUserProfilesPojo> call, Throwable t) {
                if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();
                finishActivity(t.getMessage());
            }
        });
    }

    public void getOtherUserDataBlock(String user_id, String profile_id, boolean showLoader) {
        String timezone = "?timezone=" + TimeZone.getDefault().getID();
        String URL = VolleyApiRequest.REQUEST_BASEURL + "api/get_user_detail/" + user_id + "/" + profile_id + timezone;
        new VolleyApiRequest(MatchProfileActivity.this, showLoader, URL + "",
                new VolleyCallBack() {
                    @Override
                    public void onResponse(JSONObject response) {
                        mSwipeRefreshLayout.setRefreshing(false);
                        updateOtherUserData(response);
                    }

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //VolleyApiRequest.showVolleyError(MatchProfileActivity.this, error);
                        mSwipeRefreshLayout.setRefreshing(false);
                    }
                }).
                enqueRequest(null, Request.Method.GET);
    }

    public void fnScanUser(String unique_code, boolean isProgress) {
        JSONObject obj = new JSONObject();
        try {
            obj.put("unique_code", unique_code);
            obj.put("user_id", userId);
            obj.put("timezone", "" + TimeZone.getDefault().getID());
            obj.put("latitude", qkPreferences.getLati());
            obj.put("longitude", qkPreferences.getLongi());
            obj.put("company_id", company_id + "");
            obj.put("device_type", "Android");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.e("Param-ScanUser", obj.toString());
        new VolleyApiRequest(MatchProfileActivity.this, isProgress, VolleyApiRequest.REQUEST_BASEURL + "api/scan_user_store",
                new VolleyCallBack() {
                    @Override
                    public void onResponse(JSONObject response) {
                        mSwipeRefreshLayout.setRefreshing(false);
                        updateOtherUserData(response);
                    }

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyApiRequest.showVolleyError(MatchProfileActivity.this, error);
                        mSwipeRefreshLayout.setRefreshing(false);
                    }
                }).
                enqueRequest(obj, Request.Method.POST);
    }

    public void updateOtherUserData(JSONObject response) {

        try {

            String status = response.getString("status");
            String message = response.getString("msg");

            if (status.equals("200")) {
                mSwipeRefreshLayout.setVisibility(View.VISIBLE);

                JSONObject data = response.getJSONObject("data");
                JSONObject user_basic_detail = data.getJSONObject("basic_detail");

                scan_user_id = user_basic_detail.getString("user_id");

                String isBlocked = response.optString("is_blocked");

                String userName = user_basic_detail.optString("username");
                String email = user_basic_detail.optString("email");
                String unique_code = user_basic_detail.optString("unique_code");
                String firstName = user_basic_detail.getString("firstname");
                String lastName = user_basic_detail.getString("lastname");
                String qk_story = user_basic_detail.getString("qk_story");
                String birthDate = user_basic_detail.getString("birthdate");
                String hometown = user_basic_detail.getString("hometown");
                String country = user_basic_detail.getString("country");
                String state = user_basic_detail.getString("state");
                String city = user_basic_detail.getString("city");
                String profile_pic = user_basic_detail.getString("profile_pic");
                String device_id = user_basic_detail.getString("device_id");
                String ishide_year = user_basic_detail.getString("ishide_year");

                JSONArray jsnArrExperience = data.getJSONArray("experience_detail");
                JSONArray jsnArrEducation = data.getJSONArray("education_detail");
                JSONArray contact_detail = data.getJSONArray("contact_detail");
                JSONArray approved_contacts = data.getJSONArray("approved_contacts");

                Contacts contacts = new Contacts();
                contacts.setUser_id(scan_user_id);
                contacts.setIshide_year(ishide_year);
                contacts.setUsername(userName);
                contacts.setEmail(email);
                contacts.setFirstname(firstName);
                contacts.setLastname(lastName);
                contacts.setProfile_pic(profile_pic);
                contacts.setBirthdate(birthDate);
                contacts.setHometown(hometown);
                contacts.setCountry(country);
                contacts.setState(state);
                contacts.setCity(city);
                contacts.setQk_story(qk_story);
                contacts.setUnique_code(unique_code);
                contacts.setIsBlocked(isBlocked);
                contacts.setScan_user_device_id(device_id + "");

                try {
                    contacts.setDownload_request_sent(data.optString("download_request_sent"));
                } catch (Exception e) {
                    e.printStackTrace();
                }

                contacts.setContact_detail(contact_detail.toString());
                contacts.setEducation_detail(jsnArrEducation.toString());
                contacts.setExperience_detail(jsnArrExperience.toString());

                JSONArray publication_detail = data.getJSONArray("publication_detail");
                contacts.setPublication_detail(publication_detail.toString());

                JSONArray language_detail = data.getJSONArray("language_detail");
                contacts.setLanguage_detail(language_detail.toString());

                JSONArray jsnArrConnectedSocialMedia = data.getJSONArray("connected_social_media");
                contacts.setConnected_social_media(jsnArrConnectedSocialMedia.toString());

                contacts.setApproved_contacts(approved_contacts.toString());

                try {
                    String scanDate = data.getString("scan_date");
                    if (Utilities.isEmpty(scanDate))
                        contacts.setScan_date(scanDate + "");
                    //Utilities.changeDateFormat("yyyy-MM-dd hh:mm:ss", "EEE, dd MMM yyyy hh:mm", scanDate));
                } catch (Exception e) {
                    e.printStackTrace();
                }

                RealmController.with(MatchProfileActivity.this).updateContacts(contacts);

                mSwipeRefreshLayout.setRefreshing(false);

                displayData(null, false);

                try {

                    JSONObject jsonObject = response.getJSONObject("contact_count");
                    ModelLoggedUser modelLoggedUser = qkPreferences.getLoggedUser();
                    ModelLoggedData modelLoggedData = modelLoggedUser.getData();
                    if (jsonObject.optString("contact_all") != null)
                        modelLoggedData.getContactCount().setContactAll
                                (Integer.parseInt(jsonObject.optString("contact_all")));

                    if (jsonObject.optString("contact_week") != null)
                        modelLoggedData.getContactCount().setContactWeek
                                (Integer.parseInt(jsonObject.optString("contact_week")));

                    if (jsonObject.optString("contact_month") != null)
                        modelLoggedData.getContactCount().setContactMonth
                                (Integer.parseInt(jsonObject.optString("contact_month")));

                    modelLoggedUser.setData(modelLoggedData);
                    qkPreferences.storeLoggedUser(modelLoggedUser);

                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else {
                Utilities.showToast(context, message);
                MatchProfileActivity.this.finish();
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public void displayData(Contacts contacts, boolean isReloadData) {

        try {

            if (contacts == null)
                contacts = RealmController.with(MatchProfileActivity.this).getUniqueContacts(unique_code);

            if (contacts != null) {

                if (Utilities.isEmpty(contacts.getFirstname()) && Utilities.isEmpty(contacts.getLastname()))
                    fullName = contacts.getFirstname() + " " + contacts.getLastname();

                else if (Utilities.isEmpty(contacts.getFirstname()))
                    fullName = contacts.getFirstname();

                else fullName = "";

                tv_username.setText(fullName);

                /****************************************
                 * Added for fast loading of un follow and block unblock todo remove idf not necessory
                 */

                if (keyfrom != null && keyfrom.equals(Constants.KEY_SCANQK)) {
                    // Unique code is here
                    isUniqueCode = true;
                    company_id = intent.getExtras().getString("company_id");
                    if (!login_user_uniquecode.equals(unique_code)) {
                    } else {
                        isUniqueCode = true;
                        ismyprofile = true;
                    }

                } else if (keyfrom != null && keyfrom.equalsIgnoreCase(Constants.KEY_FOLLOWER)) {
                    // if from  follower contact
                    isUniqueCode = false;
                    isFromFollower = true;
                    try {
                        ProfileId = intent.getExtras().getString("ProfileId");
                        if (ProfileId != null)
                            ProfileId = ProfileId.replace(".0", "");
                        UserId_Follower = intent.getExtras().getString("UserId_Follower");
                        if (UserId_Follower != null)
                            UserId_Follower = UserId_Follower.replace(".0", "");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if (keyfrom != null && keyfrom.equalsIgnoreCase(Constants.KEY_FOLLOWER_UNBlock)) {
                    // if from  follower contact
                    isUniqueCode = false;
                    isFromFollower_unBlock = true;
                    try {
                        ProfileId = intent.getExtras().getString("ProfileId");
                        if (ProfileId != null)
                            ProfileId = ProfileId.replace(".0", "");
                        UserId_Follower = intent.getExtras().getString("UserId_Follower");
                        if (UserId_Follower != null)
                            UserId_Follower = UserId_Follower.replace(".0", "");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    // User id is here
                    isUniqueCode = false;
                }


                /****************************************
                 *  Added at 3-7-2018
                 */


                if (ismyprofile) {
                    findViewById(R.id.llotheroptions).setVisibility(View.GONE);

                } else if (contacts.getIsBlocked() != null) {

                    if (contacts.getIsBlocked().equals("1")) {

                        lin_block.setTag("1");
                        tv_bub.setText("Unblock");
                        imgblock.setImageResource(R.drawable.ic_unblock);
                        lin_un_match.setVisibility(View.GONE);
                        lin_msg.setVisibility(View.GONE);

                        btn_download_contact.setEnabled(false);
                        btn_download_contact.setClickable(false);
                        btn_download_contact.setBackground(getResources().getDrawable(R.drawable.draw_round_corner_gray_bg));

                    } else {

                        lin_block.setTag("0");
                        tv_bub.setText("Block");
                        imgblock.setImageResource(R.drawable.ic_block);
                        lin_un_match.setVisibility(View.VISIBLE);

                        btn_download_contact.setEnabled(true);
                        btn_download_contact.setClickable(true);
                        btn_download_contact.setBackground(getResources().getDrawable(R.drawable.button_selector));
                    }

                    findViewById(R.id.llotheroptions).setVisibility(View.VISIBLE);
                }

                visited_user_id = contacts.getUser_id() + "";

                // if from follow contact
                if (isFromFollower) {
                    lin_msg.setVisibility(View.VISIBLE);
                    lin_un_match.setVisibility(View.GONE);
                    lin_block.setVisibility(View.GONE);
                    llfollowerblock.setVisibility(View.VISIBLE);
                    llunfollow.setVisibility(View.VISIBLE);
                    llunBlock.setVisibility(View.GONE);
                    findViewById(R.id.llotheroptions).setVisibility(View.VISIBLE);
                } else {
                    llfollowerblock.setVisibility(View.GONE);
                    llunfollow.setVisibility(View.GONE);
                    llunBlock.setVisibility(View.GONE);
                }

                // if from UnBlock NewScanActivity
                if (isFromFollower_unBlock) {
                    lin_msg.setVisibility(View.GONE);
                    lin_un_match.setVisibility(View.GONE);
                    lin_block.setVisibility(View.GONE);
                    llfollowerblock.setVisibility(View.GONE);
                    llunfollow.setVisibility(View.GONE);
                    llunBlock.setVisibility(View.VISIBLE);
                    findViewById(R.id.llotheroptions).setVisibility(View.VISIBLE);
                } else {
                    llunBlock.setVisibility(View.GONE);
                }

                //QK Story
                if (Utilities.isEmpty(contacts.getQk_story()))
                    tv_designation.setText(contacts.getQk_story());
                else
                    tv_designation.setText("");
                //tv_designation.setText(getResources().getString(R.string.str_qkStory));

                try {
                    //makeTextViewResizable(tv_designation, 6, "Read More", true);
                    String text = tv_designation.getText().toString();
                    Utilities.addReadMore(MatchProfileActivity.this, text, tv_designation);
                } catch (Exception e) {
                    e.printStackTrace();
                }


                //Date of Birth
                if (Utilities.isEmpty(contacts.getBirthdate())) {
                    llDOB.setVisibility(View.VISIBLE);

                    if (contacts.getIshide_year() != null && !contacts.getIshide_year().isEmpty()) {
                        try {
                            String isHide = contacts.getIshide_year();
                            if (isHide.equals("1")) {

                                String split_date[] = contacts.getBirthdate().split("/");
                                if (split_date != null && split_date.length == 3) {
                                    tv_birthDate.setText("" + split_date[0] + "/" + split_date[1]);

                                } else tv_birthDate.setText("" + contacts.getBirthdate());

                            } else {
                                tv_birthDate.setText("" + contacts.getBirthdate());
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                            tv_birthDate.setText("" + contacts.getBirthdate());
                        }
                    }

                } else llDOB.setVisibility(View.GONE);

                //Home Town
                if (Utilities.isEmpty(contacts.getHometown())) {
                    llHometown.setVisibility(View.VISIBLE);
                    tv_hometown.setText("" + contacts.getHometown());
                } else llHometown.setVisibility(View.GONE);

                //Address
                String country = contacts.getCountry();
                String state = contacts.getState();
                String city = contacts.getCity();

                if (Utilities.isEmpty(country) && Utilities.isEmpty(state) && Utilities.isEmpty(city)) {
                    llLocation.setVisibility(View.VISIBLE);
                    tv_location.setText("" + city.concat(" " + state).concat(", " + country));
                } else llLocation.setVisibility(View.GONE);

                //Profile Picture
                profile_pic = contacts.getProfile_pic();
                if (Utilities.isEmpty(profile_pic))
                    Picasso.with(MatchProfileActivity.this)
                            .load(profile_pic)
                            .error(R.drawable.ic_default_profile_photo)
                            .transform(new CircleTransform())
                            .placeholder(R.drawable.ic_default_profile_photo)
                            .fit()
                            .into(img_basic_profile_profile_pic);

                //Experience
                if (Utilities.isEmpty(contacts.getExperience_detail())) {
                    JSONArray jsnArrExperience = new JSONArray(contacts.getExperience_detail());
                    experienceArrayList = new ArrayList<>();
                    if (jsnArrExperience != null && jsnArrExperience.length() > 0 && !jsnArrExperience.toString().equals("null")) {
                        findViewById(R.id.card_experiences).setVisibility(View.VISIBLE);
                        tv_experiences_data_not_found.setVisibility(View.GONE);
                        recyclerView_education.setVisibility(View.VISIBLE);
                        for (int i = 0; i < jsnArrExperience.length(); i++) {
                            JSONObject obj = jsnArrExperience.getJSONObject(i);
                            Log.i("Success obj--> ", obj.toString());
                            experienceArrayList.add(new PojoClasses.Experience(obj.getInt("experience_id"), obj.getString("title"), obj.getString("company_name"), obj.getString("location"), obj.getString("start_date"), obj.getString("end_date")));
                        }
                        experienceListAdapter = new MatchProfileExperienceAdapter(MatchProfileActivity.this, experienceArrayList);
                        recyclerView_experience.setAdapter(experienceListAdapter);
                    } else {
                        findViewById(R.id.card_experiences).setVisibility(View.GONE);
                        tv_experiences_data_not_found.setVisibility(View.VISIBLE);
                        //divider_experience.setVisibility(View.GONE);
                        recyclerView_education.setVisibility(View.GONE);
                    }
                }

                //Social Media
                if (Utilities.isEmpty(contacts.getConnected_social_media())) {
                    JSONArray jsnArrSocialMedia = new JSONArray(contacts.getConnected_social_media());
                    socialScanResultArrayList = new ArrayList<>();
                    if (jsnArrSocialMedia != null && jsnArrSocialMedia.length() > 0 && !jsnArrSocialMedia.toString().equals("null")) {
                        findViewById(R.id.linear_social_match).setVisibility(View.VISIBLE);
                        tv_social_match_not_found.setVisibility(View.GONE);
                        recyclerView_social.setVisibility(View.VISIBLE);

                        for (int i = 0; i < jsnArrSocialMedia.length(); i++) {
                            JSONObject obj = jsnArrSocialMedia.getJSONObject(i);
                            socialScanResultArrayList.add(new PojoClasses.SocialScanResult(
                                    obj.optString("social_media_user_id"),
                                    obj.optString("social_media_id"),
                                    obj.optString("email"),
                                    obj.optString("phone"),
                                    obj.optString("about"),
                                    obj.optString("username"),
                                    obj.optString("politics"),
                                    obj.optString("media_count"),
                                    obj.optString("followers_count"),
                                    obj.optString("followed_by_count"),
                                    obj.optString("friend_list_count"),
                                    obj.optString("likes_count"),
                                    obj.optString("website"),
                                    obj.optString("publicProfileUrl")
                            ));
                        }

                        recyclerView_social.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
                        matchSocialAdapter = new MatchSocialAdapter(MatchProfileActivity.this, socialScanResultArrayList, visited_user_id, socialmediaClick);
                        recyclerView_social.setAdapter(matchSocialAdapter);

                    } else {
                        findViewById(R.id.linear_social_match).setVisibility(View.GONE);
                        tv_social_match_not_found.setVisibility(View.VISIBLE);
                        recyclerView_social.setVisibility(View.GONE);
                    }
                }


                //Education
                if (Utilities.isEmpty(contacts.getEducation_detail())) {
                    JSONArray jsnArrEducation = new JSONArray(contacts.getEducation_detail());
                    educationArrayList = new ArrayList<>();
                    if (jsnArrEducation != null && jsnArrEducation.length() > 0 && !jsnArrEducation.toString().equals("null")) {
                        findViewById(R.id.card_education).setVisibility(View.VISIBLE);
                        llEducation.setVisibility(View.VISIBLE);
                        tv_education_data_not_found.setVisibility(View.GONE);
                        recyclerView_education.setVisibility(View.VISIBLE);
                        String degreeName = jsnArrEducation.getJSONObject(0).getString("degree");
                        tv_education.setText("" + degreeName);
                        for (int i = 0; i < jsnArrEducation.length(); i++) {
                            JSONObject obj = jsnArrEducation.getJSONObject(i);
                            educationArrayList.add(new PojoClasses.Education(obj.getInt("education_id"),
                                    obj.getString("school_name"),
                                    obj.getString("degree"),
                                    obj.getString("field_of_study"),
                                    obj.getString("activities"),
                                    obj.getString("start_date"),
                                    obj.getString("end_date")));
                        }

                        educationListAdapter = new MatchProfileEducationAdapter(MatchProfileActivity.this, educationArrayList);
                        recyclerView_education.setAdapter(educationListAdapter);

                    } else {
                        findViewById(R.id.card_education).setVisibility(View.GONE);
                        //divider_education.setVisibility(View.GONE);
                        tv_education_data_not_found.setVisibility(View.VISIBLE);
                        recyclerView_education.setVisibility(View.GONE);
                        llEducation.setVisibility(View.GONE);

                    }
                }

                //Contacts
                if (Utilities.isEmpty(contacts.getContact_detail())) {
                    JSONArray contact_detail = new JSONArray(contacts.getContact_detail());
                    JSONArray approved_contacts = new JSONArray(contacts.getApproved_contacts());
                    if (approved_contacts != null && approved_contacts.length() > 0 && !approved_contacts.toString().equals("null")) {

                        Type type = new TypeToken<List<ContactDetail>>() {
                        }.getType();

                        List<ContactDetail> contactDetails = new Gson().fromJson(approved_contacts.toString(), type);

                        if (contactDetails != null && contactDetails.size() > 0) {

                            String name = "";

                            if (!tv_username.getText().toString().isEmpty())
                                name = tv_username.getText().toString();

                            List<ContactDetail> approvedcontacts = new ArrayList<>();

                            int count_pending_contacts = 0;

                            for (ContactDetail contactDetail: contactDetails) {
                                if (contactDetail.getIs_approved() == 1) {
                                    if (contactDetail.getPhone() != null && !contactDetail.getPhone().isEmpty() && !contactDetail.getPhone().equals("null")) {
                                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && checkSelfPermission(Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED) {
                                            if (Utilities.contactExists(context, contactDetail.getPhone())) {
                                                contactDetail.setIsContactExist(1);
                                            }
                                        }
                                    }
                                    approvedcontacts.add(contactDetail);

                                } else {
                                    count_pending_contacts = count_pending_contacts + 1;
                                }
                            }

                            if (approvedcontacts != null && approvedcontacts.size() > 0) {

                                card_contacts.setVisibility(View.VISIBLE);
                                btn_download_contact.setVisibility(View.GONE);
                                recyclerView_download_contact.setVisibility(View.VISIBLE);

                                ApproveContactListAdapter1 approveContactListAdapter = new ApproveContactListAdapter1(MatchProfileActivity.this, approvedcontacts, name);
                                recyclerView_download_contact.setAdapter(approveContactListAdapter);

                                if (count_pending_contacts > 0) {

                                    if (ismyprofile) {
                                        btn_download_contact.setVisibility(View.GONE);
                                        findViewById(R.id.tvlblRequestalreadysent).setVisibility(View.GONE);

                                    } else if (contacts.getIsBlocked() == null || contacts.getIsBlocked().equals("0")) {

                                        String isDonloadReqSent = contacts.getDownload_request_sent();
                                        if (isDonloadReqSent != null && !isDonloadReqSent.isEmpty() && isDonloadReqSent.equalsIgnoreCase("true")) {

                                            findViewById(R.id.tvlblRequestalreadysent).setVisibility(View.VISIBLE);
                                            btn_download_contact.setVisibility(View.VISIBLE);
                                            btn_download_contact.setBackgroundResource(R.drawable.button_selector);
                                            btn_download_contact.setText(getResources().getString(R.string.resendrequest));
                                            findViewById(R.id.tvlblRequestalreadysent).setVisibility(View.VISIBLE);
                                            ((TextView) findViewById(R.id.tvlblRequestalreadysent))
                                                    .setText(getResources().getString(R.string.contreqalreadysent));

                                        } else {

                                            btn_download_contact.setVisibility(View.VISIBLE);
                                            btn_download_contact.setBackgroundResource(R.drawable.button_selector);
                                            btn_download_contact.setText(getResources().getString(R.string.download_contact));
                                            findViewById(R.id.tvlblRequestalreadysent).setVisibility(View.VISIBLE);
                                            ((TextView) findViewById(R.id.tvlblRequestalreadysent))
                                                    .setText(fullName + " have more contacts. please click on download contact for more contacts");

                                        }
                                    }

                                } else {
                                    btn_download_contact.setVisibility(View.GONE);
                                    findViewById(R.id.tvlblRequestalreadysent).setVisibility(View.GONE);
                                }

                            } else {

                                card_contacts.setVisibility(View.VISIBLE);
                                recyclerView_download_contact.setVisibility(View.GONE);

                                if (ismyprofile) {
                                    btn_download_contact.setVisibility(View.GONE);
                                    findViewById(R.id.tvlblRequestalreadysent).setVisibility(View.GONE);

                                } else if (contacts.getIsBlocked() == null || contacts.getIsBlocked().equals("0")) {

                                    String isDonloadReqSent = contacts.getDownload_request_sent();

                                    if (isDonloadReqSent != null && !isDonloadReqSent.isEmpty() && isDonloadReqSent.equalsIgnoreCase("true")) {

                                        findViewById(R.id.tvlblRequestalreadysent).setVisibility(View.VISIBLE);
                                        ((TextView) findViewById(R.id.tvlblRequestalreadysent))
                                                .setText(getResources().getString(R.string.contreqalreadysent));
                                        btn_download_contact.setVisibility(View.VISIBLE);
                                        btn_download_contact.setBackgroundResource(R.drawable.button_selector);
                                        btn_download_contact.setText(getResources().getString(R.string.resendrequest));

                                    } else {

                                        findViewById(R.id.tvlblRequestalreadysent).setVisibility(View.GONE);
                                        btn_download_contact.setVisibility(View.VISIBLE);
                                        btn_download_contact.setBackgroundResource(R.drawable.button_selector);
                                        btn_download_contact.setText(getResources().getString(R.string.download_contact));
                                    }
                                }
                            }
                        }

                    } else if (contact_detail != null && contact_detail.length() > 0 && !contact_detail.toString().equals("")) {

                        card_contacts.setVisibility(View.VISIBLE);
                        recyclerView_download_contact.setVisibility(View.GONE);
                        if (ismyprofile || contacts.getIsBlocked().equals("1"))
                            btn_download_contact.setVisibility(View.GONE);
                        else
                            btn_download_contact.setVisibility(View.VISIBLE);

                    } else {

                        card_contacts.setVisibility(View.GONE);
                        btn_download_contact.setVisibility(View.GONE);
                        recyclerView_download_contact.setVisibility(View.GONE);
                    }
                }

//                try {
//                    if (keyfrom.equalsIgnoreCase(Constants.KEY_BLOCKED + "")) {
//                        btn_download_contact.setVisibility(View.VISIBLE);
//                        recyclerView_download_contact.setVisibility(View.GONE);
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }

                if (!Utilities.isEmpty(contacts.getBirthdate()) && !Utilities.isEmpty(contacts.getHometown()) && !Utilities.isEmpty(tv_education.getText().toString())
                        && (!Utilities.isEmpty(country) || !Utilities.isEmpty(city) || !Utilities.isEmpty(state)))
                    findViewById(R.id.card_basic_profile).setVisibility(View.GONE);
                else
                    findViewById(R.id.card_basic_profile).setVisibility(View.VISIBLE);

                //Publication
                if (Utilities.isEmpty(contacts.getPublication_detail())) {
                    JSONArray jsnArrPublication = new JSONArray(contacts.getPublication_detail());
                    Type type = new TypeToken<ArrayList<PublicationDetail>>() {
                    }.getType();
                    publicationArrayList = new Gson().fromJson(jsnArrPublication.toString(), type);
                    if (jsnArrPublication != null && jsnArrPublication.length() > 0 && !jsnArrPublication.toString().equals("null")) {
                        findViewById(R.id.card_publications).setVisibility(View.VISIBLE);
                        tv_publication_data_not_found.setVisibility(View.GONE);
                        recyclerView_publications.setVisibility(View.VISIBLE);
                        publicationListAdapter = new UpdatePublicationListAdapter(MatchProfileActivity.this, publicationArrayList, false, userId + "");
                        recyclerView_publications.setAdapter(publicationListAdapter);

                    } else {
                        findViewById(R.id.card_publications).setVisibility(View.GONE);
                        //divider_publications.setVisibility(View.GONE);
                        tv_publication_data_not_found.setVisibility(View.VISIBLE);
                        recyclerView_publications.setVisibility(View.GONE);
                    }
                }

                //Languages
                if (Utilities.isEmpty(contacts.getLanguage_detail())) {
                    JSONArray jsnArrLanguages = new JSONArray(contacts.getLanguage_detail());
                    Type type = new TypeToken<ArrayList<LanguageDetail>>() {
                    }.getType();
                    languageArrayList = new Gson().fromJson(jsnArrLanguages.toString(), type);
                    if (jsnArrLanguages != null && jsnArrLanguages.length() > 0 && !jsnArrLanguages.toString().equals("null")) {
                        findViewById(R.id.card_language).setVisibility(View.VISIBLE);
                        tv_languages_data_not_found.setVisibility(View.GONE);
                        recyclerView_languages.setVisibility(View.VISIBLE);
                        languageListAdapter = new UpdateLanguageListAdapter(MatchProfileActivity.this, languageArrayList, false);
                        recyclerView_languages.setAdapter(languageListAdapter);

                    } else {
                        findViewById(R.id.card_language).setVisibility(View.GONE);
                        //divider_languages.setVisibility(View.GONE);
                        tv_languages_data_not_found.setVisibility(View.VISIBLE);
                        recyclerView_languages.setVisibility(View.GONE);
                    }
                }


                // adding Employee Details....

                if (keyfrom != null && keyfrom.equalsIgnoreCase(Constants.KEY_OTHER_WITH_EMPLOYEE)) {
                    tv_compony_name_cmp.setText(fullName + "");
                    tv_compony_address_cmp.setText("Employee");
                    if (Utilities.isEmpty(profile_pic))
                        Picasso.with(MatchProfileActivity.this)
                                .load(profile_pic)
                                .error(R.drawable.ic_default_profile_photo)
                                .transform(new CircleTransform())
                                .placeholder(R.drawable.ic_default_profile_photo)
                                .fit()
                                .into(img_company_logo_emp_cmp);
                    lin_emp_company_detils_cmp.setVisibility(View.VISIBLE);

                    try {
                        final String profileid = intent.getExtras().getString("PROFILE_ID");
                        final String uderid = contacts.getUser_id() + "";
                        lin_emp_company_detils_cmp.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Intent intent = new Intent(MatchProfileActivity.this, EmployeeDetails.class);
                                intent.putExtra("profile_id", "" + profileid);
                                intent.putExtra("user_id", "" + uderid);
                                intent.putExtra("scan_user_id", "" + scan_user_id);
                                intent.putExtra(Constants.KEY, Constants.KEY_CONTACT_EMPLOYEER);
                                startActivityForResult(intent, FORM_EMPLOYEE_DETAIL);
                            }
                        });
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    lin_emp_company_detils_cmp.setVisibility(View.GONE);
                }


                if (isReloadData && Utilities.isNetworkConnected(context))
                    loadDatafromServer(false);

            }

        } catch (Exception e) {
            Log.e("Error", e + "");
            e.printStackTrace();
        }
    }


    SocialmediaClick socialmediaClick = new SocialmediaClick() {
        @Override
        public void onClickSocialMedia(final String pos, final String id) {
            //A sample member ID value to open for this first you have to get user id
            DeepLinkHelper deepLinkHelper = DeepLinkHelper.getInstance();
            // Open the target LinkedIn member's profile
            deepLinkHelper.openOtherProfile(MatchProfileActivity.this, id + "", new DeepLinkListener() {
                @Override
                public void onDeepLinkSuccess() {
                    // Successfully sent user to LinkedIn app
                    //Toast.makeText(MatchProfileActivity.this, "Other profile opened successfully.", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onDeepLinkError(LIDeepLinkError error) {
                    // Error sending user to LinkedIn app
                    Log.e(TAG, "Other profile open error : " + error.toString());
                    /*try {
                        Intent intent = new Intent(getApplicationContext(), QuickKonnect_Webview.class);
                        intent.putExtra("other", "http://www.linkedin.com/profile/view?id=" + id);
                        intent.putExtra(Constants.KEY, "LinkedIn");
                        intent.putExtra("VISITED_USER_ID", visited_user_id + "");
                        intent.putExtra("PROFILE_ID", pos + "");
                        intent.putExtra("TYPE", "S");
                        startActivity(intent);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }*/
                }
            });
        }
    };


    @Override
    public void onClick(final View view) {
        String username = tv_username.getText().toString();
        switch (view.getId()) {
            case R.id.btn_download_contact:
                //Object downloadTag = btn_download_contact.getTag();
//                if (Utilities.isEmpty("" + downloadTag) &&
//                        (downloadTag.toString().equals("0") || downloadTag.toString().equals("2"))) {

                if (!Utilities.isEmpty(userId))
                    Utilities.showToast(context, "User id is required");
                else if (!Utilities.isEmpty(scan_user_id))
                    Utilities.showToast(context, "Scan User  id is required");

                else {
                    if (!Utilities.isNetworkConnected(this))
                        Toast.makeText(this, Constants.NO_INTERNET_CONNECTION, Toast.LENGTH_LONG).show();
                    else
                        fnDownloadContactRequest(userId, scan_user_id);
                }
//                } else
//                    Utilities.showToast(context, "You already request to this user");
                break;

            case R.id.llClose:
                MatchProfileActivity.this.finish();
                break;

            case R.id.lin_profile_unblock:
                UnBlockUser();
                break;

            case R.id.lin_profile_block_follower:
                new AlertDialog.Builder(this)
                        .setMessage("Are you sure want to block this user?")
                        .setCancelable(false)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                block_follow();
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        }).show();
                break;
            case R.id.lin_profile_unfollow:
                unfollow_profile();
                break;

            case R.id.lin_profile_message:
                Click_On_Message();
                break;

            case R.id.lin_profile_unmatch:
                String message_unmatch = "Are you sure you want to unmatch " + username + "?";
                new AlertDialog.Builder(this)
                        .setMessage(Html.fromHtml(message_unmatch))
                        .setCancelable(false)
                        .setPositiveButton("UNMATCH", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                CLick_On_Unmatch();
                            }
                        })
                        .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        })
                        .show();
                break;

            case R.id.lin_profile_block:
                if (lin_block.getTag() != null && lin_block.getTag().toString().equals("1")) {
                    final CharSequence[] items = {"Unblock with Match", "Unblock"};
                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    builder.setTitle("Choose Option");
                    builder.setItems(items, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int item) {
                            // Do something with the selection
                            dialog.dismiss();
                            if (item == 0)// Unblock with match request
                                Click_On_Block(false, true);
                            else if (item == 1)// Only Unblock
                                Click_On_Block(false, false);
                        }
                    });
                    builder.setPositiveButton("CANCEL", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    });
                    builder.show();
                    break;
                } else {
                    String message_block = "Are you sure you want to block " + username + "?";
                    //String message_1 = username + " will no longer be able to match with you or start a conversation with you.";
                    String message_1 = username + " will no longer be able to connect with you, view your profile or any other data, nor start a conversation.";
                    //String message_2 = "Blocking " + username + " will also unmatch him/her";
                    String message_2 = "You will no longer be able view " + username + "'s profile and his contact details will be deleted from your Quickkonnect contact list.";
                    final CharSequence[] items = {message_1, message_2};
                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    builder.setItems(items, null);
                    builder.setTitle(message_block);
                    builder.setPositiveButton("BLOCK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                            Click_On_Block(true, false);
                        }
                    });
                    builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    });
                    builder.show();
                    break;
                }
        }
    }

    private void unfollow_profile() {
        JSONObject obj = new JSONObject();
        try {
            obj.put("profile_id", ProfileId + "");
            obj.put("status", "0");
            obj.put("user_id", UserId_Follower + "");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        new VolleyApiRequest(context, true, VolleyApiRequest.REQUEST_BASEURL + "/api/profile/follow_unfollow",
                new VolleyCallBack() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String message = response.getString("msg");
                            String status = response.optString("status");
                            Utilities.showToast(context, message);
                            if (status.equals("200")) {
                                RealmController.with(MatchProfileActivity.this).deleteFollower(unique_code);
                                setResult(RESULT_OK);
                                MatchProfileActivity.this.finish();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyApiRequest.showVolleyError(context, error);
                    }
                }).enqueRequest(obj, Request.Method.POST);
    }

    private void block_follow() {
        JSONObject obj = new JSONObject();
        try {
            obj.put("user_id", UserId_Follower + "");
            obj.put("profile_id", ProfileId + "");
            obj.put("status", "0");
            obj.put("is_blocked", "1");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        new VolleyApiRequest(context, true, VolleyApiRequest.REQUEST_BASEURL + "/api/profile/follow_unfollow",
                new VolleyCallBack() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String message = response.getString("msg");
                            String status = response.optString("status");
                            Utilities.showToast(context, message);
                            if (status.equals("200")) {
                                setResult(RESULT_OK);
                                MatchProfileActivity.this.finish();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyApiRequest.showVolleyError(context, error);
                    }
                }).enqueRequest(obj, Request.Method.POST);
    }

    private void UnBlockUser() {
        JSONObject obj = new JSONObject();
        try {
            obj.put("user_id", UserId_Follower + "");
            obj.put("profile_id", ProfileId + "");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        new VolleyApiRequest(context, true, VolleyApiRequest.REQUEST_BASEURL + "api/profile/unblocked_user",
                new VolleyCallBack() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String message = response.getString("msg");
                            String status = response.optString("status");
                            Utilities.showToast(context, message);
                            if (status.equals("200")) {
                                setResult(RESULT_OK);
                                MatchProfileActivity.this.finish();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyApiRequest.showVolleyError(context, error);
                    }
                }).enqueRequest(obj, Request.Method.POST);
    }

    private void Click_On_Message() {
        String username_rec = tv_username.getText().toString() + "";
        Intent chatIntent = new Intent(context, NewChatActivity.class);
        chatIntent.putExtra("fJID", unique_code + "@quickkonnect.com");
        chatIntent.putExtra("fNickName", username_rec + "");
        chatIntent.putExtra("fAvatarByte", profile_pic + "");
        startActivity(chatIntent);
    }

    private void CLick_On_Unmatch() {

        JSONObject obj = new JSONObject();
        try {
            obj.put("user_id", userId);
            obj.put("requested_user_id", scan_user_id);
            obj.put("status", "U");
            obj.put("timezone", "" + TimeZone.getDefault().getID());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.e("Param-UnMatch", obj.toString());
        new VolleyApiRequest(context, true, VolleyApiRequest.REQUEST_BASEURL + "api/user_contact_block_unmatch",
                new VolleyCallBack() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String message = response.getString("msg");
                            String status = response.optString("status");
                            Utilities.showToast(context, message);

                            if (status.equals("200")) {
                                //updateContacts(response);
                                RealmController.with(MatchProfileActivity.this).deleteContact(unique_code);
                                setResult(RESULT_OK);
                                MatchProfileActivity.this.finish();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //VolleyApiRequest.showVolleyError(context, error);
                        Toast.makeText(context, "Please try again.", Toast.LENGTH_SHORT).show();
                    }
                }).enqueRequest(obj, Request.Method.POST);
    }

    private void Click_On_Block(final boolean isBlock, boolean isMatch) {
        JSONObject obj = new JSONObject();
        try {
            obj.put("user_id", userId);

            obj.put("requested_user_id", scan_user_id);

            if (isBlock)
                obj.put("status", "B");
            else
                obj.put("status", "UB");

            if (isMatch)
                obj.put("is_match", "1");
            else
                obj.put("is_match", "0");

            obj.put("timezone", "" + TimeZone.getDefault().getID());

        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.e("Param-Block", obj.toString());
        new VolleyApiRequest(context, true, VolleyApiRequest.REQUEST_BASEURL + "api/user_contact_block_unmatch",
                new VolleyCallBack() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {

                            String message = response.getString("msg");
                            String status = response.optString("status");

                            Utilities.showToast(context, message);

                            if (status.equals("200")) {
                                //updateContacts(response);
                                if (isBlock) {
                                    RealmController.with(MatchProfileActivity.this).deleteContact(unique_code);
                                    setResult(RESULT_OK);
                                    MatchProfileActivity.this.finish();
                                } else {
                                    setResult(RESULT_OK);
                                    MatchProfileActivity.this.finish();
                                }
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyApiRequest.showVolleyError(context, error);
                    }
                }).enqueRequest(obj, Request.Method.POST);
    }

//    public void updateContacts(JSONObject response) {
//
//        try {
//
//            String responseStatus = response.getString("status");
//            String message = response.getString("msg");
//
//            if (responseStatus.equals("200")) {
//
//                JSONArray jsnArr = response.getJSONArray("data");
//                if (jsnArr != null && jsnArr.length() > 0) {
//                    for (int i = 0; i < jsnArr.length(); i++) {
//                        JSONObject obj = jsnArr.getJSONObject(i);
//                        Contacts contacts = new Contacts();
//                        contacts.setUser_id(obj.getString("scan_user_id"));
//                        contacts.setUsername(obj.getString("scan_user_name"));
//                        contacts.setProfile_pic(obj.getString("scan_user_profile_url"));
//                        contacts.setFirstname(obj.getString("firstname"));
//                        contacts.setLastname(obj.getString("lastname"));
//                        contacts.setScan_date(obj.getString("scan_date"));
//                        contacts.setUnique_code(obj.getString("scan_user_unique_code"));
//                        RealmController.with(MatchProfileActivity.this).updateContacts(contacts);
//                    }
//
//                } //else Utilities.showToast(context, "Invalid Contacts");
//
//            } else Utilities.showToast(context, message);
//
//            setResult(RESULT_OK);
//            MatchProfileActivity.this.finish();
//
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//    }

    public void fnDownloadContactRequest(String userId, String scan_user_id) {

        JSONObject obj = new JSONObject();
        try {
            obj.put("user_id", userId);
            obj.put("scan_user_id", scan_user_id);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.e("Param-ContactRequest", obj.toString());
        new VolleyApiRequest(context, true, VolleyApiRequest.REQUEST_BASEURL + "api/download_contact",
                new VolleyCallBack() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String message = response.getString("msg");
                            String status = response.optString("status");
                            if (status.equals("200") || (message != null && !message.isEmpty() && message.contains("already"))) {
//                                btn_download_contact.setBackgroundResource(R.drawable.button_unselected);
                                btn_download_contact.setText(getResources().getString(R.string.resendrequest));
//                                btn_download_contact.setTag("1");
                                findViewById(R.id.tvlblRequestalreadysent).setVisibility(View.VISIBLE);
                                ((TextView) findViewById(R.id.tvlblRequestalreadysent))
                                        .setText(getResources().getString(R.string.contreqalreadysent));
                            }

                            Utilities.showToast(context, message);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyApiRequest.showVolleyError(context, error);
                    }
                }).enqueRequest(obj, Request.Method.POST);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Add this line to your existing onActivityResult() method
        DeepLinkHelper deepLinkHelper = DeepLinkHelper.getInstance();
        deepLinkHelper.onActivityResult(this, requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK && requestCode == FORM_EMPLOYEE_DETAIL) {
            try {
                RealmController.with(MatchProfileActivity.this).deleteContact(unique_code);
            } catch (Exception e) {
                e.printStackTrace();
            }
            setResult(RESULT_OK);
            MatchProfileActivity.this.finish();
        }
    }

    public void sendContactRequest() {

        final ProgressDialog progressDialog = Utilities.showProgress(context);

        String token = String.valueOf(qkPreferences.getApiToken());

        ModelLoggedUser modelLoggedUser = qkPreferences.getLoggedUser();
        String login_user_id = String.valueOf(modelLoggedUser.getData().getId());

        Map<String, String> header = new HashMap<>();
        header.put("Authorization", "Bearer " + token);
        header.put("Accept", getResources().getString(R.string.acceptjson));
        header.put("QK_ACCESS_KEY", getResources().getString(R.string.QK_ACCESS_KEY) + "");

        mService.sendContactRequest(header, login_user_id, opposite_user_id).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();
                try {

                    JSONObject jsonObject = new JSONObject(response.body().string());
                    String message = jsonObject.optString("msg");
                    Utilities.showToast(context, message);

                    String status = jsonObject.optString("status");
                    if (status != null && !status.isEmpty() && status.equalsIgnoreCase("200")) {
                        btnSendRequest.setText(getResources().getString(R.string.resendcontactrequest));
                        tvlblRequestalreadysent1.setVisibility(View.VISIBLE);
                        tvlblRequestalreadysent1.setText(getResources().getString(R.string.yourcontactreqalreadysent));
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();
            }
        });
    }

    public void finishActivity(String message) {
        Utilities.showToast(context, message);
        MatchProfileActivity.this.finish();
    }
}