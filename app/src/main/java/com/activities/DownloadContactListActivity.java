package com.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.adapter.DownloadContactListAdapter;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.quickkonnect.PojoClasses;
import com.quickkonnect.R;
import com.quickkonnect.SharedPreference;
import com.quickkonnect.SimpleDividerItemDecoration;
import com.utilities.QKPreferences;
import com.utilities.Utilities;
import com.volleyapirequest.VolleyApiRequest;
import com.volleyapirequest.VolleyCallBack;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class DownloadContactListActivity extends AppCompatActivity implements View.OnClickListener {


    private static final String TAG = "";
    ArrayList<PojoClasses.ContactInfo> contactInfoArrayList;
    DownloadContactListAdapter contactListAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    RecyclerView recyclerView_contactinfo;

    private static SharedPreferences mSharedPreferences;
    String userid, sender_id;
    int notification_id;

    Button btn_approve_contact, btn_reject_contact;
    private QKPreferences qkPreferences;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Utilities.changeStatusbar(DownloadContactListActivity.this, getWindow());
        setContentView(R.layout.activity_download_contact_list);
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setBackgroundResource(R.drawable.toolbar_gradient);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close_white_24dp);
        context = DownloadContactListActivity.this;

        recyclerView_contactinfo = findViewById(R.id.recyclerView_contactinfo);

        btn_approve_contact = findViewById(R.id.btn_approve_contact);
        btn_reject_contact = findViewById(R.id.btn_reject_contact);


        btn_approve_contact.setOnClickListener(this);
        btn_reject_contact.setOnClickListener(this);

        mSharedPreferences = getSharedPreferences(SharedPreference.PREF_NAME, 0);
        qkPreferences = new QKPreferences(this);
        userid = qkPreferences.getLoggedUser().getData().getId() + "";

        recyclerView_contactinfo.addItemDecoration(new SimpleDividerItemDecoration(this));
        recyclerView_contactinfo.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        recyclerView_contactinfo.setLayoutManager(mLayoutManager);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            sender_id = extras.getString("sender_id");
            notification_id = extras.getInt("notification_id");
        }

        Log.d(TAG, "onCreate: " + sender_id);

        if (!Utilities.isNetworkConnected(this))
            Utilities.showToast(context, "No internet connection, please try after some time.");
        else
            functionGetContactList(userid);

    }

    // Get Download Contact List
    public void functionGetContactList(String userid) {
        new VolleyApiRequest(context, true, VolleyApiRequest.REQUEST_BASEURL + "api/user_contact_list/" + userid + "/" + sender_id,
                new VolleyCallBack() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {
                            String status = response.getString("status");
                            String message = response.getString("msg");
                            if (status.equals("200")) {

                                recyclerView_contactinfo.setVisibility(View.VISIBLE);
                                findViewById(R.id.llEmpty).setVisibility(View.GONE);

                                //Contact Information
                                JSONArray jsnArrContactInfo = response.getJSONArray("data");
                                contactInfoArrayList = new ArrayList<>();
                                if (jsnArrContactInfo != null && jsnArrContactInfo.length() > 0) {
                                    for (int i = 0; i < jsnArrContactInfo.length(); i++) {
                                        JSONObject obj = jsnArrContactInfo.getJSONObject(i);
                                        Log.i("Success obj--> ", obj.toString());
                                        String is_approved = obj.optString("is_approved");
                                        if (is_approved != null) {
                                            if (is_approved.equals("0"))
                                                contactInfoArrayList.add(new PojoClasses.ContactInfo(obj.getInt("contact_id"), obj.getString("contact_type"), obj.getString("email"), obj.getString("phone")));
                                        } else
                                            contactInfoArrayList.add(new PojoClasses.ContactInfo(obj.getInt("contact_id"), obj.getString("contact_type"), obj.getString("email"), obj.getString("phone")));
                                    }
                                    contactListAdapter = new DownloadContactListAdapter(DownloadContactListActivity.this, contactInfoArrayList);
                                    recyclerView_contactinfo.setAdapter(contactListAdapter);
                                } else {
                                    recyclerView_contactinfo.setVisibility(View.GONE);
                                    findViewById(R.id.llEmpty).setVisibility(View.VISIBLE);
                                }

                            } else {
                                recyclerView_contactinfo.setVisibility(View.GONE);
                                findViewById(R.id.llEmpty).setVisibility(View.VISIBLE);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyApiRequest.showVolleyError(context, error);
                    }
                }).enqueRequest(null, Request.Method.GET);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_approve_contact:
                if (DownloadContactListAdapter.tempArray != null && DownloadContactListAdapter.tempArray.size() > 0) {
                    try {
                        JSONObject jsonObject = new JSONObject();
                        JSONArray array = new JSONArray();
                        for (int i = 0; i < DownloadContactListAdapter.tempArray.size(); i++) {
                            JSONObject internalObject = new JSONObject();
                            internalObject.put("contact_id", DownloadContactListAdapter.tempArray.get(i));
                            array.put(internalObject);

                        }
                        jsonObject.put("sender_id", sender_id);
                        jsonObject.put("contact_approve", array);
                        jsonObject.put("notification_id", notification_id);

                        if (!Utilities.isNetworkConnected(this))
                            Utilities.showToast(context, "No internet connection, please try after some time.");
                        else
                            fnApproveContact(jsonObject);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                break;

            case R.id.btn_reject_contact:
                if (!Utilities.isNetworkConnected(this))
                    Utilities.showToast(context, "No internet connection, please try after some time.");
                else
                    fnRejectContact(userid, sender_id, notification_id);
                break;
        }
    }

    public void fnApproveContact(JSONObject jsonObject) {
        Log.e("Object -> ", jsonObject.toString());
        new VolleyApiRequest(context, true, VolleyApiRequest.REQUEST_BASEURL + "api/user_approved_contact",
                new VolleyCallBack() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String message = response.getString("msg");
                            Utilities.showToast(context, message);
                            Intent intent = new Intent();
                            intent.putExtra("notification_id", notification_id);
                            setResult(RESULT_OK, intent);
                            DownloadContactListActivity.this.finish();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyApiRequest.showVolleyError(context, error);
                    }
                }).enqueRequest(jsonObject, Request.Method.POST);
    }

    public void fnRejectContact(String userid, String sender_id, final int notification_id) {

        JSONObject obj = new JSONObject();
        try {
            obj.put("user_id", userid);
            obj.put("sender_id", sender_id);
            obj.put("notification_id", notification_id);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.e("Object -> ", obj.toString());
        new VolleyApiRequest(context, true, VolleyApiRequest.REQUEST_BASEURL + "api/reject_contact",
                new VolleyCallBack() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String message = response.getString("msg");
                            Utilities.showToast(context, message);
                            Intent intent = new Intent();
                            intent.putExtra("notification_id", notification_id);
                            setResult(RESULT_OK, intent);
                            DownloadContactListActivity.this.finish();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyApiRequest.showVolleyError(context, error);
                    }
                }).enqueRequest(obj, Request.Method.POST);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case android.R.id.home:
                DownloadContactListActivity.this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
