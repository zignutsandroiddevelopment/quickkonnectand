package com.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.Chat.NewChatActivity;
import com.adapter.ScreenSlidePagerAdapter;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.customviews.ZoomOutPageTransformer;
import com.fragments.FragDashboard;
import com.fragments.createprofileintro.FragProfInfo;
import com.fragments.createprofileintro.FragProfileTypeName;
import com.fragments.createprofileintro.FragTagSubTag;
import com.google.android.gms.vision.text.Line;
import com.google.gson.Gson;
import com.interfaces.OnDoneButton;
import com.models.createprofile.CreateProfileData;
import com.models.createprofile.ModelCreateProfile;
import com.models.logindata.ModelLoggedUser;
import com.models.tagsdata.ModelTagData;
import com.models.tagsdata.ModelTagDetail;
import com.quickkonnect.R;
import com.quickkonnect.UpdateQkTagActivity;
import com.realmtable.MyProfiles;
import com.realmtable.RealmController;
import com.realmtable.TagData;
import com.services.CreateEmployeeProfileService;
import com.services.SendPushService;
import com.utilities.Constants;
import com.utilities.QKPreferences;
import com.utilities.Utilities;
import com.volleyapirequest.VolleyApiRequest;
import com.volleyapirequest.VolleyCallBack;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.annotations.NonNull;
import io.realm.Realm;
import io.realm.RealmResults;

import static com.fragments.createprofileintro.FragProfInfo.HideChkbox;
import static com.fragments.createprofileintro.FragProfInfo.ViewChkbox;

public class CreateProfileActivity extends AppCompatActivity {

    private ViewPager mPager;
    private QKPreferences qkPreferences;
    private LinearLayout lin_next;
    public static Context context;
    private TextView tvNext;
    private AppBarLayout appbarlayout_create_profile;
    private ScreenSlidePagerAdapter mPagerAdapter;
    public CreateProfileData createProfileData;
    private ModelLoggedUser modelLoggedUser;
    private static final int RESULT_CODE_SUCCESS = 100;
    private boolean isCreatesubtab = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_profile);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        appbarlayout_create_profile = findViewById(R.id.appbarlayout_create_profile);

        setSupportActionBar(toolbar);

        if (Build.VERSION.SDK_INT >= 19 && Build.VERSION.SDK_INT < 21) {
            setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, true);
        }
        if (Build.VERSION.SDK_INT >= 19) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }
        if (Build.VERSION.SDK_INT >= 21) {
            setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, false);
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close_white_24dp);

        context = CreateProfileActivity.this;
        qkPreferences = new QKPreferences(context);

        modelLoggedUser = qkPreferences.getLoggedUser();

        createProfileData = new CreateProfileData();

        initWidget();
    }

    public static void setWindowFlag(Activity activity, final int bits, boolean on) {
        Window win = activity.getWindow();
        WindowManager.LayoutParams winParams = win.getAttributes();
        if (on) {
            winParams.flags |= bits;
        } else {
            winParams.flags &= ~bits;
        }
        win.setAttributes(winParams);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (mPager != null) {
                    if (mPager.getCurrentItem() == 0) {
                        setResult(RESULT_OK);
                        CreateProfileActivity.this.finish();
                    } else if (mPager.getCurrentItem() == 1) {
                        try {
                            Fragment fragment = mPagerAdapter.getItem(1);
                            if (fragment instanceof FragProfileTypeName) {
                                FragProfileTypeName fragDashboard = (FragProfileTypeName) fragment;
                                String validate = fragDashboard.getValidation();
                                if (validate.equalsIgnoreCase("YES")) {
                                    new AlertDialog.Builder(this)
                                            .setMessage(getResources().getString(R.string.msg_discard_changes))
                                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int whichButton) {
                                                    setResult(RESULT_OK);
                                                    CreateProfileActivity.this.finish();
                                                }
                                            })
                                            .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialogInterface, int i) {
                                                    dialogInterface.dismiss();
                                                }
                                            }).show();
                                } else {
                                    setResult(RESULT_OK);
                                    CreateProfileActivity.this.finish();
                                }
                            } else {
                                setResult(RESULT_OK);
                                CreateProfileActivity.this.finish();
                            }
                        } catch (Exception e) {
                            setResult(RESULT_OK);
                            CreateProfileActivity.this.finish();
                            e.printStackTrace();
                        }
                    } else if (mPager.getCurrentItem() != 0 && mPager.getCurrentItem() != 1) {
                        new AlertDialog.Builder(this)
                                .setMessage(getResources().getString(R.string.msg_discard_changes) + "")
                                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int whichButton) {
                                        setResult(RESULT_OK);
                                        CreateProfileActivity.this.finish();
                                    }
                                })
                                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                    }
                                }).show();
                    } else {
                        setResult(RESULT_OK);
                        CreateProfileActivity.this.finish();
                    }
                } else {
                    setResult(RESULT_OK);
                    CreateProfileActivity.this.finish();
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onBackPressed() {
        if (mPager != null) {
            if (mPager.getCurrentItem() == 0) {
                setResult(RESULT_OK);
                CreateProfileActivity.this.finish();
            } else if (mPager.getCurrentItem() == 1) {
                try {
                    Fragment fragment = mPagerAdapter.getItem(1);
                    if (fragment instanceof FragProfileTypeName) {
                        FragProfileTypeName fragDashboard = (FragProfileTypeName) fragment;
                        String validate = fragDashboard.getValidation();
                        if (validate.equalsIgnoreCase("YES")) {
                            new AlertDialog.Builder(this)
                                    .setMessage(getResources().getString(R.string.msg_discard_changes) + "")
                                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int whichButton) {
                                            setResult(RESULT_OK);
                                            CreateProfileActivity.this.finish();
                                        }
                                    })
                                    .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            dialogInterface.dismiss();
                                        }
                                    }).show();
                        } else {
                            setResult(RESULT_OK);
                            CreateProfileActivity.this.finish();
                        }
                    } else {
                        setResult(RESULT_OK);
                        CreateProfileActivity.this.finish();
                    }
                } catch (Exception e) {
                    setResult(RESULT_OK);
                    CreateProfileActivity.this.finish();
                    e.printStackTrace();
                }
            } else if (mPager.getCurrentItem() != 0 && mPager.getCurrentItem() != 1) {
                new AlertDialog.Builder(this)
                        .setMessage(getResources().getString(R.string.msg_discard_changes) + "")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                setResult(RESULT_OK);
                                CreateProfileActivity.this.finish();
                            }
                        })
                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        }).show();
            } else {
                setResult(RESULT_OK);
                CreateProfileActivity.this.finish();
            }
        } else {
            setResult(RESULT_OK);
            CreateProfileActivity.this.finish();
        }
    }

    public void initWidget() {

        tvNext = findViewById(R.id.tvNext);
        lin_next = findViewById(R.id.lin_next);

        RealmResults<TagData> taglist = RealmController.with(CreateProfileActivity.this).getAllTags();
        if (taglist == null || taglist.size() < 0 || taglist.size() == 0) {
            getTags();
        }

        mPager = (ViewPager) findViewById(R.id.pager);
        mPagerAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager(), onDoneButton);
        mPager.setPageTransformer(true, new ZoomOutPageTransformer());
        mPager.setAdapter(mPagerAdapter);
        tvNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvNext.setText("Next");
                boolean isvalidate = true;
                int current = mPager.getCurrentItem();
                Fragment fragment = mPagerAdapter.getItem(current);
                if (fragment instanceof FragProfileTypeName) {
                    FragProfileTypeName profileTypeName = (FragProfileTypeName) fragment;
                    createProfileData = profileTypeName.getNameType(createProfileData);
                    isvalidate = checkValidation(1, createProfileData);
                } else if (fragment instanceof FragTagSubTag) {
                    FragTagSubTag profileTypeName = (FragTagSubTag) fragment;
                    createProfileData = profileTypeName.getTagSubtag(createProfileData);
                    int profiletype = 1;
                    if (createProfileData.getType() != null && (createProfileData.getType() == 1 || createProfileData.getType() == 2)) {
                        profiletype = 1;
                        HideChkbox();
                    } else {
                        profiletype = 3;
                        ViewChkbox();
                    }
                    if (createProfileData.getSubTag() == -1) {
                        isCreatesubtab = true;
                        if (createProfileData.getSubtagText() != null && !createProfileData.getSubtagText().isEmpty())
                            createProfileData = createNewSubTag(createProfileData, profiletype);
                        else
                            Utilities.showToast(context, "Subcategory should not empty");
                    } else {
                        isCreatesubtab = false;
                        isvalidate = checkValidation(2, createProfileData);
                    }
                    findViewById(R.id.frame_create_main).setBackgroundColor(getResources().getColor(R.color.bg));

                } else if (fragment instanceof FragProfInfo) {

                }

                if (createProfileData.getType() != null && modelTagDataList == null) {
                    if (isvalidate)
                        getTags(createProfileData);
                } else {
                    if (!isCreatesubtab && isvalidate && current != 3)
                        try {
                            mPager.setCurrentItem(current + 1);
                            fragment = mPagerAdapter.getItem(mPager.getCurrentItem());
                            if (fragment instanceof FragProfInfo) {
                                FragProfInfo fragProfInfo = (FragProfInfo) fragment;

                                if (createProfileData.getType() != null && createProfileData.getType() == Constants.TYPE_PROFILE_COMPANY || createProfileData.getType() == Constants.TYPE_PROFILE_ENTERPRISE) {

                                    fragProfInfo.linear_founder.setVisibility(View.VISIBLE);
                                    fragProfInfo.input_teamsize.setVisibility(View.VISIBLE);
                                    fragProfInfo.input_doe.setVisibility(View.VISIBLE);
                                    fragProfInfo.input_ceo.setVisibility(View.VISIBLE);
                                    //fragProfInfo.input_cto.setVisibility(View.VISIBLE);
                                    fragProfInfo.input_dob.setVisibility(View.GONE);

                                } else if (createProfileData.getType() == Constants.TYPE_PROFILE_CELEBRITY) {
                                    fragProfInfo.linear_founder.setVisibility(View.GONE);
                                    fragProfInfo.input_teamsize.setVisibility(View.GONE);
                                    fragProfInfo.input_doe.setVisibility(View.GONE);
                                    fragProfInfo.input_ceo.setVisibility(View.GONE);
                                    //fragProfInfo.input_cto.setVisibility(View.GONE);
                                    fragProfInfo.input_dob.setVisibility(View.VISIBLE);
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                }
            }
        });

        mPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                try {
                    if (mPager.getCurrentItem() == 3) {
                        lin_next.setVisibility(View.GONE);
                        getSupportActionBar().hide();
                        //appbarlayout_create_profile.setVisibility(View.GONE);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    public void saveFragProfinfo() {

        try {
            boolean isvalidate = true;
            int current = mPager.getCurrentItem();
            Fragment fragment = mPagerAdapter.getItem(current);

            FragProfInfo profileTypeName = (FragProfInfo) fragment;
            createProfileData = profileTypeName.getProfileInfo(createProfileData);
            createProfileData.setUserId(modelLoggedUser.getData().getId());

            if (createProfileData.getType() != null && (createProfileData.getType() == 1 || createProfileData.getType() == 2)) {
                isvalidate = checkValidation(3, createProfileData);
                if (isvalidate) {
                    Log.e("CreateProfileData", "" + createProfileData);
                    try {
                        JSONObject jsonObject = new JSONObject(new Gson().toJson(createProfileData));
                        submitData(jsonObject);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } else {
                if (createProfileData.getIscheckedBox()) {
                    isvalidate = checkValidation(3, createProfileData);
                    if (isvalidate) {
                        Log.e("CreateProfileData", "" + createProfileData);
                        try {
                            JSONObject jsonObject = new JSONObject(new Gson().toJson(createProfileData));
                            submitData(jsonObject);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                } else {
                    Utilities.showToast(context, "Please accept the check box, your are representing the person whom you are signing up");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void CloseActivity() {

        try {
            new AlertDialog.Builder(this)
                    .setMessage(getResources().getString(R.string.msg_discard_changes) + "")
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            setResult(RESULT_OK);
                            CreateProfileActivity.this.finish();
                        }
                    })
                    .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    }).show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean checkValidation(int pos, CreateProfileData createProfileData) {

        // Profile Name
        if (pos == 1 && (createProfileData == null || createProfileData.getName() == null || (createProfileData.getName() != null && !Utilities.isEmpty("" + createProfileData.getName())))) {
            Utilities.showToast(context, "Profile name should not empty");
            return false;
        }

        // Profile Type
        if (pos == 1 && (createProfileData == null || createProfileData.getType() == null || (createProfileData.getType() != null && !Utilities.isEmpty("" + createProfileData.getType())))) {
            Utilities.showToast(context, "Profile type should not empty");
            return false;
        }

        // Tag
        if (pos == 2 && (createProfileData == null || createProfileData.getTag() == null || (createProfileData.getTag() != null && !Utilities.isEmpty("" + createProfileData.getTag())))) {
            Utilities.showToast(context, "Category tag should not empty");
            return false;
        }

        // Sub Tag
        if (pos == 2 && (createProfileData == null || createProfileData.getSubTag() == null || (createProfileData.getSubTag() != null && !Utilities.isEmpty("" + createProfileData.getSubTag())))) {
            Utilities.showToast(context, "Subcategory should not empty");
            return false;
        }

        // About
        if (pos == 3 && (createProfileData == null || createProfileData.getAbout() == null || (createProfileData.getAbout() != null && !Utilities.isEmpty("" + createProfileData.getAbout())))) {
            Utilities.showToast(context, "About should not empty");
            return false;
        }

        // Number of Employees
        /*if (pos == 3 && (createProfileData == null || createProfileData.getCompanyTeamSize() == null || (createProfileData.getCompanyTeamSize() != null && !Utilities.isEmpty("" + createProfileData.getCompanyTeamSize())))) {
            Utilities.showToast(context, "Team size should not empty");
            return false;
        }

        // Date of Establishment
        if (pos == 3 && (createProfileData == null || createProfileData.getEstablishDate() == null || (createProfileData.getEstablishDate() != null && !Utilities.isEmpty("" + createProfileData.getEstablishDate())))) {
            Utilities.showToast(context, "Date of establishment should not empty");
            return false;
        }



        if (pos == 3 && (createProfileData == null || createProfileData.getPhone() == null || (createProfileData.getPhone() != null && createProfileData.getPhone().size() == 0))) {
            Utilities.showToast(context, "Phone number should not blank");
            return false;
        }
        */
        // Address
        if (pos == 3 && (createProfileData == null || createProfileData.getAddress() == null || (createProfileData.getAddress() != null && !Utilities.isEmpty("" + createProfileData.getAddress())))) {
            Utilities.showToast(context, "Address should not empty");
            return false;
        }
        if (pos == 3 && (createProfileData == null || createProfileData.getEmail() == null || (createProfileData.getEmail() != null && createProfileData.getEmail().size() == 0))) {
            Utilities.showToast(context, "Email should not blank");
            return false;
        }

        return true;
    }

    List<ModelTagDetail> modelTagDataList;

    public void getTags(final CreateProfileData createProfileData) {
        String profiletype = "1";
        if (createProfileData.getType() != null && (createProfileData.getType() == 1 || createProfileData.getType() == 2)) {
            profiletype = "1";
        } else {
            profiletype = "3";
        }
        //RealmResults<TagData> taglist = RealmController.with(CreateProfileActivity.this).getFilturedTagList(profiletype);
        //List<ModelTagDetail> oldmodelTagDataList = new ArrayList<>();
        //RealmResults<TagData> taglist = RealmController.with(CreateProfileActivity.this).getAllTags();

        try {
            RealmResults<TagData> finalList = RealmController.with(CreateProfileActivity.this).getFilturedTagList(profiletype);
            if (finalList != null && finalList.size() > 0) {
                modelTagDataList = new ArrayList<>();
                for (int i = 0; i < finalList.size(); i++) {
                    ModelTagDetail md = new ModelTagDetail();
                    if (finalList.get(i) != null)
                        md.setId(finalList.get(i).getId());
                    if (finalList.get(i).getName() != null)
                        md.setName(finalList.get(i).getName());
                    if (finalList.get(i).getType() != null)
                        md.setType(finalList.get(i).getType());
                    modelTagDataList.add(md);
                }
                if (mPager.getCurrentItem() != 2) {
                    mPager.setCurrentItem(2);
                }
                Fragment fragment = mPagerAdapter.getItem(mPager.getCurrentItem());
                if (fragment instanceof FragTagSubTag) {
                    FragTagSubTag profileTypeName = (FragTagSubTag) fragment;
                    int protype = 1;
                    if (createProfileData.getType() != null && (createProfileData.getType() == 1 || createProfileData.getType() == 2)) {
                        protype = 1;
                    } else {
                        protype = 3;
                    }
                    profileTypeName.fillCategories(modelTagDataList, protype);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void getTags() {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("parent_id", 0);
            //jsonObject.put("type", 1);
            new VolleyApiRequest(context, false,
                    VolleyApiRequest.REQUEST_BASEURL + VolleyApiRequest.API_TAGLIST,
                    new VolleyCallBack() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                if (response != null && response.length() > 0) {
                                    ModelTagData modelTagData = new Gson().fromJson(response.toString(), ModelTagData.class);
                                    if (modelTagData.getStatus() == 200) {
                                        if (modelTagData.getData() != null && modelTagData.getData().size() > 0) {

                                            RealmController.getInstance().clearAllTags();

                                            for (ModelTagDetail modelTagDetail : modelTagData.getData()) {
                                                TagData tagData = new TagData();
                                                tagData.setId(modelTagDetail.getId());
                                                tagData.setName(modelTagDetail.getName());
                                                tagData.setType(modelTagDetail.getType());

                                                Realm realm = Realm.getDefaultInstance();
                                                realm.beginTransaction();
                                                realm.copyToRealm(tagData);
                                                realm.commitTransaction();
                                            }
                                        }
                                    } else
                                        Utilities.showToast(context, modelTagData.getMsg());
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onErrorResponse(VolleyError error) {
                            VolleyApiRequest.showVolleyError(context, error);
                        }
                    }).enqueRequest(jsonObject, Request.Method.POST);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public CreateProfileData createNewSubTag(final CreateProfileData createProfileData, int type) {

        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("parent_id", createProfileData.getTag());
            jsonObject.put("type", type);
            jsonObject.put("name", createProfileData.getSubtagText());
            new VolleyApiRequest(context, false,
                    VolleyApiRequest.REQUEST_BASEURL + VolleyApiRequest.API_CREATE_NEW_SUBCAT,
                    new VolleyCallBack() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                if (response != null && response.length() > 0) {
                                    JSONObject jobj = response.getJSONObject("data");
                                    int id = jobj.getInt("sub_tag_id");
                                    createProfileData.setSubTag(id);
                                    createProfileData.setSubtagText(null);
                                }
                                mPager.setCurrentItem(3);
                                Fragment fragment = mPagerAdapter.getItem(mPager.getCurrentItem());
                                if (fragment instanceof FragProfInfo) {
                                    FragProfInfo fragProfInfo = (FragProfInfo) fragment;

                                    if (createProfileData.getType() != null &&
                                            createProfileData.getType() == Constants.TYPE_PROFILE_COMPANY
                                            || createProfileData.getType() == Constants.TYPE_PROFILE_ENTERPRISE) {

                                        fragProfInfo.linear_founder.setVisibility(View.VISIBLE);
                                        fragProfInfo.input_teamsize.setVisibility(View.GONE);
                                        fragProfInfo.input_doe.setVisibility(View.GONE);
                                        fragProfInfo.input_ceo.setVisibility(View.VISIBLE);
                                        //fragProfInfo.input_cto.setVisibility(View.VISIBLE);
                                        fragProfInfo.input_dob.setVisibility(View.GONE);


                                    } else if (createProfileData.getType() == Constants.TYPE_PROFILE_CELEBRITY) {
                                        fragProfInfo.linear_founder.setVisibility(View.GONE);
                                        fragProfInfo.input_teamsize.setVisibility(View.GONE);
                                        fragProfInfo.input_doe.setVisibility(View.GONE);
                                        fragProfInfo.input_ceo.setVisibility(View.GONE);
                                        //fragProfInfo.input_cto.setVisibility(View.GONE);
                                        fragProfInfo.input_dob.setVisibility(View.VISIBLE);
                                    }
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onErrorResponse(VolleyError error) {
                            VolleyApiRequest.showVolleyError(context, error);
                        }
                    }).enqueRequest(jsonObject, Request.Method.POST);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return createProfileData;
    }

    OnDoneButton onDoneButton = new OnDoneButton() {
        @Override
        public void onDone() {

        }
    };

    @Override

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == 1596) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Fragment fragment = mPagerAdapter.getItem(mPager.getCurrentItem());
                /*if (fragment instanceof FragProfInfo) {
                    FragProfInfo profileTypeName = (FragProfInfo) fragment;
                    profileTypeName.launchCamera();
                }*/
            } else {
                Toast.makeText(CreateProfileActivity.this, "permission required to open camera", Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == RESULT_CODE_SUCCESS) {
            setResult(RESULT_OK);
            CreateProfileActivity.this.finish();
        } else {
            Fragment fragment = mPagerAdapter.getItem(mPager.getCurrentItem());
            if (fragment instanceof FragProfInfo) {
                FragProfInfo fragProfInfo = (FragProfInfo) fragment;
                fragProfInfo.onActivityResult(requestCode, resultCode, data);
            }
            Log.e("Data", "" + data);
        }
    }

    public void submitData(JSONObject jsonObject) {
        Log.e(Constants.LOG_REQUESTDATA, "" + jsonObject);
        new VolleyApiRequest(context, true, VolleyApiRequest.REQUEST_BASEURL + VolleyApiRequest.API_CREATE_PROFILE,
                new VolleyCallBack() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e(Constants.LOG_RESPONSEDATA, "" + response);
                        try {
                            ModelCreateProfile modelCreateProfile = new Gson().fromJson(response.toString(), ModelCreateProfile.class);
                            if (modelCreateProfile.getStatus() == 200) {

                                if (modelCreateProfile.getData() != null) {

                                    MyProfiles myProfiles = new MyProfiles();
                                    CreateProfileData createProfileData = modelCreateProfile.getData();

                                    myProfiles.setId("" + createProfileData.getId());
                                    myProfiles.setEmployee_unique_code("" + createProfileData.getEmployee_unique_code());
                                    myProfiles.setUser_id("" + createProfileData.getUserId());
                                    myProfiles.setType("" + createProfileData.getType());
                                    myProfiles.setName(createProfileData.getName());
                                    myProfiles.setAddress(createProfileData.getAddress());
                                    myProfiles.setLatitude(createProfileData.getLatitude());
                                    myProfiles.setLongitude(createProfileData.getLongitude());
                                    myProfiles.setTag("" + createProfileData.getTag());
                                    myProfiles.setSub_tag("" + createProfileData.getSubTag());
                                    myProfiles.setLogo(createProfileData.getLogo());
                                    myProfiles.setAbout(createProfileData.getAbout());
                                    myProfiles.setCompany_team_size("" + createProfileData.getCompanyTeamSize());
                                    myProfiles.setEstablish_date(createProfileData.getEstablishDate());
                                    myProfiles.setUnique_code(createProfileData.getUniqueCode());
                                    myProfiles.setCurrent_ceo(createProfileData.getCurrent_ceo());
                                    myProfiles.setCurrent_cfo(createProfileData.getCurrent_cfo());
                                    myProfiles.setDob(createProfileData.getDob());

                                    String social_media = new Gson().toJson(createProfileData.getSocialMedia());
                                    myProfiles.setSocial_media(social_media);

                                    String social_phone = new Gson().toJson(createProfileData.getPhone());
                                    myProfiles.setPhone(social_phone);

                                    String social_email = new Gson().toJson(createProfileData.getEmail());
                                    myProfiles.setEmail(social_email);

                                    String social_weblink = new Gson().toJson(createProfileData.getWeblink());
                                    myProfiles.setWeblink(social_weblink);

                                    String social_founder = new Gson().toJson(createProfileData.getFounders());
                                    myProfiles.setFounders(social_founder);

                                    RealmController.with(CreateProfileActivity.this).updateMyProfiles(myProfiles);

                                    /*if (myProfiles.getType() != null && myProfiles.getType().equalsIgnoreCase("1")) {
                                        startServiceforEmployeeProfile(myProfiles.getEmployee_unique_code() + "", myProfiles.getUser_id() + "", myProfiles.getId() + "");
                                    }*/

                                    Intent intent = new Intent(context, UpdateQkTagActivity.class);
                                    intent.putExtra(Constants.KEY_FROM, Constants.KEY_FROM_CREATEPROFILE);
                                    intent.putExtra(Constants.KEY_UNQUECODE, myProfiles.getUnique_code());
                                    intent.putExtra(Constants.KEY_PROFILETYPE, myProfiles.getType() + "");
                                    intent.putExtra(Constants.KEY_EMPLOYEE_UNIQUE_CODE, myProfiles.getEmployee_unique_code() + "");
                                    intent.putExtra(Constants.KEY_CREATEPROFILEID, myProfiles.getId() + "");
                                    intent.putExtra(Constants.KEY_CREATEPROFILEID, myProfiles.getId() + "");
                                    intent.putExtra(Constants.KEY_CREATE_COMPONY_PROFILE, "1");
                                    intent.putExtra(Constants.KEY_CREATE_COMPONY_PROFILE_NAME, createProfileData.getName() + "");
                                    intent.putExtra(Constants.KEY_CREATE_COMPONY_PROFILE_IMAGE, createProfileData.getLogo() + "");
                                    intent.putExtra(Constants.KEY_CREATE_COMPONY_PROFILE_ID, createProfileData.getId() + "");
                                    intent.putExtra(Constants.KEY_CREATE_COMPONY_PROFILE_ABOUT, createProfileData.getAbout() + "");
                                    startActivityForResult(intent, RESULT_CODE_SUCCESS);
                                }

                            } else Utilities.showToast(context, modelCreateProfile.getMsg());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //VolleyApiRequest.showVolleyError(context, error);
                    }
                }).enqueRequest(jsonObject, Request.Method.POST);
    }

}