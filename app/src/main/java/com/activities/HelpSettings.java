package com.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import com.models.logindata.ModelLoggedUser;
import com.quickkonnect.QuickKonnect_Webview;
import com.quickkonnect.R;
import com.retrofitservice.ApiUtils;
import com.retrofitservice.SOService;
import com.utilities.Constants;
import com.utilities.QKPreferences;
import com.utilities.Utilities;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ZTLAB-10 on 17-03-2018.
 */

public class HelpSettings extends AppCompatActivity {

    private Context context;
    private QKPreferences qkPreferences;
    private SOService soService;
    private ModelLoggedUser modelLoggedUser;
    private int status_notification = 0;
    private boolean status_isprivacy = false;
    private RecyclerView recycleHelpSettings;
    private AdapterHelpSetting adapterHelpSetting;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.helpaccountsetting);
        Utilities.ChangeStatusBar(HelpSettings.this);
        try {
            Toolbar toolbar = findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
            android.support.v7.app.ActionBar actionBar = getSupportActionBar();
            actionBar.setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close_white_24dp);
        } catch (Exception e) {
            e.printStackTrace();
        }

        context = HelpSettings.this;

        soService = ApiUtils.getSOService();
        qkPreferences = QKPreferences.getInstance(context);
        modelLoggedUser = qkPreferences.getLoggedUser();
        status_notification = modelLoggedUser.getData().getNotificationStatus();
        status_isprivacy = modelLoggedUser.getData().isIs_public();

        initWidget();
    }

    public void initWidget() {

        recycleHelpSettings = findViewById(R.id.recycleHelpSettings);
        recycleHelpSettings.setLayoutManager(new LinearLayoutManager(this));
        setupHelpSettings();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                HelpSettings.this.finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void setupHelpSettings() {

        List<ModelHelpSettings> helpSettings = new ArrayList<>();

        ModelHelpSettings modelHelpSettings = new ModelHelpSettings();

        modelHelpSettings.setIcon(R.drawable.ic_public_visible);
        modelHelpSettings.setTitle(getResources().getString(R.string.helpsetting_isPrivate));
        modelHelpSettings.setType(TYPE_PUBLICVISIBILITY);
        modelHelpSettings.setSwitchOnoff(status_isprivacy);
        helpSettings.add(modelHelpSettings);

        modelHelpSettings = new ModelHelpSettings();
        modelHelpSettings.setIcon(R.drawable.noti_setting);
        modelHelpSettings.setTitle(getResources().getString(R.string.bottom_nav_title_notis));
        if (status_notification == 1)
            modelHelpSettings.setSwitchOnoff(true);
        modelHelpSettings.setType(TYPE_NOTIFICATION);
        helpSettings.add(modelHelpSettings);

        modelHelpSettings = new ModelHelpSettings();
        modelHelpSettings.setIcon(R.drawable.helpcenter);
        modelHelpSettings.setTitle(getResources().getString(R.string.help_center));
        modelHelpSettings.setType(TYPE_HELPCENTER);
        helpSettings.add(modelHelpSettings);

        modelHelpSettings = new ModelHelpSettings();
        modelHelpSettings.setIcon(R.drawable.report_problem);
        modelHelpSettings.setTitle(getResources().getString(R.string.report_problem));
        modelHelpSettings.setType(TYPE_REPORTPROBLEM);
        helpSettings.add(modelHelpSettings);

        adapterHelpSetting = new AdapterHelpSetting(HelpSettings.this, helpSettings);
        recycleHelpSettings.setAdapter(adapterHelpSetting);
    }


    public class AdapterHelpSetting extends RecyclerView.Adapter<AdapterHelpSetting.HelpSettingHolder> {

        private Context context;
        private List<ModelHelpSettings> helpSettings;

        public AdapterHelpSetting(Context context, List<ModelHelpSettings> helpSettings) {
            this.context = context;
            this.helpSettings = helpSettings;
        }

        @NonNull
        @Override

        public HelpSettingHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(context).inflate(R.layout.item_helpsetting, parent, false);
            return new HelpSettingHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull HelpSettingHolder holder, final int position) {

            holder.imgIcon.setImageResource(helpSettings.get(position).getIcon());
            holder.tvTitle.setText(helpSettings.get(position).getTitle());

            final int type = helpSettings.get(position).getType();
            if (type == TYPE_NOTIFICATION || type == TYPE_PUBLICVISIBILITY) {
                holder.switchOnOff.setVisibility(View.VISIBLE);
                holder.switchOnOff.setChecked(helpSettings.get(position).isSwitchOnoff);
                holder.switchOnOff.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                        if (type == TYPE_NOTIFICATION) {
                            notificationStatusChange(b ? 1 : 0);

                        } else if (type == TYPE_PUBLICVISIBILITY) {
                            privacyStatusChange(b ? 1 : 0);
                        }
                    }
                });
            } else
                holder.switchOnOff.setVisibility(View.INVISIBLE);

            holder.tvTitle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    switch (helpSettings.get(position).getType()) {
                        case TYPE_HELPCENTER:
                            startActivity(new Intent(HelpSettings.this, QuickKonnect_Webview.class)
                                    .putExtra("help_center", Constants.URL_HELP));
                            break;
                        case TYPE_REPORTPROBLEM:
                            startActivity(new Intent(HelpSettings.this, ReportaProblemActivity.class));
                            break;
                    }
                }
            });
        }

        @Override
        public int getItemCount() {
            return helpSettings.size();
        }

        public class HelpSettingHolder extends RecyclerView.ViewHolder {

            ImageView imgIcon;
            TextView tvTitle;
            Switch switchOnOff;

            public HelpSettingHolder(View itemView) {
                super(itemView);
                imgIcon = itemView.findViewById(R.id.imgIcon);
                tvTitle = itemView.findViewById(R.id.tvTitle);
                switchOnOff = itemView.findViewById(R.id.switchOnOff);
            }
        }
    }

    public static final int TYPE_PUBLICVISIBILITY = 1;
    public static final int TYPE_NOTIFICATION = 2;
    public static final int TYPE_HELPCENTER = 3;
    public static final int TYPE_REPORTPROBLEM = 4;

    public class ModelHelpSettings {

        private int icon;
        private boolean isSwitchOnoff = false;
        private String title;
        private int type;

        public int getIcon() {
            return icon;
        }

        public void setIcon(int icon) {
            this.icon = icon;
        }

        public boolean isSwitchOnoff() {
            return isSwitchOnoff;
        }

        public void setSwitchOnoff(boolean switchOnoff) {
            isSwitchOnoff = switchOnoff;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public int getType() {
            return type;
        }

        public void setType(int type) {
            this.type = type;
        }
    }


    public void notificationStatusChange(final int notification_status) {

        String user_id = qkPreferences.getLoggedUserid();
        String auth_token = String.valueOf(qkPreferences.getApiToken());

        Map<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", "Bearer " + auth_token);
        headers.put("Accept", "application/json");

        final ProgressDialog progressDialog = Utilities.showProgress(context);

        soService.changNotificationStatus(headers, user_id, String.valueOf(notification_status)).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();
                try {

                    JSONObject jsonObject = new JSONObject(response.body().string());
                    String message = jsonObject.optString("msg");
                    Utilities.showToast(HelpSettings.this, message);

                    String status = jsonObject.optString("status");
                    if (status != null && !status.isEmpty() && status.equalsIgnoreCase("200")) {
                        modelLoggedUser.getData().setNotificationStatus(notification_status);
                        qkPreferences.storeLoggedUser(modelLoggedUser);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();
            }
        });
    }

    public void privacyStatusChange(final int privacy_status) {

        String statusPublic = Constants.PRIVACY_STATUS_PUBLIC;
        if (privacy_status == 1)
            statusPublic = Constants.PRIVACY_STATUS_PUBLIC;
        else
            statusPublic = Constants.PRIVACY_STATUS_PRIVATE;

        String user_id = qkPreferences.getLoggedUserid();
        String auth_token = String.valueOf(qkPreferences.getApiToken());

        Map<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", "Bearer " + auth_token);
        headers.put("Accept", "application/json");

        final ProgressDialog progressDialog = Utilities.showProgress(context);

        soService.updatePrivacyStatus(headers, user_id, statusPublic).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();
                try {

                    JSONObject jsonObject = new JSONObject(response.body().string());
                    String message = jsonObject.optString("msg");
                    Utilities.showToast(HelpSettings.this, message);

                    String status = jsonObject.optString("status");
                    if (status != null && !status.isEmpty() && status.equalsIgnoreCase("200")) {
                        ModelLoggedUser modelLoggedUser = qkPreferences.getLoggedUser();
                        if (privacy_status == 0)
                            modelLoggedUser.getData().setIs_public(false);
                        else
                            modelLoggedUser.getData().setIs_public(true);
                        qkPreferences.storeLoggedUser(modelLoggedUser);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();
            }
        });
    }
}
