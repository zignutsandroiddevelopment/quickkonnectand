package com.activities;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Patterns;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import com.models.logindata.ModelLoggedUser;
import com.models.logindata.PublicationDetail;
import com.quickkonnect.R;
import com.retrofitservice.ApiUtils;
import com.retrofitservice.SOService;
import com.utilities.Constants;
import com.utilities.QKPreferences;
import com.utilities.Utilities;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Zignuts Technolab pvt. ltd on 17-03-2018.
 */

public class UpdatePublicationActivity extends AppCompatActivity implements View.OnClickListener {

    private SOService soService;
    private String userid;
    private EditText edt_publication_title, edt_publication_publishername, edt_publication_enddate, edt_publication_url;
    private Button btn_publication_save;
    private TextView tv_delete;
    private int mYear, mMonth, mDay;
    private Calendar c;
    private QKPreferences qkPreferences;
    private ModelLoggedUser modelUserProfile;
    private PublicationDetail publicationDetail;
    private Context context;
    boolean isEdit = false;
    private String prevPubtitle, prevPublisher, prevPubDate, prevPublicationurl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_publication);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Utilities.ChangeStatusBar(UpdatePublicationActivity.this);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close_white_24dp);

        soService = ApiUtils.getSOService();

        try {
            Bundle b = getIntent().getExtras();
            if (b.getString(Constants.KEY_UPDATE).equalsIgnoreCase(Constants.ADD_DATA)) {
                getSupportActionBar().setTitle(getResources().getString(R.string.addpublication));
            } else if (b.getString(Constants.KEY_UPDATE).equalsIgnoreCase(Constants.EDIT_DATA)) {
                getSupportActionBar().setTitle(getResources().getString(R.string.updatepublication));
                isEdit = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        context = UpdatePublicationActivity.this;
        qkPreferences = new QKPreferences(context);
        userid = "" + qkPreferences.getLoggedUser().getData().getId();

        modelUserProfile = qkPreferences.getLoggedUser();

        edt_publication_title = findViewById(R.id.edt_publication_title);
        edt_publication_url = findViewById(R.id.edt_publication_url);
        edt_publication_publishername = findViewById(R.id.edt_publication_publishername);
        edt_publication_enddate = findViewById(R.id.edt_publication_enddate);
        btn_publication_save = findViewById(R.id.btn_publication_save);
        tv_delete = findViewById(R.id.tv_delete);

        btn_publication_save.setOnClickListener(this);
        tv_delete.setOnClickListener(this);
        edt_publication_enddate.setOnClickListener(this);

        c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        Bundle extras = getIntent().getExtras();

        if (isEdit) {

            publicationDetail = extras.getParcelable("publication");

            if (publicationDetail != null && publicationDetail.getPublicationTitle() != null) {
                prevPubtitle = publicationDetail.getPublicationTitle();
                edt_publication_title.setText(publicationDetail.getPublicationTitle());
            }

            if (publicationDetail != null && publicationDetail.getPublisherName() != null) {
                prevPublisher = publicationDetail.getPublisherName();
                edt_publication_publishername.setText(publicationDetail.getPublisherName());
            }

            if (publicationDetail != null && publicationDetail.getPublicationUrl() != null && Utilities.isEmpty(publicationDetail.getPublicationUrl())) {
                prevPublicationurl = publicationDetail.getPublicationUrl();
                edt_publication_url.setText(publicationDetail.getPublicationUrl());
            }

            if (publicationDetail != null && publicationDetail.getDate() != null && !publicationDetail.getDate().equals("null")) {
                prevPubDate = publicationDetail.getDate();
                edt_publication_enddate.setText(publicationDetail.getDate());
            }

            btn_publication_save.setText(getResources().getString(R.string.update));

            tv_delete.setVisibility(View.VISIBLE);
        }
    }


    @Override
    public void onClick(final View view) {

        switch (view.getId()) {
            case R.id.tv_delete:
                new AlertDialog.Builder(this)
                        .setMessage(getResources().getString(R.string.delete_confirmation_message_publication))
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int whichButton) {

                                if (!Utilities.isNetworkConnected(UpdatePublicationActivity.this))
                                    Utilities.showToast(context, getResources().getString(R.string.no_internat_connection));

                                else {
                                    Utilities.hideKeyboard(UpdatePublicationActivity.this);
                                    deletePublication(publicationDetail.getPublicationId());
                                }
                            }
                        })
                        .setNegativeButton(android.R.string.no, null).show();
                break;

            case R.id.edt_publication_enddate:

                String editextDate = edt_publication_enddate.getText().toString().trim();

                if (editextDate != null && !editextDate.isEmpty() && !editextDate.equals("null")) {


                    String[] parts = editextDate.split("/");
                    mDay = Integer.parseInt(parts[0]);
                    mMonth = Integer.parseInt(parts[1]) - 1;
                    mYear = Integer.parseInt(parts[2]);
                }


                DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {


                                int month = monthOfYear + 1;
                                String fm = "" + month;
                                String fd = "" + dayOfMonth;
                                if (month < 10) {
                                    fm = "0" + month;
                                }
                                if (dayOfMonth < 10) {
                                    fd = "0" + dayOfMonth;
                                }

                                edt_publication_enddate.setText(fd + "/" + fm + "/" + year);


                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.getDatePicker().setMaxDate(c.getTimeInMillis());
                datePickerDialog.show();
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.custom_save_update, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {

            case android.R.id.home:
                exitfromActivity();
                return true;

            case R.id.ic_save_update:

                Utilities.hideKeyboard(UpdatePublicationActivity.this);

                if (edt_publication_title.getText().toString().trim() == "" || edt_publication_title.getText().toString().trim().equals("") || edt_publication_title.getText().toString().trim().equals("null"))
                    Utilities.showToast(context, getResources().getString(R.string.publicationtitlerequired));

                else if (edt_publication_publishername.getText().toString().trim() == "" || edt_publication_publishername.getText().toString().trim().equals("") || edt_publication_publishername.getText().toString().trim().equals("null"))
                    Utilities.showToast(context, getResources().getString(R.string.publicationnameisrequired));

                else if (edt_publication_enddate.getText().toString().trim() == "" || edt_publication_enddate.getText().toString().trim().equals("") || edt_publication_enddate.getText().toString().trim().equals("null"))
                    Utilities.showToast(context, getResources().getString(R.string.publisheddaterequired));

                else if (!edt_publication_url.getText().toString().isEmpty() && !edt_publication_url.getText().toString().equalsIgnoreCase("") && !isValidUrl(edt_publication_url.getText().toString()))
                    Utilities.showToast(context, getResources().getString(R.string.publicationinvalidurl));

                else {

                    String startdate = edt_publication_enddate.getText().toString().trim();

                    String finalstartdate;

                    if (startdate == "" || startdate.equals("") || startdate.equals("null")) {
                        finalstartdate = "";

                    } else {

                        String[] separated = startdate.split("/");
                        String day = separated[0]; // this will contain "Fruit"
                        String month = separated[1];
                        String year = separated[2];

                        finalstartdate = year + "-" + month + "-" + day;
                    }

                    if (!Utilities.isNetworkConnected(this))
                        Utilities.showToast(context, getResources().getString(R.string.no_internat_connection));

                    else {

                        if (publicationDetail != null && publicationDetail.getPublicationId() != null)
                            editPublication(publicationDetail.getPublicationId(), edt_publication_title.getText().toString().trim(), edt_publication_publishername.getText().toString().trim(), finalstartdate, edt_publication_url.getText().toString());

                        else
                            addPublication(userid, edt_publication_title.getText().toString().trim(), edt_publication_publishername.getText().toString().trim(), finalstartdate, edt_publication_url.getText().toString());
                    }
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        exitfromActivity();
    }

    /**
     * A alert message to either discard changes or save it before leave page
     */
    public void exitfromActivity() {
        if (isFoundanyupdate()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(UpdatePublicationActivity.this);
            builder.setTitle(getResources().getString(R.string.alert));
            builder.setMessage(getResources().getString(R.string.exitwithoutsave));
            builder.setPositiveButton(getResources().getString(R.string.exit), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    Utilities.hideKeyboard(UpdatePublicationActivity.this);
                    setResult(RESULT_CANCELED);
                    UpdatePublicationActivity.this.finish();
                }
            });
            builder.setNegativeButton(getResources().getString(R.string.close), null);
            builder.show();

        } else {

            Utilities.hideKeyboard(UpdatePublicationActivity.this);
            setResult(RESULT_CANCELED);
            UpdatePublicationActivity.this.finish();
        }
    }

    /**
     * if any edits(changes by user) found while review the publication details, it will ask use either user wants to discard
     * it or save it
     *
     * @return
     */
    public boolean isFoundanyupdate() {

        String title = edt_publication_title.getText().toString();
        String publishername = edt_publication_publishername.getText().toString();
        String enddate = edt_publication_enddate.getText().toString();
        String url = edt_publication_url.getText().toString();

        if (prevPubtitle != null && !prevPubtitle.equals(title)) {
            return true;

        } else if (prevPublisher != null && !prevPublisher.equals(publishername)) {
            return true;

        } else if (prevPubDate != null && !prevPubDate.equals(enddate)) {
            return true;

        } else if (prevPublicationurl != null && !prevPublicationurl.equals(url)) {
            return true;
        }

        return false;
    }

    /**
     * Check if the entered url is valid or not http or https url (web url)
     *
     * @param url
     * @return
     */
    private boolean isValidUrl(String url) {
        Pattern p = Patterns.WEB_URL;
        Matcher m = p.matcher(url.toLowerCase());
        if (m.matches())
            return true;
        else
            return false;
    }

    /**
     * Add new publication
     *
     * @param user_id
     * @param publication_title
     * @param publisher_name
     * @param publication_date
     * @param publication_url
     */
    public void addPublication(String user_id, final String publication_title, final String publisher_name, final String publication_date, final String publication_url) {
        Map<String, String> headerParams = Utilities.getHeaderParameter(context);
        final ProgressDialog progressDialog = Utilities.showProgress(context);
        soService.addPublication(headerParams, user_id, publication_title, publisher_name, publication_date, publication_url).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Utilities.hideProgressDialog(progressDialog);
                try {

                    if (response == null || response.body() == null) {
                        Utilities.showToast(context, getResources().getString(R.string.unknownerror));
                        return;
                    }

                    JSONObject jsonObject = new JSONObject(response.body().string());

                    String status = jsonObject.getString("status");
                    String message = jsonObject.getString("msg");

                    if (status.equals(ApiUtils.RESPONSE_CODE_SUCCESS)) {

                        List<PublicationDetail> publicationDetailList = modelUserProfile.getData().getPublicationDetail();
                        if (publicationDetailList == null)
                            publicationDetailList = new ArrayList<>();

                        PublicationDetail publicationDetail = new PublicationDetail();

                        JSONObject object = jsonObject.getJSONObject("data");

                        publicationDetail.setPublicationTitle(object.getString("publication_title"));
                        publicationDetail.setPublicationId(object.getInt("publication_id"));
                        publicationDetail.setPublisherName(object.getString("publisher_name"));
                        publicationDetail.setPublicationUrl(object.getString("publication_url"));
                        publicationDetail.setDate(object.getString("date"));

                        publicationDetailList.add(publicationDetail);

                        modelUserProfile.getData().setPublicationDetail(publicationDetailList);

                        qkPreferences.storeLoggedUser(modelUserProfile);

                        setResult(RESULT_OK);
                        UpdatePublicationActivity.this.finish();

                    } else Utilities.showToast(context, message);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Utilities.hideProgressDialog(progressDialog);
                Utilities.showToast(context, t.getLocalizedMessage());
            }
        });
    }

    /**
     * Edit existing publication
     *
     * @param publication_id
     * @param publication_title
     * @param publisher_name
     * @param publication_date
     * @param publication_url
     */
    public void editPublication(int publication_id, final String publication_title, final String publisher_name, final String publication_date, final String publication_url) {
        Map<String, String> headerParams = Utilities.getHeaderParameter(context);
        final ProgressDialog progressDialog = Utilities.showProgress(context);
        soService.editPublication(headerParams, publication_id, publication_title, publisher_name, publication_date, publication_url).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Utilities.hideProgressDialog(progressDialog);
                try {

                    if (response == null || response.body() == null) {
                        Utilities.showToast(context, getResources().getString(R.string.unknownerror));
                        return;
                    }

                    JSONObject jsonObject = new JSONObject(response.body().string());

                    String responseStatus = jsonObject.getString("status");
                    String msg = jsonObject.getString("msg");

                    if (responseStatus.equals(ApiUtils.RESPONSE_CODE_SUCCESS)) {

                        if (modelUserProfile != null) {

                            List<PublicationDetail> publicationDetailList = modelUserProfile.getData().getPublicationDetail();
                            int index = 0;
                            for (PublicationDetail publicationDetails: publicationDetailList) {
                                if (publicationDetails.getPublicationId() != null &&
                                        publicationDetails.getPublicationId().equals(publicationDetail.getPublicationId())) {
                                    index = publicationDetailList.indexOf(publicationDetails);
                                    break;
                                }
                            }

                            JSONObject object = jsonObject.getJSONObject("data");

                            publicationDetail.setPublicationTitle(object.getString("publication_title"));
                            publicationDetail.setPublisherName(object.getString("publisher_name"));
                            publicationDetail.setPublicationUrl(object.getString("publication_url"));
                            publicationDetail.setDate(object.getString("date"));

                            publicationDetailList.set(index, publicationDetail);

                            modelUserProfile.getData().setPublicationDetail(publicationDetailList);

                            qkPreferences.storeLoggedUser(modelUserProfile);

                            setResult(RESULT_OK);
                            UpdatePublicationActivity.this.finish();
                        }

                    } else Utilities.showToast(context, msg);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Utilities.hideProgressDialog(progressDialog);
                Utilities.showToast(context, t.getLocalizedMessage());
            }
        });
    }

    /**
     * Delete publication
     *
     * @param publication_id
     */
    public void deletePublication(int publication_id) {
        Map<String, String> headerParams = Utilities.getHeaderParameter(context);
        final ProgressDialog progressDialog = Utilities.showProgress(context);
        soService.deletePublication(headerParams, publication_id).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Utilities.hideProgressDialog(progressDialog);
                try {

                    if (response == null || response.body() == null) {
                        Utilities.showToast(context, getResources().getString(R.string.unknownerror));
                        return;
                    }

                    JSONObject jsonObject = new JSONObject(response.body().string());

                    String responseStatus = jsonObject.getString("status");

                    String msg = jsonObject.getString("msg");

                    if (responseStatus.equals(ApiUtils.RESPONSE_CODE_SUCCESS)) {

                        List<PublicationDetail> publicationDetailList = modelUserProfile.getData().getPublicationDetail();

                        for (int i = 0; i < publicationDetailList.size(); i++) {
                            if (publicationDetailList.get(i).getPublicationId().equals(publicationDetail.getPublicationId())) {
                                publicationDetailList.remove(i);
                                break;
                            }
                        }

                        modelUserProfile.getData().setPublicationDetail(publicationDetailList);

                        qkPreferences.storeLoggedUser(modelUserProfile);

                        setResult(RESULT_OK);
                        UpdatePublicationActivity.this.finish();

                    } else
                        Utilities.showToast(context, msg);


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Utilities.hideProgressDialog(progressDialog);
                Utilities.showToast(context, t.getLocalizedMessage());
            }
        });
    }
}