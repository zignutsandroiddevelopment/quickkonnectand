package com.OpenFire;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.OpenFire.entry.ContactEntry;
import com.OpenFire.entry.RoasterLogic;
import com.OpenFire.entry.XMPPLogic;
import com.OpenFire.interfaces.ChatNewMessagelistener;
import com.OpenFire.interfaces.PresenceChangesListener;
import com.OpenFire.interfaces.ReconnectListener;
import com.OpenFire.interfaces.RosterChangeListener;
import com.OpenFire.interfaces.XmppConnetionListener;
import com.OpenFire.interfaces.XmppLoginListener;
import com.OpenFire.network.PingPacketFilter;
import com.OpenFire.network.PingPacketListener;
import com.OpenFire.network.PingProvider;
import com.utilities.QKPreferences;

import org.jivesoftware.smack.Chat;
import org.jivesoftware.smack.ChatManager;
import org.jivesoftware.smack.ChatManagerListener;
import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.MessageListener;
import org.jivesoftware.smack.Roster;
import org.jivesoftware.smack.RosterEntry;
import org.jivesoftware.smack.RosterListener;
import org.jivesoftware.smack.SASLAuthentication;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smack.provider.ProviderManager;
import org.jivesoftware.smackx.GroupChatInvitation;
import org.jivesoftware.smackx.PrivateDataManager;
import org.jivesoftware.smackx.packet.ChatStateExtension;
import org.jivesoftware.smackx.packet.LastActivity;
import org.jivesoftware.smackx.packet.OfflineMessageInfo;
import org.jivesoftware.smackx.packet.OfflineMessageRequest;
import org.jivesoftware.smackx.packet.SharedGroupsInfo;
import org.jivesoftware.smackx.packet.VCard;
import org.jivesoftware.smackx.provider.BytestreamsProvider;
import org.jivesoftware.smackx.provider.DataFormProvider;
import org.jivesoftware.smackx.provider.DelayInformationProvider;
import org.jivesoftware.smackx.provider.DiscoverInfoProvider;
import org.jivesoftware.smackx.provider.DiscoverItemsProvider;
import org.jivesoftware.smackx.provider.IBBProviders;
import org.jivesoftware.smackx.provider.MUCAdminProvider;
import org.jivesoftware.smackx.provider.MUCOwnerProvider;
import org.jivesoftware.smackx.provider.MUCUserProvider;
import org.jivesoftware.smackx.provider.MessageEventProvider;
import org.jivesoftware.smackx.provider.MultipleAddressesProvider;
import org.jivesoftware.smackx.provider.RosterExchangeProvider;
import org.jivesoftware.smackx.provider.StreamInitiationProvider;
import org.jivesoftware.smackx.provider.VCardProvider;
import org.jivesoftware.smackx.provider.XHTMLExtensionProvider;
import org.jivesoftware.smackx.search.UserSearch;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by ZTLAB-12 on 16-07-2018.
 */

public class XmppConnectionUtil {

    //openFile Chat
    public static String SERVER_IP = "openfire.quickkonnect.com";
    public static int SERVER_PORT = 5222;
    public static String USER_PASSWORD = "123456";
    public static String USER_RESOURCE = "Android";

    private static String LOG_TAG = "- From BackgroundService of chat -";
    public static XMPPConnection xmppConnection;
    private Roster userRoster;
    private ArrayList<ContactEntry> userContactEntries;
    private Context context;
    private QKPreferences qkPreferences;
    private boolean isConnected;
    private XmppConnetionListener connectionListener;
    private XmppLoginListener xmppLoginListener;
    private static ChatNewMessagelistener chatNewMessageistener;
    private static RosterChangeListener rosterChangeListener;
    private static PresenceChangesListener presenceChangesListener;

    public XmppConnectionUtil(Context context) {
        this.context = context;
        userContactEntries = new ArrayList<ContactEntry>();
        qkPreferences = new QKPreferences(context);
    }

    public XmppConnectionUtil(ChatNewMessagelistener chatNewMessageistener, RosterChangeListener rosterChangeListener) {
        this.chatNewMessageistener = chatNewMessageistener;
        this.rosterChangeListener = rosterChangeListener;
    }

    public void Connect(XmppConnetionListener conlistener) {
        this.connectionListener = conlistener;
        try {
            @SuppressLint("StaticFieldLeak")
            AsyncTask<Void, Void, Boolean> connectionThread = new AsyncTask<Void, Void, Boolean>() {
                @Override
                protected synchronized Boolean doInBackground(Void... arg0) {
                    try {
                        // Setup required features
                        configure(ProviderManager.getInstance());
                        // Setup connection configuration
                        ConnectionConfiguration config = new ConnectionConfiguration(SERVER_IP, SERVER_PORT);
                        config.setSASLAuthenticationEnabled(true);
                        config.setSendPresence(false);
                        SASLAuthentication.supportSASLMechanism("PLAIN", 0);

                        // Setup XMPP connection from configuration
                        xmppConnection = new XMPPConnection(config);
                        xmppConnection.connect();
                        isConnected = true;
                        Log.i(LOG_TAG, "Connected to server.");

                        XMPPLogic.getInstance().setConnection(xmppConnection);

                        connectionListener.onConnectionSuccessfull("Success.");
                    } catch (Exception e) {
                        isConnected = false;
                        Log.e(LOG_TAG, "Unable to connect server: " + e.getMessage());
                        reconnectToXmpp();
                        connectionListener.onConnectionFailure("Fail to connect to xmpp");
                    }
                    return null;
                }
            };
            connectionThread.execute();
        } catch (Exception e) {
            e.printStackTrace();
            reconnectToXmpp();
            connectionListener.onConnectionFailure("Fail to connect to xmpp");
        }
    }

    public void reconnectToXmpp() {
        try {
            @SuppressLint("StaticFieldLeak")
            AsyncTask<Void, Void, Boolean> connectionThread = new AsyncTask<Void, Void, Boolean>() {
                @Override
                protected synchronized Boolean doInBackground(Void... arg0) {
                    try {
                        try {
                            xmppConnection.disconnect();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        configure(ProviderManager.getInstance());
                        ConnectionConfiguration config = new ConnectionConfiguration(SERVER_IP, SERVER_PORT);
                        config.setSASLAuthenticationEnabled(true);
                        config.setSendPresence(false);
                        SASLAuthentication.supportSASLMechanism("PLAIN", 0);
                        xmppConnection = new XMPPConnection(config);
                        xmppConnection.connect();
                        isConnected = true;
                        Log.i(LOG_TAG, "Connected to server.");
                        XMPPLogic.getInstance().setConnection(xmppConnection);
                        String Uniquecode = qkPreferences.getValues(QKPreferences.USER_UNIQUECODE) + "";
                        if (Uniquecode != null && !Uniquecode.equalsIgnoreCase("") && !Uniquecode.equalsIgnoreCase("null")) {

                            xmppConnection.login(Uniquecode + "", USER_PASSWORD + "", USER_RESOURCE + "");

                            Log.i(LOG_TAG, "Logged in to connect server.");
                            if (xmppConnection.isConnected()) {
                                xmppConnection.addPacketListener(new PingPacketListener(xmppConnection), new PingPacketFilter());
                            }
                            // Send package to server to my status to "Available" (Online)
                            Presence userPresence = new Presence(Presence.Type.available);
                            xmppConnection.sendPacket(userPresence);
                            setRoster();
                        }
                    } catch (XMPPException e) {
                        e.printStackTrace();
                    }
                    return null;
                }
            };
            connectionThread.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void reconnectToXmpp(final ReconnectListener reconnectListener) {
        try {


            @SuppressLint("StaticFieldLeak")
            AsyncTask<Void, Void, Boolean> loginThread = new AsyncTask<Void, Void, Boolean>() {
                @Override
                protected synchronized Boolean doInBackground(Void... arg0) {
                    try {
                        xmppConnection.disconnect();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    configure(ProviderManager.getInstance());
                    ConnectionConfiguration config = new ConnectionConfiguration(SERVER_IP, SERVER_PORT);
                    config.setSASLAuthenticationEnabled(true);
                    config.setSendPresence(false);
                    SASLAuthentication.supportSASLMechanism("PLAIN", 0);
                    xmppConnection = new XMPPConnection(config);
                    try {
                        xmppConnection.connect();
                    } catch (XMPPException e) {
                        e.printStackTrace();
                    }
                    isConnected = true;
                    Log.i(LOG_TAG, "Connected to server.");
                    XMPPLogic.getInstance().setConnection(xmppConnection);
                    String Uniquecode = qkPreferences.getValues(QKPreferences.USER_UNIQUECODE) + "";
                    if (Uniquecode != null && !Uniquecode.equalsIgnoreCase("") && !Uniquecode.equalsIgnoreCase("null")) {
                        try {
                            xmppConnection.login(Uniquecode + "", USER_PASSWORD + "", USER_RESOURCE + "");
                            Log.i(LOG_TAG, "Logged in to connect server.");
                            if (xmppConnection.isConnected()) {
                                xmppConnection.addPacketListener(new PingPacketListener(xmppConnection), new PingPacketFilter());
                            }
                            // Send package to server to my status to "Available" (Online)
                            Presence userPresence = new Presence(Presence.Type.available);
                            xmppConnection.sendPacket(userPresence);
                            setRoster();
                            reconnectListener.onCalBack(1);

                        } catch (XMPPException e) {
                            reconnectListener.onCalBack(0);
                            e.printStackTrace();
                        }

                    }
                    return null;
                }
            };
            loginThread.execute();

        } catch (Exception e) {
            reconnectListener.onCalBack(0);
            e.printStackTrace();
        }
    }

    public void reconnectToXmppAndSendMessage(final ReconnectListener reconnectListener, final String message, final String fJID, final String NAME) {
        try {
            @SuppressLint("StaticFieldLeak")
            AsyncTask<Void, Void, Boolean> loginThread = new AsyncTask<Void, Void, Boolean>() {
                @Override
                protected synchronized Boolean doInBackground(Void... arg0) {
                    try {
                        xmppConnection.disconnect();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    configure(ProviderManager.getInstance());
                    ConnectionConfiguration config = new ConnectionConfiguration(SERVER_IP, SERVER_PORT);
                    config.setSASLAuthenticationEnabled(true);
                    config.setSendPresence(false);
                    SASLAuthentication.supportSASLMechanism("PLAIN", 0);
                    xmppConnection = new XMPPConnection(config);
                    try {
                        xmppConnection.connect();
                    } catch (XMPPException e) {
                        e.printStackTrace();
                    }
                    isConnected = true;
                    Log.i(LOG_TAG, "Connected to server.");
                    XMPPLogic.getInstance().setConnection(xmppConnection);
                    String Uniquecode = qkPreferences.getValues(QKPreferences.USER_UNIQUECODE) + "";
                    if (Uniquecode != null && !Uniquecode.equalsIgnoreCase("") && !Uniquecode.equalsIgnoreCase("null")) {
                        try {
                            xmppConnection.login(Uniquecode + "", USER_PASSWORD + "", USER_RESOURCE + "");
                            Log.i(LOG_TAG, "Logged in to connect server.");
                            if (xmppConnection.isConnected()) {
                                xmppConnection.addPacketListener(new PingPacketListener(xmppConnection), new PingPacketFilter());
                            }
                            // Send package to server to my status to "Available" (Online)
                            Presence userPresence = new Presence(Presence.Type.available);
                            xmppConnection.sendPacket(userPresence);

                            // todo have to seen once if user is not subsctibed giving error
                            /*Roster roster = xmppConnection.getRoster();
                            roster.createEntry(fJID, NAME + "", null);
                            roster.setSubscriptionMode(Roster.SubscriptionMode.accept_all);
                            Presence subscribed = new Presence(Presence.Type.subscribed);
                            subscribed.setTo(fJID + "");
                            xmppConnection.sendPacket(subscribed);*/

                            ChatManager chatManager = xmppConnection.getChatManager();
                            Chat chat = chatManager.createChat(fJID, new MessageListener() {
                                public void processMessage(Chat chat, final org.jivesoftware.smack.packet.Message chatMessage) {
                                    if (chatMessage.getBody() != null) {
                                        new Thread() {
                                            public void run() {
                                                android.os.Message msg = new android.os.Message();
                                                msg.what = 1;
                                                msg.obj = chatMessage;
                                            }
                                        }.start();
                                        //Log.i(LOG_TAG, "Received message from: " + chatMessage.getFrom() + ": " + chatMessage.getBody());
                                    }
                                }
                            });
                            chat.sendMessage(message);
                            reconnectListener.onCalBack(1);

                            AddChatManager();
                            AttachtoRosterListener();

                        } catch (XMPPException e) {
                            reconnectListener.onCalBack(0);
                            e.printStackTrace();
                        }
                    }
                    return null;
                }
            };
            loginThread.execute();
        } catch (Exception e) {
            reconnectListener.onCalBack(0);
            e.printStackTrace();
        }
    }

    public void Login(final String Uniquecode, XmppLoginListener loginlistener) {
        this.xmppLoginListener = loginlistener;
        try {
            @SuppressLint("StaticFieldLeak")
            AsyncTask<Void, Void, Boolean> loginThread = new AsyncTask<Void, Void, Boolean>() {
                @Override
                protected synchronized Boolean doInBackground(Void... arg0) {
                    try {
                        if (isConnected) {
                            if (Uniquecode != null && !Uniquecode.equalsIgnoreCase("") && !Uniquecode.equalsIgnoreCase("null")) {
                                xmppConnection.login(Uniquecode + "", USER_PASSWORD + "", USER_RESOURCE + "");
                                Log.i(LOG_TAG, "Logged in to connect server.");
                                if (xmppConnection.isConnected()) {
                                    xmppConnection.addPacketListener(new PingPacketListener(xmppConnection), new PingPacketFilter());
                                }
                                // Send package to server to my status to "Available" (Online)
                                Presence userPresence = new Presence(Presence.Type.available);
                                xmppConnection.sendPacket(userPresence);
                                setRoster();
                                xmppLoginListener.onLoginSuccess("Logged in to connect server.");

                            } else {
                                reconnectToXmpp();
                                xmppLoginListener.onloginFail("Not Connected to server.");
                            }
                        } else {
                            reconnectToXmpp();
                        }
                    } catch (Exception e) {
                        isConnected = false;
                        reconnectToXmpp();
                        Log.e(LOG_TAG, "Unable to connect server: " + e.getMessage());
                        xmppLoginListener.onloginFail("Logged in fail.");
                    }
                    return null;
                }
            };
            loginThread.execute();
        } catch (Exception e) {
            e.printStackTrace();
            xmppLoginListener.onloginFail("Logged in fail.");
        }
    }

    public void Disconnect() {
        try {
            if (xmppConnection != null) {
                if (userRoster != null && rosterListener != null)
                    userRoster.removeRosterListener(rosterListener);
                xmppConnection.disconnect();
                Log.i(LOG_TAG, "Disconnected from server.");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setRoster() {
        try {
            // - user's friends (Roster)
            if (xmppConnection.isConnected() && xmppConnection.isAuthenticated()) {
                Roster.setDefaultSubscriptionMode(Roster.SubscriptionMode.accept_all);
                userRoster = xmppConnection.getRoster();
                try {
                    RoasterLogic.getInstance().setRester(userRoster);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                // Store roster to collection
                Collection<RosterEntry> rosterEntries = userRoster.getEntries();
                if (rosterEntries.size() != 0) {
                    for (RosterEntry rosterEntry : rosterEntries) {
                        final VCard fVCard = new VCard();
                        try {
                            Log.i(LOG_TAG, "fJID: " + rosterEntry.getUser());
                            fVCard.load(xmppConnection, rosterEntry.getUser());
                            ContactEntry contactEntry = new ContactEntry();
                            contactEntry.setJID(rosterEntry.getUser());

                            // Check is friend online?
                            Presence presence = userRoster.getPresence(rosterEntry.getUser());
                            if (presence.isAvailable() || presence.isAway()) {
                           /* String onlineFrom = presence.getFrom();
                            String[] onlineFromResource = onlineFrom.split("/");
                            contactEntry.setOnlineViaResource(onlineFromResource[1]);*/
                                contactEntry.setIsOnline("1");
                            } else {
                                contactEntry.setIsOnline("0");
                            }

                            // Display name
                            if (fVCard.getNickName() == null) {
                                contactEntry.setUsername(rosterEntry.getUser());
                            } else {
                                contactEntry.setUsername(fVCard.getNickName());
                            }

                            contactEntry.setUnread_count("0");

                            // Avatar
                            if (fVCard.getAvatar() == null) {
                                contactEntry.setProfile_pic(null);
                            } else {
                                contactEntry.setProfile_pic(fVCard.getField("AVATAR") + "");
                            }

                            userContactEntries.add(contactEntry);
                        } catch (Exception e) {
                            Log.e(LOG_TAG, "Unable to load friend VCard: " + e.getMessage());
                        }
                    }
                }
                AddChatManager();
                AttachtoRosterListener();
            } else
                reconnectToXmpp();

        } catch (Exception e) {
            reconnectToXmpp();
            e.printStackTrace();
        }
    }

    public void AttachtoRosterListener() {
        if (xmppConnection != null && xmppConnection.isConnected() && xmppConnection.isAuthenticated()) {
            try {
                userRoster = xmppConnection.getRoster();
                userRoster.addRosterListener(rosterListener);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            reconnectToXmpp();
        }
    }

    public RosterListener rosterListener = new RosterListener() {
        @Override
        public void entriesAdded(Collection<String> collection) {
            rosterChangeListener.entriesAdded(collection);
        }

        @Override
        public void entriesUpdated(Collection<String> collection) {
            rosterChangeListener.entriesUpdated(collection);
        }

        @Override
        public void entriesDeleted(Collection<String> collection) {
            rosterChangeListener.entriesDeleted(collection);
        }

        @Override
        public void presenceChanged(Presence presence) {
            rosterChangeListener.presenceChanged(presence);
            presenceChangesListener.presenceChanged(presence);
        }
    };


    public RosterChangeListener getRosterChangeListener() {
        return rosterChangeListener;
    }

    public void setRosterChangeListener(RosterChangeListener rosterChangeListener) {
        XmppConnectionUtil.rosterChangeListener = rosterChangeListener;
    }

    public void setPresenceChangesListener(PresenceChangesListener presenceChangesListener) {
        XmppConnectionUtil.presenceChangesListener = presenceChangesListener;
    }

    public void removePresenceListener() {
        XmppConnectionUtil.presenceChangesListener = null;
    }

    public ArrayList<ContactEntry> getRosters() {
        ArrayList<ContactEntry> userContactEntries1 = new ArrayList<>();
        try {
            //if (isConnected) {
            Roster.setDefaultSubscriptionMode(Roster.SubscriptionMode.accept_all);
            userRoster = xmppConnection.getRoster();
            Collection<RosterEntry> rosterEntries = userRoster.getEntries();
            if (rosterEntries.size() != 0) {
                userContactEntries1.clear();
                for (RosterEntry rosterEntry : rosterEntries) {
                    final VCard fVCard = new VCard();
                    try {
                        Log.i(LOG_TAG, "fJID: " + rosterEntry.getUser());
                        fVCard.load(xmppConnection, rosterEntry.getUser());
                        ContactEntry contactEntry = new ContactEntry();
                        contactEntry.setJID(rosterEntry.getUser());

                        // Check is friend online?
                        Presence presence = userRoster.getPresence(rosterEntry.getUser());
                        if (presence.isAvailable() || presence.isAway()) {
                            String onlineFrom = presence.getFrom();
                            String[] onlineFromResource = onlineFrom.split("/");
                            //contactEntry.setOnlineViaResource(onlineFromResource[1]);
                            contactEntry.setIsOnline("1");
                        } else {
                            contactEntry.setIsOnline("0");
                        }

                        // Display name
                        if (fVCard.getNickName() == null) {
                            contactEntry.setUsername(rosterEntry.getUser());
                        } else {
                            contactEntry.setUsername(fVCard.getNickName());
                        }

                        contactEntry.setUnread_count("0");

                        // Avatar
                        contactEntry.setProfile_pic(fVCard.getField("AVATAR") + "");

                        userContactEntries1.add(contactEntry);
                    } catch (Exception e) {
                        Log.e(LOG_TAG, "Unable to load friend VCard: " + e.getMessage());
                    }
                    // }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return userContactEntries1;
    }

    public void AddChatManager() {
        if (xmppConnection != null && xmppConnection.isConnected() && xmppConnection.isAuthenticated()) {
            ChatManager chatManager = xmppConnection.getChatManager();
            chatManager.addChatListener(new ChatManagerListener() {
                @Override
                public void chatCreated(Chat chat, boolean create) {
                    chat.addMessageListener(new MessageListener() {
                        @Override
                        public void processMessage(Chat chat, Message msg) {
                            chatNewMessageistener.newMessagereceived(chat, msg);
                        }
                    });
                }
            });
        } else {
            reconnectToXmpp();
        }
    }

    public ChatNewMessagelistener getChatNewMessageistener() {
        return chatNewMessageistener;
    }

    public void setChatNewMessageistener(ChatNewMessagelistener chatNewMessageistener1) {
        XmppConnectionUtil.chatNewMessageistener = chatNewMessageistener1;
    }

    private void ReconnectXmpp() {
        String Uniquecode = qkPreferences.getValues(QKPreferences.USER_UNIQUECODE);
        Login(Uniquecode, xmppLoginListener);
    }

    public ArrayList<ContactEntry> getUserContactEntries() {
        if (userContactEntries != null)
            return userContactEntries;
        else
            return null;
    }

    private void configure(ProviderManager pm) {
        // Add Ping-Pong Feature
        pm.addIQProvider("ping", "urn:xmpp:ping", new PingProvider());

        // Enable VCard feature
        pm.addIQProvider("vCard", "vcard-temp", new VCardProvider());

        // User Search
        pm.addIQProvider("query", "jabber:iq:search", new UserSearch.Provider());

        //  Private Data Storage
        pm.addIQProvider("query", "jabber:iq:private", new PrivateDataManager.PrivateDataIQProvider());

        //  Time
        try {
            pm.addIQProvider("query", "jabber:iq:time", Class.forName("org.jivesoftware.smackx.packet.Time"));
        } catch (ClassNotFoundException e) {
            Log.e(LOG_TAG, "Can't load class for org.jivesoftware.smackx.packet.Time");
        }

        //  XHTML
        pm.addExtensionProvider("html", "http://jabber.org/protocol/xhtml-im", new XHTMLExtensionProvider());

        //  Roster Exchange
        pm.addExtensionProvider("x", "jabber:x:roster", new RosterExchangeProvider());

        //  Message Events
        pm.addExtensionProvider("x", "jabber:x:event", new MessageEventProvider());

        //  Chat State
        pm.addExtensionProvider("active", "http://jabber.org/protocol/chatstates", new ChatStateExtension.Provider());
        pm.addExtensionProvider("composing", "http://jabber.org/protocol/chatstates", new ChatStateExtension.Provider());
        pm.addExtensionProvider("paused", "http://jabber.org/protocol/chatstates", new ChatStateExtension.Provider());
        pm.addExtensionProvider("inactive", "http://jabber.org/protocol/chatstates", new ChatStateExtension.Provider());
        pm.addExtensionProvider("gone", "http://jabber.org/protocol/chatstates", new ChatStateExtension.Provider());

        //  File Transfer - with ASMACK-ANDROID-7
//      pm.addIQProvider("si","http://jabber.org/protocol/si", new StreamInitiationProvider());
//		pm.addIQProvider("query","http://jabber.org/protocol/bytestreams", new BytestreamsProvider());

        //  File Transfer - WITH ASMACK-ANDDROID-7-BEEM
        pm.addIQProvider("si", "http://jabber.org/protocol/si", new StreamInitiationProvider());
        pm.addIQProvider("query", "http://jabber.org/protocol/bytestreams", new BytestreamsProvider());
        pm.addIQProvider("open", "http://jabber.org/protocol/ibb", new IBBProviders.Open());
        pm.addIQProvider("close", "http://jabber.org/protocol/ibb", new IBBProviders.Close());
        pm.addExtensionProvider("data", "http://jabber.org/protocol/ibb", new IBBProviders.Data());

        //  Group Chat Invitations
        pm.addExtensionProvider("x", "jabber:x:conference", new GroupChatInvitation.Provider());

        //  Service Discovery # Items
        pm.addIQProvider("query", "http://jabber.org/protocol/disco#items", new DiscoverItemsProvider());

        //  Service Discovery # Info
        pm.addIQProvider("query", "http://jabber.org/protocol/disco#info", new DiscoverInfoProvider());

        //  Data Forms
        pm.addExtensionProvider("x", "jabber:x:data", new DataFormProvider());

        //  MUC User
        pm.addExtensionProvider("x", "http://jabber.org/protocol/muc#user", new MUCUserProvider());

        //  MUC Admin
        pm.addIQProvider("query", "http://jabber.org/protocol/muc#admin", new MUCAdminProvider());

        //  MUC Owner
        pm.addIQProvider("query", "http://jabber.org/protocol/muc#owner", new MUCOwnerProvider());

        //  Delayed Delivery
        pm.addExtensionProvider("x", "jabber:x:delay", new DelayInformationProvider());

        //  Version
        try {
            pm.addIQProvider("query", "jabber:iq:version", Class.forName("org.jivesoftware.smackx.packet.Version"));
        } catch (ClassNotFoundException e) {
            Log.w(LOG_TAG, "Can't load class for org.jivesoftware.smackx.packet.Version");
        }

        //  Offline Message Requests
        pm.addIQProvider("offline", "http://jabber.org/protocol/offline", new OfflineMessageRequest.Provider());

        //  Offline Message Indicator
        pm.addExtensionProvider("offline", "http://jabber.org/protocol/offline", new OfflineMessageInfo.Provider());

        //  Last Activity
        pm.addIQProvider("query", "jabber:iq:last", new LastActivity.Provider());

        //  SharedGroupsInfo
        pm.addIQProvider("sharedgroup", "http://www.jivesoftware.org/protocol/sharedgroup", new SharedGroupsInfo.Provider());

        //  JEP-33: Extended Stanza Addressing
        pm.addExtensionProvider("addresses", "http://jabber.org/protocol/address", new MultipleAddressesProvider());
    }

}





/*

public class ASmackWrapper
{
    private XMPPConnection connection;
    private String[] friends;
    private static final String eventClass = "ASmackEventListener";
    private static ASmackWrapper wrapper;

    public static ASmackWrapper instance()
    {
        System.out.println("instancecreator of ASmackWrapper 1!");
        if (wrapper == null)
            wrapper = new ASmackWrapper();
        return wrapper;
    }

    public ASmackWrapper()
    {
        System.out.println("constructor of ASmackWrapper");
    }

    public boolean tryToRegister(String user, String pass){
        AccountManager acManager = connection.getAccountManager();
        try {
            Map<String, String> attributes = new HashMap<String,String>();
            attributes.put("email", "MY email");
            acManager.createAccount(user, pass,attributes);
        } catch (XMPPException e) {
            System.out.println("cant autoregister user "+ user +" ... with pass: "+pass+" on server. error:" + e.getLocalizedMessage());
            if (e.getLocalizedMessage().contains("conflict"))
                return false; // Wrong password, since there is already an account with that id!
            return false;
        }
        return true;
    }

    public void setFriends(String[] _friends) {
        friends = _friends;
    }

    public void start(String host, String user, String pass)
    {
        System.out.println("Java: openConenction host:"+host);
        ConnectionConfiguration cc = new ConnectionConfiguration(host,5222);
        //cc.setSendPresence(true);
        this.connection = new XMPPConnection(cc);
        Connection.DEBUG_ENABLED = true;
        try {
            this.connection.connect();
        } catch (XMPPException e) {
            System.out.println("Error connecting to server");
            return;
        }

        if(!this.connection.isConnected()) {
            System.out.println("Java: is not connected");
            onError("Connection failed");
            return;
        }

        boolean loginStatus = login(user, pass);
        if (!loginStatus) {
            onError("Login Failed");
            return;
        }

        RosterListener rl = new RosterListener() {
            public void entriesAdded(Collection<String> addresses) {}
            public void entriesUpdated(Collection<String> addresses) {}
            public void entriesDeleted(Collection<String> addresses) {}
            public void presenceChanged(Presence presence) {
                System.out.println("presence changed!" + presence.getFrom() + " "+presence.getStatus());
                onPresence(presence);
            }
        };
        if (connection.getRoster() != null) {
            connection.getRoster().setSubscriptionMode(Roster.SubscriptionMode.accept_all);

            System.out.println("7");
            connection.getRoster().addRosterListener(rl);
        }

        onAuthenticate("");
        System.out.println("10");

        //Set presence to online!
        Presence presence = new Presence(Presence.Type.available);
        presence.setStatus("Online, Programmatically!");
        presence.setPriority(24);
        presence.setMode(Presence.Mode.available);
        connection.sendPacket(presence);
    }

    private void addFriends() throws Exception {
        if (friends == null) {
            System.out.println("No friends to add");
            return;
        }
        System.out.println("Number of friends to add: "+friends.length);
        for (int i = 0;i<friends.length;i++) {
            System.out.println("Create user in roster: "+friends[i]);
            connection.getRoster().createEntry("fb"+friends[i], "No name_",null);
        }
    }

    private boolean login(String jid, String password) {
        System.out.println("1");
        boolean isLoggedIn=true;
        try {
            this.connection.login(jid, password);
        } catch (XMPPException e) {
            isLoggedIn=false;
        }

        System.out.println("2");
        if(!isLoggedIn) {
            boolean isRegistred = tryToRegister(jid,password);
            if (isRegistred) {
                connection.disconnect();
                try {
                    connection.connect();
                    connection.login(jid, password);
                } catch (XMPPException e) {
                    onError("Could not connect and login after registring");
                    return false;
                }
            } else {
                return false;
            }
        }

        try {
            addFriends();
        } catch (Exception e) {
            onError("Could not add friends to roster");
        }
        ChatManager chatmanager = connection.getChatManager();

        chatmanager.addChatListener(new ChatManagerListener()
        {
            public void chatCreated(final Chat chat, final boolean createdLocally)
            {
                System.out.println("OK Chat created!");
                chat.addMessageListener(new MessageListener()
                {
                    public void processMessage(Chat chat, Message message)
                    {
                        onMessage(chat, message);
                    }
                });
            }
        });

        return true;
    }

    public void sendMessage(String rec, String message) {
        System.out.println("sendMessage(string,string) to host :"+connection.getHost());
        Chat chat = connection.getChatManager().createChat(rec+"@"+connection.getHost(), new MessageListener() {
            public void processMessage(Chat chat, Message message) {
                // Print out any messages we get back to standard out.
                System.out.println("Probably an error, since we got a instant reply on sent message. Received message body: " + message.getBody() + " from:"+message.getFrom() + " to:"+message.getTo());
            }
        });
        try {
            chat.sendMessage(message);
            System.out.println("Message sent");
        } catch (XMPPException e) {
            System.out.println("Error sending message: "+e.toString());
            e.printStackTrace();

        }
    }

    public void logout () {
        System.out.println("Login out...");
        connection.disconnect();
    }

    public void getOnlineFriends() {
        Roster roster = connection.getRoster();
        Collection<RosterEntry> entries = roster.getEntries();

        for(RosterEntry rosterEntry: entries) {
            String user = rosterEntry.getUser();
            Presence presence = roster.getPresence(user);
            System.out.println("Presence : "+presence);
            System.out.println("Presence type: "+presence.getType());
            System.out.println("Presence mode: "+presence.getMode());
        }

        //Set presence to online!
        Presence presence = new Presence(Presence.Type.available);
        presence.setStatus("Online, Programmatically!");
        presence.setPriority(24);
        presence.setMode(Presence.Mode.available);
        connection.sendPacket(presence);
    }

    private void onMessage(Chat chat, Message message) {
        String m = ("Received message: " + (message != null ? message.getBody() : "NULL"));
        System.out.println(m);

        UnityPlayer.UnitySendMessage(eventClass, "Message", m);
    }

    private void onError(String message) {
        UnityPlayer.UnitySendMessage(eventClass, "Error", message);
    }

    private void onAuthenticate(String message) {
        UnityPlayer.UnitySendMessage(eventClass, "Authenticate", message);
    }

    private void onPresence(Presence presence) {
        String user = presence.getFrom();
        if (presence.getType() == Presence.Type.available)
            UnityPlayer.UnitySendMessage(eventClass, "Online", user);
        else
            UnityPlayer.UnitySendMessage(eventClass, "Offline", user);

        System.out.println("Java: Presence changed, from:" +presence.getFrom() + " type:"+presence.getType() + " toString:"+presence.toString());
    }
}*/
