package com.OpenFire.interfaces;

import org.jivesoftware.smack.packet.Presence;

/**
 * Created by ZTLAB-12 on 20-07-2018.
 */

public interface PresenceChangesListener {
    void presenceChanged(Presence var1);
}
