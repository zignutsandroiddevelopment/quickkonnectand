package com.OpenFire.interfaces;

/**
 * Created by ZTLAB-12 on 16-07-2018.
 */

public interface XmppConnetionListener {

    void onConnectionSuccessfull(String message);

    void onConnectionFailure(String message);
}
