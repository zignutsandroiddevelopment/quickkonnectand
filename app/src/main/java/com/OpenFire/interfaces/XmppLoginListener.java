package com.OpenFire.interfaces;

/**
 * Created by ZTLAB-12 on 16-07-2018.
 */

public interface XmppLoginListener {
    void onLoginSuccess(String message);

    void onloginFail(String message);
}
