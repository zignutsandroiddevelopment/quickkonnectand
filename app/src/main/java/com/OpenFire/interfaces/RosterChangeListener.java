package com.OpenFire.interfaces;

import org.jivesoftware.smack.packet.Presence;

import java.util.Collection;

/**
 * Created by ZTLAB-12 on 16-07-2018.
 */

public interface RosterChangeListener {
    void entriesAdded(Collection<String> var1);

    void entriesUpdated(Collection<String> var1);

    void entriesDeleted(Collection<String> var1);

    void presenceChanged(Presence var1);
}
