package com.OpenFire.interfaces;

import org.jivesoftware.smack.Chat;
import org.jivesoftware.smack.packet.Message;

/**
 * Created by ZTLAB-12 on 18-07-2018.
 */

public interface ChatNewMessagelistener {
    void newMessagereceived(Chat chat, Message msg);
}
