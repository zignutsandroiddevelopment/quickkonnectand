package com.OpenFire;

import android.annotation.SuppressLint;
import android.app.ActivityOptions;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.Chat.NewChatActivity;
import com.Chat.UsersChatAdapter;
import com.OpenFire.entry.ContactEntry;
import com.OpenFire.interfaces.ChatNewMessagelistener;
import com.OpenFire.interfaces.RosterChangeListener;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.interfaces.getUserOnClick;
import com.models.ChatUserListModel;
import com.models.ChatUserMainModel;
import com.quickkonnect.R;
import com.realmtable.ChatUserList;
import com.realmtable.RealmController;
import com.utilities.Constants;
import com.utilities.QKPreferences;
import com.utilities.Utilities;
import com.volleyapirequest.VolleyApiRequest;
import com.volleyapirequest.VolleyCallBack;

import org.jivesoftware.smack.Chat;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Presence;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;

import io.realm.RealmResults;

public class ChatActivity extends AppCompatActivity implements ChatNewMessagelistener, RosterChangeListener, SwipeRefreshLayout.OnRefreshListener {

    private RecyclerView mUsersRecyclerView;
    private String UniqueCode, UserName = "";
    private QKPreferences qkPreferences;
    public LinearLayoutManager mLayoutManager;
    private TextView tvNoChats;
    //private EditText search_chat_contact;
    private ImageView img_chat_close, img_search_chat_user;
    private UsersChatAdapter mUsersChatAdapter;
    XmppConnectionUtil xmppConnectionUtil;
    private SwipeRefreshLayout swipeRefreshLayout_chatuser;
    private ArrayList<ContactEntry> userContactEntries;
    private int CHAT_USER_TO_CHAT = 1010;
    private int count = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        Utilities.ChangeStatusBar(ChatActivity.this);

        qkPreferences = new QKPreferences(ChatActivity.this);
        UniqueCode = qkPreferences.getValues(QKPreferences.USER_UNIQUECODE) + "";
        UserName = qkPreferences.getValues(QKPreferences.USER_FULLNAME) + "";

        xmppConnectionUtil = new XmppConnectionUtil(getApplicationContext());

        userContactEntries = new ArrayList<ContactEntry>();

        xmppConnectionUtil.setChatNewMessageistener(ChatActivity.this);
        xmppConnectionUtil.setRosterChangeListener(ChatActivity.this);

        initViews();

    }

    public void initViews() {

        String html = "<html><font color='#000000'><b>No chats yet.</b></font><br><font color='#c1c1c1'>Start a new chat with one of your matches.</font></html>";
        tvNoChats = findViewById(R.id.tv_no_record_found);
        tvNoChats.setText(Html.fromHtml(html + ""));

        img_chat_close = findViewById(R.id.img_chat_close);
        img_search_chat_user = findViewById(R.id.img_search_chat_user);
        img_chat_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setResult(RESULT_OK);
                ChatActivity.this.finish();
            }
        });

        img_search_chat_user.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ChatActivity.this, ContactSearchActivity.class);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(ChatActivity.this, view, "robot");
                    startActivity(intent, options.toBundle());
                } else {
                    startActivity(intent);
                }
            }
        });

        swipeRefreshLayout_chatuser = findViewById(R.id.swipeRefreshLayout_chatuser);
        swipeRefreshLayout_chatuser.setOnRefreshListener(this);
        swipeRefreshLayout_chatuser.setColorSchemeResources(R.color.colorPrimary,
                android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_blue_dark);

        mUsersRecyclerView = findViewById(R.id.recyclerView);
        mLayoutManager = new LinearLayoutManager(ChatActivity.this);
       /* mLayoutManager.setReverseLayout(true);
        mLayoutManager.setStackFromEnd(true);*/
        mUsersRecyclerView.setLayoutManager(mLayoutManager);
        mUsersRecyclerView.setHasFixedSize(true);
        mUsersChatAdapter = new UsersChatAdapter(ChatActivity.this, userContactEntries, getUserOnClick);
        mUsersRecyclerView.setAdapter(mUsersChatAdapter);

        GetListFromLocal(true);
        //AttachtoRosterListener();
    }

    private void GetListFromLocal(boolean isGetFromXmpp) {

        boolean isprogress = true;
        RealmResults<ChatUserList> userlist = RealmController.with(ChatActivity.this).getChatUserlist();
        userContactEntries.clear();
        if (userlist != null && userlist.size() > 0) {
            isprogress = false;
            for (int i = 0; i < userlist.size(); i++) {
                if (userlist.get(i).getUsername() != null && !userlist.get(i).getUsername().equalsIgnoreCase("") && !userlist.get(i).getUsername().equalsIgnoreCase("null") /*&& !userlist.get(i).getUsername().equalsIgnoreCase(UserName + "")*/) {

                    ContactEntry chatUserList = new ContactEntry();

                    chatUserList.setJID(userlist.get(i).getJID() + "");
                    chatUserList.setUsername(userlist.get(i).getUsername() + "");
                    chatUserList.setProfile_pic(userlist.get(i).getProfile_pic() + "");
                    chatUserList.setIsOnline(userlist.get(i).getIsOnline() + "");
                    chatUserList.setUnread_count(userlist.get(i).getUnread_count() + "");
                    chatUserList.setUser_id(userlist.get(i).getUser_id() + "");
                    chatUserList.setUnique_code(userlist.get(i).getUnique_code() + "");
                    chatUserList.setLast_message(userlist.get(i).getLast_message() + "");
                    chatUserList.setConversationID(userlist.get(i).getConversationID() + "");
                    chatUserList.setDate(userlist.get(i).getDate() + "");

                    userContactEntries.add(chatUserList);

                    try {
                        int currentcount = Integer.parseInt(userlist.get(i).getUnread_count());
                        count = count + currentcount;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
            setAdaptor();
            qkPreferences.StoreUnreadCount(count);
        }
        if (isGetFromXmpp)
            GetUserList(isprogress);
    }

    private void setAdaptor() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (userContactEntries != null && userContactEntries.size() > 0) {

                    try {
                        Collections.sort(userContactEntries, new Comparator<ContactEntry>() {
                            @Override
                            public int compare(ContactEntry chatEntry, ContactEntry t1) {
                                Date date1 = Utilities.getConvertedDateLong(chatEntry.getDate());
                                Date date2 = Utilities.getConvertedDateLong(t1.getDate());

                                return date2.compareTo(date1);
                            }
                        });
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    mUsersRecyclerView.setVisibility(View.VISIBLE);
                    tvNoChats.setVisibility(View.GONE);
                    mUsersChatAdapter.notifyDataSetChanged();
                } else {
                    mUsersRecyclerView.setVisibility(View.GONE);
                    tvNoChats.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    getUserOnClick getUserOnClick = new getUserOnClick() {
        @Override
        public void getUserclick(int type, final int pos) {
            if (type == Constants.TYPE_CLICK_ON_OTHER) {
                Intent intent = new Intent(ChatActivity.this, NewChatActivity.class);
                intent.putExtra("fJID", userContactEntries.get(pos).getJID());
                intent.putExtra("fNickName", userContactEntries.get(pos).getUsername());
                intent.putExtra("fAvatarByte", userContactEntries.get(pos).getProfile_pic());
                startActivityForResult(intent, CHAT_USER_TO_CHAT);
            } else if (type == Constants.TYPE_CHAT_DELETE) {
                new AlertDialog.Builder(ChatActivity.this)
                        .setTitle("Delete Conversation.")
                        .setMessage("Are you sure want to delete conversation with " + userContactEntries.get(pos).getUsername() + ".?")
                        .setCancelable(false)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                DeleteConversation(userContactEntries.get(pos).getJID(), pos);
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        }).show();
            }
        }
    };

    String UniqueId = "";

    private void DeleteConversation(final String jid, final int pos) {
        try {
            try {
                String fjsplit[] = jid.split("@");
                if (fjsplit[0] != null) {
                    UniqueId = fjsplit[0];
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            JSONObject jsonObject = null;
            try {
                jsonObject = new JSONObject();
                jsonObject.put("fromJID", UniqueCode + "@quickkonnect.com");
                jsonObject.put("toJID", jid + "");
            } catch (Exception e) {
                e.printStackTrace();
            }
            new VolleyApiRequest(ChatActivity.this, true, VolleyApiRequest.REQUEST_BASEURL + "api/openfire/delete_user_conversion",
                    new VolleyCallBack() {
                        @Override
                        public void onResponse(JSONObject response) {
                            swipeRefreshLayout_chatuser.setRefreshing(false);
                            try {
                                String status = response.getString("status");
                                String message = response.getString("msg");
                                if (status.equals("200")) {
                                    RealmController.with(ChatActivity.this).deleteSingleConversation(jid + "");
                                    RealmController.with(ChatActivity.this).clearChatConversationbyid(UniqueCode + "|" + UniqueId);
                                    userContactEntries.remove(pos);
                                    mUsersChatAdapter.notifyItemRemoved(pos);

                                } else {
                                    Utilities.showToast(ChatActivity.this, message
                                    );
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onErrorResponse(VolleyError error) {
                            VolleyApiRequest.showVolleyError(ChatActivity.this, error);
                        }
                    }).enqueRequest(jsonObject, Request.Method.POST);
        } catch (Exception e) {
            e.printStackTrace();
            setAdaptor();
        }

    }

    private void GetUserList(boolean isProgressShown) {
        try {
            JSONObject jsonObject = null;
            try {
                jsonObject = new JSONObject();
                jsonObject.put("JID", UniqueCode + "@quickkonnect.com");
            } catch (Exception e) {
                e.printStackTrace();
            }
            new VolleyApiRequest(ChatActivity.this, isProgressShown, VolleyApiRequest.REQUEST_BASEURL + "api/openfire/conversion_list",
                    new VolleyCallBack() {
                        @Override
                        public void onResponse(JSONObject response) {
                            swipeRefreshLayout_chatuser.setRefreshing(false);
                            try {
                                String status = response.getString("status");
                                String message = response.getString("msg");
                                if (status.equals("200")) {
                                    ChatUserMainModel gson = new Gson().fromJson(String.valueOf(response), ChatUserMainModel.class);
                                    StoreDatailRealm(gson);
                                } else {
                                    Utilities.showToast(ChatActivity.this, message);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onErrorResponse(VolleyError error) {
                            swipeRefreshLayout_chatuser.setRefreshing(false);
                            //VolleyApiRequest.showVolleyError(ChatActivity.this, error);
                        }
                    }).enqueRequest(jsonObject, Request.Method.POST);
        } catch (Exception e) {
            swipeRefreshLayout_chatuser.setRefreshing(false);
            e.printStackTrace();
            setAdaptor();
        }
    }

    private void StoreDatailRealm(final ChatUserMainModel gson) {
        try {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (gson != null) {
                        if (gson.getData().size() > 0) {
                            RealmController.with(ChatActivity.this).clearChatuserList();
                            for (int i = 0; gson.getData().size() > i; i++) {
                                ChatUserListModel chatConversationItems = gson.getData().get(i);

                                if (!chatConversationItems.getUsername().equalsIgnoreCase("") && !chatConversationItems.getUsername().equalsIgnoreCase("null")) {

                                    ChatUserList contactEntry = new ChatUserList();

                                    contactEntry.setJID(chatConversationItems.getjID());
                                    contactEntry.setUsername(chatConversationItems.getUsername());
                                    contactEntry.setProfile_pic(chatConversationItems.getProfilePic());
                                    //contactEntry.setIsOnline("0");
                                    //contactEntry.setUnread_count(chatConversationItems.getUnreadCount());
                                    contactEntry.setUser_id(chatConversationItems.getUserId());
                                    contactEntry.setUnique_code(chatConversationItems.getUniqueCode());
                                    contactEntry.setLast_message(chatConversationItems.getLastMessage());
                                    contactEntry.setDate(chatConversationItems.getDate());
                                    contactEntry.setConversationID(chatConversationItems.getConversationID());

                                    RealmController.with(ChatActivity.this).updateChatUser(contactEntry);
                                }
                            }
                            GetListFromLocal(false);
                        } else {
                            RealmController.with(ChatActivity.this).clearChatuserList();
                            GetListFromLocal(false);
                        }
                    }
                    setAdaptor();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        GetListFromLocal(false);
    }

    private void RefreshList() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                GetUserList(false);
            }
        });
    }

    private void Receivenewmessage(final Chat chat, final Message msg) {
        try {
            @SuppressLint("StaticFieldLeak")
            AsyncTask<Void, Void, String> getUserList = new AsyncTask<Void, Void, String>() {
                @Override
                protected String doInBackground(Void... voids) {
                    if (msg != null && msg.getBodies().size() > 0) {
                        if (msg.getType().toString().equalsIgnoreCase("chat")) {
                            String str = msg.getFrom();
                            String Jid[] = str.split("/");
                            if (Jid[0] != null && userContactEntries != null) {

                                int pos = 0;

                                /*Comparator<ContactEntry> c = new Comparator<ContactEntry>() {
                                    public int compare(ContactEntry u1, ContactEntry u2) {
                                        return u1.getJid().compareTo(u2.getJid() + "");
                                    }
                                };
                                int index = Collections.binarySearch(userContactEntries, new ContactEntry(), c);*/

                                try {
                                    for (int i = 0; i < userContactEntries.size(); i++) {
                                        if (userContactEntries.get(i).getJID().equalsIgnoreCase(Jid[0] + "")) {
                                            pos = i + 1;
                                        }
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                                // get last message
                                String message = "";
                                if (msg.getBodies().size() > 1)
                                    message = msg.getBody() + "";
                                else
                                    message = msg.getBody() + "";

                                //get total count
                                int totalCount = 0;

                                if (pos != 0) {
                                    int finalpos = pos - 1;
                                    if (userContactEntries.get(finalpos).getUnread_count() != null && !userContactEntries.get(finalpos).getUnread_count().equalsIgnoreCase("0") &&
                                            !userContactEntries.get(finalpos).getUnread_count().equalsIgnoreCase("") &&
                                            !userContactEntries.get(finalpos).getUnread_count().equalsIgnoreCase("null")) {
                                        try {
                                            totalCount = Integer.parseInt(userContactEntries.get(finalpos).getUnread_count());
                                            totalCount = totalCount + msg.getBodies().size();
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    } else {
                                        totalCount = msg.getBodies().size();
                                    }

                                    String jid = userContactEntries.get(finalpos).getJID() + "";
                                    String nickname = userContactEntries.get(finalpos).getUsername() + "";
                                    String avatarByte = userContactEntries.get(finalpos).getProfile_pic() + "";
                                    String isOnline = userContactEntries.get(finalpos).getIsOnline() + "";
                                    String userid = userContactEntries.get(finalpos).getIsOnline() + "";
                                    String uniquecode = userContactEntries.get(finalpos).getIsOnline() + "";
                                    String convid = userContactEntries.get(finalpos).getIsOnline() + "";
                                    String date = Calendar.getInstance().getTimeInMillis() + "";

                                    ContactEntry chatUserList = new ContactEntry();
                                    chatUserList.setJID(jid);
                                    chatUserList.setUsername(nickname);
                                    chatUserList.setProfile_pic(avatarByte + "");
                                    chatUserList.setIsOnline(isOnline + "");
                                    chatUserList.setUnread_count(totalCount + "");
                                    chatUserList.setUser_id(userid + "");
                                    chatUserList.setUnique_code(uniquecode + "");
                                    chatUserList.setLast_message(message + "");
                                    chatUserList.setConversationID(convid + "");
                                    chatUserList.setDate(date);

                                    if (!nickname.equalsIgnoreCase("null") && !nickname.equalsIgnoreCase(""))
                                        ChangeUserListCount(finalpos, chatUserList);
                                }
                            }
                        }
                    }
                    return null;
                }
            };
            getUserList.execute();
        } catch (Exception e) {
            e.printStackTrace();
            setAdaptor();
        }
    }

    private void ChangeUserListCount(final int finalpos, final ContactEntry chatUserList) {
        try {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    userContactEntries.set(finalpos, chatUserList);
                    mUsersChatAdapter.refillData(userContactEntries);
                    StoreDataInLocal(chatUserList);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void StoreDataInLocal(final ContactEntry chatUserList) {
        try {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (chatUserList != null) {

                        ChatUserList contactEntry = new ChatUserList();

                        contactEntry.setJID(chatUserList.getJID());
                        contactEntry.setUsername(chatUserList.getUsername());
                        contactEntry.setProfile_pic(chatUserList.getProfile_pic());
                        contactEntry.setIsOnline(chatUserList.getIsOnline());
                        contactEntry.setUnread_count(chatUserList.getUnread_count());
                        contactEntry.setUser_id(chatUserList.getUser_id());
                        contactEntry.setUnique_code(chatUserList.getUnique_code());
                        contactEntry.setLast_message(chatUserList.getLast_message());
                        contactEntry.setDate(chatUserList.getDate());
                        contactEntry.setConversationID(chatUserList.getConversationID());
                        RealmController.with(ChatActivity.this).updateChatUser(contactEntry);

                    }
                    setAdaptor();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void newMessagereceived(Chat chat, Message msg) {
        Receivenewmessage(chat, msg);
    }

    @Override
    public void entriesAdded(Collection<String> var1) {
        RefreshList();
    }

    @Override
    public void entriesUpdated(Collection<String> var1) {
        RefreshList();
    }

    @Override
    public void entriesDeleted(Collection<String> var1) {
        RefreshList();
    }

    @Override
    public void presenceChanged(Presence var1) {
        RefreshList();
    }

    @Override
    public void onRefresh() {
        swipeRefreshLayout_chatuser.setRefreshing(true);
        GetUserList(false);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        setResult(RESULT_OK);
        ChatActivity.this.finish();
    }
}