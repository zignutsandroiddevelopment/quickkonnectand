package com.OpenFire;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.OpenFire.entry.ChatEntry;
import com.quickkonnect.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by ZTLAB-12 on 14-07-2018.
 */

public class ChatAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<ChatEntry> data;
    private ChatEntryHolder holder;
    private String myJID;

    public ChatAdapter(Context context, ArrayList<ChatEntry> data, String myJID) {
        this.context = context;
        this.data = data;
        this.myJID = myJID;
    }

    public ArrayList<ChatEntry> getData() {
        return data;
    }

    public void setData(ArrayList<ChatEntry> data) {
        this.data = data;
    }

    public int getCount() {
        return data.size();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        if (data.get(position).getSenderJID().equalsIgnoreCase(myJID)) {
            //convertView = LayoutInflater.from(context).inflate(R.layout.layout_chat_compact_right, null);
            convertView = LayoutInflater.from(context).inflate(R.layout.layout_sender_message, null);
        } else {
            //convertView = LayoutInflater.from(context).inflate(R.layout.layout_chat_compact_left, null);
            convertView = LayoutInflater.from(context).inflate(R.layout.layout_recipient_message, null);
        }
        holder = new ChatEntryHolder();
        holder.message = convertView.findViewById(R.id.chat_compact_message);
        holder.time = convertView.findViewById(R.id.time);

        convertView.setTag(holder);

        holder.message.setText(data.get(position).getMessage());

        try {
            Long timestemp = data.get(position).getWhen();
            String times = new SimpleDateFormat("MMM, dd hh:mm aa").format(new Date(timestemp * 1000));
            String hours = new SimpleDateFormat("hh:mm aa").format(new Date(timestemp * 1000));
            String date = new SimpleDateFormat("MMM, dd").format(new Date(timestemp * 1000));
            String todaydate = new SimpleDateFormat("MMM, dd").format(Calendar.getInstance().getTime());
            if (times != null && date.equalsIgnoreCase(todaydate + "")) {
                holder.time.setText(hours + "");
            } else {
                holder.time.setText(times + "");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return convertView;
    }

    private class ChatEntryHolder {

        public TextView message, time;

    }

}

