package com.OpenFire.entry;

import org.jivesoftware.smack.Roster;

/**
 * Created by ZTLAB-12 on 21-07-2018.
 */

public class RoasterLogic {

    private Roster roster = null;
    private static RoasterLogic instance = null;

    public synchronized static RoasterLogic getInstance() {
        if (instance == null) {
            instance = new RoasterLogic();
        }
        return instance;
    }

    public void setRester(Roster roster) {
        this.roster = roster;
    }

    public Roster getRoster() {
        return this.roster;
    }
}
