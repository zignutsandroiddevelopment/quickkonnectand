package com.OpenFire.entry;

import android.graphics.Bitmap;

public class ChatEntry {

    private String id;
    private String senderJID;
    private String receiverJID;
    private String message;
    private long when;
    private boolean isSelected = false;

    public ChatEntry() {
        this.id = null;
        this.senderJID = null;
        this.receiverJID = null;
        this.message = null;
        this.when = 0;
    }

    public ChatEntry(String id, String senderJID, String receiverJID, String message, long when, boolean isSelected) {
        this.id = id;
        this.senderJID = senderJID;
        this.receiverJID = receiverJID;
        this.message = message;
        this.when = when;
        this.isSelected = isSelected;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSenderJID() {
        return senderJID;
    }

    public void setSenderJID(String senderJID) {
        this.senderJID = senderJID;
    }

    public String getReceiverJID() {
        return receiverJID;
    }

    public void setReceiverJID(String receiverJID) {
        this.receiverJID = receiverJID;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public long getWhen() {
        return when;
    }

    public void setWhen(long when) {
        this.when = when;
    }
}
