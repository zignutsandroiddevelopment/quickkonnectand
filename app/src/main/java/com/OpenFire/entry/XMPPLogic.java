package com.OpenFire.entry;

import org.jivesoftware.smack.XMPPConnection;

/**
 * Created by ZTLAB-12 on 14-07-2018.
 */

public class XMPPLogic {

    private XMPPConnection connection = null;

    private static XMPPLogic instance = null;

    public synchronized static XMPPLogic getInstance() {
        if (instance == null) {
            instance = new XMPPLogic();
        }
        return instance;
    }

    public void setConnection(XMPPConnection connection) {
        this.connection = connection;
    }

    public XMPPConnection getConnection() {
        return this.connection;
    }

}