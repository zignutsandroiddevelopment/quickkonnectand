package com.OpenFire;

import android.animation.Animator;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.Chat.NewChatActivity;
import com.adapter.NewContactListAdapter;
import com.interfaces.OnContactClick;
import com.models.contacts.ContactsPojo;
import com.models.contacts.Datum;
import com.models.logindata.ModelLoggedData;
import com.models.logindata.ModelLoggedUser;
import com.quickkonnect.PojoClasses;
import com.quickkonnect.R;
import com.realmtable.Contacts;
import com.realmtable.RealmController;
import com.retrofitservice.ApiUtils;
import com.retrofitservice.SOService;
import com.utilities.Constants;
import com.utilities.QKPreferences;
import com.utilities.Utilities;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import io.realm.RealmResults;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ContactSearchActivity extends AppCompatActivity {

    private RecyclerView mUsersRecyclerView;
    private String UniqueCode;
    private QKPreferences qkPreferences;
    public LinearLayoutManager mLayoutManager;
    private TextView tvNoChats;
    private EditText search_chat_contact;
    private ImageView img_chat_close;
    public static ArrayList<Datum> mlist;
    public static ArrayList<Datum> mlist_multiselect;
    public NewContactListAdapter newContactListAdapter;
    private SOService mService;
    private LinearLayout lin_toolbar, lin_search_tool;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_search);
        Utilities.ChangeStatusBar(ContactSearchActivity.this);
        qkPreferences = new QKPreferences(ContactSearchActivity.this);
        UniqueCode = qkPreferences.getValues(QKPreferences.USER_UNIQUECODE) + "";

        mService = ApiUtils.getSOService();

        initViews();
    }

    private void initViews() {

        tvNoChats = findViewById(R.id.tv_no_record_found_contact_search);
        /*img_chat_close = findViewById(R.id.img_chat_close);
        img_chat_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ChatActivity.this.finish();
            }
        });*/
        search_chat_contact = findViewById(R.id.search_chat_contact_search);
        search_chat_contact.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                search_chat_contact.setCursorVisible(true);
                if (newContactListAdapter != null) {
                    filter(editable.toString());
                }
            }
        });

        lin_toolbar = findViewById(R.id.lin_toolbar);
        lin_search_tool = findViewById(R.id.lin_search_tool);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            lin_toolbar.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
                @TargetApi(Build.VERSION_CODES.LOLLIPOP)
                @Override
                public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                    v.removeOnLayoutChangeListener(this);
                    revealShow(lin_search_tool);
                }
            });
        }

        try {
            //open keyboard
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
        } catch (Exception e) {
            e.printStackTrace();
        }

        mUsersRecyclerView = findViewById(R.id.recyclerView_contact_search);
        mLayoutManager = new LinearLayoutManager(ContactSearchActivity.this);
        mUsersRecyclerView.setLayoutManager(mLayoutManager);
        mUsersRecyclerView.setHasFixedSize(true);

        GetListFromLocal(true);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void revealShow(final LinearLayout lin) {
        int w = lin.getWidth();
        int h = lin.getHeight();

        int endRadius = (int) Math.hypot(w, h);
        int cx = (int) (lin_toolbar.getX() + (lin_toolbar.getWidth() / 2));
        int cy = (int) (lin_toolbar.getY()) + lin_toolbar.getHeight() + 56;

        Animator revealAnimator = ViewAnimationUtils.createCircularReveal(lin, cx, cy, 5f, endRadius);
        lin.setVisibility(View.VISIBLE);
        revealAnimator.setDuration(500);
        revealAnimator.start();
    }

    private void filter(String text) {
        try {
            ArrayList<Datum> filterdNames = new ArrayList<>();
            for (Datum s: mlist) {
                if (s.getScanUserName().toLowerCase().contains(text.toLowerCase())) {
                    filterdNames.add(s);
                }
            }
            newContactListAdapter.filterList(filterdNames);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void GetListFromLocal(boolean isReload) {

        RealmResults<Contacts> contacts = RealmController.with(ContactSearchActivity.this).getContacts();
        if (contacts != null && contacts.size() > 0) {

            mlist = new ArrayList<>();
            mlist_multiselect = new ArrayList<>();

            for (Contacts contacts1: contacts) {

                if (!String.valueOf(UniqueCode).equals(contacts1.getUnique_code()) && contacts1.getContact_type() != null && contacts1.getContact_type().equals(Constants.TYPE_CONTACTS)) {
                    Datum datum = new Datum();

                    datum.setEmail(contacts1.getEmail());
                    datum.setPhone(contacts1.getPhone());
                    datum.setScannedBy(contacts1.getScanned_by());
                    datum.setScanUserId(Integer.parseInt(contacts1.getUser_id().replace(".0", "")));
                    String cap = contacts1.getUsername();
                    datum.setScanUserName(cap);
                    datum.setScanUserProfileUrl(contacts1.getProfile_pic());
                    datum.setScanUserUniqueCode(contacts1.getUnique_code());
                    datum.setScan_user_device_id(contacts1.getScan_user_device_id() + "");
                    datum.setScanDate(contacts1.getScan_date());
                    datum.setFirstname(contacts1.getFirstname());
                    datum.setLastname(contacts1.getLastname());
                    datum.setIs_employer(contacts1.getIs_employer());
                    datum.setIs_personal(contacts1.getIs_personal());
                    mlist.add(datum);
                }
            }
        }

        if (mlist != null && mlist.size() > 0) {

            mUsersRecyclerView.setVisibility(View.VISIBLE);
            tvNoChats.setVisibility(View.GONE);

            //todo removed contact short by name
            try {
                Utilities.shortContact(mlist, "date");
            } catch (Exception e) {
                e.printStackTrace();
            }

            newContactListAdapter = new NewContactListAdapter(ContactSearchActivity.this, mlist, mlist_multiselect, true, new OnContactClick() {

                @Override
                public void onLoadMore(String type) {

                }

                @Override
                public void getContact(Datum contactsList, int type) {
                    try {
                        // hide keyboard
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(search_chat_contact.getWindowToken(), 0);

                        Intent intent = new Intent(ContactSearchActivity.this, NewChatActivity.class);
                        intent.putExtra("fJID", contactsList.getScanUserUniqueCode() + "@quickkonnect.com");
                        intent.putExtra("fNickName", contactsList.getScanUserName());
                        intent.putExtra("fAvatarByte", contactsList.getScanUserProfileUrl() + "");
                        startActivity(intent);
                        finish();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void getFollower(PojoClasses.FollowersList_Contact followerlist) {

                }

                @Override
                public void getFollowing(PojoClasses.FollowingList_Contact followinglist) {

                }
            });
            mUsersRecyclerView.setAdapter(newContactListAdapter);

        } else {
            mUsersRecyclerView.setVisibility(View.GONE);
            tvNoChats.setVisibility(View.VISIBLE);
        }
        if (isReload) {
            getContactsList(false);
        }
    }

    private void getContactsList(boolean b) {

//        String token = "";
//        if (qkPreferences != null) {
//            token = qkPreferences.getApiToken() + "";
//        }

        Map<String, String> headerparams = Utilities.getHeaderParameter(ContactSearchActivity.this);

//        params.put("Authorization", "Bearer " + token + "");
//        params.put("Accept", "application/json");
//        params.put("QK_ACCESS_KEY", getResources().getString(R.string.QK_ACCESS_KEY) + "");

        mService.getContactsList(headerparams, qkPreferences.getLoggedUserid(), TimeZone.getDefault().getID()).enqueue(new Callback<ContactsPojo>() {
            @Override
            public void onResponse(Call<ContactsPojo> call, Response<ContactsPojo> response) {
                try {
                    if (response != null && response.body() != null) {
                        ContactsPojo contactsPojo = response.body();
                        if (contactsPojo != null && contactsPojo.getStatus() == 200) {
                            parseContacts(response.body());
                        } else {
                            RealmController.with(ContactSearchActivity.this).clearContacts();
                            GetListFromLocal(false);
                        }
                    } else
                        GetListFromLocal(false);
                } catch (Exception e) {
                    GetListFromLocal(false);
                }
            }

            @Override
            public void onFailure(Call<ContactsPojo> call, Throwable t) {
                Log.e("Contacts:Error", "" + t.getLocalizedMessage());
                GetListFromLocal(false);
            }
        });

    }


    public void parseContacts(ContactsPojo contactsPojo) {
        List<Datum> datumList = contactsPojo.getData();
        if (datumList != null && datumList.size() > 0) {

            RealmController.with(ContactSearchActivity.this).clearContacts();

            for (Datum datum: datumList) {

                Contacts contacts = new Contacts();

                contacts.setUser_id("" + datum.getScanUserId());
                contacts.setUsername(datum.getScanUserName());
                contacts.setProfile_pic(datum.getScanUserProfileUrl());
                contacts.setFirstname(datum.getFirstname());
                contacts.setLastname(datum.getLastname());
                contacts.setUnique_code(datum.getScanUserUniqueCode());
                contacts.setScanned_by(datum.getScannedBy());
                contacts.setPhone(datum.getPhone());
                contacts.setEmail(datum.getEmail());
                contacts.setProfile_id("" + datum.getProfileId());
                contacts.setProfile_name("" + datum.getProfileName());
                contacts.setContact_type(Constants.TYPE_CONTACTS);
                contacts.setScan_user_device_id("" + datum.getScan_user_device_id());
                contacts.setIs_employer("" + datum.getIs_employer());
                contacts.setIs_personal("" + datum.getIs_personal());

                try {
                    String scanDate = datum.getScanDate();
                    if (Utilities.isEmpty(scanDate))
                        contacts.setScan_date(scanDate);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                RealmController.with(ContactSearchActivity.this).updateContacts(contacts);

            }
        } else {
            try {
                RealmController.with(ContactSearchActivity.this).clearContacts();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        try {
            ModelLoggedUser modelLoggedUser = qkPreferences.getLoggedUser();
            ModelLoggedData modelLoggedData = modelLoggedUser.getData();
            if (contactsPojo.getContactCount().getContactAll() != null)
                modelLoggedData.getContactCount().setContactAll(contactsPojo.getContactCount().getContactAll());

            if (contactsPojo.getContactCount().getContactWeek() != null)
                modelLoggedData.getContactCount().setContactWeek(contactsPojo.getContactCount().getContactWeek());

            if (contactsPojo.getContactCount().getContactMonth() != null)
                modelLoggedData.getContactCount().setContactMonth(contactsPojo.getContactCount().getContactMonth());

            modelLoggedUser.setData(modelLoggedData);
            qkPreferences.storeLoggedUser(modelLoggedUser);

        } catch (Exception e) {
            e.printStackTrace();
        }

        GetListFromLocal(false);
    }

}
