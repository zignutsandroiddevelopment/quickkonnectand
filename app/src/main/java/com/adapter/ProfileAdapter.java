package com.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.interfaces.ProfileSelection;
import com.quickkonnect.R;
import com.realmtable.MyProfiles;
import com.utilities.Utilities;

import java.util.List;

/**
 * Created by ZTLAB-12 on 07-05-2018.
 */

public class ProfileAdapter extends RecyclerView.Adapter<ProfileAdapter.ViewHolder> {

    private List<MyProfiles> MyProfilesList;
    private Context context;
    private ProfileSelection profileSelection;
    public int posselected;
    public boolean isfromrec;

    public ProfileAdapter(Activity activity, List<MyProfiles> list, ProfileSelection profileSelection, int posselected, boolean isfromrec) {
        context = activity;
        this.profileSelection = profileSelection;
        this.MyProfilesList = list;
        this.posselected = posselected;
        this.isfromrec = isfromrec;
    }

    @Override
    public ProfileAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_only_textview, null);
        ProfileAdapter.ViewHolder viewHolder = new ProfileAdapter.ViewHolder(itemLayoutView, profileSelection, MyProfilesList);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ProfileAdapter.ViewHolder holder, int position) {
        try {
            MyProfiles contactsList = MyProfilesList.get(position);

            if (Utilities.isEmpty(contactsList.getName())) {
                holder.tv_name.setText(contactsList.getName());
            } else {
                holder.tv_name.setText("");
            }

            try {
                if (isfromrec) {
                    if (posselected == position) {
                        holder.imageView.setImageResource(R.drawable.selected);
                    } else {
                        holder.imageView.setImageResource(R.drawable.notcheckedfilter);
                    }
                } else {
                    holder.imageView.setImageResource(R.drawable.notcheckedfilter);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return MyProfilesList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView tv_name;
        private ImageView imageView;
        List<MyProfiles> MyProfilesList;
        ProfileSelection profileSelection;

        public ViewHolder(View itemLayoutView, ProfileSelection profileSelection, List<MyProfiles> MyProfilesList) {
            super(itemLayoutView);

            this.MyProfilesList = MyProfilesList;
            this.profileSelection = profileSelection;
            tv_name = itemLayoutView.findViewById(R.id.tv_cot);
            imageView = itemLayoutView.findViewById(R.id.imag_select_rec);

            tv_name.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int pos = getAdapterPosition();
            MyProfiles current = MyProfilesList.get(pos);
            switch (view.getId()) {
                case R.id.tv_cot:
                    profileSelection.onProfileSelected(pos, current, current.getName());
                    break;
            }
        }
    }
}
