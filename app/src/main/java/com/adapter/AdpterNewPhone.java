package com.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.interfaces.AddRemevoItem;
import com.interfaces.OnSingleEdit;
import com.models.createprofile.Phone;
import com.quickkonnect.Accept_Employee;
import com.quickkonnect.R;
import com.utilities.Utilities;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ZTLAB-12 on 12-05-2018.
 */

public class AdpterNewPhone extends RecyclerView.Adapter<AdpterNewPhone.ViewHolder> {

    private Context context;
    public ArrayList<String> phones;
    private AddRemevoItem addRemevoItem;

    public AdpterNewPhone(Context context, ArrayList<String> phones, AddRemevoItem addRemevoItem) {
        this.context = context;
        this.addRemevoItem = addRemevoItem;
        this.phones = phones;
    }

    @Override
    public AdpterNewPhone.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.edit_with_plus_new, parent,false);
        AdpterNewPhone.ViewHolder viewHolder = new AdpterNewPhone.ViewHolder(itemLayoutView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final AdpterNewPhone.ViewHolder holder, final int position) {
        final String phone = phones.get(position);
        holder.edit.setText(phone);
        if(position == phones.size()){
            holder.add_remove.setImageResource(R.drawable.ic_add_white);
        }

        holder.edit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                phones.add(position,holder.edit.getText().toString()+"");
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });


        holder.add_remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addRemevoItem.onAddRemove(position, AddRemevoItem.TYPE_PHONE);
            }
        });
    }

    @Override
    public int getItemCount() {
        return phones.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        EditText edit;
        ImageView add_remove;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);

            edit = itemLayoutView.findViewById(R.id.edittext_main);
            //add_remove = itemLayoutView.findViewById(R.id.img_add_remove);

        }
    }
}
