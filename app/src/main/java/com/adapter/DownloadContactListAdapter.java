package com.adapter;

import android.app.Activity;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.quickkonnect.PojoClasses;
import com.quickkonnect.R;

import java.util.ArrayList;

/**
 * Created by ZTLAB03 on 17-11-2017.
 */

public class DownloadContactListAdapter extends RecyclerView.Adapter<DownloadContactListAdapter.ViewHolder> {

    ArrayList<PojoClasses.ContactInfo> contactInfos;
    static Activity mActivity;

    public static ArrayList<Integer> tempArray;

    public DownloadContactListAdapter(Activity activity, ArrayList<PojoClasses.ContactInfo> list) {
        this.contactInfos = list;
        mActivity = activity;

        tempArray = new ArrayList<>();


    }

    @Override
    public DownloadContactListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.custom_download_contact_list_adapter, null);

//        mSharedPreferences = mActivity.getSharedPreferences(SharedPreference.PREF_NAME, 0);

        // create ViewHolder

        DownloadContactListAdapter.ViewHolder viewHolder = new DownloadContactListAdapter.ViewHolder(itemLayoutView);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final DownloadContactListAdapter.ViewHolder holder, int position) {

        PojoClasses.ContactInfo contactInfo = contactInfos.get(position);

        if (contactInfo.getContact_type() != null && !contactInfo.getContact_type().isEmpty() && !contactInfo.getContact_type().equals("null")) {

            String first = "<font color='#414141'>Contact Type: </font>";
            String sourceString = "<b>" + first + "</b> " + contactInfo.getContact_type();

            //tv_register.setText(Html.fromHtml(first + next));
            holder.tv_contact_type.setText(Html.fromHtml(sourceString));
        } else {
            holder.tv_contact_type.setText("");
            holder.tv_contact_type.setVisibility(View.GONE);
        }

        if (contactInfo.getEmail() != null && !contactInfo.getEmail().isEmpty() && !contactInfo.getEmail().equals("null")) {
            //String first = "<font color='#414141'>Email</font>";
            //String sourceString = "<b>" +  first  + "</b> " + contactInfo.getEmail();
            holder.tv_contact_email.setText(contactInfo.getEmail());
        } else {
            holder.tv_contact_email.setText("");
            holder.tv_contact_email.setVisibility(View.GONE);
        }

        if (contactInfo.getPhone() != null && !contactInfo.getPhone().isEmpty() && !contactInfo.getPhone().equals("null")) {
            //String first = "<font color='#414141'>Phone</font>";
            //String sourceString = "<b>" +  first  + "</b> " + contactInfo.getPhone();
            if (contactInfo.getContact_type() != null && !contactInfo.getContact_type().isEmpty() && !contactInfo.getContact_type().equals("null")) {
                holder.tv_contact_phone.setText(contactInfo.getPhone() + " (" + contactInfo.getContact_type() + ")");
            } else {
                holder.tv_contact_phone.setText(contactInfo.getPhone());
            }
        } else {
            holder.tv_contact_phone.setText("");
            holder.tv_contact_phone.setVisibility(View.GONE);
        }


        holder.checkBox.setTag(contactInfo);

        // holder.checkBox.setOnClickListener(this);

        holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {


                if (isChecked) {
                    PojoClasses.ContactInfo contactInfo = (PojoClasses.ContactInfo) buttonView.getTag();
                    Log.d("contact", contactInfo + "");

                    tempArray.add(contactInfo.getContact_id());


                } else {

                    PojoClasses.ContactInfo contactInfo = (PojoClasses.ContactInfo) buttonView.getTag();

                    for (int i = 0; i < tempArray.size(); i++) {
                        if (tempArray.get(i) == contactInfo.getContact_id()) {
                            tempArray.remove(i);
                        }
                    }
                }

                // holder.tv_contact_phone.performClick();
                Log.d("final array", tempArray + "");


            }
        });


        Typeface lato_regular = Typeface.createFromAsset(mActivity.getAssets(), "fonts/latoregular.ttf");
        Typeface lato_bold = Typeface.createFromAsset(mActivity.getAssets(), "fonts/latobold.ttf");
        Typeface semi_bold = Typeface.createFromAsset(mActivity.getAssets(), "fonts/latosemibold.ttf");

        holder.tv_contact_type.setTypeface(semi_bold);
        holder.tv_contact_email.setTypeface(semi_bold);
        holder.tv_contact_phone.setTypeface(semi_bold);


    }


    @Override
    public int getItemCount() {
        return contactInfos.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_contact_type, tv_contact_email, tv_contact_phone;
        public CheckBox checkBox;


        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            tv_contact_type = itemLayoutView.findViewById(R.id.tv_contact_type);
            tv_contact_email = itemLayoutView.findViewById(R.id.tv_contact_email);
            tv_contact_phone = itemLayoutView.findViewById(R.id.tv_contact_phone);
            checkBox = itemLayoutView.findViewById(R.id.checkBox);

        }
    }
}