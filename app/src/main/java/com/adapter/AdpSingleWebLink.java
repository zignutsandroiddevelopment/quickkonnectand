package com.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.interfaces.OnSingleEdit;
import com.models.createprofile.Weblink;
import com.quickkonnect.QuickKonnect_Webview;
import com.quickkonnect.R;
import com.utilities.Utilities;

import java.util.List;

/**
 * Created by ZTLAB-10 on 22-02-2018.
 */

public class AdpSingleWebLink extends RecyclerView.Adapter<AdpSingleWebLink.ViewHolder> {

    private Context context;
    private List<Weblink> socialMediumList;
    private OnSingleEdit onSingleEdit;
    private boolean isEditenable = false;
    private String visited_user_id = "0";
    private String visited_profile_id = "0";

    public AdpSingleWebLink(Context context, List<Weblink> socialMediumList, OnSingleEdit onSingleEdit, boolean isEditenable, String visited_user_id, String visited_profile_id) {
        this.context = context;
        this.onSingleEdit = onSingleEdit;
        this.isEditenable = isEditenable;
        this.socialMediumList = socialMediumList;
        this.visited_user_id = visited_user_id;
        this.visited_profile_id = visited_profile_id;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_founders, null);
        AdpSingleWebLink.ViewHolder viewHolder = new AdpSingleWebLink.ViewHolder(itemLayoutView);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        final Weblink weblink = socialMediumList.get(position);
        if (weblink.getName() != null && Utilities.isEmpty(weblink.getName())) {
            holder.tv_name.setText(weblink.getName());
        } else holder.tv_name.setVisibility(View.GONE);

        holder.tv_designation.setText(Html.fromHtml("<font color='#4286f4'><u>Open Web Link</u></font>"));

        if (isEditenable)
            holder.imgDelete.setVisibility(View.VISIBLE);
        else
            holder.imgDelete.setVisibility(View.GONE);

        holder.imgDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isEditenable)
                    onSingleEdit.getEditSingle(position, weblink.getName(), OnSingleEdit.DELETE, OnSingleEdit.TYPE_WEBLINK);
            }
        });

        holder.llData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isEditenable)
                    onSingleEdit.getEditSingle(position, weblink.getName(), OnSingleEdit.EDIT, OnSingleEdit.TYPE_WEBLINK);
            }
        });

        holder.tv_designation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (weblink != null && weblink.getName() != null) {
                    String publicationstr = weblink.getName();
                    if (!publicationstr.contains("http:"))
                        publicationstr = "http://" + publicationstr;
                    context.startActivity(new Intent(context, QuickKonnect_Webview.class)
                            .putExtra("publication", "" + publicationstr)
                            .putExtra("title", "Web Link")
                            .putExtra("VISITED_USER_ID", visited_user_id + "")
                            .putExtra("PROFILE_ID", visited_profile_id + "")
                            .putExtra("TYPE", "W"));

                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return socialMediumList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView tv_name, tv_designation;
        private LinearLayout imgDelete, llData;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            tv_name = itemLayoutView.findViewById(R.id.tv_name);
            tv_designation = itemLayoutView.findViewById(R.id.tv_designation);
            imgDelete = itemLayoutView.findViewById(R.id.imgDelete);
            llData = itemLayoutView.findViewById(R.id.llData);
        }
    }
}
