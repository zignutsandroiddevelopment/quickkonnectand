package com.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.interfaces.OnCountrySelect;
import com.models.ModelCountry;
import com.quickkonnect.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ZTLAB03 on 01-01-2018.
 */

public class CountryAdapter extends BaseAdapter implements Filterable {

    List<ModelCountry> contactsLists;
    List<ModelCountry> contactsListsOrigi;
    static Activity mActivity;
    private SearchCountryFilter searchCountryFilter;
    private OnCountrySelect onCountrySelect;


    public CountryAdapter(Activity activity, List<ModelCountry> list, OnCountrySelect onCountrySelect) {
        this.contactsLists = list;
        this.contactsListsOrigi = list;
        mActivity = activity;
        this.onCountrySelect = onCountrySelect;
        searchCountryFilter = new SearchCountryFilter();
    }

    @Override
    public int getCount() {
        return contactsLists.size();
    }

    @Override
    public Object getItem(int i) {
        return contactsLists.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        ViewHolder viewHolder = null;

        if (view == null) {

            view = LayoutInflater.from(mActivity.getApplicationContext()).inflate(R.layout.spinner_textview, null);

            viewHolder = new ViewHolder(view);

            view.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder) view.getTag();
        }

        final ModelCountry contactsList = contactsLists.get(i);
        if (contactsList.getCountry_name() != null && !contactsList.getCountry_name().isEmpty() && !contactsList.getCountry_name().equals("null")) {
            viewHolder.tv_name.setText(contactsList.getCountry_name() + "");
        } else {
            viewHolder.tv_name.setText("");
        }

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onCountrySelect.getCountry(contactsList);
            }
        });

        return view;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_name;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            tv_name = itemLayoutView.findViewById(R.id.tv_name);

        }
    }

    @Override
    public Filter getFilter() {
        return searchCountryFilter;
    }

    public class SearchCountryFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();

            // If the constraint (search string/pattern) is null
            // or its length is 0, i.e., its empty then
            // we just set the `values` property to the
            // original contacts list which contains all of them
            if (constraint == null || constraint.length() == 0) {
                results.values = contactsListsOrigi;
                results.count = contactsListsOrigi.size();
            } else {
                // Some search copnstraint has been passed
                // so let's filter accordingly
                List<ModelCountry> filteredContacts = new ArrayList<>();

                // We'll go through all the contacts and see
                // if they contain the supplied string
                for (ModelCountry c : contactsListsOrigi) {
                    if (c.getCountry_name().toUpperCase().contains(constraint.toString().toUpperCase())) {
                        // if `contains` == true then add it
                        // to our filtered list
                        filteredContacts.add(c);
                    }
                }

                // Finally set the filtered values and size/count
                results.values = filteredContacts;
                results.count = filteredContacts.size();
            }

            // Return our FilterResults object
            return results;
        }

        @Override
        protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
            contactsLists = (List<ModelCountry>) filterResults.values;
            notifyDataSetChanged();
        }
    }
}
