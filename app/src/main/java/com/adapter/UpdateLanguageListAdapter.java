package com.adapter;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.activities.UpdateLanguageActivity;
import com.models.logindata.LanguageDetail;
import com.quickkonnect.R;
import com.utilities.Constants;

import java.util.List;

/**
 * Created by ZTLAB03 on 30-11-2017.
 */

public class UpdateLanguageListAdapter extends RecyclerView.Adapter<UpdateLanguageListAdapter.ViewHolder> implements View.OnClickListener {

    List<LanguageDetail> languages;
    static Activity mActivity;
    boolean isUpdate = false;

    public UpdateLanguageListAdapter(Activity activity, List<LanguageDetail> list, boolean isUpdate) {
        mActivity = activity;
        this.languages = list;
        this.isUpdate = isUpdate;
    }

    @Override
    public UpdateLanguageListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_language_view, null);
        UpdateLanguageListAdapter.ViewHolder viewHolder = new UpdateLanguageListAdapter.ViewHolder(itemLayoutView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(UpdateLanguageListAdapter.ViewHolder holder, int position) {

        LanguageDetail language = languages.get(position);

        if (language.getLangaugeName() != null && !language.getLangaugeName().isEmpty() && !language.getLangaugeName().equals("null")) {

            holder.tv_language_title.setText(language.getLangaugeName());
        } else {
            holder.tv_language_title.setText("");
            holder.tv_language_title.setVisibility(View.GONE);
        }

        if (language.getRating() != null && !language.getRating().isEmpty() && !language.getRating().equals("null")) {
            // holder.tv_language_title.setText(language.getRating());
            holder.rating.setRating(Float.parseFloat(language.getRating()));
        } else {
            //holder.tv_language_title.setText("");
        }

        holder.img_delete.setTag(language);
        holder.img_delete.setOnClickListener(this);

        holder.tv_language_title.setTag(language);
        holder.tv_language_title.setOnClickListener(this);

        holder.rating.setTag(language);
        holder.rating.setOnClickListener(this);

        if (isUpdate)
            holder.llEdit.setVisibility(View.VISIBLE);
        else
            holder.llEdit.setVisibility(View.GONE);

    }

    @Override
    public int getItemCount() {
        return languages.size();
    }

    @Override
    public void onClick(View view) {

        LanguageDetail language = (LanguageDetail) view.getTag();

        switch (view.getId()) {
            case R.id.img_delete:
            case R.id.tv_language_title:
            case R.id.rating:
                if (isUpdate) {
                    Intent intent = new Intent(mActivity, UpdateLanguageActivity.class);
                    intent.putExtra("language", language);
                    intent.putExtra(Constants.KEY_UPDATE, Constants.EDIT_DATA);
                    mActivity.startActivityForResult(intent, Constants.RCODE_UPDATEEDUCATION);
                }
                break;
        }
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_language_title;
        private LinearLayout llEdit;
        private RatingBar rating;
        private ImageView img_delete;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            tv_language_title = itemLayoutView.findViewById(R.id.tv_language_title);
            rating = itemLayoutView.findViewById(R.id.rating);
            llEdit = itemLayoutView.findViewById(R.id.llEdit);
            img_delete = itemLayoutView.findViewById(R.id.img_delete);
        }
    }
}
