package com.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.interfaces.OnContactClick;
import com.models.contacts.Datum;
import com.quickkonnect.CircleTransform;
import com.quickkonnect.PojoClasses;
import com.quickkonnect.R;
import com.squareup.picasso.Picasso;
import com.utilities.Constants;
import com.utilities.Utilities;

import java.util.ArrayList;

/**
 * Created by ZTLAB03 on 15-11-2017.
 */

public class ContactListAdapter extends RecyclerView.Adapter<ContactListAdapter.ViewHolder> implements View.OnClickListener {

    public ArrayList<Datum> contactsLists;
    private OnContactClick onContactClick;
    private Context context;

    public ContactListAdapter(Activity activity, ArrayList<Datum> list, OnContactClick onContactClick) {
        context = activity;
        this.contactsLists = list;
        this.onContactClick = onContactClick;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_contacts_list_adapter, null);

        ContactListAdapter.ViewHolder viewHolder = new ContactListAdapter.ViewHolder(itemLayoutView);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        Datum contactsList = contactsLists.get(position);

//        StringBuilder builder = new StringBuilder();
//        if (Utilities.isEmpty(contactsList.getFirstname()) && Utilities.isEmpty(contactsList.getLastname())) {
//            String cap = contactsList.getFirstname().substring(0, 1).toUpperCase() + contactsList.getFirstname().substring(1);
//            String cap2 = contactsList.getLastname().substring(0, 1).toUpperCase() + contactsList.getLastname().substring(1);
//            builder.append(cap + " ");
//            builder.append(cap2 + "");
//
//            holder.tv_name.setText(cap + " " + cap2);
//        } else if (Utilities.isEmpty(contactsList.getFirstname())) {
//            String cap = contactsList.getFirstname().substring(0, 1).toUpperCase() + contactsList.getFirstname().substring(1);
//            builder.append(cap + " ");
//            holder.tv_name.setText(cap);
//        } else {
//            holder.tv_name.setText("");
//        }

        if (Utilities.isEmpty(contactsList.getFirstname()))
            holder.tv_name.setText(contactsList.getScanUserName());

        if (Utilities.isEmpty(contactsList.getScanDate())) {
            String strCurrentDate = contactsList.getScanDate();
            String scanat = "<font color='#000000'>Scan at : </font>";
            String sourceString = "<b>" + scanat + "</b> <font color='#777777'>" + strCurrentDate + "</font>";
            holder.tv_date.setText(Html.fromHtml(sourceString));

        } else {
            holder.tv_date.setVisibility(View.GONE);
        }

        if (Utilities.isEmpty(contactsList.getScannedBy())) {

            holder.tv_Scan_by.setVisibility(View.VISIBLE);
            String str = contactsList.getScannedBy();
            String scanat = "<font color='#000000'>Scan by : </font>";
            String sourceString = "<b>" + scanat + "</b> <font color='#777777'>" + str + "</font>";
            holder.tv_Scan_by.setText(Html.fromHtml(sourceString));

        } else {
            holder.tv_Scan_by.setVisibility(View.GONE);
        }

        if (Utilities.isEmpty(contactsList.getScanUserProfileUrl())) {
            Picasso.with(context)
                    .load(contactsList.getScanUserProfileUrl())
                    .error(R.drawable.ic_default_profile_photo)
                    .placeholder(R.drawable.ic_default_profile_photo)
                    .transform(new CircleTransform())
                    .fit()
                    .into(holder.img_profile_pic);
        } else {
            holder.img_profile_pic.setImageResource(R.drawable.ic_default_profile_photo);
        }

        Typeface semi_bold = Typeface.createFromAsset(context.getAssets(), "fonts/latosemibold.ttf");

        holder.tv_name.setTypeface(semi_bold);

        holder.tv_name.setTag(contactsList);
        holder.img_next_arrow.setTag(contactsList);
        holder.tv_name.setOnClickListener(this);

        holder.img_profile_pic.setTag(contactsList);
        holder.img_profile_pic.setOnClickListener(this);
        holder.img_next_arrow.setOnClickListener(this);

        holder.tv_date.setTag(contactsList);
        holder.tv_date.setOnClickListener(this);

    }

    @Override
    public int getItemCount() {
        return contactsLists.size();
    }

    @Override
    public void onClick(View view) {

        Datum contact = (Datum) view.getTag();

        switch (view.getId()) {
            case R.id.tv_name:
            case R.id.img_profile_pic:
            case R.id.img_next_arrow:
            case R.id.tv_date:
                onContactClick.getContact(contact, Constants.TYPE_CLICK_ON_PROFILE_PIC);
                break;
        }
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_name, tv_date, tv_Scan_by;
        ImageView img_profile_pic, img_next_arrow, img_profile_pic_selected;
        LinearLayout lin_main_contact;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            tv_Scan_by = itemLayoutView.findViewById(R.id.tv_Scan_by);
            tv_name = itemLayoutView.findViewById(R.id.tv_name);
            tv_date = itemLayoutView.findViewById(R.id.tv_date);
            img_profile_pic = itemLayoutView.findViewById(R.id.img_profile_pic);
            img_next_arrow = itemLayoutView.findViewById(R.id.img_next_arrow);
            img_profile_pic_selected = itemLayoutView.findViewById(R.id.img_profile_pic_selected);
            lin_main_contact = itemLayoutView.findViewById(R.id.linear_main_contact);
        }
    }

    public void filterList(ArrayList<Datum> filterdNames) {
        this.contactsLists = filterdNames;
        notifyDataSetChanged();
    }
}
