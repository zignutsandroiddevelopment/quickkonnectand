package com.adapter;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.activities.UpdateExperienceActivity;
import com.quickkonnect.PojoClasses;
import com.quickkonnect.R;
import com.utilities.Constants;

import java.util.ArrayList;

/**
 * Created by ZTLAB03 on 16-11-2017.
 */

public class MatchProfileExperienceAdapter extends RecyclerView.Adapter<MatchProfileExperienceAdapter.ViewHolder> implements View.OnClickListener {

    ArrayList<PojoClasses.Experience> experiences;
    static Activity mActivity;

    public MatchProfileExperienceAdapter(Activity activity, ArrayList<PojoClasses.Experience> list) {
        this.experiences = list;
        mActivity = activity;

    }

    @Override
    public MatchProfileExperienceAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.match_profile_experience_adapter, null);
        // create ViewHolder

        MatchProfileExperienceAdapter.ViewHolder viewHolder = new MatchProfileExperienceAdapter.ViewHolder(itemLayoutView);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(MatchProfileExperienceAdapter.ViewHolder holder, int position) {

        PojoClasses.Experience  experience = experiences.get(position);

        if (experience.getTitle() != null && !experience.getTitle() .isEmpty() && !experience.getTitle() .equals("null")) {

            holder.tv_title.setText(experience.getTitle());
        }else {

            holder.tv_title.setText("Title");
            //holder.tv_title.setVisibility(View.GONE);
            //holder.linear_title.setVisibility(View.GONE);
            // holder.linear_edit.setGravity(Gravity.RIGHT);
        }

        if (experience.getCompany_name() != null && !experience.getCompany_name() .isEmpty() && !experience.getCompany_name() .equals("null")) {

            holder.tv_company_name.setText(experience.getCompany_name());
        }else {
            holder.tv_company_name.setText("");
            holder.tv_company_name.setVisibility(View.GONE);
        }

        if (experience.getLocation() != null && !experience.getLocation() .isEmpty() && !experience.getLocation() .equals("null")) {

            holder.tv_location.setText(experience.getLocation());
        }else {
            holder.tv_location.setText("");
            holder.tv_location.setVisibility(View.GONE);
        }

        if (experience.getStart_date() != null && !experience.getStart_date() .isEmpty() && !experience.getStart_date() .equals("null")) {

            holder.tv_startdate.setText(experience.getStart_date()+" - "+experience.getEnd_date());
        }else {
            holder.tv_startdate.setText("");
            holder.tv_startdate.setVisibility(View.GONE);
        }

        if (experience.getEnd_date() != null && !experience.getEnd_date() .isEmpty() && !experience.getEnd_date() .equals("null")) {

            holder.tv_enddate.setText(experience.getEnd_date());
        }else {
            holder.tv_enddate.setText("");
            holder.tv_enddate.setVisibility(View.GONE);
        }

        holder.img_delete.setTag(experience);
        holder.img_delete.setOnClickListener(this);

        holder.tv_title.setTag(experience);
        holder.tv_title.setOnClickListener(this);

        holder.tv_company_name.setTag(experience);
        holder.tv_company_name.setOnClickListener(this);

        holder.tv_location.setTag(experience);
        holder.tv_location.setOnClickListener(this);

        holder.tv_startdate.setTag(experience);
        holder.tv_startdate.setOnClickListener(this);

        holder.tv_enddate.setTag(experience);
        holder.tv_enddate.setOnClickListener(this);

        Typeface lato_regular = Typeface.createFromAsset(mActivity.getAssets(), "fonts/latoregular.ttf");
        Typeface lato_bold = Typeface.createFromAsset(mActivity.getAssets(), "fonts/latobold.ttf");
        Typeface semi_bold = Typeface.createFromAsset(mActivity.getAssets(), "fonts/latosemibold.ttf");

        holder.tv_title.setTypeface(semi_bold);




        holder.tv_company_name.setTypeface(lato_regular);
        holder.tv_location.setTypeface(lato_regular);
        holder.tv_startdate.setTypeface(lato_regular);


    }

    @Override
    public int getItemCount() {
        return experiences.size();
    }

    @Override
    public void onClick(View view) {
        PojoClasses.Experience expList = (PojoClasses.Experience) view.getTag();
        switch (view.getId()){
            case R.id.img_delete:
                //((UserProfileActivity)mActivity).DeleteExperience(view, expList.getExperience_id());
                Intent intent = new Intent(mActivity,UpdateExperienceActivity.class);
                intent.putExtra("experience_id",expList.getExperience_id());
                intent.putExtra("title",expList.getTitle());
                intent.putExtra("company",expList.getCompany_name());
                intent.putExtra("location",expList.getLocation());
                intent.putExtra("start_date",expList.getStart_date());
                intent.putExtra("end_date",expList.getEnd_date());
                intent.putExtra(Constants.KEY_UPDATE, Constants.EDIT_DATA);

                //mActivity.startActivity(intent);
                break;

            case R.id.tv_title:
            case R.id.tv_company_name:
            case R.id.tv_location:
            case R.id.tv_startdate:
            case R.id.tv_enddate:

                intent = new Intent(mActivity,UpdateExperienceActivity.class);
                intent.putExtra("experience_id",expList.getExperience_id());
                intent.putExtra("title",expList.getTitle());
                intent.putExtra("company",expList.getCompany_name());
                intent.putExtra("location",expList.getLocation());
                intent.putExtra("start_date",expList.getStart_date());
                intent.putExtra("end_date",expList.getEnd_date());
                intent.putExtra(Constants.KEY_UPDATE, Constants.EDIT_DATA);

                //mActivity.startActivity(intent);
                break;
        }





    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_title,tv_company_name,tv_location,tv_startdate,tv_enddate;
        ImageView img_delete;
        LinearLayout linear_title;
        LinearLayout linear_edit;


        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            tv_title = itemLayoutView.findViewById(R.id.tv_title);
            tv_company_name = itemLayoutView.findViewById(R.id.tv_company_name);
            tv_location = itemLayoutView.findViewById(R.id.tv_location);
            tv_startdate = itemLayoutView.findViewById(R.id.tv_startdate);
            tv_enddate = itemLayoutView.findViewById(R.id.tv_enddate);
            img_delete = itemLayoutView.findViewById(R.id.img_delete);
            linear_title = itemLayoutView.findViewById(R.id.linear_title);
            linear_edit = itemLayoutView.findViewById(R.id.linear_edit);



        }
    }
}