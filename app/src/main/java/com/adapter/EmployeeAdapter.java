package com.adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.interfaces.EmployeeClicked;
import com.quickkonnect.CircleTransform;
import com.quickkonnect.PojoClasses;
import com.quickkonnect.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by ZTLAB03 on 10-04-2018.
 */

public class EmployeeAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;
    EmployeeClicked employeeClicked;
    private ArrayList<PojoClasses.EmployeesList> employeeList;
    private Activity activity;
    String Reason = "", login_user_id = "";
    boolean isEdit;

    public EmployeeAdapter(String login_user_id, Activity activity, ArrayList<PojoClasses.EmployeesList> list, EmployeeClicked employeeClicked, boolean isEdit) {
        this.activity = activity;
        this.employeeList = list;
        this.login_user_id = login_user_id;
        this.employeeClicked = employeeClicked;
        this.isEdit = isEdit;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_child_employee, parent, false);
        Employees employees = new Employees(itemLayoutView);
        return employees;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof Employees) {
            final int pos = position;
            final Employees empholder = (Employees) holder;
            final PojoClasses.EmployeesList current = employeeList.get(pos);
            try {
                PojoClasses.EmployeesList.User currentuser = new Gson().fromJson(current.getUser1(), PojoClasses.EmployeesList.User.class);
                if (currentuser.getFirstname() != null && currentuser.getLastname() != null && !currentuser.getFirstname().equalsIgnoreCase("") && !currentuser.getFirstname().equalsIgnoreCase("null"))
                    empholder.tv_name.setText(currentuser.getFirstname() + " " + currentuser.getLastname());
                else
                    empholder.tv_name.setVisibility(View.GONE);

                empholder.tv_email_emp.setVisibility(View.GONE);
                if (currentuser.getProfilePic() != null && !currentuser.getProfilePic().isEmpty() && !currentuser.getProfilePic().equals("null")) {
                    Picasso.with(activity)
                            .load(currentuser.getProfilePic())
                            .error(R.drawable.ic_default_profile_photo)
                            .placeholder(R.drawable.ic_default_profile_photo)
                            .transform(new CircleTransform())
                            .fit()
                            .into(empholder.img_profile_pic);
                } else {
                    empholder.img_profile_pic.setImageResource(R.drawable.ic_default_profile_photo);
                }

                empholder.img_next_menu.setImageResource(R.drawable.ic_more_options);

                if (isEdit) {
                    empholder.img_next_menu.setVisibility(View.VISIBLE);
                } else {
                    empholder.img_next_menu.setVisibility(View.GONE);
                }
                empholder.img_next_menu.setTag(empholder);


                if (isEdit) {
                    empholder.img_next_menu.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            employeeClicked.onEmployeeClicked("IMG_MORE", pos, current.getId() + "", empholder.tv_name.getText().toString() + "", current.getStatus() + "", current.getRole() + "", current.getRole_name() + "");
                        }
                    });
                    empholder.img_profile_pic.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            employeeClicked.onEmployeeClicked("EDIT", pos, current.getId() + "", empholder.tv_name.getText().toString() + "", "", "", "");
                        }
                    });
                    empholder.tv_name.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            employeeClicked.onEmployeeClicked("EDIT", pos, current.getId() + "", empholder.tv_name.getText().toString() + "", "", "", "");
                        }
                    });
                }

                if (current.getStatus() != null) {
                    switch (current.getStatus()) {
                        case "A":
                            empholder.img_profile_strip.setVisibility(View.VISIBLE);
                            empholder.img_profile_strip.setImageResource(R.drawable.noti_approve);
                            empholder.tv_email_emp.setVisibility(View.VISIBLE);
                            break;
                        case "R":
                            empholder.img_profile_strip.setVisibility(View.VISIBLE);
                            empholder.img_profile_strip.setImageResource(R.drawable.noti_reject);
                            empholder.tv_email_emp.setVisibility(View.GONE);
                            break;
                        case "P":
                            empholder.img_profile_strip.setVisibility(View.GONE);
                            //myViewHolder.img_profile_strip.setVisibility(View.GONE);
                            empholder.tv_email_emp.setVisibility(View.GONE);
                            break;
                    }
                }
                if (current.getRole_name() != null && !current.getRole_name().equalsIgnoreCase("null")) {
                    if (current.getRole_name().equalsIgnoreCase("None")) {
                        empholder.tv_email_emp.setVisibility(View.GONE);
                    } else {
                        empholder.tv_email_emp.setText(current.getRole_name() + "");
                    }
                } else {
                    empholder.tv_email_emp.setVisibility(View.GONE);
                }

                if (current.getStatus().equalsIgnoreCase("A")) {
                    empholder.tv_status_verification.setVisibility(View.GONE);
                } else if (current.getStatus().equalsIgnoreCase("P")) {
                    empholder.tv_status_verification.setVisibility(View.VISIBLE);
                    empholder.tv_status_verification.setText("Pending");
                } else if (current.getStatus().equalsIgnoreCase("R")) {
                    switch (current.getReason()) {
                        case "1":
                            Reason = "My details are not correct.";
                            break;
                        case "2":
                            Reason = "I am not the concerned person.";
                            break;
                        case "3":
                            Reason = "I am not interested.";
                            break;
                        default:
                            Reason = "Not Specified";
                            break;
                    }
                    String text = "<font color='#FF0000'>Rejected &nbsp;&nbsp;&nbsp;</font><font color='#d1cfcf'>Details</font>";
                    empholder.tv_status_verification.setVisibility(View.VISIBLE);
                    empholder.tv_status_verification.setText(Html.fromHtml(text));
                    empholder.tv_status_verification.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            new AlertDialog.Builder(activity)
                                    .setTitle("Reason of rejection")
                                    .setMessage(Reason)
                                    .setCancelable(false)
                                    .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                        }
                                    }).show();
                        }
                    });
                } else {
                    empholder.tv_status_verification.setVisibility(View.GONE);
                }

                if (current.getRole() != null && current.getRole().equalsIgnoreCase("4")) {
                    empholder.tv_email_emp.setVisibility(View.VISIBLE);
                    empholder.tv_email_emp.setText("Owner");
                    empholder.img_next_menu.setVisibility(View.GONE);
                } else if (current.getUserId().equalsIgnoreCase(login_user_id + "")) {
                    empholder.img_next_menu.setVisibility(View.GONE);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            //cast holder to VHItem and set data
        } else if (holder instanceof Header) {
            Header headerholder = (Header) holder;
            /*headerholder.tv_name.setText("Add Employees");
            headerholder.tv_status_verification.setVisibility(View.GONE);
            headerholder.img_profile_pic.setImageResource(R.drawable.ic_add);
            headerholder.img_next_menu.setVisibility(View.GONE);
            headerholder.img_profile_strip.setVisibility(View.GONE); */
            headerholder.linmain.setTag(headerholder);
            headerholder.linmain.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    employeeClicked.onEmployeeClicked("HEADER", position, "", "", "", "", "");
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return employeeList.size();
    }

    @Override
    public int getItemViewType(int position) {
//        if (position == 0)
//            return TYPE_HEADER;
//        else
        return TYPE_ITEM;
    }

    private boolean isPositionHeader(int position) {
        return position == 0;
    }

    class Header extends RecyclerView.ViewHolder {

        //TextView tv_name, tv_status_verification;
        //ImageView img_profile_pic, img_next_menu, img_profile_strip;
        LinearLayout linmain;

        public Header(View itemView) {
            super(itemView);
           /* tv_name = itemView.findViewById(R.id.tv_name_mqp);
            img_profile_pic = itemView.findViewById(R.id.img_profile_pic_mqp);
            img_profile_strip = itemView.findViewById(R.id.img_profile_strip);
            img_next_menu = itemView.findViewById(R.id.img_next_arrow_mqp);
            tv_status_verification = itemView.findViewById(R.id.tv_status_verification);*/
            linmain = itemView.findViewById(R.id.lin_header_emp);
        }
    }

    class Employees extends RecyclerView.ViewHolder {

        TextView tv_name, tv_status_verification, tv_email_emp;
        ImageView img_profile_pic, img_next_menu, img_profile_strip;

        public Employees(View itemView) {
            super(itemView);
            tv_name = itemView.findViewById(R.id.tv_name_mqp);
            img_profile_pic = itemView.findViewById(R.id.img_profile_pic_mqp);
            tv_email_emp = itemView.findViewById(R.id.tv_email_emp);
            img_profile_strip = itemView.findViewById(R.id.img_profile_strip);
            img_next_menu = itemView.findViewById(R.id.img_next_arrow_mqp);
            tv_status_verification = itemView.findViewById(R.id.tv_status_verification);
        }

    }

    public void filterList(ArrayList<PojoClasses.EmployeesList> filterdNames) {
        this.employeeList = filterdNames;
        notifyDataSetChanged();
    }
}