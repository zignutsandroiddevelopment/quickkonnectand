package com.adapter;

import android.animation.Animator;
import android.content.Context;
import android.graphics.Color;
import android.support.v4.view.PagerAdapter;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.interfaces.OnQkTagInfo;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.quickkonnect.R;
import com.realmtable.MyProfiles;
import com.utilities.OutlineContainer;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ZTLAB-12 on 07-06-2018.
 */

public class MainAdapter extends PagerAdapter {

    private List<MyProfiles> myProfileslist;
    private DisplayImageOptions imageOptions;
    private OnQkTagInfo onQkTagInfo;
    private ImageLoader imageLoader;
    private List<String> gradient_array;
    private Context context;

    private ImageView imgTags;
    private RelativeLayout rel_qk_cover;

    public MainAdapter(Context context, List<MyProfiles> myProfileslist, OnQkTagInfo onQkTagInfo) {
        Log.e("ProfilesSize", "" + myProfileslist.size());
        this.myProfileslist = myProfileslist;
        this.onQkTagInfo = onQkTagInfo;
        this.context = context;
        this.gradient_array = new ArrayList<>();
        this.gradient_array.add("1");
        this.gradient_array.add("2");
        this.gradient_array.add("3");
        this.gradient_array.add("4");
        this.gradient_array.add("5");
        this.gradient_array.add("6");
        this.gradient_array.add("7");
        this.gradient_array.add("8");
        this.gradient_array.add("9");
        this.gradient_array.add("10");
        imageLoader = ImageLoader.getInstance();
        imageOptions = new DisplayImageOptions.Builder()
                .showImageForEmptyUri(R.drawable.ic_logo_blue_qk)
                .showImageOnFail(R.drawable.ic_logo_blue_qk)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .build();
    }

    @Override
    public int getItemPosition(Object object) {
        return PagerAdapter.POSITION_NONE;
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {

        final View itemLayoutView = LayoutInflater.from(context).inflate(R.layout.frag_qktags, null);

        itemLayoutView.setTag(position);

        imgTags = itemLayoutView.findViewById(R.id.imgTags);
        TextView tv_cardname = itemLayoutView.findViewById(R.id.tv_cardname);
        TextView tv_employee_text = itemLayoutView.findViewById(R.id.tv_employee_text);
        LinearLayout lin_name_emp = itemLayoutView.findViewById(R.id.lin_name_emp);
        //final LinearLayout lin_background = itemLayoutView.findViewById(R.id.lin_qk_background);
        boolean isShowing = false;

        rel_qk_cover = itemLayoutView.findViewById(R.id.rel_qk_cover);
        rel_qk_cover.setTag(position);

        try {

            final MyProfiles myProfiles = myProfileslist.get(position);
            if (myProfiles != null && myProfiles.getName() != null) {
                //String upperString = myProfiles.getName().substring(0, 1).toUpperCase() + myProfiles.getName().substring(1);
                String upperString = myProfiles.getName();
                // if (myProfiles.getRole() != null && myProfiles.getRole().equalsIgnoreCase("4")) {
                //     tv_cardname.setText(upperString + "(Owner)");
                // } else {

                //tv_cardname.setText(upperString);

                // }
                /*
                * Type = 4 = Employee profile
                * Role = 4 = Owner
                * */
                tv_cardname.setText(upperString + "");
                if (myProfiles.getType() != null && myProfiles.getType().equalsIgnoreCase("4")) {

                    if (myProfiles.getRole() != null && myProfiles.getRole().equalsIgnoreCase("4")) {
                        tv_employee_text.setVisibility(View.VISIBLE);
                        tv_employee_text.setText("Owner");
                    } else {
                        tv_employee_text.setVisibility(View.VISIBLE);
                        tv_employee_text.setText("Employee");
                    }

                } else {
                    tv_employee_text.setVisibility(View.GONE);
                }

            }
            JSONObject jsonObject = new JSONObject(myProfiles.getQktaginfo());
            if (jsonObject.optString("qr_code") != null && !jsonObject.optString("qr_code").isEmpty())
                imageLoader.displayImage(jsonObject.optString("qr_code"), imgTags, imageOptions);

            rel_qk_cover.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    RelativeLayout rl = (RelativeLayout) view;
                    int tag = (int) rl.getTag();
                    onQkTagInfo.OnQKTaginfo(myProfiles, tag, itemLayoutView);
                    //openQKTagsOptions(myProfiles);
                    /* if (isFabMenuOpen)
                        collapseFabMenu();
                    else
                        expandFabMenu();*/
                    //openQkOptions(tag, view, itemLayoutView);
                }
            });

            /*
            for setting background

            try {
                if (jsonObject.optString("background_gradient") != null && !jsonObject.optString("background_gradient").isEmpty()) {
                    String background_gradient = myProfiles.getBackground_gradient();
                    //Log.e("220", "" + background_gradient);
                    if (isGradient(background_gradient)) {
                        lin_background.setBackgroundResource(getBackgroundRes(Integer.parseInt(myProfiles.getBackground_gradient())));
                    } else {
                        lin_background.setBackgroundColor(Color.parseColor(background_gradient));
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }*/

        } catch (Exception e) {
            e.printStackTrace();
        }

        container.addView(itemLayoutView, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

        /* try {
            mJazzy.setObjectForPosition(itemLayoutView, position);
        } catch (Exception e) {
            e.printStackTrace();
        }*/

        return itemLayoutView;
    }

  /*  private void openQkOptions(int position, View view, View mainview) {
    }*/


    @Override
    public void destroyItem(ViewGroup container, int position, Object obj) {

        /*container.invalidate();
        try {
            container.removeView(mJazzy.findViewFromObject(position));
        } catch (Exception e) {
            e.printStackTrace();
        }*/
    }

    @Override
    public int getCount() {
        return myProfileslist.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object obj) {
        if (view instanceof OutlineContainer) {
            return ((OutlineContainer) view).getChildAt(0) == obj;
        } else {
            return view == obj;
        }
    }

    public boolean isGradient(String background_gradient) {
        for (String s : gradient_array) {
            if (s.equals(background_gradient))
                return true;
        }
        return false;
    }

   /* private int getBackgroundRes(int current_selected) {
        if (current_selected != 0) {
            if (current_selected == 1)
                return R.drawable.img_1;
            else if (current_selected == 2)
                return R.drawable.img_2;
            else if (current_selected == 3)
                return R.drawable.img_3;
            else if (current_selected == 4)
                return R.drawable.img_4;
            else if (current_selected == 5)
                return R.drawable.img_5;
            else if (current_selected == 6)
                return R.drawable.img_6;
            else if (current_selected == 7)
                return R.drawable.img_7;
            else if (current_selected == 8)
                return R.drawable.img_8;
            else if (current_selected == 9)
                return R.drawable.img_9;
            else if (current_selected == 10)
                return R.drawable.img_10;
            else
                return R.drawable.img_1;
        }
        return R.drawable.img_1;
    }*/
}