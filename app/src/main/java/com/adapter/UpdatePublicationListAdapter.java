package com.adapter;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.activities.UpdatePublicationActivity;
import com.models.logindata.PublicationDetail;
import com.quickkonnect.QuickKonnect_Webview;
import com.quickkonnect.R;
import com.utilities.Constants;
import com.utilities.Utilities;

import java.util.List;

/**
 * Created by ZTLAB03 on 30-11-2017.
 */

public class UpdatePublicationListAdapter extends RecyclerView.Adapter<UpdatePublicationListAdapter.ViewHolder> implements View.OnClickListener {

    List<PublicationDetail> publications;
    static Activity mActivity;
    boolean isUpdate = false;
    private String visited_user_id = "0";
    private String visited_profile_id = "0";

    public UpdatePublicationListAdapter(Activity activity, List<PublicationDetail> list, boolean isUpdate, String visited_user_id) {
        mActivity = activity;
        this.publications = list;
        this.isUpdate = isUpdate;
        this.visited_user_id = visited_user_id;

    }

    @Override
    public UpdatePublicationListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.custom_publication_view, null);

//        mSharedPreferences = mActivity.getSharedPreferences(SharedPreference.PREF_NAME, 0);

        // create ViewHolder

        UpdatePublicationListAdapter.ViewHolder viewHolder = new UpdatePublicationListAdapter.ViewHolder(itemLayoutView);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(UpdatePublicationListAdapter.ViewHolder holder, int position) {

        PublicationDetail publication = publications.get(position);

        if (publication.getPublicationTitle() != null && !publication.getPublicationTitle().isEmpty() && !publication.getPublicationTitle().equals("null")) {
            holder.tv_publication_title.setText(publication.getPublicationTitle());
        } else {
            holder.tv_publication_title.setText("");
            holder.tv_publication_title.setVisibility(View.GONE);
        }

        if (publication.getPublisherName() != null && !publication.getPublisherName().isEmpty() && !publication.getPublisherName().equals("null")) {
            holder.tv_publisher_name.setText(publication.getPublisherName());
        } else {
            holder.tv_publisher_name.setText("");
            holder.tv_publisher_name.setVisibility(View.GONE);
        }

        if (publication.getDate() != null && !publication.getDate().isEmpty() && !publication.getDate().equals("null")) {
            holder.tv_date.setText(publication.getDate());
        } else {
            holder.tv_date.setText("");
            holder.tv_date.setVisibility(View.GONE);
        }

        if (Utilities.isEmpty(publication.getPublicationUrl())) {
            holder.tv_openurl.setVisibility(View.VISIBLE);
        } else
            holder.tv_openurl.setVisibility(View.GONE);

        holder.tv_openurl.setTag(publication);

        holder.tv_publication_title.setTag(publication);
        holder.tv_publication_title.setOnClickListener(this);


        holder.tv_publisher_name.setTag(publication);
        holder.tv_publisher_name.setOnClickListener(this);

        holder.tv_date.setTag(publication);
        holder.tv_date.setOnClickListener(this);

        holder.img_delete.setTag(publication);
        holder.img_delete.setOnClickListener(this);

        holder.tv_openurl.setOnClickListener(this);

        if (isUpdate)
            holder.llEdit.setVisibility(View.VISIBLE);
        else
            holder.llEdit.setVisibility(View.GONE);

    }

    @Override
    public int getItemCount() {
        return publications.size();
    }

    @Override
    public void onClick(View view) {

        PublicationDetail publication = (PublicationDetail) view.getTag();

        switch (view.getId()) {
            case R.id.img_delete:
            case R.id.tv_publisher_name:
            case R.id.tv_date:
                if (isUpdate) {
                    Intent intent = new Intent(mActivity, UpdatePublicationActivity.class);
                    intent.putExtra("publication", publication);
                    intent.putExtra(Constants.KEY_UPDATE, Constants.EDIT_DATA);
                    mActivity.startActivityForResult(intent, Constants.RCODE_UPDATEEDUCATION);
                }
                break;
            case R.id.tv_openurl:
                if (publication != null) {
                    String publicationstr = publication.getPublicationUrl();
                    if (!publicationstr.contains("http:"))
                        publicationstr = "http://" + publicationstr;
                    mActivity.startActivity(new Intent(mActivity, QuickKonnect_Webview.class)
                            .putExtra("publication", "" + publicationstr)
                            .putExtra("title", "" + publication.getPublicationTitle())
                            .putExtra("VISITED_USER_ID", visited_user_id + "")
                            .putExtra("PROFILE_ID", visited_profile_id + "")
                            .putExtra("TYPE", "P"));

                }
                break;
        }
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_publication_title, tv_publisher_name, tv_date, tv_openurl;
        ImageView img_delete;
        LinearLayout llEdit;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            tv_openurl = itemLayoutView.findViewById(R.id.tv_openurl);
            tv_publication_title = itemLayoutView.findViewById(R.id.tv_publication_title);
            tv_publisher_name = itemLayoutView.findViewById(R.id.tv_publisher_name);
            tv_date = itemLayoutView.findViewById(R.id.tv_date);
            img_delete = itemLayoutView.findViewById(R.id.img_delete);
            llEdit = itemLayoutView.findViewById(R.id.llEdit);

        }
    }
}

