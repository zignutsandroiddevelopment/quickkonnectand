package com.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.interfaces.EmployeeItemSelected;
import com.models.createprofile.Email;
import com.quickkonnect.R;
import com.utilities.Utilities;

import java.util.ArrayList;

/**
 * Created by ZTLAB-12 on 15-05-2018.
 */

public class Adp_Emp_Phone extends RecyclerView.Adapter<Adp_Emp_Phone.ViewHolder> {

    private Context context;
    private ArrayList<String> phoneList;
    //private EmployeeItemSelected employeeItemSelected;
    private int type;

    public Adp_Emp_Phone(Context context, ArrayList<String> phoneList, int type) {
        this.context = context;
        this.phoneList = phoneList;
        this.type = type;
        //this.employeeItemSelected = employeeItemSelected;
    }

    @Override
    public Adp_Emp_Phone.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_employee_item, null);
        Adp_Emp_Phone.ViewHolder viewHolder = new Adp_Emp_Phone.ViewHolder(itemLayoutView, phoneList, type);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        String item = phoneList.get(position);

        if (item.equalsIgnoreCase("") && item.equalsIgnoreCase("null")) {
            holder.item_name.setVisibility(View.GONE);
        } else {
            holder.item_name.setText(item + "");
        }
        holder.edit.setVisibility(View.GONE);
    }

    @Override
    public int getItemCount() {
        return phoneList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView item_name;
        private ImageView edit;
        private ArrayList<String> phoneList;
        //private EmployeeItemSelected employeeItemSelected;
        private int type;

        public ViewHolder(View itemLayoutView, ArrayList<String> phoneList, int type) {
            super(itemLayoutView);
            this.phoneList = phoneList;
            //this.employeeItemSelected = employeeItemSelected;
            this.type = type;

            item_name = itemLayoutView.findViewById(R.id.tv_edu_item);
            edit = itemLayoutView.findViewById(R.id.img_emp_edit);

            item_name.setOnClickListener(this);
            edit.setOnClickListener(this);

        }

        @Override
        public void onClick(View view) {
            int pos = getAdapterPosition();
            String item = phoneList.get(pos);
            if (view == itemView) {
                //employeeItemSelected.onItemSelecetd(pos, item, type);
            } else if (view == edit) {
                //employeeItemSelected.onItemSelecetd(pos, item, type);
            }
        }
    }
}
