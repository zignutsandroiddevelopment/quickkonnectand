package com.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.quickkonnect.NavModelItems;
import com.quickkonnect.R;

import java.util.List;

/**
 * Created by ZTLAB-10 on 13-05-2018.
 */

public class AdpNavigationMenu extends BaseAdapter {

    Context context;
    List<NavModelItems> navModelItems;
    ImageLoader imageLoader;
    DisplayImageOptions options;

    public AdpNavigationMenu(Context context, List<NavModelItems> navModelItems) {
        this.context = context;
        this.navModelItems = navModelItems;
        this.imageLoader = ImageLoader.getInstance();
        options = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .showImageForEmptyUri(R.drawable.user_profile_pic)
                .showImageOnFail(R.drawable.user_profile_pic)
                .showImageOnLoading(R.drawable.user_profile_pic)
                .cacheOnDisk(true)
                .build();
    }

    @Override
    public int getCount() {
        return navModelItems.size();
    }

    @Override
    public Object getItem(int i) {
        return navModelItems.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        ItemHolder itemHolder = null;

        NavModelItems current = navModelItems.get(i);

        if (view == null) {

            view = LayoutInflater.from(context).inflate(R.layout.item_nav, null);
            itemHolder = new ItemHolder(view);
            view.setTag(itemHolder);

        } else {
            itemHolder = (ItemHolder) view.getTag();
        }

        if (current.getTitle() != null && !current.getTitle().isEmpty())
            itemHolder.title.setText(current.getTitle());

        imageLoader.displayImage("drawable://" + current.getIcon(), itemHolder.icons, options);

        return view;
    }

    public class ItemHolder {
        TextView title;
        ImageView icons;

        public ItemHolder(View view) {
            this.title = view.findViewById(R.id.tv_name_nav);
            icons = view.findViewById(R.id.img_icon_nav);
        }
    }
}
