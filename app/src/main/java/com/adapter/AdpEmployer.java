package com.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.interfaces.EmployerClick;
import com.models.profiles.ProfilesList;
import com.quickkonnect.CircleTransform;
import com.quickkonnect.PojoClasses;
import com.quickkonnect.R;
import com.squareup.picasso.Picasso;
import com.utilities.Utilities;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ZTLAB-10 on 15-05-2018.
 */

public class AdpEmployer extends RecyclerView.Adapter<AdpEmployer.MyEmployerHolder> {

    private Context context;
    List<ProfilesList> listEmployerprofile;
    EmployerClick employerClick;

    public AdpEmployer(Context context, List<ProfilesList> listEmployerprofile, EmployerClick employerClick) {
        this.context = context;
        this.employerClick = employerClick;
        this.listEmployerprofile = listEmployerprofile;
    }

    @Override
    public MyEmployerHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_contacts_list_adapter, parent, false);
        AdpEmployer.MyEmployerHolder viewHolder = new AdpEmployer.MyEmployerHolder(itemLayoutView, listEmployerprofile, employerClick);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(MyEmployerHolder holder, int position) {

        ProfilesList contactsList = listEmployerprofile.get(position);

        if (Utilities.isEmpty(contactsList.getName()))
            holder.tv_name.setText(contactsList.getName());

        if (Utilities.isEmpty("" + contactsList.getLogo())) {
            Picasso.with(context)
                    .load("" + contactsList.getLogo())
                    .error(R.drawable.ic_default_profile_photo)
                    .placeholder(R.drawable.ic_default_profile_photo)
                    .transform(new CircleTransform())
                    .fit()
                    .into(holder.img_profile_pic);
        } else {
            holder.img_profile_pic.setImageResource(R.drawable.ic_default_profile_photo);
        }

        holder.tv_Scan_by.setVisibility(View.GONE);
        holder.tv_date.setVisibility(View.GONE);

    }

    @Override
    public int getItemCount() {
        return listEmployerprofile.size();
    }

    public class MyEmployerHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView tv_name, tv_date, tv_Scan_by;
        ImageView img_profile_pic;
        LinearLayout linear_main_contact;

        List<ProfilesList> listEmployerprofile;
        EmployerClick employerClick;


        public MyEmployerHolder(View itemView, List<ProfilesList> listEmployerprofile, EmployerClick employerClick) {
            super(itemView);

            this.employerClick = employerClick;
            this.listEmployerprofile = listEmployerprofile;

            linear_main_contact = itemView.findViewById(R.id.linear_main_contact);
            tv_name = itemView.findViewById(R.id.tv_name);
            tv_date = itemView.findViewById(R.id.tv_date);
            tv_Scan_by = itemView.findViewById(R.id.tv_Scan_by);
            img_profile_pic = itemView.findViewById(R.id.img_profile_pic);

            linear_main_contact.setOnClickListener(this);

        }

        @Override
        public void onClick(View view) {
            int pos = getAdapterPosition();
            ProfilesList contactsList = listEmployerprofile.get(pos);
            if (pos == 0) {
                employerClick.onEmployerclick(pos, contactsList.getUniqueCode() + "", "0");
            } else {
                employerClick.onEmployerclick(pos, contactsList.getUniqueCode() + "", contactsList.getId() + "");
            }

        }
    }

    public void filterList(ArrayList<ProfilesList> filterdNames) {
        this.listEmployerprofile = filterdNames;
        notifyDataSetChanged();
    }
}
