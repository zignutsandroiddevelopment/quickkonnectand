package com.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.interfaces.OnSingleEdit;
import com.models.createprofile.Email;
import com.quickkonnect.R;
import com.utilities.Utilities;

import java.util.List;

/**
 * Created by ZTLAB-10 on 22-02-2018.
 */

public class AdpSingleEmail extends RecyclerView.Adapter<AdpSingleEmail.ViewHolder> {

    private Context context;
    private List<Email> socialMediumList;
    private OnSingleEdit onSingleEdit;
    private boolean isEditenable = false;

    public AdpSingleEmail(Context context, List<Email> socialMediumList, OnSingleEdit onSingleEdit, boolean isEditenable) {
        this.context = context;
        this.onSingleEdit = onSingleEdit;
        this.isEditenable = isEditenable;
        this.socialMediumList = socialMediumList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_founders, null);
        AdpSingleEmail.ViewHolder viewHolder = new AdpSingleEmail.ViewHolder(itemLayoutView);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final Email weblink = socialMediumList.get(position);
        if (weblink.getName() != null && Utilities.isEmpty(weblink.getName())) {
            holder.tv_name.setText(weblink.getName());
        } else holder.tv_name.setVisibility(View.GONE);

        if (isEditenable)
            holder.imgDelete.setVisibility(View.VISIBLE);
        else {
            holder.tv_name.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Utilities.sendEmail((Activity) context, String.valueOf(weblink.getName()));
                }
            });
            holder.imgDelete.setVisibility(View.GONE);
        }

        holder.imgIcon.setVisibility(View.GONE);
//        holder.imgIcon.setBackgroundResource(R.drawable.ic_email_blue_24);

        holder.imgDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isEditenable)
                    onSingleEdit.getEditSingle(position, weblink.getName(), OnSingleEdit.DELETE, OnSingleEdit.TYPE_EMAIL);
            }
        });

        holder.llData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isEditenable)
                    onSingleEdit.getEditSingle(position, weblink.getName(), OnSingleEdit.EDIT, OnSingleEdit.TYPE_EMAIL);
            }
        });
    }

    @Override
    public int getItemCount() {
        return socialMediumList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView tv_name, tv_designation;
        private LinearLayout imgDelete, llData;
        private ImageView imgIcon;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            tv_name = itemLayoutView.findViewById(R.id.tv_name);
            tv_designation = itemLayoutView.findViewById(R.id.tv_designation);
            tv_designation.setVisibility(View.GONE);
            imgDelete = itemLayoutView.findViewById(R.id.imgDelete);
            llData = itemLayoutView.findViewById(R.id.llData);
            imgIcon = itemLayoutView.findViewById(R.id.imgIcon);
        }
    }
}
