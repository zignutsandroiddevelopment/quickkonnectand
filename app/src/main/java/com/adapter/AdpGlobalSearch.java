package com.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.interfaces.onGlobalSearchClick;
import com.models.globalsearch.Continent;
import com.models.globalsearch.Datum;
import com.models.globalsearch.ModelGlobalSearch;
import com.models.globalsearch.ModelSearhWebResult;
import com.quickkonnect.DashboardActivity;
import com.quickkonnect.R;
import com.retrofitservice.ApiUtils;
import com.retrofitservice.SOService;
import com.utilities.QKPreferences;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ZTLAB-10 on 23-02-2018.
 */

public class AdpGlobalSearch extends BaseExpandableListAdapter {

    private Context _context;
    private QKPreferences qkPreferences;
    private SOService soService;
    private ArrayList<Continent> continentList;
    private ArrayList<Continent> originalList;
    private ArrayList<Continent> tempinalList;
    private onGlobalSearchClick searchClick;
    boolean isEmptyQuery = false;

    public AdpGlobalSearch(Context context, ArrayList<Continent> originalList, onGlobalSearchClick searchClick) {
        this._context = context;
        this.qkPreferences = QKPreferences.getInstance(context);
        this.soService = ApiUtils.getSOService();
        this.searchClick = searchClick;
        this.tempinalList = new ArrayList<>();
        this.continentList = new ArrayList<>();
        this.originalList = new ArrayList<>();
        this.originalList.addAll(originalList);
    }

    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        ArrayList<ModelGlobalSearch> countryList = continentList.get(groupPosition).getCountryList();
        return countryList.get(childPosititon);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }


    @Override
    public View getChildView(int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

        final ModelGlobalSearch memData = (ModelGlobalSearch) getChild(groupPosition, childPosition);
        MyViewHolder myViewHolder = null;

        if (convertView == null) {

            convertView = LayoutInflater.from(_context).inflate(R.layout.item_globalsearch_child, null);

            myViewHolder = new MyViewHolder(convertView);

            convertView.setTag(myViewHolder);

        } else {
            myViewHolder = (MyViewHolder) convertView.getTag();
        }

        myViewHolder.tvUsername.setText(memData.getUsername());
        if (memData.getKey().equalsIgnoreCase(DashboardActivity.FOLLOWERS)) {

            if (memData.getCompanyname() != null) {
                myViewHolder.tv_company_name.setVisibility(View.VISIBLE);
                myViewHolder.tv_company_name.setText(memData.getCompanyname());

            } else myViewHolder.tv_company_name.setVisibility(View.GONE);

        } else myViewHolder.tv_company_name.setVisibility(View.GONE);

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                searchClick.onChildClick(memData);
            }
        });

        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        ArrayList<ModelGlobalSearch> countryList = continentList.get(groupPosition).getCountryList();
        return countryList.size();

    }

    @Override
    public Object getGroup(int groupPosition) {
        return this.continentList.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this.continentList.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {

        Continent continent = (Continent) getGroup(groupPosition);

        MyGroupHolder myGroupHolder = null;

        if (convertView == null) {
            convertView = LayoutInflater.from(_context).inflate(R.layout.item_globalsearch_group, null);
            myGroupHolder = new MyGroupHolder(convertView);
            convertView.setTag(myGroupHolder);
        } else {
            myGroupHolder = (MyGroupHolder) convertView.getTag();
        }

        myGroupHolder.tvTitle.setText(continent.getName());

        return convertView;

    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    public ArrayList<Continent> getSearchList() {
        return continentList;
    }

    public class MyViewHolder {
        public TextView tvUsername, tv_company_name;

        public MyViewHolder(View itemView) {
            tvUsername = itemView.findViewById(R.id.tvUsername);
            tv_company_name = itemView.findViewById(R.id.tv_company_name);
        }
    }

    public class MyGroupHolder {
        public TextView tvTitle;

        public MyGroupHolder(View itemView) {
            tvTitle = itemView.findViewById(R.id.tv_title);
        }
    }

    public boolean isLoadingData = false;

    public void filterData(String query) {

        query = query.toLowerCase();
        continentList.clear();
        if (query != null && !query.isEmpty()) {
            isEmptyQuery = false;
            searchClick.onListEmpty(false);
            if (!isLoadingData) {
                isLoadingData = true;
                searchGlobal(query);
            }
        } else {
            searchClick.onListEmpty(true);
            isEmptyQuery = true;
            notifyDataSetChanged();
        }

//        Log.v("Query 2", String.valueOf(continentList.size()));
        //notifyDataSetChanged();

    }

    ArrayList<ModelGlobalSearch> listglobalsearch = null;

    public void searchGlobal(final String query) {

        String user_id = qkPreferences.getLoggedUserid();
        String auth_token = String.valueOf(qkPreferences.getApiToken());

        Map<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", "Bearer " + auth_token);
        headers.put("Accept", "application/json");

        soService.searchGlobalData(headers, user_id, query).enqueue(new Callback<ModelSearhWebResult>() {
            @Override
            public void onResponse(Call<ModelSearhWebResult> call, Response<ModelSearhWebResult> response) {
                if (response != null && response.body() != null) {

                    ModelSearhWebResult modelSearhWebResult = response.body();

                    if (modelSearhWebResult.getStatus() == 200) {

                        tempinalList.clear();
                        tempinalList.addAll(originalList);

                        Log.e("DataSize", "" + originalList.size());
                        Log.e("DataSize", "" + response.body().getData().size());

                        if (modelSearhWebResult.getData() != null && modelSearhWebResult.getData().size() > 0) {

                            List<Datum> datumList = modelSearhWebResult.getData();

                            listglobalsearch = new ArrayList<>();

                            for (Datum datum: datumList) {

                                ModelGlobalSearch modelGlobalSearch = new ModelGlobalSearch();

                                modelGlobalSearch.setKey(datum.getKey());
                                modelGlobalSearch.setId(datum.getId() + "");
                                modelGlobalSearch.setUser_id(datum.getUser_id() + "");
                                modelGlobalSearch.setIs_followed(datum.getIs_followed() + "");
                                modelGlobalSearch.setUsername(datum.getName());
                                modelGlobalSearch.setProfile_pic(datum.getProfilePic() + "");
                                modelGlobalSearch.setUnique_code(datum.getUniqueCode());

                                if (datum.getKey() != null && !datum.getKey().isEmpty() && datum.getKey().equalsIgnoreCase("user"))
                                    modelGlobalSearch.setType(DashboardActivity.CONTACTS);
                                else if (datum.getKey() != null && !datum.getKey().isEmpty() && datum.getKey().equalsIgnoreCase("profile"))
                                    modelGlobalSearch.setType(DashboardActivity.MYPROFILES);
                                else
                                    modelGlobalSearch.setType(DashboardActivity.CONTACTS);

                                modelGlobalSearch.setScan_request(datum.getScan_request());

                                if (datum.getId() != null)
                                    listglobalsearch.add(modelGlobalSearch);
                            }

                            Continent nContinent1 = new Continent(DashboardActivity.MORERESULTS, listglobalsearch);

                            tempinalList.add(nContinent1);
                        }

                        for (Continent continent: tempinalList) {

                            ArrayList<ModelGlobalSearch> countryList = continent.getCountryList();
                            ArrayList<ModelGlobalSearch> newList = new ArrayList<>();
                            for (ModelGlobalSearch country: countryList) {
                                if (country.getUsername().toLowerCase().contains(query) ||
                                        country.getUsername().toLowerCase().contains(query)) {
                                    newList.add(country);
                                }
                            }

                            if (newList.size() > 0) {
                                Continent nContinent = new Continent(continent.getName(), newList);
                                continentList.add(nContinent);
                            }

                            notifyDataSetChanged();
                        }
                    }

                    if (isEmptyQuery) {
                        continentList.clear();
                        notifyDataSetChanged();
                    }

                    searchClick.onMoreresultRefresh(null);
                }

                isLoadingData = false;
            }

            @Override
            public void onFailure(Call<ModelSearhWebResult> call, Throwable t) {

            }
        });
    }

//
//    public void getGlobalSearch(final String query) {
//        try {
//            Log.e("QueryData", "" + query);
//            if (DashboardActivity.progresbarLoading != null)
//                DashboardActivity.progresbarLoading.setVisibility(View.VISIBLE);
//            JSONObject jsonObject = new JSONObject();
//            jsonObject.put("search", query);
//            new VolleyApiRequest(_context, false, VolleyApiRequest.REQUEST_BASEURL + "api/global_search",
//                    new VolleyCallBack() {
//                        @Override
//                        public void onResponse(JSONObject response) {
//                            try {
//                                if (DashboardActivity.progresbarLoading != null && DashboardActivity.progresbarLoading.getVisibility() == View.VISIBLE)
//                                    DashboardActivity.progresbarLoading.setVisibility(View.GONE);
//                                continentList.clear();
//                                isLoadingData = false;
//                                if (response != null && response.length() > 0) {
//                                    ModelSearhWebResult modelSearhWebResult = new Gson().fromJson(response.toString(), ModelSearhWebResult.class);
//                                    if (modelSearhWebResult.getStatus() == 200) {
//
//                                        tempinalList.clear();
//                                        tempinalList.addAll(originalList);
//
//                                        if (modelSearhWebResult.getData() != null && modelSearhWebResult.getData().size() > 0) {
//                                            List<Datum> datumList = modelSearhWebResult.getData();
//                                            Log.v("Query 3", "" + new Gson().toJson(datumList));
//                                            listglobalsearch = new ArrayList<>();
//                                            for (Datum datum: datumList) {
//                                                ModelGlobalSearch modelGlobalSearch = new ModelGlobalSearch();
//                                                modelGlobalSearch.setKey(datum.getKey());
//                                                modelGlobalSearch.setId(datum.getId() + "");
//                                                modelGlobalSearch.setUser_id(datum.getUser_id() + "");
//                                                modelGlobalSearch.setIs_followed(datum.getIs_followed() + "");
//                                                modelGlobalSearch.setUsername(datum.getName());
//                                                modelGlobalSearch.setProfile_pic(datum.getProfilePic() + "");
//                                                modelGlobalSearch.setUnique_code(datum.getUniqueCode());
//                                                modelGlobalSearch.setType("" + datum.getType());
//                                                if (datum.getId() != null)
//                                                    listglobalsearch.add(modelGlobalSearch);
//                                            }
//                                            Continent nContinent1 = new Continent(DashboardActivity.MORERESULTS, listglobalsearch);
//                                            tempinalList.add(nContinent1);
//                                        }
//
//                                        for (Continent continent: tempinalList) {
//
//                                            ArrayList<ModelGlobalSearch> countryList = continent.getCountryList();
//                                            ArrayList<ModelGlobalSearch> newList = new ArrayList<>();
//                                            for (ModelGlobalSearch country: countryList) {
//                                                if (country.getUsername().toLowerCase().contains(query) ||
//                                                        country.getUsername().toLowerCase().contains(query)) {
//                                                    newList.add(country);
//                                                }
//                                            }
//
//                                            if (newList.size() > 0) {
//                                                Continent nContinent = new Continent(continent.getName(), newList);
//                                                continentList.add(nContinent);
//                                            }
//
//                                            notifyDataSetChanged();
//                                        }
//                                    }
//                                }
//
//                                if (isEmptyQuery) {
//                                    continentList.clear();
//                                    notifyDataSetChanged();
//                                }
//
//                            } catch (Exception e) {
//                                e.printStackTrace();
//                            }
//                            searchClick.onMoreresultRefresh(null);
//                        }
//
//                        @Override
//                        public void onErrorResponse(VolleyError error) {
//                        }
//                    }).enqueRequest(jsonObject, Request.Method.POST);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
}