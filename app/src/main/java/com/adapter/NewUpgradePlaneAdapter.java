package com.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.interfaces.OnQkTagInfo;
import com.interfaces.ProfileSelection;
import com.interfaces.Upgradepackagelistener;
import com.models.UpgradePlan;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.quickkonnect.R;
import com.realmtable.MyProfiles;
import com.utilities.OutlineContainer;
import com.utilities.Utilities;

import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ZTLAB-12 on 13-06-2018.
 */

public class NewUpgradePlaneAdapter extends RecyclerView.Adapter<NewUpgradePlaneAdapter.ViewHolder> {

    ArrayList<UpgradePlan.Datum> upList;
    private Context context;
    Upgradepackagelistener upgradepackagelistener;


    public NewUpgradePlaneAdapter(ArrayList<UpgradePlan.Datum> upList, Context context, Upgradepackagelistener upgradepackagelistener) {
        this.upList = upList;
        this.context = context;
        this.upgradepackagelistener = upgradepackagelistener;
    }

    @Override
    public NewUpgradePlaneAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.dialog_up, parent, false);
        NewUpgradePlaneAdapter.ViewHolder viewHolder = new NewUpgradePlaneAdapter.ViewHolder(itemLayoutView, upList, upgradepackagelistener);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(NewUpgradePlaneAdapter.ViewHolder holder, int position) {

        try {
            final String name = upList.get(position).getName() + "";
            String description = upList.get(position).getDescription() + "";
            String amount = upList.get(position).getAmount() + "";
            String minEmployeeLimit = upList.get(position).getMinEmployeeLimit() + "";
            String maxEmployeeLimit = upList.get(position).getMaxEmployeeLimit() + "";
            String costPerEmployee = upList.get(position).getCostPerEmployee() + "";
            String max_employee_cost = upList.get(position).getMax_employee_cost() + "";

            holder.title_upd.setText(description + "");

            if (amount != null && !amount.equalsIgnoreCase("null") && !amount.equalsIgnoreCase("")) {
                holder.price_shadow.setText(amount + "");
                holder.price_main.setText(amount + "");
            } else {
                holder.price_shadow.setText("0");
                holder.price_main.setText("0");
            }

           /* int minemp = 0;
            try {
                minemp = Integer.parseInt(minEmployeeLimit);
                if (minemp != 1) {
                    minemp = minemp - 1;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }*/

            String Doller = context.getResources().getString(R.string.doller);
            String text = "(" + minEmployeeLimit + " Employee incl) + " + Doller + costPerEmployee + "/Employee/Mo";
            holder.tv_tv1.setText(text + "");

            holder.tv_contact_up.setText("Minimum " + minEmployeeLimit + " Employees");
            holder.tv_maxemp_up.setText("Maximum " + maxEmployeeLimit + " Employees");

            holder.tv_min_up.setText("Min. " + Doller + amount + "/mo (" + minEmployeeLimit + " Employees)");
            if (max_employee_cost.equalsIgnoreCase("no_maximum")) {
                holder.tv_max_up.setText("No Maximum");
            } else {
                holder.tv_max_up.setText("Max. " + Doller + max_employee_cost + "/mo (" + maxEmployeeLimit + " Employees)");
            }

           /* if (maxEmployeeLimit.equalsIgnoreCase("unlimited")) {
                holder.tv_max_up.setText("NO MAXIMUM");
                holder.tv_maxemp_up.setText("UNLIMITED EMPLOYEES");
            } else {
                int max = Integer.parseInt(maxEmployeeLimit);
                int min = Integer.parseInt(minEmployeeLimit);
                double emp = Double.parseDouble(costPerEmployee);
                double amt = Double.parseDouble(amount);

                double cost;
                if (name != null && name.equalsIgnoreCase("small-plan")) {
                    cost = max * emp + amt;
                } else {
                    cost = (max - min) * emp + amt;
                }

                String fullcoast = new DecimalFormat("##.##").format(cost);

                holder.tv_max_up.setText("Max. " + Doller + fullcoast + "/mo (" + maxEmployeeLimit + " Employees)");
                //Max.$18 .45 / mo(30 Employees)
            }*/
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return upList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView title_upd, price_shadow, price_main, tv_tv1, tv_maxemp_up, tv_contact_up, tv_min_up, tv_max_up;
        FrameLayout frame_up_now;

        ArrayList<UpgradePlan.Datum> upList;
        Upgradepackagelistener upgradepackagelistener;

        public ViewHolder(View itemLayoutView, ArrayList<UpgradePlan.Datum> upList, Upgradepackagelistener upgradepackagelistener) {
            super(itemLayoutView);

            this.upList = upList;
            this.upgradepackagelistener = upgradepackagelistener;

            title_upd = itemLayoutView.findViewById(R.id.title_upd);
            price_shadow = itemLayoutView.findViewById(R.id.price_shadow);
            price_main = itemLayoutView.findViewById(R.id.price_main);
            tv_tv1 = itemLayoutView.findViewById(R.id.tv_tv1);
            tv_maxemp_up = itemLayoutView.findViewById(R.id.tv_maxemp_up);
            tv_contact_up = itemLayoutView.findViewById(R.id.tv_contact_up);
            tv_min_up = itemLayoutView.findViewById(R.id.tv_min_up);
            tv_max_up = itemLayoutView.findViewById(R.id.tv_max_up);
            frame_up_now = itemLayoutView.findViewById(R.id.frame_up_now);

            frame_up_now.setOnClickListener(this);

        }

        @Override
        public void onClick(View view) {
            int pos = getAdapterPosition();
            String name = upList.get(pos).getName() + "";
            String max = upList.get(pos).getMaxEmployeeLimit() + "";
            switch (view.getId()) {
                case R.id.frame_up_now:
                    upgradepackagelistener.onUpgradePackage(pos, name + "", max + "");
                    break;
            }
        }
    }
}