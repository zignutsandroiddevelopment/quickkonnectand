package com.adapter;

import android.Manifest;
import android.app.Activity;
import android.content.ContentProviderOperation;
import android.content.pm.PackageManager;
import android.os.Build;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.models.ContactDetail;
import com.models.SynchContactsPojo;
import com.quickkonnect.R;
import com.retrofitservice.ApiUtils;
import com.retrofitservice.SOService;
import com.utilities.QKPreferences;
import com.utilities.Utilities;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Zignuts Technolab pvt. ltd on 20-09-2017.
 */

public class ApproveContactListAdapter1 extends RecyclerView.Adapter<ApproveContactListAdapter1.ViewHolder> implements View.OnClickListener, ActivityCompat.OnRequestPermissionsResultCallback {

    private List<ContactDetail> contactInfos;
    static Activity mActivity;
    public static ArrayList<Integer> tempArray;
    private static final int PERMISSIONS_REQUEST_READ_CONTACTS = 100;

    public ApproveContactListAdapter1(Activity activity, List<ContactDetail> list, String Name) {
        this.contactInfos = list;
        this.mActivity = activity;
        this.tempArray = new ArrayList<>();
    }

    @Override
    public ApproveContactListAdapter1.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.approve_contact_list_adapter, null);
        ApproveContactListAdapter1.ViewHolder viewHolder = new ApproveContactListAdapter1.ViewHolder(itemLayoutView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ApproveContactListAdapter1.ViewHolder holder, int position) {

        final ContactDetail contactInfo = contactInfos.get(position);
        contactInfo.setCurrentPos(position);

        if (Utilities.isEmpty(contactInfo.getContactType())) {
            String first = "<font color='#000000'>Contact Type: </font>";
            String sourceString = "<b>" + first + "</b> " + contactInfo.getContactType();
            holder.tv_contact_type.setText(Html.fromHtml(sourceString));
        } else {
            holder.tv_contact_type.setText("");
            holder.tv_contact_type.setVisibility(View.GONE);
        }

        if (contactInfo.getPhone() != null && !contactInfo.getPhone().isEmpty() && !contactInfo.getPhone().equals("null")) {

            if (contactInfo.getIsContactExist() == 1)
                holder.btn_add_contact.setBackgroundResource(R.drawable.contact_added);
            else if (contactInfo.getIsContactExist() == 0 && contactInfo.getIs_update() == 1)
                holder.btn_add_contact.setBackgroundResource(R.drawable.shape_update_contact);
            else
                holder.btn_add_contact.setBackgroundResource(R.drawable.add_contacts);

        } else holder.btn_add_contact.setVisibility(View.GONE);

        if (contactInfo.getEmail() != null && !contactInfo.getEmail().isEmpty() && !contactInfo.getEmail().equals("null")) {

            String first = "<font color='#000000'>Email:</font>";
            String sourceString = "<b>" + first + "</b> <u><font color='#0433FF'>" + contactInfo.getEmail()+"</font></u>";
            holder.tv_contact_email.setText(Html.fromHtml(sourceString));
            holder.tv_contact_email.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Utilities.sendEmail(mActivity, String.valueOf(contactInfo.getEmail()));
                }
            });
        } else {
            holder.tv_contact_email.setText("");
            holder.tv_contact_email.setVisibility(View.GONE);
        }

        if (contactInfo.getPhone() != null && !contactInfo.getPhone().isEmpty() && !contactInfo.getPhone().equals("null")) {
            String first = "<font color='#000000'>Phone:</font>";
            String sourceString = "<b>" + first + "</b> <u><font color='#0433FF'>" + contactInfo.getPhone()+"</font></u>";
            holder.tv_contact_phone.setText(Html.fromHtml(sourceString));
            holder.tv_contact_phone.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Utilities.makeCall(mActivity, String.valueOf(contactInfo.getPhone()));
                }
            });
        } else {
            holder.tv_contact_phone.setText("");
            holder.tv_contact_phone.setVisibility(View.GONE);
        }

        holder.btn_add_contact.setTag(contactInfo);
        holder.btn_add_contact.setOnClickListener(this);
    }

    @Override
    public int getItemCount() {
        return contactInfos.size();
    }

    @Override
    public void onClick(View view) {

        ContactDetail info = (ContactDetail) view.getTag();

        if (info.getPhone() != null && !info.getPhone().equals("null") && !info.getPhone().isEmpty()) {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && mActivity.checkSelfPermission(Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
                mActivity.requestPermissions(new String[]{Manifest.permission.READ_CONTACTS}, PERMISSIONS_REQUEST_READ_CONTACTS);

            } else {
                if (!Utilities.contactExists(mActivity, info.getPhone()))
                    fnAddContact(info, info.getCurrentPos());
            }

        } else
            Utilities.showToast(mActivity, mActivity.getResources().getString(R.string.mobilenumbernotavailable));

    }

    public void fnAddContact(ContactDetail contactDetail, int curretPos) {

        int Contact_Type = ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE;

        if (contactDetail.getContactType() != null &&
                (contactDetail.getContactType().equalsIgnoreCase("Business")
                        || contactDetail.getContactType().equalsIgnoreCase("Work")))
            Contact_Type = ContactsContract.CommonDataKinds.Phone.TYPE_WORK;

        else if (contactDetail.getContactType() != null && contactDetail.getContactType().equalsIgnoreCase("Other"))
            Contact_Type = ContactsContract.CommonDataKinds.Phone.TYPE_OTHER;

        ArrayList<ContentProviderOperation> ops = new ArrayList<>();

        ops.add(ContentProviderOperation.newInsert(
                ContactsContract.RawContacts.CONTENT_URI)
                .withValue(ContactsContract.RawContacts.ACCOUNT_TYPE, null)
                .withValue(ContactsContract.RawContacts.ACCOUNT_NAME, null)
                .build());

        // Names
        if (contactDetail.getContactname() != null) {
            ops.add(ContentProviderOperation.newInsert(
                    ContactsContract.Data.CONTENT_URI)
                    .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
                    .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE)
                    .withValue(ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME, contactDetail.getContactname()).build());
        }

        // Mobile Number
        if (contactDetail.getPhone() != null) {
            ops.add(ContentProviderOperation.
                    newInsert(ContactsContract.Data.CONTENT_URI)
                    .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
                    .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE)
                    .withValue(ContactsContract.CommonDataKinds.Phone.NUMBER, contactDetail.getPhone())
                    .withValue(ContactsContract.CommonDataKinds.Phone.TYPE, Contact_Type)
                    .build());
        }

        // Email
        if (contactDetail.getEmail() != null) {
            ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                    .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
                    .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.Email.CONTENT_ITEM_TYPE)
                    .withValue(ContactsContract.CommonDataKinds.Email.DATA, contactDetail.getEmail())
                    .withValue(ContactsContract.CommonDataKinds.Email.TYPE, Contact_Type)
                    .build());
        }

        try {

            mActivity.getContentResolver().applyBatch(ContactsContract.AUTHORITY, ops);

            if (contactDetail.getIs_update() == 1) {
                if (Utilities.isNetworkConnected(mActivity))
                    updateSynchStatus(contactDetail.getContactId());
            } else
                Utilities.showToast(mActivity, mActivity.getResources().getString(R.string.contactaddedsuccess));

            contactInfos.get(curretPos).setIs_update(1);
            contactInfos.get(curretPos).setIsContactExist(1);
            notifyDataSetChanged();

        } catch (Exception e) {
            Utilities.showToast(mActivity, String.valueOf(e.getMessage()));
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        if (requestCode == PERMISSIONS_REQUEST_READ_CONTACTS) {

            if (grantResults[0] == PackageManager.PERMISSION_GRANTED)
                Utilities.showToast(mActivity, mActivity.getResources().getString(R.string.permissiongranted));

            else
                Utilities.showToast(mActivity, mActivity.getResources().getString(R.string.cannotdisplaynames));
        }
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_contact_type, tv_contact_email, tv_contact_phone;
        public ImageView btn_add_contact;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            tv_contact_type = itemLayoutView.findViewById(R.id.tv_contact_type);
            tv_contact_email = itemLayoutView.findViewById(R.id.tv_contact_email);
            tv_contact_phone = itemLayoutView.findViewById(R.id.tv_contact_phone);
            btn_add_contact = itemLayoutView.findViewById(R.id.btn_add_contact);
        }
    }

    public void updateSynchStatus(String contact_id) {
        SOService soService = ApiUtils.getSOService();
        QKPreferences qkPreferences = new QKPreferences(mActivity);
        Map<String, String> params = new HashMap<>();
        params.put("Authorization", "Bearer " + qkPreferences.getApiToken() + "");
        params.put("Accept", "application/json");
        soService.synchContact(params, contact_id, qkPreferences.getLoggedUserid()).enqueue(new Callback<SynchContactsPojo>() {
            @Override
            public void onResponse(Call<SynchContactsPojo> call, Response<SynchContactsPojo> response) {
                try {
                    if (response != null && response.body() != null) {
                        SynchContactsPojo contactsPojo = response.body();
                        if (contactsPojo != null && contactsPojo.getStatus() == 200) {
                            Utilities.showToast(mActivity, contactsPojo.getMsg());
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<SynchContactsPojo> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }
}
