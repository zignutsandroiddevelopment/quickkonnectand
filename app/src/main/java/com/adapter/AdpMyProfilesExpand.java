package com.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.models.profiles.ProfilesList;
import com.quickkonnect.CircleTransform;
import com.quickkonnect.R;
import com.squareup.picasso.Picasso;
import com.utilities.Constants;
import com.utilities.Utilities;

import java.util.HashMap;
import java.util.List;

/**
 * Created by ZTLAB-10 on 23-02-2018.
 */

public class AdpMyProfilesExpand extends BaseExpandableListAdapter {

    private Context _context;
    boolean isClick = false;
    private List<String> _listGroupTitle; // header titles

    private HashMap<String, List<ProfilesList>> _listDataMembers;

    public AdpMyProfilesExpand(Context context, List<String> listGroupTitle, HashMap<String, List<ProfilesList>> listDataMembers) {
        this._context = context;
        this._listGroupTitle = listGroupTitle;
        this._listDataMembers = listDataMembers;
    }

    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        return this._listDataMembers.get(this._listGroupTitle.get(groupPosition)).get(childPosititon);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }


    @Override
    public View getChildView(int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

        final ProfilesList memData = (ProfilesList) getChild(groupPosition, childPosition);
        MyViewHolder myViewHolder = null;

        if (convertView == null) {

            convertView = LayoutInflater.from(_context).inflate(R.layout.list_item_child, null);
            myViewHolder = new MyViewHolder(convertView);
            convertView.setTag(myViewHolder);

        } else {
            myViewHolder = (MyViewHolder) convertView.getTag();
        }

        myViewHolder.tv_name.setText(memData.getName());

        if (memData.getType().equals(Constants.TYPE_PROFILE_COMPANY)) {
            myViewHolder.tv_status_verification.setVisibility(View.GONE);

        } else {

            if (memData.getStatus().equalsIgnoreCase("A")) {
                myViewHolder.tv_status_verification.setVisibility(View.GONE);

            } else if (memData.getStatus().equalsIgnoreCase("R")) {

                String text = "<font color='#FF0000'>Rejected &nbsp;&nbsp;&nbsp;</font><font color='#d1cfcf'>Details</font>";
                myViewHolder.tv_status_verification.setVisibility(View.VISIBLE);
                myViewHolder.tv_status_verification.setText(Html.fromHtml(text));
                myViewHolder.tv_status_verification.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        new AlertDialog.Builder(_context)
                                .setTitle("Reason")
                                .setMessage(memData.getResson() + "")
                                .setCancelable(false)
                                .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                }).show();
                    }
                });
            } else {
                myViewHolder.tv_status_verification.setVisibility(View.VISIBLE);
            }
        }

        if (memData.getIs_transfer() != null && memData.getTransfer_status() != null) {

            if (memData.getIs_transfer() != null) {
                if (memData.getIs_transfer().equalsIgnoreCase("0")) {
                    isClick = true;
                } else if (memData.getIs_transfer().equalsIgnoreCase("1")) {

                    if (memData.getTransfer_status().equalsIgnoreCase("0")) {
                        String text = "<font color='#e68e23'>Transfer in process</font>";
                        myViewHolder.tv_status_verification.setVisibility(View.VISIBLE);
                        myViewHolder.tv_status_verification.setText(Html.fromHtml(text));
                        isClick = false;
                    } else if (memData.getTransfer_status().equalsIgnoreCase("1")) {
                        myViewHolder.tv_status_verification.setVisibility(View.GONE);
                        isClick = true;
                    }
                }
            }

        } else {
            isClick = true;
        }

        if (memData.getStatus() != null) {

            switch (memData.getStatus()) {
                case "A":
                    myViewHolder.img_profile_strip.setImageResource(R.drawable.noti_approve);
                    break;
                case "R":
                    myViewHolder.img_profile_strip.setImageResource(R.drawable.noti_reject);
                    break;
                case "P":
                    //myViewHolder.img_profile_strip.setVisibility(View.GONE);
                    break;
            }

        } else {
            //myViewHolder.img_profile_strip.setVisibility(View.GONE);
        }


        if (memData.getLogo() != null && Utilities.isEmpty("" + memData.getLogo()))

            Picasso.with(_context)
                    .load(memData.getLogo() + "")
                    .error(R.drawable.ic_logo)
                    .placeholder(R.drawable.ic_logo)
                    .transform(new CircleTransform())
                    .fit()
                    .into(myViewHolder.img_profile_pic);

        else

            Picasso.with(_context)
                    .load(R.drawable.ic_logo)
                    .error(R.drawable.ic_logo)
                    .placeholder(R.drawable.ic_logo)
                    .transform(new CircleTransform())
                    .fit()
                    .into(myViewHolder.img_profile_pic);

        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return this._listDataMembers.get(this._listGroupTitle.get(groupPosition)).size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this._listGroupTitle.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this._listGroupTitle.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {

        String gData = (String) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_item_group, null);
        }
        TextView txtProName = convertView.findViewById(R.id.tv_title);
        txtProName.setText(gData);
        return convertView;

    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    public class MyViewHolder {
        public TextView tv_name, tv_status_verification;
        ImageView img_profile_pic, img_next_menu, img_profile_strip;

        public MyViewHolder(View itemView) {
            tv_name = itemView.findViewById(R.id.tv_name_mqp);
            img_profile_pic = itemView.findViewById(R.id.img_profile_pic_mqp);
            img_profile_strip = itemView.findViewById(R.id.img_profile_strip);
            img_next_menu = itemView.findViewById(R.id.img_next_arrow_mqp);
            tv_status_verification = itemView.findViewById(R.id.tv_status_verification);
        }
    }
}
