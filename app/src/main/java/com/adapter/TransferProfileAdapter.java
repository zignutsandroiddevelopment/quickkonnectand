package com.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.interfaces.onTransferClick;
import com.l4digital.fastscroll.FastScroller;
import com.quickkonnect.CircleTransform;
import com.quickkonnect.PojoClasses;
import com.quickkonnect.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by ZTLAB03 on 30-03-2018.
 */

public class TransferProfileAdapter extends RecyclerView.Adapter<TransferProfileAdapter.ViewHolder> implements FastScroller.SectionIndexer {

    private ArrayList<PojoClasses.TransferProfileList> transferProfileLists;
    private Context context;
    onTransferClick onTransferClick;

    public TransferProfileAdapter(Activity activity, ArrayList<PojoClasses.TransferProfileList> list,onTransferClick onTransferClick) {
        context = activity;
        this.onTransferClick = onTransferClick;
        this.transferProfileLists = list;
    }

    @Override
    public TransferProfileAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_followers_list_adapter, null);

        TransferProfileAdapter.ViewHolder viewHolder = new TransferProfileAdapter.ViewHolder(itemLayoutView,onTransferClick,transferProfileLists);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(TransferProfileAdapter.ViewHolder holder, int position) {

        PojoClasses.TransferProfileList current = transferProfileLists.get(position);
        holder.tv_name.setText(current.getName()+"");

        if(current.getEmail() != null && !current.getEmail().isEmpty() && !current.getEmail().equals("null") && !current.getEmail().equals(""))
            holder.tv_email.setText(current.getEmail());
        else
            holder.tv_email.setVisibility(View.GONE)
                    ;
        if (current.getProfile_pic() != null && !current.getProfile_pic().isEmpty() && !current.getProfile_pic().equals("null")) {
            Picasso.with(context)
                    .load(current.getProfile_pic())
                    .error(R.drawable.ic_default_profile_photo)
                    .placeholder(R.drawable.ic_default_profile_photo)
                    .transform(new CircleTransform())
                    .fit()
                    .into(holder.img_profile_pic);
        } else {
            holder.img_profile_pic.setImageResource(R.drawable.ic_default_profile_photo);
        }

        holder.tv_name.setTag(current);
        //holder.tv_name.setOnClickListener(this);

        holder.img_profile_pic.setTag(current);
        //holder.img_profile_pic.setOnClickListener(this);

    }

    @Override
    public int getItemCount() {
        return transferProfileLists.size();
    }

    @Override
    public String getSectionText(int position) {
        String str="";
        try {
            str = String.valueOf(transferProfileLists.get(position).getName().charAt(0));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return str;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView tv_name, tv_email;
        ImageView img_profile_pic;
        LinearLayout lin_follow;
        onTransferClick onTransferClick;
        private ArrayList<PojoClasses.TransferProfileList> transferProfileLists;

        public ViewHolder(View itemLayoutView,onTransferClick onTransferClick,ArrayList<PojoClasses.TransferProfileList> list) {
            super(itemLayoutView);
            this.onTransferClick = onTransferClick;
            this.transferProfileLists = list;

            tv_name = itemLayoutView.findViewById(R.id.tv_name_follow);
            tv_email = itemLayoutView.findViewById(R.id.tv_email_follow);
            img_profile_pic = itemLayoutView.findViewById(R.id.img_profile_pic_follow);
            lin_follow = itemLayoutView.findViewById(R.id.lin_follow);

            lin_follow.setOnClickListener(this);

        }

        @Override
        public void onClick(View view) {
            int pos = getAdapterPosition();
            PojoClasses.TransferProfileList current = transferProfileLists.get(pos);
            if(view == lin_follow){
                onTransferClick.onUserSelectedClick(current);
            }
        }
    }

    public void filterList(ArrayList<PojoClasses.TransferProfileList> filterdNames) {
        this.transferProfileLists = filterdNames;
        notifyDataSetChanged();
    }
}
