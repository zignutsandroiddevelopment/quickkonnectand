package com.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.interfaces.CallBackReadNoti;
import com.models.ModelNotifications;
import com.quickkonnect.CircleTransform;
import com.quickkonnect.R;
import com.squareup.picasso.Picasso;
import com.utilities.Constants;
import com.utilities.Utilities;

import java.util.ArrayList;

/**
 * Created by ZTLAB03 on 15-12-2017.
 */

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.ViewHolder> implements View.OnClickListener {

    ArrayList<ModelNotifications> notificationLists;
    static Context mActivity;
    private CallBackReadNoti callBackReadNoti;

    public NotificationAdapter(Context activity, ArrayList<ModelNotifications> list, CallBackReadNoti callBackReadNoti, RecyclerView recyclerView) {
        this.notificationLists = list;
        this.callBackReadNoti = callBackReadNoti;
        mActivity = activity;
    }

    @Override
    public NotificationAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.notification_adapter_layout, parent, false);
        NotificationAdapter.ViewHolder viewHolder = new NotificationAdapter.ViewHolder(itemLayoutView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(NotificationAdapter.ViewHolder holder, int position) {

        ModelNotifications notifications = notificationLists.get(position);

        if (notifications.getMessage() != null && Utilities.isEmpty(notifications.getMessage())) {

            if (notifications.getNotification_type().equals(Constants.NOTIFICATION_TYPE_SCAN) ||
                    notifications.getNotification_type().equals(Constants.NOTIFICATION_TYPE_CONTREQ) ||
                    notifications.getNotification_type().equals(Constants.NOTIFICATION_TYPE_APPROVED)) {

                if (notifications.getUsername() != null && !notifications.getUsername().isEmpty() && !notifications.getUsername().equals("null")) {

                    StringBuilder builder = new StringBuilder();

                    String cap = notifications.getUsername();

                    builder.append(cap + " ");

                    String message = notifications.getMessage().replace(notifications.getUsername(), "");

                    String htmlMessage = "<html><font color='#000000'>" + cap + "</font> <font color='#bcbcbc'>" + message + "</font></html>";

                    holder.tv_notification_username.setText(Html.fromHtml(htmlMessage));

                } else holder.tv_notification_username.setText(notifications.getMessage());

            } else if (notifications.getNotification_type().equals(Constants.NOTIFICATION_TYPE_PROFAPPROVED) ||
                    notifications.getNotification_type().equals(Constants.NOTIFICATION_TYPE_PROFREJECT)) {

                if (notifications.getProfile_name() != null) {

                    if (notifications.getNotification_type().equals(Constants.NOTIFICATION_TYPE_PROFAPPROVED)) {

                        String htmlMessage = "<html><font color='#bcbcbc'>Your profile</font> <font color='#000000'> " + notifications.getProfile_name() + "</font><font color='#bcbcbc'> is approved</font></html>";
                        holder.tv_notification_username.setText(Html.fromHtml(htmlMessage));

                    } else if (notifications.getNotification_type().equals(Constants.NOTIFICATION_TYPE_PROFREJECT)) {

                        String htmlMessage = "<html><font color='#bcbcbc'>Your profile</font> <font color='#000000'> " + notifications.getProfile_name() + "</font><font color='#bcbcbc'> is rejected</font></html>";
                        holder.tv_notification_username.setText(Html.fromHtml(htmlMessage));
                    }
                }

            } else holder.tv_notification_username.setText(notifications.getMessage());

        } else if (Utilities.isEmpty(notifications.getUsername()))
            holder.tv_notification_username.setText(notifications.getUsername());

        else holder.tv_notification_username.setText("");

        if (Utilities.isEmpty(notifications.getProfile_pic()))
            Picasso.with(mActivity)
                    .load(notifications.getProfile_pic())
                    .error(R.drawable.ic_default_profile_photo)
                    .placeholder(R.drawable.ic_default_profile_photo)
                    .transform(new CircleTransform())
                    .fit()
                    .into(holder.profile_pic);

        else holder.profile_pic.setImageResource(R.drawable.ic_default_profile_photo);


        if (notifications.getNotification_type() != null && !notifications.getNotification_type().isEmpty() && !notifications.getNotification_type().equals("null")) {

            switch (notifications.getNotification_type()) {

                case Constants.NOTIFICATION_TYPE_SCAN://Scan Match
                case Constants.NOTIFICATION_TYPE_FOLLOW://Follow Notification
                case Constants.NOTIFICATION_TYPE_TRANSFER:// Transfer profile request
                case Constants.NOTIFICATION_TYPE_EMPLOYY_REQUEST://Employee Request
                    holder.img_state.setImageResource(R.drawable.noti_addmatch);
//                    holder.imgMoreoption.setVisibility(View.GONE);
                    holder.lin_app_rej.setVisibility(View.GONE);
                    break;

                case Constants.NOTIFICATION_SCAN_CONTREQ:// Scan contact request
                case Constants.NOTIFICATION_TYPE_CONTREQ:// Download contact request
                    holder.img_state.setImageResource(R.drawable.noti_download);
//                    holder.imgMoreoption.setVisibility(View.VISIBLE);
                    holder.lin_app_rej.setVisibility(View.VISIBLE);
                    break;

                case Constants.NOTIFICATION_SCAN_APRROVED:// Scan contact request approved
                case Constants.NOTIFICATION_TYPE_APPROVED:// Download contact request approved
                case Constants.NOTIFICATION_TYPE_PROFAPPROVED:// Profile request approved
                case Constants.NOTIFICATION_TYPE_TRANSFER_APPROVE://Transfer Request Approved
                case Constants.NOTIFICATION_TYPE_EMPLOYY_ACCEPT://Employee Request Approve
                    holder.img_state.setImageResource(R.drawable.noti_approve);
//                    holder.imgMoreoption.setVisibility(View.GONE);
                    holder.lin_app_rej.setVisibility(View.GONE);
                    break;

                case Constants.NOTIFICATION_SCAN_REJECTED:// Scan contact request rejected
                case Constants.NOTIFICATION_TYPE_PROFREJECT:// Profile request rejected
                case Constants.NOTIFICATION_TYPE_TRANSFER_REJECT:// Transfer profile request rejected
                case Constants.NOTIFICATION_TYPE_EMPLOYY_REJECT:// Employee request rejected
                    holder.img_state.setImageResource(R.drawable.noti_reject);
//                    holder.imgMoreoption.setVisibility(View.GONE);
                    holder.lin_app_rej.setVisibility(View.GONE);
                    break;

                case Constants.NOTIFICATION_TYPE_CHANGE_ROLE:// Change Roles
                case Constants.NOTIFICATION_TYPE_TRIAL_EXPIRATION:// Trial Expiration Alert
                case Constants.NOTIFICATION_TYPE_CHANGE_PLAN_CANCEL:// Plan Cancel Notification
                case Constants.NOTIFICATION_TYPE_CHANGE_PAYMENT_FAIL:// Payment Failed
                case Constants.NOTIFICATION_TYPE_CHANGE_EMPLOYEE_DELETE:// Employee Delete Admin Notify
                case Constants.NOTIFICATION_TYPE_PROFILE_DELETE:// Delete Profile
                    holder.img_state.setVisibility(View.GONE);
//                    holder.imgMoreoption.setVisibility(View.GONE);
                    holder.lin_app_rej.setVisibility(View.GONE);
                    break;
            }

        } else holder.img_state.setVisibility(View.GONE);

        if (Utilities.isEmpty(notifications.getUserEmail()))
            holder.tv_notification_email.setText(notifications.getUserEmail());
        else holder.tv_notification_email.setText("");

        if (notifications.getIsRead() == 0) {
            holder.tv_notification_username.setTextColor(Color.BLACK);
            holder.rlUnread.setVisibility(View.VISIBLE);

        } else {
            holder.tv_notification_username.setTextColor(Color.GRAY);
            holder.rlUnread.setVisibility(View.GONE);
        }

        if (Utilities.isEmpty(notifications.getCreatedTime())) {
            String strCurrentDate = notifications.getCreatedTime();
            String formatDate = Utilities.changeDateFormat("yyyy-MM-dd hh:mm:ss", "EEEE, dd MMM yyyy hh:mm aa", strCurrentDate);
            holder.tv_date.setText(formatDate);

        } else holder.tv_date.setText("");

        holder.tv_notification_username.setTag(notifications);
        holder.tv_notification_username.setOnClickListener(this);

        holder.tv_notification_email.setTag(notifications);
        holder.tv_notification_email.setOnClickListener(this);

//        holder.imgMoreoption.setTag(notifications);
//        holder.imgMoreoption.setOnClickListener(this);

        holder.tv_date.setTag(notifications);
        holder.tv_date.setOnClickListener(this);

        holder.img_noti_reject.setTag(notifications);
        holder.img_noti_reject.setOnClickListener(this);

        holder.rel1.setTag(notifications);
        holder.rel1.setOnClickListener(this);

        holder.rlUnread.setTag(notifications);
        holder.rlUnread.setOnClickListener(this);

        holder.img_noti_accept.setTag(notifications);
        holder.img_noti_accept.setOnClickListener(this);

        if (notificationLists.size() > 15 && position == notificationLists.size() - 1) {
            callBackReadNoti.onLoadmore();
        }
    }

    @Override
    public int getItemCount() {
        return notificationLists.size();
    }

    @Override
    public void onClick(View view) {

        ModelNotifications notifications = (ModelNotifications) view.getTag();

        switch (view.getId()) {
            case R.id.tv_notification_username:
            case R.id.tv_notification_email:
            case R.id.rlUnread:
            case R.id.tv_date:
            case R.id.rel1:
//            case R.id.imgMoreoption:
                callBackReadNoti.actionNotification(notifications, notificationLists.indexOf(notifications));
                break;
            case R.id.img_noti_accept:
                callBackReadNoti.onAccept(notifications, notificationLists.indexOf(notifications));
                break;
            case R.id.img_noti_reject:
                callBackReadNoti.onReject(notifications, notificationLists.indexOf(notifications));
//                if (notifications.getNotification_type() != null) {
//                    switch (notifications.getNotification_type()) {
//                        case Constants.NOTIFICATION_TYPE_CONTREQ:// Download Contacts
//                            callBackReadNoti.onReject(Constants.NOTIFICATION_TYPE_CONTREQ, notifications, notificationLists.indexOf(notifications));
//                            break;
//                        case Constants.NOTIFICATION_TYPE_TRANSFER://Transfer Profile
//                            callBackReadNoti.onReject(Constants.NOTIFICATION_TYPE_TRANSFER, notifications, notificationLists.indexOf(notifications));
//                            break;
//                        case Constants.NOTIFICATION_TYPE_EMPLOYY_REQUEST://Employee Request
//                            callBackReadNoti.onReject(Constants.NOTIFICATION_TYPE_EMPLOYY_REQUEST, notifications, notificationLists.indexOf(notifications));
//                            break;
//                    }
//                }
                break;
        }
    }

    public void removeItem(int position) {
        notificationLists.remove(position);
        notifyItemRemoved(position);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_notification_username, tv_notification_email, tv_date;
        public ImageView profile_pic, img_state, img_noti_reject, img_noti_accept;//imgMoreoption,
        public RelativeLayout viewForeground;
        public RelativeLayout rlUnread;
        public FrameLayout rel1;
        public LinearLayout lin_app_rej;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            tv_notification_username = itemLayoutView.findViewById(R.id.tv_notification_username);
            tv_notification_email = itemLayoutView.findViewById(R.id.tv_notification_email);
            tv_notification_email.setVisibility(View.GONE);
            tv_date = itemLayoutView.findViewById(R.id.tv_date);
            viewForeground = itemLayoutView.findViewById(R.id.view_foreground);
            profile_pic = itemLayoutView.findViewById(R.id.img_noti_profile);
            img_state = itemLayoutView.findViewById(R.id.imag_pic_icon);
//            imgMoreoption = itemLayoutView.findViewById(R.id.imgMoreoption);
            rlUnread = itemLayoutView.findViewById(R.id.rlUnread);
            rel1 = itemLayoutView.findViewById(R.id.rel1);
            lin_app_rej = itemLayoutView.findViewById(R.id.lin_app_rej);
            img_noti_reject = itemLayoutView.findViewById(R.id.img_noti_reject);
            img_noti_accept = itemLayoutView.findViewById(R.id.img_noti_accept);
        }
    }
}
