package com.adapter;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.media.Image;
import android.os.Build;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.activities.MatchProfileActivity;
import com.bumptech.glide.Glide;
import com.interfaces.DashboardContact;
import com.models.contacts.Datum;
import com.quickkonnect.CircleTransform;
import com.quickkonnect.R;
import com.squareup.picasso.Picasso;
import com.utilities.Constants;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ZTLAB03 on 12-03-2018.
 */

public class Rec_DashBoard_main extends RecyclerView.Adapter<Rec_DashBoard_main.ViewHolder> {

    Context context;
    ArrayList<Datum> profileimages;
    DashboardContact dashboardContact;
    private int[] images = {
            R.drawable.ic_default_profile_photo,
            R.drawable.ic_default_profile_photo,
    };

    public Rec_DashBoard_main(Context context, ArrayList<Datum> profileimages, DashboardContact dashboardContact) {
        this.context = context;
        this.dashboardContact = dashboardContact;
        this.profileimages = profileimages;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_rec_dashbord, parent, false);
        Rec_DashBoard_main.ViewHolder viewHolder = new Rec_DashBoard_main.ViewHolder(itemLayoutView, context, profileimages, dashboardContact);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        String url = profileimages.get(position).getScanUserProfileUrl() + "";
        try {
            if (url != null && !url.equalsIgnoreCase("") && !url.equalsIgnoreCase("null")) {
                Picasso.with(context)
                        .load(url + "")
                        .error(R.drawable.ic_default_profile_photo)
                        .placeholder(R.drawable.ic_default_profile_photo)
                        .transform(new CircleTransform())
                        .fit()
                        .into(holder.image);
            } else {
                holder.image.setImageResource(R.drawable.ic_default_profile_photo);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        if (profileimages.size() > 4)
            return 4;
        else
            return profileimages.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ImageView image;
        Context context;
        ArrayList<Datum> profileimages;
        DashboardContact dashboardContact;

        public ViewHolder(View itemView, Context context, ArrayList<Datum> profileimages, DashboardContact dashboardContact) {
            super(itemView);
            image = itemView.findViewById(R.id.image_rec_dashbord_main);
            this.context = context;
            this.profileimages = profileimages;
            this.dashboardContact = dashboardContact;

            image.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (view == image) {
                int pos = getAdapterPosition();
                Datum datum = profileimages.get(pos);

                dashboardContact.OnUnMatchedContact(datum,datum.getScanUserUniqueCode() + "", datum.getScan_user_device_id() + "", datum.getScanUserId() + "");

                /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(activity, image, "transition_contact_image");
                    context.startActivity(intent, options.toBundle());
                } else {
                    context.startActivity(intent);
                }*/

            }
        }
    }
}
