package com.adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.activities.UpdateEducationActivity;
import com.models.logindata.EducationDetail;
import com.quickkonnect.R;
import com.utilities.Constants;
import com.utilities.Utilities;

import java.util.List;

/**
 * Created by ZTLAB03 on 30-11-2017.
 */

public class UpdateEducationListAdapter extends RecyclerView.Adapter<UpdateEducationListAdapter.ViewHolder> implements View.OnClickListener {

    List<EducationDetail> educations;
    static Activity mActivity;

    public UpdateEducationListAdapter(Activity activity, List<EducationDetail> list) {
        this.educations = list;
        mActivity = activity;
    }

    @Override
    public UpdateEducationListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_education_list_adapter, null);
        UpdateEducationListAdapter.ViewHolder viewHolder = new UpdateEducationListAdapter.ViewHolder(itemLayoutView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(UpdateEducationListAdapter.ViewHolder holder, int position) {

        EducationDetail education = educations.get(position);

        if (Utilities.isEmpty(education.getSchoolName()))
            holder.tv_school_name.setText(education.getSchoolName());
        else
            holder.tv_school_name.setVisibility(View.GONE);

        if (Utilities.isEmpty(education.getDegree()))
            holder.tv_degree.setText(education.getDegree());
        else
            holder.tv_degree.setVisibility(View.GONE);

        if (Utilities.isEmpty(education.getFieldOfStudy()))
            holder.tv_field_of_study.setText(education.getFieldOfStudy());
        else
            holder.tv_field_of_study.setVisibility(View.GONE);

        if (Utilities.isEmpty(education.getStartDate()) && Utilities.isEmpty(education.getEndDate())) {
            String endDate = education.getEndDate();
            try {
                String format_date[] = endDate.split("-");
                endDate = format_date[2] + "/" + format_date[1] + "/" + format_date[0];
            } catch (Exception e) {
                e.printStackTrace();
            }
            holder.tv_startdate.setText(education.getStartDate() + " - " + endDate);

        } else if (Utilities.isEmpty(education.getStartDate())) {
            holder.tv_startdate.setText(education.getStartDate() + " - Present");
        } else {
            holder.tv_startdate.setVisibility(View.GONE);
        }

        if (Utilities.isEmpty(education.getActivities()))
            holder.tv_activites.setText(education.getActivities());
        else
            holder.tv_activites.setVisibility(View.GONE);


        holder.img_delete.setTag(education);
        holder.img_delete.setOnClickListener(this);

        holder.tv_school_name.setTag(education);
        holder.tv_school_name.setOnClickListener(this);

        holder.tv_degree.setTag(education);
        holder.tv_degree.setOnClickListener(this);

        holder.tv_field_of_study.setTag(education);
        holder.tv_field_of_study.setOnClickListener(this);

        holder.tv_startdate.setTag(education);
        holder.tv_startdate.setOnClickListener(this);

        holder.tv_enddate.setTag(education);
        holder.tv_enddate.setOnClickListener(this);

        holder.tv_activites.setTag(education);
        holder.tv_activites.setOnClickListener(this);

    }

    @Override
    public int getItemCount() {
        return educations.size();
    }

    @Override
    public void onClick(View view) {

        EducationDetail eduList = (EducationDetail) view.getTag();

        switch (view.getId()) {
            case R.id.img_delete:
            case R.id.tv_school_name:
            case R.id.tv_degree:
            case R.id.tv_field_of_study:
            case R.id.tv_startdate:
            case R.id.tv_enddate:
            case R.id.tv_activites:
                Intent intent = new Intent(mActivity, UpdateEducationActivity.class);
                intent.putExtra("education", eduList);
                intent.putExtra(Constants.KEY_UPDATE, Constants.EDIT_DATA);
                mActivity.startActivityForResult(intent, Constants.RCODE_UPDATEEDUCATION);
                break;
        }
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_school_name, tv_degree, tv_field_of_study, tv_startdate, tv_enddate, tv_activites;
        private ImageView img_delete;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            tv_school_name = itemLayoutView.findViewById(R.id.tv_school_name);
            tv_degree = itemLayoutView.findViewById(R.id.tv_degree);
            tv_field_of_study = itemLayoutView.findViewById(R.id.tv_field_of_study);
            tv_startdate = itemLayoutView.findViewById(R.id.tv_startdate);
            tv_enddate = itemLayoutView.findViewById(R.id.tv_enddate);
            tv_activites = itemLayoutView.findViewById(R.id.tv_activites);
            img_delete = itemLayoutView.findViewById(R.id.img_delete);
        }
    }
}
