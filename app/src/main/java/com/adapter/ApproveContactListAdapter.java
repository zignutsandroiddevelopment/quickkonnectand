package com.adapter;

import android.Manifest;
import android.app.Activity;
import android.content.ContentProviderOperation;
import android.content.Context;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.quickkonnect.PojoClasses;
import com.quickkonnect.R;

import java.util.ArrayList;

/**
 * Created by ZTLAB03 on 18-11-2017.
 */

public class ApproveContactListAdapter extends RecyclerView.Adapter<ApproveContactListAdapter.ViewHolder> implements View.OnClickListener, ActivityCompat.OnRequestPermissionsResultCallback {

    ArrayList<PojoClasses.ContactInfo> contactInfos;
    static Activity mActivity;
    public static ArrayList<Integer> tempArray;
    private static final int PERMISSIONS_REQUEST_READ_CONTACTS = 100;


    public ApproveContactListAdapter(Activity activity, ArrayList<PojoClasses.ContactInfo> list) {
        this.contactInfos = list;
        mActivity = activity;

        tempArray = new ArrayList<>();


    }

    @Override
    public ApproveContactListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.approve_contact_list_adapter, null);

//        mSharedPreferences = mActivity.getSharedPreferences(SharedPreference.PREF_NAME, 0);

        // create ViewHolder

        ApproveContactListAdapter.ViewHolder viewHolder = new ApproveContactListAdapter.ViewHolder(itemLayoutView);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ApproveContactListAdapter.ViewHolder holder, int position) {

        PojoClasses.ContactInfo contactInfo = contactInfos.get(position);

        if (contactInfo.getContact_type() != null && !contactInfo.getContact_type().isEmpty() && !contactInfo.getContact_type().equals("null")) {


            String first = "<font color='#414141'>Contact Type: </font>";
            String sourceString = "<b>" + first + "</b> " + contactInfo.getContact_type();

            //tv_register.setText(Html.fromHtml(first + next));
            holder.tv_contact_type.setText(Html.fromHtml(sourceString));
        } else {
            holder.tv_contact_type.setText("");
            holder.tv_contact_type.setVisibility(View.GONE);
        }

        if (contactInfo.getEmail() != null && !contactInfo.getEmail().isEmpty() && !contactInfo.getEmail().equals("null")) {

            String first = "<font color='#414141'>Email:</font>";
            String sourceString = "<b>" + first + "</b> " + contactInfo.getEmail();
            holder.tv_contact_email.setText(Html.fromHtml(sourceString));
        } else {
            holder.tv_contact_email.setText("");
            holder.tv_contact_email.setVisibility(View.GONE);
        }

        if (contactInfo.getPhone() != null && !contactInfo.getPhone().isEmpty() && !contactInfo.getPhone().equals("null")) {
            String first = "<font color='#414141'>Phone:</font>";
            String sourceString = "<b>" + first + "</b> " + contactInfo.getPhone();
            holder.tv_contact_phone.setText(Html.fromHtml(sourceString));
        } else {
            holder.tv_contact_phone.setText("");
            holder.tv_contact_phone.setVisibility(View.GONE);
        }


        holder.btn_add_contact.setTag(contactInfo);
        holder.btn_add_contact.setOnClickListener(this);

        Typeface lato_regular = Typeface.createFromAsset(mActivity.getAssets(), "fonts/latoregular.ttf");
        Typeface lato_bold = Typeface.createFromAsset(mActivity.getAssets(), "fonts/latobold.ttf");
        Typeface semi_bold = Typeface.createFromAsset(mActivity.getAssets(), "fonts/latosemibold.ttf");

        holder.tv_contact_type.setTypeface(semi_bold);
        holder.tv_contact_email.setTypeface(semi_bold);
        holder.tv_contact_phone.setTypeface(semi_bold);

    }


    @Override
    public int getItemCount() {
        return contactInfos.size();
    }

    @Override
    public void onClick(View view) {

        PojoClasses.ContactInfo info = (PojoClasses.ContactInfo) view.getTag();

        //addContact("jatin devani",info.getPhone(),info.getEmail());


        //fnAddContact();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && mActivity.checkSelfPermission(Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
            mActivity.requestPermissions(new String[]{Manifest.permission.READ_CONTACTS}, PERMISSIONS_REQUEST_READ_CONTACTS);
            //After this point you wait for callback in onRequestPermissionsResult(int, String[], int[]) overriden method
        } else {

            if (contactExists(mActivity, info.getPhone())) {
                Toast.makeText(mActivity, "Contact already added", Toast.LENGTH_SHORT).show();
            } else {
                fnAddContact(info.getContact_type(), info.getPhone(), info.getEmail());
            }


        }

    }


    public void fnAddContact(String contect_type, String phone, String email) {
        ArrayList<ContentProviderOperation> ops = new ArrayList<ContentProviderOperation>();

        ops.add(ContentProviderOperation.newInsert(
                ContactsContract.RawContacts.CONTENT_URI)
                .withValue(ContactsContract.RawContacts.ACCOUNT_TYPE, null)
                .withValue(ContactsContract.RawContacts.ACCOUNT_NAME, null)
                .build());

        //------------------------------------------------------ Names
        if (contect_type != null) {
            ops.add(ContentProviderOperation.newInsert(
                    ContactsContract.Data.CONTENT_URI)
                    .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
                    .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE)
                    .withValue(ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME, contect_type).build());
        }

        //------------------------------------------------------ Mobile Number
        if (phone != null) {
            ops.add(ContentProviderOperation.
                    newInsert(ContactsContract.Data.CONTENT_URI)
                    .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
                    .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE)
                    .withValue(ContactsContract.CommonDataKinds.Phone.NUMBER, phone)
                    .withValue(ContactsContract.CommonDataKinds.Phone.TYPE, ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE)
                    .build());
        }


        //------------------------------------------------------ Email
        if (email != null) {
            ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                    .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
                    .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.Email.CONTENT_ITEM_TYPE)
                    .withValue(ContactsContract.CommonDataKinds.Email.DATA, email)
                    .withValue(ContactsContract.CommonDataKinds.Email.TYPE, ContactsContract.CommonDataKinds.Email.TYPE_WORK)
                    .build());
        }

        // Asking the Contact provider to create a new contact
        try {
            mActivity.getContentResolver().applyBatch(ContactsContract.AUTHORITY, ops);
            Toast.makeText(mActivity, "Contact added successfully", Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(mActivity, "Exception: " + e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    public boolean contactExists(Context context, String number) {
/// number is the phone number
        Uri lookupUri = Uri.withAppendedPath(
                ContactsContract.PhoneLookup.CONTENT_FILTER_URI,
                Uri.encode(number));
        String[] mPhoneNumberProjection = {ContactsContract.PhoneLookup._ID, ContactsContract.PhoneLookup.NUMBER, ContactsContract.PhoneLookup.DISPLAY_NAME};
        Cursor cur = context.getContentResolver().query(lookupUri, mPhoneNumberProjection, null, null, null);
        try {
            if (cur.moveToFirst()) {
                return true;
            }
        } finally {
            if (cur != null)
                cur.close();
        }
        return false;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PERMISSIONS_REQUEST_READ_CONTACTS) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission is granted
                //fnAddContact();
                Toast.makeText(mActivity, "Permission granted ", Toast.LENGTH_SHORT).show();

            } else {
                Toast.makeText(mActivity, "Until you grant the permission, we canot display the names", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_contact_type, tv_contact_email, tv_contact_phone;
        public ImageView btn_add_contact;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            tv_contact_type = itemLayoutView.findViewById(R.id.tv_contact_type);
            tv_contact_email = itemLayoutView.findViewById(R.id.tv_contact_email);
            tv_contact_phone = itemLayoutView.findViewById(R.id.tv_contact_phone);
            btn_add_contact = itemLayoutView.findViewById(R.id.btn_add_contact);

        }
    }
}
