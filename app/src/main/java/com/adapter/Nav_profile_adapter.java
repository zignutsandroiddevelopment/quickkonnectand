package com.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.interfaces.Refresh_Nav_Activity;
import com.models.profiles.ProfilesList;
import com.quickkonnect.CircleTransform;
import com.quickkonnect.R;
import com.realmtable.MyProfiles;
import com.squareup.picasso.Picasso;
import com.utilities.Constants;

import java.util.List;

import static com.utilities.Constants.TYPE_PROFILE_CELEBRITY;
import static com.utilities.Constants.TYPE_PROFILE_COMPANY;
import static com.utilities.Constants.TYPE_PROFILE_ENTERPRISE;

/**
 * Created by ZTLAB03 on 16-03-2018.
 */

public class Nav_profile_adapter extends RecyclerView.Adapter<Nav_profile_adapter.ViewHolder> {

    List<ProfilesList> navModelItems;
    Context context;
    Refresh_Nav_Activity refresh_nav_activity;

    public Nav_profile_adapter(List<ProfilesList> navModelItems, Context context, Refresh_Nav_Activity refresh_nav_activity) {
        this.navModelItems = navModelItems;
        this.context = context;
        this.refresh_nav_activity = refresh_nav_activity;
    }

    @Override
    public Nav_profile_adapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_nav, null);
        Nav_profile_adapter.ViewHolder viewHolder = new Nav_profile_adapter.ViewHolder(itemLayoutView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(Nav_profile_adapter.ViewHolder holder, int position) {
        final ProfilesList current = navModelItems.get(position);

        try {
            Picasso.with(context)
                    .load(current.getLogo() + "")
                    .error(R.drawable.user_profile_pic)
                    .placeholder(R.drawable.user_profile_pic)
                    .transform(new CircleTransform())
                    .fit()
                    .into(holder.icons);
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            if (current.getName() != null && !current.getName().isEmpty())
                holder.title.setText(current.getName());

            if (current.getType() != null) {
                try {

                    if (current.getType().equals(TYPE_PROFILE_COMPANY)) {

                        if (Integer.parseInt(current.getStatus()) == Constants.STATUS_0)
                            holder.tv_status_verification.setVisibility(View.VISIBLE);
                        else
                            holder.tv_status_verification.setVisibility(View.VISIBLE);

                    } else {

                        if (current.getStatus().equalsIgnoreCase("A")) {
                            holder.tv_status_verification.setVisibility(View.INVISIBLE);
                        } else if (current.getStatus().equalsIgnoreCase("R")) {
                            String text = "<font color='#FF0000'>Rejected &nbsp;&nbsp;&nbsp;</font><font color='#d1cfcf'>Details</font>";
                            holder.tv_status_verification.setVisibility(View.VISIBLE);
                            holder.tv_status_verification.setText(Html.fromHtml(text));
                            holder.tv_status_verification.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    new AlertDialog.Builder(context)
                                            .setTitle("Reason")
                                            .setMessage(current.getResson() + "")
                                            .setCancelable(false)
                                            .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    dialog.dismiss();
                                                }
                                            }).show();
                                }
                            });
                        } else {
                            holder.tv_status_verification.setVisibility(View.VISIBLE);
                        }
                    }
                    if (current.getType().equals(TYPE_PROFILE_ENTERPRISE)) {
                        holder.tv_name_type.setVisibility(View.VISIBLE);
                        holder.tv_name_type.setText("Enterprise");
                    } else if (current.getType().equals(TYPE_PROFILE_CELEBRITY)) {
                        holder.tv_name_type.setVisibility(View.VISIBLE);
                        holder.tv_name_type.setText("Celebrity/Public Figure");
                    } else {
                        holder.tv_name_type.setVisibility(View.GONE);
                    }

                   /* switch (profiletype) {
                        case TYPE_PROFILE_COMPANY:
                        holder.tv_name_type.setText("Company");
                            break;
                        case TYPE_PROFILE_ENTERPRISE:
                            holder.tv_name_type.setText("Enterprise");
                            break;
                        case TYPE_PROFILE_CELEBRITY:
                            holder.tv_name_type.setText("Celebrity/Public Figure");
                            break;
                        default:
                            holder.tv_name_type.setVisibility(View.GONE);
                    }*/
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        if (navModelItems.size() > 2) {
            return 2;
        } else {
            return navModelItems.size();
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ImageView icons;
        TextView title, tv_name_type, tv_status_verification;
        LinearLayout linearLayout;

        public ViewHolder(View itemView) {
            super(itemView);
            icons = itemView.findViewById(R.id.img_icon_nav);
            title = itemView.findViewById(R.id.tv_name_nav);
            tv_name_type = itemView.findViewById(R.id.tv_name_type);
            linearLayout = itemView.findViewById(R.id.linear_main_nav);
            tv_status_verification = itemView.findViewById(R.id.tv_status_verification);
            linearLayout.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int pos = getAdapterPosition();
            ProfilesList current = navModelItems.get(pos);
            if (view == linearLayout) {
                refresh_nav_activity.onRefresh(current);
            }
        }
    }
}

