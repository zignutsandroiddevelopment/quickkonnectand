package com.adapter;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.Log;

import com.fragments.Frag_Analytics;
import com.fragments.Frag_Employees;
import com.fragments.Frag_Followers;
import com.fragments.NewScanFragment;
import com.fragments.ScanFragSetting;
import com.realmtable.MyProfiles;

/**
 * Created by ZTLAB03 on 27-03-2018.
 */

public class NewScanViewPagerAdapter extends FragmentPagerAdapter {

    private String[] title = new String[]{""};
    private Bundle bundle;
    private String TabType = "";
    private MyProfiles myProfiles;


    public NewScanViewPagerAdapter(MyProfiles myProfiles, FragmentManager manager, Bundle bundle, String[] title, String TabType, String type, String status) {
        super(manager);
        this.myProfiles = myProfiles;
        this.bundle = bundle;
        this.TabType = TabType;
        this.title = title;
    }

    @Override
    public Fragment getItem(int position) {
        Log.d("position", position + "");

        //if (TabType.equalsIgnoreCase("BFA") || TabType.equalsIgnoreCase("BFAS")) {
        if (TabType.equalsIgnoreCase("BF") || TabType.equalsIgnoreCase("BFS")) {
            switch (position) {
                case 0:  //Basic details
                    return NewScanFragment.newInstance(position, bundle, myProfiles);
                case 1:  // Followers
                    return Frag_Followers.newInstance(position, bundle);
               /* case 2:  // Analytics
                    return Frag_Analytics.newInstance(position, bundle);*/
                case 2:  // Settings
                    return ScanFragSetting.newInstance(position, bundle, myProfiles);
                default:
                    return null;
            }
            //} else if (TabType.equalsIgnoreCase("BFEA") || TabType.equalsIgnoreCase("BF") || TabType.equalsIgnoreCase("BFEAS")) {
        } else if (TabType.equalsIgnoreCase("BFE") || TabType.equalsIgnoreCase("BFES")) {
            switch (position) {
                case 0:  //Basic details
                    return NewScanFragment.newInstance(position, bundle, myProfiles);
                case 1:  // Followers
                    return Frag_Followers.newInstance(position, bundle);
                case 2:  // Employees
                    return Frag_Employees.newInstance(position, bundle);
               /* case 3:  // Analytics
                    return Frag_Analytics.newInstance(position, bundle);*/
                case 3:  // Settings
                    return ScanFragSetting.newInstance(position, bundle, myProfiles);
                default:
                    return null;
            }
        } else {
            switch (position) {
                case 0:  //Basic details
                    return NewScanFragment.newInstance(position, bundle, myProfiles);
                case 1:  // Followers
                    return Frag_Followers.newInstance(position, bundle);
                default:
                    return null;
            }
        }

        /*if (type != null && type.equalsIgnoreCase("3")) {
            switch (position) {
                case 0:  //Basic details
                    return NewScanFragment.newInstance(position, bundle);
                case 1:  // Followers
                    return Frag_Followers.newInstance(position, bundle);
                case 2:  // Analytics
                    return Frag_Analytics.newInstance(position, bundle);
                case 3:  // Settings
                    return ScanFragSetting.newInstance(position, bundle);
                default:
                    return null;
            }
        } else if (type != null && type.equalsIgnoreCase("1")) {
            switch (position) {
                case 0:  //Basic details
                    return NewScanFragment.newInstance(position, bundle);
                case 1:  // Followers
                    return Frag_Followers.newInstance(position, bundle);
                case 2:  // Employees
                    return Frag_Employees.newInstance(position, bundle);
                case 3:  // Analytics
                    return Frag_Analytics.newInstance(position, bundle);
                case 4:  // Settings
                    return ScanFragSetting.newInstance(position, bundle);
                default:
                    return null;
            }
        } else {
            if (status.equalsIgnoreCase("A")) {
                switch (position) {
                    case 0:  //Basic details
                        return NewScanFragment.newInstance(position, bundle);
                    case 1:  // Followers
                        return Frag_Followers.newInstance(position, bundle);
                    case 2:  // Employees
                        return Frag_Employees.newInstance(position, bundle);
                    case 3:  // Analytics
                        return Frag_Analytics.newInstance(position, bundle);
                    case 4:  // Settings
                        return ScanFragSetting.newInstance(position, bundle);
                    default:
                        return null;
                }
            } else {
                switch (position) {
                    case 0:  //Basic details
                        return NewScanFragment.newInstance(position, bundle);
                    case 1:  // Followers
                        return Frag_Followers.newInstance(position, bundle);
                    case 2:  // Analytics
                        return Frag_Analytics.newInstance(position, bundle);
                    case 3:  // Settings
                        return ScanFragSetting.newInstance(position, bundle);
                    default:
                        return null;
                }
            }
        }*/
    }

    @Override
    public int getCount() {
        int count = 0;
        try {
            count = title.length;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return count;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return title[position];
    }

}