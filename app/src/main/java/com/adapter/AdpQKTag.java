package com.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.interfaces.OnQkTagInfo;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.quickkonnect.R;
import com.realmtable.MyProfiles;

import org.json.JSONObject;

import java.util.List;

public class AdpQKTag extends RecyclerView.Adapter<AdpQKTag.MyTagHolder> {

    Context context;
    List<MyProfiles> mDataimg;
    OnQkTagInfo onQkTagInfo;
    DisplayImageOptions imageOptions;
    ImageLoader imageLoader;

    public AdpQKTag(Context context, List<MyProfiles> mDataimg, OnQkTagInfo onQkTagInfo) {
        this.context = context;
        this.mDataimg = mDataimg;
        this.onQkTagInfo = onQkTagInfo;
        imageLoader = ImageLoader.getInstance();
        imageOptions = new DisplayImageOptions.Builder()
                .showImageForEmptyUri(R.drawable.ic_logo_blue_qk)
                .showImageOnFail(R.drawable.ic_logo_blue_qk)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .build();
    }

    @Override
    public MyTagHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.frag_qktags, null);

        ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        itemLayoutView.setLayoutParams(layoutParams);

        MyTagHolder viewHolder = new MyTagHolder(itemLayoutView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(MyTagHolder holder, int position) {

        try {
            final MyProfiles myProfiles = mDataimg.get(position);

            if (myProfiles != null && myProfiles.getName() != null) {
                holder.tv_cardname.setText(myProfiles.getName());
            }
            JSONObject jsonObject = new JSONObject(myProfiles.getQktaginfo());
            if (jsonObject.optString("qr_code") != null && !jsonObject.optString("qr_code").isEmpty())
                imageLoader.displayImage(jsonObject.optString("qr_code"), holder.imgTags, imageOptions);

            holder.imgTags.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                   // onQkTagInfo.OnQKTaginfo(myProfiles);
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return mDataimg.size();
    }

    public class MyTagHolder extends RecyclerView.ViewHolder {

        ImageView imgTags;
        TextView tv_cardname;

        public MyTagHolder(View itemView) {
            super(itemView);
            imgTags = itemView.findViewById(R.id.imgTags);
            tv_cardname = itemView.findViewById(R.id.tv_cardname);

        }
    }
}