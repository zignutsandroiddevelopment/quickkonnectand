package com.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.media.Image;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.interfaces.OnItemClick;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.quickkonnect.CircleTransform;
import com.quickkonnect.NavModelItems;
import com.quickkonnect.R;
import com.squareup.picasso.Picasso;
import com.utilities.QKPreferences;

import java.util.List;

import static com.utilities.Constants.TYPE_PROFILE_CELEBRITY;
import static com.utilities.Constants.TYPE_PROFILE_COMPANY;
import static com.utilities.Constants.TYPE_PROFILE_EMPLOYEE;
import static com.utilities.Constants.TYPE_PROFILE_ENTERPRISE;

/**
 * Created by ZTLAB03 on 14-03-2018.
 */

public class NavItemAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    List<NavModelItems> navModelItems;
    QKPreferences qkPreferences;
    public static int viewTypeHeader = 1;
    public static int viewTypeItem = 2;
    public static int viewTypeDivider = 3;
    public static int viewTypeProfile = 4;
    ImageLoader imageLoader;
    DisplayImageOptions options;
    Context context;
    OnItemClick onItemClick;
    public boolean isHideSetting = false;

    public NavItemAdapter(List<NavModelItems> navModelItems, Context context, OnItemClick onItemClick) {
        this.navModelItems = navModelItems;
        this.context = context;
        this.onItemClick = onItemClick;
        this.qkPreferences = new QKPreferences(context);
        this.imageLoader = ImageLoader.getInstance();
        options = new DisplayImageOptions.Builder()
                .displayer(new RoundedBitmapDisplayer(1000))
                .cacheInMemory(true)
                .showImageForEmptyUri(R.drawable.ic_logo)
                .showImageOnFail(R.drawable.ic_logo)
                .showImageOnLoading(R.drawable.ic_logo)
                .cacheOnDisk(true)
                .build();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemLayoutView = null;

        if (viewType == viewTypeHeader) {

            itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_navigation_header, parent, false);
            RecyclerView.ViewHolder viewHolder = new NavItemAdapter.HeaderHolder(itemLayoutView);
            return viewHolder;

        } else if (viewType == viewTypeDivider) {

            itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_navigation_divider, parent, false);
            RecyclerView.ViewHolder viewHolder = new NavItemAdapter.DividerHolder(itemLayoutView);
            return viewHolder;

        } else if (viewType == viewTypeProfile) {

            itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_nav, parent, false);
            RecyclerView.ViewHolder viewHolder = new NavItemAdapter.ItemHolder(itemLayoutView);
            return viewHolder;

        } else if (viewType == viewTypeItem) {

            itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_nav_setting_items, parent, false);
            RecyclerView.ViewHolder viewHolder = new NavItemAdapter.ItemSettingHolder(itemLayoutView);
            return viewHolder;

        }

        return null;
    }

    @Override
    public int getItemViewType(int position) {
        return navModelItems.get(position).getViewType();
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ItemHolder) {

            ItemHolder itemHolder = (ItemHolder) holder;

            final NavModelItems current = navModelItems.get(position);

           /* if (current.getViewType() == viewTypeItem) {
                itemHolder.icons.setImageResource(current.getIcon());

            } else */
            if (current.getViewType() == viewTypeProfile) {

                if (current.getTitle() != null && !current.getTitle().isEmpty())
                    itemHolder.title.setText(current.getTitle());

                imageLoader.displayImage("" + current.getLogo(), itemHolder.icons, options);
                if (current.getStatus().equalsIgnoreCase("A")) {
                    itemHolder.tv_status_verification.setVisibility(View.INVISIBLE);

                    if (current.getType() != null && current.getType().equals(TYPE_PROFILE_ENTERPRISE)) {
                        itemHolder.tv_name_type.setVisibility(View.VISIBLE);
                        itemHolder.tv_name_type.setText("Enterprise");
                        itemHolder.tv_status_verification.setVisibility(View.GONE);
                    } else if (current.getType() != null && current.getType().equals(TYPE_PROFILE_CELEBRITY)) {
                        itemHolder.tv_name_type.setVisibility(View.VISIBLE);
                        itemHolder.tv_name_type.setText("Celebrity/Public Figure");
                        itemHolder.tv_status_verification.setVisibility(View.GONE);
                    } else if (current.getType() != null && current.getType().equals(TYPE_PROFILE_COMPANY)) {
                        itemHolder.tv_name_type.setVisibility(View.VISIBLE);
                        itemHolder.tv_name_type.setText("Company");
                        itemHolder.tv_status_verification.setVisibility(View.GONE);
                    } else if (current.getType() != null && current.getType().equals(TYPE_PROFILE_EMPLOYEE)) {
                        itemHolder.tv_name_type.setVisibility(View.VISIBLE);
                        itemHolder.tv_name_type.setText("Employee");
                        itemHolder.tv_status_verification.setVisibility(View.GONE);
                    } else {
                        itemHolder.tv_name_type.setVisibility(View.GONE);
                    }

                } else if (current.getStatus().equalsIgnoreCase("R")) {
                    String text = "<font color='#FF0000'>Rejected &nbsp;&nbsp;&nbsp;</font><font color='#d1cfcf'>Details</font>";
                    itemHolder.tv_status_verification.setVisibility(View.VISIBLE);
                    itemHolder.tv_status_verification.setText(Html.fromHtml(text));
                    itemHolder.tv_status_verification.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            new AlertDialog.Builder(context)
                                    .setTitle("Reason")
                                    .setMessage(current.getResson() + "")
                                    .setCancelable(false)
                                    .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                        }
                                    }).show();
                        }
                    });

                    if (current.getType() != null && current.getType().equals(TYPE_PROFILE_ENTERPRISE)) {
                        itemHolder.tv_name_type.setVisibility(View.VISIBLE);
                        itemHolder.tv_name_type.setText("Enterprise");
                    } else if (current.getType() != null && current.getType().equals(TYPE_PROFILE_CELEBRITY)) {
                        itemHolder.tv_name_type.setVisibility(View.VISIBLE);
                        itemHolder.tv_name_type.setText("Celebrity/Public Figure");
                    } else if (current.getType() != null && current.getType().equals(TYPE_PROFILE_COMPANY)) {
                        itemHolder.tv_name_type.setVisibility(View.VISIBLE);
                        itemHolder.tv_name_type.setText("Company");
                    } else if (current.getType() != null && current.getType().equals(TYPE_PROFILE_EMPLOYEE)) {
                        itemHolder.tv_name_type.setVisibility(View.VISIBLE);
                        itemHolder.tv_name_type.setText("Employee");
                    } else {
                        itemHolder.tv_name_type.setVisibility(View.GONE);
                    }
                } else {
                    itemHolder.tv_status_verification.setVisibility(View.VISIBLE);
                    if (current.getType() != null && current.getType().equals(TYPE_PROFILE_ENTERPRISE)) {
                        itemHolder.tv_name_type.setVisibility(View.VISIBLE);
                        itemHolder.tv_name_type.setText("Enterprise");
                    } else if (current.getType() != null && current.getType().equals(TYPE_PROFILE_CELEBRITY)) {
                        itemHolder.tv_name_type.setVisibility(View.VISIBLE);
                        itemHolder.tv_name_type.setText("Celebrity/Public Figure");
                    } else if (current.getType() != null && current.getType().equals(TYPE_PROFILE_COMPANY)) {
                        itemHolder.tv_name_type.setVisibility(View.VISIBLE);
                        itemHolder.tv_name_type.setText("Company");
                    } else if (current.getType() != null && current.getType().equals(TYPE_PROFILE_EMPLOYEE)) {
                        itemHolder.tv_name_type.setVisibility(View.VISIBLE);
                        itemHolder.tv_name_type.setText("Employee");
                    } else {
                        itemHolder.tv_name_type.setVisibility(View.GONE);
                    }
                }

                itemHolder.linearLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (current.getViewType() == viewTypeItem) {
                            onItemClick.getItemClick(1, current, current.getTitle());
                        } else if (current.getViewType() == viewTypeProfile) {
                            onItemClick.getItemClick(2, current, current.getTitle());
                        }
                    }
                });

            }

        } else if (holder instanceof HeaderHolder) {

            HeaderHolder headerHolder = (HeaderHolder) holder;

            String user_fullname = qkPreferences.getValues(QKPreferences.USER_FULLNAME);
            String upperString = null;
            try {
                upperString = user_fullname.substring(0, 1).toUpperCase() + user_fullname.substring(1);
                headerHolder.tv_name_nav_main.setText(user_fullname + "");
            } catch (Exception e) {
                e.printStackTrace();
            }

            String profile_picture = qkPreferences.getValues(QKPreferences.USER_PROFILEIMG);
            //imageLoader.displayImage(profile_picture, headerHolder.imageView_header_nav, options);
            try {
                if (profile_picture != null && !profile_picture.isEmpty()) {
                    Picasso.with(context)
                            .load(profile_picture)
                            .transform(new CircleTransform())
                            .placeholder(R.drawable.ic_default_profile_photo)
                            .fit()
                            .into(headerHolder.imageView_header_nav);
                } else
                    headerHolder.imageView_header_nav.setImageResource(R.drawable.user_profile_pic);
            } catch (Exception e) {
                e.printStackTrace();
            }

            int contactn_all = qkPreferences.getValuesIntDefault0(QKPreferences.USER_CONT_ALL);
            headerHolder.tv_contact_all.setText("" + contactn_all);

            int contactn_week = qkPreferences.getValuesIntDefault0(QKPreferences.USER_CONT_WEEK);
            headerHolder.tv_contact_week.setText("" + contactn_week);

            int contactn_month = qkPreferences.getValuesIntDefault0(QKPreferences.USER_CONT_MONTH);
            headerHolder.tv_contact_month.setText("" + contactn_month);

            headerHolder.linerRootview.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onItemClick.getItemClick(4, null, null);
                }
            });

        } else if (holder instanceof DividerHolder) {

            final NavModelItems current = navModelItems.get(position);

            DividerHolder dividerHolder = (DividerHolder) holder;

            if (isHideSetting) {
                dividerHolder.img_up_down_aero.setImageResource(R.drawable.arrowdown);
            } else {
                dividerHolder.img_up_down_aero.setImageResource(R.drawable.arrowup);
            }

            dividerHolder.linearLayoutCreateProfile.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onItemClick.getItemClick(3, null, "createProfile");
                }
            });
            dividerHolder.linearLayoutViewAll.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onItemClick.getItemClick(3, null, "viewAllProfile");
                }
            });
            dividerHolder.linearsetting.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        onItemClick.getItemClick(5, null, "Setting");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        } else if (holder instanceof ItemSettingHolder) {

            final NavModelItems current = navModelItems.get(position);

            ItemSettingHolder itemSettingHolder = (ItemSettingHolder) holder;
            itemSettingHolder.icons.setImageResource(current.getIcon());

            if (current.getTitle() != null && !current.getTitle().isEmpty())
                itemSettingHolder.title.setText(current.getTitle());

            itemSettingHolder.linearLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (current.getViewType() == viewTypeItem) {
                        onItemClick.getItemClick(1, current, current.getTitle());
                    } else if (current.getViewType() == viewTypeProfile) {
                        onItemClick.getItemClick(2, current, current.getTitle());
                    }
                }
            });

            if (isHideSetting) {
                itemSettingHolder.linearLayout.setVisibility(View.GONE);
            } else {
                itemSettingHolder.linearLayout.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public int getItemCount() {
        return navModelItems.size();
    }

    public class DividerHolder extends RecyclerView.ViewHolder {
        LinearLayout linearLayoutCreateProfile, linearLayoutViewAll, linearsetting;
        ImageView img_up_down_aero;

        public DividerHolder(View itemView) {
            super(itemView);
            linearLayoutCreateProfile = itemView.findViewById(R.id.linearCreateProfile);
            linearLayoutViewAll = itemView.findViewById(R.id.linaerViewAllAccount);
            linearsetting = itemView.findViewById(R.id.linearsetting);
            img_up_down_aero = itemView.findViewById(R.id.img_up_down_aero);
        }
    }

    public class HeaderHolder extends RecyclerView.ViewHolder {
        TextView tv_name_nav_main, tv_contact_all, tv_contact_week, tv_contact_month;
        ImageView imageView_header_nav;
        LinearLayout linerRootview;

        public HeaderHolder(View itemView) {
            super(itemView);
            tv_name_nav_main = itemView.findViewById(R.id.tv_name_nav_main);
            imageView_header_nav = itemView.findViewById(R.id.imageView_header_nav);
            tv_contact_all = itemView.findViewById(R.id.tv_contact_all);
            tv_contact_week = itemView.findViewById(R.id.tv_contact_week);
            tv_contact_month = itemView.findViewById(R.id.tv_contact_month);
            linerRootview = itemView.findViewById(R.id.linerRootview);
        }
    }

    public class ItemHolder extends RecyclerView.ViewHolder {
        ImageView icons;
        TextView title, tv_name_type, tv_status_verification;
        LinearLayout linearLayout;

        public ItemHolder(View itemView) {
            super(itemView);
            icons = itemView.findViewById(R.id.img_icon_nav);
            //bg = itemView.findViewById(R.id.img_back_profile);
            //bg.setVisibility(View.VISIBLE);
            title = itemView.findViewById(R.id.tv_name_nav);
            linearLayout = itemView.findViewById(R.id.linear_main_nav);
            tv_status_verification = itemView.findViewById(R.id.tv_status_verification);
            tv_name_type = itemView.findViewById(R.id.tv_name_type);
            tv_name_type.setVisibility(View.GONE);
        }
    }

    public class ItemSettingHolder extends RecyclerView.ViewHolder {

        ImageView icons;
        TextView title;
        LinearLayout linearLayout;

        public ItemSettingHolder(View itemView) {
            super(itemView);
            icons = itemView.findViewById(R.id.img_linearsetting);
            title = itemView.findViewById(R.id.tv_linearsetting);
            linearLayout = itemView.findViewById(R.id.line_arsetting);
        }
    }
}

