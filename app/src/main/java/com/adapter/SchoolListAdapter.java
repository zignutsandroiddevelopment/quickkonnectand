package com.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.TextView;

import com.activities.UpdateEducationActivity;
import com.quickkonnect.CircleTransform;
import com.quickkonnect.R;
import com.squareup.picasso.Picasso;
import com.utilities.Utilities;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ZTLAB-12 on 14-05-2018.
 */

public class SchoolListAdapter extends ArrayAdapter<UpdateEducationActivity.SchoolList> {

    private Context context;
    private int resourceId;
    private List<UpdateEducationActivity.SchoolList> items, tempItems, suggestions;

    public SchoolListAdapter(Context context, int resourceId, ArrayList<UpdateEducationActivity.SchoolList> items) {
        super(context, resourceId, items);
        this.items = items;
        this.context = context;
        this.resourceId = resourceId;
        tempItems = new ArrayList<>(items);
        suggestions = new ArrayList<>();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        try {
            if (convertView == null) {
                LayoutInflater inflater = ((Activity) context).getLayoutInflater();
                view = inflater.inflate(resourceId, parent, false);
            }
            UpdateEducationActivity.SchoolList fruit = getItem(position);
            TextView name = view.findViewById(R.id.textView_header_school);
            ImageView imageView = view.findViewById(R.id.imageView_header_school);
            name.setText(fruit.getName());
            //imageView.setImageResource(R.drawable.ic_default_profile_photo);
            Picasso.with((Activity) context)
                    .load(fruit.getLogo() + "")
                    .error(R.drawable.ic_default_profile_photo)
                    .placeholder(R.drawable.ic_default_profile_photo)
                    .into(imageView);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return view;
    }

    @Override
    public UpdateEducationActivity.SchoolList getItem(int position) {
        return items.get(position);
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public Filter getFilter() {
        return fruitFilter;
    }

    private Filter fruitFilter = new Filter() {
        @Override
        public CharSequence convertResultToString(Object resultValue) {
            UpdateEducationActivity.SchoolList fruit = (UpdateEducationActivity.SchoolList) resultValue;
            return fruit.getName();
        }

        @Override
        protected FilterResults performFiltering(CharSequence charSequence) {
            if (charSequence != null) {
                suggestions.clear();
                for (UpdateEducationActivity.SchoolList fruit : tempItems) {
                    if (fruit.getName().toLowerCase().startsWith(charSequence.toString().toLowerCase())) {
                        suggestions.add(fruit);
                    }
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = suggestions;
                filterResults.count = suggestions.size();
                return filterResults;
            } else {
                return new FilterResults();
            }
        }

        @Override
        protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
            ArrayList<UpdateEducationActivity.SchoolList> c = (ArrayList<UpdateEducationActivity.SchoolList>) filterResults.values;
            if (filterResults != null && filterResults.count > 0) {
                clear();
                for (UpdateEducationActivity.SchoolList cust : c) {
                    add(cust);
                    notifyDataSetChanged();
                }
            } else {
                clear();
                notifyDataSetChanged();
            }
        }
    };
}