package com.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.interfaces.OnEditMedia;
import com.interfaces.OnSingleEdit;
import com.models.createprofile.SocialMedium;
import com.quickkonnect.QuickKonnect_Webview;
import com.quickkonnect.R;
import com.utilities.Constants;

import java.util.List;

/**
 * Created by ZTLAB-10 on 21-02-2018.
 */

public class AdpSocialMedium extends RecyclerView.Adapter<AdpSocialMedium.ViewHolder> {

    private Context context;
    private List<SocialMedium> socialMediumList;
    private OnEditMedia onEditMedia;
    private boolean isEditenable = false;
    private String visited_user_id = "0";
    private String visited_profile_id = "0";

    public AdpSocialMedium(Context context, List<SocialMedium> socialMediumList, OnEditMedia onEditMedia, boolean isEditenable, String visited_user_id, String visited_profile_id) {
        this.context = context;
        this.onEditMedia = onEditMedia;
        this.isEditenable = isEditenable;
        this.socialMediumList = socialMediumList;
        this.visited_user_id = visited_user_id;
        this.visited_profile_id = visited_profile_id;
    }

    @Override
    public AdpSocialMedium.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_founders, null);
        AdpSocialMedium.ViewHolder viewHolder = new AdpSocialMedium.ViewHolder(itemLayoutView);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(AdpSocialMedium.ViewHolder holder, final int position) {

        final SocialMedium socialMedium = socialMediumList.get(position);

        holder.tv_social_media_name.setText(socialMedium.getName());
        String media_type = "";
        if (socialMedium.getType() != null) {
            if (socialMedium.getType().equals(Constants.SOCIAL_FACEBOOK_ID)) {
                media_type = "Facebook";
            } else if (socialMedium.getType().equals(Constants.SOCIAL_LINKEDIN_ID)) {
                media_type = "LinkedIn";
            } else if (socialMedium.getType().equals(Constants.SOCIAL_TWITTER_ID)) {
                media_type = "Twitter";
            } else if (socialMedium.getType().equals(Constants.SOCIAL_INSTAGRAM_ID)) {
                media_type = "Instagram";
            } else if (socialMedium.getType().equals(Constants.SOCIAL_GOOGLE_PLUS)) {
                media_type = "Google Plus";
            }
            String html = "<html><font color='#bcbcbc'> " + media_type + "</font></html>";
            holder.tv_social_media_type.setText(Html.fromHtml(html));
        } else holder.tv_social_media_type.setVisibility(View.GONE);

        holder.tv_openurl.setVisibility(View.VISIBLE);
        holder.tv_openurl.setText(Html.fromHtml("<font color='#4286f4'><u>Open Social Media URL</u></font>"));

        if (isEditenable)
            holder.imgDelete.setVisibility(View.VISIBLE);
        else
            holder.imgDelete.setVisibility(View.GONE);

        holder.imgDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onEditMedia.getEditMedia(position, socialMedium, OnSingleEdit.DELETE);
            }
        });

        holder.llData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isEditenable)
                    onEditMedia.getEditMedia(position, socialMedium, OnSingleEdit.EDIT);
            }
        });

        holder.tv_openurl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (socialMedium != null && socialMedium.getName() != null) {
                    String publicationstr = socialMedium.getName();
                    if (!publicationstr.contains("http:"))
                        publicationstr = "http://" + publicationstr;

                    String media_type = "";
                    if (socialMedium.getType() != null) {
                        if (socialMedium.getType().equals(Constants.SOCIAL_FACEBOOK_ID)) {
                            media_type = "Facebook";
                        } else if (socialMedium.getType().equals(Constants.SOCIAL_LINKEDIN_ID)) {
                            media_type = "LinkedIn";
                        } else if (socialMedium.getType().equals(Constants.SOCIAL_TWITTER_ID)) {
                            media_type = "Twitter";
                        } else if (socialMedium.getType().equals(Constants.SOCIAL_INSTAGRAM_ID)) {
                            media_type = "Instagram";
                        } else if (socialMedium.getType().equals(Constants.SOCIAL_GOOGLE_PLUS)) {
                            media_type = "Google Plus";
                        }
                    } else media_type = "Social";
                    context.startActivity(new Intent(context, QuickKonnect_Webview.class)
                            .putExtra("publication", "" + publicationstr.replace("http://", ""))
                            .putExtra("title", "" + media_type)
                            .putExtra("VISITED_USER_ID", visited_user_id + "")
                            .putExtra("PROFILE_ID", visited_profile_id + "")
                            .putExtra("TYPE", "S"));

                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return socialMediumList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView tv_social_media_name, tv_social_media_type, tv_openurl;
        private LinearLayout imgDelete, llData;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            tv_social_media_name = itemLayoutView.findViewById(R.id.tv_name);
            tv_social_media_type = itemLayoutView.findViewById(R.id.tv_designation);
            imgDelete = itemLayoutView.findViewById(R.id.imgDelete);
            tv_openurl = itemLayoutView.findViewById(R.id.tv_openurl);
            llData = itemLayoutView.findViewById(R.id.llData);
        }
    }
}
