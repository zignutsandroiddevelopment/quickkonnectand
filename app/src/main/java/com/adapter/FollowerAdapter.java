package com.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.interfaces.OnContactClick;
import com.quickkonnect.CircleTransform;
import com.quickkonnect.PojoClasses;
import com.quickkonnect.R;
import com.squareup.picasso.Picasso;
import com.utilities.Utilities;

import java.util.ArrayList;

/**
 * Created by ZTLAB03 on 29-03-2018.
 */

public class FollowerAdapter extends RecyclerView.Adapter<FollowerAdapter.ViewHolder> implements View.OnClickListener {

    private ArrayList<PojoClasses.FollowersList> followersLists;
    private Context context;

    public FollowerAdapter(Activity activity, ArrayList<PojoClasses.FollowersList> list) {
        context = activity;
        this.followersLists = list;
    }

    @Override
    public FollowerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_followers_list_adapter, null);
        FollowerAdapter.ViewHolder viewHolder = new FollowerAdapter.ViewHolder(itemLayoutView);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(FollowerAdapter.ViewHolder holder, int position) {

        PojoClasses.FollowersList followersList = followersLists.get(position);
        holder.tv_name.setText(followersList.getName() + "");
        holder.tv_email.setText(followersList.getEmail() + "");

        if (followersList.getProfile_pic() != null && !followersList.getProfile_pic().isEmpty() && !followersList.getProfile_pic().equals("null")) {
            Picasso.with(context)
                    .load(followersList.getProfile_pic())
                    .error(R.drawable.ic_default_profile_photo)
                    .placeholder(R.drawable.ic_default_profile_photo)
                    .transform(new CircleTransform())
                    .fit()
                    .into(holder.img_profile_pic);
        } else {
            holder.img_profile_pic.setImageResource(R.drawable.ic_default_profile_photo);
        }

        holder.tv_name.setTag(followersList);
        holder.tv_name.setOnClickListener(this);

        holder.img_profile_pic.setTag(followersList);
        holder.img_profile_pic.setOnClickListener(this);
    }

    @Override
    public int getItemCount() {
        return followersLists.size();
    }

    @Override
    public void onClick(View view) {

        PojoClasses.FollowersList follow = (PojoClasses.FollowersList) view.getTag();
        switch (view.getId()) {
            case R.id.tv_name:
            case R.id.img_profile_pic:
                //onContactClick.getContact(contact);
                break;
        }

    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_name, tv_email;
        ImageView img_profile_pic;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            tv_name = itemLayoutView.findViewById(R.id.tv_name_follow);
            tv_email = itemLayoutView.findViewById(R.id.tv_email_follow);
            img_profile_pic = itemLayoutView.findViewById(R.id.img_profile_pic_follow);

        }
    }

    public void filterList(ArrayList<PojoClasses.FollowersList> filterdNames) {
        this.followersLists = filterdNames;
        notifyDataSetChanged();
    }
}
