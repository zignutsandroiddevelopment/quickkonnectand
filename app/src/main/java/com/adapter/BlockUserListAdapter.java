package com.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.interfaces.BlockUserListInterface;
import com.interfaces.BlockedProfile;
import com.quickkonnect.CircleTransform;
import com.quickkonnect.PojoClasses;
import com.quickkonnect.R;
import com.squareup.picasso.Picasso;
import com.utilities.Utilities;

import java.util.ArrayList;

/**
 * Created by ZTLAB03 on 04-05-2018.
 */

public class BlockUserListAdapter extends RecyclerView.Adapter<BlockUserListAdapter.ViewHolder> {

    private ArrayList<PojoClasses.BlockUserList> emplist;
    private BlockUserListInterface blockedProfile;
    private Context context;

    public BlockUserListAdapter(Activity activity, ArrayList<PojoClasses.BlockUserList> list, BlockUserListInterface blockedProfile) {
        context = activity;
        this.emplist = list;
        this.blockedProfile = blockedProfile;
    }

    @Override
    public BlockUserListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_contacts_list_adapter, null);

        BlockUserListAdapter.ViewHolder viewHolder = new BlockUserListAdapter.ViewHolder(itemLayoutView, blockedProfile, emplist);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(BlockUserListAdapter.ViewHolder holder, int position) {
        try {
            PojoClasses.BlockUserList contactsList = emplist.get(position);

            if (Utilities.isEmpty(contactsList.getName())) {
                holder.tv_name.setText(contactsList.getName());
            } else {
                holder.tv_name.setText("");
            }

            if (contactsList.getProfile_pic() != null && !contactsList.getProfile_pic().isEmpty() && !contactsList.getProfile_pic().equals("null")) {
                Picasso.with(context)
                        .load(contactsList.getProfile_pic())
                        .error(R.drawable.ic_default_profile_photo)
                        .placeholder(R.drawable.ic_default_profile_photo)
                        .transform(new CircleTransform())
                        .fit()
                        .into(holder.img_profile_pic);
            } else {
                holder.img_profile_pic.setImageResource(R.drawable.ic_default_profile_photo);
            }
            Typeface semi_bold = Typeface.createFromAsset(context.getAssets(), "fonts/latosemibold.ttf");
            holder.tv_name.setTypeface(semi_bold);
            holder.tv_date.setVisibility(View.VISIBLE);
            if (Utilities.isEmpty(contactsList.getEmail())) {
                holder.tv_date.setText(contactsList.getEmail());
            } else {
                holder.tv_date.setText("");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return emplist.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView tv_name, tv_date;
        ImageView img_profile_pic, img_next_arrow;
        BlockUserListInterface blockedProfile;
        ArrayList<PojoClasses.BlockUserList> emplist;

        public ViewHolder(View itemLayoutView, BlockUserListInterface blockedProfile, ArrayList<PojoClasses.BlockUserList> emplist) {
            super(itemLayoutView);
            this.blockedProfile = blockedProfile;
            this.emplist = emplist;
            tv_name = itemLayoutView.findViewById(R.id.tv_name);
            tv_date = itemLayoutView.findViewById(R.id.tv_date);
            img_profile_pic = itemLayoutView.findViewById(R.id.img_profile_pic);
            img_next_arrow = itemLayoutView.findViewById(R.id.img_next_arrow);

            tv_name.setOnClickListener(this);
            img_profile_pic.setOnClickListener(this);
            img_next_arrow.setOnClickListener(this);
            tv_date.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int pos = getAdapterPosition();
            PojoClasses.BlockUserList current = emplist.get(pos);
            switch (view.getId()) {
                case R.id.tv_name:
                case R.id.img_profile_pic:
                case R.id.img_next_arrow:
                case R.id.tv_date:
                    blockedProfile.onBlockedClickerd(pos, current);
                    break;
            }
        }
    }
}
