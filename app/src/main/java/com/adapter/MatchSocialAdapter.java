package com.adapter;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Color;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.vision.Frame;
import com.interfaces.SocialmediaClick;
import com.quickkonnect.LoginActivity;
import com.quickkonnect.PojoClasses;
import com.quickkonnect.QuickKonnect_Webview;
import com.quickkonnect.R;
import com.utilities.Constants;
import com.utilities.Utilities;

import java.util.ArrayList;
import java.util.List;

import static com.activities.CreateProfileActivity.context;
import static com.linkedin.Config.LINKEDIN_PACKAGE_NAME;
import static com.linkedin.Config.SNAPCHAT_PACKAGE_NAME;

/**
 * Created by ZTLAB03 on 15-11-2017.
 */

public class MatchSocialAdapter extends RecyclerView.Adapter<MatchSocialAdapter.ViewHolder> {

    ArrayList<PojoClasses.SocialScanResult> socialScanResults;
    static Activity mActivity;
    private String visited_user_id = "0";
    private String visited_profile_id = "0";
    SocialmediaClick socialmediaClick;

    public MatchSocialAdapter(Activity activity, ArrayList<PojoClasses.SocialScanResult> list, String visited_user_id, SocialmediaClick socialmediaClick) {
        this.socialScanResults = list;
        mActivity = activity;
        this.socialmediaClick = socialmediaClick;
        this.visited_user_id = visited_user_id;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_social_media_match_prfoile, parent, false);
        MatchSocialAdapter.ViewHolder viewHolder = new MatchSocialAdapter.ViewHolder(itemLayoutView);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        final PojoClasses.SocialScanResult scanResult = socialScanResults.get(position);

        if (scanResult.getSocial_media_id().equals("1")) {
            holder.img_socialmedia_logo.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.ic_social_fb));
        } else if (scanResult.getSocial_media_id().equals("2")) {
            holder.img_socialmedia_logo.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.ic_social_linkin));
        } else if (scanResult.getSocial_media_id().equals("3")) {
            holder.img_socialmedia_logo.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.ic_social_tweet));
        } else if (scanResult.getSocial_media_id().equals("4")) {
            holder.img_socialmedia_logo.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.ic_social_insta));
        } else if (scanResult.getSocial_media_id().equals("5")) {
            holder.img_socialmedia_logo.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.ic_social_gplus));
        } else if (scanResult.getSocial_media_id().equals("6")) {
            holder.img_socialmedia_logo.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.share_chat));
        }

        holder.frag_img_social.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String strUsername = scanResult.getSocial_religion();
                String appid = scanResult.getSocial_media_user_id() + "";
                final String profileurl = scanResult.getPublicProfileUrl();
                if (scanResult.getSocial_media_id().equals("1")) {
                    OpenFacebookPage(mActivity, appid);

                } else if (scanResult.getSocial_media_id().equals("2")) {
                    if (profileurl != null && !profileurl.equalsIgnoreCase("")) {
                        try {
                            String idStr = profileurl.substring(profileurl.lastIndexOf('/') + 1);
                            Intent intent = new Intent(mActivity, QuickKonnect_Webview.class);
                            intent.putExtra("other", profileurl + "");
                            intent.putExtra(Constants.KEY, "LinkedIn");
                            intent.putExtra("VISITED_USER_ID", visited_user_id + "");
                            intent.putExtra("PROFILE_ID", visited_profile_id + "");
                            intent.putExtra("TYPE", "S");
                            mActivity.startActivity(intent);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                        /*if (scanResult.getSocial_media_user_id() != null && !scanResult.getSocial_media_user_id().equalsIgnoreCase("null") && !scanResult.getSocial_media_user_id().equalsIgnoreCase(""))
                            socialmediaClick.onClickSocialMedia(visited_profile_id, scanResult.getSocial_media_user_id() + "");
                        else {
                            String idStr = profileurl.substring(profileurl.lastIndexOf('/') + 1);
                            socialmediaClick.onClickSocialMedia(visited_profile_id, idStr + "");
                        }*/



                       /* try {
                            String socialmediauserid = scanResult.getSocial_media_user_id() + "";
                            String idStr = profileurl.substring(profileurl.lastIndexOf('/') + 1);
                            //intent.setData(Uri.parse("linkedin://profile/"+profileId));
                            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("linkedin://" + socialmediauserid));
                            final PackageManager packageManager = mActivity.getPackageManager();
                            final List<ResolveInfo> list = packageManager.queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
                            if (list.isEmpty()) {
                                //intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.linkedin.com/profile/view?id=you"));
                                intent = new Intent(mActivity, QuickKonnect_Webview.class);
                                intent.putExtra("other", "http://www.linkedin.com/profile/view?id=" + idStr);
                                intent.putExtra(Constants.KEY, "LinkedIn");
                                intent.putExtra("VISITED_USER_ID", visited_user_id + "");
                                intent.putExtra("PROFILE_ID", visited_profile_id + "");
                                intent.putExtra("TYPE", "S");
                            }
                            mActivity.startActivity(intent);
                        } catch (Exception e) {
                            e.printStackTrace();
                            Intent webviewintent = new Intent(mActivity, QuickKonnect_Webview.class);
                            webviewintent.putExtra("other", profileurl);
                            webviewintent.putExtra(Constants.KEY, "LinkedIn");
                            webviewintent.putExtra("VISITED_USER_ID", visited_user_id + "");
                            webviewintent.putExtra("PROFILE_ID", visited_profile_id + "");
                            webviewintent.putExtra("TYPE", "S");
                            mActivity.startActivity(webviewintent);
                        }*/

                        /*  Intent webviewintent = new Intent(Intent.ACTION_VIEW);
                        webviewintent.setData(Uri.parse(profileurl));
                        webviewintent.putExtra("other", profileurl);
                        webviewintent.putExtra(Constants.KEY, "LinkedIn");
                        mActivity.startActivity(webviewintent);*/
                    }
                } else if (scanResult.getSocial_media_id().equals("3")) {
                    try {
                        mActivity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("twitter://user?screen_name=" + strUsername)));
                    } catch (Exception e) {
                        //mActivity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://twitter.com/#!/" + strUsername)));
                        String profile_url = "http://www.twitter.com/" + strUsername;
                        Intent webviewintent = new Intent(mActivity, QuickKonnect_Webview.class);
                        webviewintent.putExtra("other", profile_url);
                        webviewintent.putExtra(Constants.KEY, "Twitter");
                        webviewintent.putExtra("VISITED_USER_ID", visited_user_id + "");
                        webviewintent.putExtra("PROFILE_ID", visited_profile_id + "");
                        webviewintent.putExtra("TYPE", "S");
                        mActivity.startActivity(webviewintent);
                    }


                } else if (scanResult.getSocial_media_id().equals("4")) {
                    String idStr = "";
                    try {
                        idStr = profileurl.substring(profileurl.lastIndexOf('/') + 1);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    String profile_url = "http://www.instagram.com/" + idStr;

                    Uri uri = Uri.parse("http://instagram.com/" + idStr + "");
                    Intent likeIng = new Intent(Intent.ACTION_VIEW, uri);
                    likeIng.setPackage("com.instagram.android");
                    try {
                        mActivity.startActivity(likeIng);
                    } catch (Exception e) {
                        Intent webviewintent = new Intent(mActivity, QuickKonnect_Webview.class);
                        webviewintent.putExtra("other", profile_url + "");
                        webviewintent.putExtra(Constants.KEY, "Instagram");
                        webviewintent.putExtra("VISITED_USER_ID", visited_user_id + "");
                        webviewintent.putExtra("PROFILE_ID", visited_profile_id + "");
                        webviewintent.putExtra("TYPE", "S");
                        mActivity.startActivity(webviewintent);
                    }

                } else if (scanResult.getSocial_media_id().equals("6")) {
                    //"https://snapchat.com/add/username"
                    //"com.snapchat.android"
                    try {
                        boolean isAppInstalled = Utilities.appInstalledOrNot(mActivity, SNAPCHAT_PACKAGE_NAME);
                        if (isAppInstalled) {
                            String idStr = profileurl.substring(profileurl.lastIndexOf('/') + 1);
                            Intent nativeAppIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://snapchat.com/add/" + idStr));
                            mActivity.startActivity(nativeAppIntent);
                        } else {
                            String profile_url = scanResult.getPublicProfileUrl() + "";
                            Intent webviewintent = new Intent(mActivity, QuickKonnect_Webview.class);
                            webviewintent.putExtra("other", profile_url);
                            webviewintent.putExtra(Constants.KEY, "Snapchat");
                            webviewintent.putExtra("VISITED_USER_ID", visited_user_id + "");
                            webviewintent.putExtra("PROFILE_ID", visited_profile_id + "");
                            webviewintent.putExtra("TYPE", "S");
                            mActivity.startActivity(webviewintent);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        String profile_url = scanResult.getPublicProfileUrl() + "";
                        Intent webviewintent = new Intent(mActivity, QuickKonnect_Webview.class);
                        webviewintent.putExtra("other", profile_url);
                        webviewintent.putExtra(Constants.KEY, "Snapchat");
                        webviewintent.putExtra("VISITED_USER_ID", visited_user_id + "");
                        webviewintent.putExtra("PROFILE_ID", visited_profile_id + "");
                        webviewintent.putExtra("TYPE", "S");
                        mActivity.startActivity(webviewintent);
                    }
                } else if (scanResult.getSocial_media_id().equals("5")) {
                    try {
                        String profile_url = "https://plus.google.com/" + appid + "";
                        final Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(profile_url + ""));
                        intent.setPackage("com.google.android.apps.plus");
                        if (intent.resolveActivity(mActivity.getPackageManager()) != null) {
                            mActivity.startActivity(intent);
                        } else {
                            Intent webviewintent = new Intent(mActivity, QuickKonnect_Webview.class);
                            webviewintent.putExtra("other", profile_url);
                            webviewintent.putExtra(Constants.KEY, "GooglePlus");
                            webviewintent.putExtra("VISITED_USER_ID", visited_user_id + "");
                            webviewintent.putExtra("PROFILE_ID", visited_profile_id + "");
                            webviewintent.putExtra("TYPE", "S");
                            mActivity.startActivity(webviewintent);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });


    }

    private void OpenFacebookPage(Activity activity, String name) {

        String facebookPageID = name;
        facebookPageID = facebookPageID.replace(" ", "");
        String facebookUrl = "https://www.facebook.com/" + facebookPageID;
        String facebookUrlScheme = "fb://profile/" + facebookPageID;
        try {
            int versionCode = activity.getPackageManager().getPackageInfo("com.facebook.katana", 0).versionCode;
            if (versionCode >= 3002850) {
                Uri uri = Uri.parse("fb://facewebmodal/f?href=" + facebookUrl);
                activity.startActivity(new Intent(Intent.ACTION_VIEW, uri));
            } else {
                activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(facebookUrlScheme)));
            }
        } catch (Exception e) {
            Intent intent = new Intent(mActivity, QuickKonnect_Webview.class);
            intent.putExtra("other", facebookUrl + "");
            intent.putExtra(Constants.KEY, "Facebook");
            intent.putExtra("VISITED_USER_ID", visited_user_id + "");
            intent.putExtra("PROFILE_ID", visited_profile_id + "");
            intent.putExtra("TYPE", "S");
            mActivity.startActivity(intent);
            //activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(facebookUrl)));
        }
    }

    @Override
    public int getItemCount() {
        return socialScanResults.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        ImageView img_socialmedia_logo;
        FrameLayout frag_img_social;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            img_socialmedia_logo = itemLayoutView.findViewById(R.id.img_socialmedia_logo);
            frag_img_social = itemLayoutView.findViewById(R.id.frag_img_social);
        }
    }
}