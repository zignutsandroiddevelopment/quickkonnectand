package com.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.interfaces.OnContactClick;
import com.models.contacts.Datum;
import com.quickkonnect.CircleTransform;
import com.quickkonnect.R;
import com.squareup.picasso.Picasso;
import com.utilities.Constants;
import com.utilities.Utilities;

import java.util.ArrayList;

/**
 * Created by ZTLAB03 on 02-05-2018.
 */

public class NewContactListAdapter extends RecyclerView.Adapter<NewContactListAdapter.ViewHolder> implements View.OnClickListener {

    public ArrayList<Datum> contactsLists;
    public ArrayList<Datum> contactsLists_Selected;
    private OnContactClick onContactClick;
    private Context context;
    private Animation fabOpenAnimation;
    private boolean isfromsearch = false;

    public NewContactListAdapter(Activity activity, ArrayList<Datum> list, ArrayList<Datum> contactsLists_Selected, boolean isfromSearch, OnContactClick onContactClick) {
        context = activity;
        this.contactsLists = list;
        this.isfromsearch = isfromSearch;
        this.contactsLists_Selected = contactsLists_Selected;
        this.onContactClick = onContactClick;
        try {
            fabOpenAnimation = AnimationUtils.loadAnimation(context, R.anim.fab_open);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public NewContactListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_contacts_list_adapter, null);
        NewContactListAdapter.ViewHolder viewHolder = new NewContactListAdapter.ViewHolder(itemLayoutView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        Datum contactsList = contactsLists.get(position);

//        StringBuilder builder = new StringBuilder();
//        if (Utilities.isEmpty(contactsList.getFirstname()) && Utilities.isEmpty(contactsList.getLastname())) {
//            String cap = contactsList.getFirstname().substring(0, 1).toUpperCase() + contactsList.getFirstname().substring(1);
//            String cap2 = contactsList.getLastname().substring(0, 1).toUpperCase() + contactsList.getLastname().substring(1);
//            builder.append(cap + " ");
//            builder.append(cap2 + "");
//
//            holder.tv_name.setText(cap + " " + cap2);
//        } else if (Utilities.isEmpty(contactsList.getFirstname())) {
//            String cap = contactsList.getFirstname().substring(0, 1).toUpperCase() + contactsList.getFirstname().substring(1);
//            builder.append(cap + " ");
//            holder.tv_name.setText(cap);
//        } else {
//            holder.tv_name.setText("");
//        }

        /*if (Utilities.isEmpty(contactsList.getFirstname())) {
            holder.tv_name.setText(contactsList.getScanUserName());
        }*/
        if (Utilities.isEmpty(contactsList.getFirstname())) {
            if (contactsList.getIs_employer() != null && !contactsList.getIs_employer().equalsIgnoreCase("0")) {
                String sourceString = "<font color='#000000'>" + contactsList.getScanUserName()/* + " </font> <font color='#d1cfcf'> (Employee)</font>"*/;
                holder.tv_name.setText(Html.fromHtml(sourceString + ""));
            } else {
                holder.tv_name.setText(contactsList.getScanUserName());
            }
        }

        if (Utilities.isEmpty(contactsList.getScanDate())) {
            try {
                String scanDate = contactsList.getScanDate();
                String strCurrentDate = Utilities.changeDateFormat("yyyy-MM-dd hh:mm:ss", "dd MMM yyyy", scanDate);
                String scanat = "<font color='#000000'>Date : </font>";
                String sourceString = "<b>" + scanat + "</b> <font color='#d1cfcf'>" + strCurrentDate + "</font>";
                holder.tv_date.setText(Html.fromHtml(sourceString));
            } catch (Exception e) {
                e.printStackTrace();
               /* try {
                    //Thu, 28 Jun 2018 04:38
                    String scanDate = contactsList.getScanDate();
                    String strCurrentDate = Utilities.changeDateFormat("EEE, dd MMM yyyy hh:mm", "dd MMM yyyy", scanDate);
                    String scanat = "<font color='#000000'>Date : </font>";
                    String sourceString = "<b>" + scanat + "</b> <font color='#777777'>" + strCurrentDate + "</font>";
                    holder.tv_date.setText(Html.fromHtml(sourceString));
                } catch (Exception e1) {
                    e1.printStackTrace();
                }*/
            }


        } else {
            holder.tv_date.setVisibility(View.GONE);
        }

        holder.tv_Scan_by.setVisibility(View.GONE);
        holder.img_next_arrow.setVisibility(View.GONE);

       /* if (contactsList.getScanned_by() != null && !contactsList.getScanned_by().isEmpty() && !contactsList.getScanned_by().equals("null")) {

            holder.tv_Scan_by.setVisibility(View.VISIBLE);
            String str = contactsList.getScanned_by();
            String scanat = "<font color='#000000'>Scan by : </font>";
            String sourceString = "<b>" + scanat + "</b> <font color='#777777'>" + str+"</font>";
            holder.tv_Scan_by.setText(Html.fromHtml(sourceString));

        } else {
            holder.tv_Scan_by.setVisibility(View.GONE);
        }*/

        if (Utilities.isEmpty(contactsList.getScanUserProfileUrl())) {
            Picasso.with(context)
                    .load(contactsList.getScanUserProfileUrl())
                    .error(R.drawable.ic_default_profile_photo)
                    .placeholder(R.drawable.ic_default_profile_photo)
                    .transform(new CircleTransform())
                    .fit()
                    .into(holder.img_profile_pic);
        } else {
            holder.img_profile_pic.setImageResource(R.drawable.ic_default_profile_photo);
        }

//        Typeface semi_bold = Typeface.createFromAsset(context.getAssets(), "fonts/latosemibold.ttf");

        if (contactsLists_Selected.contains(contactsList)) {
            holder.lin_main_contact.setBackgroundColor(Color.parseColor("#DEFEFF"));
            holder.img_profile_pic_selected.setVisibility(View.VISIBLE);
        } else {
            holder.img_profile_pic_selected.setVisibility(View.GONE);
            holder.lin_main_contact.setBackgroundColor(Color.parseColor("#FFFFFF"));
        }

//        holder.tv_name.setTypeface(semi_bold);

//        holder.tv_name.setTag(contactsList);
//        holder.img_next_arrow.setTag(contactsList);
//        holder.tv_name.setOnClickListener(this);

//        holder.img_profile_pic.setTag(contactsList);
//        holder.img_profile_pic.setOnClickListener(this);

//        holder.img_next_arrow.setOnClickListener(this);

//        holder.tv_date.setTag(contactsList);
//        holder.tv_date.setOnClickListener(this);

//        holder.tv_Scan_by.setTag(contactsList);
//        holder.tv_Scan_by.setOnClickListener(this);

//        holder.linearBackarrow.setTag(contactsList);
//        holder.linearBackarrow.setOnClickListener(this);

//        holder.frameimage.setTag(contactsList);
//        holder.frameimage.setOnClickListener(this);

        holder.linearProfilepic.setTag(contactsList);
        holder.linearProfilepic.setOnClickListener(this);

        holder.linearOther.setTag(contactsList);
        holder.linearOther.setOnClickListener(this);
    }

    @Override
    public int getItemCount() {
        return contactsLists.size();
    }

    @Override
    public void onClick(View view) {

        Datum contact = (Datum) view.getTag();

        switch (view.getId()) {
            case R.id.linearProfilepic:
                if (isfromsearch)
                    onContactClick.getContact(contact, Constants.TYPE_SEARCH_CONTACT);
                else
                    onContactClick.getContact(contact, Constants.TYPE_CLICK_ON_PROFILE_PIC);
                break;
//            case R.id.tv_name:
//            case R.id.img_next_arrow:
//            case R.id.tv_Scan_by:
//            case R.id.linearBackarrow:
//            case R.id.tv_date:
            case R.id.linearOther:
                if (isfromsearch)
                    onContactClick.getContact(contact, Constants.TYPE_SEARCH_CONTACT);
                else
                    onContactClick.getContact(contact, Constants.TYPE_CLICK_ON_OTHER);
                break;
        }
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_name, tv_date, tv_Scan_by;
        ImageView img_profile_pic, img_next_arrow, img_profile_pic_selected;
        LinearLayout lin_main_contact, linearBackarrow;
        FrameLayout frameimage;
        LinearLayout linearProfilepic, linearOther;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            tv_Scan_by = itemLayoutView.findViewById(R.id.tv_Scan_by);
            tv_name = itemLayoutView.findViewById(R.id.tv_name);
            tv_date = itemLayoutView.findViewById(R.id.tv_date);
            img_profile_pic = itemLayoutView.findViewById(R.id.img_profile_pic);
            img_next_arrow = itemLayoutView.findViewById(R.id.img_next_arrow);
            img_profile_pic_selected = itemLayoutView.findViewById(R.id.img_profile_pic_selected);
            lin_main_contact = itemLayoutView.findViewById(R.id.linear_main_contact);
            linearBackarrow = itemLayoutView.findViewById(R.id.linearBackarrow);
            frameimage = itemLayoutView.findViewById(R.id.frameimage);
            linearProfilepic = itemLayoutView.findViewById(R.id.linearProfilepic);
            linearOther = itemLayoutView.findViewById(R.id.linearOther);
        }
    }

    public void filterList(ArrayList<Datum> filterdNames) {
        this.contactsLists = filterdNames;
        notifyDataSetChanged();
    }

    public ArrayList<Datum> getCurrentList() {
        return this.contactsLists;
    }
}