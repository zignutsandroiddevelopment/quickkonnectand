package com.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.interfaces.ImageSelected;
import com.quickkonnect.R;
import com.quickkonnect.UpdateQkTagActivity;

import java.util.ArrayList;

/**
 * Created by ZTLAB-12 on 13-05-2018.
 */

public class BackgroundAdapter extends RecyclerView.Adapter<BackgroundAdapter.ViewHolder> {

    private ArrayList<UpdateQkTagActivity.BgList> bgLists;
    private Context context;
    public int Selected_pos = 0;
    ImageSelected imageSelected;

    public BackgroundAdapter(Activity activity, ArrayList<UpdateQkTagActivity.BgList> list, ImageSelected imageSelected, int selected_pos) {
        context = activity;
        this.bgLists = list;
        this.Selected_pos = selected_pos;
        this.imageSelected = imageSelected;
    }

    @Override
    public BackgroundAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.background_selection, null);

        BackgroundAdapter.ViewHolder viewHolder = new BackgroundAdapter.ViewHolder(itemLayoutView, bgLists, imageSelected);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(BackgroundAdapter.ViewHolder holder, int position) {
        try {
            UpdateQkTagActivity.BgList list = bgLists.get(position);

            /* IsImage() = 1 = Gradient
               IsImage() = 0 = Color */

            if (list.getIsImage() == 1) {
                holder.image_background_selection.setImageResource(list.getDrawable());

            } else {
                holder.image_background_selection.setImageBitmap(getCircle(list.getColorcode()));
            }

            if (Selected_pos == position) {
                holder.image_selected_background.setVisibility(View.VISIBLE);
            } else {
                holder.image_selected_background.setVisibility(View.GONE);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Bitmap getCircle(int color) {
        Bitmap bitmap = Bitmap.createBitmap(
                40, // Width
                40, // Height
                Bitmap.Config.ARGB_8888 // Config
        );

        // Initialize a new Canvas instance
        Canvas canvas = new Canvas(bitmap);

        // Draw a solid color to the canvas background
        canvas.drawColor(color);

        // Initialize a new Paint instance to draw the Circle
        Paint paint = new Paint();
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(color);
        paint.setAntiAlias(true);

        // Calculate the available radius of canvas
        int radius = Math.min(canvas.getWidth(), canvas.getHeight() / 2);

        // Set a pixels value to padding around the circle
        int padding = 5;

                /*
                    public void drawCircle (float cx, float cy, float radius, Paint paint)
                        Draw the specified circle using the specified paint. If radius is <= 0, then
                        nothing will be drawn. The circle will be filled or framed based on the
                        Style in the paint.

                    Parameters
                        cx : The x-coordinate of the center of the circle to be drawn
                        cy : The y-coordinate of the center of the circle to be drawn
                        radius : The radius of the cirle to be drawn
                        paint : The paint used to draw the circle
                */
        // Finally, draw the circle on the canvas
        canvas.drawCircle(
                canvas.getWidth() / 2, // cx
                canvas.getHeight() / 2, // cy
                radius - padding, // Radius
                paint // Paint
        );

        return bitmap;
    }

    @Override
    public int getItemCount() {
        return bgLists.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView image_background_selection, image_selected_background;
        private ArrayList<UpdateQkTagActivity.BgList> bgLists;
        ImageSelected imageSelected;

        public ViewHolder(View itemLayoutView, ArrayList<UpdateQkTagActivity.BgList> list, ImageSelected imageSelected) {
            super(itemLayoutView);

            this.bgLists = list;
            this.imageSelected = imageSelected;
            image_background_selection = itemLayoutView.findViewById(R.id.image_background_selection);
            image_selected_background = itemLayoutView.findViewById(R.id.image_selected_background);

            image_background_selection.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int pos = getAdapterPosition();
            UpdateQkTagActivity.BgList current = bgLists.get(pos);
            imageSelected.onImageSlected(pos, current);
        }
    }
}
