package com.adapter;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.activities.UpdateEducationActivity;
import com.quickkonnect.PojoClasses;
import com.quickkonnect.R;
import com.utilities.Constants;

import java.util.ArrayList;

/**
 * Created by ZTLAB03 on 16-11-2017.
 */

public class MatchProfileEducationAdapter extends RecyclerView.Adapter<MatchProfileEducationAdapter.ViewHolder> implements View.OnClickListener {

    ArrayList<PojoClasses.Education> educations;
    static Activity mActivity;

    public MatchProfileEducationAdapter(Activity activity, ArrayList<PojoClasses.Education> list) {
        this.educations = list;
        mActivity = activity;


    }

    @Override
    public MatchProfileEducationAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.match_profile_education_adapter, null);

//        mSharedPreferences = mActivity.getSharedPreferences(SharedPreference.PREF_NAME, 0);

        // create ViewHolder

        MatchProfileEducationAdapter.ViewHolder viewHolder = new MatchProfileEducationAdapter.ViewHolder(itemLayoutView);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(MatchProfileEducationAdapter.ViewHolder holder, int position) {

        PojoClasses.Education education = educations.get(position);

        if (education.getSchoolname() != null && !education.getSchoolname().isEmpty() && !education.getSchoolname().equals("null")) {

            holder.tv_school_name.setText(education.getSchoolname());
        } else {
            holder.tv_school_name.setText("");
            holder.tv_school_name.setVisibility(View.GONE);
        }

        if (education.getDegree() != null && !education.getDegree().isEmpty() && !education.getDegree().equals("null")) {

            holder.tv_degree.setText(education.getDegree());
        } else {
            holder.tv_degree.setText("");
            holder.tv_degree.setVisibility(View.GONE);
        }

        if (education.getFieldofstudy() != null && !education.getFieldofstudy().isEmpty() && !education.getFieldofstudy().equals("null")) {

            holder.tv_field_of_study.setText(education.getFieldofstudy());
        } else {
            holder.tv_field_of_study.setText("");
            holder.tv_field_of_study.setVisibility(View.GONE);
        }


        if (education.getStartdate() != null && !education.getStartdate().isEmpty() && !education.getStartdate().equals("null")) {

            holder.tv_startdate.setText(education.getStartdate() + " - " + education.getEnddate());
        } else {
            holder.tv_startdate.setText("");
            holder.tv_startdate.setVisibility(View.GONE);
        }

        if (education.getEnddate() != null && !education.getEnddate().isEmpty() && !education.getEnddate().equals("null")) {

            holder.tv_enddate.setText(education.getEnddate());

        } else {
            holder.tv_enddate.setText("");
            holder.tv_enddate.setVisibility(View.GONE);
        }

        if (education.getActivities() != null && !education.getActivities().isEmpty() && !education.getActivities().equals("null")) {

            holder.tv_activites.setText(education.getActivities());
        } else {
            holder.tv_activites.setText("");
            holder.tv_activites.setVisibility(View.GONE);
        }

        holder.img_delete.setTag(education);
        holder.img_delete.setOnClickListener(this);

        holder.tv_school_name.setTag(education);
        holder.tv_school_name.setOnClickListener(this);

        holder.tv_degree.setTag(education);
        holder.tv_degree.setOnClickListener(this);

        holder.tv_field_of_study.setTag(education);
        holder.tv_field_of_study.setOnClickListener(this);

        holder.tv_startdate.setTag(education);
        holder.tv_startdate.setOnClickListener(this);

        holder.tv_enddate.setTag(education);
        holder.tv_enddate.setOnClickListener(this);


        holder.tv_activites.setTag(education);
        holder.tv_activites.setOnClickListener(this);

        Typeface lato_regular = Typeface.createFromAsset(mActivity.getAssets(), "fonts/latoregular.ttf");
        Typeface lato_bold = Typeface.createFromAsset(mActivity.getAssets(), "fonts/latobold.ttf");
        Typeface semi_bold = Typeface.createFromAsset(mActivity.getAssets(), "fonts/latosemibold.ttf");

        holder.tv_school_name.setTypeface(semi_bold);


        holder.tv_startdate.setTypeface(lato_regular);
        holder.tv_degree.setTypeface(lato_regular);
        holder.tv_field_of_study.setTypeface(lato_regular);

    }

    @Override
    public int getItemCount() {
        return educations.size();
    }

    @Override
    public void onClick(View view) {

        PojoClasses.Education eduList = (PojoClasses.Education) view.getTag();

        switch (view.getId()) {
            case R.id.img_delete:
                //((UserProfileActivity)mActivity).DeleteEducation(view, eduList.getEducation_id());
                Intent intent = new Intent(mActivity, UpdateEducationActivity.class);
                intent.putExtra("education_id", eduList.getEducation_id());
                intent.putExtra("school_name", eduList.getSchoolname());
                intent.putExtra("degree", eduList.getDegree());
                intent.putExtra("field_of_study", eduList.getFieldofstudy());
                intent.putExtra("activities", eduList.getActivities());
                intent.putExtra("start_date", eduList.getStartdate());
                intent.putExtra("end_date", eduList.getEnddate());
                intent.putExtra(Constants.KEY_UPDATE, Constants.EDIT_DATA);
                //mActivity.startActivity(intent);
                break;

            case R.id.tv_school_name:
            case R.id.tv_degree:
            case R.id.tv_field_of_study:
            case R.id.tv_startdate:
            case R.id.tv_enddate:
            case R.id.tv_activites:

                Log.d("field of", eduList.getFieldofstudy());

                intent = new Intent(mActivity, UpdateEducationActivity.class);
                intent.putExtra("education_id", eduList.getEducation_id());
                intent.putExtra("school_name", eduList.getSchoolname());
                intent.putExtra("degree", eduList.getDegree());
                intent.putExtra("field_of_study", eduList.getFieldofstudy());
                intent.putExtra("activities", eduList.getActivities());
                intent.putExtra("start_date", eduList.getStartdate());
                intent.putExtra("end_date", eduList.getEnddate());
                intent.putExtra(Constants.KEY_UPDATE, Constants.EDIT_DATA);

                //mActivity.startActivity(intent);

                break;
        }


    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_school_name, tv_degree, tv_field_of_study, tv_startdate, tv_enddate, tv_activites;
        ImageView img_delete;


        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            tv_school_name = itemLayoutView.findViewById(R.id.tv_school_name);
            tv_degree = itemLayoutView.findViewById(R.id.tv_degree);
            tv_field_of_study = itemLayoutView.findViewById(R.id.tv_field_of_study);
            tv_startdate = itemLayoutView.findViewById(R.id.tv_startdate);
            tv_enddate = itemLayoutView.findViewById(R.id.tv_enddate);
            tv_activites = itemLayoutView.findViewById(R.id.tv_activites);
            img_delete = itemLayoutView.findViewById(R.id.img_delete);


        }
    }

}
