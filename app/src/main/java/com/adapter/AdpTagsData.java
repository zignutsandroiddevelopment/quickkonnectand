package com.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;

import com.interfaces.OnTagsSelected;
import com.models.tagsdata.ModelTagDetail;
import com.quickkonnect.R;
import com.utilities.Utilities;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ZTLAB-10 on 21-02-2018.
 */

public class AdpTagsData extends ArrayAdapter<ModelTagDetail> implements Filterable {

    private Context context;
    private List<ModelTagDetail> tagslist;
    private List<ModelTagDetail> tagslistOrigi;
    private SearchTagsFilter searchCountryFilter;
    private OnTagsSelected onTagsSelected;

    public AdpTagsData(Context context, List<ModelTagDetail> tagslist, OnTagsSelected onTagsSelected) {
        super(context, R.layout.spinner_textview, tagslist);
        this.context = context;
        this.tagslist = tagslist;
        this.tagslistOrigi = tagslist;
        this.onTagsSelected = onTagsSelected;
        searchCountryFilter = new SearchTagsFilter();
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        CountryAdapter.ViewHolder viewHolder = null;

        if (view == null) {

            view = LayoutInflater.from(context).inflate(R.layout.spinner_textview, null);

            viewHolder = new CountryAdapter.ViewHolder(view);

            view.setTag(viewHolder);

        } else {
            viewHolder = (CountryAdapter.ViewHolder) view.getTag();
        }

        final ModelTagDetail contactsList = tagslist.get(i);
        if (Utilities.isEmpty(contactsList.getName())) {
            viewHolder.tv_name.setText(contactsList.getName() + "");
        } else {
            viewHolder.tv_name.setText("");
        }


//        view.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                onTagsSelected.onTagsSelected(contactsList);
//            }
//        });

        return view;
    }

    @Override
    public Filter getFilter() {
        return searchCountryFilter;
    }

    public class SearchTagsFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();

            // If the constraint (search string/pattern) is null
            // or its length is 0, i.e., its empty then
            // we just set the `values` property to the
            // original contacts list which contains all of them
            if (constraint == null || constraint.length() == 0) {
                results.values = tagslistOrigi;
                results.count = tagslistOrigi.size();
            } else {
                // Some search copnstraint has been passed
                // so let's filter accordingly
                List<ModelTagDetail> filteredContacts = new ArrayList<>();

                // We'll go through all the contacts and see
                // if they contain the supplied string
                for (ModelTagDetail c : tagslistOrigi) {
                    if (c.getName() != null && c.getName().toUpperCase().contains(constraint.toString().toUpperCase())) {
                        // if `contains` == true then add it
                        // to our filtered list
                        filteredContacts.add(c);
                    }
                }

                // Finally set the filtered values and size/count
                results.values = filteredContacts;
                results.count = filteredContacts.size();
            }

            // Return our FilterResults object
            return results;
        }

        @Override
        protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
            tagslist = (List<ModelTagDetail>) filterResults.values;
            notifyDataSetChanged();
        }
    }
}
