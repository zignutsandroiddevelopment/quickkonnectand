package com.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.interfaces.OnContactClick;
import com.quickkonnect.CircleTransform;
import com.quickkonnect.PojoClasses;
import com.quickkonnect.R;
import com.squareup.picasso.Picasso;
import com.utilities.Constants;
import com.utilities.Utilities;

import java.util.ArrayList;

/**
 * Created by ZTLAB03 on 02-05-2018.
 */

public class FollowerContactAdapter extends RecyclerView.Adapter<FollowerContactAdapter.ViewHolder> implements View.OnClickListener {

    public ArrayList<PojoClasses.FollowersList_Contact> followerlist;
    public ArrayList<PojoClasses.FollowersList_Contact> followerlist_Selected;
    private OnContactClick onContactClick;
    private Context context;

    public FollowerContactAdapter(Activity activity, ArrayList<PojoClasses.FollowersList_Contact> list, ArrayList<PojoClasses.FollowersList_Contact> followerlist_Selected, OnContactClick onContactClick) {
        context = activity;
        this.followerlist = list;
        this.followerlist_Selected = followerlist_Selected;
        this.onContactClick = onContactClick;
    }

    @Override
    public FollowerContactAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_contacts_list_adapter, parent, false);
        FollowerContactAdapter.ViewHolder viewHolder = new FollowerContactAdapter.ViewHolder(itemLayoutView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(FollowerContactAdapter.ViewHolder holder, int position) {

        PojoClasses.FollowersList_Contact contactsList = followerlist.get(position);
        if (Utilities.isEmpty(contactsList.getName()) && Utilities.isEmpty(contactsList.getName())) {
            holder.tv_name.setText(contactsList.getName() + "");
        } else {
            holder.tv_name.setText("");
        }
        // date as opposite profile name
        if (contactsList.getProfile_name() != null && !contactsList.getProfile_name().isEmpty() && !contactsList.getProfile_name().equals("null")) {
            String strCurrentDate = contactsList.getProfile_name();
            String sourceString = "<font color='#2ABBBE'>" + strCurrentDate + "</font>";
            holder.tv_date.setText(Html.fromHtml(sourceString));
        } else {
            holder.tv_date.setVisibility(View.GONE);
        }
        holder.tv_Scan_by.setVisibility(View.GONE);

        if (contactsList.getProfile_pic() != null && !contactsList.getProfile_pic().isEmpty() && !contactsList.getProfile_pic().equals("null")) {
            Picasso.with(context)
                    .load(contactsList.getProfile_pic())
                    .error(R.drawable.ic_default_profile_photo)
                    .placeholder(R.drawable.ic_default_profile_photo)
                    .transform(new CircleTransform())
                    .fit()
                    .into(holder.img_profile_pic);
        } else {
            holder.img_profile_pic.setImageResource(R.drawable.ic_default_profile_photo);
        }
        holder.img_next_arrow.setVisibility(View.GONE);
        Typeface semi_bold = Typeface.createFromAsset(context.getAssets(), "fonts/latosemibold.ttf");

        if (followerlist_Selected.contains(contactsList)) {
            holder.img_profile_pic_selected.setVisibility(View.VISIBLE);
            holder.lin_main_contact.setBackgroundColor(Color.parseColor("#DEFEFF"));
        } else {
            holder.img_profile_pic_selected.setVisibility(View.GONE);
            holder.lin_main_contact.setBackgroundColor(Color.parseColor("#FFFFFF"));
        }

        holder.tv_Scan_by.setText("");

        holder.tv_name.setTypeface(semi_bold);

        holder.tv_name.setTag(contactsList);
        holder.img_next_arrow.setTag(contactsList);
        holder.tv_name.setOnClickListener(this);

        holder.img_profile_pic.setTag(contactsList);
        holder.img_profile_pic.setOnClickListener(this);
        holder.img_next_arrow.setOnClickListener(this);

        holder.tv_date.setTag(contactsList);
        holder.tv_date.setOnClickListener(this);

        holder.tv_Scan_by.setTag(contactsList);
        holder.tv_Scan_by.setOnClickListener(this);

        holder.linearBackarrow.setTag(contactsList);
        holder.linearBackarrow.setOnClickListener(this);

        holder.frameimage.setTag(contactsList);
        holder.frameimage.setOnClickListener(this);

        if (position == followerlist.size() - 1)
            onContactClick.onLoadMore(Constants.CONTACTS_TYPE_FOLLOWERS);
    }

    @Override
    public int getItemCount() {
        return followerlist.size();
    }

    @Override
    public void onClick(View view) {

        PojoClasses.FollowersList_Contact contact = (PojoClasses.FollowersList_Contact) view.getTag();

        switch (view.getId()) {
            case R.id.tv_name:
            case R.id.img_profile_pic:
            case R.id.img_next_arrow:
            case R.id.tv_Scan_by:
            case R.id.linearBackarrow:
            case R.id.frameimage:
            case R.id.tv_date:
                onContactClick.getFollower(contact);
                break;
        }
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_name, tv_date, tv_Scan_by;
        ImageView img_profile_pic, img_next_arrow, img_profile_pic_selected;
        LinearLayout lin_main_contact, linearBackarrow;
        FrameLayout frameimage;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            tv_Scan_by = itemLayoutView.findViewById(R.id.tv_Scan_by);
            tv_name = itemLayoutView.findViewById(R.id.tv_name);
            tv_date = itemLayoutView.findViewById(R.id.tv_date);
            img_profile_pic = itemLayoutView.findViewById(R.id.img_profile_pic);
            img_next_arrow = itemLayoutView.findViewById(R.id.img_next_arrow);
            img_profile_pic_selected = itemLayoutView.findViewById(R.id.img_profile_pic_selected);
            lin_main_contact = itemLayoutView.findViewById(R.id.linear_main_contact);
            linearBackarrow = itemLayoutView.findViewById(R.id.linearBackarrow);
            frameimage = itemLayoutView.findViewById(R.id.frameimage);
        }
    }

    public void filterList(ArrayList<PojoClasses.FollowersList_Contact> filterdNames) {
        this.followerlist = filterdNames;
        notifyDataSetChanged();
    }

    public ArrayList<PojoClasses.FollowersList_Contact> getCurrentList() {
        return this.followerlist;
    }
}
