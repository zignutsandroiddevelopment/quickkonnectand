package com.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.interfaces.OnEditFounder;
import com.interfaces.OnSingleEdit;
import com.models.createprofile.Founder;
import com.quickkonnect.R;
import com.utilities.Utilities;

import java.util.List;

/**
 * Created by ZTLAB-10 on 21-02-2018.
 */

public class AdpFounders extends RecyclerView.Adapter<AdpFounders.ViewHolder> {

    private Context context;
    private List<Founder> socialMediumList;
    private OnEditFounder getEditSingle;
    private boolean isEditenable = false;

    public AdpFounders(Context context, List<Founder> socialMediumList, OnEditFounder getEditSingle, boolean isEditenable) {
        this.context = context;
        this.getEditSingle = getEditSingle;
        this.isEditenable = isEditenable;
        this.socialMediumList = socialMediumList;
    }

    @Override
    public AdpFounders.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_founders, parent, false);
        AdpFounders.ViewHolder viewHolder = new AdpFounders.ViewHolder(itemLayoutView);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(AdpFounders.ViewHolder holder, final int position) {

        final Founder socialMedium = socialMediumList.get(position);
        if (Utilities.isEmpty(socialMedium.getFirstname()) && Utilities.isEmpty(socialMedium.getLastname())) {
//            String html = "<html><font color='#616161'>Name:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</font><font color='#bcbcbc'> " + socialMedium.getFirstname()
//                    + " " + socialMedium.getLastname() + "</font></html>";
            holder.tv_name.setText(socialMedium.getFirstname() + " " + socialMedium.getLastname());
        } else if (Utilities.isEmpty(socialMedium.getFirstname())) {
            //String html = "<html><font color='#616161'>Name:&nbsp;</font><font color='#bcbcbc'> " + socialMedium.getFirstname() + "</font></html>";
            holder.tv_name.setText(socialMedium.getFirstname());
        } else holder.tv_name.setVisibility(View.GONE);

        if (Utilities.isEmpty(socialMedium.getEmail())) {
            String html = "<html><font color='#616161'>Email:&nbsp;</font><font color='#bcbcbc'> " + socialMedium.getEmail() + "</font></html>";
            holder.tv_email.setVisibility(View.VISIBLE);
            holder.tv_email.setText(Html.fromHtml(html));
        } else {
            holder.tv_email.setVisibility(View.GONE);
        }
        if (Utilities.isEmpty(socialMedium.getDesignation())) {
            String html2 = "<html><font color='#616161'>Designation:&nbsp;</font><font color='#bcbcbc'> " + socialMedium.getDesignation() + "</font></html>";
            holder.tv_designation.setText(Html.fromHtml(html2));
            holder.tv_designation.setVisibility(View.VISIBLE);
        } else {
            holder.tv_designation.setVisibility(View.GONE);
        }

        if (isEditenable)
            holder.imgDelete.setVisibility(View.VISIBLE);
        else
            holder.imgDelete.setVisibility(View.GONE);

        holder.imgDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isEditenable)
                    getEditSingle.getEditFounder(position, socialMedium, OnSingleEdit.DELETE);
            }
        });

        holder.llData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isEditenable)
                    getEditSingle.getEditFounder(position, socialMedium, OnSingleEdit.EDIT);
            }
        });
    }

    @Override
    public int getItemCount() {
        return socialMediumList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView tv_designation, tv_name, tv_email;
        private LinearLayout imgDelete, llData;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            tv_email = itemLayoutView.findViewById(R.id.tv_designation_email);
            tv_name = itemLayoutView.findViewById(R.id.tv_name);
            tv_designation = itemLayoutView.findViewById(R.id.tv_designation);
            imgDelete = itemLayoutView.findViewById(R.id.imgDelete);
            llData = itemLayoutView.findViewById(R.id.llData);
        }
    }
}
