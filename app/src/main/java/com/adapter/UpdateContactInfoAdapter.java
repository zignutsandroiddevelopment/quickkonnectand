package com.adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.activities.UpdateContactInfoActivity;
import com.models.logindata.ContactDetail;
import com.quickkonnect.R;
import com.utilities.Constants;
import com.utilities.Utilities;

import java.util.List;

/**
 * Created by Zignuts Technolab pvt. ltd on 30-11-2017.
 */

public class UpdateContactInfoAdapter extends RecyclerView.Adapter<UpdateContactInfoAdapter.ViewHolder> implements View.OnClickListener {

    List<ContactDetail> contactInfos;
    static Activity mActivity;

    public UpdateContactInfoAdapter(Activity activity, List<ContactDetail> list) {
        this.contactInfos = list;
        mActivity = activity;
    }

    @Override
    public UpdateContactInfoAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_contact_info_view, null);
        UpdateContactInfoAdapter.ViewHolder viewHolder = new UpdateContactInfoAdapter.ViewHolder(itemLayoutView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(UpdateContactInfoAdapter.ViewHolder holder, int position) {

        ContactDetail contactInfo = contactInfos.get(position);

        if (contactInfo.getEmail() != null && Utilities.isEmpty(contactInfo.getEmail()))
            holder.tv_contact_email.setText(contactInfo.getEmail());

        else holder.tv_contact_email.setVisibility(View.GONE);

        if (contactInfo.getPhone() != null && Utilities.isEmpty("" + contactInfo.getPhone())) {

            if (contactInfo.getContactType() != null && Utilities.isEmpty(contactInfo.getContactType()))
                holder.tv_contact_phone.setText(String.valueOf(contactInfo.getPhone())
                        + "(" + String.valueOf(contactInfo.getContactType()) + ")");

            else holder.tv_contact_phone.setText(String.valueOf(contactInfo.getPhone()));

        } else {

            if (contactInfo.getContactType() != null && Utilities.isEmpty(contactInfo.getContactType()))
                holder.tv_contact_phone.setText(String.valueOf(contactInfo.getContactType()));

            else holder.tv_contact_phone.setVisibility(View.GONE);
        }

        holder.img_delete.setTag(contactInfo);
        holder.tv_contact_phone.setTag(contactInfo);
        holder.tv_contact_email.setTag(contactInfo);

        holder.img_delete.setOnClickListener(this);
        holder.tv_contact_phone.setOnClickListener(this);
        holder.tv_contact_email.setOnClickListener(this);
    }

    @Override
    public int getItemCount() {
        return contactInfos.size();
    }

    @Override
    public void onClick(View view) {

        ContactDetail contactInfo = (ContactDetail) view.getTag();

        switch (view.getId()) {
            case R.id.img_delete:
                Intent intent = new Intent(mActivity, UpdateContactInfoActivity.class);
                intent.putExtra("contactInfo", contactInfo);
                intent.putExtra(Constants.KEY_UPDATE, Constants.EDIT_DATA);
                mActivity.startActivityForResult(intent, Constants.RCODE_UPDATEEDUCATION);
                break;

            case R.id.tv_contact_phone:
            case R.id.tv_contact_email:
                intent = new Intent(mActivity, UpdateContactInfoActivity.class);
                intent.putExtra("contactInfo", contactInfo);
                intent.putExtra(Constants.KEY_UPDATE, Constants.EDIT_DATA);
                mActivity.startActivityForResult(intent, Constants.RCODE_UPDATEEDUCATION);
                break;
        }
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_contact_email, tv_contact_phone;
        ImageView img_delete;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            tv_contact_email = itemLayoutView.findViewById(R.id.tv_contact_email);
            tv_contact_phone = itemLayoutView.findViewById(R.id.tv_contact_phone);
            img_delete = itemLayoutView.findViewById(R.id.img_delete);
        }
    }
}
