package com.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.interfaces.BlockedProfile;
import com.interfaces.OnContactClick;
import com.quickkonnect.CircleTransform;
import com.quickkonnect.PojoClasses;
import com.quickkonnect.R;
import com.squareup.picasso.Picasso;
import com.utilities.Utilities;

import java.util.ArrayList;

/**
 * Created by ZTLAB03 on 13-04-2018.
 */

public class BlockProfileListAdapter extends RecyclerView.Adapter<BlockProfileListAdapter.ViewHolder> {

    private ArrayList<PojoClasses.EmployeesList> emplist;
    private BlockedProfile blockedProfile;
    private Context context;

    public BlockProfileListAdapter(Activity activity, ArrayList<PojoClasses.EmployeesList> list, BlockedProfile blockedProfile) {
        context = activity;
        this.emplist = list;
        this.blockedProfile = blockedProfile;
    }

    @Override
    public BlockProfileListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_contacts_list_adapter, null);

        BlockProfileListAdapter.ViewHolder viewHolder = new BlockProfileListAdapter.ViewHolder(itemLayoutView,blockedProfile,emplist);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(BlockProfileListAdapter.ViewHolder holder, int position) {
        try {
            PojoClasses.EmployeesList contactsList = emplist.get(position);
            PojoClasses.EmployeesList.Profile profilelist = new Gson().fromJson(contactsList.getProfile1(),PojoClasses.EmployeesList.Profile.class);

            StringBuilder builder = new StringBuilder();
            if (Utilities.isEmpty(profilelist.getName())) {
                //String cap = profilelist.getName().substring(0, 1).toUpperCase() + profilelist.getName().substring(1);
                String cap = profilelist.getName();
                builder.append(cap + " ");
                holder.tv_name.setText(cap);
            } else if (Utilities.isEmpty(profilelist.getName())) {
                //String cap = profilelist.getName().substring(0, 1).toUpperCase();
                String cap = profilelist.getName();
                builder.append(cap + " ");
                holder.tv_name.setText(cap);
            } else {
                holder.tv_name.setText("");
            }

            if (profilelist.getLogo() != null && !profilelist.getLogo().isEmpty() && !profilelist.getLogo().equals("null")) {
                Picasso.with(context)
                        .load(profilelist.getLogo())
                        .error(R.drawable.ic_default_profile_photo)
                        .placeholder(R.drawable.ic_default_profile_photo)
                        .transform(new CircleTransform())
                        .fit()
                        .into(holder.img_profile_pic);
            } else {
                holder.img_profile_pic.setImageResource(R.drawable.ic_default_profile_photo);
            }
            Typeface semi_bold = Typeface.createFromAsset(context.getAssets(), "fonts/latosemibold.ttf");
            holder.tv_name.setTypeface(semi_bold);
            holder.tv_date.setVisibility(View.GONE);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return emplist.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView tv_name, tv_date;
        ImageView img_profile_pic, img_next_arrow;
        BlockedProfile blockedProfile;
        ArrayList<PojoClasses.EmployeesList> emplist;

        public ViewHolder(View itemLayoutView,BlockedProfile blockedProfile,ArrayList<PojoClasses.EmployeesList> emplist) {
            super(itemLayoutView);
            this.blockedProfile = blockedProfile;
            this.emplist = emplist;
            tv_name = itemLayoutView.findViewById(R.id.tv_name);
            tv_date = itemLayoutView.findViewById(R.id.tv_date);
            img_profile_pic = itemLayoutView.findViewById(R.id.img_profile_pic);
            img_next_arrow = itemLayoutView.findViewById(R.id.img_next_arrow);

            tv_name.setOnClickListener(this);
            img_profile_pic.setOnClickListener(this);
            img_next_arrow.setOnClickListener(this);
            tv_date.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int pos = getAdapterPosition();
            PojoClasses.EmployeesList current = emplist.get(pos) ;
            switch (view.getId()) {
                case R.id.tv_name:
                case R.id.img_profile_pic:
                case R.id.img_next_arrow:
                case R.id.tv_date:
                    blockedProfile.onBlockedProfile(pos,current,current.getProfile1(),current.getUser1());
                    break;
            }
        }
    }
}
