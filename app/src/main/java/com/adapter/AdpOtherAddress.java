package com.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.interfaces.OnEditMedia;
import com.interfaces.getPosForAddress;
import com.models.createprofile.OtherAddresModel;
import com.quickkonnect.R;

import java.util.List;


/**
 * Created by ZTLAB03 on 15-03-2018.
 */

public class AdpOtherAddress extends RecyclerView.Adapter<AdpOtherAddress.ViewHolder> {

    private Context context;
    private List<OtherAddresModel> OaList;
    private OnEditMedia onEditMedia;
    private getPosForAddress posForAddress;
    private boolean isEditenable = false;

    public AdpOtherAddress(Context context, List<OtherAddresModel> OaList, OnEditMedia onEditMedia, boolean isEditenable, getPosForAddress posForAddress) {
        this.context = context;
        this.onEditMedia = onEditMedia;
        this.OaList = OaList;
        this.posForAddress = posForAddress;
        this.isEditenable = isEditenable;
    }

    @Override
    public AdpOtherAddress.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_founders, parent, false);
        AdpOtherAddress.ViewHolder viewHolder = new AdpOtherAddress.ViewHolder(itemLayoutView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(AdpOtherAddress.ViewHolder holder, final int position) {

        final OtherAddresModel socialMedium = OaList.get(position);

        if (socialMedium != null && socialMedium.getAddress() != null)
            holder.tv_social_media_name.setText(socialMedium.getAddress());

        holder.tv_social_media_type.setVisibility(View.GONE);
        holder.imgIcon.setVisibility(View.GONE);

        if (isEditenable)
            holder.imgDelete.setVisibility(View.VISIBLE);
        else
            holder.imgDelete.setVisibility(View.GONE);

        holder.imgDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                posForAddress.getPOsforAddress(position, "DELETE");
            }
        });
    }

    @Override
    public int getItemCount() {
        return OaList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView tv_social_media_name, tv_social_media_type, tv_openurl;
        private LinearLayout imgDelete;
        private ImageView imgIcon;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);

            tv_social_media_name = itemLayoutView.findViewById(R.id.tv_name);
            tv_social_media_type = itemLayoutView.findViewById(R.id.tv_designation);
            imgDelete = itemLayoutView.findViewById(R.id.imgDelete);
            tv_openurl = itemLayoutView.findViewById(R.id.tv_openurl);
            imgIcon = itemLayoutView.findViewById(R.id.imgIcon);
            tv_social_media_name.setVisibility(View.VISIBLE);
            tv_social_media_type.setVisibility(View.VISIBLE);
            tv_openurl.setVisibility(View.GONE);

            tv_social_media_name.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int pos = getAdapterPosition();
            if (view == tv_social_media_name) {
                posForAddress.getPOsforAddress(pos, "ADDRESS");
            }
        }
    }
}
