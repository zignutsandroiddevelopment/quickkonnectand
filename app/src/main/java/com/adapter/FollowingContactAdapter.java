package com.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.interfaces.OnContactClick;
import com.quickkonnect.CircleTransform;
import com.quickkonnect.PojoClasses;
import com.quickkonnect.R;
import com.squareup.picasso.Picasso;
import com.utilities.Constants;
import com.utilities.Utilities;

import java.util.ArrayList;

/**
 * Created by ZTLAB03 on 02-05-2018.
 */

public class FollowingContactAdapter extends RecyclerView.Adapter<FollowingContactAdapter.ViewHolder> implements View.OnClickListener {

    public ArrayList<PojoClasses.FollowingList_Contact> followinglist;
    private OnContactClick onContactClick;
    private Context context;

    public FollowingContactAdapter(Activity activity, ArrayList<PojoClasses.FollowingList_Contact> list, OnContactClick onContactClick) {
        context = activity;
        this.followinglist = list;
        this.onContactClick = onContactClick;
    }

    @Override
    public FollowingContactAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_contacts_list_adapter, null);
        FollowingContactAdapter.ViewHolder viewHolder = new FollowingContactAdapter.ViewHolder(itemLayoutView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(FollowingContactAdapter.ViewHolder holder, int position) {

        PojoClasses.FollowingList_Contact contactsList = followinglist.get(position);
        if (Utilities.isEmpty(contactsList.getName()) && Utilities.isEmpty(contactsList.getName())) {
            holder.tv_name.setText(contactsList.getName() + "");
        } else {
            holder.tv_name.setText("");
        }
        if (contactsList.getLogo() != null && !contactsList.getLogo().isEmpty() && !contactsList.getLogo().equals("null")) {
            Picasso.with(context)
                    .load(contactsList.getLogo())
                    .error(R.drawable.ic_logo_blue_qk)
                    .placeholder(R.drawable.ic_logo_blue_qk)
                    .transform(new CircleTransform())
                    .fit()
                    .into(holder.img_profile_pic);
        } else {
            holder.img_profile_pic.setImageResource(R.drawable.ic_logo_blue_qk);
        }
        holder.img_next_arrow.setVisibility(View.GONE);
        Typeface semi_bold = Typeface.createFromAsset(context.getAssets(), "fonts/latosemibold.ttf");

        holder.tv_name.setTypeface(semi_bold);

        holder.tv_name.setTag(contactsList);
        holder.img_next_arrow.setTag(contactsList);
        holder.tv_name.setOnClickListener(this);

        holder.img_profile_pic.setTag(contactsList);
        holder.img_profile_pic.setOnClickListener(this);
        holder.img_next_arrow.setOnClickListener(this);

        //holder.tv_date.setTag(contactsList);
        holder.tv_date.setVisibility(View.GONE);
        //holder.tv_date.setOnClickListener(this);

        //holder.tv_Scan_by.setTag(contactsList);
        holder.tv_Scan_by.setVisibility(View.GONE);
        //holder.tv_Scan_by.setOnClickListener(this);

        holder.linearBackarrow.setTag(contactsList);
        holder.linearBackarrow.setOnClickListener(this);

        holder.frameimage.setTag(contactsList);
        holder.frameimage.setOnClickListener(this);

//        if (position == followinglist.size() - 1)
//            onContactClick.onLoadMore(Constants.CONTACTS_TYPE_FOLLOWING);
    }

    @Override
    public int getItemCount() {
        return followinglist.size();
    }

    @Override
    public void onClick(View view) {

        PojoClasses.FollowingList_Contact contact = (PojoClasses.FollowingList_Contact) view.getTag();

        switch (view.getId()) {
            case R.id.tv_name:
            case R.id.img_profile_pic:
            case R.id.img_next_arrow:
            case R.id.tv_Scan_by:
            case R.id.linearBackarrow:
            case R.id.frameimage:
            case R.id.tv_date:
                onContactClick.getFollowing(contact);
                break;
        }
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_name, tv_date, tv_Scan_by;
        ImageView img_profile_pic, img_next_arrow, img_profile_pic_selected;
        LinearLayout lin_main_contact, linearBackarrow;
        FrameLayout frameimage;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            tv_Scan_by = itemLayoutView.findViewById(R.id.tv_Scan_by);
            tv_name = itemLayoutView.findViewById(R.id.tv_name);
            tv_date = itemLayoutView.findViewById(R.id.tv_date);
            img_profile_pic = itemLayoutView.findViewById(R.id.img_profile_pic);
            img_next_arrow = itemLayoutView.findViewById(R.id.img_next_arrow);
            img_profile_pic_selected = itemLayoutView.findViewById(R.id.img_profile_pic_selected);
            lin_main_contact = itemLayoutView.findViewById(R.id.linear_main_contact);
            linearBackarrow = itemLayoutView.findViewById(R.id.linearBackarrow);
            frameimage = itemLayoutView.findViewById(R.id.frameimage);
        }
    }

    public void filterList(ArrayList<PojoClasses.FollowingList_Contact> filterdNames) {
        this.followinglist = filterdNames;
        notifyDataSetChanged();
    }
}
