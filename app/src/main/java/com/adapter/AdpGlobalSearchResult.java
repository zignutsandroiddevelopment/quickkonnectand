package com.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.interfaces.onGlobalSearchClick;
import com.models.globalsearch.ModelGlobalSearch;
import com.quickkonnect.CircleTransform;
import com.quickkonnect.DashboardActivity;
import com.quickkonnect.R;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by ZTLAB-10 on 23-02-2018.
 */

public class AdpGlobalSearchResult extends RecyclerView.Adapter<AdpGlobalSearchResult.MyViewHolder> {

    private Context _context;
    List<ModelGlobalSearch> originalList;
    private onGlobalSearchClick searchClick;

    public AdpGlobalSearchResult(Context context, List<ModelGlobalSearch> originalList, onGlobalSearchClick searchClick) {
        this._context = context;
        this.searchClick = searchClick;
        this.originalList = originalList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_globalsearch_child, parent,false);
        MyViewHolder viewHolder = new MyViewHolder(itemLayoutView, originalList, searchClick);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder myViewHolder, int position) {

        final ModelGlobalSearch modelGlobalSearch = originalList.get(position);

        myViewHolder.tvUsername.setText(modelGlobalSearch.getUsername());

        if (modelGlobalSearch.getKey().equalsIgnoreCase(DashboardActivity.FOLLOWERS)) {

            if (modelGlobalSearch.getCompanyname() != null) {
                myViewHolder.tv_company_name.setVisibility(View.VISIBLE);
                myViewHolder.tv_company_name.setText(modelGlobalSearch.getCompanyname());

            } else myViewHolder.tv_company_name.setVisibility(View.GONE);

        } else myViewHolder.tv_company_name.setVisibility(View.GONE);

        myViewHolder.imgProfilepic.setVisibility(View.VISIBLE);

        if (modelGlobalSearch.getProfile_pic() != null && !modelGlobalSearch.getProfile_pic().isEmpty() && !modelGlobalSearch.getProfile_pic().equals("null")) {
            Picasso.with(_context)
                    .load(modelGlobalSearch.getProfile_pic())
                    .error(R.drawable.ic_default_profile_photo)
                    .placeholder(R.drawable.ic_default_profile_photo)
                    .transform(new CircleTransform())
                    .fit()
                    .into(myViewHolder.imgProfilepic);
        } else {
            myViewHolder.imgProfilepic.setImageResource(R.drawable.ic_default_profile_photo);
        }

        myViewHolder.lineaerRoot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                searchClick.onChildClick(modelGlobalSearch);
            }
        });
    }

    @Override
    public int getItemCount() {
        return originalList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView imgProfilepic;
        public TextView tvUsername, tv_company_name;
        LinearLayout lineaerRoot;
        List<ModelGlobalSearch> originalList;
        onGlobalSearchClick searchClick;

        public MyViewHolder(View itemView, List<ModelGlobalSearch> originalList, onGlobalSearchClick searchClick) {
            super(itemView);

            this.searchClick = searchClick;
            this.originalList = originalList;

            imgProfilepic = itemView.findViewById(R.id.imgProfilepic);
            tvUsername = itemView.findViewById(R.id.tvUsername);
            lineaerRoot = itemView.findViewById(R.id.lineaerRoot);
            tv_company_name = itemView.findViewById(R.id.tv_company_name);
        }
    }
}
