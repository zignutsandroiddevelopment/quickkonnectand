package com.adapter;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.fragments.createprofileintro.FragInformation;
import com.fragments.createprofileintro.FragProfInfo;
import com.fragments.createprofileintro.FragProfileTypeName;
import com.fragments.createprofileintro.FragTagSubTag;
import com.interfaces.OnDoneButton;

/**
 * Created by ZTLAB-10 on 15-01-2018.
 */

public class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {

    OnDoneButton onDoneButton;

    public ScreenSlidePagerAdapter(FragmentManager fm, OnDoneButton onDoneButton) {
        super(fm);
        this.onDoneButton = onDoneButton;
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        Bundle bundle = new Bundle();
        switch (position) {
            case 0:
                bundle.putInt("pos", 0);
                fragment = FragInformation.getInstance(bundle);
                break;
            case 1:
                bundle.putInt("pos", 1);
                fragment = FragProfileTypeName.getInstance(bundle);
                break;
            case 2:
                bundle.putInt("pos", 2);
                fragment = FragTagSubTag.getInstance(bundle);
                break;
            case 3:
                bundle.putInt("pos", 3);
                fragment = FragProfInfo.getInstance(bundle);
                break;
        }
        return fragment;
    }

    @Override
    public int getCount() {
        return 4;
    }
}
