package com.adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.activities.UpdateExperienceActivity;
import com.models.logindata.ExperienceDetail;
import com.quickkonnect.R;
import com.utilities.Constants;
import com.utilities.Utilities;

import java.util.List;

/**
 * Created by ZTLAB03 on 30-11-2017.
 */

public class UpdateExperienceListAdapter extends RecyclerView.Adapter<UpdateExperienceListAdapter.ViewHolder> implements View.OnClickListener {

    List<ExperienceDetail> experiences;
    static Activity mActivity;

    public UpdateExperienceListAdapter(Activity activity, List<ExperienceDetail> list) {
        this.experiences = list;
        mActivity = activity;
    }

    @Override
    public UpdateExperienceListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_experience_view, null);
        UpdateExperienceListAdapter.ViewHolder viewHolder = new UpdateExperienceListAdapter.ViewHolder(itemLayoutView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(UpdateExperienceListAdapter.ViewHolder holder, int position) {

        ExperienceDetail experience = experiences.get(position);

        if (Utilities.isEmpty(experience.getTitle()))
            holder.tv_title.setText(experience.getTitle());
        else
            holder.tv_title.setText("Title");

        if (Utilities.isEmpty(experience.getCompanyName()))
            holder.tv_company_name.setText(experience.getCompanyName());
        else
            holder.tv_company_name.setVisibility(View.GONE);

        if (Utilities.isEmpty(experience.getLocation()))
            holder.tv_location.setText(experience.getLocation());
        else
            holder.tv_location.setVisibility(View.GONE);

        if (Utilities.isEmpty(experience.getStartDate()) && Utilities.isEmpty(experience.getEndDate())) {
            String endDate = experience.getEndDate();
            try {
                String format_date[] = endDate.split("-");
                endDate = format_date[2] + "/" + format_date[1] + "/" + format_date[0];
            } catch (Exception e) {
                e.printStackTrace();
            }
            holder.tv_startdate.setText(experience.getStartDate() + " - " + endDate);

        } else if (Utilities.isEmpty(experience.getStartDate())) {
            holder.tv_startdate.setText(experience.getStartDate() + " - Present");
        } else {
            holder.tv_startdate.setVisibility(View.GONE);
        }

        holder.img_delete.setTag(experience);
        holder.img_delete.setOnClickListener(this);

        holder.tv_title.setTag(experience);
        holder.tv_title.setOnClickListener(this);

        holder.tv_company_name.setTag(experience);
        holder.tv_company_name.setOnClickListener(this);

        holder.tv_location.setTag(experience);
        holder.tv_location.setOnClickListener(this);

        holder.tv_startdate.setTag(experience);
        holder.tv_startdate.setOnClickListener(this);

        holder.tv_enddate.setTag(experience);
        holder.tv_enddate.setOnClickListener(this);
    }

    @Override
    public int getItemCount() {
        return experiences.size();
    }

    @Override
    public void onClick(View view) {
        ExperienceDetail expList = (ExperienceDetail) view.getTag();
        switch (view.getId()) {
            case R.id.img_delete:
            case R.id.tv_title:
            case R.id.tv_company_name:
            case R.id.tv_location:
            case R.id.tv_startdate:
            case R.id.tv_enddate:
                Intent intent = new Intent(mActivity, UpdateExperienceActivity.class);
                intent.putExtra("experience", expList);
                intent.putExtra(Constants.KEY_UPDATE, Constants.EDIT_DATA);
                mActivity.startActivityForResult(intent, Constants.RCODE_UPDATEEDUCATION);
                break;
        }
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_title, tv_company_name, tv_location, tv_startdate, tv_enddate;
        private ImageView img_delete;
        private LinearLayout linear_title;
        private LinearLayout linear_edit;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            tv_title = itemLayoutView.findViewById(R.id.tv_title);
            tv_company_name = itemLayoutView.findViewById(R.id.tv_company_name);
            tv_location = itemLayoutView.findViewById(R.id.tv_location);
            tv_startdate = itemLayoutView.findViewById(R.id.tv_startdate);
            tv_enddate = itemLayoutView.findViewById(R.id.tv_enddate);
            img_delete = itemLayoutView.findViewById(R.id.img_delete);
            linear_title = itemLayoutView.findViewById(R.id.linear_title);
            linear_edit = itemLayoutView.findViewById(R.id.linear_edit);
        }
    }
}

