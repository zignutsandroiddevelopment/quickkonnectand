package com.interfaces;

public interface OnImagePick {

    void onUploadPhoto();

    void onTakePhoto();

    void removePhoto();
}
