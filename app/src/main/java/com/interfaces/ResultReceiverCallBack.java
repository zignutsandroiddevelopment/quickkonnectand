package com.interfaces;

/**
 * Created by ZTLAB-12 on 06-06-2018.
 */

public interface ResultReceiverCallBack<T> {
    public void onSuccess(T data);

    public void onError(Exception exception);

}
