package com.interfaces;

import com.quickkonnect.UpdateQkTagActivity;

/**
 * Created by ZTLAB-12 on 13-05-2018.
 */

public interface ImageSelected {
    void onImageSlected(int pos, UpdateQkTagActivity.BgList bgList);
}
