package com.interfaces;

import com.models.profiles.ProfilesList;
import com.realmtable.MyProfiles;

/**
 * Created by ZTLAB03 on 17-03-2018.
 */

public interface Refresh_Nav_Activity {

    void onRefresh(ProfilesList myProfiles);
}
