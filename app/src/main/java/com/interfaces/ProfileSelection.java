package com.interfaces;

import com.realmtable.MyProfiles;

/**
 * Created by ZTLAB-12 on 07-05-2018.
 */

public interface ProfileSelection {
    void onProfileSelected(int pos, MyProfiles myProfiles, String Name);
}
