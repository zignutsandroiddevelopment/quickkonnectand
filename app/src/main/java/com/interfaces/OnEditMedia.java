package com.interfaces;

import com.models.createprofile.SocialMedium;

/**
 * Created by ZTLAB-10 on 22-02-2018.
 */

public interface OnEditMedia {

    void getEditMedia(int pos, SocialMedium socialMedium, int action);
}
