package com.interfaces;

import android.support.v4.app.Fragment;

/**
 * Created by ZTLAB-10 on 24-01-2018.
 */

public interface OnFragLoad {

    void onFragLoad(android.app.Fragment fragment);
    void onFragLoad(Fragment fragment);
}
