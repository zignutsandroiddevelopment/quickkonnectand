package com.interfaces;

import com.models.createprofile.Founder;

/**
 * Created by ZTLAB-10 on 22-02-2018.
 */

public interface OnEditFounder {

    void getEditFounder(int pos, Founder founder, int action);
}
