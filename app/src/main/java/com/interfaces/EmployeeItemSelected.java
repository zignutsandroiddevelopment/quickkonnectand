package com.interfaces;

import java.util.ArrayList;

/**
 * Created by ZTLAB-12 on 15-05-2018.
 */

public interface EmployeeItemSelected {

    int EMP_PHONE = 1;
    int EMP_EMAIL = 2;
    int EMP_FAX = 3;
    int EMP_WEBSITE = 4;

    int OPERATION_EDIT = 5;
    int OPERATION_DELETE = 6;

    void onItemSelecetd(int pos, String item, int type);
}
