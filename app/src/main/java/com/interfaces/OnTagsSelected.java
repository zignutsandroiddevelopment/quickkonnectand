package com.interfaces;

import com.models.tagsdata.ModelTagDetail;

/**
 * Created by ZTLAB-10 on 21-02-2018.
 */

public interface OnTagsSelected {

    void onTagsSelected(ModelTagDetail modelTagDetail);
}
