package com.interfaces;

/**
 * Created by ZTLAB-12 on 12-05-2018.
 */

public interface AddRemevoItem {


    int TYPE_PHONE = 1;
    int TYPE_EMAIL = 2;
    int TYPE_WEBLINK = 3;
    int TYPE_FAX = 4;

    void onAddRemove(int position, int type);
}
