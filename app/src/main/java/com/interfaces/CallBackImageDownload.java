package com.interfaces;

import android.graphics.Bitmap;

/**
 * Created by ZTLAB09 on 15-12-2017.
 */

public interface CallBackImageDownload {

    void getBase64(String mData);
    void getBitmap(Bitmap mData);
}
