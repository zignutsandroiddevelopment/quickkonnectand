package com.interfaces;

import com.quickkonnect.NavModelItems;

public interface OnItemClick {
    void getItemClick(int itemType, NavModelItems modelItems, String action);
}