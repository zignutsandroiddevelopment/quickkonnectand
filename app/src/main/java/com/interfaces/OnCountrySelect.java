package com.interfaces;

import com.models.ModelCountry;

/**
 * Created by ZTLAB03 on 01-01-2018.
 */

public interface OnCountrySelect {

    void getCountry(ModelCountry modelCountry);
}
