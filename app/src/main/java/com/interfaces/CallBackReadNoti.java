package com.interfaces;

import com.models.ModelNotifications;

/**
 * Created by ZTLAB03 on 19-12-2017.
 */

public interface CallBackReadNoti {

    void actionNotification(ModelNotifications modelNotifications, int pos);

    void onLoadmore();

    void onReject(ModelNotifications modelNotifications, int pos);
    void onAccept(ModelNotifications modelNotifications, int pos);
}
