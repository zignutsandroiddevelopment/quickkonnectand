package com.interfaces;

import com.quickkonnect.PojoClasses;

/**
 * Created by ZTLAB03 on 13-04-2018.
 */

public interface BlockedProfile {
    void onBlockedProfile(int pos, PojoClasses.EmployeesList contactsList,String profile,String user);
}
