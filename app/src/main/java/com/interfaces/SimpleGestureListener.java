package com.interfaces;

/**
 * Created by ZTLAB09 on 13-12-2017.
 */

public interface SimpleGestureListener {

    void onSwipe(int direction);
    void onDoubleTap();
}
