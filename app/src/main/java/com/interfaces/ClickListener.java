package com.interfaces;

import android.view.View;

/**
 * Created by ZTLAB-12 on 29-05-2018.
 */

public interface ClickListener {
    void onClick(View view, int position);

    void onLongClick(View view, int position);
}
