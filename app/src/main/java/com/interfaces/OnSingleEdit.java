package com.interfaces;

/**
 * Created by ZTLAB-10 on 22-02-2018.
 */

public interface OnSingleEdit {

    int DELETE = 1;
    int EDIT = 2;

    int TYPE_EMAIL = 1;
    int TYPE_PHONE = 2;
    int TYPE_WEBLINK = 3;
    int TYPE_FAX = 4;

    void getEditSingle(int pos, String data, int action, int type);
}
