package com.interfaces;

/**
 * Created by ZTLAB09 on 14-12-2017.
 */

public interface CallBackGetResult {

    void onSuccess();
    void onCancel();
}
