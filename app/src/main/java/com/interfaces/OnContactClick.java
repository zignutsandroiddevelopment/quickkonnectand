package com.interfaces;

import com.models.contacts.Datum;
import com.quickkonnect.PojoClasses;

/**
 * Created by ZTLAB-10 on 16-02-2018.
 */

public interface OnContactClick {

    void getContact(Datum contactsList, int type);

    void getFollower(PojoClasses.FollowersList_Contact followerlist);

    void getFollowing(PojoClasses.FollowingList_Contact followinglist);

    void onLoadMore(String type);
}
