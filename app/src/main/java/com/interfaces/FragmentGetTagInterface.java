package com.interfaces;

import com.models.tagsdata.ModelTagDetail;

import java.util.List;

/**
 * Created by ZTLAB03 on 14-03-2018.
 */

public interface FragmentGetTagInterface {

    void getList(List<ModelTagDetail> modelTagDetails);
}
