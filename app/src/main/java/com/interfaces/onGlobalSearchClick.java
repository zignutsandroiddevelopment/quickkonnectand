package com.interfaces;

import com.models.globalsearch.Continent;
import com.models.globalsearch.ModelGlobalSearch;

/**
 * Created by ZTLAB-10 on 04-05-2018.
 */

public interface onGlobalSearchClick {

    public void onChildClick(ModelGlobalSearch modelGlobalSearch);

    public void onMoreresultRefresh(Continent nContinent);

    public void onListEmpty(boolean b);
}
