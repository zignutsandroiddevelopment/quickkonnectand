package com.interfaces;

import org.json.JSONObject;

/**
 * Created by ZTLAB09 on 14-12-2017.
 */

public interface OnSuccessError {

    void onSuccess(JSONObject jsonObject);
    void onCancel(String error_message);
}