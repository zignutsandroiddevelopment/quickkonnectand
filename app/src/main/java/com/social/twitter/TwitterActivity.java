package com.social.twitter;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.google.gson.Gson;
import com.quickkonnect.R;
import com.social.Constants;
import com.social.SendSocialDataUtils;

import org.json.JSONException;
import org.json.JSONObject;

import twitter4j.PagableResponseList;
import twitter4j.ResponseList;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.User;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;
import twitter4j.conf.ConfigurationBuilder;

public class TwitterActivity extends AppCompatActivity {
    static final String PREF_KEY_OAUTH_TOKEN = "oauth_token";
    static final String PREF_KEY_OAUTH_SECRET = "oauth_token_secret";
    static final String PREF_KEY_TWITTER_LOGIN = "isTwitterLogedIn";
    static final String TWITTER_CALLBACK_URL = "oauth://t4jsample";
    // Twitter oauth urls
    static final String URL_TWITTER_AUTH = "auth_url";
    static final String URL_TWITTER_OAUTH_VERIFIER = "oauth_verifier";
    static final String URL_TWITTER_OAUTH_TOKEN = "oauth_token";
    // Preference Constants
    static String PREFERENCE_NAME = "twitter_oauth";
    static String oauth_url;
    static Dialog auth_dialog;
    static String oauth_verifier;
    static AccessToken accessToken;
    static String profile_url;
    static ProgressDialog progress;
    // Twitter
    private static Twitter twitter;
    private static RequestToken requestToken;
    // Shared Preferences
    private static SharedPreferences mSharedPreferences;
    private static String TAG = "TAG";

    private void getDetailsForTwitter() {
        ConfigurationBuilder cb = new ConfigurationBuilder();
        cb.setDebugEnabled(true)
                .setOAuthConsumerKey(getString(R.string.com_twitter_sdk_android_CONSUMER_KEY))
                .setOAuthConsumerSecret(getString(R.string.com_twitter_sdk_android_CONSUMER_SECRET))
                .setOAuthAccessToken(mSharedPreferences.getString(PREF_KEY_OAUTH_TOKEN, ""))
                .setOAuthAccessTokenSecret(mSharedPreferences.getString(PREF_KEY_OAUTH_SECRET, ""));
        TwitterFactory tf = new TwitterFactory(cb.build());
        Twitter twitter = tf.getInstance();

        User user;
        try {
            Log.d(TAG, "getDetailsForTwitter: " + mSharedPreferences.getString(PREF_KEY_OAUTH_SECRET, ""));

            long userId = new AccessToken(mSharedPreferences.getString(PREF_KEY_OAUTH_TOKEN, ""), mSharedPreferences.getString(PREF_KEY_OAUTH_SECRET, "")).getUserId();
            user = twitter.showUser(userId);

            JSONObject dataObject = new JSONObject();
            dataObject.put("profile_id", user.getId());
            dataObject.put("name", user.getName());
            dataObject.put("favourite_count", user.getFavouritesCount());
            dataObject.put("followers_count", user.getFollowersCount());
            dataObject.put("friends_count", user.getFriendsCount());
            dataObject.put("screen_name", user.getScreenName());
            dataObject.put("favourites", new Gson().toJson(twitter.getFavorites(), ResponseList.class));
            dataObject.put("friends_list", new Gson().toJson(twitter.getFriendsList(user.getScreenName(), -1), PagableResponseList.class));
            dataObject.put("followers_list", new Gson().toJson(twitter.getFollowersList(user.getScreenName(), -1), PagableResponseList.class));

            Log.d("TAG", "doInBackground: " + dataObject);

            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("user_id", 38);
                jsonObject.put("social_media_id", Constants.SOCIAL_TWITTER_ID);
                jsonObject.put("data", dataObject);
                SendSocialDataUtils.sendToServer(TwitterActivity.this, jsonObject);
            } catch (JSONException e) {
                e.printStackTrace();
            }

        } catch (TwitterException e) {
            Log.e("TAG", "getDetailsForTwitter: " + e.getErrorMessage());
        } catch (JSONException e) {
            Log.e("TAG", "getDetailsForTwitter: " + e.getLocalizedMessage());
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_twitter);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        // Shared Preferences
        mSharedPreferences = getApplicationContext().getSharedPreferences("MyPref", 0);
        twitter = new TwitterFactory().getInstance();
        twitter.setOAuthConsumer(getString(R.string.com_twitter_sdk_android_CONSUMER_KEY), getString(R.string.com_twitter_sdk_android_CONSUMER_SECRET));

        auth_dialog = new Dialog(TwitterActivity.this);
        auth_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        auth_dialog.setContentView(R.layout.auth_dialog);

        progress = new ProgressDialog(TwitterActivity.this);
        progress.setMessage("Fetching ModelLoggedData ...");
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progress.setIndeterminate(true);

        findViewById(R.id.buttonTwitter).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //                loginToTwitter();
                if (!isTwitterLoggedInAlready()) {
                    new TokenGet().execute();
                } else {
                    getDetailsForTwitter();
                }
            }
        });
    }

    /**
     * Check user already logged in your application using twitter Login flag is
     * fetched from Shared Preferences
     */
    private boolean isTwitterLoggedInAlready() {
        // return twitter login status from Shared Preferences
        return mSharedPreferences.getBoolean(PREF_KEY_TWITTER_LOGIN, false);
    }

    private class TokenGet extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... args) {

            try {
                requestToken = twitter.getOAuthRequestToken();
                oauth_url = requestToken.getAuthorizationURL();
            } catch (TwitterException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            return oauth_url;
        }

        @Override
        protected void onPostExecute(String oauth_url) {
            if (oauth_url != null) {
                Log.e("URL", oauth_url);
                WebView web = auth_dialog.findViewById(R.id.webv);
                web.getSettings().setJavaScriptEnabled(true);
                web.loadUrl(oauth_url);
                web.setWebViewClient(new WebViewClient() {
                    boolean authComplete = false;

                    @Override
                    public void onPageStarted(WebView view, String url, Bitmap favicon) {
                        super.onPageStarted(view, url, favicon);
                    }

                    @Override
                    public void onPageFinished(WebView view, String url) {
                        super.onPageFinished(view, url);
                        if (url.contains(URL_TWITTER_OAUTH_VERIFIER) && !authComplete) {
                            authComplete = true;
                            Log.e("Url", url);
                            Uri uri = Uri.parse(url);
                            oauth_verifier = uri.getQueryParameter(URL_TWITTER_OAUTH_VERIFIER);
                            auth_dialog.dismiss();
                            new AccessTokenGet().execute();
                        } else if (url.contains("denied")) {
                            auth_dialog.dismiss();
                        }
                    }
                });
                auth_dialog.show();
                auth_dialog.setCancelable(true);
            } else {
                Log.d("TAG", "onPostExecute: Network Error");
                //                Toast.makeTex(), "Sorry !, Network Error or Invalid Credentials", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private class AccessTokenGet extends AsyncTask<String, String, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress.show();
        }

        @Override
        protected Boolean doInBackground(String... args) {
            try {
                accessToken = twitter.getOAuthAccessToken(requestToken, oauth_verifier);
                SharedPreferences.Editor edit = mSharedPreferences.edit();
                edit.putString(PREF_KEY_OAUTH_TOKEN, accessToken.getToken());
                edit.putString(PREF_KEY_OAUTH_SECRET, accessToken.getTokenSecret());
                edit.putBoolean(PREF_KEY_TWITTER_LOGIN, true);
                edit.apply();
            } catch (TwitterException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            return true;
        }

        @Override
        protected void onPostExecute(Boolean response) {
            if (response) {
                progress.hide();
                getDetailsForTwitter();
            }
        }
    }
}
