package com.social;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.quickkonnect.R;
import com.social.instagram.InstagramApp;
import com.social.twitter.TwitterActivity;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.linkedin.platform.APIHelper;
import com.linkedin.platform.LISessionManager;
import com.linkedin.platform.errors.LIApiError;
import com.linkedin.platform.errors.LIAuthError;
import com.linkedin.platform.listeners.ApiListener;
import com.linkedin.platform.listeners.ApiResponse;
import com.linkedin.platform.listeners.AuthListener;
import com.linkedin.platform.utils.Scope;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;

public class SocialActivity extends AppCompatActivity {

    /**
     * FACEBOOK VARIABLES
     */
    CallbackManager callbackManager;
    private String TAG = "TAG";

    /**
     * Instagram
     */
    private InstagramApp mApp;
    private HashMap<String, String> userInfoHashmap = new HashMap<String, String>();
    private Handler handler = new Handler(new Handler.Callback() {

        @Override
        public boolean handleMessage(Message msg) {
            if (msg.what == InstagramApp.WHAT_FINALIZE) {
                userInfoHashmap = mApp.getUserInfo();
                getInstagramFollowers();
                getInstagramFollowings();
            } else if (msg.what == InstagramApp.WHAT_FINALIZE) {
                Toast.makeText(SocialActivity.this, "Check your network.", Toast.LENGTH_SHORT).show();
            }
            return false;
        }
    });

    private Button buttonInstagram;

    /**
     * Process for LinkedIn
     */
    private Button buttonLinkedIn;

    /**
     * Process for Twitter
     */
    private TwitterLoginButton buttonTwitter;

    // set the permission to retrieve basic information of User's linkedIn account
    private static Scope buildScope() {
        return Scope.build(Scope.R_BASICPROFILE, Scope.R_EMAILADDRESS);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_social);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        /**
         * Setup For Facebook
         */
        setupForFacebook();

        /**
         * Setup for Instagram
         */
        setupForInstagram();

        /**
         * Setup for Linkedin
         */
        setupForLinkedIn();

    }

    public void setupForTwitter(View view) {
        startActivity(new Intent(SocialActivity.this, TwitterActivity.class));
        //        buttonTwitter = (TwitterLoginButton) findViewById(R.id.button_twitter);
        //        buttonTwitter.setCallback(new Callback<TwitterSession>() {
        //            @Override
        //            public void success(Result<TwitterSession> result) {
        //                // Do something with result, which provides a TwitterSession for making API calls
        //                Log.d(TAG, "success: " + result.data.getUserId() + " : " + result.data.getUserName());
        //            }
        //
        //            @Override
        //            public void failure(TwitterException exception) {
        //                // Do something on failure
        //                Log.e(TAG, "failure: " + exception.getLocalizedMessage());
        //            }
        //        });
    }

    /**
     * Setting up with Instagram
     */
    private void setupForInstagram() {
        //        String scope = "basic+public_content+follower_list+comments+relationships+likes";

        buttonInstagram = findViewById(R.id.button_instagram);

        mApp = new InstagramApp(this, Constants.IG_CLIENT_ID, Constants.IG_CLIENT_SECRET, Constants.IG_CALLBACK_URL);
        mApp.setListener(new InstagramApp.OAuthAuthenticationListener() {

            @Override
            public void onSuccess() {
                // tvSummary.setText("Connected as " + mApp.getUserName());
                buttonInstagram.setText("Disconnect Instagram");
                // userInfoHashmap = mApp.
                mApp.fetchUserName(handler);
            }

            @Override
            public void onFail(String error) {
                Toast.makeText(SocialActivity.this, error, Toast.LENGTH_SHORT).show();
            }
        });

        if (mApp.hasAccessToken()) {
            // tvSummary.setText("Connected as " + mApp.getUserName());
            buttonInstagram.setText("Disconnect");
            mApp.fetchUserName(handler);
        }

        buttonInstagram.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                connectOrDisconnectUser();

                getInstagramFollowers();
                getInstagramFollowings();

            }
        });
    }

    /**
     * Setting up with Instagram
     */
    private void setupForFacebook() {
        LoginButton loginButton = findViewById(R.id.button_facebook);
        loginButton.setReadPermissions(Arrays.asList("public_profile", "email", "user_location", "user_education_history", "user_website", "user_work_history", "read_custom_friendlists", "user_friends", "user_about_me", "user_birthday", "user_hometown", "user_likes", "user_events", "user_religion_politics", "user_tagged_places"));
        callbackManager = CallbackManager.Factory.create();

        // Callback registration
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                // App code
                Toast.makeText(SocialActivity.this, "Logging Successfully.", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onCancel() {
                // App code
                Toast.makeText(SocialActivity.this, "Cancel", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
                Toast.makeText(SocialActivity.this, "Ex: " + exception.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    /**
     * Setting up with LinkedIn
     */
    private void setupForLinkedIn() {

        buttonLinkedIn = findViewById(R.id.button_linkedin);
        buttonLinkedIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                LISessionManager.getInstance(SocialActivity.this).init(SocialActivity.this, buildScope(), new AuthListener() {
                    @Override
                    public void onAuthSuccess() {

                        // Authentication was successful.  You can now do
                        // other calls with the SDK.
                        Log.e(TAG, "sUCCESS: ");

                        String url = "https://api.linkedin.com/v1/people/~:(id,first-name,last-name,email-address,location,industry)";
                        Log.d(TAG, "onAuthSuccess: " + url);

                        APIHelper apiHelper = APIHelper.getInstance(getApplicationContext());
                        apiHelper.getRequest(SocialActivity.this, url, new ApiListener() {
                            @Override
                            public void onApiSuccess(ApiResponse apiResponse) {
                                // Success!
                                Log.d(TAG, "ApiResponse: " + apiResponse.getResponseDataAsString());
                            }

                            @Override
                            public void onApiError(LIApiError liApiError) {
                                // Error making GET request!
                                Log.d(TAG, "Error : " + liApiError.getHttpStatusCode());
                            }
                        });
                    }

                    @Override
                    public void onAuthError(LIAuthError error) {
                        // Handle authentication errors
                        Log.e(TAG, "onAuthError: " + error.toString());
                    }
                }, true);
            }
        });
    }

    //    @Override
    //    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    //        LISessionManager.getInstance(getApplicationContext()).onActivityResult(this, requestCode, resultCode, data);
    //        //        Intent intent = new Intent(MainActivity.this, HomePage.class);
    //        //        startActivity(intent);
    //    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //            if (resultCode == RESULT_OK) {
        Log.d(TAG, "onActivityResult: FACEBOOK : " + requestCode);
        /**
         * Facebook
         */
        //        callbackManager.onActivityResult(requestCode, resultCode, data);
        //        String otherUserId = "1869382549738893";// here you have to pass other user_id
        //        getFacebookMutualFriendsList(otherUserId);
        //        getFacebookUserData();

        /**
         * LinkedIn
         */
        //LISessionManager.getInstance(getApplicationContext()).onActivityResult(this, requestCode, resultCode, data);

        /**
         * Twitter
         */
        // Pass the activity result to the login button.
        buttonTwitter.onActivityResult(requestCode, resultCode, data);
    }

    private void getFacebookUserData() {
        try {
            GraphRequest request = GraphRequest.newMeRequest(AccessToken.getCurrentAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                @Override
                public void onCompleted(JSONObject object, GraphResponse response) {
                    // Application code
                    Log.d(TAG, "onCompleted: " + object + " : ");
                    JSONObject jsonObject = new JSONObject();
                    try {
                        jsonObject.put("user_id", 38);
                        jsonObject.put("social_media_id", com.utilities.Constants.SOCIAL_FACEBOOK_ID);
                        jsonObject.put("data", object);
                        SendSocialDataUtils.sendToServer(SocialActivity.this, jsonObject);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
            Bundle parameters = new Bundle();
            parameters.putString("fields", "id,name,first_name,music,last_name,email,location{location},education,website,work,friendlists,friends,link,about,birthday,hometown,books,events,religion,political,tagged_places");
            request.setParameters(parameters);
            request.executeAsync();

        } catch (FacebookException e) {
            e.printStackTrace();
        }
    }

    private void getFacebookMutualFriendsList(String fbId) {
        Bundle params = new Bundle();
        params.putString("fields", "context.fields(mutual_friends)");
        new GraphRequest(AccessToken.getCurrentAccessToken(), "/" + fbId, params, HttpMethod.GET, new GraphRequest.Callback() {
            @Override
            public void onCompleted(GraphResponse graphResponse) {
                try {
                    JSONObject jsonObject = new JSONObject(graphResponse.getRawResponse());
                    if (jsonObject.has("context")) {
                        jsonObject = jsonObject.getJSONObject("context");
                        if (jsonObject.has("mutual_friends")) {
                            JSONArray mutualFriendsJSONArray = jsonObject.getJSONObject("mutual_friends").getJSONArray("data");
                            // this mutualFriendsJSONArray contains the id and name of the mutual friends.
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).executeAsync();
    }


    private void connectOrDisconnectUser() {
        if (mApp.hasAccessToken()) {
            final AlertDialog.Builder builder = new AlertDialog.Builder(SocialActivity.this);
            builder.setMessage("Disconnect from Instagram?").setCancelable(false).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    mApp.resetAccessToken();
                    // btnConnect.setVisibility(View.VISIBLE);
                    buttonInstagram.setText("Connect to Instagram");
                    // tvSummary.setText("Not connected");
                }
            }).setNegativeButton("No", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.cancel();
                }
            });
            final AlertDialog alert = builder.create();
            alert.show();
        } else {
            mApp.authorize();
        }
    }

    public String getInstagramFollowers() {
        String instagramFollowers = "";
        String url = "https://api.instagram.com/v1/users/" + userInfoHashmap.get(InstagramApp.TAG_ID) + "/follows?access_token=" + mApp.getTOken();

        Log.d(TAG, "getInstagramFollowers: " + url);

        final ProgressDialog mDialog = new ProgressDialog(SocialActivity.this);
        mDialog.setTitle("Loading...");
        mDialog.setMessage("Getting Instagran Followers");
        mDialog.setIndeterminate(true);
        mDialog.setCancelable(false);
        mDialog.show();

        JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                // TODO Auto-generated method stub
                mDialog.dismiss();
                Log.i("Success --> ", "Response :" + response.toString());
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                // TODO Auto-generated method stub
                mDialog.dismiss();
                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null) {
                    Log.e("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                }

                if (error instanceof TimeoutError) {
                    Log.e("Volley", "TimeoutError");
                } else if (error instanceof NoConnectionError) {
                    Log.e("Volley", "NoConnectionError");
                } else if (error instanceof AuthFailureError) {
                    Log.e("Volley", "AuthFailureError");
                } else if (error instanceof ServerError) {
                    Log.e("Volley", "ServerError");
                } else if (error instanceof NetworkError) {
                    Log.e("Volley", "NetworkError");
                } else if (error instanceof ParseError) {
                    Log.e("Volley", "ParseError");
                }
            }
        });
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(100000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Volley.newRequestQueue(SocialActivity.this).add(jsObjRequest);

        return instagramFollowers;
    }

    public String getInstagramFollowings() {
        String instagramFollowings = "";

        String url = "https://api.instagram.com/v1/users/" + userInfoHashmap.get(InstagramApp.TAG_ID) + "/followed-by?access_token=" + mApp.getTOken();
        Log.d(TAG, "getInstagramFollowings: " + url);

        final ProgressDialog mDialog = new ProgressDialog(SocialActivity.this);
        mDialog.setTitle("Loading...");
        mDialog.setMessage("Getting Instagran Followings");
        mDialog.setIndeterminate(true);
        mDialog.setCancelable(false);
        mDialog.show();

        JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                // TODO Auto-generated method stub
                mDialog.dismiss();
                Log.i("Success --> ", "Response :" + response.toString());
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                // TODO Auto-generated method stub
                mDialog.dismiss();
                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null) {
                    Log.e("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                }

                if (error instanceof TimeoutError) {
                    Log.e("Volley", "TimeoutError");
                } else if (error instanceof NoConnectionError) {
                    Log.e("Volley", "NoConnectionError");
                } else if (error instanceof AuthFailureError) {
                    Log.e("Volley", "AuthFailureError");
                } else if (error instanceof ServerError) {
                    Log.e("Volley", "ServerError");
                } else if (error instanceof NetworkError) {
                    Log.e("Volley", "NetworkError");
                } else if (error instanceof ParseError) {
                    Log.e("Volley", "ParseError");
                }
            }
        });
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(100000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Volley.newRequestQueue(SocialActivity.this).add(jsObjRequest);

        return instagramFollowings;
    }
}
