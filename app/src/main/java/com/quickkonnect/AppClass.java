package com.quickkonnect;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;
import android.widget.Toast;

import com.facebook.appevents.AppEventsLogger;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.twitter.sdk.android.core.Twitter;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by Pratik on 30-Oct-17.
 */

public class AppClass extends Application {

    private static AppClass instance;
    public static List<String> facebook_permission;
    public static String facebook_requestdata
            = "id,name,first_name,music,last_name,email,location{location},education,website,work,friendlists,friends,link,about,birthday,hometown,books,events,religion,political,tagged_places";
    public static String LINKEDIN_URL
            = "https://api.linkedin.com/v1/people/~:(id,first-name,public-profile-url,last-name,email-address,location,industry)";
    // = "https://api.linkedin.com/v1/people/~:(id,first-name,last-name,email-address,location,industry)";

    @Override
    public void onCreate() {
        super.onCreate();

        instance = this;

        /**
         * Twitter Initialization
         */
        Twitter.initialize(this);

        /**
         * Facebook Initialization
         */
        AppEventsLogger.activateApp(this);

        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder(this)
                .name(Realm.DEFAULT_REALM_NAME)
                .schemaVersion(0)
                .deleteRealmIfMigrationNeeded()
                .build();
        Realm.setDefaultConfiguration(realmConfiguration);

        facebook_permission = new ArrayList<>();

        facebook_permission.add("public_profile");
        facebook_permission.add("email");
        facebook_permission.add("user_location");
        facebook_permission.add("user_education_history");
        facebook_permission.add("user_website");
        facebook_permission.add("user_work_history");
        facebook_permission.add("read_custom_friendlists");
        facebook_permission.add("user_friends");
        facebook_permission.add("user_about_me");
        facebook_permission.add("user_birthday");
        facebook_permission.add("user_hometown");

        initImageLoader(getApplicationContext());
    }

    public static void initImageLoader(Context context) {
        // This configuration tuning is custom. You can tune every option, you may tune some of them,
        // or you can create default configuration by
        //  ImageLoaderConfiguration.createDefault(this);
        // method.
        ImageLoaderConfiguration.Builder config = new ImageLoaderConfiguration.Builder(context);
        config.threadPriority(Thread.NORM_PRIORITY - 2);
        config.denyCacheImageMultipleSizesInMemory();
        config.diskCacheFileNameGenerator(new Md5FileNameGenerator());
        config.diskCacheSize(50 * 1024 * 1024); // 50 MiB
        config.tasksProcessingOrder(QueueProcessingType.LIFO);
        config.writeDebugLogs(); // Remove for release app

        // Initialize ImageLoader with configuration.
        ImageLoader.getInstance().init(config.build());
    }

    public static AppClass getInstance() {
        return instance;
    }

    @Override
    protected void attachBaseContext(Context context) {
        super.attachBaseContext(context);
        MultiDex.install(this);
    }
}
