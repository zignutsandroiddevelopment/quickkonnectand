package com.quickkonnect;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.Selection;
import android.text.TextWatcher;
import android.util.Log;
import android.view.GestureDetector;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.activities.ConfirmRegistration;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.plus.Plus;
import com.google.code.linkedinapi.client.LinkedInApiClient;
import com.google.code.linkedinapi.client.LinkedInApiClientFactory;
import com.google.code.linkedinapi.client.enumeration.ProfileField;
import com.google.code.linkedinapi.client.oauth.LinkedInAccessToken;
import com.google.code.linkedinapi.schema.Person;
import com.google.firebase.database.FirebaseDatabase;
import com.google.gson.Gson;
import com.linkedin.LinkedinDialog;
import com.linkedin.platform.APIHelper;
import com.linkedin.platform.LISessionManager;
import com.linkedin.platform.errors.LIApiError;
import com.linkedin.platform.errors.LIAuthError;
import com.linkedin.platform.listeners.ApiListener;
import com.linkedin.platform.listeners.ApiResponse;
import com.linkedin.platform.listeners.AuthListener;
import com.linkedin.platform.utils.Scope;
import com.models.ModelSocialMedia;
import com.models.logindata.Facebook;
import com.models.logindata.Google;
import com.models.logindata.Instagram;
import com.models.logindata.Linkedin;
import com.models.logindata.ModelLoggedData;
import com.models.logindata.ModelLoggedUser;
import com.models.logindata.Snapchat;
import com.models.logindata.Twitter;
import com.rilixtech.CountryCodePicker;
import com.social.Constants;
import com.social.instagram.InstagramApp;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterApiClient;
import com.twitter.sdk.android.core.TwitterAuthToken;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;
import com.twitter.sdk.android.core.services.AccountService;
import com.utilities.QKPreferences;
import com.utilities.Utilities;
import com.volleyapirequest.VolleyApiRequest;
import com.volleyapirequest.VolleyCallBack;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;

import retrofit2.Call;
import twitter4j.PagableResponseList;
import twitter4j.ResponseList;
import twitter4j.TwitterFactory;
import twitter4j.User;
import twitter4j.auth.RequestToken;
import twitter4j.conf.ConfigurationBuilder;

import static android.webkit.URLUtil.isValidUrl;
import static com.linkedin.Config.LINKEDIN_CONSUMER_KEY;
import static com.linkedin.Config.LINKEDIN_CONSUMER_SECRET;
import static com.linkedin.Config.TWITTER_PACKAGE_NAME;

public class AddSocialMediaAccountActivity extends AppCompatActivity implements SearchView.OnQueryTextListener, View.OnClickListener, RecyclerView.OnItemTouchListener {

    private RecyclerView recyclerView;
    private static SharedPreferences mSharedPreferences;
    private QKPreferences qkPreferences;
    private ModelLoggedUser modelLoggedUser;
    private RecyclerView.LayoutManager mLayoutManager;
    private AddSocialMediaAccountAdapter adapter;
    private String userid;
    boolean aa = false;
    private ArrayList<ModelSocialMedia> mlist;
    //private Button btn_continue;
    private CallbackManager callbackManager;
    private LoginButton loginButton;
    private GestureDetector mGestureDetector;
    private OnItemClickListener mListener;

    static final String PREF_KEY_OAUTH_TOKEN = "oauth_token";
    static final String PREF_KEY_OAUTH_SECRET = "oauth_token_secret";
    static final String PREF_KEY_TWITTER_LOGIN = "isTwitterLogedIn";
    // Twitter oauth urls
    static final String URL_TWITTER_OAUTH_VERIFIER = "oauth_verifier";
    static String oauth_url;
    static Dialog auth_dialog;
    static String oauth_verifier;
    static twitter4j.auth.AccessToken accessToken;
    // static String profile_url;
    static ProgressDialog progress;
    // Twitter
    private static twitter4j.Twitter twitter;
    private static RequestToken requestToken;
    // Shared Preferences
    private static String TAG = "TAG";
    private InstagramApp mApp;
    private Button buttonInstagram;
    private Bundle bundle;
    final LinkedInApiClientFactory factory = LinkedInApiClientFactory
            .newInstance(LINKEDIN_CONSUMER_KEY,
                    LINKEDIN_CONSUMER_SECRET);
    private LinkedInApiClient client;
    private LinkedInAccessToken accessToken1 = null;
    private Context context;

    private GoogleSignInOptions gso;
    private GoogleApiClient mGoogleApiClient;
    private int SIGN_IN_GOOGLE = 30;

    TwitterAuthClient mTwitterAuthClient;

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }

    public AddSocialMediaAccountActivity() {

    }

    public AddSocialMediaAccountActivity(Context context, OnItemClickListener listener) {
        mListener = listener;
        mGestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
            @Override
            public boolean onSingleTapUp(MotionEvent e) {
                return true;
            }
        });
    }

    ProfileTracker profileTracker;

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (profileTracker != null) {
            profileTracker.stopTracking();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Utilities.changeStatusbar(AddSocialMediaAccountActivity.this, getWindow());
        com.twitter.sdk.android.core.Twitter.initialize(this);
        setContentView(R.layout.activity_add_social_media_account);

        Utilities.ChangeStatusBar(AddSocialMediaAccountActivity.this);
        Toolbar toolbar = findViewById(R.id.toolbar);
        //toolbar.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        //toolbar.setBackgroundResource(R.drawable.toolbar_gradient);
        setSupportActionBar(toolbar);

        context = AddSocialMediaAccountActivity.this;

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        mSharedPreferences = getApplicationContext().getSharedPreferences("MyPref", 0);

        qkPreferences = new QKPreferences(this);

        try {
            userid = qkPreferences.getValuesInt(QKPreferences.USER_ID) + "";
        } catch (Exception e) {
            e.printStackTrace();
        }

        modelLoggedUser = qkPreferences.getLoggedUser();

        //btn_continue = findViewById(R.id.btn_continue);
        recyclerView = findViewById(R.id.recyclerView);

        //btn_continue.setOnClickListener(this);

        twitter = new TwitterFactory().getInstance();
        twitter.setOAuthConsumer(getString(R.string.com_twitter_sdk_android_CONSUMER_KEY), getString(R.string.com_twitter_sdk_android_CONSUMER_SECRET));

        auth_dialog = new Dialog(AddSocialMediaAccountActivity.this);
        auth_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        auth_dialog.setContentView(R.layout.auth_dialog);

        progress = new ProgressDialog(AddSocialMediaAccountActivity.this);
        progress.setMessage("Fetching ModelLoggedData ...");
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progress.setIndeterminate(true);

        FloatingActionButton fab = findViewById(R.id.btn_add);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });


        mSharedPreferences = getSharedPreferences(SharedPreference.PREF_NAME, 0);

        if (qkPreferences != null && modelLoggedUser != null && modelLoggedUser.getData() != null)
            userid = modelLoggedUser.getData().getId() + "";

        recyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL_LIST));
        recyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);

        aa = true;

        mlist = new ArrayList<>();

        ModelSocialMedia modelSocialMedia = new ModelSocialMedia();

        modelSocialMedia.setId(0);
        modelSocialMedia.setName("Quickkonnect");
        modelSocialMedia.setStatus("Active");
        if (modelLoggedUser.getData() != null && !modelLoggedUser.getData().getFirstname().equalsIgnoreCase("") && !modelLoggedUser.getData().getFirstname().equalsIgnoreCase("null")) {
            modelSocialMedia.setUserName(modelLoggedUser.getData().getFirstname() + " " + modelLoggedUser.getData().getLastname());
        } else {
            modelSocialMedia.setUserName("");
        }
        modelSocialMedia.setIc_social_icon(R.drawable.ic_logo);
        mlist.add(modelSocialMedia);

        modelSocialMedia = new ModelSocialMedia();
        modelSocialMedia.setId(1);
        modelSocialMedia.setName("Facebook");
        if (modelLoggedUser != null && modelLoggedUser.getData() != null && modelLoggedUser.getData().getFacebook() != null) {
            Facebook facebook = modelLoggedUser.getData().getFacebook();
            if (facebook.getStatus() == 1)
                modelSocialMedia.setStatus("Active");
            else
                modelSocialMedia.setStatus("Inactive");
            if (facebook != null && facebook.getData() != null
                    && Utilities.isEmpty(facebook.getData().getFirstname()) && Utilities.isEmpty(facebook.getData().getLastName()))
                modelSocialMedia.setUserName(facebook.getData().getFirstname().replace("0", "")
                        + " " + facebook.getData().getLastName().replace("0", ""));
            else if (facebook != null && facebook.getData() != null
                    && Utilities.isEmpty(facebook.getData().getFirstname()))
                modelSocialMedia.setUserName(facebook.getData().getFirstname().replace("0", ""));
        }
        modelSocialMedia.setIc_social_icon(R.drawable.ic_social_fb);
        mlist.add(modelSocialMedia);

        modelSocialMedia = new ModelSocialMedia();
        modelSocialMedia.setId(2);
        modelSocialMedia.setName("Linkedin");
        if (modelLoggedUser != null && modelLoggedUser.getData() != null && modelLoggedUser.getData().getLinkedin() != null) {
            Linkedin facebook = modelLoggedUser.getData().getLinkedin();
            if (facebook.getStatus() == 1)
                modelSocialMedia.setStatus("Active");
            else
                modelSocialMedia.setStatus("Inactive");
            if (facebook != null && facebook.getData() != null
                    && Utilities.isEmpty(facebook.getData().getFirstname()) && Utilities.isEmpty(facebook.getData().getLastName()))
                modelSocialMedia.setUserName(facebook.getData().getFirstname().replace("0", "")
                        + " " + facebook.getData().getLastName().replace("0", ""));
            else if (facebook != null && facebook.getData() != null
                    && Utilities.isEmpty(facebook.getData().getFirstname()))
                modelSocialMedia.setUserName(facebook.getData().getFirstname().replace("0", ""));

        }
        modelSocialMedia.setIc_social_icon(R.drawable.ic_social_linkin);
        mlist.add(modelSocialMedia);

        modelSocialMedia = new ModelSocialMedia();
        modelSocialMedia.setId(3);
        modelSocialMedia.setName("Twitter");
        if (modelLoggedUser != null && modelLoggedUser.getData() != null && modelLoggedUser.getData().getTwitter() != null) {
            Twitter facebook = modelLoggedUser.getData().getTwitter();
            if (facebook.getStatus() == 1)
                modelSocialMedia.setStatus("Active");
            else
                modelSocialMedia.setStatus("Inactive");
            if (facebook != null && facebook.getData() != null
                    && Utilities.isEmpty(facebook.getData().getFirstname()) && Utilities.isEmpty(facebook.getData().getLastName()))
                modelSocialMedia.setUserName(facebook.getData().getFirstname()
                        .replace("0", "") + " " + facebook.getData().getLastName().replace("0", ""));
            else if (facebook != null && facebook.getData() != null
                    && Utilities.isEmpty(facebook.getData().getFirstname()))
                modelSocialMedia.setUserName(facebook.getData().getFirstname().replace("0", ""));

        }
        modelSocialMedia.setIc_social_icon(R.drawable.ic_social_tweet);
        mlist.add(modelSocialMedia);


        modelSocialMedia = new ModelSocialMedia();
        modelSocialMedia.setId(4);
        modelSocialMedia.setName("Instagram");
        modelSocialMedia.setUserName(qkPreferences.getInstaUname());
        if (modelLoggedUser != null && modelLoggedUser.getData() != null && modelLoggedUser.getData().getInstagram() != null) {
            Instagram facebook = modelLoggedUser.getData().getInstagram();
            if (facebook.getStatus() == 1)
                modelSocialMedia.setStatus("Active");
            else
                modelSocialMedia.setStatus("Inactive");
            if (facebook != null && facebook.getData() != null
                    && Utilities.isEmpty(facebook.getData().getFirstname()) && Utilities.isEmpty(facebook.getData().getLastName()))
                modelSocialMedia.setUserName(facebook.getData().getFirstname().replace("0", "")
                        + " " + facebook.getData().getLastName().replace("0", ""));
            else if (facebook != null && facebook.getData() != null
                    && Utilities.isEmpty(facebook.getData().getFirstname()))
                modelSocialMedia.setUserName(facebook.getData().getFirstname().replace("0", ""));

        }
        modelSocialMedia.setIc_social_icon(R.drawable.ic_social_insta);
        mlist.add(modelSocialMedia);


        modelSocialMedia = new ModelSocialMedia();
        modelSocialMedia.setId(5);
        modelSocialMedia.setName("Google Plus");
        if (modelLoggedUser != null && modelLoggedUser.getData() != null && modelLoggedUser.getData().getGoogle() != null) {
            Google google = modelLoggedUser.getData().getGoogle();
            if (google.getStatus() == 1)
                modelSocialMedia.setStatus("Active");
            else
                modelSocialMedia.setStatus("Inactive");
            if (google != null && google.getData() != null && Utilities.isEmpty(google.getData().getUsername()))
                modelSocialMedia.setUserName(google.getData().getUsername().replace("0", ""));

        }
        modelSocialMedia.setIc_social_icon(R.drawable.ic_social_gplus);
        mlist.add(modelSocialMedia);


        modelSocialMedia = new ModelSocialMedia();
        modelSocialMedia.setId(6);
        modelSocialMedia.setName("Snapchat");
        if (modelLoggedUser != null && modelLoggedUser.getData() != null && modelLoggedUser.getData().getSnapchat() != null) {
            Snapchat snapchat = modelLoggedUser.getData().getSnapchat();
            if (snapchat.getStatus() == 1)
                modelSocialMedia.setStatus("Active");
            else
                modelSocialMedia.setStatus("Inactive");
            if (snapchat != null && snapchat.getData() != null
                    && Utilities.isEmpty(snapchat.getData().getFirstname()) && Utilities.isEmpty(snapchat.getData().getLastName()))
                modelSocialMedia.setUserName(snapchat.getData().getFirstname().replace("0", "")
                        + " " + snapchat.getData().getLastName().replace("0", ""));
            else if (snapchat != null && snapchat.getData() != null
                    && Utilities.isEmpty(snapchat.getData().getFirstname()))
                modelSocialMedia.setUserName(snapchat.getData().getFirstname().replace("0", ""));

        }
        modelSocialMedia.setIc_social_icon(R.drawable.share_chat);
        mlist.add(modelSocialMedia);

        adapter = new AddSocialMediaAccountAdapter(AddSocialMediaAccountActivity.this, mlist);
        recyclerView.setAdapter(adapter);

//        if (!Utilities.isNetworkConnected(this))
//            Utilities.showToast(context, getResources().getString(R.string.no_internat_connection));
//        else
//            getSocialMediaList();

        callbackManager = CallbackManager.Factory.create();
        loginButton = findViewById(R.id.login_button);
        loginButton.setReadPermissions(AppClass.facebook_permission);
        callbackManager = CallbackManager.Factory.create();

        // google init
        gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        //mGoogleApiClient = GoogleSignIn.getClient(this, gso);
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
                    }
                })
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .addApi(Plus.API)
                .build();


        // Callback registration
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                // App code
                Log.d("FB TOKEN: ", "" + loginResult.getAccessToken());
                getFacebookUserData(loginResult.getAccessToken());
            }

            @Override
            public void onCancel() {
                // App code
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
            }
        });

        bundle = getIntent().getExtras();
        if (bundle != null) {
            if (getIntent() != null && getIntent().getExtras().getString("from") != null) {
                String from = getIntent().getExtras().getString("from");
                if (from != null && from.equals("menu")) {
                    //btn_continue.setVisibility(View.GONE);
                    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                    getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close_white_24dp);
                }

//                else if (from != null && from.equals("other")) {
//                    findViewById(R.id.tv_next).setVisibility(View.VISIBLE);
//                    findViewById(R.id.tv_next).setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View view) {
//                            Intent intent = new Intent(AddSocialMediaAccountActivity.this, UpdateUserProfileActivity.class);
//                            startActivity(intent);
//                        }
//                    });
//                }
            }
        }

        // todo for instagram
        //setupForInstagram();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (bundle != null) {
            if (getIntent().getExtras().getString("from") != null) {
                String from = getIntent().getExtras().getString("from");
                if (from != null && !from.equals("menu")) {
                    MenuInflater inflater = getMenuInflater();
                    inflater.inflate(R.menu.custom_searchview, menu);
                }
            }
        } else {
            MenuInflater inflater = getMenuInflater();
            inflater.inflate(R.menu.custom_searchview, menu);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case android.R.id.home:
                AddSocialMediaAccountActivity.this.finish();
                return true;

            case R.id.skip:
                try {
                    modelLoggedUser.getData().setStatus(2);
                    qkPreferences.storeLoggedUser(modelLoggedUser);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Intent i = new Intent(this, ConfirmRegistration.class);
                this.startActivity(i);
                return true;
            case R.id.search:
                final SearchView searchView = (SearchView) MenuItemCompat.getActionView(item);
                searchView.setOnQueryTextListener(this);
                MenuItemCompat.setOnActionExpandListener(item,
                        new MenuItemCompat.OnActionExpandListener() {
                            @Override
                            public boolean onMenuItemActionCollapse(MenuItem item) {
                                adapter.setFilter(mlist);
                                return true;
                            }

                            @Override
                            public boolean onMenuItemActionExpand(MenuItem item) {
                                return true;
                            }
                        });
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        final List<ModelSocialMedia> filteredModelList = filter(mlist, newText);
        adapter.setFilter(filteredModelList);
        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    private List<ModelSocialMedia> filter(List<ModelSocialMedia> models, String query) {
        query = query.toLowerCase();
        final List<ModelSocialMedia> filteredModelList = new ArrayList<>();
        for (ModelSocialMedia model: models) {
            final String text = model.getName().toLowerCase();
            if (text.contains(query)) {
                filteredModelList.add(model);
            }
        }
        return filteredModelList;
    }


    @Override
    public void onClick(View view) {
        Intent intent = new Intent(AddSocialMediaAccountActivity.this, UpdateUserProfileActivity.class);
        startActivity(intent);
    }

    @Override
    public boolean onInterceptTouchEvent(RecyclerView view, MotionEvent e) {
        View childView = view.findChildViewUnder(e.getX(), e.getY());
        if (childView != null && mListener != null && mGestureDetector.onTouchEvent(e)) {
            mListener.onItemClick(childView, view.getChildAdapterPosition(childView));
        }
        return false;
    }

    @Override
    public void onTouchEvent(RecyclerView rv, MotionEvent e) {

    }

    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

    }

    public void onClickActiveAccount(String userid, final int social_media_id, final String status) {

        JSONObject obj = new JSONObject();
        if (userid.equalsIgnoreCase("") || userid.equalsIgnoreCase("null")) {
            userid = qkPreferences.getValuesInt(QKPreferences.USER_ID) + "";
        }
        try {
            obj.put("user_id", userid);
            obj.put("social_media_id", social_media_id);
            obj.put("status", status);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        new VolleyApiRequest(context, false, VolleyApiRequest.REQUEST_BASEURL + "api/social_network_status",
                new VolleyCallBack() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG, "SocialMediaResponce: " + response);
                        try {

                            if (mlist != null && mlist.size() > 0) {

                                for (int i = 0; i < mlist.size(); i++) {
                                    ModelSocialMedia modelSocialMedia = mlist.get(i);
                                    if (modelSocialMedia.getId() == social_media_id) {
                                        if (status.equals("1"))
                                            modelSocialMedia.setStatus("Active");
                                        else {
                                            modelSocialMedia.setStatus("Inactive");
                                            try {
                                                profileTracker.stopTracking();
                                                LoginManager.getInstance().logOut();
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                        }
                                        break;
                                    }
                                }
                                if (adapter != null)
                                    adapter.notifyDataSetChanged();

                                try {
                                    if (social_media_id == 5) {
                                        // google plus
                                        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(new ResultCallback<Status>() {
                                            @Override
                                            public void onResult(Status status) {
                                            }
                                        });
                                    }
                                    if (social_media_id == 3) {
                                        TwitterCore.getInstance().getSessionManager().clearActiveSession();
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }

                            ModelLoggedData modelLoggedData = modelLoggedUser.getData();
                            if (modelLoggedData != null && social_media_id == Integer.parseInt(com.utilities.Constants.SOCIAL_FACEBOOK_ID)) {
                                Facebook facebook = modelLoggedData.getFacebook();
                                facebook.setStatus(Integer.parseInt(status));

                                /*********
                                 * if status = 1 => socialmedia is integrated
                                 * if status = 0 => not integrated
                                 * status = 1= check public url ==> if (null){ open popup for email}else( go ahead)
                                 ****/
                                if (status.equalsIgnoreCase("1")) {
                                    try {
                                        if (modelLoggedUser.getData().getFacebook() != null && modelLoggedUser.getData().getFacebook().getStatus() == 1) {
                                            if (modelLoggedUser.getData().getFacebook().getData().getPublicProfileUrl() == null || modelLoggedUser.getData().getFacebook().getData().getPublicProfileUrl().equalsIgnoreCase("")) {
                                                openSocialMediaEmailDialog("1");
                                            }
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                } else if (status.equalsIgnoreCase("0")) {
                                    modelLoggedUser.getData().getFacebook().getData().setPublicProfileUrl("");
                                    qkPreferences.storeLoggedUser(modelLoggedUser);
                                }

                            } else if (modelLoggedData != null && social_media_id == Integer.parseInt(com.utilities.Constants.SOCIAL_LINKEDIN_ID)) {
                                Linkedin linkedin = modelLoggedData.getLinkedin();
                                linkedin.setStatus(Integer.parseInt(status));

                                if (status.equalsIgnoreCase("1")) {
                                    try {
                                        if (modelLoggedUser.getData().getLinkedin() != null && modelLoggedUser.getData().getLinkedin().getStatus() == 1) {
                                            if (modelLoggedUser.getData().getLinkedin().getData().getPublicProfileUrl() == null || modelLoggedUser.getData().getLinkedin().getData().getPublicProfileUrl().equalsIgnoreCase("")) {
                                                openSocialMediaEmailDialog("2");
                                            }
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                } else if (status.equalsIgnoreCase("0")) {
                                    modelLoggedUser.getData().getLinkedin().getData().setPublicProfileUrl("");
                                    qkPreferences.storeLoggedUser(modelLoggedUser);
                                }

                            } else if (modelLoggedData != null && social_media_id == Integer.parseInt(com.utilities.Constants.SOCIAL_TWITTER_ID)) {
                                Twitter twitter = modelLoggedData.getTwitter();
                                twitter.setStatus(Integer.parseInt(status));

                            } else if (modelLoggedData != null && social_media_id == Integer.parseInt(com.utilities.Constants.SOCIAL_SNAPCHAT)) {
                                Snapchat snapchat = modelLoggedData.getSnapchat();
                                snapchat.setStatus(Integer.parseInt(status));

                            } else if (modelLoggedData != null && social_media_id == Integer.parseInt(com.utilities.Constants.SOCIAL_GOOGLE_PLUS)) {
                                com.models.logindata.Google google = modelLoggedData.getGoogle();
                                google.setStatus(Integer.parseInt(status));
                            } else if (modelLoggedData != null && social_media_id == Integer.parseInt(com.utilities.Constants.SOCIAL_INSTAGRAM_ID)) {
                                Instagram instagram = modelLoggedData.getInstagram();
                                instagram.setStatus(Integer.parseInt(status));
                            }

                            modelLoggedUser.setData(modelLoggedData);

                            qkPreferences.storeLoggedUser(modelLoggedUser);

                            /*try {
                                AddSocialMediaAccountActivity.this.finish();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }*/

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyApiRequest.showVolleyError(context, error);
                    }
                }).enqueRequest(obj, Request.Method.POST);
    }

    // Twitter ModelLoggedData
    public void twitterMapping() {
        try {
            boolean isAppInstalled = Utilities.appInstalledOrNot(AddSocialMediaAccountActivity.this, TWITTER_PACKAGE_NAME);
            if (isAppInstalled) {
                getTwitterFromApp();
            } else {
                if (!isTwitterLoggedInAlready()) {
                    new TokenGet().execute();
                } else {
                    getDetailsForTwitter();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            if (!isTwitterLoggedInAlready()) {
                new TokenGet().execute();
            } else {
                getDetailsForTwitter();
            }
        }

    }

    private void getTwitterFromApp() {
        mTwitterAuthClient = new TwitterAuthClient();
        mTwitterAuthClient.authorize(AddSocialMediaAccountActivity.this, new Callback<TwitterSession>() {

            @Override
            public void success(Result<TwitterSession> twitterSessionResult) {
                // Success
                TwitterSession session = TwitterCore.getInstance().getSessionManager().getActiveSession();
                TwitterAuthToken authToken = session.getAuthToken();
                String token = authToken.token;
                String secret = authToken.secret;
                //getDetailsForTwitter();
                TwitterApiClient twitterApiClient = TwitterCore.getInstance().getApiClient();
                AccountService accountService = twitterApiClient.getAccountService();
                Call<com.twitter.sdk.android.core.models.User> call = accountService.verifyCredentials(true, true, true);
                call.enqueue(new Callback<com.twitter.sdk.android.core.models.User>() {
                    @Override
                    public void success(Result<com.twitter.sdk.android.core.models.User> result) {
                        //here we go User details
                        if (result != null && result.data != null) {
                            try {
                                JSONObject dataObject = new JSONObject();
                                dataObject.put("profile_id", result.data.id + "");
                                dataObject.put("name", result.data.name + "");
                                dataObject.put("favourite_count", result.data.favouritesCount + "");
                                dataObject.put("followers_count", result.data.followersCount + "");
                                dataObject.put("friends_count", result.data.friendsCount + "");
                                dataObject.put("screen_name", result.data.screenName);
                                //dataObject.put("favourites", new Gson().toJson(twitter.getFavorites(), ResponseList.class));
                                //dataObject.put("friends_list", new Gson().toJson(twitter.getFriendsList(user.getScreenName(), -1), PagableResponseList.class));
                                //dataObject.put("followers_list", new Gson().toJson(twitter.getFollowersList(user.getScreenName(), -1), PagableResponseList.class));
                                JSONObject jsonObject = new JSONObject();
                                jsonObject.put("user_id", userid);
                                jsonObject.put("social_media_id", Constants.SOCIAL_TWITTER_ID);
                                jsonObject.put("data", dataObject);
                                //sendToServerTwitter(jsonObject);
                                JSONObject data = jsonObject.getJSONObject("data");
                                String twit_uname = data.getString("name");
                                qkPreferences.storeTwitter_Uname(twit_uname);
                                //onClickActiveAccount(userid, Integer.parseInt(com.utilities.Constants.SOCIAL_TWITTER_ID), "1");
                                sendSocialMapData(jsonObject, Integer.parseInt(com.utilities.Constants.SOCIAL_TWITTER_ID), null);
                                Log.d(TAG, "getDetailsForTwitter: " + jsonObject);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            Toast.makeText(AddSocialMediaAccountActivity.this, "Failed to get information!", Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void failure(TwitterException exception) {
                        Toast.makeText(AddSocialMediaAccountActivity.this, "Failed to get information!", Toast.LENGTH_LONG).show();
                    }
                });
                //login(session);
            }

            @Override
            public void failure(TwitterException e) {
                e.printStackTrace();
                Toast.makeText(AddSocialMediaAccountActivity.this, "Authentication failed!", Toast.LENGTH_LONG).show();

            }
        });
    }

    public void getDetailsForTwitter() {

        ConfigurationBuilder cb = new ConfigurationBuilder();
        cb.setDebugEnabled(true)
                .setOAuthConsumerKey(getString(R.string.com_twitter_sdk_android_CONSUMER_KEY))
                .setOAuthConsumerSecret(getString(R.string.com_twitter_sdk_android_CONSUMER_SECRET))
                .setOAuthAccessToken(mSharedPreferences.getString(PREF_KEY_OAUTH_TOKEN, ""))
                .setOAuthAccessTokenSecret(mSharedPreferences.getString(PREF_KEY_OAUTH_SECRET, ""));
        TwitterFactory tf = new TwitterFactory(cb.build());
        twitter4j.Twitter twitter = tf.getInstance();

        User user;
        try {
            Log.d(TAG, "getDetailsForTwitter: " + mSharedPreferences.getString(PREF_KEY_OAUTH_SECRET, ""));

            long userId = new twitter4j.auth.AccessToken(mSharedPreferences.getString(PREF_KEY_OAUTH_TOKEN, ""), mSharedPreferences.getString(PREF_KEY_OAUTH_SECRET, "")).getUserId();
            user = twitter.showUser(userId);

            JSONObject dataObject = new JSONObject();
            dataObject.put("profile_id", user.getId());
            dataObject.put("name", user.getName());
            dataObject.put("favourite_count", user.getFavouritesCount());
            dataObject.put("followers_count", user.getFollowersCount());
            dataObject.put("friends_count", user.getFriendsCount());
            dataObject.put("screen_name", user.getScreenName());
            dataObject.put("favourites", new Gson().toJson(twitter.getFavorites(), ResponseList.class));
            dataObject.put("friends_list", new Gson().toJson(twitter.getFriendsList(user.getScreenName(), -1), PagableResponseList.class));
            dataObject.put("followers_list", new Gson().toJson(twitter.getFollowersList(user.getScreenName(), -1), PagableResponseList.class));

            Log.d("TAG", "doInBackground: " + dataObject);

            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("user_id", userid);
                jsonObject.put("social_media_id", Constants.SOCIAL_TWITTER_ID);
                jsonObject.put("data", dataObject);
                //sendToServerTwitter(jsonObject);

                JSONObject data = jsonObject.getJSONObject("data");
                String twit_uname = data.getString("name");
                qkPreferences.storeTwitter_Uname(twit_uname);

                //onClickActiveAccount(userid, Integer.parseInt(com.utilities.Constants.SOCIAL_TWITTER_ID), "1");

                sendSocialMapData(jsonObject, Integer.parseInt(com.utilities.Constants.SOCIAL_TWITTER_ID), null);

                Log.d(TAG, "getDetailsForTwitter: " + jsonObject);

            } catch (JSONException e) {
                e.printStackTrace();
            }

        } catch (twitter4j.TwitterException e) {
            Log.e("TAG", "getDetailsForTwitter: " + e.getErrorMessage());
        } catch (JSONException e) {
            Log.e("TAG", "getDetailsForTwitter: " + e.getLocalizedMessage());
        }
    }

    /**
     * Check user already logged in your application using twitter Login flag is
     * fetched from Shared Preferences
     */
    public boolean isTwitterLoggedInAlready() {
        // return twitter login status from Shared Preferences
        return mSharedPreferences.getBoolean(PREF_KEY_TWITTER_LOGIN, false);
    }

    private class TokenGet extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... args) {
            try {
                requestToken = twitter.getOAuthRequestToken();
                oauth_url = requestToken.getAuthorizationURL();
            } catch (twitter4j.TwitterException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            return oauth_url;
        }

        @Override
        protected void onPostExecute(String oauth_url) {
            if (oauth_url != null) {
                Log.e("URL", oauth_url);
                WebView web = auth_dialog.findViewById(R.id.webv);
                web.getSettings().setJavaScriptEnabled(true);
                web.loadUrl(oauth_url);
                web.setWebViewClient(new WebViewClient() {
                    boolean authComplete = false;

                    @Override
                    public void onPageStarted(WebView view, String url, Bitmap favicon) {
                        super.onPageStarted(view, url, favicon);
                    }

                    @Override
                    public void onPageFinished(WebView view, String url) {
                        super.onPageFinished(view, url);
                        if (url.contains(URL_TWITTER_OAUTH_VERIFIER) && !authComplete) {
                            authComplete = true;
                            Log.e("Url", url);
                            Uri uri = Uri.parse(url);
                            oauth_verifier = uri.getQueryParameter(URL_TWITTER_OAUTH_VERIFIER);
                            auth_dialog.dismiss();
                            new AccessTokenGet().execute();
                        } else if (url.contains("denied")) {
                            auth_dialog.dismiss();
                        }
                    }
                });
                auth_dialog.show();
                auth_dialog.setCancelable(true);
            } else {
                Log.d("TAG", "onPostExecute: Network Error");
                //                Toast.makeTex(), "Sorry !, Network Error or Invalid Credentials", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private class AccessTokenGet extends AsyncTask<String, String, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress.show();
        }

        @Override
        protected Boolean doInBackground(String... args) {
            try {
                accessToken = twitter.getOAuthAccessToken(requestToken, oauth_verifier);
                SharedPreferences.Editor edit = mSharedPreferences.edit();
                edit.putString(PREF_KEY_OAUTH_TOKEN, accessToken.getToken());
                edit.putString(PREF_KEY_OAUTH_SECRET, accessToken.getTokenSecret());
                edit.putBoolean(PREF_KEY_TWITTER_LOGIN, true);
                edit.apply();
            } catch (twitter4j.TwitterException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            return true;
        }

        @Override
        protected void onPostExecute(Boolean response) {
            if (response) {
                progress.hide();
                getDetailsForTwitter();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
        LISessionManager.getInstance(getApplicationContext()).onActivityResult(this, requestCode, resultCode, data);
        if (requestCode == SIGN_IN_GOOGLE) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }
        try {
            mTwitterAuthClient.onActivityResult(requestCode, resultCode, data);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void handleSignInResult(GoogleSignInResult result) {
        //If the login succeed
        if (result.isSuccess()) {
            try {
                //Getting google account
                final GoogleSignInAccount acct = result.getSignInAccount();
                //Displaying name and email

                if (acct != null) {
                    String name = acct.getDisplayName() + "";
                    String mail = acct.getEmail() + "";
                    String id = acct.getId() + "";

                    JSONObject object = new JSONObject();
                    JSONObject jsonObject = new JSONObject();
                    String userid = qkPreferences.getValuesInt(QKPreferences.USER_ID) + "";
                    try {
                        object.put("publicProfileUrl", "");
                        object.put("id", id + "");
                        object.put("username", name + "");
                        object.put("email", mail + "");
                        jsonObject.put("user_id", userid);
                        jsonObject.put("social_media_id", com.utilities.Constants.SOCIAL_GOOGLE_PLUS);
                        jsonObject.put("data", object);
                        sendSocialMapData(jsonObject, Integer.parseInt(com.utilities.Constants.SOCIAL_GOOGLE_PLUS), null);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            try {
                String eror = new Gson().toJson(result) + "\n" + result.getStatus() + "\n" + result + "";
                FirebaseDatabase.getInstance().getReference().push().setValue(eror);
            } catch (Exception e) {
                e.printStackTrace();
            }
            Toast.makeText(this, "Login Failed", Toast.LENGTH_LONG).show();
        }
    }

    //Facebook ModelLoggedData
    public void facebookMapping() {
        if (!Utilities.isNetworkConnected(this))
            Toast.makeText(this, com.utilities.Constants.NO_INTERNET_CONNECTION, Toast.LENGTH_LONG).show();
        else
            loginButton.performClick();
    }

    public void getFacebookUserData(AccessToken accessToken) {
        try {

            profileTracker = new ProfileTracker() {
                @Override
                protected void onCurrentProfileChanged(Profile oldProfile, Profile currentProfile) {
                    Profile profile = currentProfile;
                    if (profile != null) {

                        JSONObject objectProfile = new JSONObject();
                        try {
                            objectProfile.put("id", profile.getId());
                            objectProfile.put("name", profile.getName());
                            objectProfile.put("first_name", profile.getFirstName());
                            objectProfile.put("last_name", profile.getLastName());
                            objectProfile.put("link", profile.getLinkUri());

                            JSONObject jsonObject = new JSONObject();
                            try {

                                String link = profile.getLinkUri().toString();

                                if (link != null && !link.equalsIgnoreCase("") && !link.equalsIgnoreCase("null"))
                                    objectProfile.put("publicProfileUrl", link + "");
                                else
                                    objectProfile.put("publicProfileUrl", "");

                                jsonObject.put("user_id", userid);
                                jsonObject.put("social_media_id", com.utilities.Constants.SOCIAL_FACEBOOK_ID);
                                jsonObject.put("data", objectProfile);
                                sendSocialMapData(jsonObject, Integer.parseInt(com.utilities.Constants.SOCIAL_FACEBOOK_ID), null);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            };

            profileTracker.startTracking();


//            GraphRequest request = GraphRequest.newMeRequest(accessToken, new GraphRequest.GraphJSONObjectCallback() {
//                @Override
//                public void onCompleted(JSONObject object, GraphResponse response) {
//                    JSONObject jsonObject = new JSONObject();
//                    try {
//
//                        String link = object.optString("link");
//
//                        if (link != null && !link.equalsIgnoreCase("") && !link.equalsIgnoreCase("null"))
//                            object.put("publicProfileUrl", link + "");
//                        else
//                            object.put("publicProfileUrl", "");
//
//                        jsonObject.put("user_id", userid);
//                        jsonObject.put("social_media_id", com.utilities.Constants.SOCIAL_FACEBOOK_ID);
//                        jsonObject.put("data", object);
//                        sendSocialMapData(jsonObject, Integer.parseInt(com.utilities.Constants.SOCIAL_FACEBOOK_ID), null);
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }
//
//                }
//            });
//            Bundle parameters = new Bundle();
//            parameters.putString("fields", AppClass.facebook_requestdata);
//            request.setParameters(parameters);
//            request.executeAsync();

        } catch (FacebookException e) {
            e.printStackTrace();
        }
    }

    // Instagram
    public void InstagramMapping() {
        openDialogForInstagram();
        // todo uncomment below for start instagram connection
        //buttonInstagram.performClick();
    }

    private void openDialogForInstagram() {
        openSocialMediaEmailDialog("4");
    }

    // SnapChat
    public void SnapChatintegrating() {
        openSocialMediaEmailDialog("6");
    }

    // GooglePlus
    public void GoogleIntegration() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, SIGN_IN_GOOGLE);
    }

    private void setupForInstagram() {

        buttonInstagram = findViewById(R.id.button_instagram);
        mApp = new InstagramApp(this, Constants.IG_CLIENT_ID, Constants.IG_CLIENT_SECRET, Constants.IG_CALLBACK_URL);
        mApp.setListener(new InstagramApp.OAuthAuthenticationListener() {

            @Override
            public void onSuccess() {
                try {
                    String token = mApp.getTOken();
                    if (token != null) {
                        Log.d("Instagram Token", "" + token);
                        getInstagramUserDetail(token);

                    } else
                        Utilities.showToast(context, "Invalid Login, please try again later");

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFail(String error) {
                Utilities.showToast(context, "" + error);
            }
        });

        buttonInstagram.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                connectOrDisconnectUser();
            }
        });
    }

    private void connectOrDisconnectUser() {
        if (mApp.hasAccessToken()) {
            final AlertDialog.Builder builder = new AlertDialog.Builder(AddSocialMediaAccountActivity.this);
            builder.setMessage(getResources().getString(R.string.disconnect_from_instagram))
                    .setCancelable(false).setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    mApp.resetAccessToken();
                    buttonInstagram.setText(getResources().getString(R.string.connect_to_instagram));
                }
            }).setNegativeButton(getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.cancel();
                }
            });
            final AlertDialog alert = builder.create();
            alert.show();
        } else {
            mApp.authorize();
        }
    }

    public void getInstagramUserDetail(String token) {

        String url = com.utilities.Constants.INSTA_SELF_URL + token;
        new VolleyApiRequest(context, true, url, new VolleyCallBack() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("Instagram Token", "" + response);
                try {
                    if (response != null) {
                        JSONObject jsonObject = new JSONObject();
                        jsonObject.put("user_id", userid);
                        jsonObject.put("social_media_id", com.utilities.Constants.SOCIAL_INSTAGRAM_ID);
                        jsonObject.put("data", response.getJSONObject("data"));
                        sendSocialMapData(jsonObject, Integer.parseInt(com.utilities.Constants.SOCIAL_INSTAGRAM_ID), null);
                    } else
                        Utilities.showToast(context, "Invalid Login, please try again later");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyApiRequest.showVolleyError(context, error);
            }
        }).enqueRequest(null, Request.Method.GET);
    }

    // LinkedIn
    private static Scope buildScope() {
        return Scope.build(Scope.R_BASICPROFILE, Scope.R_EMAILADDRESS);
    }

    public void LinkedInMapping() {

        LISessionManager.getInstance(AddSocialMediaAccountActivity.this).init(AddSocialMediaAccountActivity.this, buildScope(), new AuthListener() {
            @Override
            public void onAuthSuccess() {
                final APIHelper apiHelper = APIHelper.getInstance(getApplicationContext());
                apiHelper.getRequest(AddSocialMediaAccountActivity.this, AppClass.LINKEDIN_URL, new ApiListener() {
                    @Override
                    public void onApiSuccess(ApiResponse apiResponse) {
                        Log.d("LinkedInMapping: ", "" + apiResponse.getResponseDataAsJson());
                        JSONObject jsonObject = new JSONObject();
                        try {
                            if (apiResponse != null) {
                                jsonObject.put("user_id", userid);
                                jsonObject.put("social_media_id", com.utilities.Constants.SOCIAL_LINKEDIN_ID);
                                jsonObject.put("data", apiResponse.getResponseDataAsJson());
                                sendSocialMapData(jsonObject, Integer.parseInt(com.utilities.Constants.SOCIAL_LINKEDIN_ID), null);
                            } else
                                Utilities.showToast(context, "Invalid Login, please try again later");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onApiError(LIApiError liApiError) {
                        Utilities.showToast(context, "" + liApiError.getMessage());
                    }
                });
            }

            @Override
            public void onAuthError(LIAuthError error) {
                Utilities.showToast(context, "" + error.toString());
            }
        }, true);
    }

    public void linkedInLoginWithoutApp() {

        ProgressDialog progressDialog = new ProgressDialog(this);
        LinkedinDialog d = new LinkedinDialog(this, progressDialog);
        d.show();
        d.setVerifierListener(new LinkedinDialog.OnVerifyListener() {
            @SuppressLint("NewApi")
            public void onVerify(String verifier) {
                try {
                    accessToken1 = LinkedinDialog.oAuthService.getOAuthAccessToken(LinkedinDialog.liToken, verifier);
                    factory.createLinkedInApiClient(accessToken1);
                    client = factory.createLinkedInApiClient(accessToken1);
                    Log.d("LinkedIn Token", "" + accessToken1.getToken());
                    Log.d("LinkedIn Secret", "" + accessToken1.getTokenSecret());
                    Log.d("LinkedIn Secret", client.getAccessToken().getTokenSecret());
                    Person p = client.getProfileForCurrentUser(EnumSet.of(
                            ProfileField.ID, ProfileField.FIRST_NAME,
                            ProfileField.PHONE_NUMBERS, ProfileField.LAST_NAME,
                            ProfileField.HEADLINE, ProfileField.INDUSTRY,
                            ProfileField.PICTURE_URL, ProfileField.PUBLIC_PROFILE_URL,
                            ProfileField.DATE_OF_BIRTH,
                            ProfileField.LOCATION_NAME, ProfileField.MAIN_ADDRESS,
                            ProfileField.LOCATION_COUNTRY));
                    Log.d("pppp", p + "");

                    JSONObject jsonObject = new JSONObject();

                    jsonObject.put("social_media_user_id", p.getId());
                    jsonObject.put("social_media_id", com.utilities.Constants.SOCIAL_LINKEDIN_ID);
                    jsonObject.put("user_id", userid);

                    JSONObject jsonLinkedinData = new JSONObject();

                    if (p.getPublicProfileUrl() != null)
                        jsonLinkedinData.put("publicProfileUrl", p.getPublicProfileUrl());

                    if (p.getFirstName() != null)
                        jsonLinkedinData.put("firstName", p.getFirstName());

                    if (p.getId() != null)
                        jsonLinkedinData.put("id", p.getId());
                    else
                        jsonLinkedinData.put("id", "");

                    if (p.getIndustry() != null)
                        jsonLinkedinData.put("industry", p.getIndustry());
                    else
                        jsonLinkedinData.put("industry", "");

                    if (p.getLastName() != null)
                        jsonLinkedinData.put("lastName", p.getLastName());
                    else
                        jsonLinkedinData.put("lastName", "");

                    JSONObject locationJson = new JSONObject();
                    if (p.getLocation() != null && p.getLocation().getName() != null)
                        locationJson.put("name", p.getLocation().getName());
                    else
                        locationJson.put("name", "");

                    JSONObject country = new JSONObject();
                    if (p.getLocation() != null && p.getLocation().getCountry() != null && p.getLocation().getCountry().getCode() != null)
                        country.put("code", p.getLocation().getCountry().getCode());
                    else
                        country.put("code", "");

                    locationJson.put("country", country);
                    jsonLinkedinData.put("location", locationJson);

                    jsonObject.put("data", jsonLinkedinData);

                    sendSocialMapData(jsonObject, Integer.parseInt(com.utilities.Constants.SOCIAL_LINKEDIN_ID), null);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(true);
        progressDialog.show();
    }

    private void sendSocialMapData(JSONObject jsonObject, final int socialMedia_id, final Dialog dialog) {
        try {
            new VolleyApiRequest(context, true, VolleyApiRequest.REQUEST_BASEURL + "api/social_media_map",
                    new VolleyCallBack() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.d("On Social Map: ", "" + socialMedia_id + " - " + response);
                            try {
                                if (response != null) {
                                    String message = response.optString("msg");
                                    String status = response.optString("status");
                                    Utilities.showToast(context, message);
                                    if (status.equalsIgnoreCase("200")) {
                                        if (dialog != null && dialog.isShowing()) {
                                            dialog.dismiss();
                                        }
                                    }
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            onClickActiveAccount(userid, socialMedia_id, "1");
                        }

                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Toast.makeText(context, "Please try again.", Toast.LENGTH_SHORT).show();
                        }
                    }).enqueRequest(jsonObject, Request.Method.POST);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void openSocialMediaEmailDialog(String mediaId) {

        final Dialog dialog = new Dialog(AddSocialMediaAccountActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View view = LinearLayout.inflate(AddSocialMediaAccountActivity.this, R.layout.dialog_addsocialmedia, null);
        dialog.setContentView(view);
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        dialog.show();
        dialog.getWindow().setAttributes(lp);
        TextView tvAlert = view.findViewById(R.id.tvAlert);
        TextView tvSubTitles = view.findViewById(R.id.tvSubTitles);
        TextView tvDone = view.findViewById(R.id.tvDone);
        TextView tvCancel = view.findViewById(R.id.tvCancel);
        CountryCodePicker ccp = (CountryCodePicker) view.findViewById(R.id.ccp);
        LinearLayout lin_cpp = view.findViewById(R.id.lin_cpp);
        String Url = "";
        String Title = "";
        String SocialMediaId = "";
        if (mediaId != null && mediaId.equalsIgnoreCase("1")) {
            Url = "https://www.facebook.com/";
            Title = "Add facebook url";
            SocialMediaId = "1";
            tvSubTitles.setVisibility(View.GONE);
        } else if (mediaId != null && mediaId.equalsIgnoreCase("2")) {
            Url = "https://www.linkedin.com/";
            Title = "Add LinkedIn URL";
            SocialMediaId = "2";
            tvSubTitles.setVisibility(View.GONE);
        } else if (mediaId != null && mediaId.equalsIgnoreCase("6")) {
            Url = "https://snapchat.com/add/";
            Title = "Add snapchat url";
            SocialMediaId = "6";
            tvSubTitles.setVisibility(View.VISIBLE);
            tvSubTitles.setText("Please insert username after url");
        } else if (mediaId != null && mediaId.equalsIgnoreCase("4")) {
            Url = "https://www.instagram.com/";
            Title = "Add instagram url";
            SocialMediaId = "4";
            tvSubTitles.setVisibility(View.VISIBLE);
            tvSubTitles.setText("Please insert username after url");
        }

        view.findViewById(R.id.divider_select_socialmedia).setVisibility(View.GONE);
        ccp.setVisibility(View.GONE);

        tvAlert.setText(Title + "");
        final SearchableSpinner spinnerSocialmedia = view.findViewById(R.id.spinnerSocialMedia);
        spinnerSocialmedia.setVisibility(View.GONE);
        final EditText edtSocialmediaLInk = view.findViewById(R.id.edt_socialmedia_link);
        edtSocialmediaLInk.setText(Url + "");
        Selection.setSelection(edtSocialmediaLInk.getText(), edtSocialmediaLInk.getText().length());
        final String finalUrl = Url;
        edtSocialmediaLInk.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!s.toString().startsWith(finalUrl + "")) {
                    edtSocialmediaLInk.setText(finalUrl + "");
                    Selection.setSelection(edtSocialmediaLInk.getText(), edtSocialmediaLInk.getText().length());
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        final String finalSocialMediaId = SocialMediaId;
        final String finalUrl1 = Url;
        tvDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    Utilities.hidekeyboard(AddSocialMediaAccountActivity.this, view);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (finalSocialMediaId.equalsIgnoreCase("1")) {
                    try {
                        Utilities.hidekeyboard(AddSocialMediaAccountActivity.this, view);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    dialog.dismiss();
                    if (!edtSocialmediaLInk.getText().toString().isEmpty() && isValidUrl(edtSocialmediaLInk.getText().toString()) && !edtSocialmediaLInk.getText().toString().equalsIgnoreCase(finalUrl1 + "")) {
                        String URL = edtSocialmediaLInk.getText().toString() + "";
                        UploadSocialmedia(URL + "", finalSocialMediaId + "", dialog);
                    } else {
                        Utilities.showToast(AddSocialMediaAccountActivity.this, "Please enter valid URL");
                    }
                } else if (finalSocialMediaId.equalsIgnoreCase("2")) {
                    try {
                        Utilities.hidekeyboard(AddSocialMediaAccountActivity.this, view);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    dialog.dismiss();
                    if (!edtSocialmediaLInk.getText().toString().isEmpty() && isValidUrl(edtSocialmediaLInk.getText().toString()) && !edtSocialmediaLInk.getText().toString().equalsIgnoreCase(finalUrl1 + "")) {
                        String URL = edtSocialmediaLInk.getText().toString() + "";
                        UploadSocialmedia(URL + "", finalSocialMediaId + "", dialog);
                    } else {
                        Utilities.showToast(AddSocialMediaAccountActivity.this, "Please enter valid URL");
                    }
                } else if (finalSocialMediaId.equalsIgnoreCase("6")) {

                    if (!edtSocialmediaLInk.getText().toString().isEmpty() && !edtSocialmediaLInk.getText().toString().equalsIgnoreCase(finalUrl1 + "")) {
                        JSONObject object = new JSONObject();
                        JSONObject jsonObject = new JSONObject();
                        String userid = qkPreferences.getValuesInt(QKPreferences.USER_ID) + "";
                        try {
                            String URL = edtSocialmediaLInk.getText().toString() + "";
                            object.put("publicProfileUrl", URL + "");

                            jsonObject.put("user_id", userid);
                            jsonObject.put("social_media_id", com.utilities.Constants.SOCIAL_SNAPCHAT);
                            jsonObject.put("data", object);
                            sendSocialMapData(jsonObject, Integer.parseInt(com.utilities.Constants.SOCIAL_SNAPCHAT), dialog);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else {
                        Toast.makeText(AddSocialMediaAccountActivity.this, "please enter valid username after the url.", Toast.LENGTH_SHORT).show();
                    }
                } else if (finalSocialMediaId.equalsIgnoreCase("4")) {

                    if (!edtSocialmediaLInk.getText().toString().isEmpty() && !edtSocialmediaLInk.getText().toString().equalsIgnoreCase(finalUrl1 + "")) {
                        JSONObject object = new JSONObject();
                        JSONObject jsonObject = new JSONObject();
                        String userid = qkPreferences.getValuesInt(QKPreferences.USER_ID) + "";
                        try {
                            String URL = edtSocialmediaLInk.getText().toString() + "";
                            object.put("publicProfileUrl", URL + "");

                            jsonObject.put("user_id", userid);
                            jsonObject.put("social_media_id", com.utilities.Constants.SOCIAL_INSTAGRAM_ID);
                            jsonObject.put("data", object);
                            sendSocialMapData(jsonObject, Integer.parseInt(com.utilities.Constants.SOCIAL_INSTAGRAM_ID), dialog);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else {
                        Toast.makeText(AddSocialMediaAccountActivity.this, "please enter valid username after the url.", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    Utilities.hidekeyboard(AddSocialMediaAccountActivity.this, view);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void UploadSocialmedia(String Url, String socialmedia_id, final Dialog dialog) {
        try {
            // socialmedia_id = 1 = facebook
            // socialmedia_id = 6 = snapchat
            final ProgressDialog pd = Utilities.showProgress(AddSocialMediaAccountActivity.this);
            String userid = qkPreferences.getValuesInt(QKPreferences.USER_ID) + "";
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("user_id", userid);
            jsonObject.put("social_media_id", socialmedia_id + "");
            jsonObject.put("publicProfileUrl", Url + "");
            new VolleyApiRequest(context, true, VolleyApiRequest.REQUEST_BASEURL + "api/update_social_profile_url",
                    new VolleyCallBack() {
                        @Override
                        public void onResponse(JSONObject response) {
                            if (pd != null && pd.isShowing()) {
                                pd.dismiss();
                            }
                            try {
                                if (response != null && response.length() > 0) {
                                    String ststus = response.getString("status");
                                    String message = response.getString("msg");

                                    if (ststus.equalsIgnoreCase("200")) {
                                        if (dialog != null && dialog.isShowing()) {
                                            dialog.dismiss();
                                        }
                                    } else
                                        Utilities.showToast(context, message + "");
                                } else

                                    Utilities.showToast(context, "Please try again");

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onErrorResponse(VolleyError error) {
                            if (pd != null && pd.isShowing()) {
                                pd.dismiss();
                            }
                        }
                    }).enqueRequest(jsonObject, Request.Method.POST);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
