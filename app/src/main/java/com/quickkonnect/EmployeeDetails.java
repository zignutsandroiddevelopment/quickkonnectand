package com.quickkonnect;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.activities.MatchProfileActivity;
import com.adapter.Adp_Emp_Phone;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.interfaces.EmployeeItemSelected;
import com.models.EmployeeView.Email;
import com.models.EmployeeView.EmployeeDetailView;
import com.models.EmployeeView.Fax;
import com.models.EmployeeView.Phone;
import com.models.EmployeeView.Profile;
import com.models.EmployeeView.Weblink;
import com.realmtable.MyProfiles;
import com.realmtable.RealmController;
import com.retrofitservice.ApiUtils;
import com.retrofitservice.SOService;
import com.squareup.picasso.Picasso;
import com.utilities.Constants;
import com.utilities.QKPreferences;
import com.utilities.Utilities;
import com.volleyapirequest.VolleyApiRequest;
import com.volleyapirequest.VolleyCallBack;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EmployeeDetails extends AppCompatActivity {

    private TextView employee_name, about, emp_number, emp_department, emp_valid_from, tv_compony_name, tv_compony_address;
    private LinearLayout lin_emp_phone, lin_emp_email, lin_emp_fax, lin_emp_web, lin_profile_unmatch_emp;
    private RelativeLayout lin_emp_company_detils;
    private ImageView emp_logo, img_company_logo_emp;
    private FrameLayout frameroot;
    private RecyclerView rec_phone, rec_web, rec_email, rec_fax;
    private Toolbar toolbar;
    private QKPreferences qkPreferences;
    private Context context;
    private SOService mService;
    private ProgressDialog progressDialog;
    private String login_user_id = "", profile_id = "", unique_code = "", cprofile = "", cunique = "", cuser = "", user_id = "";
    private Adp_Emp_Phone adapter;
    private ArrayList<String> phone_list;
    private ArrayList<String> email_list;
    private ArrayList<String> website_list;
    private ArrayList<String> fax_list;
    private EmployeeItemSelected employeeItemSelected;

    //for Employer scan
    private String company_id = "0", userId = "", scan_user_id = "";
    ;
    private boolean isUnmatch = false;

    private int EMP_PHONE = 1;
    private int EMP_EMAIL = 2;
    private int EMP_FAX = 3;
    private int EMP_WEBSITE = 4;

    String key_from = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        //getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        //this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_employee_details);


        if (Build.VERSION.SDK_INT >= 19 && Build.VERSION.SDK_INT < 21) {
            setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, true);
        }
        if (Build.VERSION.SDK_INT >= 19) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }
        if (Build.VERSION.SDK_INT >= 21) {
            setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, false);
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }

        context = EmployeeDetails.this;
        mService = ApiUtils.getSOService();
        qkPreferences = new QKPreferences(context);
        login_user_id = "" + qkPreferences.getValuesInt(QKPreferences.USER_ID);
        login_user_id = login_user_id.replace(".0", "");
        initView();

        userId = qkPreferences.getValuesInt(QKPreferences.USER_ID) + "";

        key_from = getIntent().getExtras().getString(Constants.KEY);

        if (key_from != null && key_from.equalsIgnoreCase(Constants.KEY_SCANQK)) {

            unique_code = getIntent().getExtras().getString("unique_code");
            company_id = getIntent().getExtras().getString("company_id");
            try {
                MyProfiles myProfiles = RealmController.with(EmployeeDetails.this).getMyProfielDataByProfileId(unique_code);
                if (myProfiles != null) {
                    profile_id = myProfiles.getId();
                    showDataOffline(true, 1, unique_code);
                } else if (login_user_id != null && Utilities.isNetworkConnected(EmployeeDetails.this)) {
                    fnScanUser(unique_code, true);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (key_from != null && key_from.equalsIgnoreCase(Constants.KEY_CONTACT_EMPLOYEER)) {
            try {
                profile_id = getIntent().getExtras().getString("profile_id");
                login_user_id = getIntent().getExtras().getString("user_id");
                scan_user_id = getIntent().getExtras().getString("scan_user_id");
            } catch (Exception e) {
                e.printStackTrace();
            }
            isUnmatch = true;
            try {
                MyProfiles myProfiles = RealmController.with(EmployeeDetails.this).getMyProfielDataByid(profile_id);
                if (myProfiles != null)
                    showDataOffline(true, 0, "");
                else if (login_user_id != null && Utilities.isNetworkConnected(EmployeeDetails.this))
                    CallApi(true);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            try {
                unique_code = getIntent().getExtras().getString("unique_code");
                profile_id = getIntent().getExtras().getString("profile_id");
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                MyProfiles myProfiles = RealmController.with(EmployeeDetails.this).getMyProfielDataByProfileId(unique_code);
                if (myProfiles != null)
                    showDataOffline(true, 0, "");
                else if (login_user_id != null && Utilities.isNetworkConnected(EmployeeDetails.this))
                    CallApi(true);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void fnScanUser(String unique_code, boolean isLoadershow) {
        try {
            if (Utilities.isNetworkConnected(EmployeeDetails.this)) {
                JSONObject obj = new JSONObject();
                try {
                    obj.put("unique_code", unique_code);
                    obj.put("user_id", login_user_id);
                    obj.put("timezone", "" + TimeZone.getDefault().getID());
                    obj.put("latitude", qkPreferences.getLati());
                    obj.put("longitude", qkPreferences.getLongi());
                    obj.put("company_id", company_id + "");
                    obj.put("device_type", "Android");
                    String model = Build.MODEL;
                    int version = Build.VERSION.SDK_INT;
                    if (model.isEmpty())
                        obj.put("model", "z-404");
                    else
                        obj.put("model", model + "");
                    obj.put("version", version + "");
                } catch (Exception e) {
                    e.printStackTrace();
                }
                new VolleyApiRequest(EmployeeDetails.this, isLoadershow, VolleyApiRequest.REQUEST_BASEURL + "api/scan_user_store",
                        new VolleyCallBack() {
                            @Override
                            public void onResponse(JSONObject response) {
                                try {
                                    Log.e("Response", "" + response);
                                    EmployeeDetailView employeeDetailView = new Gson().fromJson(response.toString(), EmployeeDetailView.class);
                                    if (employeeDetailView != null) {
                                        if (employeeDetailView.getStatus() == 200)
                                            parseData(employeeDetailView);
                                        else
                                            Utilities.showToast(context, employeeDetailView.getMsg());
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }

                            @Override
                            public void onErrorResponse(VolleyError error) {
                                VolleyApiRequest.showVolleyError(EmployeeDetails.this, error);
                            }
                        }).enqueRequest(obj, Request.Method.POST);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void setWindowFlag(Activity activity, final int bits, boolean on) {
        Window win = activity.getWindow();
        WindowManager.LayoutParams winParams = win.getAttributes();
        if (on) {
            winParams.flags |= bits;
        } else {
            winParams.flags &= ~bits;
        }
        win.setAttributes(winParams);
    }

    private void CallApi(boolean isRefresh) {
        if (isRefresh)
            progressDialog = Utilities.showProgress(EmployeeDetails.this);

        String token = "";
        if (qkPreferences != null) {
            token = qkPreferences.getApiToken() + "";
        }

        Map<String, String> params = new HashMap<>();
        params.put("Authorization", "Bearer " + token + "");
        params.put("Accept", "application/json");
        params.put("QK_ACCESS_KEY", getResources().getString(R.string.QK_ACCESS_KEY) + "");

        mService.getEmployeeInfo(params, profile_id + "", login_user_id + "", "4").enqueue(new Callback<EmployeeDetailView>() {
            @Override
            public void onResponse(Call<EmployeeDetailView> call, Response<EmployeeDetailView> response) {
                if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();
                if (response != null && response.body() != null) {
                    EmployeeDetailView employeeDetailView = response.body();
                    //parseData(employeeDetailView);
                    if (employeeDetailView != null && employeeDetailView.getData() != null && employeeDetailView.getData().getUniqueCode() != null && !employeeDetailView.getData().getUniqueCode().equalsIgnoreCase("null") && !employeeDetailView.getData().getUniqueCode().equalsIgnoreCase(""))
                        getDataFromUniqueCode(employeeDetailView, employeeDetailView.getData().getUniqueCode());
                    else
                        parseData(employeeDetailView);
                } else
                    Log.e("Error", "Error" + "\n" + response.body());
            }

            @Override
            public void onFailure(Call<EmployeeDetailView> call, Throwable t) {
                Log.e("Error", "Error" + "\n" + t.getLocalizedMessage());
                if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();
            }
        });
    }

    private void getDataFromUniqueCode(EmployeeDetailView employeeDetailView, String unique_code) {
        try {
            if (employeeDetailView != null && employeeDetailView.getStatus().equals(200)) {

                MyProfiles myProfiles = new MyProfiles();
                myProfiles.setId("" + employeeDetailView.getData().getProfile().getId());
                myProfiles.setType("" + employeeDetailView.getData().getType());
                myProfiles.setName(employeeDetailView.getData().getUsername());
                myProfiles.setLogo(employeeDetailView.getData().getProfilePic());
                myProfiles.setUnique_code(employeeDetailView.getData().getUniqueCode());

                myProfiles.setEmployee_no(employeeDetailView.getData().getEmployeeNo());
                myProfiles.setDepartment(employeeDetailView.getData().getDepartment());
                myProfiles.setDesignation(employeeDetailView.getData().getDesignation());
                myProfiles.setValid_to(employeeDetailView.getData().getValidTo());
                myProfiles.setValid_from(employeeDetailView.getData().getValidFrom());

                String profile = new Gson().toJson(employeeDetailView.getData().getProfile());
                myProfiles.setProfile(profile);

                String qkinfo = new Gson().toJson(employeeDetailView.getData().getQkTagInfo());
                myProfiles.setQktaginfo(qkinfo);

                String social_phone = new Gson().toJson(employeeDetailView.getData().getPhone());
                myProfiles.setPhone(social_phone);

                String social_fax = new Gson().toJson(employeeDetailView.getData().getFax());
                myProfiles.setFax(social_fax);

                String social_email = new Gson().toJson(employeeDetailView.getData().getEmail());
                myProfiles.setEmail(social_email);

                String social_weblink = new Gson().toJson(employeeDetailView.getData().getWeblink());
                myProfiles.setWeblink(social_weblink);

                RealmController.with(EmployeeDetails.this).updateMyProfiles(myProfiles);

                showDataOffline(false, 0, "");

            } else {
                Utilities.showToast(getApplicationContext(), employeeDetailView.getMsg());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            MyProfiles myProfiles = RealmController.with(EmployeeDetails.this).getMyProfielDataByProfileId(unique_code);
            if (myProfiles != null) {
                profile_id = myProfiles.getId() + "";
                // Company Logo
                String profile_pic = myProfiles.getLogo();
                if (profile_pic != null && Utilities.isEmpty(profile_pic)) {
                    Picasso.with(EmployeeDetails.this)
                            .load(profile_pic)
                            .error(R.drawable.ic_default_profile_photo)
                            .transform(new CircleTransform())
                            .placeholder(R.drawable.ic_default_profile_photo)
                            .fit()
                            .into(emp_logo);
                } else {
                    emp_logo.setImageResource(R.drawable.ic_default_profile_photo);
                }

                if (isUnmatch) {
                    lin_profile_unmatch_emp.setVisibility(View.VISIBLE);
                    lin_profile_unmatch_emp.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            UnmatchContact();
                        }
                    });
                } else {
                    lin_profile_unmatch_emp.setVisibility(View.GONE);
                }

                String cpmpony_logo = "";
                String cpmpony_name = "";
                String cpmpony_address = "";
                try {
                    JSONObject jsonObject = new JSONObject(myProfiles.getProfile());
                    if (jsonObject.getString("name") != null)
                        cpmpony_name = jsonObject.getString("name");
                    if (jsonObject.getString("logo") != null)
                        cpmpony_logo = jsonObject.getString("logo");
                    if (jsonObject.getString("address") != null)
                        cpmpony_address = jsonObject.getString("address");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (cpmpony_logo != null && Utilities.isEmpty(cpmpony_logo)) {
                    Picasso.with(EmployeeDetails.this)
                            .load(cpmpony_logo)
                            .error(R.drawable.ic_logo)
                            .transform(new CircleTransform())
                            .placeholder(R.drawable.ic_logo)
                            .fit()
                            .into(img_company_logo_emp);
                } else {
                    img_company_logo_emp.setImageResource(R.drawable.ic_logo);
                }

                // compony Name
                if (cpmpony_name != null || !cpmpony_name.equalsIgnoreCase(""))
                    tv_compony_name.setText(cpmpony_name + "");
                else
                    tv_compony_name.setVisibility(View.GONE);

                // compony address
                if (cpmpony_address != null || !cpmpony_address.equalsIgnoreCase(""))
                    tv_compony_address.setText(cpmpony_address + "");
                else
                    tv_compony_address.setVisibility(View.GONE);

                // Name
                if (myProfiles.getName() != null || !myProfiles.getName().equalsIgnoreCase(""))
                    employee_name.setText(myProfiles.getName() + "");
                else
                    employee_name.setVisibility(View.GONE);

                // employee number
                if (myProfiles.getEmployee_no() != null || !myProfiles.getEmployee_no().equalsIgnoreCase(""))
                    emp_number.setText("( " + myProfiles.getEmployee_no() + " )");
                else
                    emp_number.setVisibility(View.GONE);

                if (emp_number.getText().toString().equalsIgnoreCase("(  )")) {
                    emp_number.setVisibility(View.GONE);
                }

                // employee department designation
                if (myProfiles.getDepartment() != null || !myProfiles.getDepartment().equalsIgnoreCase("")) {
                    emp_department.setText(myProfiles.getDepartment() + " , " + myProfiles.getDesignation());

                } else
                    emp_department.setVisibility(View.GONE);


                if (!emp_department.getText().toString().isEmpty()) {
                    try {
                        String emp = emp_department.getText().toString().charAt(1) + "";
                        String str = emp_department.getText().toString() + "";
                        if (emp != null && emp.equalsIgnoreCase(","))
                            emp_department.setText(str.substring(3, str.length()));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                // employee valid to
                String dateformate = "";
                if (myProfiles.getValid_to() != null || !myProfiles.getValid_to().equalsIgnoreCase("")) {
                    try {
                        dateformate = Utilities.changeDateFormat("dd/MM/yyyy", "MMM yyyy", myProfiles.getValid_to() + "");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    emp_valid_from.setText("Valid upto : " + dateformate);
                } else
                    emp_valid_from.setVisibility(View.GONE);


                if (emp_valid_from.getText().toString().equalsIgnoreCase("Valid upto : ")) {
                    emp_valid_from.setVisibility(View.GONE);
                }


                // Phone
                if (myProfiles.getPhone().equalsIgnoreCase("") || myProfiles.getPhone() == null || myProfiles.getPhone().equalsIgnoreCase("[]")) {
                    lin_emp_phone.setVisibility(View.GONE);
                } else {
                    lin_emp_phone.setVisibility(View.VISIBLE);
                    Type type = new TypeToken<List<Phone>>() {
                    }.getType();
                    List<Phone> phoneModel = new Gson().fromJson(myProfiles.getPhone(), type);
                    if (phoneModel != null) {
                        phone_list = new ArrayList<>();
                        for (int i = 0; i < phoneModel.size(); i++) {
                            phone_list.add(phoneModel.get(i).getName() + "");
                        }
                        rec_phone.setHasFixedSize(true);
                        rec_phone.setLayoutManager(new LinearLayoutManager(EmployeeDetails.this));
                        adapter = new Adp_Emp_Phone(context, phone_list, EMP_PHONE);
                        rec_phone.setAdapter(adapter);
                    } else {
                        lin_emp_phone.setVisibility(View.GONE);
                    }
                }


                // email
                if (myProfiles.getEmail().equalsIgnoreCase("") || myProfiles.getEmail() == null || myProfiles.getEmail().equalsIgnoreCase("[]")) {
                    lin_emp_email.setVisibility(View.GONE);
                } else {
                    lin_emp_email.setVisibility(View.VISIBLE);
                    Type type = new TypeToken<List<Email>>() {
                    }.getType();
                    List<Email> phoneModel = new Gson().fromJson(myProfiles.getEmail(), type);
                    if (phoneModel != null) {
                        email_list = new ArrayList<>();
                        for (int i = 0; i < phoneModel.size(); i++) {
                            email_list.add(phoneModel.get(i).getName() + "");
                        }
                        rec_email.setHasFixedSize(true);
                        rec_email.setLayoutManager(new LinearLayoutManager(EmployeeDetails.this));
                        adapter = new Adp_Emp_Phone(context, email_list, EMP_EMAIL);
                        rec_email.setAdapter(adapter);
                    } else {
                        lin_emp_email.setVisibility(View.GONE);
                    }
                }

                // fax
                if (myProfiles.getFax().equalsIgnoreCase("") || myProfiles.getFax() == null || myProfiles.getFax().equalsIgnoreCase("[]")) {
                    lin_emp_fax.setVisibility(View.GONE);
                } else {
                    lin_emp_fax.setVisibility(View.VISIBLE);
                    Type type = new TypeToken<List<Fax>>() {
                    }.getType();
                    List<Fax> phoneModel = new Gson().fromJson(myProfiles.getFax(), type);
                    if (phoneModel != null) {
                        fax_list = new ArrayList<>();
                        for (int i = 0; i < phoneModel.size(); i++) {
                            fax_list.add(phoneModel.get(i).getName() + "");
                        }
                        rec_fax.setHasFixedSize(true);
                        rec_fax.setLayoutManager(new LinearLayoutManager(EmployeeDetails.this));
                        adapter = new Adp_Emp_Phone(context, fax_list, EMP_FAX);
                        rec_fax.setAdapter(adapter);
                    } else {
                        lin_emp_fax.setVisibility(View.GONE);
                    }
                }

                // web
                if (myProfiles.getWeblink().equalsIgnoreCase("") || myProfiles.getWeblink() == null || myProfiles.getWeblink().equalsIgnoreCase("[]")) {
                    lin_emp_web.setVisibility(View.GONE);
                } else {
                    lin_emp_web.setVisibility(View.VISIBLE);
                    Type type = new TypeToken<List<Weblink>>() {
                    }.getType();
                    List<Weblink> phoneModel = new Gson().fromJson(myProfiles.getWeblink(), type);
                    if (phoneModel != null) {
                        website_list = new ArrayList<>();
                        for (int i = 0; i < phoneModel.size(); i++) {
                            website_list.add(phoneModel.get(i).getName() + "");
                        }
                        rec_web.setHasFixedSize(true);
                        rec_web.setLayoutManager(new LinearLayoutManager(EmployeeDetails.this));
                        adapter = new Adp_Emp_Phone(context, website_list, EMP_WEBSITE);
                        rec_web.setAdapter(adapter);
                    } else {
                        lin_emp_web.setVisibility(View.GONE);
                    }
                }

                if (myProfiles.getProfile() != null) {
                    Profile profile = new Gson().fromJson(myProfiles.getProfile(), Profile.class);
                    {
                        if (profile != null) {
                            cprofile = profile.getId() + "";
                            cunique = profile.getUniqueCode() + "";
                            cuser = login_user_id;
                        }
                    }
                }

                /*if (isRealodData) {
                    if (which == 1)
                        fnScanUser(uniquecode, false);
                    else
                        CallApi(false);
                }*/
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void parseData(EmployeeDetailView employeeDetailView) {

        try {
            if (employeeDetailView != null && employeeDetailView.getStatus().equals(200)) {

                MyProfiles myProfiles = new MyProfiles();
                myProfiles.setId("" + employeeDetailView.getData().getProfile().getId());
                myProfiles.setType("" + employeeDetailView.getData().getType());
                myProfiles.setName(employeeDetailView.getData().getUsername());
                myProfiles.setLogo(employeeDetailView.getData().getProfilePic());
                myProfiles.setUnique_code(employeeDetailView.getData().getUniqueCode());

                myProfiles.setEmployee_no(employeeDetailView.getData().getEmployeeNo());
                myProfiles.setDepartment(employeeDetailView.getData().getDepartment());
                myProfiles.setDesignation(employeeDetailView.getData().getDesignation());
                myProfiles.setValid_to(employeeDetailView.getData().getValidTo());
                myProfiles.setValid_from(employeeDetailView.getData().getValidFrom());

                String profile = new Gson().toJson(employeeDetailView.getData().getProfile());
                myProfiles.setProfile(profile);

                String qkinfo = new Gson().toJson(employeeDetailView.getData().getQkTagInfo());
                myProfiles.setQktaginfo(qkinfo);

                String social_phone = new Gson().toJson(employeeDetailView.getData().getPhone());
                myProfiles.setPhone(social_phone);

                String social_fax = new Gson().toJson(employeeDetailView.getData().getFax());
                myProfiles.setFax(social_fax);

                String social_email = new Gson().toJson(employeeDetailView.getData().getEmail());
                myProfiles.setEmail(social_email);

                String social_weblink = new Gson().toJson(employeeDetailView.getData().getWeblink());
                myProfiles.setWeblink(social_weblink);

                RealmController.with(EmployeeDetails.this).updateMyProfiles(myProfiles);

                showDataOffline(false, 0, "");

            } else {
                Utilities.showToast(getApplicationContext(), employeeDetailView.getMsg());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void showDataOffline(final boolean isRealodData, int which, String uniquecode) {
        try {
            final MyProfiles myProfiles;

            if (key_from != null && key_from.equalsIgnoreCase(Constants.KEY_CONTACT_EMPLOYEER))
                myProfiles = RealmController.with(EmployeeDetails.this).getMyProfielDataByid(profile_id);
            else
                myProfiles = RealmController.with(EmployeeDetails.this).getMyProfielDataByProfileId(unique_code);

            if (myProfiles != null) {
                profile_id = myProfiles.getId() + "";
                // Company Logo
                String profile_pic = myProfiles.getLogo();
                try {
                    if (profile_pic != null && Utilities.isEmpty(profile_pic)) {
                        Picasso.with(EmployeeDetails.this)
                                .load(profile_pic)
                                .error(R.drawable.ic_default_profile_photo)
                                .transform(new CircleTransform())
                                .placeholder(R.drawable.ic_default_profile_photo)
                                .fit()
                                .into(emp_logo);
                    } else {
                        emp_logo.setImageResource(R.drawable.ic_default_profile_photo);
                    }

                    if (isUnmatch) {
                        lin_profile_unmatch_emp.setVisibility(View.VISIBLE);
                        lin_profile_unmatch_emp.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                UnmatchContact();
                            }
                        });
                    } else {
                        lin_profile_unmatch_emp.setVisibility(View.GONE);
                    }

                    String cpmpony_logo = "";
                    String cpmpony_name = "";
                    String cpmpony_address = "";
                    try {
                        JSONObject jsonObject = new JSONObject(myProfiles.getProfile());
                        if (jsonObject.getString("name") != null)
                            cpmpony_name = jsonObject.getString("name");
                        if (jsonObject.getString("logo") != null)
                            cpmpony_logo = jsonObject.getString("logo");
                        if (jsonObject.getString("address") != null)
                            cpmpony_address = jsonObject.getString("address");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    if (cpmpony_logo != null && Utilities.isEmpty(cpmpony_logo)) {
                        Picasso.with(EmployeeDetails.this)
                                .load(cpmpony_logo)
                                .error(R.drawable.ic_logo)
                                .transform(new CircleTransform())
                                .placeholder(R.drawable.ic_logo)
                                .fit()
                                .into(img_company_logo_emp);
                    } else {
                        img_company_logo_emp.setImageResource(R.drawable.ic_logo);
                    }

                    // compony Name
                    if (cpmpony_name != null || !cpmpony_name.equalsIgnoreCase(""))
                        tv_compony_name.setText(cpmpony_name + "");
                    else
                        tv_compony_name.setVisibility(View.GONE);

                    // compony address
                    if (cpmpony_address != null || !cpmpony_address.equalsIgnoreCase(""))
                        tv_compony_address.setText(cpmpony_address + "");
                    else
                        tv_compony_address.setVisibility(View.GONE);

                    // Name
                    if (myProfiles.getName() != null || !myProfiles.getName().equalsIgnoreCase(""))
                        employee_name.setText(myProfiles.getName() + "");
                    else
                        employee_name.setVisibility(View.GONE);

                    // employee number
                    if (myProfiles.getEmployee_no() != null || !myProfiles.getEmployee_no().equalsIgnoreCase(""))
                        emp_number.setText("( " + myProfiles.getEmployee_no() + " )");
                    else
                        emp_number.setVisibility(View.GONE);

                    if (emp_number.getText().toString().equalsIgnoreCase("(  )")) {
                        emp_number.setVisibility(View.GONE);
                    }

                    // employee department designation
                    if (myProfiles.getDepartment() != null || !myProfiles.getDepartment().equalsIgnoreCase("")) {
                        emp_department.setText(myProfiles.getDepartment() + " , " + myProfiles.getDesignation());

                    } else
                        emp_department.setVisibility(View.GONE);


                    if (!emp_department.getText().toString().isEmpty()) {
                        try {
                            String emp = emp_department.getText().toString().charAt(1) + "";
                            String str = emp_department.getText().toString() + "";
                            if (emp != null && emp.equalsIgnoreCase(","))
                                emp_department.setText(str.substring(3, str.length()));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    // employee valid to
                    String dateformate = "";
                    if (myProfiles.getValid_to() != null || !myProfiles.getValid_to().equalsIgnoreCase("")) {
                        try {
                            dateformate = Utilities.changeDateFormat("dd/MM/yyyy", "MMM yyyy", myProfiles.getValid_to() + "");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        emp_valid_from.setText("Valid upto : " + dateformate);
                    } else
                        emp_valid_from.setVisibility(View.GONE);


                    if (emp_valid_from.getText().toString().equalsIgnoreCase("Valid upto : ")) {
                        emp_valid_from.setVisibility(View.GONE);
                    }


                    // Phone
                    if (myProfiles.getPhone().equalsIgnoreCase("") || myProfiles.getPhone() == null || myProfiles.getPhone().equalsIgnoreCase("[]")) {
                        lin_emp_phone.setVisibility(View.GONE);
                    } else {
                        lin_emp_phone.setVisibility(View.VISIBLE);
                        Type type = new TypeToken<List<Phone>>() {
                        }.getType();
                        List<Phone> phoneModel = new Gson().fromJson(myProfiles.getPhone(), type);
                        if (phoneModel != null) {
                            phone_list = new ArrayList<>();
                            for (int i = 0; i < phoneModel.size(); i++) {
                                phone_list.add(phoneModel.get(i).getName() + "");
                            }
                            rec_phone.setHasFixedSize(true);
                            rec_phone.setLayoutManager(new LinearLayoutManager(EmployeeDetails.this));
                            adapter = new Adp_Emp_Phone(context, phone_list, EMP_PHONE);
                            rec_phone.setAdapter(adapter);
                        } else {
                            lin_emp_phone.setVisibility(View.GONE);
                        }
                    }


                    // email
                    if (myProfiles.getEmail().equalsIgnoreCase("") || myProfiles.getEmail() == null || myProfiles.getEmail().equalsIgnoreCase("[]")) {
                        lin_emp_email.setVisibility(View.GONE);
                    } else {
                        lin_emp_email.setVisibility(View.VISIBLE);
                        Type type = new TypeToken<List<Email>>() {
                        }.getType();
                        List<Email> phoneModel = new Gson().fromJson(myProfiles.getEmail(), type);
                        if (phoneModel != null) {
                            email_list = new ArrayList<>();
                            for (int i = 0; i < phoneModel.size(); i++) {
                                email_list.add(phoneModel.get(i).getName() + "");
                            }
                            rec_email.setHasFixedSize(true);
                            rec_email.setLayoutManager(new LinearLayoutManager(EmployeeDetails.this));
                            adapter = new Adp_Emp_Phone(context, email_list, EMP_EMAIL);
                            rec_email.setAdapter(adapter);
                        } else {
                            lin_emp_email.setVisibility(View.GONE);
                        }
                    }

                    // fax
                    if (myProfiles.getFax().equalsIgnoreCase("") || myProfiles.getFax() == null || myProfiles.getFax().equalsIgnoreCase("[]")) {
                        lin_emp_fax.setVisibility(View.GONE);
                    } else {
                        lin_emp_fax.setVisibility(View.VISIBLE);
                        Type type = new TypeToken<List<Fax>>() {
                        }.getType();
                        List<Fax> phoneModel = new Gson().fromJson(myProfiles.getFax(), type);
                        if (phoneModel != null) {
                            fax_list = new ArrayList<>();
                            for (int i = 0; i < phoneModel.size(); i++) {
                                fax_list.add(phoneModel.get(i).getName() + "");
                            }
                            rec_fax.setHasFixedSize(true);
                            rec_fax.setLayoutManager(new LinearLayoutManager(EmployeeDetails.this));
                            adapter = new Adp_Emp_Phone(context, fax_list, EMP_FAX);
                            rec_fax.setAdapter(adapter);
                        } else {
                            lin_emp_fax.setVisibility(View.GONE);
                        }
                    }

                    // web
                    if (myProfiles.getWeblink().equalsIgnoreCase("") || myProfiles.getWeblink() == null || myProfiles.getWeblink().equalsIgnoreCase("[]")) {
                        lin_emp_web.setVisibility(View.GONE);
                    } else {
                        lin_emp_web.setVisibility(View.VISIBLE);
                        Type type = new TypeToken<List<Weblink>>() {
                        }.getType();
                        List<Weblink> phoneModel = new Gson().fromJson(myProfiles.getWeblink(), type);
                        if (phoneModel != null) {
                            website_list = new ArrayList<>();
                            for (int i = 0; i < phoneModel.size(); i++) {
                                website_list.add(phoneModel.get(i).getName() + "");
                            }
                            rec_web.setHasFixedSize(true);
                            rec_web.setLayoutManager(new LinearLayoutManager(EmployeeDetails.this));
                            adapter = new Adp_Emp_Phone(context, website_list, EMP_WEBSITE);
                            rec_web.setAdapter(adapter);
                        } else {
                            lin_emp_web.setVisibility(View.GONE);
                        }
                    }

                    if (myProfiles.getProfile() != null) {
                        Profile profile = new Gson().fromJson(myProfiles.getProfile(), Profile.class);
                        {
                            if (profile != null) {
                                cprofile = profile.getId() + "";
                                cunique = profile.getUniqueCode() + "";
                                cuser = login_user_id;
                            }
                        }
                    }

                    if (isRealodData) {
                        if (which == 1)
                            fnScanUser(uniquecode, false);
                        else
                            CallApi(false);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void UnmatchContact() {
        String username = employee_name.getText().toString();
        String message_unmatch = "Are you sure you want to unmatch " + username + "?";
        new AlertDialog.Builder(this)
                .setMessage(Html.fromHtml(message_unmatch))
                .setCancelable(false)
                .setPositiveButton("UNMATCH", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        CLick_On_Unmatch();
                    }
                })
                .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                })
                .show();
    }

    private void CLick_On_Unmatch() {

        JSONObject obj = new JSONObject();
        try {
            login_user_id = "" + qkPreferences.getValuesInt(QKPreferences.USER_ID);
            obj.put("user_id", login_user_id);
            obj.put("requested_user_id", scan_user_id);
            obj.put("status", "U");
            obj.put("timezone", "" + TimeZone.getDefault().getID());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.e("Param-UnMatch", obj.toString());
        new VolleyApiRequest(context, true, VolleyApiRequest.REQUEST_BASEURL + "api/user_contact_block_unmatch",
                new VolleyCallBack() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {

                            String message = response.getString("msg");
                            String status = response.optString("status");

                            Utilities.showToast(context, message);
                            if (status.equals("200")) {
                                setResult(RESULT_OK);
                                EmployeeDetails.this.finish();
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //VolleyApiRequest.showVolleyError(context, error);
                        Toast.makeText(context, "Please try again.", Toast.LENGTH_SHORT).show();
                    }
                }).enqueRequest(obj, Request.Method.POST);
    }


    private void initView() {

        toolbar = findViewById(R.id.toolbar_emp_detail);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close_white_24dp);
        getSupportActionBar().setTitle("");

        lin_emp_company_detils = findViewById(R.id.lin_emp_company_detils);
        lin_emp_phone = findViewById(R.id.lin_emp_phone);
        lin_emp_email = findViewById(R.id.lin_emp_email);
        lin_emp_fax = findViewById(R.id.lin_emp_fax);
        lin_emp_web = findViewById(R.id.lin_emp_web);
        lin_profile_unmatch_emp = findViewById(R.id.lin_profile_unmatch_emp);

        frameroot = findViewById(R.id.fram_employee_detail);
        emp_logo = findViewById(R.id.img_logo_emp);
        img_company_logo_emp = findViewById(R.id.img_company_logo_emp);
        emp_number = findViewById(R.id.ed_emp_number);
        emp_department = findViewById(R.id.ed_emp_department);
        emp_valid_from = findViewById(R.id.ed_emp_valid_from);
        employee_name = findViewById(R.id.tv_name_emp);
        about = findViewById(R.id.tv_about_emp);
        rec_phone = findViewById(R.id.rec_phone);
        rec_phone.addItemDecoration(new SimpleDividerItemDecoration(context));
        rec_fax = findViewById(R.id.rec_fax);
        rec_fax.addItemDecoration(new SimpleDividerItemDecoration(context));
        rec_email = findViewById(R.id.rec_email);
        rec_email.addItemDecoration(new SimpleDividerItemDecoration(context));
        rec_web = findViewById(R.id.rec_web);
        rec_web.addItemDecoration(new SimpleDividerItemDecoration(context));
        tv_compony_name = findViewById(R.id.tv_compony_name);
        tv_compony_address = findViewById(R.id.tv_compony_address);

        phone_list = new ArrayList<>();
        email_list = new ArrayList<>();
        website_list = new ArrayList<>();
        fax_list = new ArrayList<>();

        lin_emp_company_detils.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (cprofile != null && cunique != null) {
                    Intent intent = new Intent(EmployeeDetails.this, NewScanProfileActivity.class);
                    intent.putExtra("unique_code", "" + cunique);
                    intent.putExtra("profile_id", "" + cprofile);
                    intent.putExtra("user_id", "" + qkPreferences.getValuesInt(QKPreferences.USER_ID));
                    intent.putExtra(Constants.KEY, "");
                    startActivity(intent);
                }
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

}
