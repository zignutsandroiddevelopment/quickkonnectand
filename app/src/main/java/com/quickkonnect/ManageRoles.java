package com.quickkonnect;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.fontinator.FontTextView;
import com.google.code.linkedinapi.schema.Role;
import com.google.gson.Gson;
import com.models.BasicDetail;
import com.realmtable.Employees;
import com.realmtable.RealmController;
import com.realmtable.Roles;
import com.squareup.picasso.Picasso;
import com.utilities.Utilities;
import com.volleyapirequest.VolleyApiRequest;
import com.volleyapirequest.VolleyCallBack;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmResults;

public class ManageRoles extends AppCompatActivity {

    private Toolbar toolbar;
    String Key = "", UserID = "", UserDetails = "", id = "", mlistdata = "", notificationid = "", role = "", role_name = "";
    private boolean isEditinfo = false;
    private boolean isAcceptReject = false;
    private boolean isNotiAR = false;
    TextView submit, name, about;
    ImageView profile, close;
    private ArrayList<PojoClasses.RoleList> mlist;
    int selection = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_manage_roles);

        toolbar = findViewById(R.id.toolbar_manage_role);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");

        try {
            Bundle b = getIntent().getExtras();
            Key = b.getString("KEY");
            UserID = b.getString("USER_ID");
            role = b.getString("ROLE_ID");
            role_name = b.getString("ROLE");

            if (Key.equalsIgnoreCase("EDIT")) {
                mlistdata = b.getString("USER_BASIC_DETAIL");
                isEditinfo = true;
            } else if (Key.equalsIgnoreCase("NOTIFICATION")) {
                isAcceptReject = true;
                notificationid = b.getString("notification_id");
            } else if (Key.equalsIgnoreCase("NOTIFICATION_AR")) {
                isAcceptReject = true;
                isNotiAR = true;
                notificationid = b.getString("notification_id");
            } else {
                UserDetails = b.getString("USER_BASIC_DETAIL");
                isEditinfo = false;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        initView();
    }

    private void initView() {
        submit = findViewById(R.id.tv_submit_manage_role);
        profile = findViewById(R.id.img_profile_manage_role);
        close = findViewById(R.id.img_close_manage_roles);
        about = findViewById(R.id.tv_about_manage_role);
        name = findViewById(R.id.tv_name_manage_role);

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent returnIntent = new Intent();
                setResult(AppCompatActivity.RESULT_OK, returnIntent);
                finish();
            }
        });

        mlist = new ArrayList<>();
      /*  if (Utilities.isNetworkConnected(ManageRoles.this)) {
            getRoleList();
        } else {*/
        LoadRoleList();
        //}
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SubmitData();
            }
        });

        if (isEditinfo) {
            submit.setText("Submit");
            LoadData();
        } else {
            submit.setText("Submit");
            getData();
        }
    }

    private void getRoleList() {
        try {
            new VolleyApiRequest(ManageRoles.this, true, VolleyApiRequest.REQUEST_BASEURL + "api/profile/role_list",
                    new VolleyCallBack() {
                        @Override
                        public void onResponse(JSONObject response) {
                            parseResponse(response);
                        }

                        @Override
                        public void onErrorResponse(VolleyError error) {
                            VolleyApiRequest.showVolleyError(ManageRoles.this, error);
                        }
                    }).enqueRequest(null, Request.Method.POST);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void parseResponse(JSONObject response) {
        try {
            String responseStatus = response.getString("status");
            if (responseStatus.equals("200")) {
                RealmController.with(ManageRoles.this).clearRoles();
                JSONArray jsnArr = response.getJSONArray("data");
                if (jsnArr != null && jsnArr.length() > 0) {
                    for (int i = 0; i < jsnArr.length(); i++) {
                        JSONObject obj = jsnArr.getJSONObject(i);

                        Roles role = new Roles();
                        role.setId(obj.getString("id"));
                        role.setName(obj.getString("name"));
                        role.setDescription(obj.getString("description"));

                        RealmController.with(ManageRoles.this).updateRoles(role);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        LoadRoleList();
    }

    private void LoadRoleList() {
        try {
            RealmResults<Roles> roles = RealmController.with(ManageRoles.this).getRoles();
            if (roles != null && roles.size() > 0) {
                try {
                    for (Roles foloo1 : roles) {
                        mlist.add(new PojoClasses.RoleList(
                                foloo1.getId(),
                                foloo1.getName(),
                                foloo1.getDescription()));
                    }

                    try {
                        for (int i = 0; i < mlist.size(); i++) {
                            if (role_name.equalsIgnoreCase(mlist.get(i).getName())) {
                                selection = i;
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    if (mlist != null && mlist.size() > 0)
                        getDataRadio();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                getRoleList();
            }
        } catch (Exception e) {
            getRoleList();
            e.printStackTrace();
        }
    }

    private void getDataRadio() {
        try {
            ViewGroup radiogroup = (ViewGroup) findViewById(R.id.radio_group);
            for (int i = 0; i < mlist.size(); i++) {
                RadioButton button = new RadioButton(this);
                RadioGroup.LayoutParams lpr = new RadioGroup.LayoutParams(RadioGroup.LayoutParams.WRAP_CONTENT, RadioGroup.LayoutParams.WRAP_CONTENT);
                button.setLayoutParams(lpr);
                button.setGravity(Gravity.TOP);
                String msg = "";
                String name = "";
                String desc = "";
                if (mlist.get(i).getName() != null && !mlist.get(i).getName().equalsIgnoreCase("null"))
                    name = mlist.get(i).getName() + "";

                if (mlist.get(i).getDescription() != null && !mlist.get(i).getDescription().equalsIgnoreCase("null"))
                    desc = mlist.get(i).getDescription() + "";

                msg = "<h1>" + name + "</h1><p>" + desc + "</p>";

                button.setText(Html.fromHtml(msg));
                button.setId(i);
                button.setChecked(i == selection);
                radiogroup.addView(button);
                button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        ((RadioGroup) view.getParent()).check(view.getId());
                        selection = view.getId();
                    }
                });
            }
        } catch (Exception e) {
            getRoleList();
            e.printStackTrace();
        }
    }

    private void SubmitData() {
        JSONObject obj = new JSONObject();
        try {
            obj.put("profile_id", UserID);
            obj.put("user_id", id);
            try {
                obj.put("role", mlist.get(selection).getId() + "");
            } catch (Exception e) {
                obj.put("role", selection + "");
                e.printStackTrace();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        new VolleyApiRequest(ManageRoles.this, true, VolleyApiRequest.REQUEST_BASEURL + "api/profile/assign_role",
                new VolleyCallBack() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String responseStatus = response.getString("status");
                            String msg = response.getString("msg");
                            Utilities.showToast(ManageRoles.this, msg);
                            if (responseStatus.equals("200")) {
                                Intent returnIntent = new Intent();
                                setResult(AppCompatActivity.RESULT_OK, returnIntent);
                                ManageRoles.this.finish();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyApiRequest.showVolleyError(ManageRoles.this, error);
                    }
                }).enqueRequest(obj, Request.Method.POST);
    }

    private void getData() {
        if (UserDetails != null && !UserDetails.equalsIgnoreCase("")) {
            BasicDetail basicDetail = new Gson().fromJson(UserDetails.toString(), BasicDetail.class);
            if (basicDetail != null && !basicDetail.toString().equalsIgnoreCase("null")) {
                if (basicDetail.getUserId() != null && !basicDetail.getUserId().toString().equalsIgnoreCase("null")) {
                    id = basicDetail.getUserId().toString();
                }
                if (basicDetail.getProfilePic() != null && !basicDetail.getProfilePic().isEmpty() && !basicDetail.getProfilePic().equals("null")) {
                    //user_dialog_profilepic = basicDetail.getProfilePic();
                    Picasso.with(ManageRoles.this)
                            .load(basicDetail.getProfilePic())
                            .error(R.drawable.ic_default_profile_photo)
                            .placeholder(R.drawable.ic_default_profile_photo)
                            .transform(new CircleTransform())
                            .fit()
                            .into(profile);
                } else {
                    profile.setImageResource(R.drawable.ic_default_profile_photo);
                }
                name.setText(basicDetail.getFirstname() + " " + basicDetail.getLastname());
                if (role_name != null && !role_name.equalsIgnoreCase("") && !role_name.equalsIgnoreCase("null"))
                    about.setText(role_name + "");
            }
        }
    }

    private void LoadData() {
        try {
            if (mlistdata != null && !mlistdata.equalsIgnoreCase("") && !mlistdata.equalsIgnoreCase("null")) {
                try {
                    PojoClasses.EmployeesList emp = new Gson().fromJson(mlistdata, PojoClasses.EmployeesList.class);
                    PojoClasses.EmployeesList.User usr = new Gson().fromJson(emp.getUser1(), PojoClasses.EmployeesList.User.class);
                    if (emp.getUserId() != null && !emp.getUserId().equalsIgnoreCase("null")) {
                        id = emp.getUserId() + "";
                    }

                    if (usr.getProfilePic() != null && !usr.getProfilePic().isEmpty() && !usr.getProfilePic().equals("null")) {
                        Picasso.with(ManageRoles.this)
                                .load(usr.getProfilePic())
                                .error(R.drawable.ic_default_profile_photo)
                                .placeholder(R.drawable.ic_default_profile_photo)
                                .transform(new CircleTransform())
                                .fit()
                                .into(profile);
                    } else {
                        profile.setImageResource(R.drawable.ic_default_profile_photo);
                    }
                    name.setText(usr.getFirstname() + " " + usr.getLastname());

                    if (role_name != null && !role_name.equalsIgnoreCase("") && !role_name.equalsIgnoreCase("null"))
                        about.setText(role_name + "");

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
