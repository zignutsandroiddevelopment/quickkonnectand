package com.quickkonnect;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.activities.UpdateBasicProfileActivity;
import com.adapter.FollowerAdapter;
import com.adapter.TransferProfileAdapter;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.interfaces.onTransferClick;
import com.l4digital.fastscroll.FastScrollRecyclerView;
import com.models.logindata.ModelLoggedData;
import com.models.logindata.ModelLoggedUser;
import com.realmtable.Followers;
import com.realmtable.RealmController;
import com.realmtable.TransferUser;
import com.utilities.QKPreferences;
import com.utilities.Utilities;
import com.volleyapirequest.VolleyApiRequest;
import com.volleyapirequest.VolleyCallBack;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;

import io.realm.Realm;
import io.realm.RealmResults;

public class TransferProfileActivity extends AppCompatActivity {

    private String userid, profileid;
    private Context context;
    private QKPreferences qkPreferences;
    private TransferProfileAdapter adapter;
    private ModelLoggedUser modelLoggedUser;
    private RecyclerView.LayoutManager mLayoutManager;
    private ArrayList<PojoClasses.TransferProfileList> mlist;
    FastScrollRecyclerView recyclerView;
    TextView no_recored_found;
    EditText edtContactSearch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Utilities.changeStatusbar(TransferProfileActivity.this, getWindow());
        setContentView(R.layout.activity_transfer_profile);

        Toolbar toolbar = findViewById(R.id.toolbar_transfer);
        //toolbar.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        toolbar.setBackgroundResource(R.drawable.toolbar_gradient);
        toolbar.setTitle("Transfer Profile");
        setSupportActionBar(toolbar);
        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close_white_24dp);
        } catch (Exception e) {
            e.printStackTrace();
        }
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TransferProfileActivity.this.finish();
            }
        });

        try {
            final Bundle extras = getIntent().getExtras();
            profileid = extras.getString("USER_ID");
        } catch (Exception e) {
            e.printStackTrace();
        }

        context = TransferProfileActivity.this;

        qkPreferences = new QKPreferences(context);
        userid = "" + qkPreferences.getLoggedUser().getData().getId();
        //modelLoggedUser = qkPreferences.getLoggedUser();

        recyclerView = findViewById(R.id.recyclerView_transfer);
        no_recored_found = findViewById(R.id.tv_no_record_found_transfer);
        edtContactSearch = findViewById(R.id.edtContactSearch_transfer);
        mLayoutManager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(mLayoutManager);
        edtContactSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (adapter != null) {
                    filter(editable.toString());
                }
            }
        });
        recyclerView.setAdapter(adapter);

        if (!Utilities.isNetworkConnected(context))
            getUsers();
        else
            getUserList();

    }

    private void filter(String text) {
        ArrayList<PojoClasses.TransferProfileList> filterdNames = new ArrayList<>();
        for (PojoClasses.TransferProfileList s : mlist) {
            if (s.getEmail().equalsIgnoreCase(text) || s.getName().equalsIgnoreCase(text) || s.getName().contains(text) || s.getName().contains(text.toLowerCase()) || s.getName().contains(text.toUpperCase()) || s.getEmail().contains(text.toLowerCase()) || s.getEmail().contains(text.toUpperCase())) {
                filterdNames.add(s);
            }
        }
        adapter.filterList(filterdNames);
    }

    private void getUsers() {

        RealmResults<TransferUser> followers = RealmController.with(TransferProfileActivity.this).getTransferUser();

        if (followers != null && followers.size() > 0) {
            mlist = new ArrayList<>();
            for (TransferUser foloo1 : followers) {
                mlist.add(new PojoClasses.TransferProfileList(
                        foloo1.getId(),
                        foloo1.getName(),
                        foloo1.getEmail(),
                        foloo1.getProfile_pic()));
            }

            try {
                Collections.sort(mlist, new Comparator<PojoClasses.TransferProfileList>() {
                    @Override
                    public int compare(PojoClasses.TransferProfileList transferProfileList, PojoClasses.TransferProfileList t1) {
                        String s1 = String.valueOf(transferProfileList.getName());
                        String s2 = String.valueOf(t1.getName());
                        return s1.compareToIgnoreCase(s2);
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
            adapter = new TransferProfileAdapter(TransferProfileActivity.this, mlist, onTransferClick);
            recyclerView.setAdapter(adapter);

        } else {
            getUserList();
        }
    }

    onTransferClick onTransferClick = new onTransferClick() {
        @Override
        public void onUserSelectedClick(final PojoClasses.TransferProfileList follow) {
            AlertDialog.Builder builder = new AlertDialog.Builder(TransferProfileActivity.this);
            builder.setTitle("Alert!");
            String msg = "";
            if (follow.getName() != null && !follow.getName().equalsIgnoreCase("") && !follow.getName().equalsIgnoreCase("null"))
                msg = "Are you sure want to transfer this profile to " + follow.getName() + " ?";
            else
                msg = "Are you sure want to transfer this profile?";

            builder.setMessage(msg);
            builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                    TransferProfile(follow.getId());
                }
            });
            builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });
            builder.show();
        }
    };

    private void TransferProfile(String id) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("profile_id", profileid);
            jsonObject.put("user_id", id);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        new VolleyApiRequest(context, true, VolleyApiRequest.REQUEST_BASEURL + "api/profile/transfer_request",
                new VolleyCallBack() {
                    @Override
                    public void onResponse(JSONObject response) {
                        String responseStatus = null;
                        try {
                            responseStatus = response.getString("status");
                            String msg = response.getString("msg");
                            if (responseStatus.equals("200")) {
                                Intent intent = new Intent();
                                intent.putExtra("Result", "Success");
                                setResult(RESULT_OK, intent);
                                finish();
                            } else {
                                Intent intent = new Intent();
                                intent.putExtra("Result", "Fail");
                                setResult(RESULT_OK, intent);
                                finish();
                            }
                            Utilities.showToast(getApplicationContext(), msg);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyApiRequest.showVolleyError(context, error);
                    }
                }).enqueRequest(jsonObject, Request.Method.POST);
    }

    private void getUserList() {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("user_id", profileid);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        new VolleyApiRequest(context, true, VolleyApiRequest.REQUEST_BASEURL + "api/qk_user_list",
                new VolleyCallBack() {
                    @Override
                    public void onResponse(JSONObject response) {
                        parseResponse(response);
                    }

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyApiRequest.showVolleyError(context, error);
                    }
                }).enqueRequest(jsonObject, Request.Method.POST);
    }

    public void parseResponse(JSONObject response) {
        try {
            String responseStatus = response.getString("status");
            if (responseStatus.equals("200")) {

                Realm realm = Realm.getDefaultInstance();
                realm.beginTransaction();
                realm.clear(Followers.class);
                realm.commitTransaction();

                JSONArray jsnArr = response.getJSONArray("users");
                if (jsnArr != null && jsnArr.length() > 0) {
                    mlist = new ArrayList<>();
                    for (int i = 0; i < jsnArr.length(); i++) {
                        JSONObject obj = jsnArr.getJSONObject(i);
                        TransferUser userlist = new TransferUser();

                        userlist.setId(obj.getString("id"));
                        userlist.setName(obj.getString("name"));
                        userlist.setEmail(obj.getString("email"));
                        userlist.setProfile_pic(obj.getString("profile_pic"));

                        RealmController.with(TransferProfileActivity.this).updateTrsnsferUserList(userlist);
                        getUsers();
                    }
                } else {
                    recyclerView.setVisibility(View.GONE);
                    no_recored_found.setVisibility(View.VISIBLE);
                }
            } else {
                recyclerView.setVisibility(View.GONE);
                no_recored_found.setVisibility(View.VISIBLE);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}
