package com.quickkonnect;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

import com.activities.MatchProfileActivity;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.utilities.Constants;
import com.utilities.QKPreferences;
import com.utilities.Utilities;
import com.volleyapirequest.VolleyApiRequest;
import com.volleyapirequest.VolleyCallBack;

import net.quikkly.android.Quikkly;
import net.quikkly.android.ui.ScanActivity;
import net.quikkly.core.ScanResult;

import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigInteger;

public class ScanQkTagActivity extends ScanActivity {

    boolean isScan = false;
    QKPreferences qkPreferences;
    String company_id = "";
    MediaPlayer mp;
    private static final int MY_CAMERA_REQUEST_CODE = 100;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Utilities.changeStatusbar(ScanQkTagActivity.this, getWindow());

        Quikkly.getDefaultInstance();

        isScan = true;

        try {
            company_id = getIntent().getExtras().getString("PROFILE_ID");
        } catch (Exception e) {
            e.printStackTrace();
        }

        qkPreferences = new QKPreferences(ScanQkTagActivity.this);
    }

    @Override
    public void onScanResult(@Nullable ScanResult result) {
        if (result != null && result.isEmpty() == false) {
            BigInteger scancode = null;
            for (int i = 0; i < result.tags.length; i++) {
                scancode = result.tags[i].getData();
            }
            if (isScan) {
                isScan = false;
                try {
                    Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        v.vibrate(VibrationEffect.createOneShot(500, VibrationEffect.DEFAULT_AMPLITUDE));
                    } else {
                        //deprecated in API 26
                        v.vibrate(500);
                    }
                    mp = MediaPlayer.create(this, R.raw.plucky);
                    mp.start();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                checkUserexist(scancode);
            }
        }
    }

    public void checkUserexist(final BigInteger unique_code) {

        JSONObject obj = new JSONObject();
        try {
            obj.put("unique_code", unique_code);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.e("Param-ScanUser", obj.toString());
        new VolleyApiRequest(ScanQkTagActivity.this, false, VolleyApiRequest.REQUEST_BASEURL + "api/is_user_exist",
                new VolleyCallBack() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String status = response.getString("status");
                            String message = response.getString("msg");
                            if (status.equals("200")) {
                                String user_type = response.optString("type");
                                String profile_id = response.optString("profile_id");

                                try {
                                    Intent intent = new Intent("BROADCAST_FROM_SCAN_QK");
                                    sendBroadcast(intent);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                                if (user_type.equals("0")) {
                                    Intent intent = new Intent(ScanQkTagActivity.this, MatchProfileActivity.class);
                                    intent.putExtra("unique_code", "" + unique_code);
                                    intent.putExtra("company_id", "" + company_id);
                                    intent.putExtra(Constants.KEY, Constants.KEY_SCANQK);
                                    startActivity(intent);
                                    ScanQkTagActivity.this.finish();
                                } else if (user_type.equals("1")) {
                                    //Intent intent = new Intent(ScanQkTagActivity.this, ScanProfileDetailActivity.class);
                                    Intent intent = new Intent(ScanQkTagActivity.this, NewScanProfileActivity.class);
                                    intent.putExtra("unique_code", "" + unique_code);
                                    intent.putExtra("profile_id", "" + profile_id);
                                    intent.putExtra("company_id", "" + company_id);
                                    intent.putExtra(Constants.KEY, Constants.KEY_SCANQK);
                                    startActivity(intent);
                                    ScanQkTagActivity.this.finish();
                                } else if (user_type.equals("2")) {
                                    Intent intent = new Intent(ScanQkTagActivity.this, EmployeeDetails.class);
                                    intent.putExtra("unique_code", "" + unique_code);
                                    intent.putExtra("company_id", "" + company_id);
                                    intent.putExtra(Constants.KEY, Constants.KEY_SCANQK);
                                    startActivity(intent);
                                    ScanQkTagActivity.this.finish();
                                }
                            } else {
                                Utilities.showToast(ScanQkTagActivity.this, message);
                                ScanQkTagActivity.this.finish();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyApiRequest.showVolleyError(ScanQkTagActivity.this, error);
                    }
                }).enqueRequest(obj, Request.Method.POST);
    }
}
