package com.quickkonnect;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class Select_QkImage extends AppCompatActivity {

    Button scan;
    ImageView qktag;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_qk_image);

        scan = findViewById(R.id.button_Scan);
        qktag = findViewById(R.id.img_tag);

        scan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                OpenProfile();
            }
        });
        
    }

    private void OpenProfile() {
    }

}
