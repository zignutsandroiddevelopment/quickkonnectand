package com.quickkonnect;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityOptions;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.Html;
import android.text.Selection;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.activities.ConfirmRegistration;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.code.linkedinapi.client.LinkedInApiClient;
import com.google.code.linkedinapi.client.LinkedInApiClientFactory;
import com.google.code.linkedinapi.client.enumeration.ProfileField;
import com.google.code.linkedinapi.client.oauth.LinkedInAccessToken;
import com.google.code.linkedinapi.schema.Person;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.interfaces.CallBackGetResult;
import com.interfaces.CallBackImageDownload;
import com.linkedin.LinkedinDialog;
import com.linkedin.platform.APIHelper;
import com.linkedin.platform.LISessionManager;
import com.linkedin.platform.errors.LIApiError;
import com.linkedin.platform.errors.LIAuthError;
import com.linkedin.platform.listeners.ApiListener;
import com.linkedin.platform.listeners.ApiResponse;
import com.linkedin.platform.listeners.AuthListener;
import com.linkedin.platform.utils.Scope;
import com.models.login.LoginPojo;
import com.models.logindata.ModelLoggedUser;
import com.retrofitservice.ApiUtils;
import com.retrofitservice.SOService;
import com.rilixtech.CountryCodePicker;
import com.services.BackgroundService;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;
import com.utilities.Constants;
import com.utilities.DownloadImage;
import com.utilities.QKPreferences;
import com.utilities.Utilities;
import com.volleyapirequest.VolleyApiRequest;
import com.volleyapirequest.VolleyCallBack;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.EnumSet;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.webkit.URLUtil.isValidUrl;
import static com.linkedin.Config.LINKEDIN_CONSUMER_KEY;
import static com.linkedin.Config.LINKEDIN_CONSUMER_SECRET;
import static com.linkedin.Config.LINKEDIN_PACKAGE_NAME;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private Context context;
    private EditText edt_email, edt_password;
    private TextView tv_register, tv_forgot_password, btn_Login, btn_Facebook, btn_LinkedIn_Login;
    private QKPreferences qkPreferences;
    private SOService mService;

    private CallbackManager callbackManager;

    private LoginButton loginButton;
    private String TAG = "TAG";
    final LinkedInApiClientFactory factory = LinkedInApiClientFactory.newInstance(LINKEDIN_CONSUMER_KEY, LINKEDIN_CONSUMER_SECRET);
    private LinkedInApiClient client;
    private LinkedInAccessToken accessToken = null;

    private String LoginSocialMediaType = "";

    ProfileTracker profileTracker;

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (profileTracker != null) {
            profileTracker.stopTracking();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Utilities.changeStatusbar(LoginActivity.this, getWindow());
        FacebookSdk.sdkInitialize(this.getApplicationContext());
        //getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        setContentView(R.layout.activity_login);

        if (Build.VERSION.SDK_INT >= 19 && Build.VERSION.SDK_INT < 21) {
            setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, true);
        }
        if (Build.VERSION.SDK_INT >= 19) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }
        if (Build.VERSION.SDK_INT >= 21) {
            setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, false);
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }

        context = LoginActivity.this;
        mService = ApiUtils.getSOService();

        qkPreferences = new QKPreferences(LoginActivity.this);

        if (Build.VERSION.SDK_INT >= 21) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        initWidget();
    }

    public static void setWindowFlag(Activity activity, final int bits, boolean on) {
        Window win = activity.getWindow();
        WindowManager.LayoutParams winParams = win.getAttributes();
        if (on) {
            winParams.flags |= bits;
        } else {
            winParams.flags &= ~bits;
        }
        win.setAttributes(winParams);
    }

    public void initWidget() {

        edt_email = findViewById(R.id.edt_email);
        edt_password = findViewById(R.id.edt_password);
        btn_Login = findViewById(R.id.btn_signin);
        btn_Facebook = findViewById(R.id.btn_facebook);
        btn_LinkedIn_Login = findViewById(R.id.btn_linkedin_signin);
        tv_register = findViewById(R.id.tv_register);
        tv_forgot_password = findViewById(R.id.tv_forgot_password);

        btn_Login.setOnClickListener(this);
        btn_Facebook.setOnClickListener(this);
        tv_register.setOnClickListener(this);
        tv_forgot_password.setOnClickListener(this);
        btn_LinkedIn_Login.setOnClickListener(this);

        loginButton = findViewById(R.id.login_button);
        loginButton.setReadPermissions(AppClass.facebook_permission);
        callbackManager = CallbackManager.Factory.create();

        String first = "<font color='#777777'>Not registered yet?</font>";
        String next = "<font color='#2ABBBE'><b> SIGN UP!</b></font>";
        tv_register.setText(Html.fromHtml(first + next));

        try {
            LoginManager.getInstance().logOut();
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Facebook Callback Registration
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.d("FB TOKEN: ", "" + loginResult.getAccessToken());
                getFacebookUserData(loginResult.getAccessToken());
            }

            @Override
            public void onCancel() {
            }

            @Override
            public void onError(FacebookException exception) {
                Utilities.showToast(context, exception.getMessage());
            }
        });
    }

    @Override
    public void onClick(View view) {
        String pass = edt_password.getText().toString();
        switch (view.getId()) {
            case R.id.btn_signin:
                if (!Utilities.isEmpty(edt_email.getText().toString()))
                    Utilities.showToast(context, "Email is required");

                else if (!edt_email.getText().toString().trim().matches(Constants.emailPattern))
                    Utilities.showToast(context, "Invalid email address");

                else if (TextUtils.isEmpty(edt_password.getText().toString()) || pass.length() < 6)
                    Utilities.showToast(context, "Password must contain at least 6 characters.");

                else if (!Utilities.isNetworkConnected(context))
                    Utilities.showToast(context, "No internet connection, please try after some time.");

                else {
                    Utilities.hideKeyboard(LoginActivity.this);
                    functionSignIn(edt_email.getText().toString().trim(), edt_password.getText().toString().trim());
                }
                break;

            case R.id.btn_facebook:
                Utilities.showTermsCondpopUp(LoginActivity.this, "Alert", Constants.CONTENT_ALER_PRIVACY,
                        new CallBackGetResult() {
                            @Override
                            public void onSuccess() {
                                loginButton.performClick();
                            }

                            @Override
                            public void onCancel() {

                            }
                        });
                break;

            case R.id.tv_register:
                startActivity(new Intent(context, SignupActivity.class));
                break;

            case R.id.tv_forgot_password:
                //startActivity(new Intent(context, ForgotPasswordActivity.class));
                Intent intent = new Intent(this, ForgotPasswordActivity.class);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(this, view, "robot");
                    startActivity(intent, options.toBundle());
                } else {
                    startActivity(intent);
                }
                break;

            case R.id.btn_linkedin_signin:
                Utilities.showTermsCondpopUp(LoginActivity.this, "Alert", Constants.CONTENT_ALER_PRIVACY,
                        new CallBackGetResult() {
                            @Override
                            public void onSuccess() {
                                boolean isAppInstalled = Utilities.appInstalledOrNot(LoginActivity.this, LINKEDIN_PACKAGE_NAME);
                                if (isAppInstalled) {
                                    LinkedInMapping();
                                    //linkedInLoginWithoutApp();
                                } else {
                                    linkedInLoginWithoutApp();
                                }
                            }

                            @Override
                            public void onCancel() {
                            }
                        });
                break;
        }
    }

    public void functionSignIn(String email, String password) {
        final ProgressDialog progressDialog = Utilities.showProgress(context);
        mService.getLogin(email, password, "Android", FirebaseInstanceId.getInstance().getToken()).enqueue(new Callback<LoginPojo>() {
            @Override
            public void onResponse(Call<LoginPojo> call, Response<LoginPojo> response) {
                Log.e("Login-Success", "" + response.body());
                if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();
                try {
                    if (response != null && response.body() != null) {
                        LoginPojo loginPojo = response.body();
                        if (loginPojo.getStatus() == 200) {

                            JSONObject jsonObject = new JSONObject(new Gson().toJson(loginPojo));
                            parseResponse(jsonObject);

                        } else Utilities.showToast(context, loginPojo.getMsg());

                    } else Utilities.showToast(context, "Login failed, please try again");

                } catch (Exception e) {
                    Log.e("Login-Error", "" + e.getMessage());
                }
            }

            @Override
            public void onFailure(Call<LoginPojo> call, Throwable t) {
                Log.e("Login-Error", "" + t.getLocalizedMessage());
                if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
        LISessionManager.getInstance(getApplicationContext()).onActivityResult(this, requestCode, resultCode, data);
    }

    public void getFacebookUserData(AccessToken accessToken) {

        profileTracker = new ProfileTracker() {
            @Override
            protected void onCurrentProfileChanged(Profile oldProfile, Profile currentProfile) {

                Profile profile = currentProfile;

                if (profile != null) {

                    JSONObject objectProfile = new JSONObject();
                    try {
                        objectProfile.put("id", profile.getId());
                        objectProfile.put("name", profile.getName());
                        objectProfile.put("first_name", profile.getFirstName());
                        objectProfile.put("last_name", profile.getLastName());
                        objectProfile.put("link", profile.getLinkUri());

                        JSONObject jsonObject = new JSONObject();
                        try {

                            String facebook_id = profile.getId();

                            jsonObject.put("social_media_user_id", facebook_id);
                            jsonObject.put("social_media_id", Constants.SOCIAL_FACEBOOK_ID);
                            jsonObject.put("device_type", "Android");
                            if (FirebaseInstanceId.getInstance() != null &&
                                    FirebaseInstanceId.getInstance().getToken() != null)
                                jsonObject.put("device_id", FirebaseInstanceId.getInstance().getToken());
                            else
                                jsonObject.put("device_id", "");

                            jsonObject.put("data", objectProfile);

                            Log.d(TAG, "request object: " + jsonObject);

                            sendSocialData(jsonObject);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        };

        profileTracker.startTracking();


//        GraphRequest request = GraphRequest.newMeRequest(accessToken.getCurrentAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
//            @Override
//            public void onCompleted(JSONObject object, GraphResponse response) {
//                JSONObject jsonObject = new JSONObject();
//                try {
//                    LoginSocialMediaType = "1";
//
//                    String facebook_id = object.optString("id");
//
//                    String link = object.optString("link");
//
//                    jsonObject.put("social_media_user_id", facebook_id);
//                    jsonObject.put("social_media_id", Constants.SOCIAL_FACEBOOK_ID);
//                    jsonObject.put("device_type", "Android");
//                    if (link != null && !link.equalsIgnoreCase("") && !link.equalsIgnoreCase("null"))
//                        object.put("publicProfileUrl", link + "");
//                    else
//                        object.put("publicProfileUrl", "");
//
//                    if (FirebaseInstanceId.getInstance() != null && FirebaseInstanceId.getInstance().getToken() != null)
//                        jsonObject.put("device_id", FirebaseInstanceId.getInstance().getToken());
//                    else
//                        jsonObject.put("device_id", "");
//
//                    jsonObject.put("data", object);
//
//                    Log.d(TAG, "request object: " + jsonObject);
//
//                    sendSocialData(jsonObject);
//
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
//        });
//        Bundle parameters = new Bundle();
//        parameters.putString("fields", "id,name,first_name,music,last_name,email,location{location},education,website,work,friendlists,friends,link,about,birthday,hometown,books,events,religion,political,tagged_places");
//        request.setParameters(parameters);
//        request.executeAsync();
    }

    // LinkedIn
    private static Scope buildScope() {
        return Scope.build(Scope.R_BASICPROFILE, Scope.R_EMAILADDRESS);
    }

    public void LinkedInMapping() {
        LISessionManager.getInstance(context).init(LoginActivity.this, buildScope(), new AuthListener() {
            @Override
            public void onAuthSuccess() {
                final APIHelper apiHelper = APIHelper.getInstance(getApplicationContext());
                apiHelper.getRequest(context, AppClass.LINKEDIN_URL, new ApiListener() {
                    @Override
                    public void onApiSuccess(final ApiResponse apiResponse) {
                        Log.d("Linkedin ModelLogedData", "" + apiResponse.getResponseDataAsJson());
                        JSONObject objData = apiResponse.getResponseDataAsJson();
                        try {
                            LoginSocialMediaType = "2";

                            JSONObject jsonObject = new JSONObject();

                            String linkedIn_id = objData.getString("id");

                            jsonObject.put("social_media_user_id", linkedIn_id);
                            jsonObject.put("social_media_id", Constants.SOCIAL_LINKEDIN_ID);
                            jsonObject.put("device_type", "Android");
                            if (FirebaseInstanceId.getInstance() != null && FirebaseInstanceId.getInstance().getToken() != null)
                                jsonObject.put("device_id", FirebaseInstanceId.getInstance().getToken());
                            else
                                jsonObject.put("device_id", "");

                            jsonObject.put("data", objData);

                            Log.d(TAG, "request object: " + jsonObject);

                            sendSocialData(jsonObject);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onApiError(LIApiError liApiError) {
                        Log.e("Linked_in_auth", liApiError + "" + liApiError.getApiErrorResponse());
                    }
                });
            }

            @Override
            public void onAuthError(LIAuthError error) {
                //Utilities.showToast(context, "" + error.toString());
            }
        }, true);

    }

    private void linkedInLoginWithoutApp() {
        ProgressDialog progressDialog = new ProgressDialog(this);
        LinkedinDialog d = new LinkedinDialog(this, progressDialog);
        d.show();
        d.setVerifierListener(new LinkedinDialog.OnVerifyListener() {
            @SuppressLint("NewApi")
            public void onVerify(String verifier) {
                Log.i("LinkedIn Verifier", "" + verifier);
                try {
                    LoginSocialMediaType = "2";

                    accessToken = LinkedinDialog.oAuthService.getOAuthAccessToken(LinkedinDialog.liToken, verifier);
                    factory.createLinkedInApiClient(accessToken);
                    client = factory.createLinkedInApiClient(accessToken);
                    Log.d("LinkedIn Token", "" + accessToken.getToken());
                    Log.d("LinkedIn Secret", "" + accessToken.getTokenSecret());
                    Log.d("LinkedIn Secret", client.getAccessToken().getTokenSecret());
                    Person p = client.getProfileForCurrentUser(EnumSet.of(
                            ProfileField.ID, ProfileField.FIRST_NAME,
                            ProfileField.PHONE_NUMBERS, ProfileField.LAST_NAME,
                            ProfileField.HEADLINE, ProfileField.INDUSTRY,
                            ProfileField.PICTURE_URL, ProfileField.PUBLIC_PROFILE_URL,
                            ProfileField.DATE_OF_BIRTH,
                            ProfileField.LOCATION_NAME, ProfileField.MAIN_ADDRESS,
                            ProfileField.LOCATION_COUNTRY));
                    Log.d("pppp", p + "");

                    JSONObject jsonObject = new JSONObject();

                    jsonObject.put("social_media_user_id", p.getId());
                    jsonObject.put("social_media_id", Constants.SOCIAL_LINKEDIN_ID);
                    jsonObject.put("device_type", "Android");

                    if (FirebaseInstanceId.getInstance() != null &&
                            FirebaseInstanceId.getInstance().getToken() != null)
                        jsonObject.put("device_id", FirebaseInstanceId.getInstance().getToken());
                    else
                        jsonObject.put("device_id", "");

                    JSONObject jsonLinkedinData = new JSONObject();

                    if (p.getPublicProfileUrl() != null)
                        jsonLinkedinData.put("publicProfileUrl", p.getPublicProfileUrl());

                    if (p.getFirstName() != null)
                        jsonLinkedinData.put("firstName", p.getFirstName());

                    if (p.getId() != null)
                        jsonLinkedinData.put("id", p.getId());
                    else
                        jsonLinkedinData.put("id", "");


                    if (p.getIndustry() != null)
                        jsonLinkedinData.put("industry", p.getIndustry());
                    else
                        jsonLinkedinData.put("industry", "");

                    if (p.getLastName() != null)
                        jsonLinkedinData.put("lastName", p.getLastName());
                    else
                        jsonLinkedinData.put("lastName", "");

                    JSONObject locationJson = new JSONObject();
                    if (p.getLocation() != null && p.getLocation().getName() != null)
                        locationJson.put("name", p.getLocation().getName());
                    else
                        locationJson.put("name", "");

                    JSONObject country = new JSONObject();
                    if (p.getLocation() != null && p.getLocation().getCountry() != null && p.getLocation().getCountry().getCode() != null)
                        country.put("code", p.getLocation().getCountry().getCode());
                    else
                        country.put("code", "");

                    locationJson.put("country", country);
                    jsonLinkedinData.put("location", locationJson);

                    jsonObject.put("data", jsonLinkedinData);

                    sendSocialData(jsonObject);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(true);
        progressDialog.show();
    }

    private void sendSocialData(JSONObject jsonObject) {
        new VolleyApiRequest(context, true, VolleyApiRequest.REQUEST_BASEURL + VolleyApiRequest.API_CREATE_USER,
                new VolleyCallBack() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String status = response.getString("status");
                            String message = response.getString("msg");
                            if (status.equals("200")) {
                                qkPreferences.storeLoginFrom("SOCIAL");
                                parseResponse(response);
                            } else {
                                Utilities.showToast(context, message);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyApiRequest.showVolleyError(context, error);
                    }
                }).enqueRequest(jsonObject, Request.Method.POST);
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    public void parseResponse(JSONObject data) {

        try {
            ModelLoggedUser modelLoggedUser = new Gson().fromJson(data.toString(), ModelLoggedUser.class);
            if (modelLoggedUser != null && modelLoggedUser.getStatus() == 200) {

                qkPreferences.storeLoggedUser(modelLoggedUser);

                qkPreferences.storeApiToken(modelLoggedUser.getApiToken() + "");
                JSONObject qktagInfo = new JSONObject();
                qktagInfo.put(Constants.QKTAGURL, modelLoggedUser.getData().getQrCode());
                qktagInfo.put(Constants.QKMIDDLETAGURL, "");

                /*try {
                    // Add user to firebase and update user details
                    String UniqueCode = modelLoggedUser.getData().getUniqueCode();
                    String user_profilepic = modelLoggedUser.getData().getProfilePic() + "";
                    String name = modelLoggedUser.getData().getName() + "";
                    String userid = modelLoggedUser.getData().getId() + "";
                    String token = FirebaseInstanceId.getInstance().getToken() + "";
                    if (!UniqueCode.equalsIgnoreCase("") && !UniqueCode.equalsIgnoreCase("null")) {
                        ChatUserModel cum = new ChatUserModel(name, user_profilepic, "1", userid, token);
                        ChatConfig.addUser(LoginActivity.this, UniqueCode, cum);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }*/

                if (modelLoggedUser.getData().getBorderColor() != null && !modelLoggedUser.getData().getBorderColor().toString().isEmpty()
                        && !modelLoggedUser.getData().getBorderColor().toString().equals("0"))
                    qktagInfo.put(Constants.QKBORDERCOLOR, modelLoggedUser.getData().getBorderColor().toString());
                else
                    qktagInfo.put(Constants.QKBORDERCOLOR, getResources().getString(R.string.default_border));

                if (modelLoggedUser.getData().getBackgroundColor() != null && !modelLoggedUser.getData().getBackgroundColor().toString().isEmpty()
                        && !modelLoggedUser.getData().getBackgroundColor().toString().equals("0"))
                    qktagInfo.put(Constants.QKBACKCOLOR, modelLoggedUser.getData().getBackgroundColor().toString());
                else
                    qktagInfo.put(Constants.QKBACKCOLOR, getResources().getString(R.string.default_back));

                if (modelLoggedUser.getData().getPatternColor() != null && !modelLoggedUser.getData().getPatternColor().toString().isEmpty()
                        && !modelLoggedUser.getData().getPatternColor().toString().equals("0"))
                    qktagInfo.put(Constants.QKPATTERNCOLOR, modelLoggedUser.getData().getPatternColor().toString());
                else
                    qktagInfo.put(Constants.QKPATTERNCOLOR, getResources().getString(R.string.default_data));

                qkPreferences.storeQKInfo(qktagInfo.toString());

//                SharedPreferences.Editor ed = mSharedPreferences.edit();
//                ed.putString(SharedPreference.CURRENT_USER_ID, "" + modelLoggedUser.getData().getId());
//                ed.putString(SharedPreference.CURRENT_USER_NAME, modelLoggedUser.getData().getFirstname()
//                        + " " + modelLoggedUser.getData().getLastname());
//                ed.putString(SharedPreference.CURRENT_USER_FIRSTTIME, "" + modelLoggedUser.getData().getFirsttime());
//                ed.putString(SharedPreference.CURRENT_USER_EMAIL, modelLoggedUser.getData().getEmail());
//                ed.putString(SharedPreference.CURRENT_USER_FULLNAME, modelLoggedUser.getData().getFirstname()
//                        + " " + modelLoggedUser.getData().getLastname());
//                ed.putString(SharedPreference.CURRENT_USER_PROFILE_PIC, "" + modelLoggedUser.getData().getProfilePic());
//                ed.putString(SharedPreference.CURRENT_UNIQU_QK_TAG_NUMBER, modelLoggedUser.getData().getUniqueCode());
//                ed.putString(SharedPreference.NOTIFICATION_STATUS, "" + modelLoggedUser.getData().getNotificationStatus());
//                ed.putString(Constants.QKTAGURL, "" + modelLoggedUser.getData().getQrCode());
//                ed.apply();

                Object middle_tag = modelLoggedUser.getData().getMiddleTag();
                if (middle_tag != null && !middle_tag.toString().isEmpty() && !middle_tag.toString().equals("null")) {
                    new DownloadImage(context, new CallBackImageDownload() {
                        @Override
                        public void getBase64(String mData) {
                            try {
                                if (mData != null) {
                                    JSONObject jsonObject = new JSONObject(qkPreferences.getQKInfo());
                                    jsonObject.put(Constants.QKMIDDLETAGURL, mData);
                                    qkPreferences.storeQKInfo(jsonObject.toString());
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void getBitmap(Bitmap mData) {
                        }
                    }).execute(middle_tag.toString());
                }

                /*
                 *  for Connecting and login to XMPP openFire
                 */
                try {
                    // Already started in Splash therefor have to stop
                    stopService(new Intent(getApplicationContext(), BackgroundService.class));
                    // start again for connection
                    startService(new Intent(getApplicationContext(), BackgroundService.class));
                } catch (Exception e) {
                    e.printStackTrace();
                }

                /*********
                 * if status = 1 => socialmedia is integrated
                 * if status = 0 => not integrated
                 * status = 1= check public url ==> if (null){ open popup for email}else( go ahead)
                 ****/
                try {
                    if (LoginSocialMediaType != null && LoginSocialMediaType.equalsIgnoreCase("1") && !LoginSocialMediaType.equalsIgnoreCase("")) {
                        if (modelLoggedUser.getData().getFacebook() != null && modelLoggedUser.getData().getFacebook().getStatus() == 1) {
                            if (modelLoggedUser.getData().getFacebook().getData().getPublicProfileUrl() != null && !modelLoggedUser.getData().getFacebook().getData().getPublicProfileUrl().equalsIgnoreCase("") && !modelLoggedUser.getData().getFacebook().getData().getPublicProfileUrl().equalsIgnoreCase("null")) {
                                GoAhead(modelLoggedUser);
                            } else {
                                openSocialMediaEmailDialog(modelLoggedUser, 1);
                            }
                        } else {
                            GoAhead(modelLoggedUser);
                        }
                    } else if (LoginSocialMediaType != null && LoginSocialMediaType.equalsIgnoreCase("2") && !LoginSocialMediaType.equalsIgnoreCase("")) {
                        if (modelLoggedUser.getData().getLinkedin() != null && modelLoggedUser.getData().getLinkedin().getStatus() == 1) {
                            if (modelLoggedUser.getData().getLinkedin().getData().getPublicProfileUrl() != null && !modelLoggedUser.getData().getLinkedin().getData().getPublicProfileUrl().equalsIgnoreCase("") && !modelLoggedUser.getData().getLinkedin().getData().getPublicProfileUrl().equalsIgnoreCase("null")) {
                                GoAhead(modelLoggedUser);
                            } else {
                                openSocialMediaEmailDialog(modelLoggedUser, 2);
                            }
                        } else {
                            GoAhead(modelLoggedUser);
                        }
                    } else {
                        GoAhead(modelLoggedUser);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else
                Utilities.showToast(context, getResources().getString(R.string.error_process));
        } catch (Exception e) {
            Utilities.showToast(context, "" + e.getMessage());
        }
    }

    /*
    private void LoginUsertoFirebase(String uniqueCode) {
        FirebaseAuth.getInstance()
                .signInWithCustomToken(uniqueCode)
                //.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(LoginActivity.this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.d(TAG, "performFirebaseLogin:onComplete:" + task.isSuccessful());
                        if (task.isSuccessful()) {
                            Log.d("Firebase_Success", task.getResult().toString() + "");
                            Log.d("Firebase_Uid", task.getResult().getUser().getUid() + "");
                        } else {
                            Log.d("Firebase_Failure", task.getException().getMessage() + "");
                        }
                    }
                });
    }
    */

    private void GoAhead(ModelLoggedUser modelLoggedUser) {
        if (modelLoggedUser.getData().getRegister() != null && modelLoggedUser.getData().getRegister() == 1) {
            try {
                modelLoggedUser.getData().setStatus(1);
                modelLoggedUser.getData().setRegister(null);
                qkPreferences.storeLoggedUser(modelLoggedUser);
            } catch (Exception e) {
                e.printStackTrace();
            }
            Intent intent = new Intent(context, AddSocialMediaAccountActivity.class);
            intent.putExtra("from", "other");
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        } else {
            if (modelLoggedUser.getData().getStatus() == 1) {
                Intent intent = new Intent(context, AddSocialMediaAccountActivity.class);
                intent.putExtra("from", "other");
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);

            } else if (modelLoggedUser.getData().getStatus() == 2 || modelLoggedUser.getData().getStatus() == 3) {
                Intent intent = new Intent(context, ConfirmRegistration.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);

            }

//            else if (modelLoggedUser.getData().getStatus() == 3) {
//
////                Intent intent = new Intent(context, UpdateQkTagActivity.class);
////                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
////                intent.putExtra("from", Constants.KEY_FROM_UPDATEPROFILE);
////                startActivity(intent);
//
//                startActivity(new Intent(LoginActivity.this, ConfirmRegistration.class));
//
//            }

            else if (modelLoggedUser.getData().getStatus() == 4) {
                Intent intent = new Intent(context, DashboardActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        }
        LoginActivity.this.finish();
    }

    String msg = "";
    String Url = "";
    String SOcialMediaId = "";

    private void openSocialMediaEmailDialog(final ModelLoggedUser modelLoggedUser, final int type) {

        final Dialog dialog = new Dialog(LoginActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View view = LinearLayout.inflate(LoginActivity.this, R.layout.dialog_addsocialmedia, null);
        dialog.setContentView(view);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        dialog.show();
        dialog.getWindow().setAttributes(lp);
        TextView tvAlert = view.findViewById(R.id.tvAlert);
        TextView tvDone = view.findViewById(R.id.tvDone);
        TextView tvCancel = view.findViewById(R.id.tvCancel);
        CountryCodePicker ccp = (CountryCodePicker) view.findViewById(R.id.ccp);
        LinearLayout lin_cpp = view.findViewById(R.id.lin_cpp);

        if (type == 1) {
            msg = "Add Facebook URL";
            Url = "https://www.facebook.com/";
            SOcialMediaId = "1";
        } else if (type == 2) {
            msg = "Add LinkedIn URL";
            Url = "https://www.linkedin.com/";
            SOcialMediaId = "2";
        }

        view.findViewById(R.id.divider_select_socialmedia).setVisibility(View.GONE);
        ccp.setVisibility(View.GONE);
        tvAlert.setText(msg);
        final SearchableSpinner spinnerSocialmedia = view.findViewById(R.id.spinnerSocialMedia);
        spinnerSocialmedia.setVisibility(View.GONE);
        final EditText edtSocialmediaLInk = view.findViewById(R.id.edt_socialmedia_link);
        edtSocialmediaLInk.setText(Url);
        Selection.setSelection(edtSocialmediaLInk.getText(), edtSocialmediaLInk.getText().length());

        edtSocialmediaLInk.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!s.toString().startsWith(Url)) {
                    edtSocialmediaLInk.setText(Url);
                    Selection.setSelection(edtSocialmediaLInk.getText(), edtSocialmediaLInk.getText().length());
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        tvDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    Utilities.hidekeyboard(LoginActivity.this, view);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                dialog.dismiss();
                if (!edtSocialmediaLInk.getText().toString().isEmpty() && isValidUrl(edtSocialmediaLInk.getText().toString())) {
                    String URL = edtSocialmediaLInk.getText().toString() + "";
                    UploadSocialmedia(URL + "", SOcialMediaId, dialog, modelLoggedUser);
                } else {
                    Utilities.showToast(LoginActivity.this, "Please enter valid URL");
                }
            }
        });

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    if (type == 1) {
                        Utilities.hidekeyboard(LoginActivity.this, view);
                        LoginManager.getInstance().logOut();
                    } else if (type == 2) {
                        LISessionManager.getInstance(LoginActivity.this).clearSession();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void UploadSocialmedia(String Url, String SocialMediaId, final Dialog dialog, final ModelLoggedUser modelLoggedUser) {
        try {
            //final ProgressDialog pd = Utilities.showProgress(LoginActivity.this);
            String userid = qkPreferences.getValuesInt(QKPreferences.USER_ID) + "";
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("user_id", userid);
            jsonObject.put("social_media_id", SocialMediaId + "");
            jsonObject.put("publicProfileUrl", Url + "");
            new VolleyApiRequest(context, true, VolleyApiRequest.REQUEST_BASEURL + "api/update_social_profile_url",
                    new VolleyCallBack() {
                        @Override
                        public void onResponse(JSONObject response) {
                           /* if (pd != null && pd.isShowing()) {
                                pd.dismiss();
                            }*/
                            try {
                                if (response != null && response.length() > 0) {
                                    String ststus = response.getString("status");
                                    String message = response.getString("msg");

                                    if (ststus.equalsIgnoreCase("200")) {
                                        if (dialog != null && dialog.isShowing()) {
                                            dialog.dismiss();
                                        }
                                        GoAhead(modelLoggedUser);
                                    } else
                                        Utilities.showToast(context, message + "");
                                } else

                                    Utilities.showToast(context, "Please try again");

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onErrorResponse(VolleyError error) {
                            /*if (pd != null && pd.isShowing()) {
                                pd.dismiss();
                            }*/
                            //VolleyApiRequest.showVolleyError(context, error);
                        }
                    }).enqueRequest(jsonObject, Request.Method.POST);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


//    public void getTags() {
//        try {
//            JSONObject jsonObject = new JSONObject();
//            jsonObject.put("parent_id", 0);
//            new VolleyApiRequest(context, true,
//                    VolleyApiRequest.REQUEST_BASEURL + VolleyApiRequest.API_TAGLIST,
//                    new VolleyCallBack() {
//                        @Override
//                        public void onResponse(JSONObject response) {
//                            try {
//                                if (response != null && response.length() > 0) {
//                                    ModelTagData modelTagData = new Gson().fromJson(response.toString(), ModelTagData.class);
//                                    if (modelTagData.getStatus() == 200) {
//                                        if (modelTagData.getData() != null && modelTagData.getData().size() > 0) {
//
//                                            RealmController.getInstance().clearAllTags();
//
//                                            for (ModelTagDetail modelTagDetail : modelTagData.getData()) {
//
//                                                TagData tagData = new TagData();
//                                                tagData.setId(modelTagDetail.getId());
//                                                tagData.setName(modelTagDetail.getName());
//
//                                                Realm realm = Realm.getDefaultInstance();
//                                                realm.beginTransaction();
//                                                realm.copyToRealm(tagData);
//                                                realm.commitTransaction();
//                                            }
//                                        }
//                                    } else
//                                        Utilities.showToast(context, modelTagData.getMsg());
//
//                                }
//                            } catch (Exception e) {
//                                e.printStackTrace();
//                            }
//                        }
//
//                        @Override
//                        public void onErrorResponse(VolleyError error) {
//                            VolleyApiRequest.showVolleyError(context, error);
//                        }
//                    }).enqueRequest(jsonObject, Request.Method.POST);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
}




/* for generating hash key of linkedin
 try {
         PackageInfo info = getPackageManager().getPackageInfo("com.quickkonnect", PackageManager.GET_SIGNATURES);
         for (Signature signature : info.signatures) {
         MessageDigest md = MessageDigest.getInstance("SHA");
         md.update(signature.toByteArray());

         Log.e("HashKey", Base64.encodeToString(md.digest(), Base64.NO_WRAP) + "");
         Log.e("package", info.packageName + "");
         //((TextView) findViewById(R.id.package_name)).setText(info.packageName);
         //((TextView) findViewById(R.id.hash_key)).setText(Base64.encodeToString(md.digest(), Base64.NO_WRAP));
         }
         } catch (PackageManager.NameNotFoundException e) {
         Log.d(TAG, e.getMessage(), e);
         } catch (NoSuchAlgorithmException e) {
         Log.d(TAG, e.getMessage(), e);
         }*/