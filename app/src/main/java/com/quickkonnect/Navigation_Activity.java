package com.quickkonnect;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.LinearSmoothScroller;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.activities.AccountsSettings;
import com.activities.CreateProfileActivity;
import com.activities.HelpSettings;
import com.activities.ManageQKProfilesActivity;
import com.adapter.NavItemAdapter;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.facebook.login.LoginManager;
import com.google.firebase.iid.FirebaseInstanceId;
import com.interfaces.OnFragLoad;
import com.interfaces.OnItemClick;
import com.models.profiles.ProfilesList;
import com.realmtable.MyProfiles;
import com.realmtable.RealmController;
import com.retrofitservice.ApiUtils;
import com.retrofitservice.SOService;
import com.services.UserAnalytics;
import com.utilities.Constants;
import com.utilities.QKPreferences;
import com.utilities.Utilities;
import com.volleyapirequest.VolleyApiRequest;
import com.volleyapirequest.VolleyCallBack;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import io.realm.RealmResults;

/**
 * Created by ZTLAB03 on 13-03-2018.
 */

public class Navigation_Activity extends Fragment {

    private SOService mService;
    private BroadcastReceiver receiver, received_delete;
    private static Context context;
    private View rootView;
    private int RESULT_CODE_PROFILEDATAUPDATE = 100;
    private int RESULT_CODE_UPDATEWHOLE_DATA = 1010;
    String[] setting_title = {"Social Accounts", "Help and Settings", "Legal", "Account Settings", "Logout"};
    int[] setting_img = {R.drawable.account,
            R.drawable.help,
            R.drawable.legal,
            R.drawable.accountsetting,
            R.drawable.logout};
    private List<NavModelItems> listDataMembers;
    private RecyclerView recyclerMenu;
    private QKPreferences qkPreferences;
    private List<NavModelItems> modelitems;
    private String userid = "";
    private NavItemAdapter navItemAdapter;

    public static Navigation_Activity getInstance(OnFragLoad onFragLoad) {
        Navigation_Activity fragChats = new Navigation_Activity();
        return fragChats;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.activity_navigation, container, false);

        context = getActivity();

        mService = ApiUtils.getSOService();
        qkPreferences = new QKPreferences(context);

        userid = qkPreferences.getValuesInt(QKPreferences.USER_ID) + "";

        recyclerMenu = rootView.findViewById(R.id.recyclerMenu);
        recyclerMenu.setNestedScrollingEnabled(false);
        recyclerMenu.setLayoutManager(setSmoothScroll());

        filladapter();
        navItemAdapter = new NavItemAdapter(modelitems, getActivity(), new OnItemClick() {
            @Override
            public void getItemClick(int itemType, NavModelItems modelItems, String action) {

                if (itemType == 5) {
                    try {
                        if (navItemAdapter.isHideSetting) {
                            navItemAdapter.isHideSetting = false;
                            navItemAdapter.notifyDataSetChanged();
                        } else {
                            navItemAdapter.isHideSetting = true;
                            navItemAdapter.notifyDataSetChanged();
                        }
                    } catch (Exception e) {
                        navItemAdapter.isHideSetting = false;
                        navItemAdapter.notifyDataSetChanged();
                        e.printStackTrace();
                    }
                } else if (itemType == 4) {
                    try {
                        DashboardActivity.showProfileinfo();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else if (itemType == 3) {

                    if (action.equals("createProfile")) {
                        startActivityForResult(new Intent(context, CreateProfileActivity.class),
                                RESULT_CODE_PROFILEDATAUPDATE);

                    } else if (action.equals("viewAllProfile")) {
                        startActivityForResult(new Intent(getActivity(), ManageQKProfilesActivity.class),
                                RESULT_CODE_UPDATEWHOLE_DATA);
                    }

                } else if (itemType == 1) {

                    if (action.equals(setting_title[0])) {
                        startActivity(new Intent(context,
                                AddSocialMediaAccountActivity.class).putExtra("from", "menu"));

                    } else if (action.equals(setting_title[1])) {
                        startActivity(new Intent(context, HelpSettings.class));

                    } else if (action.equals(setting_title[2])) {
                        startActivity(new Intent(context, Legal.class));

                    } else if (action.equals(setting_title[3])) {
                        startActivity(new Intent(context, AccountsSettings.class));

                    } else if (action.equals(setting_title[4])) {
                        if (!Utilities.isNetworkConnected(context))
                            Toast.makeText(context, Constants.NO_INTERNET_CONNECTION, Toast.LENGTH_LONG).show();
                        else
                            showDeleteconfirmation(true);
                    }
                } else if (itemType == 2) {
                    if (modelItems.getType() != null && modelItems.getType() == 4) {
                        /*Intent intent = new Intent(context, EmployeeDetails.class);
                        intent.putExtra("unique_code", "" + modelItems.getUniqueCode());
                        intent.putExtra(Constants.KEY, Constants.KEY_FROM_MANAGEPROFILES);
                        intent.putExtra("profile_id", "" + modelItems.getId());
                        startActivityForResult(intent, 102);*/
                        Intent intent = new Intent(context, Accept_Employee.class);
                        intent.putExtra(Constants.KEY, Constants.KEY_FROM_MANAGEPROFILES);
                        intent.putExtra("PROFILE_NAME", modelItems.getName() + "");
                        intent.putExtra("unique_id", modelItems.getUniqueCode() + "");
                        intent.putExtra("profile_id", "" + modelItems.getId());
                        startActivityForResult(intent, 102);
                    } else {
                        Intent intent = new Intent(context, NewScanProfileActivity.class);
                        intent.putExtra(Constants.KEY, Constants.KEY_FROM_MANAGEPROFILES);
                        intent.putExtra("unique_code", "" + modelItems.getUniqueCode());
                        intent.putExtra("profile_id", "" + modelItems.getId());
                        intent.putExtra("user_id", "" + modelItems.getUser_id());
                        intent.putExtra("ROLE", "" + modelItems.getRole());
                        intent.putExtra("ROLE_NAME", "" + modelItems.getRole_name());
                        startActivityForResult(intent, 102);
                    }
                }
            }
        });
        recyclerMenu.setAdapter(navItemAdapter);
        showFirst3Profiles();

        IntentFilter filter = new IntentFilter();
        filter.addAction("BROADCAST_FROM_SCAN_QK");

        try {
            receiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    showFirst3Profiles();
                }
            };
            getActivity().registerReceiver(receiver, filter);
        } catch (Exception e) {
            e.printStackTrace();
        }

        IntentFilter filter1 = new IntentFilter();
        filter1.addAction("BROADCAST_FROM_DELETE_PROFILE");

        try {
            received_delete = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    showFirst3Profiles();
                }
            };
            getActivity().registerReceiver(received_delete, filter1);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return rootView;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            if (receiver != null) {
                getActivity().unregisterReceiver(receiver);
                receiver = null;
            }
            if (received_delete != null) {
                getActivity().unregisterReceiver(received_delete);
                received_delete = null;
            }
            super.onDestroy();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void filladapter() {

        try {
            if (modelitems == null) {
                modelitems = new ArrayList<>();

            } else modelitems.clear();

            // Add Navigation Header
            NavModelItems navModelHeader = new NavModelItems();
            navModelHeader.setViewType(NavItemAdapter.viewTypeHeader);
            modelitems.add(navModelHeader);

            NavModelItems navModelDivider = new NavModelItems();
            navModelDivider.setViewType(NavItemAdapter.viewTypeDivider);
            modelitems.add(navModelDivider);

            if (setting_title != null && setting_title.length > 0) {

                for (int i = 0; i < setting_title.length; i++) {
                    NavModelItems navModelItems = new NavModelItems();
                    navModelItems.setIcon(setting_img[i]);
                    navModelItems.setTitle(setting_title[i]);
                    navModelItems.setViewType(NavItemAdapter.viewTypeItem);
                    modelitems.add(navModelItems);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void showDeleteconfirmation(final boolean isDelete) {

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Alert!");
        if (isDelete) {
            builder.setMessage("Are you sure,want to logout?");
        }
        builder.setPositiveButton("Done", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                if (isDelete) {
                    Logout_final();
                }
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        builder.show();
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            showFirst3Profiles();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void Logout_final() {
        try {
            /*try {
                QKPreferences qkPreferences = new QKPreferences(getActivity());
                String UniqueCode = qkPreferences.getValues(QKPreferences.USER_UNIQUECODE);
                if (!UniqueCode.equalsIgnoreCase("") && !UniqueCode.equalsIgnoreCase("null")) {
                    ChatConfig.changeOnlineStatus("0", UniqueCode + "");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }*/
            Intent i = new Intent(getActivity(), UserAnalytics.class);
            getActivity().startService(i);
        } catch (Exception e) {
            e.printStackTrace();
        }
        String firebase_token = FirebaseInstanceId.getInstance().getToken();
        new VolleyApiRequest(context, true, VolleyApiRequest.REQUEST_BASEURL + "api/logout/" + userid + "/" + firebase_token,
                new VolleyCallBack() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String status = response.getString("status");
                            if (status.equals("200")) {

                                qkPreferences.clearQKPreference();

                                LoginManager.getInstance().logOut();

                                RealmController.with(getActivity()).clearChatConversation();
                                RealmController.with(getActivity()).clearChatuserList();
                                RealmController.with(getActivity()).clearAllTables();
                                Intent intent = new Intent(context, Introduction_Activity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);
                                getActivity().finish();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyApiRequest.showVolleyError(context, error);
                    }
                }).enqueRequest(null, Request.Method.GET);
    }

    private LinearLayoutManager setSmoothScroll() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity()) {

            @Override
            public void smoothScrollToPosition(RecyclerView recyclerView, RecyclerView.State state, int position) {
                LinearSmoothScroller smoothScroller = new LinearSmoothScroller(getActivity()) {

                    private static final float SPEED = 300f;// Change this value (default=25f)

                    @Override
                    protected float calculateSpeedPerPixel(DisplayMetrics displayMetrics) {
                        return SPEED / displayMetrics.densityDpi;
                    }

                };
                smoothScroller.setTargetPosition(position);
                startSmoothScroll(smoothScroller);
            }
        };
        return layoutManager;
    }

    public void showFirst3Profiles() {

        try {
            listDataMembers = new ArrayList<>();

            RealmResults<MyProfiles> myProfiles = RealmController.with(getActivity()).getMyProfiles();

            if (myProfiles != null && myProfiles.size() > 0) {

                for (MyProfiles profiles : myProfiles) {

                    ProfilesList profilesList = RealmController.getRealmtoPojo(profiles);

                    if (listDataMembers.size() == 2) {
                        break;
                    } else {
                        if (!qkPreferences.getValues(QKPreferences.USER_UNIQUECODE).equalsIgnoreCase(profilesList.getUniqueCode())) {
                            if (profiles.getIsOwnProfile() != null && profiles.getIsOwnProfile().equals("true")) {
                                NavModelItems navModelItems = new NavModelItems();
                                navModelItems.setId(profilesList.getId());
                                navModelItems.setType(profilesList.getType());
                                navModelItems.setUniqueCode(profilesList.getUniqueCode());
                                navModelItems.setUser_id(profilesList.getUser_id());
                                navModelItems.setStatus(profilesList.getStatus());
                                navModelItems.setRole(profilesList.getRole());
                                navModelItems.setRole_name(profilesList.getRole_name());
                                navModelItems.setViewType(NavItemAdapter.viewTypeProfile);

                                if (profiles.getType().equals("" + Constants.TYPE_PROFILE_EMPLOYEE)) {
                                    String companyProfile = profilesList.getCompany_profile();
                                    if (companyProfile != null && !companyProfile.isEmpty()) {
                                        try {
                                            JSONObject jsonObject = new JSONObject(companyProfile);
                                            String profile_image = jsonObject.optString("logo");
                                            profilesList.setLogo(profile_image);
                                            String profile_name = jsonObject.optString("name");
                                            profilesList.setName(profile_name);
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }
                                navModelItems.setTitle(profilesList.getName());
                                navModelItems.setLogo(profilesList.getLogo());

                                if (isMyProfile(navModelItems.getUniqueCode()))
                                    listDataMembers.add(navModelItems);
                            }
                        }
                    }
                }

                filladapter();

                if (listDataMembers != null && listDataMembers.size() < 3) {
                    for (int j = 0; j < listDataMembers.size(); j++) {
                        modelitems.add(j + 1, listDataMembers.get(j));
                    }
                    if (navItemAdapter != null)
                        navItemAdapter.notifyDataSetChanged();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean isMyProfile(String uniquecode) {
        List<String> myprofiles = qkPreferences.getMyProfilesIds();
        if (myprofiles != null)
            for (String s : myprofiles) {
                if (s.equals(uniquecode))
                    return true;
            }

        return false;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        showFirst3Profiles();
    }
}
