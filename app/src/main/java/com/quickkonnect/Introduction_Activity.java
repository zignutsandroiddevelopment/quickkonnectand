package com.quickkonnect;

import android.annotation.SuppressLint;
import android.app.ActivityOptions;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.Html;
import android.text.Selection;
import android.text.SpannableStringBuilder;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.activities.ConfirmRegistration;
import com.adapter.HomeAdapter;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.code.linkedinapi.client.LinkedInApiClient;
import com.google.code.linkedinapi.client.LinkedInApiClientFactory;
import com.google.code.linkedinapi.client.enumeration.ProfileField;
import com.google.code.linkedinapi.client.oauth.LinkedInAccessToken;
import com.google.code.linkedinapi.schema.Person;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.interfaces.CallBackGetResult;
import com.interfaces.CallBackImageDownload;
import com.linkedin.LinkedinDialog;
import com.linkedin.platform.APIHelper;
import com.linkedin.platform.LISessionManager;
import com.linkedin.platform.errors.LIApiError;
import com.linkedin.platform.errors.LIAuthError;
import com.linkedin.platform.listeners.ApiListener;
import com.linkedin.platform.listeners.ApiResponse;
import com.linkedin.platform.listeners.AuthListener;
import com.linkedin.platform.utils.Scope;
import com.models.logindata.ModelLoggedUser;
import com.retrofitservice.ApiUtils;
import com.rilixtech.CountryCodePicker;
import com.services.BackgroundService;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;
import com.utilities.Constants;
import com.utilities.DownloadImage;
import com.utilities.QKPreferences;
import com.utilities.Utilities;
import com.volleyapirequest.VolleyApiRequest;
import com.volleyapirequest.VolleyCallBack;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.webkit.URLUtil.isValidUrl;
import static com.linkedin.Config.LINKEDIN_CONSUMER_KEY;
import static com.linkedin.Config.LINKEDIN_CONSUMER_SECRET;
import static com.linkedin.Config.LINKEDIN_PACKAGE_NAME;

public class Introduction_Activity extends AppCompatActivity implements View.OnClickListener {

    private Context context;
    private CoordinatorLayout coordinator;
    private ViewPager pager;
    private ImageView imgBackBottomsheet;
    private LinearLayout linearFBLogin, linearInstaLogin, linearMobileLogin;
    private LoginButton login_button;
    private TextView tvTemrsConditions;

    private CallbackManager callbackManager;
    private QKPreferences qkPreferences;

    private TextView tvTermsPrivacySheetTitle;
    private WebView webView;
    private ProgressBar progressBar;
    private BottomSheetBehavior bottomSheetTermsPrivacy;

    private LinkedInApiClient client;
    final LinkedInApiClientFactory factory = LinkedInApiClientFactory.newInstance(LINKEDIN_CONSUMER_KEY, LINKEDIN_CONSUMER_SECRET);
    private LinkedInAccessToken accessToken = null;

    private String LoginSocialMediaType = "";

    public static final String CONTENT_ALER_PRIVACY = "<html>\n" +
            "<body>\n" +
            "By Signing in, you agree with our " +
            "<a href=\"http://quickkonnect.com/terms-of-use.html\">Terms and Services</a> and " +
            "<a href=\"http://quickkonnect.com/privacy-contract.html\">Privacy Policy</a></body></html>";

    ProfileTracker profileTracker;

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (profileTracker != null) {
            profileTracker.stopTracking();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        setContentView(R.layout.activity_introduction);

        Utilities.darkenStatusBar(this, R.color.bg);

        FacebookSdk.sdkInitialize(this.getApplicationContext());

        if (Build.VERSION.SDK_INT >= 21) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        qkPreferences = new QKPreferences(Introduction_Activity.this);

        context = Introduction_Activity.this;
        initViews();
    }

    @SuppressWarnings("ConstantConditions")
    private void initViews() {

        HomeAdapter adapter = new HomeAdapter(context);
        adapter.setData(createPageList());

        pager = findViewById(R.id.viewPager);
        pager.setAdapter(adapter);

        linearFBLogin = findViewById(R.id.linearFBLogin);
        linearInstaLogin = findViewById(R.id.linearInstaLogin);
        linearMobileLogin = findViewById(R.id.linearMobileLogin);

        login_button = findViewById(R.id.login_button);
        login_button.setReadPermissions(AppClass.facebook_permission);
        callbackManager = CallbackManager.Factory.create();

        tvTemrsConditions = findViewById(R.id.tvTemrsConditions);

        coordinator = findViewById(R.id.coordinator);

        imgBackBottomsheet = findViewById(R.id.imgBackBottomsheet);

        tvTermsPrivacySheetTitle = findViewById(R.id.tvTermsPrivacySheetTitle);
        webView = findViewById(R.id.webView);
        progressBar = findViewById(R.id.progrssbar_tandc);
        progressBar.setVisibility(View.VISIBLE);

        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setDomStorageEnabled(true);
        webView.clearView();
        webView.measure(100, 100);
        webView.getSettings().setUseWideViewPort(true);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.setVisibility(View.GONE);

        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                webView.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.GONE);
                view.loadUrl(url);
                return false;
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                webView.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.GONE);
            }
            /*
            @SuppressLint("NewApi")
            @Override
            public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
                //super.onReceivedSslError(view, handler, error);
                // this will ignore the Ssl error and will go forward to your site
                handler.proceed();L
                error.getCertificate();
            }*/

        });

        linearFBLogin.setOnClickListener(this);
        linearInstaLogin.setOnClickListener(this);
        linearMobileLogin.setOnClickListener(this);

        imgBackBottomsheet.setOnClickListener(this);

        customTextView(context, tvTemrsConditions);

        bottomSheetTermsPrivacy = BottomSheetBehavior.from(findViewById(R.id.bottomSheetTermsPrivacy));

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tab_dots);
        tabLayout.setupWithViewPager(pager, true);

        try {
            LoginManager.getInstance().logOut();
        } catch (Exception e) {
            e.printStackTrace();
        }

        login_button.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.d("FB TOKEN: ", "" + loginResult.getAccessToken());
                getFacebookUserData(loginResult.getAccessToken());
            }

            @Override
            public void onCancel() {
            }

            @Override
            public void onError(FacebookException exception) {
                Utilities.showToast(context, exception.getMessage());
            }
        });

        verifyemail(getIntent());
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        verifyemail(intent);
    }

    public void verifyemail(Intent intent) {

        Uri data = intent.getData();

        if (data != null && !data.getPath().isEmpty()) {

            if (isLoggedIn()) {

                startActivity(new Intent(getApplicationContext(), DashboardActivity.class)
                        .putExtra("url", data.toString())
                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                Introduction_Activity.this.finish();

            } else {

                String path = data.toString();
                if (path != null && !path.isEmpty() && path.contains("email_token")) {
                    verifyEmail(data.toString());
                }
            }

        } else {

            String path = getIntent().getExtras().getString("url");
            if (path != null && !path.isEmpty()) {
                verifyEmail(path);
            }
        }
    }

    public void verifyEmail(String path) {
        if (Utilities.isNetworkConnected(context)) {
            final ProgressDialog progressDialog = Utilities.showProgress(context);
            String url = path + "?json=true";
            ApiUtils.getSOService().getEmailVerify(url).enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (progressDialog != null && progressDialog.isShowing())
                        progressDialog.dismiss();
                    try {

                        JSONObject jsonObject = new JSONObject(response.body().string());
                        if (jsonObject != null) {

                            String status = jsonObject.optString("status");
                            if (status != null && !status.isEmpty() && status.equalsIgnoreCase("200"))
                                Utilities.showToast(context, "" + jsonObject.optString("msg"));

                            else
                                Utilities.showToast(context, getResources().getString(R.string.failedtoverifyemail));

                        } else
                            Utilities.showToast(context, getResources().getString(R.string.failedtoverifyemail));

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    if (progressDialog != null && progressDialog.isShowing())
                        progressDialog.dismiss();
                    Utilities.showToast(context, "Email verification is failed,please try again");
                }
            });
        }
    }

    public boolean isLoggedIn() {
        try {

            ModelLoggedUser modelLoggedUser = qkPreferences.getLoggedUser();

            if (modelLoggedUser != null) {

                String qk_tag_info = "" + modelLoggedUser.getData().getQrCode();

                if (qk_tag_info != null && Utilities.isEmpty(qk_tag_info))
                    return true;
                else
                    return false;

            } else return false;

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
        LISessionManager.getInstance(getApplicationContext()).onActivityResult(this, requestCode, resultCode, data);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Introduction_Activity.this.finish();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.linearFBLogin:
                if (Utilities.isNetworkConnected(context))
                    Utilities.showTermsCondpopUp(Introduction_Activity.this, "Alert", Constants.CONTENT_ALER_PRIVACY,
                            new CallBackGetResult() {
                                @Override
                                public void onSuccess() {
                                    login_button.performClick();
                                }

                                @Override
                                public void onCancel() {

                                }
                            });
                else
                    Utilities.showToast(context, "No internet connection, please try after some time.");

                break;

            case R.id.linearInstaLogin:
                //for Linked in
                if (Utilities.isNetworkConnected(context))
                    Utilities.showTermsCondpopUp(Introduction_Activity.this, "Alert", Constants.CONTENT_ALER_PRIVACY,
                            new CallBackGetResult() {
                                @Override
                                public void onSuccess() {
                                    boolean isAppInstalled = Utilities.appInstalledOrNot(Introduction_Activity.this, LINKEDIN_PACKAGE_NAME);
                                    if (isAppInstalled) {
                                        LinkedInMapping();
                                    } else {
                                        linkedInLoginWithoutApp();
                                    }
                                }

                                @Override
                                public void onCancel() {
                                }
                            });
                else
                    Utilities.showToast(context, "No internet connection, please try after some time.");
                break;

            case R.id.linearMobileLogin:
                // for login with email
                redirecttoLogin(view);
                break;

            case R.id.imgBackBottomsheet:
                bottomSheetTermsPrivacy.setState(BottomSheetBehavior.STATE_COLLAPSED);
                break;
        }
    }

    public void redirecttoLogin(View view) {
        Intent intent = new Intent(this, LoginActivity.class);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            // Apply activity transition
            // create the transition animation - the images in the layouts
            // of both activities are defined with android:transitionName="robot"
            ActivityOptions options = ActivityOptions
                    .makeSceneTransitionAnimation(this, view, "robot");
            // start the new activity
            startActivity(intent, options.toBundle());
        } else {
            // Swap without transition
            startActivity(intent);
        }
    }

    @NonNull
    private List<View> createPageList() {
        List<View> pageList = new ArrayList<>();
        pageList.add(createPageView(0));
        pageList.add(createPageView(1));
        pageList.add(createPageView(2));
        pageList.add(createPageView(3));

        return pageList;
    }

    @NonNull
    private View createPageView(int pos) {
        View view = LayoutInflater.from(context).inflate(R.layout.view_introduction, null);
        TextView tvTtitlesubText = view.findViewById(R.id.tvTtitlesubText);
        TextView tvTtitleText = view.findViewById(R.id.tvTtitleText);
        ImageView imgIcon = view.findViewById(R.id.imgIcon);
        switch (pos) {
            case 0:
                tvTtitlesubText.setText(getResources().getString(R.string.intro_1_title));
                tvTtitleText.setText(getResources().getString(R.string.intro_1));
                imgIcon.setImageResource(R.drawable.intro_one);
                break;
            case 1:
                tvTtitlesubText.setText(getResources().getString(R.string.intro_2_title));
                tvTtitleText.setText(getResources().getString(R.string.intro_2));
                imgIcon.setImageResource(R.drawable.intro_two);
                break;
            case 2:
                tvTtitlesubText.setText(getResources().getString(R.string.intro_3_title));
                tvTtitleText.setText(getResources().getString(R.string.intro_3));
                imgIcon.setImageResource(R.drawable.intro_three);
                break;
            case 3:
                tvTtitlesubText.setText(getResources().getString(R.string.intro_4_title));
                tvTtitleText.setText(getResources().getString(R.string.intro_4));
                imgIcon.setImageResource(R.drawable.intro_four);
                break;
        }
        return view;
    }

    public void customTextView(final Context context, TextView view) {
        //https://stackoverflow.com/questions/28720117/multiple-clickable-links-in-textview-on-android
        SpannableStringBuilder spanTxt = new SpannableStringBuilder(
                "By Signing in, you agree with our ");
        String terms = "<font color='#2ABBBE'> Term of services</font>";
        spanTxt.append(Html.fromHtml(terms));
        spanTxt.setSpan(new ClickableSpan() {
            @Override
            public void onClick(View widget) {
                tvTermsPrivacySheetTitle.setText("Term of services");
                webView.setVisibility(View.GONE);
                progressBar.setVisibility(View.VISIBLE);
                bottomSheetTermsPrivacy.setState(BottomSheetBehavior.STATE_EXPANDED);
                webView.loadUrl("https://quickkonnect.com/terms-of-use");

            }
        }, spanTxt.length() - " Term of services".length(), spanTxt.length(), 0);
        spanTxt.append(" and ");
        String policy = "<font color='#2ABBBE'> Privacy Policy</font>";
        spanTxt.append(Html.fromHtml(policy));
        spanTxt.setSpan(new ClickableSpan() {
            @Override
            public void onClick(View widget) {
                tvTermsPrivacySheetTitle.setText("Privacy Policy");
                webView.setVisibility(View.GONE);
                progressBar.setVisibility(View.VISIBLE);
                bottomSheetTermsPrivacy.setState(BottomSheetBehavior.STATE_EXPANDED);
                webView.loadUrl("https://quickkonnect.com/privacy-contract");
            }
        }, spanTxt.length() - " Privacy Policy".length(), spanTxt.length(), 0);
        view.setMovementMethod(LinkMovementMethod.getInstance());
        view.setText(spanTxt, TextView.BufferType.SPANNABLE);
    }

    public void getFacebookUserData(AccessToken accessToken) {

        profileTracker = new ProfileTracker() {
            @Override
            protected void onCurrentProfileChanged(Profile oldProfile, Profile currentProfile) {
                Profile profile = currentProfile;

                if (profile != null) {
                    JSONObject objectProfile = new JSONObject();
                    try {
                        objectProfile.put("id", profile.getId());
                        objectProfile.put("name", profile.getName());
                        objectProfile.put("first_name", profile.getFirstName());
                        objectProfile.put("last_name", profile.getLastName());
                        objectProfile.put("link", profile.getLinkUri());

                        JSONObject jsonObject = new JSONObject();
                        try {

                            String facebook_id = profile.getId();

                            jsonObject.put("social_media_user_id", facebook_id);
                            jsonObject.put("social_media_id", Constants.SOCIAL_FACEBOOK_ID);
                            jsonObject.put("device_type", "Android");
                            if (FirebaseInstanceId.getInstance() != null &&
                                    FirebaseInstanceId.getInstance().getToken() != null)
                                jsonObject.put("device_id", FirebaseInstanceId.getInstance().getToken());
                            else
                                jsonObject.put("device_id", "");

                            jsonObject.put("data", objectProfile);

                            Log.d("Data", "request object: " + jsonObject);

                            sendSocialData(jsonObject);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        };

        profileTracker.startTracking();


//        GraphRequest request = GraphRequest.newMeRequest(accessToken.getCurrentAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
//            @Override
//            public void onCompleted(JSONObject object, GraphResponse response) {
//                JSONObject jsonObject = new JSONObject();
//                try {
//                    LoginSocialMediaType = "1";
//
//                    String facebook_id = object.optString("id");
//
//                    String link = object.optString("link");
//
//                    jsonObject.put("social_media_user_id", facebook_id);
//                    jsonObject.put("social_media_id", Constants.SOCIAL_FACEBOOK_ID);
//                    jsonObject.put("device_type", "Android");
//                    if (link != null && !link.equalsIgnoreCase("") && !link.equalsIgnoreCase("null"))
//                        object.put("publicProfileUrl", link + "");
//
//                    if (FirebaseInstanceId.getInstance() != null && FirebaseInstanceId.getInstance().getToken() != null)
//                        jsonObject.put("device_id", FirebaseInstanceId.getInstance().getToken());
//                    else
//                        jsonObject.put("device_id", "");
//
//                    jsonObject.put("data", object);
//
//                    Log.d("request object: ", "" + jsonObject);
//
//                    sendSocialData(jsonObject);
//
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
//        });
//        Bundle parameters = new Bundle();
//        parameters.putString("fields", "id,name,first_name,music,last_name,email,location{location},education,website,work,friendlists,friends,link,about,birthday,hometown,books,events,religion,political,tagged_places");
//        request.setParameters(parameters);
//        request.executeAsync();
    }

    private static Scope buildScope() {
        return Scope.build(Scope.R_BASICPROFILE, Scope.R_EMAILADDRESS);
    }

    public void LinkedInMapping() {
        LISessionManager.getInstance(context).init(Introduction_Activity.this, buildScope(), new AuthListener() {
            @Override
            public void onAuthSuccess() {
                final APIHelper apiHelper = APIHelper.getInstance(getApplicationContext());
                apiHelper.getRequest(context, AppClass.LINKEDIN_URL, new ApiListener() {
                    @Override
                    public void onApiSuccess(final ApiResponse apiResponse) {
                        Log.d("Linkedin ModelLogedData", "" + apiResponse.getResponseDataAsJson());
                        JSONObject objData = apiResponse.getResponseDataAsJson();
                        try {
                            LoginSocialMediaType = "2";

                            JSONObject jsonObject = new JSONObject();

                            String linkedIn_id = objData.getString("id");

                            jsonObject.put("social_media_user_id", linkedIn_id);
                            jsonObject.put("social_media_id", Constants.SOCIAL_LINKEDIN_ID);
                            jsonObject.put("device_type", "Android");
                            if (FirebaseInstanceId.getInstance() != null && FirebaseInstanceId.getInstance().getToken() != null)
                                jsonObject.put("device_id", FirebaseInstanceId.getInstance().getToken());
                            else
                                jsonObject.put("device_id", "");

                            jsonObject.put("data", objData);

                            Log.d("request object: ", jsonObject + "");

                            sendSocialData(jsonObject);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onApiError(LIApiError liApiError) {
                        Log.e("Linked_in_auth", liApiError + "" + liApiError.getApiErrorResponse());
                        //Utilities.showToast(context, "" + liApiError.getHttpStatusCode());
                    }
                });
            }

            @Override
            public void onAuthError(LIAuthError error) {
                //Utilities.showToast(context, "" + error.toString());
            }
        }, true);

    }

    private void linkedInLoginWithoutApp() {
        ProgressDialog progressDialog = null;
        LinkedinDialog d = null;
        try {
            progressDialog = new ProgressDialog(this);
            d = new LinkedinDialog(this, progressDialog);
            d.show();
            d.setVerifierListener(new LinkedinDialog.OnVerifyListener() {
                @SuppressLint("NewApi")
                public void onVerify(String verifier) {
                    Log.i("LinkedIn Verifier", "" + verifier);
                    try {
                        LoginSocialMediaType = "2";

                        accessToken = LinkedinDialog.oAuthService.getOAuthAccessToken(LinkedinDialog.liToken, verifier);
                        factory.createLinkedInApiClient(accessToken);
                        client = factory.createLinkedInApiClient(accessToken);
                        Log.d("LinkedIn Token", "" + accessToken.getToken());
                        Log.d("LinkedIn Secret", "" + accessToken.getTokenSecret());
                        Log.d("LinkedIn Secret", client.getAccessToken().getTokenSecret());
                        Person p = client.getProfileForCurrentUser(EnumSet.of(
                                ProfileField.ID, ProfileField.FIRST_NAME,
                                ProfileField.PHONE_NUMBERS, ProfileField.LAST_NAME,
                                ProfileField.HEADLINE, ProfileField.INDUSTRY,
                                ProfileField.PICTURE_URL, ProfileField.PUBLIC_PROFILE_URL,
                                ProfileField.DATE_OF_BIRTH,
                                ProfileField.LOCATION_NAME, ProfileField.MAIN_ADDRESS,
                                ProfileField.LOCATION_COUNTRY));
                        /* Person p = client.getProfileForCurrentUser(EnumSet.of(
                                ProfileField.ID, ProfileField.FIRST_NAME,
                                ProfileField.PHONE_NUMBERS, ProfileField.LAST_NAME,
                                ProfileField.HEADLINE, ProfileField.INDUSTRY,
                                ProfileField.PICTURE_URL, ProfileField.DATE_OF_BIRTH,
                                ProfileField.LOCATION_NAME, ProfileField.MAIN_ADDRESS,
                                ProfileField.LOCATION_COUNTRY));*/
                        Log.d("pppp", p + "");

                        JSONObject jsonObject = new JSONObject();

                        jsonObject.put("social_media_user_id", p.getId());
                        jsonObject.put("social_media_id", Constants.SOCIAL_LINKEDIN_ID);
                        jsonObject.put("device_type", "Android");
                        if (FirebaseInstanceId.getInstance() != null &&
                                FirebaseInstanceId.getInstance().getToken() != null)
                            jsonObject.put("device_id", FirebaseInstanceId.getInstance().getToken());
                        else
                            jsonObject.put("device_id", "");

                        JSONObject jsonLinkedinData = new JSONObject();

                        if (p.getPublicProfileUrl() != null)
                            jsonLinkedinData.put("publicProfileUrl", p.getPublicProfileUrl());

                        if (p.getFirstName() != null)
                            jsonLinkedinData.put("firstName", p.getFirstName());

                        if (p.getId() != null)
                            jsonLinkedinData.put("id", p.getId());
                        else
                            jsonLinkedinData.put("id", "");


                        if (p.getIndustry() != null)
                            jsonLinkedinData.put("industry", p.getIndustry());
                        else
                            jsonLinkedinData.put("industry", "");

                        if (p.getLastName() != null)
                            jsonLinkedinData.put("lastName", p.getLastName());
                        else
                            jsonLinkedinData.put("lastName", "");

                        JSONObject locationJson = new JSONObject();
                        if (p.getLocation() != null && p.getLocation().getName() != null)
                            locationJson.put("name", p.getLocation().getName());
                        else
                            locationJson.put("name", "");

                        JSONObject country = new JSONObject();
                        if (p.getLocation() != null && p.getLocation().getCountry() != null && p.getLocation().getCountry().getCode() != null)
                            country.put("code", p.getLocation().getCountry().getCode());
                        else
                            country.put("code", "");

                        locationJson.put("country", country);
                        jsonLinkedinData.put("location", locationJson);

                        jsonObject.put("data", jsonLinkedinData);

                        sendSocialData(jsonObject);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
            progressDialog.setMessage("Loading...");
            progressDialog.setCancelable(true);
            progressDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void sendSocialData(JSONObject jsonObject) {
        new VolleyApiRequest(context, true, VolleyApiRequest.REQUEST_BASEURL + VolleyApiRequest.API_CREATE_USER,
                new VolleyCallBack() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String status = response.getString("status");
                            String message = response.getString("msg");
                            if (status.equals("200")) {
                                qkPreferences.storeLoginFrom("SOCIAL");
                                parseResponse(response);
                            } else {
                                Utilities.showToast(context, message);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyApiRequest.showVolleyError(context, error);
                    }
                }).enqueRequest(jsonObject, Request.Method.POST);
    }

    public void parseResponse(JSONObject data) {

        try {
            ModelLoggedUser modelLoggedUser = new Gson().fromJson(data.toString(), ModelLoggedUser.class);
            if (modelLoggedUser != null && modelLoggedUser.getStatus() == 200) {

                qkPreferences.storeLoggedUser(modelLoggedUser);
                qkPreferences.storeApiToken(modelLoggedUser.getApiToken() + "");

                JSONObject qktagInfo = new JSONObject();
                qktagInfo.put(Constants.QKTAGURL, modelLoggedUser.getData().getQrCode());
                qktagInfo.put(Constants.QKMIDDLETAGURL, "");

                if (modelLoggedUser.getData().getBorderColor() != null && !modelLoggedUser.getData().getBorderColor().toString().isEmpty()
                        && !modelLoggedUser.getData().getBorderColor().toString().equals("0"))
                    qktagInfo.put(Constants.QKBORDERCOLOR, modelLoggedUser.getData().getBorderColor().toString());
                else
                    qktagInfo.put(Constants.QKBORDERCOLOR, getResources().getString(R.string.default_border));

                if (modelLoggedUser.getData().getBackgroundColor() != null && !modelLoggedUser.getData().getBackgroundColor().toString().isEmpty()
                        && !modelLoggedUser.getData().getBackgroundColor().toString().equals("0"))
                    qktagInfo.put(Constants.QKBACKCOLOR, modelLoggedUser.getData().getBackgroundColor().toString());
                else
                    qktagInfo.put(Constants.QKBACKCOLOR, getResources().getString(R.string.default_back));

                if (modelLoggedUser.getData().getPatternColor() != null && !modelLoggedUser.getData().getPatternColor().toString().isEmpty()
                        && !modelLoggedUser.getData().getPatternColor().toString().equals("0"))
                    qktagInfo.put(Constants.QKPATTERNCOLOR, modelLoggedUser.getData().getPatternColor().toString());
                else
                    qktagInfo.put(Constants.QKPATTERNCOLOR, getResources().getString(R.string.default_data));

                qkPreferences.storeQKInfo(qktagInfo.toString());

//                SharedPreferences.Editor ed = mSharedPreferences.edit();
//                ed.putString(SharedPreference.CURRENT_USER_ID, "" + modelLoggedUser.getData().getId());
//                ed.putString(SharedPreference.CURRENT_USER_NAME, modelLoggedUser.getData().getFirstname()
//                        + " " + modelLoggedUser.getData().getLastname());
//                ed.putString(SharedPreference.CURRENT_USER_FIRSTTIME, "" + modelLoggedUser.getData().getFirsttime());
//                ed.putString(SharedPreference.CURRENT_USER_EMAIL, modelLoggedUser.getData().getEmail());
//                ed.putString(SharedPreference.CURRENT_USER_FULLNAME, modelLoggedUser.getData().getFirstname()
//                        + " " + modelLoggedUser.getData().getLastname());
//                ed.putString(SharedPreference.CURRENT_USER_PROFILE_PIC, "" + modelLoggedUser.getData().getProfilePic());
//                ed.putString(SharedPreference.CURRENT_UNIQU_QK_TAG_NUMBER, modelLoggedUser.getData().getUniqueCode());
//                ed.putString(SharedPreference.NOTIFICATION_STATUS, "" + modelLoggedUser.getData().getNotificationStatus());
//                ed.putString(Constants.QKTAGURL, "" + modelLoggedUser.getData().getQrCode());
//                ed.apply();

                Object middle_tag = modelLoggedUser.getData().getMiddleTag();
                if (middle_tag != null && !middle_tag.toString().isEmpty() && !middle_tag.toString().equals("null")) {
                    new DownloadImage(context, new CallBackImageDownload() {
                        @Override
                        public void getBase64(String mData) {
                            try {
                                if (mData != null) {
                                    JSONObject jsonObject = new JSONObject(qkPreferences.getQKInfo());
                                    jsonObject.put(Constants.QKMIDDLETAGURL, mData);
                                    qkPreferences.storeQKInfo(jsonObject.toString());
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void getBitmap(Bitmap mData) {
                        }
                    }).execute(middle_tag.toString());
                }

                /*
                 *  for Connecting and login to XMPP openFire
                 */
                try {
                    // Already started in Splash therefor have to stop
                    stopService(new Intent(getApplicationContext(), BackgroundService.class));
                    // start again for connection
                    startService(new Intent(getApplicationContext(), BackgroundService.class));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                /*********
                 * if status = 1 => socialmedia is integrated
                 * if status = 0 => not integrated
                 * status = 1= check public url ==> if (null){ open popup for email}else( go ahead)
                 ****/

                try {
                    if (LoginSocialMediaType != null && LoginSocialMediaType.equalsIgnoreCase("1") && !LoginSocialMediaType.equalsIgnoreCase("")) {
                        if (modelLoggedUser.getData().getFacebook() != null && modelLoggedUser.getData().getFacebook().getStatus() == 1) {
                            if (modelLoggedUser.getData().getFacebook().getData().getPublicProfileUrl() != null && !modelLoggedUser.getData().getFacebook().getData().getPublicProfileUrl().equalsIgnoreCase("") && !modelLoggedUser.getData().getFacebook().getData().getPublicProfileUrl().equalsIgnoreCase("null")) {
                                GoAhead(modelLoggedUser);
                            } else {
                                openSocialMediaEmailDialog(modelLoggedUser, 1);
                            }
                        } else {
                            GoAhead(modelLoggedUser);
                        }
                    } else if (LoginSocialMediaType != null && LoginSocialMediaType.equalsIgnoreCase("2") && !LoginSocialMediaType.equalsIgnoreCase("")) {
                        if (modelLoggedUser.getData().getLinkedin() != null && modelLoggedUser.getData().getLinkedin().getStatus() == 1) {
                            if (modelLoggedUser.getData().getLinkedin().getData().getPublicProfileUrl() != null && !modelLoggedUser.getData().getLinkedin().getData().getPublicProfileUrl().equalsIgnoreCase("") && !modelLoggedUser.getData().getLinkedin().getData().getPublicProfileUrl().equalsIgnoreCase("null")) {
                                GoAhead(modelLoggedUser);
                            } else {
                                openSocialMediaEmailDialog(modelLoggedUser, 2);
                            }
                        } else {
                            GoAhead(modelLoggedUser);
                        }
                    } else {
                        GoAhead(modelLoggedUser);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else
                Utilities.showToast(context, getResources().getString(R.string.error_process));
        } catch (Exception e) {
            Utilities.showToast(context, "" + e.getMessage());
        }
    }

    private void GoAhead(ModelLoggedUser modelLoggedUser) {
        if (modelLoggedUser.getData().getRegister() != null && modelLoggedUser.getData().getRegister() == 1) {
            try {
                modelLoggedUser.getData().setStatus(1);
                modelLoggedUser.getData().setRegister(null);
                qkPreferences.storeLoggedUser(modelLoggedUser);
            } catch (Exception e) {
                e.printStackTrace();
            }
            Intent intent = new Intent(context, AddSocialMediaAccountActivity.class);
            intent.putExtra("from", "other");
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        } else {
            if (modelLoggedUser.getData().getStatus() == 1) {
                Intent intent = new Intent(context, AddSocialMediaAccountActivity.class);
                intent.putExtra("from", "other");
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            } else if (modelLoggedUser.getData().getStatus() == 2 || modelLoggedUser.getData().getStatus() == 3) {
                Intent intent = new Intent(context, ConfirmRegistration.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            } else if (modelLoggedUser.getData().getStatus() == 4) {
                Intent intent = new Intent(context, DashboardActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        }
        Introduction_Activity.this.finish();
    }

    String msg = "";
    String Url = "";
    String SOcialMediaId = "";

    private void openSocialMediaEmailDialog(final ModelLoggedUser modelLoggedUser, final int type) {

        final Dialog dialog = new Dialog(Introduction_Activity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View view = LinearLayout.inflate(Introduction_Activity.this, R.layout.dialog_addsocialmedia, null);
        dialog.setContentView(view);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        dialog.show();
        dialog.getWindow().setAttributes(lp);
        TextView tvAlert = view.findViewById(R.id.tvAlert);
        TextView tvDone = view.findViewById(R.id.tvDone);
        TextView tvCancel = view.findViewById(R.id.tvCancel);
        CountryCodePicker ccp = (CountryCodePicker) view.findViewById(R.id.ccp);
        LinearLayout lin_cpp = view.findViewById(R.id.lin_cpp);

        if (type == 1) {
            msg = "Add Facebook URL";
            Url = "https://www.facebook.com/";
            SOcialMediaId = "1";
        } else if (type == 2) {
            msg = "Add LinkedIn URL";
            Url = "https://www.linkedin.com/";
            SOcialMediaId = "2";
        }

        view.findViewById(R.id.divider_select_socialmedia).setVisibility(View.GONE);
        ccp.setVisibility(View.GONE);
        tvAlert.setText(msg);
        final SearchableSpinner spinnerSocialmedia = view.findViewById(R.id.spinnerSocialMedia);
        spinnerSocialmedia.setVisibility(View.GONE);
        final EditText edtSocialmediaLInk = view.findViewById(R.id.edt_socialmedia_link);
        edtSocialmediaLInk.setText(Url);
        Selection.setSelection(edtSocialmediaLInk.getText(), edtSocialmediaLInk.getText().length());

        edtSocialmediaLInk.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!s.toString().startsWith(Url)) {
                    edtSocialmediaLInk.setText(Url);
                    Selection.setSelection(edtSocialmediaLInk.getText(), edtSocialmediaLInk.getText().length());
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        tvDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    Utilities.hidekeyboard(Introduction_Activity.this, view);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                dialog.dismiss();
                if (!edtSocialmediaLInk.getText().toString().isEmpty() && isValidUrl(edtSocialmediaLInk.getText().toString())) {
                    String URL = edtSocialmediaLInk.getText().toString() + "";
                    UploadSocialmedia(URL + "", SOcialMediaId, dialog, modelLoggedUser);
                } else {
                    Utilities.showToast(Introduction_Activity.this, "Please enter valid URL");
                }
            }
        });

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    if (type == 1) {
                        Utilities.hidekeyboard(Introduction_Activity.this, view);
                        LoginManager.getInstance().logOut();
                    } else if (type == 2) {
                        LISessionManager.getInstance(Introduction_Activity.this).clearSession();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void UploadSocialmedia(String Url, String SocialMediaId, final Dialog dialog, final ModelLoggedUser modelLoggedUser) {
        try {
            //final ProgressDialog pd = Utilities.showProgress(LoginActivity.this);
            String userid = qkPreferences.getValuesInt(QKPreferences.USER_ID) + "";
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("user_id", userid);
            jsonObject.put("social_media_id", SocialMediaId + "");
            jsonObject.put("publicProfileUrl", Url + "");
            new VolleyApiRequest(context, true, VolleyApiRequest.REQUEST_BASEURL + "api/update_social_profile_url",
                    new VolleyCallBack() {
                        @Override
                        public void onResponse(JSONObject response) {
                           /* if (pd != null && pd.isShowing()) {
                                pd.dismiss();
                            }*/
                            try {
                                if (response != null && response.length() > 0) {
                                    String ststus = response.getString("status");
                                    String message = response.getString("msg");

                                    if (ststus.equalsIgnoreCase("200")) {
                                        if (dialog != null && dialog.isShowing()) {
                                            dialog.dismiss();
                                        }
                                        GoAhead(modelLoggedUser);
                                    } else
                                        Utilities.showToast(context, message + "");
                                } else

                                    Utilities.showToast(context, "Please try again");

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onErrorResponse(VolleyError error) {
                            /*if (pd != null && pd.isShowing()) {
                                pd.dismiss();
                            }*/
                            //VolleyApiRequest.showVolleyError(context, error);
                        }
                    }).enqueRequest(jsonObject, Request.Method.POST);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}




/*
try {
        if (modelLoggedUser.getData().getFacebook() != null && modelLoggedUser.getData().getFacebook().getStatus() == 1) {
        if (modelLoggedUser.getData().getFacebook().getData().getPublicProfileUrl() != null && !modelLoggedUser.getData().getFacebook().getData().getPublicProfileUrl().equalsIgnoreCase("")
        && !modelLoggedUser.getData().getFacebook().getData().getPublicProfileUrl().equalsIgnoreCase("null")) {
        GoAhead(modelLoggedUser);
        } else {
        openSocialMediaEmailDialog(modelLoggedUser, 1);
        }
        } else if (modelLoggedUser.getData().getLinkedin() != null && modelLoggedUser.getData().getLinkedin().getStatus() == 1) {
        if (modelLoggedUser.getData().getLinkedin().getData().getPublicProfileUrl() != null && !modelLoggedUser.getData().getLinkedin().getData().getPublicProfileUrl().equalsIgnoreCase("") && !modelLoggedUser.getData().getLinkedin().getData().getPublicProfileUrl().equalsIgnoreCase("null")) {
        GoAhead(modelLoggedUser);
        } else {
        openSocialMediaEmailDialog(modelLoggedUser, 2);
        }
        } else {
        GoAhead(modelLoggedUser);
        }
        } catch (Exception e) {
        e.printStackTrace();
        }*/
