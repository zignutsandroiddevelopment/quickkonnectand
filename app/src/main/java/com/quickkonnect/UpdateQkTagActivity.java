package com.quickkonnect;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.adapter.BackgroundAdapter;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.cunoraz.gifview.library.GifView;
import com.customviews.BackgroundColorPicker;
import com.customviews.BorderDataColorPicker;
import com.interfaces.CallBackImageDownload;
import com.interfaces.ImageSelected;
import com.models.logindata.ModelLoggedData;
import com.models.logindata.ModelLoggedUser;
import com.realmtable.MyProfiles;
import com.realmtable.QkTagBase64;
import com.realmtable.RealmController;
import com.services.CreateEmployeeProfileService;
import com.services.LoadProfiles;
import com.utilities.CompressImage;
import com.utilities.Constants;
import com.utilities.DownloadImage;
import com.utilities.QKPreferences;
import com.utilities.Utilities;
import com.volleyapirequest.VolleyApiRequest;
import com.volleyapirequest.VolleyCallBack;
import com.yalantis.ucrop.UCrop;

import net.quikkly.android.Quikkly;
import net.quikkly.android.QuikklyBuilder;
import net.quikkly.android.ui.RenderTagView;
import net.quikkly.core.Skin;

import org.apache.commons.io.IOUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import io.reactivex.annotations.NonNull;

public class UpdateQkTagActivity extends AppCompatActivity implements View.OnClickListener {

    private Context context;
    private static final String TAG = "TAG";
    //private SOService mService;
    private QKPreferences qkPreferences;
    private TextView tvRemovePhoto, tvSelectphoto, tvalertmsg;
    private String template;
    private TextView textView_toolbar_update_qk;
    private BigInteger quikklyUniqueCode, quikklyUniqueCode_for_employee;
    private Skin skin;
    private String Base64Image, Base64Image_for_employee;
    private RenderTagView renderView, render_tag_employee;
    private BorderDataColorPicker borderColor;
    private BackgroundColorPicker maskColor;
    private BorderDataColorPicker dataColor;
    private LinearLayout llremovephoto;
    private MarshMallowPermission marshMallowPermission;
    private File mediaFile = null;
    //private File croppedFile = null;
    private View border_view;
    private static final int PICK_FROM_CAMERA = 1;
    private static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 5;
    private static final int REQUEST_STORAGE_READ_ACCESS_PERMISSION = 6;
    private Bitmap selectedImage = null;
    private String background_color, border_color, data_color, qktag_base64, from = "", profile_middletag = "", logo = "";
    public final static int MASK = 0xff;
    private static final int REQUEST_SELECT_PICTURE = 0x01;
    private int profile_type = 0, profile_id = -1, user_id = -1;
    private ModelLoggedData modelLoggedData;
    private int current_selected = 0;
    private String Background = "#0BC5AD";
    private RelativeLayout rel_qkTag;
    private String KEY_PROFILE_TYPE = "1", login_user_id = "";
    private RecyclerView rec_select_background;
    private ArrayList<BgList> bglist;
    private BackgroundAdapter adapter;
    private int colors[] = null;
    private TextView save;
    private LinearLayout lin_choosefile;
    private boolean is_from_employee = false, isCreateEmployee = false, isCreateFromProfile = false, isCreateEmp = false;
    private String name = "", profile_pic = "", profile_ID = "", about = "";
    private String key_unique_code_from = "";
    private String key_employee_unique_code_from = "";
    private boolean isFileupload = false, isFileremove = false;
    private String prevborderColor = "", prevtagbackgroundColor = "", prevdataColor = "", prevbackgroundColor = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_qk_tag);

        Utilities.ChangeStatusBar(UpdateQkTagActivity.this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        textView_toolbar_update_qk = findViewById(R.id.textView_toolbar_update_qk);
        setSupportActionBar(toolbar);

        //mService = ApiUtils.getSOService();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close_white_24dp);

        context = UpdateQkTagActivity.this;

        qkPreferences = new QKPreferences(UpdateQkTagActivity.this);
        marshMallowPermission = new MarshMallowPermission(this);
        login_user_id = "" + qkPreferences.getValuesInt(QKPreferences.USER_ID);

        from = getIntent().getExtras().getString("from");
        try {
            KEY_PROFILE_TYPE = getIntent().getExtras().getString(Constants.KEY_PROFILETYPE);

            String str = getIntent().getExtras().getString(Constants.KEY_CREATE_COMPONY_PROFILE);
            if (str != null && str.equalsIgnoreCase("1")) {
                isCreateEmployee = true;
                name = getIntent().getExtras().getString(Constants.KEY_CREATE_COMPONY_PROFILE_NAME) + "";
                profile_pic = getIntent().getExtras().getString(Constants.KEY_CREATE_COMPONY_PROFILE_IMAGE) + "";
                profile_ID = getIntent().getExtras().getString(Constants.KEY_CREATE_COMPONY_PROFILE_ID) + "";
                about = getIntent().getExtras().getString(Constants.KEY_CREATE_COMPONY_PROFILE_ABOUT) + "";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (from != null && !from.isEmpty()) {
            if (from.equals(Constants.KEY_FROM_DASHBOARD)) {
                textView_toolbar_update_qk.setText(getResources().getString(R.string.updateqktag));
                getSupportActionBar().setTitle("");
            } else if (from.equals("ADD_EMPLOYEE")) {
                is_from_employee = true;
                isCreateFromProfile = true;
                textView_toolbar_update_qk.setText(getResources().getString(R.string.generateqktag));
                getSupportActionBar().setTitle("");
            } else {
                textView_toolbar_update_qk.setText(getResources().getString(R.string.generateqktag));
                getSupportActionBar().setTitle("");
            }
        } else {
            textView_toolbar_update_qk.setText(getResources().getString(R.string.generateqktag));
            getSupportActionBar().setTitle("");
        }

        new QuikklyBuilder()
                .setApiKey(getResources().getString(R.string.quikkly_key))
                .loadDefaultBlueprintFromLibraryAssets(this)
                .build()
                .setAsDefault();

        Quikkly.getDefaultInstance();// Check that Quikkly is set up

        rel_qkTag = findViewById(R.id.rel_qkTag);
        tvalertmsg = findViewById(R.id.tvalertmsg);
        save = findViewById(R.id.tv_save_qk_tag);
        lin_choosefile = findViewById(R.id.lin_choosefile);
        border_view = findViewById(R.id.bottom_line_update_qk);

        if (is_from_employee) {
            lin_choosefile.setVisibility(View.GONE);
            border_view.setVisibility(View.GONE);
        } else {
            lin_choosefile.setVisibility(View.VISIBLE);
            border_view.setVisibility(View.VISIBLE);
        }

        rec_select_background = findViewById(R.id.rec_select_background);
        rec_select_background.setLayoutManager(new GridLayoutManager(UpdateQkTagActivity.this, 2, GridLayoutManager.HORIZONTAL, false));
        rec_select_background.setNestedScrollingEnabled(false);
        rec_select_background.setHasFixedSize(true);

        bglist = new ArrayList<>();
        adapter = new BackgroundAdapter(UpdateQkTagActivity.this, bglist, imageSelected, current_selected);
        rec_select_background.setAdapter(adapter);

        this.gradient_array = new ArrayList<>();
        this.gradient_array.add("1");
        this.gradient_array.add("2");
        this.gradient_array.add("3");
        this.gradient_array.add("4");
        this.gradient_array.add("5");
        this.gradient_array.add("6");
        this.gradient_array.add("7");
        this.gradient_array.add("8");
        this.gradient_array.add("9");
        this.gradient_array.add("10");

        colors = getResources().getIntArray(R.array.gradient_colors);
        String colors_array[] = {"#0BC5AD"
                , "#ED1B24"
                , "#FE5722"
                , "#7B0002"
                , "#2095F2"
                , "#3F51B5"
                , "#FEC107"
                , "#7DA7D9"
                , "#4BB14E"
                , "#EC1D63"};

        for (int i = 0; i < colors.length; i++) {
            BgList blist = new BgList();
            blist.setIsImage(0);
            blist.setColorcode(colors[i]);
            blist.setStrColorcode(colors_array[i]);
            bglist.add(blist);
        }

        try {
            Background = bglist.get(0).getStrColorcode();
            prevbackgroundColor = Background;
        } catch (Exception e) {
            e.printStackTrace();
        }

        tvSelectphoto = findViewById(R.id.tvSelectphoto);
        tvSelectphoto.setOnClickListener(this);

        llremovephoto = findViewById(R.id.llremovephoto);
        tvRemovePhoto = findViewById(R.id.tvRemovePhoto);

        tvRemovePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isFileremove = true;
                tvRemovePhoto.setTag("0");
                selectedImage = null;
                qktag_base64 = null;
                llremovephoto.setVisibility(View.GONE);
                inputChanged();
            }
        });

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Utilities.isNetworkConnected(UpdateQkTagActivity.this)) {
                    if (from != null && from.equals(Constants.KEY_FROM_DASHBOARD)) {

                        if (profile_type == 0)
                            functionGenerateQuicklyCOde(1, Base64Image, "" + user_id, "" + profile_id);

                        else if (profile_type == 1 || profile_type == 2 || profile_type == 3)
                            functionGenerateQuicklyCOde(2, Base64Image, "" + user_id, "" + profile_id);

                        else if (profile_type == 4)
                            functionGenerateQuicklyCOde(3, Base64Image, "" + user_id, "" + profile_id);


                    } else if (from != null && from.equals(Constants.KEY_FROM_CREATEPROFILE)) {
                        functionGenerateQuicklyCOde(2, Base64Image, "" + user_id, "" + +profile_id);

                    } else if (from != null && from.equals("ADD_EMPLOYEE")) {
                        AcceptEmployee();

                    } else
                        functionGenerateQuicklyCOde(1, Base64Image, "" + user_id, "" + profile_id);

                } else
                    Utilities.showToast(UpdateQkTagActivity.this, Constants.NO_INTERNET_CONNECTION);
            }
        });

        if (from != null && from.equals(Constants.KEY_FROM_DASHBOARD)) {
            key_unique_code_from = getIntent().getExtras().getString(Constants.KEY_UNQUECODE);
            quikklyUniqueCode = TextUtils.isEmpty(key_unique_code_from) ? BigInteger.ZERO : new BigInteger(key_unique_code_from);

            try {
                profile_id = Integer.parseInt(getIntent().getExtras().getString(Constants.KEY_PROFILEID));
                profile_type = Integer.parseInt(getIntent().getExtras().getString(Constants.KEY_PROFILETYPE));
                user_id = qkPreferences.getValuesInt(QKPreferences.USER_ID);

                try {
                    if (profile_type == 4) {
                        lin_choosefile.setVisibility(View.GONE);
                        llremovephoto.setVisibility(View.GONE);
                    } else {
                        lin_choosefile.setVisibility(View.VISIBLE);
                        llremovephoto.setVisibility(View.VISIBLE);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                profile_middletag = getIntent().getExtras().getString(Constants.KEY_MIDDLETAG);

                String qk_tag_info = getIntent().getExtras().getString(Constants.KEY_QKTAGINFO);

                try {
                    String background_gradient = getIntent().getExtras().getString(Constants.KEY_BACKGROUND_GRADIENT);
                    if (background_gradient != null && !background_gradient.isEmpty()) {
                        Background = background_gradient;
                        prevbackgroundColor = Background;
                        if (isGradient(background_gradient)) {
                            //rel_qkTag.setBackgroundResource(getBackgroundRes(Integer.parseInt(background_gradient)));
                            rel_qkTag.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                        } else {
                            rel_qkTag.setBackgroundColor(Color.parseColor(background_gradient));
                        }

                    } else {
                        Background = bglist.get(0).getStrColorcode();
                        prevbackgroundColor = Background;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                if (qk_tag_info != null && !qk_tag_info.isEmpty()) {

                    JSONObject data = new JSONObject(qk_tag_info);

                    String borderClr = data.optString(Constants.QKBORDERCOLOR);
                    if (Utilities.isEmpty(borderClr) && !borderClr.equals("0"))
                        border_color = borderClr.replace("#", "0xff");
                    else
                        border_color = getResources().getString(R.string.default_border).replace("#", "0xff");

                    String backgroundClr = data.optString(Constants.QKBACKCOLOR);
                    if (Utilities.isEmpty(backgroundClr) && !backgroundClr.equals("0"))
                        background_color = backgroundClr.replace("#", "0xff");
                    else
                        background_color = getResources().getString(R.string.default_back).replace("#", "0xff");

                    String patternClr = data.optString(Constants.QKPATTERNCOLOR);
                    if (Utilities.isEmpty(patternClr) && !patternClr.equals("0"))
                        data_color = patternClr.replace("#", "0xff");
                    else
                        data_color = getResources().getString(R.string.default_data).replace("#", "0xff");

                } else {

                    border_color = getResources().getString(R.string.default_border).replace("#", "0xff");
                    background_color = getResources().getString(R.string.default_back).replace("#", "0xff");
                    data_color = getResources().getString(R.string.default_data).replace("#", "0xff");
                }

                prevborderColor = border_color;
                prevtagbackgroundColor = background_color;
                prevdataColor = data_color;

                if (Utilities.isEmpty(profile_middletag)) {
                    QkTagBase64 qkTagBase64 = RealmController.with(UpdateQkTagActivity.this).getQKTagsbyID("" + profile_id);
                    if (qkTagBase64 == null) {
                        downloadQkTag(profile_middletag);

                    } else {

                        if (qkTagBase64.getMiddletag_url() != null
                                && !qkTagBase64.getMiddletag_url().isEmpty()
                                && qkTagBase64.getMiddletag_url().equals(profile_middletag)) {

                            qktag_base64 = qkTagBase64.getBase64Data();
                            setupViews();
                            inputChanged();

                        } else {
                            downloadQkTag(profile_middletag);
                        }
                    }

                } else {
                    setupViews();
                    inputChanged();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (from != null && from.equals(Constants.KEY_FROM_CREATEPROFILE)) {
            key_unique_code_from = getIntent().getExtras().getString(Constants.KEY_UNQUECODE);
            quikklyUniqueCode = TextUtils.isEmpty(key_unique_code_from) ? BigInteger.ZERO : new BigInteger(key_unique_code_from);

            border_color = getResources().getString(R.string.default_border).replace("#", "0xff");
            background_color = getResources().getString(R.string.default_back).replace("#", "0xff");
            data_color = getResources().getString(R.string.default_data).replace("#", "0xff");

            try {

                profile_id = Integer.parseInt(getIntent().getExtras().getString(Constants.KEY_CREATEPROFILEID));
                profile_type = Integer.parseInt(getIntent().getExtras().getString(Constants.KEY_PROFILETYPE));
                user_id = qkPreferences.getValuesInt(QKPreferences.USER_ID);

                if (profile_type == 1) {
                    isCreateEmp = true;
                    key_employee_unique_code_from = getIntent().getExtras().getString(Constants.KEY_EMPLOYEE_UNIQUE_CODE);
                    quikklyUniqueCode_for_employee = TextUtils.isEmpty(key_employee_unique_code_from) ? BigInteger.ZERO : new BigInteger(key_employee_unique_code_from);
                }

                setupViews();
                inputChanged();

            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (from != null && from.equals("ADD_EMPLOYEE")) {
            key_unique_code_from = getIntent().getExtras().getString(Constants.KEY_UNQUECODE);
            quikklyUniqueCode = TextUtils.isEmpty(key_unique_code_from) ? BigInteger.ZERO : new BigInteger(key_unique_code_from);

            border_color = getResources().getString(R.string.default_border).replace("#", "0xff");
            background_color = getResources().getString(R.string.default_back).replace("#", "0xff");
            data_color = getResources().getString(R.string.default_data).replace("#", "0xff");

            profile_type = 3;

            try {

                try {
                    logo = getIntent().getExtras().getString("PROFILE_LOGO");
                    profile_middletag = logo;
                    profile_id = Integer.parseInt(getIntent().getExtras().getString("PROFILE_ID"));
                    user_id = qkPreferences.getValuesInt(QKPreferences.USER_ID);

                } catch (NumberFormatException e) {
                    e.printStackTrace();
                }

                if (profile_middletag != null && Utilities.isEmpty(profile_middletag)) {
                    downloadQkTag(profile_middletag);
                } else {
                    setupViews();
                    inputChanged();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
            try {
                key_unique_code_from = qkPreferences.getValues(QKPreferences.USER_UNIQUECODE);
            } catch (Exception e) {
                e.printStackTrace();
            }

            quikklyUniqueCode = TextUtils.isEmpty(key_unique_code_from) ? BigInteger.ZERO : new BigInteger(key_unique_code_from);

            modelLoggedData = qkPreferences.getLoggedUser().getData();

            profile_id = modelLoggedData.getId();
            profile_type = 0;
            user_id = qkPreferences.getValuesInt(QKPreferences.USER_ID);

            border_color = getResources().getString(R.string.default_border).replace("#", "0xff");
            background_color = getResources().getString(R.string.default_back).replace("#", "0xff");
            data_color = getResources().getString(R.string.default_data).replace("#", "0xff");

            setupViews();

            inputChanged();
        }

        if (is_from_employee) {
            llremovephoto.setVisibility(View.GONE);
        }

        rec_select_background.setFocusable(false);
        findViewById(R.id.relativeRoot).requestFocus();
    }


    ImageSelected imageSelected = new ImageSelected() {
        @Override
        public void onImageSlected(int pos, BgList bgList1) {
            adapter.Selected_pos = pos;
            adapter.notifyDataSetChanged();
            try {
                if (bgList1.getIsImage() == 1) {
                    current_selected = pos - colors.length + 1;
                    //rel_qkTag.setBackgroundResource(getBackgroundRes(current_selected));
                    rel_qkTag.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                    Background = current_selected + "";
                } else {
                    current_selected = 0;
                    Background = bgList1.getStrColorcode() + "";
                    rel_qkTag.setBackgroundColor(bgList1.getColorcode());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    private List<String> gradient_array;

    public boolean isGradient(String background_gradient) {
        for (String s: gradient_array) {
            if (s.equals(background_gradient))
                return true;
        }
        return false;
    }

    public void downloadQkTag(String url) {
        new DownloadImage(context, new CallBackImageDownload() {
            @Override
            public void getBase64(String mData) {
            }

            @Override
            public void getBitmap(Bitmap mData) {
                String imgPath = Utilities.storeBitmapinFile(UpdateQkTagActivity.this, mData, "qk_tag");
                if (imgPath != null) {
                    qktag_base64 = Utilities.encodeTobase64(mData);
                    QkTagBase64 qkTagBase64 = new QkTagBase64();
                    qkTagBase64.setProfile_id("" + profile_id);
                    qkTagBase64.setMiddletag_url(profile_middletag);
                    if (qktag_base64 != null && !qktag_base64.isEmpty()) {
                        qkTagBase64.setBase64Data(qktag_base64);
                    }
                    RealmController.with(UpdateQkTagActivity.this).updateQKTags(qkTagBase64);
                    setupViews();
                    inputChanged();
                }
            }
        }).execute(url);
    }

    private void setupViews() {

        renderView = findViewById(R.id.render_tag);

        render_tag_employee = findViewById(R.id.render_tag_employee);

        template = "template0015style2";

        borderColor = findViewById(R.id.render_border_color);
        maskColor = findViewById(R.id.render_mask_color);
        dataColor = findViewById(R.id.render_data_color);

        String aa = convertToString(data_color);
        String bb = convertToString(background_color);
        String cc = convertToString(border_color);

        int data_color = Integer.parseInt(aa, 16) + 0xFF000000;
        int bg_color = Integer.parseInt(bb, 16) + 0xFF000000;
        int bc_color = Integer.parseInt(cc, 16) + 0xFF000000;

        borderColor.setColor(bc_color);
        borderColor.setSelected(true);
        maskColor.setColor(bg_color);
        dataColor.setColor(data_color);

        for (BorderDataColorPicker p: new BorderDataColorPicker[]{borderColor, dataColor}) {
            p.setFragmentManager(getSupportFragmentManager());
            p.setColorChangedListener(colorChangedListener);
        }

        for (BackgroundColorPicker p: new BackgroundColorPicker[]{maskColor}) {
            p.setFragmentManager(getSupportFragmentManager());
            p.setColorChangedListener(colorChangedListener1);
        }
    }

    public String convertToString(String hex) {
        String hexTmp = hex.substring(4);
        System.out.println("Hex to convert: " + hexTmp);
        //return Integer.parseInt(hexTmp, 16);
        //return Integer.parseInt(hexTmp);
        return hexTmp;
    }

    public int convertToInt(String hex) {
        String hexTmp = hex.substring(4);
        System.out.println("Hex to convert: " + hexTmp);
        return Integer.parseInt(hexTmp, 16);

    }

    int old_mask, old_border, old_data;

    private void inputChanged() {

        skin = new Skin();

        old_border = borderColor.getColor();
        old_mask = maskColor.getColor();
        old_data = dataColor.getColor();

        skin.borderColor = borderColor.getColorHtmlHex();
        skin.maskColor = maskColor.getColorHtmlHex();
        skin.dataColor = dataColor.getColorHtmlHex();

        Log.e("color_maskColor", maskColor.getColorHtmlHex());
        Log.e("color_dataColor", dataColor.getColorHtmlHex());

        tvalertmsg.setVisibility(View.GONE);

        skin.imageFit = 1;
        skin.logoUrl = "";
        skin.logoFit = 1;

        try {

            if (selectedImage != null) {
                llremovephoto.setVisibility(View.VISIBLE);
                String mime = "image/jpeg";
                String dataUri = "data:" + mime + ";base64," + Utilities.encodeTobase64(selectedImage);
                skin.imageUrl = dataUri;

            } else if (qktag_base64 != null && !qktag_base64.isEmpty() && Utilities.isEmpty(qktag_base64)) {
                llremovephoto.setVisibility(View.VISIBLE);
                String mime = "image/jpeg";
                String dataUri = "data:" + mime + ";base64," + qktag_base64;
                skin.imageUrl = dataUri;

            } else {
                llremovephoto.setVisibility(View.GONE);
                //croppedFile = new File("sdsadsdsd");
                skin.imageUrl = readAndBase64EncodeFromAssets(this, "qk_logo.png");
            }

            if (is_from_employee || profile_type == 4) {
                llremovephoto.setVisibility(View.GONE);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        renderView.post(new Runnable() {
            @Override
            public void run() {
                getViewBitmap(renderView);
            }
        });

        render_tag_employee.post(new Runnable() {
            @Override
            public void run() {
                getViewBitmapEmployee(render_tag_employee);
            }
        });

        renderView.setAll(template, quikklyUniqueCode, skin);

        try {
            render_tag_employee.setAll(template, quikklyUniqueCode_for_employee, skin);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private BackgroundColorPicker.ColorChangedListener colorChangedListener1 = new BackgroundColorPicker.ColorChangedListener() {
        @Override
        public void onColorChanged(BackgroundColorPicker picker, int color) {
            inputChanged();
        }
    };


    private BorderDataColorPicker.ColorChangedListener colorChangedListener = new BorderDataColorPicker.ColorChangedListener() {
        @Override
        public void onColorChanged(BorderDataColorPicker picker, int color) {
            inputChanged();
        }
    };

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.tvSelectphoto:
                final CharSequence[] options = getResources().getStringArray(R.array.choose_option_qktag);
                android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(this);
                builder.setTitle(getResources().getString(R.string.msg_select_option));
                builder.setItems(options, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int item) {
                        if (options[item].equals(getResources().getString(R.string.take_photo))) {
                            dialog.dismiss();
                            getPhotoFromCamera();
                        } else if (options[item].equals(getResources().getString(R.string.choose_from_gallery))) {
                            dialog.dismiss();
                            pickFromGallery();
                        } else if (options[item].equals(getResources().getString(R.string.cancel))) {
                            dialog.dismiss();
                        }
                    }
                });
                builder.show();
                break;
        }
    }

    public void functionGenerateQuicklyCOde(final int profile_type, final String qr_code, final String user_id, final String profileid) {

        String borderColor = skin.borderColor;
        String backgroundColor = skin.maskColor;
        String dataColor = skin.dataColor;

        borderColor = borderColor.replace("#", "0xff");
        backgroundColor = backgroundColor.replace("#", "0xff");
        dataColor = dataColor.replace("#", "0xff");

        JSONObject obj = new JSONObject();
        try {
            if (selectedImage != null)
                obj.put("middle_tag", Utilities.encodeTobase64(selectedImage));
            else if (tvRemovePhoto.getTag() != null && tvRemovePhoto.getTag().equals("0")) {
                obj.put("middle_tag", "R");
                RealmController.with(UpdateQkTagActivity.this).deleteTag(user_id);
            }

            obj.put("qr_code", qr_code);
            obj.put("border_color", borderColor);
            obj.put("background_color", backgroundColor);
            obj.put("pattern_color", dataColor);
            obj.put("user_id", "" + user_id.replace(".0", ""));
            obj.put("profile_id", "" + profileid.replace(".0", ""));
            obj.put("type", profile_type);
            obj.put("background_gradient", Background + "");
            //obj.put("background_gradient", current_selected + 1);

        } catch (Exception e) {
            e.printStackTrace();
        }
        Log.e("Object -> ", obj.toString());
        new VolleyApiRequest(context, true, VolleyApiRequest.REQUEST_BASEURL + "api/user_qr",
                new VolleyCallBack() {
                    @Override
                    public void onResponse(JSONObject response) {
                        startService(new Intent(UpdateQkTagActivity.this, LoadProfiles.class));
                        try {
                            String status = response.getString("status");
                            String message = response.getString("msg");

                            if (status.equals("200")) {
                                try {
                                    if (isCreateEmp)
                                        startServiceforEmployeeProfile(Base64Image_for_employee + "", login_user_id + "", profile_ID + "");
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                                parseQKTagGenerateResponse(response);
                            } else
                                Utilities.showToast(context, message);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyApiRequest.showVolleyError(context, error);
                    }
                }).enqueRequest(obj, Request.Method.POST);
    }

    private void pickFromGallery() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(UpdateQkTagActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                    REQUEST_STORAGE_READ_ACCESS_PERMISSION);

        } else {
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(Intent.createChooser(intent, getString(R.string.label_select_picture)), REQUEST_SELECT_PICTURE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_STORAGE_READ_ACCESS_PERMISSION:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    pickFromGallery();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    public void getPhotoFromCamera() {

        int currentapiVersion = Build.VERSION.SDK_INT;
        if (currentapiVersion >= Build.VERSION_CODES.M) {
            checkAndRequestPermissions();
        } else {

            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            File mediaStorageDir = new File(
                    Environment.getExternalStorageDirectory()
                            + File.separator
                            + getString(R.string.directory_name_temp_staff)
                            + File.separator
                            + getString(R.string.directory_name_temp)
            );

            if (!mediaStorageDir.exists()) {
                mediaStorageDir.mkdirs();
            }

            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
            try {
                mediaFile = File.createTempFile(
                        "TEMP_FULL_IMG_" + timeStamp,  /* prefix */
                        ".jpg",         /* suffix */
                        mediaStorageDir      /* directory */
                );
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(mediaFile));
                startActivityForResult(takePictureIntent, PICK_FROM_CAMERA);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_SELECT_PICTURE) {
                final Uri selectedUri = data.getData();
                if (selectedUri != null) {
                    try {
                        String compressImg = new CompressImage(context).compressImage("" + selectedUri);
                        if (compressImg != null && !compressImg.isEmpty()) {
                            Uri compressUri = Uri.fromFile(new File(compressImg));
                            startCropActivity(compressUri);
                        } else
                            Toast.makeText(UpdateQkTagActivity.this, R.string.toast_cannot_retrieve_selected_image, Toast.LENGTH_SHORT).show();
                    } catch (Exception e) {
                        startCropActivity(selectedUri);
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(UpdateQkTagActivity.this, R.string.toast_cannot_retrieve_selected_image, Toast.LENGTH_SHORT).show();
                }
            } else if (requestCode == UCrop.REQUEST_CROP) {
                handleCropResult(data);
            }
        }
        if (resultCode == UCrop.RESULT_ERROR) {
            handleCropError(data);
        }
    }

    private void handleCropResult(@NonNull Intent result) {
        if (result != null) {
            final Uri resultUri = UCrop.getOutput(result);
            if (resultUri != null) {

                try {
                    isFileupload = true;
                    final InputStream imageStream = getContentResolver().openInputStream(resultUri);
                    selectedImage = BitmapFactory.decodeStream(imageStream);
                    tvRemovePhoto.setTag("1");
                    //selectedImage = Utilities.modifyOrientation(selectedImage, Utilities.getRealPathFromURI(UpdateQkTagActivity.this, resultUri));
                    inputChanged();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else {
                Toast.makeText(UpdateQkTagActivity.this, R.string.toast_cannot_retrieve_cropped_image, Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void startCropActivity(@NonNull Uri uri) {
        String destinationFileName = "" + System.currentTimeMillis();
        destinationFileName += ".png";

        UCrop uCrop = UCrop.of(uri, Uri.fromFile(new File(getCacheDir(), destinationFileName)));
        uCrop.withAspectRatio(1, 1);
        uCrop.start(UpdateQkTagActivity.this);
    }


    @SuppressWarnings("ThrowableResultOfMethodCallIgnored")
    private void handleCropError(@NonNull Intent result) {
        final Throwable cropError = UCrop.getError(result);
        if (cropError != null) {
            Log.e(TAG, "handleCropError: ", cropError);
            Toast.makeText(UpdateQkTagActivity.this, cropError.getMessage(), Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(UpdateQkTagActivity.this, R.string.toast_unexpected_error, Toast.LENGTH_SHORT).show();
        }
    }

    private boolean checkAndRequestPermissions() {
        int permissionSendMessage = ContextCompat.checkSelfPermission(this,
                Manifest.permission.CAMERA);
        int locationPermission = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        List<String> listPermissionsNeeded = new ArrayList<>();
        if (locationPermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (permissionSendMessage != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CAMERA);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), REQUEST_ID_MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case android.R.id.home:
                if (from.equals(Constants.KEY_FROM_CREATEPROFILE)) {
                    new AlertDialog.Builder(this)
                            .setMessage(getResources().getString(R.string.msg_discard_changes))
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                    UpdateQkTagActivity.this.finish();
                                }
                            })
                            .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.dismiss();
                                }
                            }).show();
                } else {
                    exitfromActivity();
                }
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private String readAndBase64EncodeFromAssets(Context context, String assetsFile) throws IOException {
        String mime;
        if (assetsFile.toLowerCase().endsWith(".png"))
            mime = "image/png";
        else if (assetsFile.toLowerCase().endsWith(".jpg") || assetsFile.toLowerCase().endsWith(".jpeg"))
            mime = "image/jpeg";
        else
            throw new IllegalArgumentException(getResources().getString(R.string.msg_unknownexception_image) + assetsFile);

        AssetManager am = context.getAssets();
        InputStream stream = am.open(assetsFile, AssetManager.ACCESS_STREAMING);
        try {
            byte[] bytes = IOUtils.toByteArray(stream);
            String dataUri = "data:" + mime + ";base64," + Base64.encodeToString(bytes, Base64.DEFAULT);

            String base64 = Base64.encodeToString(bytes, Base64.DEFAULT);
            String aa = base64.replaceAll("\\s+", "");

            Log.d("base64", aa + "");
            return dataUri;
        } finally {
            stream.close();
        }
    }

    private Bitmap getViewBitmap(View v) {
        v.clearFocus();
        v.setPressed(false);

        boolean willNotCache = v.willNotCacheDrawing();
        v.setWillNotCacheDrawing(false);

        // Reset the drawing cache background color to fully transparent
        // for the duration of this operation
        int color = v.getDrawingCacheBackgroundColor();
        v.setDrawingCacheBackgroundColor(0);

        if (color != 0) {
            v.destroyDrawingCache();
        }
        v.buildDrawingCache();
        Bitmap cacheBitmap = v.getDrawingCache();
        if (cacheBitmap == null) {
            Log.e("jd", "failed getViewBitmap(" + v + ")", new RuntimeException());
            return null;
        }

        Bitmap bitmap = Bitmap.createBitmap(cacheBitmap);

        // Restore the view
        v.destroyDrawingCache();
        v.setWillNotCacheDrawing(willNotCache);
        v.setDrawingCacheBackgroundColor(color);

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream.toByteArray();

        String encoded = Base64.encodeToString(byteArray, Base64.DEFAULT);

        Log.d("my encode", encoded + "");

        Base64Image = encoded.replaceAll("\\s+", "");

        // Toast.makeText(this,base64,Toast.LENGTH_LONG).show();
        Log.d("base64", Base64Image + "");

        return bitmap;
    }

    private Bitmap getViewBitmapEmployee(View v) {
        v.clearFocus();
        v.setPressed(false);

        boolean willNotCache = v.willNotCacheDrawing();
        v.setWillNotCacheDrawing(false);

        // Reset the drawing cache background color to fully transparent
        // for the duration of this operation
        int color = v.getDrawingCacheBackgroundColor();
        v.setDrawingCacheBackgroundColor(0);

        if (color != 0) {
            v.destroyDrawingCache();
        }
        v.buildDrawingCache();
        Bitmap cacheBitmap = v.getDrawingCache();
        if (cacheBitmap == null) {
            Log.e("jd", "failed getViewBitmap(" + v + ")", new RuntimeException());
            return null;
        }

        Bitmap bitmap = Bitmap.createBitmap(cacheBitmap);

        // Restore the view
        v.destroyDrawingCache();
        v.setWillNotCacheDrawing(willNotCache);
        v.setDrawingCacheBackgroundColor(color);

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream.toByteArray();

        String encoded = Base64.encodeToString(byteArray, Base64.DEFAULT);

        Log.d("my encode", encoded + "");

        Base64Image_for_employee = encoded.replaceAll("\\s+", "");

        // Toast.makeText(this,base64,Toast.LENGTH_LONG).show();
        Log.d("base64", Base64Image_for_employee + "");

        return bitmap;
    }

    public class BgList {

        int drawable;
        int colorcode;
        int isImage;
        String strColorcode;

        public String getStrColorcode() {
            return strColorcode;
        }

        public void setStrColorcode(String strColorcode) {
            this.strColorcode = strColorcode;
        }

        public int getDrawable() {
            return drawable;
        }

        public void setDrawable(int drawable) {
            this.drawable = drawable;
        }

        public int getColorcode() {
            return colorcode;
        }

        public void setColorcode(int colorcode) {
            this.colorcode = colorcode;
        }

        public int getIsImage() {
            return isImage;
        }

        public void setIsImage(int isImage) {
            this.isImage = isImage;
        }
    }

    private void ShowSuccessDialog(final String type) {
        final Dialog dialog = new Dialog(UpdateQkTagActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View view = LinearLayout.inflate(UpdateQkTagActivity.this, R.layout.layout_sucess_dialog, null);
        dialog.setContentView(view);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        dialog.show();
        dialog.getWindow().setAttributes(lp);

        TextView tvmessage = dialog.findViewById(R.id.tvmessage);
        //ImageView imag = dialog.findViewById(R.id.image_suc_gif);
        final GifView gifView1 = (GifView) view.findViewById(R.id.image_suc_gif);
        gifView1.setVisibility(View.VISIBLE);
        gifView1.play();
        gifView1.setGifResource(R.drawable.done);
        gifView1.getGifResource();
        try {
            Handler h = new Handler();
            h.postDelayed(new Runnable() {
                @Override
                public void run() {
                    gifView1.pause();
                }
            }, 2000);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (from.equals(Constants.KEY_FROM_DASHBOARD))
            tvmessage.setText(getResources().getString(R.string.qktagwasupdated));
        else
            tvmessage.setText(getResources().getString(R.string.yourtagwascreatedsuccess));

        TextView textView = dialog.findViewById(R.id.tv_ok);
        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (type.equals("1")) {
                    setResult(RESULT_OK);
                    UpdateQkTagActivity.this.finish();
                } else if (type.equals("2")) {
                    Intent intent = new Intent(context, DashboardActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                }
            }
        });
    }

    public void parseQKTagGenerateResponse(JSONObject response) {

        try {
            JSONObject data = response.getJSONObject("data");

            String background_color = data.getString("background_color");
            String border_color = data.getString("border_color");
            String pattern_color = data.getString("pattern_color");
            String qr_code_url = data.getString("url");
            String qr_middle_tag = data.getString("middle_tag");
            String background_gradient = data.getString("background_gradient");

            MyProfiles myProfiles = RealmController.with(UpdateQkTagActivity.this).getProfileByID(key_unique_code_from);
            if (myProfiles != null) {

                JSONObject jsonObject = new JSONObject();

                MyProfiles updateData = new MyProfiles();

                updateData.setId("" + myProfiles.getId());
                updateData.setName(myProfiles.getName());
                updateData.setBackground_gradient(background_gradient);
                updateData.setStatus(myProfiles.getStatus());
                updateData.setType("" + myProfiles.getType());
                updateData.setLogo("" + myProfiles.getLogo());
                updateData.setUnique_code("" + myProfiles.getUnique_code());

                jsonObject.put("border_color", border_color);
                jsonObject.put("pattern_color", pattern_color);
                jsonObject.put("background_color", background_color);
                if (qr_middle_tag != null && Utilities.isEmpty(qr_code_url))
                    jsonObject.put("middle_tag", qr_middle_tag);
                else
                    jsonObject.put("middle_tag", "");
                jsonObject.put("qr_code", qr_code_url);
                jsonObject.put("background_gradient", current_selected);

                updateData.setQktaginfo(jsonObject.toString());
                RealmController.with(UpdateQkTagActivity.this).updateMyProfiles(updateData);
            }

            if (from != null && (from.equals(Constants.KEY_FROM_CREATEPROFILE))) {
                // Create profile
                String msg = "";
                if (KEY_PROFILE_TYPE.equalsIgnoreCase("3")) {
                   /* new AlertDialog.Builder(UpdateQkTagActivity.this)
                            .setMessage(getResources().getString(R.string.msg_thanking_you))
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                    setResult(RESULT_OK);
                                    UpdateQkTagActivity.this.finish();
                                }
                            }).show();*/
                    msg = "Thank you for creating a new celebrity profile. We will let you know of the status as soon as we have reviewed your application.";
                } else if (KEY_PROFILE_TYPE.equalsIgnoreCase("2")) {
                    msg = "Thank you for creating a new enterprise profile. We will let you know of the status as soon as we have reviewed your application.";
                    //ShowSuccessDialog("1");
                } else if (KEY_PROFILE_TYPE.equalsIgnoreCase("1")) {
                    //ShowSuccessDialog("1");
                    msg = "Thank you for creating a new company profile.";
                }
                new AlertDialog.Builder(UpdateQkTagActivity.this)
                        .setMessage(msg + "")
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                if (profile_type == 1) {
                                    setResult(RESULT_OK);
                                    UpdateQkTagActivity.this.finish();
                                } else {
                                    setResult(RESULT_OK);
                                    UpdateQkTagActivity.this.finish();
                                }
                            }
                        }).show();

            } else if (from != null && (from.equals(Constants.KEY_FROM_DASHBOARD))) {
                // Edit Tag from dashboard
                ShowSuccessDialog("1");

            } else {
                // For new user registration
                ModelLoggedUser modelLoggedUser = qkPreferences.getLoggedUser();
                modelLoggedUser.getData().setQrCode(qr_code_url);
                qkPreferences.storeLoggedUser(modelLoggedUser);

                ShowSuccessDialog("2");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void startServiceforEmployeeProfile(final String qr_code, final String user_id, final String profileid) {
        Intent i = new Intent(UpdateQkTagActivity.this, CreateEmployeeProfileService.class);
        i.putExtra("QR_CODE", qr_code + "");
        i.putExtra("USER_ID", user_id + "");
        i.putExtra("PROFILE_ID", profileid + "");
        startService(i);
        //functionGenerateEmployeeProfile(qr_code + "", user_id + "", profileid + "");
    }

    private void functionGenerateEmployeeProfile(String qr_code, String user_id, String profileid) {
        String borderColor = "0xff000000";
        String backgroundColor = "0xffFFFFFF";
        String dataColor = "0xff191919";

        final ProgressDialog pd = Utilities.showProgress(UpdateQkTagActivity.this);
        JSONObject obj = new JSONObject();
        try {
            obj.put("middle_tag", "A");
            obj.put("qr_code", qr_code);
            obj.put("border_color", borderColor);
            obj.put("background_color", backgroundColor);
            obj.put("pattern_color", dataColor);
            obj.put("user_id", "" + user_id.replace(".0", ""));
            obj.put("profile_id", "" + profileid.replace(".0", ""));
            obj.put("type", "3");
            obj.put("background_gradient", "#2ABBBE");

        } catch (JSONException e) {
            e.printStackTrace();
        }

        String url = VolleyApiRequest.REQUEST_BASEURL + "api/user_qr";
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, url, obj, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                if (pd != null && pd.isShowing()) {
                    pd.dismiss();
                }
                startService(new Intent(UpdateQkTagActivity.this, LoadProfiles.class));
                Log.d("Response", response + "");
                try {
                    String status = response.getString("status");
                    if (status.equals("200")) {

                        JSONObject data = response.getJSONObject("data");

                        String background_color = data.getString("background_color");
                        String border_color = data.getString("border_color");
                        String pattern_color = data.getString("pattern_color");
                        String qr_code_url = data.getString("url");
                        String qr_middle_tag = data.getString("middle_tag");
                        String background_gradient = data.getString("background_gradient");

                        String p_id = data.getString("profile_id");
                        String name = data.getString("name");
                        String type = data.getString("type");
                        String logo = data.getString("logo");
                        String unique_code = data.getString("unique_code");

                        JSONObject jsonObject = new JSONObject();

                        MyProfiles updateData = new MyProfiles();
                        updateData.setId(p_id + "");
                        updateData.setName(name + "");
                        updateData.setBackground_gradient(background_gradient);
                        updateData.setType(type + "");
                        updateData.setLogo(logo + "");
                        updateData.setUnique_code(unique_code + "");

                        jsonObject.put("border_color", border_color);
                        jsonObject.put("pattern_color", pattern_color);
                        jsonObject.put("background_color", background_color);
                        if (qr_middle_tag != null && Utilities.isEmpty(qr_code_url))
                            jsonObject.put("middle_tag", qr_middle_tag);
                        else
                            jsonObject.put("middle_tag", "");
                        jsonObject.put("qr_code", qr_code_url);
                        jsonObject.put("background_gradient", current_selected);

                        updateData.setQktaginfo(jsonObject.toString());
                        RealmController.with(UpdateQkTagActivity.this).updateMyProfiles(updateData);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (pd != null && pd.isShowing()) {
                    pd.dismiss();
                }
                //handle the error
                Log.d("Response", error.getLocalizedMessage() + "");
                error.printStackTrace();

            }
        }) {
            @Override
            public Map<String, String> getHeaders() {

                String token = "";
                if (qkPreferences != null) {
                    token = qkPreferences.getApiToken() + "";
                }

                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "Bearer " + token + "");
                params.put("Accept", "application/json");
                params.put("QK_ACCESS_KEY", context.getResources().getString(R.string.QK_ACCESS_KEY));
                return params;
            }
        };
        Volley.newRequestQueue(this).add(jsonRequest);
    }

    private void AcceptEmployee() {
        JSONObject obj = new JSONObject();
        try {
            obj.put("status", "A");
            obj.put("profile_id", String.valueOf(profile_id).replace(".0", ""));
            obj.put("user_id", this.login_user_id);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        new VolleyApiRequest(context, true, VolleyApiRequest.REQUEST_BASEURL + "api/member/status",
                new VolleyCallBack() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String responseStatus = response.getString("status");
                            String msg = response.getString("msg");
                            //Utilities.showToast(context, msg);
                            if (responseStatus.equals("200")) {
                                functionGenerateQuicklyCOde(3, Base64Image, "" + user_id, profile_id + "");
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyApiRequest.showVolleyError(context, error);
                    }
                }).enqueRequest(obj, Request.Method.POST);
    }

    @Override
    public void onBackPressed() {
        exitfromActivity();
    }

    public void exitfromActivity() {
        if (isFoundanyupdate()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(UpdateQkTagActivity.this);
            builder.setTitle(getResources().getString(R.string.alert));
            builder.setMessage(getResources().getString(R.string.exitwithoutsave));
            builder.setPositiveButton(getResources().getString(R.string.exit), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    Utilities.hideKeyboard(UpdateQkTagActivity.this);
                    setResult(RESULT_CANCELED);
                    UpdateQkTagActivity.this.finish();
                }
            });
            builder.setNegativeButton(getResources().getString(R.string.close), null);
            builder.show();

        } else {

            Utilities.hideKeyboard(UpdateQkTagActivity.this);
            setResult(RESULT_CANCELED);
            UpdateQkTagActivity.this.finish();
        }
    }

    public boolean isFoundanyupdate() {

        String borderColor = skin.borderColor;
        String backgroundColor = skin.maskColor;
        String dataColor = skin.dataColor;

        borderColor = borderColor.replace("#", "0xff");
        backgroundColor = backgroundColor.replace("#", "0xff");
        dataColor = dataColor.replace("#", "0xff");

        if (prevborderColor != null && !prevborderColor.equals(borderColor)) {
            return true;

        } else if (prevtagbackgroundColor != null && !prevtagbackgroundColor.equals(backgroundColor)) {
            return true;

        } else if (prevdataColor != null && !prevdataColor.equals(dataColor)) {
            return true;

        } else if (isFileupload) {
            return true;

        } else if (isFileremove) {
            return true;

        } else if (prevbackgroundColor != null && !prevbackgroundColor.equals(Background)) {
            return true;

        }

        return false;
    }
}