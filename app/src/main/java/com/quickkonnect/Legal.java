package com.quickkonnect;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.utilities.Constants;
import com.utilities.Utilities;

/**
 * Created by ZTLAB03 on 14-03-2018.
 */

public class Legal extends AppCompatActivity implements View.OnClickListener {

    ActionBar actionBar;
    private Context context;
    private TextView tv_term_condition, tv_privacy_contract, tv_quickkonnect_ug, tv_press;
    private ImageView img_term_condition, img_privacy_contract, img_quickkonnect_ug, img_press;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Utilities.changeStatusbar(Legal.this, getWindow());
        setContentView(R.layout.frag_legal);

        context = Legal.this;

        Utilities.ChangeStatusBar(Legal.this);
        try {
            Toolbar toolbar = findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
            actionBar = getSupportActionBar();
            actionBar.setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close_white_24dp);
        } catch (Exception e) {
            e.printStackTrace();
        }

        tv_term_condition = findViewById(R.id.tv_term_condition);
        tv_privacy_contract = findViewById(R.id.tv_privacy_contract);
        tv_quickkonnect_ug = findViewById(R.id.tv_quickkonnect_ug);
        tv_press = findViewById(R.id.tv_press);

        img_term_condition = findViewById(R.id.img_term_condition);
        img_privacy_contract = findViewById(R.id.img_privacy_contract);
        img_quickkonnect_ug = findViewById(R.id.img_quickkonnect_ug);
        img_press = findViewById(R.id.img_press);

        tv_term_condition.setOnClickListener(this);
        tv_privacy_contract.setOnClickListener(this);
        tv_quickkonnect_ug.setOnClickListener(this);
        tv_press.setOnClickListener(this);

        img_term_condition.setOnClickListener(this);
        img_privacy_contract.setOnClickListener(this);
        img_quickkonnect_ug.setOnClickListener(this);
        img_press.setOnClickListener(this);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.tv_term_condition:
            case R.id.img_term_condition:
                Intent webviewintent = new Intent(context, QuickKonnect_Webview.class);
                webviewintent.putExtra("terms_condition", Constants.URL_TERMSOFUSE);
                startActivity(webviewintent);
                break;

            case R.id.tv_privacy_contract:
            case R.id.img_privacy_contract:
                Intent privacypolicy = new Intent(context, QuickKonnect_Webview.class);
                privacypolicy.putExtra("privacypolicy", Constants.URL_PRIVACYCONTENT);
                startActivity(privacypolicy);
                break;

            case R.id.tv_quickkonnect_ug:
            case R.id.img_quickkonnect_ug:
                Intent quickkonnect_ug = new Intent(context, QuickKonnect_Webview.class);
                quickkonnect_ug.putExtra("quickkonnect_ug", Constants.URL_UG);
                startActivity(quickkonnect_ug);
                break;

            case R.id.tv_press:
            case R.id.img_press:;

                Intent press = new Intent(context, QuickKonnect_Webview.class);
                press.putExtra("press", Constants.URL_PRESS);
                startActivity(press);

                break;
        }
    }
}