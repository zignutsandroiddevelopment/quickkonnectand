package com.quickkonnect;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.utilities.Constants;
import com.utilities.QKPreferences;
import com.utilities.Utilities;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class ViewAndShareQkTag extends AppCompatActivity {

    private QKPreferences qkPreferences;
    private Context context;
    String qktagurlmain = "", mainname = "", background = "";
    private List<String> gradient_array;
    LinearLayout linearLayout, main_root_layout;
    ImageView image_tag, img_share, img_close, imgQrCode;
    TextView tv_name;
    RelativeLayout toolbar_rel;

    // loading image
    byte[] bary;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_and_share_qk_tag);
        context = ViewAndShareQkTag.this;
        if (Build.VERSION.SDK_INT >= 19 && Build.VERSION.SDK_INT < 21) {
            setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, true);
        }
        if (Build.VERSION.SDK_INT >= 19) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }
        if (Build.VERSION.SDK_INT >= 21) {
            setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, false);
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }

        qkPreferences = QKPreferences.getInstance(context);

        linearLayout = findViewById(R.id.lin_share_main);
        main_root_layout = findViewById(R.id.main_root_layout);
        toolbar_rel = findViewById(R.id.toolbar_rel);
        imgQrCode = findViewById(R.id.imgQrCode);
        image_tag = findViewById(R.id.image_tag);
        tv_name = findViewById(R.id.tv_name);
        img_share = findViewById(R.id.img_share);
        img_close = findViewById(R.id.img_close);

        this.gradient_array = new ArrayList<>();
        this.gradient_array.add("1");
        this.gradient_array.add("2");
        this.gradient_array.add("3");
        this.gradient_array.add("4");
        this.gradient_array.add("5");
        this.gradient_array.add("6");
        this.gradient_array.add("7");
        this.gradient_array.add("8");
        this.gradient_array.add("9");
        this.gradient_array.add("10");

        try {
            Bundle b = getIntent().getExtras();
            qktagurlmain = b.getString("QKTAG");
            mainname = b.getString("MAIN_NAME");
            background = b.getString("BACKGROUND");

            bary = getIntent().getExtras().getByteArray("picture");
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            if (isGradient(background)) {
                linearLayout.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
            } else {
                linearLayout.setBackgroundColor(Color.parseColor(background));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            if (bary != null && bary.length > 0) {
                Bitmap bmp = BitmapFactory.decodeByteArray(bary, 0, bary.length);
                image_tag.setImageBitmap(bmp);
            } else {
                if (qktagurlmain != null)
                    Picasso.with(ViewAndShareQkTag.this)
                            .load(qktagurlmain + "")
                            .fit()
                            .into(image_tag);
            }
        } catch (Exception e) {
            e.printStackTrace();
            if (qktagurlmain != null)
                Picasso.with(ViewAndShareQkTag.this)
                        .load(qktagurlmain + "")
                        .fit()
                        .into(image_tag);
        }

        if (!mainname.equalsIgnoreCase("") && !mainname.equalsIgnoreCase("null")) {
            tv_name.setVisibility(View.VISIBLE);
            tv_name.setText(mainname + "");
        } else {
            tv_name.setVisibility(View.GONE);
        }


        img_share.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View view) {
                try {
                    if (ContextCompat.checkSelfPermission(ViewAndShareQkTag.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(ViewAndShareQkTag.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 100);

                    } else if (ContextCompat.checkSelfPermission(ViewAndShareQkTag.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(ViewAndShareQkTag.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 100);

                    } else shareItem();

                } catch (Exception e) {
                    toolbar_rel.setVisibility(View.VISIBLE);
                    e.printStackTrace();
                }
            }
        });

        img_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ViewAndShareQkTag.this.finish();
            }
        });

        String qr_code = qkPreferences.getValues(Constants.SHARE_USER_QRCODE);
        if (qr_code != null && !qr_code.isEmpty()) {
            Picasso.with(ViewAndShareQkTag.this)
                    .load(qr_code + "")
                    .fit()
                    .placeholder(R.drawable.qrcode)
                    .into(imgQrCode);
        }
    }

    public boolean isGradient(String background_gradient) {
        for (String s: gradient_array) {
            if (s.equals(background_gradient))
                return true;
        }
        return false;
    }

    @Override
    protected void onResume() {
        super.onResume();
        try {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            //Log.v(TAG,"Permission: "+permissions[0]+ "was "+grantResults[0]);
            //resume tasks needing this permission
            shareItem();
        }
    }

    public static void setWindowFlag(Activity activity, final int bits, boolean on) {
        Window win = activity.getWindow();
        WindowManager.LayoutParams winParams = win.getAttributes();
        if (on) {
            winParams.flags |= bits;
        } else {
            winParams.flags &= ~bits;
        }
        win.setAttributes(winParams);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void shareItem() {

        final CharSequence[] items = new CharSequence[]{"QK Tag", "QK Tag with QR Code", "Public QR Code", "Public Profile Link"};
        AlertDialog.Builder builder = new AlertDialog.Builder(ViewAndShareQkTag.this);
        builder.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {

                if (item == 0) {
                    // Share Only Qk Tag
                    ShareQkTag("qktag");
                } else if (item == 1) {
                    // Share Whole Screen Shots
                    ShareScreenShot();
                } else if (item == 2) {
                    // Share Public QR Code
                    String qr_code = qkPreferences.getValues(Constants.SHARE_USER_QRCODE);
                    if (qr_code != null && !qr_code.isEmpty()) {
                        ShareQkTag("qrcode");
                    } else
                        Utilities.showToast(context, getResources().getString(R.string.qrcodenotfound));

                } else if (item == 3) {
                    // Share Public Profile Link
                    String qr_link = qkPreferences.getValues(Constants.SHARE_USER_PUBLIC_URL);
                    if (qr_link != null && !qr_link.isEmpty()) {
                        Intent sendIntent = new Intent();
                        sendIntent.setAction(Intent.ACTION_SEND);
                        sendIntent.putExtra(Intent.EXTRA_TEXT, qr_link);
                        sendIntent.setType("text/plain");
                        startActivity(sendIntent);
                    } else
                        Utilities.showToast(context, getResources().getString(R.string.publicprofilenotfound));
                }
            }
        });
        builder.setPositiveButton("Close", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        builder.show();
    }

    private void ShareQkTag(String typeof_file) {
        try {
            toolbar_rel.setVisibility(View.GONE);
            String mPath = Environment.getExternalStorageDirectory().toString() + "/Quickkonnect/Screenshots/" + Calendar.getInstance().getTimeInMillis() + ".jpg";
            File folder = new File(Environment.getExternalStorageDirectory() + "/Quickkonnect/Screenshots");
            boolean var = false;
            if (!folder.exists())
                var = folder.mkdir();

            Bitmap bitmap = null;

            if (typeof_file.equalsIgnoreCase("qktag")) {
                View v1 = image_tag;
                v1.setDrawingCacheEnabled(true);
                bitmap = Bitmap.createBitmap(v1.getDrawingCache());
                v1.setDrawingCacheEnabled(false);
            }

            if (typeof_file.equalsIgnoreCase("qrcode")) {
                View v1 = imgQrCode;
                v1.setDrawingCacheEnabled(true);
                bitmap = Bitmap.createBitmap(v1.getDrawingCache());
                v1.setDrawingCacheEnabled(false);
            }

            if (bitmap != null) {

                File imageFile = new File(mPath);

                FileOutputStream outputStream = new FileOutputStream(imageFile);
                int quality = 100;
                bitmap.compress(Bitmap.CompressFormat.JPEG, quality, outputStream);
                outputStream.flush();
                outputStream.close();

                toolbar_rel.setVisibility(View.VISIBLE);

                final Intent intent = new Intent(android.content.Intent.ACTION_SEND);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    // only for gingerbread and newer versions
                    Uri uri = FileProvider.getUriForFile(ViewAndShareQkTag.this, BuildConfig.APPLICATION_ID + ".provider", imageFile);
                    intent.putExtra(Intent.EXTRA_STREAM, uri);
                } else intent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(imageFile));
                intent.setType("image/jpg");
                startActivity(Intent.createChooser(intent, "Share image via"));

            } else Utilities.showToast(context, getResources().getString(R.string.filenotfound));

        } catch (Throwable e) {
            toolbar_rel.setVisibility(View.VISIBLE);
            e.printStackTrace();
        }
    }

    private void ShareScreenShot() {

        try {
            toolbar_rel.setVisibility(View.GONE);
            String mPath = Environment.getExternalStorageDirectory().toString() + "/Quickkonnect/Screenshots/" + Calendar.getInstance().getTimeInMillis() + ".jpg";
            File folder = new File(Environment.getExternalStorageDirectory() + "/Quickkonnect/Screenshots");
            boolean var = false;
            if (!folder.exists())
                var = folder.mkdir();

            View v1 = main_root_layout;//getWindow().getDecorView().getRootView();main_root_layout
            v1.setDrawingCacheEnabled(true);
            Bitmap bitmap = Bitmap.createBitmap(v1.getDrawingCache());
            v1.setDrawingCacheEnabled(false);

            File imageFile = new File(mPath);

            FileOutputStream outputStream = new FileOutputStream(imageFile);
            int quality = 100;
            bitmap.compress(Bitmap.CompressFormat.JPEG, quality, outputStream);
            outputStream.flush();
            outputStream.close();

            toolbar_rel.setVisibility(View.VISIBLE);

            final Intent intent = new Intent(android.content.Intent.ACTION_SEND);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                // only for gingerbread and newer versions
                Uri uri = FileProvider.getUriForFile(ViewAndShareQkTag.this, BuildConfig.APPLICATION_ID + ".provider", imageFile);
                intent.putExtra(Intent.EXTRA_STREAM, uri);
            } else intent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(imageFile));
            intent.setType("image/jpg");
            intent.setType("image/jpg");
            startActivity(Intent.createChooser(intent, "Share image via"));

        } catch (Throwable e) {
            toolbar_rel.setVisibility(View.VISIBLE);
            e.printStackTrace();
        }
    }
}
