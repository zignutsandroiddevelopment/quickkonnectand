package com.quickkonnect;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.NestedScrollView;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.activities.UpdateBasicProfileActivity;
import com.activities.UpdateContactInfoActivity;
import com.activities.UpdateDashboardBasicProfileActivity;
import com.activities.UpdateEducationActivity;
import com.activities.UpdateExperienceActivity;
import com.activities.UpdateLanguageActivity;
import com.activities.UpdatePublicationActivity;
import com.adapter.UpdateContactInfoAdapter;
import com.adapter.UpdateEducationListAdapter;
import com.adapter.UpdateExperienceListAdapter;
import com.adapter.UpdateLanguageListAdapter;
import com.adapter.UpdatePublicationListAdapter;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.interfaces.SimpleGestureListener;
import com.models.logindata.BasicDetail;
import com.models.logindata.ContactDetail;
import com.models.logindata.EducationDetail;
import com.models.logindata.ExperienceDetail;
import com.models.logindata.LanguageDetail;
import com.models.logindata.ModelLoggedUser;
import com.models.logindata.ModelUserProfile;
import com.models.logindata.PublicationDetail;
import com.squareup.picasso.Picasso;
import com.utilities.Constants;
import com.utilities.QKPreferences;
import com.utilities.Utilities;
import com.volleyapirequest.VolleyApiRequest;
import com.volleyapirequest.VolleyCallBack;

import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class UpdateUserProfileActivity extends AppCompatActivity implements View.OnClickListener, SimpleGestureListener, SwipeRefreshLayout.OnRefreshListener {

    private Context context;
    private QKPreferences qkPreferences;
    private String userid, gender;
    private TextView tv_basicProfile_gender, tv_basicProfile_add_info, tv_language_add_info, tv_education_add_info, tv_contact_add_info,
            tv_experiences_add_info, tv_publication_add_info, tv_skip;
    private TextView tv_basicProfile_username, tv_basicProfile_qk_story, tv_basicProfile_birthdate,
            tv_basicProfile_location, tv_basic_info_title;
    private TextView tv_add_education, tv_add_contact, tv_add_experiences, tv_add_publication,
            tv_add_languages, tv_update_profile;
    private ImageView img_basic_profile_profile_pic;
    private String country_id, state_id, city_id;
    boolean isBirthEmpty = false, isGenderEmpty = false, isHometownEmpt = false;
    private RecyclerView recyclerView_education, recyclerView_contactinfo,
            recyclerView_experience, recyclerView_publication, recyclerView_languages;
    public RecyclerView.LayoutManager mLayoutManager;

    public UpdateEducationListAdapter adapter;
    public UpdateContactInfoAdapter contactInfoAdapter;
    public UpdateExperienceListAdapter experienceListAdapter;
    public UpdatePublicationListAdapter publicationListAdapter;
    public UpdateLanguageListAdapter languageListAdapter;

    private String birthdate, hometown;
    private String firstname, lastname, profile_pic, qkstory;
    private ImageView img_update_profile;
    private SimpleGestureFilter detector;
    private NestedScrollView scrollView;
    private TextView tv_next;
    private Bundle extras;
    private SwipeRefreshLayout mSwipeRefreshLayout;

    LinearLayout lldob, llhometown, llgender;
    private boolean isRefresh = false;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_user_profile);

        Utilities.ChangeStatusBar(UpdateUserProfileActivity.this);

        context = UpdateUserProfileActivity.this;

        qkPreferences = new QKPreferences(UpdateUserProfileActivity.this);

        scrollView = findViewById(R.id.scrollView);

        Handler h = new Handler();

        h.postDelayed(new Runnable() {

            @Override
            public void run() {
                scrollView.scrollTo(-50, -50);
            }
        }, 250);

        userid = qkPreferences.getLoggedUser().getData().getId() + "";

        detector = new SimpleGestureFilter(UpdateUserProfileActivity.this, this);

        // Basic Info
        tv_basicProfile_username = findViewById(R.id.tv_basicProfile_username);
        tv_basicProfile_qk_story = findViewById(R.id.tv_basicProfile_qk_story);
        tv_basicProfile_birthdate = findViewById(R.id.tv_basicProfile_birthdate);
        tv_basicProfile_location = findViewById(R.id.tv_basicProfile_location);
        tv_basic_info_title = findViewById(R.id.tv_basic_info_title);

        lldob = findViewById(R.id.lldob);
        llhometown = findViewById(R.id.llhometown);
        llgender = findViewById(R.id.llgender);

        // Show/Hide Add Information
        tv_basicProfile_add_info = findViewById(R.id.tv_basicProfile_add_info);
        tv_basicProfile_gender = findViewById(R.id.tv_basicProfile_gender);
        tv_education_add_info = findViewById(R.id.tv_education_add_info);
        tv_contact_add_info = findViewById(R.id.tv_contact_add_info);
        tv_experiences_add_info = findViewById(R.id.tv_experiences_add_info);
        tv_publication_add_info = findViewById(R.id.tv_publication_add_info);
        tv_language_add_info = findViewById(R.id.tv_language_add_info);

        //Title Text
        tv_next = findViewById(R.id.tv_next);
        tv_skip = findViewById(R.id.tv_skip);
        tv_update_profile = findViewById(R.id.tv_update_profile);
        img_update_profile = findViewById(R.id.img_update_profile);

        //fab_up = findViewById(R.id.fab_up);

        mSwipeRefreshLayout = findViewById(R.id.swipeRefreshLayout_Updateuser_Profile);
        mSwipeRefreshLayout.setOnRefreshListener(UpdateUserProfileActivity.this);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary,
                android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_blue_dark);

        //Basic Profile EditText
        img_basic_profile_profile_pic = findViewById(R.id.img_basic_profile_profile_pic);

        img_basic_profile_profile_pic.setOnClickListener(this);


        tv_skip.setOnClickListener(this);
        tv_next.setOnClickListener(this);
        tv_update_profile.setOnClickListener(this);
        img_update_profile.setOnClickListener(this);
        //fab_up.setOnClickListener(this);

        // Add Education Exeprience etc
        tv_add_education = findViewById(R.id.tv_add_education);
        tv_add_contact = findViewById(R.id.tv_add_contact);
        tv_add_experiences = findViewById(R.id.tv_add_experiences);
        tv_add_publication = findViewById(R.id.tv_add_publication);
        tv_add_languages = findViewById(R.id.tv_add_languages);

        tv_add_education.setOnClickListener(this);
        tv_add_contact.setOnClickListener(this);
        tv_add_experiences.setOnClickListener(this);
        tv_add_publication.setOnClickListener(this);
        tv_add_languages.setOnClickListener(this);

        tv_basicProfile_add_info.setOnClickListener(this);
        tv_education_add_info.setOnClickListener(this);
        tv_contact_add_info.setOnClickListener(this);
        tv_experiences_add_info.setOnClickListener(this);
        tv_publication_add_info.setOnClickListener(this);
        tv_language_add_info.setOnClickListener(this);
        tv_basic_info_title.setOnClickListener(this);

        recyclerView_education = findViewById(R.id.recyclerView_education);
        recyclerView_education.setNestedScrollingEnabled(false);
        recyclerView_education.addItemDecoration(new SimpleDividerItemDecoration(this));
        recyclerView_education.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        recyclerView_education.setLayoutManager(mLayoutManager);

        recyclerView_contactinfo = findViewById(R.id.recyclerView_contactinfo);
        recyclerView_contactinfo.setNestedScrollingEnabled(false);
        recyclerView_contactinfo.addItemDecoration(new SimpleDividerItemDecoration(this));
        recyclerView_contactinfo.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        recyclerView_contactinfo.setLayoutManager(mLayoutManager);


        recyclerView_experience = findViewById(R.id.recyclerView_experience);
        recyclerView_experience.setNestedScrollingEnabled(false);
        recyclerView_experience.addItemDecoration(new SimpleDividerItemDecoration(this));
        recyclerView_experience.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        recyclerView_experience.setLayoutManager(mLayoutManager);

        recyclerView_publication = findViewById(R.id.recyclerView_publication);
        recyclerView_publication.setNestedScrollingEnabled(false);
        recyclerView_publication.addItemDecoration(new SimpleDividerItemDecoration(this));
        recyclerView_publication.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        recyclerView_publication.setLayoutManager(mLayoutManager);

        recyclerView_languages = findViewById(R.id.recyclerView_languages);
        recyclerView_languages.setNestedScrollingEnabled(false);
        recyclerView_languages.addItemDecoration(new SimpleDividerItemDecoration(this));
        recyclerView_languages.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        recyclerView_languages.setLayoutManager(mLayoutManager);

        extras = getIntent().getExtras();

        if (extras != null) {
            tv_next.setVisibility(View.INVISIBLE);
            tv_skip.setVisibility(View.VISIBLE);
        } else {
            tv_next.setVisibility(View.VISIBLE);
            tv_skip.setVisibility(View.INVISIBLE);
        }

        parseResponse();
    }

    @Override
    public void onClick(View view) {

        Intent intent;
        switch (view.getId()) {
            case R.id.tv_add_education:
            case R.id.tv_education_add_info:
                intent = new Intent(this, UpdateEducationActivity.class);
                intent.putExtra(Constants.KEY_UPDATE, Constants.ADD_DATA);
                startActivityForResult(intent, Constants.RCODE_UPDATEEDUCATION);
                break;
            case R.id.tv_add_contact:
            case R.id.tv_contact_add_info:
                intent = new Intent(this, UpdateContactInfoActivity.class);
                intent.putExtra(Constants.KEY_UPDATE, Constants.ADD_DATA);
                startActivityForResult(intent, Constants.RCODE_UPDATEEDUCATION);
                break;
            case R.id.tv_add_experiences:
            case R.id.tv_experiences_add_info:
                intent = new Intent(this, UpdateExperienceActivity.class);
                intent.putExtra(Constants.KEY_UPDATE, Constants.ADD_DATA);
                startActivityForResult(intent, Constants.RCODE_UPDATEEDUCATION);
                break;
            case R.id.tv_add_publication:
            case R.id.tv_publication_add_info:
                intent = new Intent(this, UpdatePublicationActivity.class);
                intent.putExtra(Constants.KEY_UPDATE, Constants.ADD_DATA);
                startActivityForResult(intent, Constants.RCODE_UPDATEEDUCATION);
                break;
            case R.id.tv_add_languages:
            case R.id.tv_language_add_info:
                intent = new Intent(this, UpdateLanguageActivity.class);
                intent.putExtra(Constants.KEY_UPDATE, Constants.ADD_DATA);
                startActivityForResult(intent, Constants.RCODE_UPDATEEDUCATION);
                break;

            case R.id.tv_skip:
                if (isRefresh) {
                    setResult(RESULT_OK);
                    UpdateUserProfileActivity.this.finish();
                } else
                    UpdateUserProfileActivity.this.finish();
                break;
            case R.id.tv_next:
                if (tv_basicProfile_birthdate != null && !tv_basicProfile_birthdate.getText().toString().isEmpty() && tv_basicProfile_gender != null && !tv_basicProfile_gender.getText().toString().isEmpty()) {
                    intent = new Intent(this, UpdateQkTagActivity.class);
                    intent.putExtra(Constants.KEY_FROM, Constants.KEY_FROM_UPDATEPROFILE);
                    startActivity(intent);
                    finish();
                } else {
                    Utilities.showToast(UpdateUserProfileActivity.this, "Please enter the gender and date of birth to basic detail.");
                }
                break;

            case R.id.tv_basic_info_title:
                intent = new Intent(this, UpdateBasicProfileActivity.class);
                intent.putExtra("birthdate", birthdate);
                intent.putExtra("hometown", hometown);
                intent.putExtra("country_id", country_id);
                intent.putExtra("state_id", state_id);
                intent.putExtra("city_id", city_id);
                intent.putExtra(Constants.KEY_UPDATE, Constants.EDIT_DATA);
                startActivityForResult(intent, Constants.RCODE_UPDATEEDUCATION);
                break;

            case R.id.img_update_profile:
                intent = new Intent(this, UpdateDashboardBasicProfileActivity.class);
                intent.putExtra("profile_pic", profile_pic);
                intent.putExtra("firstname", firstname);
                intent.putExtra("lastname", lastname);
                intent.putExtra("qkstory", qkstory);
                startActivityForResult(intent, Constants.RCODE_UPDATEEDUCATION);
                break;
            case R.id.tv_basicProfile_add_info:
                intent = new Intent(this, UpdateBasicProfileActivity.class);
                intent.putExtra(Constants.KEY_UPDATE, Constants.ADD_DATA);
                startActivityForResult(intent, Constants.RCODE_UPDATEEDUCATION);
                break;
        }
    }

    @Override
    public void onRefresh() {
        if (Utilities.isNetworkConnected(context))
            functionGetUserProfile(userid, false);
        else
            Utilities.showToast(context, Constants.NO_INTERNET_CONNECTION);
    }

    // Get User Profile
    public void functionGetUserProfile(final String userid, boolean isProcess) {
        new VolleyApiRequest(UpdateUserProfileActivity.this, isProcess, VolleyApiRequest.REQUEST_BASEURL + "api/get_user_detail/" + userid,
                new VolleyCallBack() {
                    @Override
                    public void onResponse(JSONObject response) {
                        isRefresh = true;
                        mSwipeRefreshLayout.setRefreshing(false);
                        try {

                            String status = response.getString("status");
                            String message = response.getString("msg");

                            if (status.equals("200")) {
                                mSwipeRefreshLayout.setVisibility(View.VISIBLE);

                                JSONObject dataObject = response.getJSONObject("data");

                                ModelUserProfile object = new Gson().fromJson(dataObject.toString(), ModelUserProfile.class);

                                ModelLoggedUser modelLoggedUser = qkPreferences.getLoggedUser();

                                modelLoggedUser.getData().setBasicDetail(object.getBasicDetail());
                                modelLoggedUser.getData().setContactDetail(object.getContactDetail());
                                modelLoggedUser.getData().setEducationDetail(object.getEducationDetail());
                                modelLoggedUser.getData().setExperienceDetail(object.getExperienceDetail());
                                modelLoggedUser.getData().setPublicationDetail(object.getPublicationDetail());
                                modelLoggedUser.getData().setLanguageDetail(object.getLanguageDetail());

                                qkPreferences.storeLoggedUser(modelLoggedUser);

                                String filePath = qkPreferences.getValues(Constants.QKTAGLOCALURL);
                                try {
                                    if (Utilities.isEmpty(filePath)) {
                                        File file = new File(filePath);
                                        boolean isDelete = file.delete();
                                        Log.e("File Delete", "" + isDelete);
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                qkPreferences.storeValues(Constants.QKTAGLOCALURL, null);

                                parseResponse();

                            } else {
                                mSwipeRefreshLayout.setVisibility(View.GONE);
                                Utilities.showToast(UpdateUserProfileActivity.this, message);
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        mSwipeRefreshLayout.setRefreshing(false);
                        VolleyApiRequest.showVolleyError(UpdateUserProfileActivity.this, error);
                    }
                }).enqueRequest(null, Request.Method.GET);
    }

    public void parseResponse() {

        try {
            ModelLoggedUser modelLoggedUser = qkPreferences.getLoggedUser();
            if (modelLoggedUser.getData() != null && modelLoggedUser.getData().getBasicDetail() != null) {
                BasicDetail basicDetail = modelLoggedUser.getData().getBasicDetail();
                if (basicDetail.getProfilePic() != null && !basicDetail.getProfilePic().toString().isEmpty()) {
                    Picasso.with(UpdateUserProfileActivity.this)
                            .load("" + basicDetail.getProfilePic())
                            .error(R.drawable.ic_default_profile_photo)
                            .placeholder(R.drawable.ic_default_profile_photo)
                            .transform(new CircleTransform())
                            .fit()
                            .into(img_basic_profile_profile_pic);
                    profile_pic = "" + basicDetail.getProfilePic();
                } else {
                    img_basic_profile_profile_pic.setImageResource(R.drawable.ic_default_profile_photo);
                }

                if (basicDetail.getFirstname() != null && basicDetail.getFirstname().length() > 0
                        && !basicDetail.getFirstname().equals("null")) {
                    firstname = basicDetail.getFirstname();
                    if (basicDetail.getLastname() != null && basicDetail.getLastname().length() > 0
                            && !basicDetail.getLastname().equals("null")) {
                        lastname = basicDetail.getLastname();
                        tv_basicProfile_username.setText(basicDetail.getFirstname()
                                + " " + basicDetail.getLastname());
                    } else
                        tv_basicProfile_username.setText(basicDetail.getFirstname());
                }

                if (basicDetail.getGender() != null && !basicDetail.getGender().equals("null") && Utilities.isEmpty(basicDetail.getGender())) {
                    isGenderEmpty = true;
                    llgender.setVisibility(View.GONE);
                    tv_basicProfile_add_info.setVisibility(View.GONE);
                    switch (basicDetail.getGender()) {
                        case "M":
                            tv_basicProfile_gender.setText("Male");
                            llgender.setVisibility(View.VISIBLE);
                            gender = "Male";
                            break;
                        case "F":
                            tv_basicProfile_gender.setText("Female");
                            llgender.setVisibility(View.VISIBLE);
                            gender = "Female";
                            break;
                        case "O":
                            tv_basicProfile_gender.setText("Other");
                            llgender.setVisibility(View.VISIBLE);
                            gender = "Other";
                            break;
                    }
                    tv_basicProfile_gender.setVisibility(View.VISIBLE);
                    birthdate = basicDetail.getBirthdate();
                    gender = basicDetail.getGender();
                } else
                    llgender.setVisibility(View.GONE);


                if (basicDetail.getQkStory() != null && basicDetail.getQkStory().toString().length() > 0
                        && !basicDetail.getQkStory().equals("null"))
                    tv_basicProfile_qk_story.setText("" + basicDetail.getQkStory());
                qkstory = "" + basicDetail.getQkStory();
                if (!Utilities.isEmpty("" + basicDetail.getQkStory()))
                    tv_basicProfile_qk_story.setText("");


                if (basicDetail.getBirthdate() != null && basicDetail.getBirthdate().length() > 0 && !basicDetail.getBirthdate().equals("null") && Utilities.isEmpty(basicDetail.getBirthdate())) {
                    isBirthEmpty = true;
                    lldob.setVisibility(View.VISIBLE);
                    tv_basicProfile_add_info.setVisibility(View.GONE);
                    //String first = "<font color='#414141'>Date of Birth:</font>";
                    //String sourceString = "<b>" + first + "</b> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + modelUserProfile.getBasicDetail().getBirthdate();
                    tv_basicProfile_birthdate.setText(basicDetail.getBirthdate());
                    birthdate = basicDetail.getBirthdate();
                } else
                    lldob.setVisibility(View.GONE);

                if (basicDetail.getHometown() != null && basicDetail.getHometown().length() > 0
                        && !basicDetail.getHometown().equals("null") && Utilities.isEmpty(basicDetail.getHometown())) {
                    isHometownEmpt = true;
                    llhometown.setVisibility(View.VISIBLE);
                    //String first = "<font color='#414141'>Hometown: </font>";
                    //String sourceString = "<b>" + first + "</b> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; " + modelUserProfile.getBasicDetail().getHometown();
                    tv_basicProfile_location.setText(basicDetail.getHometown());
                    hometown = basicDetail.getHometown();
                } else llhometown.setVisibility(View.GONE);

                if (isBirthEmpty || isHometownEmpt || isGenderEmpty) {
                    tv_basic_info_title.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_edit_black_24dp, 0);
                    tv_basicProfile_add_info.setVisibility(View.GONE);
                    if (!Utilities.isEmpty(basicDetail.getHometown()))
                        tv_basicProfile_location.setText("");
                    if (!Utilities.isEmpty(basicDetail.getBirthdate()))
                        tv_basicProfile_birthdate.setText("");
                    if (!Utilities.isEmpty(basicDetail.getGender()))
                        tv_basicProfile_gender.setText("");
                    //llgender.setVisibility(View.VISIBLE);
                } else {
                    tv_basicProfile_location.setText("");
                    tv_basicProfile_birthdate.setText("");
                    tv_basicProfile_add_info.setVisibility(View.VISIBLE);
                    tv_basicProfile_gender.setText("");
                    // llgender.setVisibility(View.GONE);
                    tv_basic_info_title.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);

                }

                if (basicDetail.getCountryId() != null) {
                    country_id = basicDetail.getCountryId() + "";
                }
                if (basicDetail.getCityId() != null) {
                    city_id = basicDetail.getCityId() + "";
                }
                if (basicDetail.getStateId() != null) {
                    state_id = basicDetail.getStateId() + "";
                }

            }

            //Education
            if (modelLoggedUser.getData() != null && modelLoggedUser.getData().getEducationDetail() != null
                    && modelLoggedUser.getData().getEducationDetail().size() > 0) {
                tv_add_education.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_add_black_24dp, 0);
                tv_education_add_info.setVisibility(View.GONE);
                recyclerView_education.setVisibility(View.VISIBLE);

                adapter = new UpdateEducationListAdapter(UpdateUserProfileActivity.this, modelLoggedUser.getData().getEducationDetail());
                recyclerView_education.setAdapter(adapter);

            } else {
                List<EducationDetail> educationDetailList = new ArrayList<>();
                adapter = new UpdateEducationListAdapter(UpdateUserProfileActivity.this, educationDetailList);
                recyclerView_education.setAdapter(adapter);
                //divider_education.setVisibility(View.GONE);
                tv_add_education.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                tv_education_add_info.setVisibility(View.VISIBLE);
            }

            //Contact Information
            if (modelLoggedUser.getData() != null && modelLoggedUser.getData().getContactDetail() != null
                    && modelLoggedUser.getData().getContactDetail().size() > 0) {
                tv_add_contact.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_add_black_24dp, 0);
                tv_contact_add_info.setVisibility(View.GONE);
                contactInfoAdapter = new UpdateContactInfoAdapter(UpdateUserProfileActivity.this, modelLoggedUser.getData().getContactDetail());
                recyclerView_contactinfo.setAdapter(contactInfoAdapter);
            } else {
                List<ContactDetail> contactDetails = new ArrayList<>();
                contactInfoAdapter = new UpdateContactInfoAdapter(UpdateUserProfileActivity.this, contactDetails);
                recyclerView_contactinfo.setAdapter(contactInfoAdapter);
                //divider_contact.setVisibility(View.GONE);
                tv_add_contact.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                tv_contact_add_info.setVisibility(View.VISIBLE);
            }

            // Experience
            if (modelLoggedUser.getData().getExperienceDetail() != null && modelLoggedUser.getData().getExperienceDetail().size() > 0) {
                tv_add_experiences.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_add_black_24dp, 0);
                tv_experiences_add_info.setVisibility(View.GONE);
                experienceListAdapter = new UpdateExperienceListAdapter(UpdateUserProfileActivity.this, modelLoggedUser.getData().getExperienceDetail());
                recyclerView_experience.setAdapter(experienceListAdapter);
            } else {
                List<ExperienceDetail> experienceDetails = new ArrayList<>();
                experienceListAdapter = new UpdateExperienceListAdapter(UpdateUserProfileActivity.this, experienceDetails);
                recyclerView_experience.setAdapter(experienceListAdapter);
                //divider_experience.setVisibility(View.GONE);
                tv_add_experiences.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                tv_experiences_add_info.setVisibility(View.VISIBLE);
            }


            //Publication
            if (modelLoggedUser.getData().getPublicationDetail() != null && modelLoggedUser.getData().getPublicationDetail().size() > 0) {
                tv_add_publication.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_add_black_24dp, 0);
                tv_publication_add_info.setVisibility(View.GONE);
                publicationListAdapter = new UpdatePublicationListAdapter(UpdateUserProfileActivity.this, modelLoggedUser.getData().getPublicationDetail(), true, userid + "");
                recyclerView_publication.setAdapter(publicationListAdapter);

            } else {
                List<PublicationDetail> publicationDetails = new ArrayList<>();
                publicationListAdapter = new UpdatePublicationListAdapter(UpdateUserProfileActivity.this, publicationDetails, true, userid + "");
                recyclerView_publication.setAdapter(publicationListAdapter);
                //divider_publication.setVisibility(View.GONE);
                tv_add_publication.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                tv_publication_add_info.setVisibility(View.VISIBLE);
            }

            //Language
            if (modelLoggedUser.getData().getLanguageDetail() != null && modelLoggedUser.getData().getLanguageDetail().size() > 0) {
                tv_add_languages.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_add_black_24dp, 0);
                tv_language_add_info.setVisibility(View.GONE);
                languageListAdapter = new UpdateLanguageListAdapter(UpdateUserProfileActivity.this, modelLoggedUser.getData().getLanguageDetail(), true);
                recyclerView_languages.setAdapter(languageListAdapter);
                try {
                    Collections.sort(modelLoggedUser.getData().getLanguageDetail(), new Comparator<LanguageDetail>() {
                        @Override
                        public int compare(LanguageDetail languageDetail, LanguageDetail t1) {

                            Float first = Float.parseFloat(languageDetail.getRating());
                            Float second = Float.parseFloat(t1.getRating());

                            if (first.floatValue() > second.floatValue()) {
                                return -1;
                            } else if (first.floatValue() < second.floatValue()) {
                                return 1;
                            } else {
                                return 0;
                            }
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                List<LanguageDetail> languageDetails = new ArrayList<>();
                languageListAdapter = new UpdateLanguageListAdapter(UpdateUserProfileActivity.this, languageDetails, true);
                recyclerView_languages.setAdapter(languageListAdapter);
                //divider_languages.setVisibility(View.GONE);
                tv_add_languages.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                tv_language_add_info.setVisibility(View.VISIBLE);
                try {
                    Collections.sort(modelLoggedUser.getData().getLanguageDetail(), new Comparator<LanguageDetail>() {
                        @Override
                        public int compare(LanguageDetail languageDetail, LanguageDetail t1) {

                            Float first = Float.parseFloat(languageDetail.getRating());
                            Float second = Float.parseFloat(t1.getRating());

                            if (first.floatValue() > second.floatValue()) {
                                return -1;
                            } else if (first.floatValue() < second.floatValue()) {
                                return 1;
                            } else {
                                return 0;
                            }
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == Constants.RCODE_UPDATEEDUCATION) {
            parseResponse();
        }
    }


    @Override
    public void onSwipe(int direction) {
        String str = "";
        switch (direction) {
            case SimpleGestureFilter.SWIPE_RIGHT:
                str = "Swipe Right";
                if (extras != null) {
                    finish();
                }
                Log.d("swipe right", str);
                break;
            case SimpleGestureFilter.SWIPE_LEFT:
                str = "Swipe Left";
                break;
            case SimpleGestureFilter.SWIPE_DOWN:
                str = "Swipe Down";
                break;
            case SimpleGestureFilter.SWIPE_UP:
                str = "Swipe Up";
                break;
        }
    }

    @Override
    public void onDoubleTap() {
    }

    @Override
    public void onBackPressed() {
        if (isRefresh) {
            setResult(RESULT_OK);
            UpdateUserProfileActivity.this.finish();
        } else
            UpdateUserProfileActivity.this.finish();
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent me) {
        // Call onTouchEvent of SimpleGestureFilter class
        this.detector.onTouchEvent(me);
        return super.dispatchTouchEvent(me);
    }
}