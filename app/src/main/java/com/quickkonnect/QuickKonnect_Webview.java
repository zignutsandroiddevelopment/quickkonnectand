package com.quickkonnect;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.webkit.SslErrorHandler;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.activities.HelpSettings;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.models.EmployeeView.EmployeeDetailView;
import com.utilities.Constants;
import com.utilities.QKPreferences;
import com.utilities.Utilities;
import com.volleyapirequest.VolleyApiRequest;
import com.volleyapirequest.VolleyCallBack;

import org.json.JSONObject;

import java.util.TimeZone;

public class QuickKonnect_Webview extends AppCompatActivity {

    WebView webView;
    Context context;
    private ProgressBar mprogrssbar;
    private String visited_user_id = "0";
    private String visited_profile_id = "0";
    private String TYPE = "W";
    private String URL = "", login_user_id = "";
    private QKPreferences qkPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Utilities.changeStatusbar(QuickKonnect_Webview.this, getWindow());
        setContentView(R.layout.activity_quick_konnect__webview);

        context = QuickKonnect_Webview.this;
        qkPreferences = new QKPreferences(context);

        login_user_id = "" + qkPreferences.getValuesInt(QKPreferences.USER_ID);
        login_user_id = login_user_id.replace(".0", "");

        Utilities.ChangeStatusBar(QuickKonnect_Webview.this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        //toolbar.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        toolbar.setBackgroundResource(R.drawable.toolbar_gradient);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        webView = findViewById(R.id.webView);

        /*WebSettings settings = webView.getSettings();

        settings.setJavaScriptEnabled(true);*/
        //webView.setWebViewClient(new myWebClient());
        //webView.getSettings().setJavaScriptEnabled(true);
        // By using this method together with the overridden method onReceivedSslError()
        // you will avoid the "WebView Blank Page" problem to appear. This might happen if you
        // use a "https" url!
        //settings.setDomStorageEnabled(true);

        //webView.loadUrl(URL);

        //webView.setWebViewClient(new MyWebViewClient());

        mprogrssbar = findViewById(R.id.mprogrssbar);

        Bundle data = getIntent().getExtras();

        if (data.containsKey("terms_condition")) {
            //webView.loadUrl(Constants.URL_TERMSOFUSE);
            loadWebview(Constants.URL_TERMSOFUSE);
            URL = Constants.URL_TERMSOFUSE;
            setTitle("Terms & Conditions");
        } else if (data.containsKey("privacypolicy")) {
            loadWebview(Constants.URL_PRIVACYCONTENT);
            URL = Constants.URL_PRIVACYCONTENT;
            setTitle("Privacy Contract");
        } else if (data.containsKey("quickkonnect_ug")) {
            loadWebview(Constants.URL_UG);
            URL = Constants.URL_UG;
            setTitle("QuickKonnect UG");
        } else if (data.containsKey("press")) {
            loadWebview(Constants.URL_PRESS);
            URL = Constants.URL_PRESS;
            setTitle("Press");
        } else if (data.containsKey("help_center")) {
            loadWebview(Constants.URL_HELP);
            URL = Constants.URL_HELP;
            setTitle("Help Center");
        } else if (data.containsKey("publication")) {
            String publication = getIntent().getExtras().getString("publication");
            String title = getIntent().getExtras().getString("title");
            URL = publication;
            loadWebview(publication);
            setTitle(title);
        } else {
            String profile_url = getIntent().getExtras().getString("other");
            String title = getIntent().getExtras().getString(Constants.KEY);
            URL = profile_url;
            loadWebview(profile_url);
            setTitle(title);
        }

        visited_user_id = getIntent().getExtras().getString("VISITED_USER_ID") + "";
        visited_profile_id = getIntent().getExtras().getString("PROFILE_ID") + "";
        TYPE = getIntent().getExtras().getString("TYPE") + "";

        //webView.loadUrl(URL);

        /*mprogrssbar.setVisibility(View.VISIBLE);
        //webView = (WebView) findViewById(R.id.webView);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setDomStorageEnabled(true);
        webView.loadUrl(URL);
        webView.clearView();
        webView.measure(100, 100);
        webView.getSettings().setUseWideViewPort(true);
        webView.getSettings().setLoadWithOverviewMode(true);

        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                mprogrssbar.setVisibility(View.GONE);
                view.loadUrl(URL);
                return false;
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                mprogrssbar.setVisibility(View.GONE);
            }

            @SuppressLint("NewApi")
            @Override
            public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
                //super.onReceivedSslError(view, handler, error);
                // this will ignore the Ssl error and will go forward to your site
                handler.proceed();
                error.getCertificate();
            }
        });
*/
        if (!login_user_id.equalsIgnoreCase(visited_user_id)) {
            SetUpAnalytics();
        }
    }

    private void SetUpAnalytics() {

        try {
            if (Utilities.isNetworkConnected(QuickKonnect_Webview.this)) {
                JSONObject obj = new JSONObject();
                try {
                    obj.put("user_id", login_user_id + "");
                    obj.put("visited_user_id", visited_user_id + "");
                    obj.put("profile_id", visited_profile_id + "");
                    obj.put("link", URL + "");
                    obj.put("link_type", TYPE + "");
                } catch (Exception e) {
                    e.printStackTrace();
                }
                new VolleyApiRequest(QuickKonnect_Webview.this, false, VolleyApiRequest.REQUEST_BASEURL + "api/analytics/visited_link",
                        new VolleyCallBack() {
                            @Override
                            public void onResponse(JSONObject response) {
                                try {
                                    Log.e("Response", "" + response);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }

                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Log.e("error ", "" + error);
                            }
                        }).enqueRequest(obj, Request.Method.POST);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public class myWebClient extends WebViewClient {
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            // TODO Auto-generated method stub
            super.onPageStarted(view, url, favicon);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            // TODO Auto-generated method stub
            mprogrssbar.setVisibility(View.VISIBLE);
            //view.loadUrl(url);
            return true;

        }

        @Override
        public void onPageFinished(WebView view, String url) {
            // TODO Auto-generated method stub
            super.onPageFinished(view, url);

            mprogrssbar.setVisibility(View.GONE);
        }
    }

    public void loadWebview(String url) {

        WebSettings settings = webView.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setLoadWithOverviewMode(true);
        settings.setPluginState(WebSettings.PluginState.ON);
        settings.setAllowFileAccess(true);
        settings.setUseWideViewPort(true);
        webView.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);

        mprogrssbar.setVisibility(View.VISIBLE);
        webView.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (url.contains("http:") || url.contains("https:"))
                    view.loadUrl(url);
                return true;
            }

            public void onPageFinished(WebView view, String url) {
                mprogrssbar.setVisibility(View.GONE);
            }

            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                mprogrssbar.setVisibility(View.GONE);
                Toast.makeText(context, description + "", Toast.LENGTH_SHORT).show();
            }
        });
        webView.loadUrl(url);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                setResult(RESULT_CANCELED);
                QuickKonnect_Webview.this.finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);

        }
    }

}
