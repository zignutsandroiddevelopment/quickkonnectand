package com.quickkonnect;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.Spanned;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.models.EmployeeView.Email;
import com.models.EmployeeView.EmployeeDetailView;
import com.models.EmployeeView.Fax;
import com.models.EmployeeView.Phone;
import com.models.EmployeeView.Weblink;
import com.models.StausMessage;
import com.realmtable.MyProfiles;
import com.realmtable.RealmController;
import com.retrofitservice.ApiUtils;
import com.retrofitservice.SOService;
import com.rilixtech.CountryCodePicker;
import com.squareup.picasso.Picasso;
import com.utilities.Constants;
import com.utilities.QKPreferences;
import com.utilities.Utilities;
import com.volleyapirequest.VolleyApiRequest;
import com.volleyapirequest.VolleyCallBack;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Accept_Employee extends AppCompatActivity implements View.OnClickListener {

    private TextView next, username, subtag;
    private ImageView profile_image, img_delete_employee;
    private Toolbar toolbar;
    private QKPreferences qkPreferences;
    private Context context;
    private String PROFILE_DATA = "", key_from = "", unique_code = "", PROFILE_NAME = "", login_user_id = "", profile_id = "", profile_pic_url = "", about = "";
    private Menu menu;

    //about

    EditText textin_about;
    TextView counter;

    // Phone
    TextView text_lable_phone;
    private String PhoneString = "";
    EditText textIn;
    Button buttonAdd;
    LinearLayout container;
    FrameLayout frame_line_below;
    private CountryCodePicker ccp, ccp_fax;

    int TYPE_PHONE = 1;
    int TYPE_EMAIL = 2;
    int TYPE_FAX = 3;
    int TYPE_WEBSITE = 4;

    // Email
    TextView text_lable_email;
    private String EmailString = "";
    EditText textIn_email;
    Button buttonAdd_email;
    LinearLayout container_email;
    FrameLayout frame_line_below_email;

    // Fax
    TextView text_lable_fax;
    private String FaxString = "";
    EditText textIn_fax;
    Button buttonAdd_fax;
    LinearLayout container_fax;
    FrameLayout frame_line_below_fax;

    // Website
    TextView text_lable_website;
    private String WebsiteString = "";
    EditText textIn_website;
    Button buttonAdd_website;
    LinearLayout container_website;
    FrameLayout frame_line_below_website;

    SOService mService;
    ProgressDialog progressDialog;

    // for edit info
    private boolean isEditinfo = false, isCreateFromProfile = false;
    private TextView ed_emp_valid_from_edit, ed_emp_department_edit, ed_emp_number_edit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_accept_employee);

        try {
            Utilities.ChangeStatusBar(Accept_Employee.this);
        } catch (Exception e) {
            e.printStackTrace();
        }


        context = Accept_Employee.this;
        qkPreferences = new QKPreferences(context);
        mService = ApiUtils.getSOService();
        login_user_id = "" + qkPreferences.getValuesInt(QKPreferences.USER_ID);

        try {
            Bundle b = getIntent().getExtras();
            key_from = b.getString(Constants.KEY);
            if (key_from.equalsIgnoreCase(Constants.KEY_FROM_MANAGEPROFILES)) {
                isEditinfo = true;
            } else if (key_from.equalsIgnoreCase("FROM_UPDATE_QK_TAG")) {
                isCreateFromProfile = true;
                about = b.getString("about");
            }
            unique_code = b.getString("unique_id");
            PROFILE_DATA = b.getString("PROFILE_DATA");
            PROFILE_NAME = b.getString("PROFILE_NAME");
            profile_id = b.getString("profile_id");

        } catch (Exception e) {
            e.printStackTrace();
        }
        initView();
    }

    private void initView() {

        toolbar = findViewById(R.id.toolbar_accept_emp);
        setSupportActionBar(toolbar);
        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close_white_24dp);
        getSupportActionBar().setTitle("");

        next = findViewById(R.id.tv_next_emp);
        username = findViewById(R.id.tv_username_emp);
        subtag = findViewById(R.id.tv_about_emp);
        profile_image = findViewById(R.id.img_logo_emp);
        img_delete_employee = findViewById(R.id.img_delete_employee);

        next.setOnClickListener(this);

        try {
            if (isCreateFromProfile) {
                img_delete_employee.setVisibility(View.GONE);
                if (PROFILE_DATA != null && !PROFILE_DATA.equalsIgnoreCase("") && !PROFILE_DATA.equalsIgnoreCase("null")) {
                    profile_pic_url = PROFILE_DATA;
                    Picasso.with(Accept_Employee.this)
                            .load(profile_pic_url + "")
                            .error(R.drawable.ic_default_profile_photo)
                            .placeholder(R.drawable.ic_default_profile_photo)
                            .transform(new CircleTransform())
                            .fit()
                            .into(profile_image);
                } else {
                    profile_image.setImageResource(R.drawable.ic_default_profile_photo);
                }
                if (PROFILE_NAME != null && !PROFILE_NAME.equalsIgnoreCase("") && !PROFILE_NAME.equalsIgnoreCase("null")) {
                    username.setText(PROFILE_NAME + "");
                }
            } else {
                if (PROFILE_DATA != null && !PROFILE_DATA.equalsIgnoreCase("")) {
                    Gson g = new Gson();
                    Profile profile = g.fromJson(PROFILE_DATA, Profile.class);

                    if (profile.getId() != null) {
                        profile_id = profile.getId();
                    }
                    if (profile.getLogo() != null && !profile.getLogo().equalsIgnoreCase("") && !profile.getLogo().equalsIgnoreCase("null")) {
                        profile_pic_url = profile.getLogo();
                        Picasso.with(Accept_Employee.this)
                                .load(profile.getLogo())
                                .error(R.drawable.ic_default_profile_photo)
                                .placeholder(R.drawable.ic_default_profile_photo)
                                .transform(new CircleTransform())
                                .fit()
                                .into(profile_image);
                    } else {
                        profile_image.setImageResource(R.drawable.ic_default_profile_photo);
                    }
                    if (profile.getName() != null && !profile.getName().equalsIgnoreCase("") && !profile.getName().equalsIgnoreCase("null")) {
                        username.setText(profile.getName());
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        if (isEditinfo) {
            String userimage = "";
            String uname = "";
            //img_delete_employee.setVisibility(View.VISIBLE);
            userimage = qkPreferences.getValues(QKPreferences.USER_PROFILEIMG);
            if (userimage != null && !userimage.isEmpty()) {
                Picasso.with(context)
                        .load(userimage)
                        .transform(new CircleTransform())
                        .placeholder(R.drawable.ic_default_profile_photo)
                        .fit()
                        .into(profile_image);
            } else profile_image.setImageResource(R.drawable.user_profile_pic);

            uname = qkPreferences.getValues(QKPreferences.USER_FULLNAME);
            if (uname != null && !uname.equalsIgnoreCase("") && !uname.equalsIgnoreCase("null")) {
                username.setText(uname + "");
            }
        } else {
            img_delete_employee.setVisibility(View.GONE);
            if (PROFILE_NAME != null && !PROFILE_NAME.equalsIgnoreCase("") && !PROFILE_NAME.equalsIgnoreCase("null")) {
                subtag.setText(PROFILE_NAME + "");
            } else {
                subtag.setVisibility(View.GONE);
            }
        }


        // for isEditinfo
        ed_emp_valid_from_edit = findViewById(R.id.ed_emp_valid_from_edit);
        ed_emp_department_edit = findViewById(R.id.ed_emp_department_edit);
        ed_emp_number_edit = findViewById(R.id.ed_emp_number_edit);

        if (isEditinfo) {
            ed_emp_valid_from_edit.setVisibility(View.VISIBLE);
            ed_emp_department_edit.setVisibility(View.VISIBLE);
            ed_emp_number_edit.setVisibility(View.VISIBLE);
            subtag.setVisibility(View.GONE);
            next.setText(getResources().getString(R.string.save));
        } else {
            ed_emp_valid_from_edit.setVisibility(View.GONE);
            ed_emp_department_edit.setVisibility(View.GONE);
            ed_emp_number_edit.setVisibility(View.GONE);
            subtag.setVisibility(View.VISIBLE);
            next.setText(getResources().getString(R.string.smallNext));
        }

        if (isCreateFromProfile) {
            if (about.equalsIgnoreCase("") || about.equalsIgnoreCase("null")) {
                subtag.setVisibility(View.GONE);
            } else {
                subtag.setVisibility(View.VISIBLE);
                subtag.setText(about + "");
            }
        }
        // About
        textin_about = findViewById(R.id.textin_about);
        counter = findViewById(R.id.tv_counter_employee);

        try {
            Utilities.TextWatcher(textin_about, counter, "400");
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Phone
        text_lable_phone = findViewById(R.id.text_lable_phone);
        textIn = findViewById(R.id.textin);
        buttonAdd = findViewById(R.id.add);
        container = findViewById(R.id.container_lin_phone);
        ccp = findViewById(R.id.ccp_phone);
        ccp_fax = findViewById(R.id.ccp_fax);
        frame_line_below = findViewById(R.id.frame_line_below);
        ccp.enablePhoneAutoFormatter(true);
        ccp_fax.enablePhoneAutoFormatter(true);
        text_lable_phone = findViewById(R.id.text_lable_phone);

        SetUpForPhone();

        // Email
        text_lable_email = findViewById(R.id.text_lable_email);
        textIn_email = findViewById(R.id.textin_email);
        buttonAdd_email = findViewById(R.id.add_email);
        container_email = findViewById(R.id.container_lin_email);
        frame_line_below_email = findViewById(R.id.frame_line_below_email);
        text_lable_email = findViewById(R.id.text_lable_email);

        SetUpForEmail();

        // Fax
        text_lable_fax = findViewById(R.id.text_lable_fax);
        textIn_fax = findViewById(R.id.textin_fax);
        buttonAdd_fax = findViewById(R.id.add_fax);
        container_fax = findViewById(R.id.container_lin_fax);
        frame_line_below_fax = findViewById(R.id.frame_line_below_fax);
        text_lable_fax = findViewById(R.id.text_lable_fax);

        SetUpForFax();

        // Website
        text_lable_website = findViewById(R.id.text_lable_website);
        textIn_website = findViewById(R.id.textin_website);
        buttonAdd_website = findViewById(R.id.add_website);
        container_website = findViewById(R.id.container_lin_website);
        frame_line_below_website = findViewById(R.id.frame_line_below_website);
        text_lable_website = findViewById(R.id.text_lable_website);

        SetUpForWebsite();

        if (isEditinfo) {
            try {
                MyProfiles myProfiles = RealmController.with(Accept_Employee.this).getMyProfielDataByProfileId(unique_code);
                if (myProfiles != null) {
                    profile_id = myProfiles.getId();
                    showDataOffline(true);
                } else if (login_user_id != null && Utilities.isNetworkConnected(Accept_Employee.this)) {
                    CallApi(true);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        img_delete_employee.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Utilities.isNetworkConnected(Accept_Employee.this)) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(Accept_Employee.this);
                    builder.setTitle(getResources().getString(R.string.areyousure));
                    //String msg = "Are you sure want to Delete this Employee Profile ?";

                    builder.setMessage(getResources().getString(R.string.youwanttoremoveempaccount));
                    builder.setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                            deleteEmployee();
                        }
                    });
                    builder.setNegativeButton(getResources().getString(R.string.no), null);
                    builder.show();

                } else
                    Utilities.showToast(Accept_Employee.this, getResources().getString(R.string.no_internat_connection));
            }
        });

    }

    private void SetUpForPhone() {
        textIn.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
               /* if (textIn.getText().toString().isEmpty())
                    text_lable_phone.setVisibility(View.GONE);
                else
                    text_lable_phone.setVisibility(View.VISIBLE);*/
                if (textIn.getText().toString().length() == 1 && textIn.getText().toString().equalsIgnoreCase("0")) {
                    textIn.setText("");
                } else {
                    textIn.setFilters(new InputFilter[]{filter1, new InputFilter.LengthFilter(15)});
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });
        textIn.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (!hasFocus) {
                    frame_line_below.setBackgroundResource(R.color.text);
                    buttonAdd.setBackgroundResource(R.drawable.add_item_false);
                } else {
                    frame_line_below.setBackgroundResource(R.color.colorPrimary);
                    buttonAdd.setBackgroundResource(R.drawable.add_item_true);
                }
            }
        });

        buttonAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utilities.hideKeyboard(Accept_Employee.this);
                if (container.getChildCount() < 5) {
                    if (!textIn.getText().toString().isEmpty()) {
                        LayoutInflater layoutInflater = (LayoutInflater) getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        final View addView = layoutInflater.inflate(R.layout.edit_with_plus_new, null);
                        final EditText textOut = addView.findViewById(R.id.edittext_main);
                        final FrameLayout frame_layput_below = addView.findViewById(R.id.frame_layput_below);
                        CountryCodePicker ccp1 = addView.findViewById(R.id.ccp_phone);
                        String CurrentString = ccp.getSelectedCountryCodeWithPlus() + " " + textIn.getText().toString();
                        String[] separated = CurrentString.split(" ");
                        ccp1.enablePhoneAutoFormatter(true);
                        try {
                            ccp1.setCountryForPhoneCode(Integer.parseInt(separated[0]));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        textOut.setText(separated[1]);
                        final Button buttonRemove = addView.findViewById(R.id.remove);

                        textOut.addTextChangedListener(new TextWatcher() {
                            @Override
                            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                            }

                            @Override
                            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                                if (textOut.getText().toString().length() == 1 && textOut.getText().toString().equalsIgnoreCase("0")) {
                                    textOut.setText("");
                                } else {
                                    textOut.setFilters(new InputFilter[]{filter1, new InputFilter.LengthFilter(15)});
                                }
                            }

                            @Override
                            public void afterTextChanged(Editable editable) {
                            }
                        });

                        textOut.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                            @Override
                            public void onFocusChange(View view, boolean hasFocus) {
                                if (!hasFocus) {
                                    frame_layput_below.setBackgroundResource(R.color.text);
                                    buttonRemove.setBackgroundResource(R.drawable.remove_invi);
                                } else {
                                    frame_layput_below.setBackgroundResource(R.color.colorPrimary);
                                    buttonRemove.setBackgroundResource(R.drawable.remove_vi);
                                }
                            }
                        });

                        final View.OnClickListener thisListener = new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                ((LinearLayout) addView.getParent()).removeView(addView);
                                listAllAddView(TYPE_PHONE, 0);
                            }
                        };
                        buttonRemove.setOnClickListener(thisListener);
                        container.addView(addView);
                        listAllAddView(TYPE_PHONE, 1);
                    } else {
                        Utilities.showToast(getApplicationContext(), "Please enter valid phone number.");
                    }
                }
            }
        });
    }

    private void SetUpForEmail() {

        /*textIn_email.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (textIn_email.getText().toString().isEmpty())
                    text_lable_email.setVisibility(View.GONE);
                else
                    text_lable_email.setVisibility(View.VISIBLE);
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });*/

        textIn_email.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (!hasFocus) {
                    frame_line_below_email.setBackgroundResource(R.color.text);
                    buttonAdd_email.setBackgroundResource(R.drawable.add_item_false);
                } else {
                    frame_line_below_email.setBackgroundResource(R.color.colorPrimary);
                    buttonAdd_email.setBackgroundResource(R.drawable.add_item_true);
                }
            }
        });

        buttonAdd_email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utilities.hideKeyboard(Accept_Employee.this);
                if (container_email.getChildCount() < 5) {
                    if (!textIn_email.getText().toString().isEmpty() && Utilities.isValidEmail(textIn_email.getText().toString() + "")) {
                        LayoutInflater layoutInflater = (LayoutInflater) getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        final View addView = layoutInflater.inflate(R.layout.edit_with_plus_new, null);
                        final EditText textOut = addView.findViewById(R.id.edittext_main);
                        //textOut.setHint("Email");
                        textOut.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
                        final FrameLayout frame_layput_below = addView.findViewById(R.id.frame_layput_below);
                        CountryCodePicker ccp1 = addView.findViewById(R.id.ccp_phone);
                        ccp1.setVisibility(View.GONE);
                        String CurrentString = textIn_email.getText().toString();
                        textOut.setText(CurrentString);
                        final Button buttonRemove = addView.findViewById(R.id.remove);

                        textOut.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                            @Override
                            public void onFocusChange(View view, boolean hasFocus) {
                                if (!hasFocus) {
                                    frame_layput_below.setBackgroundResource(R.color.text);
                                    buttonRemove.setBackgroundResource(R.drawable.remove_invi);
                                } else {
                                    frame_layput_below.setBackgroundResource(R.color.colorPrimary);
                                    buttonRemove.setBackgroundResource(R.drawable.remove_vi);
                                }
                            }
                        });

                        final View.OnClickListener thisListener = new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                ((LinearLayout) addView.getParent()).removeView(addView);
                                listAllAddView(TYPE_EMAIL, 0);
                            }
                        };
                        buttonRemove.setOnClickListener(thisListener);
                        container_email.addView(addView);
                        listAllAddView(TYPE_EMAIL, 1);
                    } else {
                        Utilities.showToast(getApplicationContext(), "Please enter valid email.");
                    }
                }
            }
        });
    }

    private void SetUpForFax() {

        /*textIn_fax.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (textIn_fax.getText().toString().isEmpty())
                    text_lable_fax.setVisibility(View.GONE);
                else
                    text_lable_fax.setVisibility(View.VISIBLE);
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });*/
        textIn_fax.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                textIn_fax.setFilters(new InputFilter[]{filter1, new InputFilter.LengthFilter(15)});
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        textIn_fax.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (!hasFocus) {
                    frame_line_below_fax.setBackgroundResource(R.color.text);
                    buttonAdd_fax.setBackgroundResource(R.drawable.add_item_false);
                } else {
                    frame_line_below_fax.setBackgroundResource(R.color.colorPrimary);
                    buttonAdd_fax.setBackgroundResource(R.drawable.add_item_true);
                }
            }
        });

        buttonAdd_fax.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utilities.hideKeyboard(Accept_Employee.this);
                if (container_fax.getChildCount() < 5) {
                    if (!textIn_fax.getText().toString().isEmpty()) {
                        LayoutInflater layoutInflater = (LayoutInflater) getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        final View addView = layoutInflater.inflate(R.layout.edit_with_plus_new, null);
                        final EditText textOut = addView.findViewById(R.id.edittext_main);
                        //textOut.setHint("FAX");
                        textOut.setInputType(InputType.TYPE_CLASS_NUMBER);
                        final FrameLayout frame_layput_below = addView.findViewById(R.id.frame_layput_below);
                        CountryCodePicker ccp1 = addView.findViewById(R.id.ccp_phone);
                        ccp1.setVisibility(View.VISIBLE);
                        String CurrentString = ccp_fax.getSelectedCountryCodeWithPlus() + " " + textIn_fax.getText().toString();
                        String[] separated = CurrentString.split(" ");
                        ccp1.enablePhoneAutoFormatter(true);
                        try {
                            ccp1.setCountryForPhoneCode(Integer.parseInt(separated[0]));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        textOut.setText(separated[1]);
                        final Button buttonRemove = addView.findViewById(R.id.remove);

                        textOut.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                            @Override
                            public void onFocusChange(View view, boolean hasFocus) {
                                if (!hasFocus) {
                                    frame_layput_below.setBackgroundResource(R.color.text);
                                    buttonRemove.setBackgroundResource(R.drawable.remove_invi);
                                } else {
                                    frame_layput_below.setBackgroundResource(R.color.colorPrimary);
                                    buttonRemove.setBackgroundResource(R.drawable.remove_vi);
                                }
                            }
                        });

                        final View.OnClickListener thisListener = new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                ((LinearLayout) addView.getParent()).removeView(addView);
                                listAllAddView(TYPE_FAX, 0);
                            }
                        };
                        buttonRemove.setOnClickListener(thisListener);
                        container_fax.addView(addView);
                        listAllAddView(TYPE_FAX, 1);
                    } else {
                        Utilities.showToast(getApplicationContext(), "Please enter valid fax.");
                    }
                }
            }
        });
    }

    private void SetUpForWebsite() {

        /*
        textIn_website.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (textIn_website.getText().toString().isEmpty())
                    text_lable_website.setVisibility(View.GONE);
                else
                    text_lable_website.setVisibility(View.VISIBLE);
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });
        */

        textIn_website.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (!hasFocus) {
                    frame_line_below_website.setBackgroundResource(R.color.text);
                    buttonAdd_website.setBackgroundResource(R.drawable.add_item_false);
                } else {
                    frame_line_below_website.setBackgroundResource(R.color.colorPrimary);
                    buttonAdd_website.setBackgroundResource(R.drawable.add_item_true);
                }
            }
        });
        buttonAdd_website.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utilities.hideKeyboard(Accept_Employee.this);
                if (container_website.getChildCount() < 5) {
                    if (!textIn_website.getText().toString().isEmpty() && isValidUrl(textIn_website.getText().toString() + "")) {
                        LayoutInflater layoutInflater = (LayoutInflater) getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        final View addView = layoutInflater.inflate(R.layout.edit_with_plus_new, null);
                        final EditText textOut = addView.findViewById(R.id.edittext_main);
                        //textOut.setHint("Website");
                        textOut.setInputType(InputType.TYPE_CLASS_TEXT);
                        final FrameLayout frame_layput_below = addView.findViewById(R.id.frame_layput_below);
                        CountryCodePicker ccp1 = addView.findViewById(R.id.ccp_phone);
                        ccp1.setVisibility(View.GONE);
                        String CurrentString = textIn_website.getText().toString();
                        textOut.setText(CurrentString);
                        final Button buttonRemove = addView.findViewById(R.id.remove);

                        textOut.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                            @Override
                            public void onFocusChange(View view, boolean hasFocus) {
                                if (!hasFocus) {
                                    frame_layput_below.setBackgroundResource(R.color.text);
                                    buttonRemove.setBackgroundResource(R.drawable.remove_invi);
                                } else {
                                    frame_layput_below.setBackgroundResource(R.color.colorPrimary);
                                    buttonRemove.setBackgroundResource(R.drawable.remove_vi);
                                }
                            }
                        });

                        final View.OnClickListener thisListener = new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                ((LinearLayout) addView.getParent()).removeView(addView);
                                listAllAddView(TYPE_WEBSITE, 0);
                            }
                        };
                        buttonRemove.setOnClickListener(thisListener);
                        container_website.addView(addView);
                        listAllAddView(TYPE_WEBSITE, 1);
                    } else {
                        Utilities.showToast(getApplicationContext(), "Please enter valid website.");
                    }
                }
            }
        });
    }

    private void listAllAddView(int type, int operation) {

        // operation 0 = delete
        //           1 = edit

        if (type == TYPE_PHONE) {
            if (operation == 1)
                textIn.setText("");
           /* int childCount = container.getChildCount();
            for (int i = 0; i < childCount; i++) {
                View thisChild = container.getChildAt(i);
                EditText childTextView = thisChild.findViewById(R.id.edittext_main);
                String childTextViewValue = childTextView.getText().toString();
            }*/
        } else if (type == TYPE_EMAIL) {
            if (operation == 1)
                textIn_email.setText("");
        } else if (type == TYPE_FAX) {
            if (operation == 1)
                textIn_fax.setText("");
        } else if (type == TYPE_WEBSITE) {
            if (operation == 1)
                textIn_website.setText("");
        }
    }

    class Profile {
        String id;
        String name;
        String logo;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getLogo() {
            return logo;
        }

        public void setLogo(String logo) {
            this.logo = logo;
        }

    }

    @Override
    public void onClick(View view) {
        if (view == next) {
            if (isEditinfo) {
                submitData();
            } else {
                if (isCreateFromProfile) {
                    submitData();
                } else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(Accept_Employee.this);
                    builder.setMessage(getResources().getString(R.string.wanttoacceptrequest));
                    builder.setTitle(getResources().getString(R.string.areyousure));
                    builder.setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                            submitData();
                        }
                    });
                    builder.setNegativeButton(getResources().getString(R.string.no), null);
                    builder.show();
                }
            }
        }
    }

    public void submitData() {
        // Phone
        int phonecount = container.getChildCount();
        StringBuilder sb = new StringBuilder();
        JSONArray jsonArray_phone = new JSONArray();
        for (int i = 0; i < phonecount; i++) {
            View thisChild = container.getChildAt(i);
            EditText childTextView = thisChild.findViewById(R.id.edittext_main);
            CountryCodePicker cpk = thisChild.findViewById(R.id.ccp_phone);
            if (childTextView.getText().toString().isEmpty()) {
                int pos = i + 1;
                Toast.makeText(context, "Wrong phone at position " + pos, Toast.LENGTH_SHORT).show();
            } else {
                String CurrentString = cpk.getSelectedCountryCodeWithPlus() + " " + childTextView.getText().toString();
                sb.append(CurrentString + "");
                sb.append(",");
                try {
                    JSONObject jsonObject_phone = new JSONObject();
                    jsonObject_phone.put("name", CurrentString + "");
                    jsonArray_phone.put(jsonObject_phone);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                PhoneString = sb.toString();
            }
        }

        // Email
        int Emailcount = container_email.getChildCount();
        StringBuilder sbemail = new StringBuilder();
        JSONArray jsonArray_email = new JSONArray();
        for (int i = 0; i < Emailcount; i++) {
            View thisChild = container_email.getChildAt(i);
            EditText childTextView = thisChild.findViewById(R.id.edittext_main);
            if (childTextView.getText().toString().isEmpty() && !Utilities.isValidEmail(textIn_email.getText().toString() + "")) {
                int pos = i + 1;
                Toast.makeText(context, "Wrong email at position " + pos, Toast.LENGTH_SHORT).show();
            } else {
                String CurrentString = childTextView.getText().toString() + "";
                sbemail.append(CurrentString + "");
                sbemail.append(",");
                try {
                    JSONObject jsonObject_email = new JSONObject();
                    jsonObject_email.put("name", CurrentString + "");
                    jsonArray_email.put(jsonObject_email);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                EmailString = sbemail.toString();
            }
        }

        // Fax
        int Faxcount = container_fax.getChildCount();
        StringBuilder sbfax = new StringBuilder();
        JSONArray jsonArray_fax = new JSONArray();
        for (int i = 0; i < Faxcount; i++) {
            View thisChild = container_fax.getChildAt(i);
            CountryCodePicker cpk = thisChild.findViewById(R.id.ccp_phone);
            EditText childTextView = thisChild.findViewById(R.id.edittext_main);
            if (childTextView.getText().toString().isEmpty()) {
                int pos = i + 1;
                Toast.makeText(context, "Wrong fax at position " + pos, Toast.LENGTH_SHORT).show();
            } else {
                String CurrentString = cpk.getSelectedCountryCodeWithPlus() + " " + childTextView.getText().toString();
                sb.append(CurrentString + "");
                sb.append(",");
                try {
                    JSONObject jsonObject_fax = new JSONObject();
                    jsonObject_fax.put("name", CurrentString + "");
                    jsonArray_fax.put(jsonObject_fax);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                FaxString = sbfax.toString();
            }
        }

        // Website
        int Websitecount = container_website.getChildCount();
        StringBuilder sbweb = new StringBuilder();
        JSONArray jsonArray_web = new JSONArray();

        for (int i = 0; i < Websitecount; i++) {
            View thisChild = container_website.getChildAt(i);
            EditText childTextView = thisChild.findViewById(R.id.edittext_main);
            if (childTextView.getText().toString().isEmpty() && !isValidUrl(childTextView.getText().toString() + "")) {
                int pos = i + 1;
                Toast.makeText(context, "Wrong url at position " + pos, Toast.LENGTH_SHORT).show();
            } else {
                String CurrentString = childTextView.getText().toString() + "";
                sbweb.append(CurrentString + "");
                sbweb.append(",");
                try {
                    JSONObject jsonObject_web = new JSONObject();
                    jsonObject_web.put("name", CurrentString + "");
                    jsonArray_web.put(jsonObject_web);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                WebsiteString = sbweb.toString();
            }
        }

        Log.d("Result phone", PhoneString + "\n" + jsonArray_phone);
        Log.d("Result email", EmailString + "\n" + jsonArray_email);
        Log.d("Result fax", FaxString + "\n" + jsonArray_fax);
        Log.d("Result website", WebsiteString + "\n" + jsonArray_web);
        String about = textin_about.getText().toString() + "";
        Log.e("result", login_user_id + "\n" + profile_id + "\n" + jsonArray_phone + "\n" + jsonArray_email + "\n" + jsonArray_web + "\n" + jsonArray_fax);
        if (isEditinfo) {
            //for edit information
            CallApiUpdateInfo(about, jsonArray_phone, jsonArray_email, jsonArray_fax, jsonArray_web);
        } else if (isCreateFromProfile) {
            // for First time with status P or A
            Callapi(about, jsonArray_phone, jsonArray_email, jsonArray_fax, jsonArray_web);
        } else {
            Callapi(about, jsonArray_phone, jsonArray_email, jsonArray_fax, jsonArray_web);
        }
        //getTags(jsonArray_phone, jsonArray_email, jsonArray_fax, jsonArray_web);
    }

    private void CallApiUpdateInfo(String about, JSONArray jsonArray_phone, JSONArray jsonArray_email, JSONArray jsonArray_fax, JSONArray jsonArray_web) {
        try {
            progressDialog = Utilities.showProgress(Accept_Employee.this);
            String userid = login_user_id.replace(".0", "");
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("user_id", userid + "");
            jsonObject.put("profile_id", profile_id + "");
            jsonObject.put("phone", jsonArray_phone);
            jsonObject.put("email", jsonArray_email);
            jsonObject.put("weblink", jsonArray_web);
            jsonObject.put("fax", jsonArray_fax);
            jsonObject.put("about", about);
            new VolleyApiRequest(context, false, VolleyApiRequest.REQUEST_BASEURL + "api/profile/employer_profile_edit",
                    new VolleyCallBack() {
                        @Override
                        public void onResponse(JSONObject response) {
                            if (progressDialog != null && progressDialog.isShowing()) {
                                progressDialog.dismiss();
                            }
                            Log.d("Response", response + "");
                            try {
                                if (response != null) {
                                    EmployeeDetailView employeeDetailView = new Gson().fromJson(response.toString(), EmployeeDetailView.class);
                                    if (employeeDetailView.getStatus() == 200) {

                                        MyProfiles myProfiles = new MyProfiles();

                                        myProfiles.setUnique_code(unique_code + "");
                                        myProfiles.setId(profile_id + "");

                                        String social_phone = new Gson().toJson(employeeDetailView.getData().getPhone());
                                        myProfiles.setPhone(social_phone);

                                        String social_fax = new Gson().toJson(employeeDetailView.getData().getFax());
                                        myProfiles.setFax(social_fax);

                                        String social_email = new Gson().toJson(employeeDetailView.getData().getEmail());
                                        myProfiles.setEmail(social_email);

                                        String social_weblink = new Gson().toJson(employeeDetailView.getData().getWeblink());
                                        myProfiles.setWeblink(social_weblink);

                                        RealmController.with(Accept_Employee.this).updateMyProfiles(myProfiles);

                                        //startActivity(new Intent(Accept_Employee.this, ManageQKProfilesActivity.class));
                                        setResult(RESULT_OK);
                                        Accept_Employee.this.finish();

                                    } else {
                                        Utilities.showToast(Accept_Employee.this, employeeDetailView.getMsg() + "");
                                    }
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onErrorResponse(VolleyError error) {
                            if (progressDialog != null && progressDialog.isShowing()) {
                                progressDialog.dismiss();
                            }
                            VolleyApiRequest.showVolleyError(context, error);
                        }
                    }).enqueRequest(jsonObject, Request.Method.POST);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void Callapi(String about, JSONArray jsonArray_phone, JSONArray jsonArray_email, JSONArray jsonArray_fax, JSONArray jsonArray_web) {
        try {
            progressDialog = Utilities.showProgress(Accept_Employee.this);
            String userid = login_user_id.replace(".0", "");
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("status", "P");
            jsonObject.put("user_id", userid + "");
            jsonObject.put("profile_id", profile_id + "");
            jsonObject.put("phone", jsonArray_phone);
            jsonObject.put("email", jsonArray_email);
            jsonObject.put("weblink", jsonArray_web);
            jsonObject.put("fax", jsonArray_fax);
            jsonObject.put("about", about);
            new VolleyApiRequest(context, false, VolleyApiRequest.REQUEST_BASEURL + "api/member/status",
                    new VolleyCallBack() {
                        @Override
                        public void onResponse(JSONObject response) {
                            if (progressDialog != null && progressDialog.isShowing()) {
                                progressDialog.dismiss();
                            }
                            Log.d("Response", response + "");
                            if (response != null) {
                                StausMessage stausMessage = new Gson().fromJson(response.toString(), StausMessage.class);
                                if (stausMessage != null && stausMessage.getStatus().equalsIgnoreCase("200")) {
                                    String UniqCode = stausMessage.getData().getUniqueCode() + "";
                                    String ProfileId = stausMessage.getData().getProfile_id() + "";
                                    if (ProfileId.equalsIgnoreCase("") && ProfileId.equalsIgnoreCase("null")) {
                                        ProfileId = profile_id;
                                    }
                                    Log.d("UniqCode", UniqCode + "");
                                    if (!UniqCode.equalsIgnoreCase("") && !UniqCode.equalsIgnoreCase("null")) {
                                        Intent i = new Intent(getApplicationContext(), UpdateQkTagActivity.class);
                                        i.putExtra(Constants.KEY_FROM, "ADD_EMPLOYEE");
                                        i.putExtra(Constants.KEY_UNQUECODE, UniqCode + "");
                                        i.putExtra("PROFILE_ID", ProfileId + "");
                                        i.putExtra("PROFILE_LOGO", profile_pic_url + "");
                                        if (isCreateFromProfile)
                                            i.putExtra("KEY_FORM", "isCreateFromProfile");
                                        startActivity(i);
                                    }
                                } else {
                                    Utilities.showToast(getApplicationContext(), stausMessage.getMsg());
                                }
                            }
                        }

                        @Override
                        public void onErrorResponse(VolleyError error) {
                            if (progressDialog != null && progressDialog.isShowing()) {
                                progressDialog.dismiss();
                            }
                            VolleyApiRequest.showVolleyError(context, error);
                        }
                    }).enqueRequest(jsonObject, Request.Method.POST);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    InputFilter filter1 = new InputFilter() {
        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
            for (int i = start; i < end; ++i) {
                if (!Pattern.compile("[0123456789]*").matcher(String.valueOf(source.charAt(i))).matches()) {
                    return "";
                }
            }
            return null;
        }
    };

    private boolean isValidUrl(String url) {
        Pattern p = Patterns.WEB_URL;
        Matcher m = p.matcher(url.toLowerCase());
        return m.matches();
    }

    private void CallApi(boolean isRefresh) {
        if (isRefresh)
            progressDialog = Utilities.showProgress(Accept_Employee.this);

        String token = "";
        if (qkPreferences != null) {
            token = qkPreferences.getApiToken() + "";
        }

        Map<String, String> params = new HashMap<>();
        params.put("Authorization", "Bearer " + token + "");
        params.put("Accept", "application/json");
        params.put("QK_ACCESS_KEY", getResources().getString(R.string.QK_ACCESS_KEY) + "");

        mService.getEmployeeInfo(params, profile_id + "", login_user_id + "", "4").enqueue(new Callback<EmployeeDetailView>() {
            @Override
            public void onResponse(Call<EmployeeDetailView> call, Response<EmployeeDetailView> response) {
                if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();
                if (response != null && response.body() != null) {
                    EmployeeDetailView employeeDetailView = response.body();
                    parseData(employeeDetailView);
                } else
                    Log.e("Error", "Error" + "\n" + response.body());
            }

            @Override
            public void onFailure(Call<EmployeeDetailView> call, Throwable t) {
                Log.e("Error", "Error" + "\n" + t.getLocalizedMessage());
                if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();
            }
        });
    }

    public void parseData(EmployeeDetailView employeeDetailView) {

        try {
            if (employeeDetailView != null && employeeDetailView.getStatus().equals(200)) {

                // RealmController.with(Accept_Employee.this).deleteProfile(unique_code);

                try {
                    container.removeAllViews();
                    container_email.removeAllViews();
                    container_website.removeAllViews();
                    container_fax.removeAllViews();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                MyProfiles myProfiles = new MyProfiles();
                myProfiles.setId("" + employeeDetailView.getData().getProfile().getId());
                myProfiles.setType("" + employeeDetailView.getData().getType());
                myProfiles.setName(employeeDetailView.getData().getUsername());
                myProfiles.setLogo(employeeDetailView.getData().getProfilePic());
                myProfiles.setUnique_code(employeeDetailView.getData().getUniqueCode());


                if (employeeDetailView.getData().getAbout() != null && !employeeDetailView.getData().getAbout().equalsIgnoreCase(""))
                    myProfiles.setAbout(employeeDetailView.getData().getAbout());
                else
                    myProfiles.setAbout("");

                if (employeeDetailView.getData().getEmployeeNo() != null && !employeeDetailView.getData().getEmployeeNo().equalsIgnoreCase(""))
                    myProfiles.setEmployee_no(employeeDetailView.getData().getEmployeeNo());
                else
                    myProfiles.setEmployee_no("");

                if (employeeDetailView.getData().getDepartment() != null && !employeeDetailView.getData().getDepartment().equalsIgnoreCase(""))
                    myProfiles.setDepartment(employeeDetailView.getData().getDepartment());
                else
                    myProfiles.setDepartment("");

                if (employeeDetailView.getData().getDesignation() != null && !employeeDetailView.getData().getDesignation().equalsIgnoreCase(""))
                    myProfiles.setDesignation(employeeDetailView.getData().getDesignation());
                else
                    myProfiles.setDesignation("");

                if (employeeDetailView.getData().getValidTo() != null && !employeeDetailView.getData().getValidTo().equalsIgnoreCase(""))
                    myProfiles.setValid_to(employeeDetailView.getData().getValidTo());
                else
                    myProfiles.setValid_to("");

                if (employeeDetailView.getData().getValidFrom() != null && !employeeDetailView.getData().getValidFrom().equalsIgnoreCase(""))
                    myProfiles.setValid_from(employeeDetailView.getData().getValidFrom());
                else
                    myProfiles.setValid_from("");

                if (employeeDetailView.getData().getRole() != null)
                    myProfiles.setRole(employeeDetailView.getData().getRole() + "");
                else
                    myProfiles.setRole("");

                if (employeeDetailView.getData().getRoleName() != null && !employeeDetailView.getData().getRoleName().equalsIgnoreCase(""))
                    myProfiles.setRole_name(employeeDetailView.getData().getRoleName());
                else
                    myProfiles.setRole_name("");

                String profile = new Gson().toJson(employeeDetailView.getData().getProfile());
                myProfiles.setProfile(profile);

                String qkinfo = new Gson().toJson(employeeDetailView.getData().getQkTagInfo());
                myProfiles.setQktaginfo(qkinfo);

                String social_phone = new Gson().toJson(employeeDetailView.getData().getPhone());
                myProfiles.setPhone(social_phone);

                String social_fax = new Gson().toJson(employeeDetailView.getData().getFax());
                myProfiles.setFax(social_fax);

                String social_email = new Gson().toJson(employeeDetailView.getData().getEmail());
                myProfiles.setEmail(social_email);

                String social_weblink = new Gson().toJson(employeeDetailView.getData().getWeblink());
                myProfiles.setWeblink(social_weblink);

                RealmController.with(Accept_Employee.this).updateMyProfiles(myProfiles);

                showDataOffline(false);

            } else {
                Utilities.showToast(getApplicationContext(), employeeDetailView.getMsg());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void showDataOffline(final boolean isRealodData) {
        try {
            final MyProfiles myProfiles = RealmController.with(Accept_Employee.this).getMyProfielDataByProfileId(unique_code);
            if (myProfiles != null) {
                profile_id = myProfiles.getId() + "";
                try {
                    // Company Logo
                    /*String profile_pic = myProfiles.getLogo();
                    /*String profile_pic = myProfiles.getLogo();
                    if (profile_pic != null && Utilities.isEmpty(profile_pic)) {
                        Picasso.with(Accept_Employee.this)
                                .load(profile_pic)
                                .error(R.drawable.ic_default_profile_photo)
                                .transform(new CircleTransform())
                                .placeholder(R.drawable.ic_default_profile_photo)
                                .fit()
                                .into(profile_image);
                    } else {
                        profile_image.setImageResource(R.drawable.ic_default_profile_photo);
                    }

                    // Name
                    if (myProfiles.getName() != null || !myProfiles.getName().equalsIgnoreCase(""))
                        username.setText(myProfiles.getName() + "");
                    else
                        username.setVisibility(View.GONE);*/

                    if (myProfiles.getRole() != null && myProfiles.getRole().equalsIgnoreCase("4")) {
                        img_delete_employee.setVisibility(View.GONE);
                    } else if (isEditinfo) {
                        img_delete_employee.setVisibility(View.VISIBLE);
                    } else {
                        img_delete_employee.setVisibility(View.GONE);
                    }

                    if (myProfiles.getAbout() != null && !myProfiles.getAbout().equalsIgnoreCase("")) {
                        textin_about.setText(myProfiles.getAbout() + "");
                    }

                    // employee number
                    if (myProfiles.getEmployee_no() != null && !myProfiles.getEmployee_no().equalsIgnoreCase(""))
                        ed_emp_number_edit.setText("( " + myProfiles.getEmployee_no() + " )");
                    else
                        ed_emp_number_edit.setVisibility(View.GONE);

                    // employee department designation
                    if (myProfiles.getDepartment() != null && !myProfiles.getDepartment().equalsIgnoreCase("") ||
                            myProfiles.getDesignation() != null && !myProfiles.getDesignation().equalsIgnoreCase("")) {

                        ed_emp_department_edit.setText(myProfiles.getDepartment() + " , " + myProfiles.getDesignation());

                        if (ed_emp_department_edit.getText().toString().equalsIgnoreCase(" , ")) {
                            ed_emp_department_edit.setVisibility(View.GONE);
                        }
                    } else
                        ed_emp_department_edit.setVisibility(View.GONE);

                    try {
                        String msg = ed_emp_department_edit.getText().toString().charAt(1) + "";
                        if (msg.equalsIgnoreCase(",")) {
                            ed_emp_department_edit.setText(myProfiles.getDesignation() + "");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    // employee valid to
                    String dateformate = "";
                    if (myProfiles.getValid_to() != null || !myProfiles.getValid_to().equalsIgnoreCase("")) {
                        try {
                            dateformate = Utilities.changeDateFormat("dd/MM/yyyy", "MMM yyyy", myProfiles.getValid_to() + "");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        if (!dateformate.equalsIgnoreCase(""))
                            ed_emp_valid_from_edit.setText("Valid upto : " + dateformate);
                        else
                            ed_emp_valid_from_edit.setVisibility(View.GONE);
                    } else
                        ed_emp_valid_from_edit.setVisibility(View.GONE);


                    if (ed_emp_number_edit.getText().toString().equalsIgnoreCase("( )")) {
                        ed_emp_number_edit.setVisibility(View.GONE);
                    }
                    if (!ed_emp_department_edit.getText().toString().equalsIgnoreCase("")) {
                        String first = ed_emp_department_edit.getText().toString().charAt(0) + "";
                        if (first.equalsIgnoreCase(","))
                            ed_emp_department_edit.setVisibility(View.GONE);
                    }


                    // Phone
                    if (!myProfiles.getPhone().equalsIgnoreCase("") || myProfiles.getPhone() != null || !myProfiles.getPhone().equalsIgnoreCase("[]")) {
                        Type type = new TypeToken<List<Phone>>() {
                        }.getType();
                        List<Phone> phoneModel = new Gson().fromJson(myProfiles.getPhone(), type);
                        if (phoneModel != null) {
                            for (int i = 0; i < phoneModel.size(); i++) {
                                String phone = phoneModel.get(i).getName() + "";
                                AddPhone(phone);
                            }
                        }
                    }


                    // email
                    if (!myProfiles.getEmail().equalsIgnoreCase("") || myProfiles.getEmail() != null || !myProfiles.getEmail().equalsIgnoreCase("[]")) {
                        Type type = new TypeToken<List<Email>>() {
                        }.getType();
                        List<Email> phoneModel = new Gson().fromJson(myProfiles.getEmail(), type);
                        if (phoneModel != null) {
                            for (int i = 0; i < phoneModel.size(); i++) {
                                String email = phoneModel.get(i).getName() + "";
                                AddEmail(email);
                            }
                        }
                    }

                    // fax
                    if (!myProfiles.getFax().equalsIgnoreCase("") || myProfiles.getFax() != null || !myProfiles.getFax().equalsIgnoreCase("[]")) {
                        Type type = new TypeToken<List<Fax>>() {
                        }.getType();
                        List<Fax> phoneModel = new Gson().fromJson(myProfiles.getFax(), type);
                        if (phoneModel != null) {
                            for (int i = 0; i < phoneModel.size(); i++) {
                                String fax = phoneModel.get(i).getName() + "";
                                AddFax(fax);
                            }
                        }
                    }
                    // web
                    if (!myProfiles.getWeblink().equalsIgnoreCase("") || myProfiles.getWeblink() != null || !myProfiles.getWeblink().equalsIgnoreCase("[]")) {
                        Type type = new TypeToken<List<Weblink>>() {
                        }.getType();
                        List<Weblink> phoneModel = new Gson().fromJson(myProfiles.getWeblink(), type);
                        if (phoneModel != null) {
                            for (int i = 0; i < phoneModel.size(); i++) {
                                String web = phoneModel.get(i).getName() + "";
                                AddWeb(web);
                            }
                        }
                    }

                    if (isRealodData) {
                        CallApi(false);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void AddPhone(String phone) {
        try {
            LayoutInflater layoutInflater = (LayoutInflater) getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View addView = layoutInflater.inflate(R.layout.edit_with_plus_new, null);
            final EditText textOut1 = addView.findViewById(R.id.edittext_main);
            final FrameLayout frame_layput_below = addView.findViewById(R.id.frame_layput_below);
            CountryCodePicker ccp1 = addView.findViewById(R.id.ccp_phone);
            String CurrentString = phone + "";
            //String CurrentString = ccp.getSelectedCountryCodeWithPlus() + " " + textIn.getText().toString();
            String[] separated = CurrentString.split(" ");
            ccp1.enablePhoneAutoFormatter(true);
            try {
                ccp1.setCountryForPhoneCode(Integer.parseInt(separated[0]));
            } catch (Exception e) {
                e.printStackTrace();
            }
            textOut1.setText(separated[1]);
            final Button buttonRemove = addView.findViewById(R.id.remove);
            textOut1.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    if (textOut1.getText().toString().length() == 1 && textOut1.getText().toString().equalsIgnoreCase("0")) {
                        textOut1.setText("");
                    } else {
                        textOut1.setFilters(new InputFilter[]{filter1, new InputFilter.LengthFilter(15)});
                    }
                }

                @Override
                public void afterTextChanged(Editable editable) {
                }
            });
            textOut1.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View view, boolean hasFocus) {
                    if (!hasFocus) {
                        frame_layput_below.setBackgroundResource(R.color.text);
                        buttonRemove.setBackgroundResource(R.drawable.remove_invi);
                    } else {
                        frame_layput_below.setBackgroundResource(R.color.colorPrimary);
                        buttonRemove.setBackgroundResource(R.drawable.remove_vi);
                    }
                }
            });
            final View.OnClickListener thisListener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((LinearLayout) addView.getParent()).removeView(addView);
                    listAllAddView(TYPE_PHONE, 0);
                }
            };
            buttonRemove.setOnClickListener(thisListener);
            container.addView(addView);
            listAllAddView(TYPE_PHONE, 1);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void AddEmail(String email) {
        try {
            LayoutInflater layoutInflater = (LayoutInflater) getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View addView = layoutInflater.inflate(R.layout.edit_with_plus_new, null);
            final EditText textOut = addView.findViewById(R.id.edittext_main);
            //textOut.setHint("Email");
            textOut.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
            final FrameLayout frame_layput_below = addView.findViewById(R.id.frame_layput_below);
            CountryCodePicker ccp1 = addView.findViewById(R.id.ccp_phone);
            ccp1.setVisibility(View.GONE);
            String CurrentString = email + "";
            textOut.setText(CurrentString);
            final Button buttonRemove = addView.findViewById(R.id.remove);

            textOut.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View view, boolean hasFocus) {
                    if (!hasFocus) {
                        frame_layput_below.setBackgroundResource(R.color.text);
                        buttonRemove.setBackgroundResource(R.drawable.remove_invi);
                    } else {
                        frame_layput_below.setBackgroundResource(R.color.colorPrimary);
                        buttonRemove.setBackgroundResource(R.drawable.remove_vi);
                    }
                }
            });

            final View.OnClickListener thisListener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((LinearLayout) addView.getParent()).removeView(addView);
                    listAllAddView(TYPE_EMAIL, 0);
                }
            };
            buttonRemove.setOnClickListener(thisListener);
            container_email.addView(addView);
            listAllAddView(TYPE_EMAIL, 1);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void AddFax(String fax) {
        try {
            LayoutInflater layoutInflater = (LayoutInflater) getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View addView = layoutInflater.inflate(R.layout.edit_with_plus_new, null);
            final EditText textOut = addView.findViewById(R.id.edittext_main);
            //textOut.setHint("FAX");
            textOut.setInputType(InputType.TYPE_CLASS_NUMBER);
            final FrameLayout frame_layput_below = addView.findViewById(R.id.frame_layput_below);
            CountryCodePicker ccp1 = addView.findViewById(R.id.ccp_phone);
            String CurrentString = fax + "";
            //String CurrentString = ccp.getSelectedCountryCodeWithPlus() + " " + textIn.getText().toString();
            String[] separated = CurrentString.split(" ");
            ccp1.enablePhoneAutoFormatter(true);
            try {
                ccp1.setCountryForPhoneCode(Integer.parseInt(separated[0]));
            } catch (Exception e) {
                e.printStackTrace();
            }
            textOut.setText(separated[1]);
            final Button buttonRemove = addView.findViewById(R.id.remove);

            textOut.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View view, boolean hasFocus) {
                    if (!hasFocus) {
                        frame_layput_below.setBackgroundResource(R.color.text);
                        buttonRemove.setBackgroundResource(R.drawable.remove_invi);
                    } else {
                        frame_layput_below.setBackgroundResource(R.color.colorPrimary);
                        buttonRemove.setBackgroundResource(R.drawable.remove_vi);
                    }
                }
            });

            final View.OnClickListener thisListener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((LinearLayout) addView.getParent()).removeView(addView);
                    listAllAddView(TYPE_FAX, 0);
                }
            };
            buttonRemove.setOnClickListener(thisListener);
            container_fax.addView(addView);
            listAllAddView(TYPE_FAX, 1);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void AddWeb(String web) {
        try {
            LayoutInflater layoutInflater = (LayoutInflater) getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View addView = layoutInflater.inflate(R.layout.edit_with_plus_new, null);
            final EditText textOut = addView.findViewById(R.id.edittext_main);
            //textOut.setHint("Website");
            textOut.setInputType(InputType.TYPE_CLASS_TEXT);
            final FrameLayout frame_layput_below = addView.findViewById(R.id.frame_layput_below);
            CountryCodePicker ccp1 = addView.findViewById(R.id.ccp_phone);
            ccp1.setVisibility(View.GONE);
            String CurrentString = web + "";
            textOut.setText(CurrentString);
            final Button buttonRemove = addView.findViewById(R.id.remove);

            textOut.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View view, boolean hasFocus) {
                    if (!hasFocus) {
                        frame_layput_below.setBackgroundResource(R.color.text);
                        buttonRemove.setBackgroundResource(R.drawable.remove_invi);
                    } else {
                        frame_layput_below.setBackgroundResource(R.color.colorPrimary);
                        buttonRemove.setBackgroundResource(R.drawable.remove_vi);
                    }
                }
            });

            final View.OnClickListener thisListener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((LinearLayout) addView.getParent()).removeView(addView);
                    listAllAddView(TYPE_WEBSITE, 0);
                }
            };
            buttonRemove.setOnClickListener(thisListener);
            container_website.addView(addView);
            listAllAddView(TYPE_WEBSITE, 1);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                setResult(RESULT_OK);
                Accept_Employee.this.finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void deleteEmployee() {
        try {
            progressDialog = Utilities.showProgress(Accept_Employee.this);
            String userid = login_user_id.replace(".0", "");
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("web", "true");
            jsonObject.put("user_id", userid + "");
            jsonObject.put("profile_id", profile_id + "");

            new VolleyApiRequest(context, false, VolleyApiRequest.REQUEST_BASEURL + "api/user_account_delete",
                    new VolleyCallBack() {
                        @Override
                        public void onResponse(JSONObject response) {
                            if (progressDialog != null && progressDialog.isShowing()) {
                                progressDialog.dismiss();
                            }
                            Log.d("Response", response + "");
                            if (response != null) {
                                StausMessage stausMessage = new Gson().fromJson(response.toString(), StausMessage.class);
                                if (stausMessage != null && stausMessage.getStatus().equalsIgnoreCase("200")) {
                                    RealmController.with(Accept_Employee.this).deleteProfile(unique_code);
                                    setResult(RESULT_OK);
                                    try {
                                        Intent intent = new Intent("BROADCAST_FROM_DELETE_PROFILE");
                                        sendBroadcast(intent);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                    Accept_Employee.this.finish();
                                } else {
                                    Utilities.showToast(getApplicationContext(), stausMessage.getMsg() + "");
                                }
                            }
                        }

                        @Override
                        public void onErrorResponse(VolleyError error) {
                            if (progressDialog != null && progressDialog.isShowing()) {
                                progressDialog.dismiss();
                            }
                        }
                    }).enqueRequest(jsonObject, Request.Method.POST);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
