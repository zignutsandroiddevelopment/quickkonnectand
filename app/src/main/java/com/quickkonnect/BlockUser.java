package com.quickkonnect;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.activities.MatchProfileActivity;
import com.adapter.BlockProfileListAdapter;
import com.adapter.BlockUserListAdapter;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.facebook.Profile;
import com.google.gson.Gson;
import com.interfaces.BlockUserListInterface;
import com.interfaces.BlockedProfile;
import com.realmtable.BlockEmpList;
import com.realmtable.BlockUserListdata;
import com.realmtable.RealmController;
import com.utilities.Constants;
import com.utilities.Utilities;
import com.volleyapirequest.VolleyApiRequest;
import com.volleyapirequest.VolleyCallBack;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import io.realm.RealmResults;

public class BlockUser extends AppCompatActivity {

    private Context context;
    RecyclerView recyclerView;
    private static SharedPreferences mSharedPreferences;
    private RecyclerView.LayoutManager mLayoutManager;
    BlockUserListAdapter adapter;
    String userid;
    SwipeRefreshLayout swipeRefreshLayout;
    ArrayList<PojoClasses.BlockUserList> mlist;
    TextView tv_no_record_found;
    int FROM_MATCH_PROFILE = 1204;
    String Prof_id = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_block_user);

        Utilities.ChangeStatusBar(BlockUser.this);

        //Utilities.changeStatusbar(BlockUser.this, getWindow());
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_blocked_user);
        toolbar.setBackgroundResource(R.drawable.toolbar_gradient);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        context = BlockUser.this;

        try {
            Bundle b = getIntent().getExtras();
            Prof_id = b.getString("ProfileId");
        } catch (Exception e) {
            e.printStackTrace();
        }

        getSupportActionBar().setTitle("Blocked Users");
        mSharedPreferences = context.getSharedPreferences(SharedPreference.PREF_NAME, 0);
        userid = mSharedPreferences.getString(SharedPreference.CURRENT_USER_ID, "");

        swipeRefreshLayout = findViewById(R.id.swipeRefreshLayout_user);
        tv_no_record_found = findViewById(R.id.tv_no_record_found_Blocked_user);
        recyclerView = findViewById(R.id.recyclerView_Blocked_user);
        recyclerView.addItemDecoration(new SimpleDividerItemDecoration(context));
        recyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(mLayoutManager);

        if (Utilities.isNetworkConnected(BlockUser.this))
            getBlockedList();
        else
            getList();

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (Utilities.isNetworkConnected(BlockUser.this))
                    getBlockedList();
                else
                    getList();
            }
        });
    }

    private void getBlockedList() {
        swipeRefreshLayout.setRefreshing(true);
        JSONObject jobj = new JSONObject();
        try {
            jobj.put("profile_id", Prof_id + "");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        new VolleyApiRequest(context, false, VolleyApiRequest.REQUEST_BASEURL + "api/profile/block_list",
                new VolleyCallBack() {
                    @Override
                    public void onResponse(JSONObject response) {
                        swipeRefreshLayout.setRefreshing(false);
                        parseResponse(response);
                    }
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyApiRequest.showVolleyError(context, error);
                    }
                }).enqueRequest(jobj, Request.Method.POST);
    }

    public void parseResponse(JSONObject response) {
        try {
            String responseStatus = response.getString("status");
            String msg = response.getString("msg");
            RealmController.with(BlockUser.this).clearAllBlockedUserdata();

            if (responseStatus.equals("200")) {
                try {
                    JSONArray jsnArr = response.getJSONArray("data");
                    recyclerView.setVisibility(View.VISIBLE);
                    mlist = new ArrayList<>();
                    for (int i = 0; i < jsnArr.length(); i++) {
                        JSONObject obj = jsnArr.getJSONObject(i);

                        BlockUserListdata bl = new BlockUserListdata();
                        bl.setEmail(obj.getString("email"));
                        bl.setName(obj.getString("name"));
                        bl.setProfile_pic(obj.getString("profile_pic"));
                        bl.setUser_id(obj.getString("user_id"));
                        bl.setUnique_code(obj.getString("unique_code"));

                        RealmController.with(BlockUser.this).createUpdateBlockeduser(bl);
                    }
                } catch (Exception e) {
                    recyclerView.setVisibility(View.GONE);
                    tv_no_record_found.setVisibility(View.VISIBLE);
                    e.printStackTrace();
                }
                getList();
            } else {
                //Utilities.showToast(BlockUser.this,msg+"");
                recyclerView.setVisibility(View.GONE);
                tv_no_record_found.setVisibility(View.VISIBLE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getList() {
        RealmResults<BlockUserListdata> blockLists = RealmController.with(BlockUser.this).getBlockUserData();
        if (blockLists != null && blockLists.size() > 0) {
            mlist = new ArrayList<>();
            for (BlockUserListdata bl : blockLists) {
                mlist.add(new PojoClasses.BlockUserList(
                        bl.getUser_id(),
                        bl.getName(),
                        bl.getUnique_code(),
                        bl.getEmail(),
                        bl.getProfile_pic()));
            }
            adapter = new BlockUserListAdapter(BlockUser.this, mlist, blockUserListInterface);
            recyclerView.setAdapter(adapter);
        } else {
            getBlockedList();
        }
    }


    BlockUserListInterface blockUserListInterface = new BlockUserListInterface() {
        @Override
        public void onBlockedClickerd(int pos, PojoClasses.BlockUserList BlockList) {
            Intent intent = new Intent(context, MatchProfileActivity.class);
            intent.putExtra("unique_code", BlockList.getUnique_code() + "");
            intent.putExtra("ProfileId", Prof_id+"");
            intent.putExtra("UserId_Follower", BlockList.getUser_id() + "");
            intent.putExtra(Constants.KEY, Constants.KEY_FOLLOWER_UNBlock);
            startActivityForResult(intent, FROM_MATCH_PROFILE);
        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK && requestCode == FROM_MATCH_PROFILE) {
            getBlockedList();
        }
    }
}
