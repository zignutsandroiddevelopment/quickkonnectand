package com.quickkonnect;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.facebook.login.LoginManager;
import com.utilities.Constants;
import com.utilities.QKPreferences;
import com.utilities.Utilities;
import com.volleyapirequest.VolleyApiRequest;
import com.volleyapirequest.VolleyCallBack;

import org.json.JSONException;
import org.json.JSONObject;

public class ChangePasswordActivity extends AppCompatActivity implements View.OnClickListener {

    EditText edt_old_pass, edt_new_pass, edt_confirm_pass;
    TextInputLayout textInputLayout1, textInputLayout2, textInputLayout3;
    Button btn_change_password;
    private static SharedPreferences mSharedPreferences;
    String userid;
    private QKPreferences qkPreferences;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Utilities.changeStatusbar(ChangePasswordActivity.this, getWindow());
        setContentView(R.layout.activity_change_password);
        Toolbar toolbar = findViewById(R.id.toolbar);
        //toolbar.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        toolbar.setBackgroundResource(R.drawable.toolbar_gradient);
        setSupportActionBar(toolbar);
        context = ChangePasswordActivity.this;

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close_white_24dp);
        mSharedPreferences = getSharedPreferences(SharedPreference.PREF_NAME, 0);
        //userid = mSharedPreferences.getString(SharedPreference.CURRENT_USER_ID, "");
        qkPreferences = new QKPreferences(this);
        userid = qkPreferences.getLoggedUser().getData().getId() + "";
        edt_old_pass = findViewById(R.id.edt_old_pass);
        edt_new_pass = findViewById(R.id.edt_new_pass);
        edt_confirm_pass = findViewById(R.id.edt_confirm_pass);
        btn_change_password = findViewById(R.id.btn_change_password);
        textInputLayout1 = findViewById(R.id.textInputLayout1);
        textInputLayout2 = findViewById(R.id.textInputLayout2);
        textInputLayout3 = findViewById(R.id.textInputLayout3);

        btn_change_password.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        Utilities.hideKeyboard(ChangePasswordActivity.this);
        if (edt_old_pass.getText().toString().trim() == "" || edt_old_pass.getText().toString().trim().equals("") || edt_old_pass.getText().toString().trim().equals("null")) {
            Utilities.showToast(context, "Old Password is required");
        } else if (edt_new_pass.getText().toString().trim() == "" || edt_new_pass.getText().toString().trim().equals("") || edt_new_pass.getText().toString().trim().equals("null")) {
            Utilities.showToast(context, "New Password is required");
        } else if (edt_confirm_pass.getText().toString().trim() == "" || edt_confirm_pass.getText().toString().trim().equals("") || edt_confirm_pass.getText().toString().trim().equals("null")) {
            Utilities.showToast(context, "Confirm Password is required");
        } else if (!edt_confirm_pass.getText().toString().trim().matches(edt_new_pass.getText().toString().trim())) {
            Utilities.showToast(context, "Password and confirm password must be same.");
        } else {

            if (!Utilities.isNetworkConnected(this))
                Toast.makeText(this, Constants.NO_INTERNET_CONNECTION, Toast.LENGTH_LONG).show();
            else
                fnChangePassword(view, edt_old_pass.getText().toString().trim(), edt_new_pass.getText().toString().trim());

            //functionSignIn(view,edt_email.getText().toString().trim(),edt_password.getText().toString().trim());
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);

    }

    public void fnChangePassword(final View view, String old_password, String new_pass) {

        JSONObject obj = new JSONObject();
        try {
            obj.put("user_id", userid);
            obj.put("old_password", old_password);
            obj.put("new_password", new_pass);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.e("Object -> ", obj.toString());


        new VolleyApiRequest(context, true, VolleyApiRequest.REQUEST_BASEURL + "api/reset_password",
                new VolleyCallBack() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String status = response.getString("status");
                            String message = response.getString("msg");

                            if (status.equals("200")) {
                                Utilities.showToast(context, "Password Change Successfully");
                                finish();

                            } else if (status.equals("400")) {
                                if (message.equalsIgnoreCase("No user found")) {
                                    SharedPreferences.Editor e = mSharedPreferences.edit();
                                    e.clear();
                                    e.remove(SharedPreference.CURRENT_USER_ID);
                                    e.remove(SharedPreference.CURRENT_USER_NAME);
                                    e.commit();
                                    e.apply();
                                    Utilities.showToast(context, message+"");
                                    LoginManager.getInstance().logOut();
                                    Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    startActivity(intent);
                                    finish();
                                } else {
                                    Utilities.showToast(context, message);
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyApiRequest.showVolleyError(context, error);
                    }
                }).enqueRequest(obj, Request.Method.POST);

    }
}

