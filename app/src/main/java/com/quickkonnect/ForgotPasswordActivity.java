package com.quickkonnect;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.utilities.Constants;
import com.utilities.Utilities;
import com.volleyapirequest.VolleyApiRequest;
import com.volleyapirequest.VolleyCallBack;

import org.json.JSONException;
import org.json.JSONObject;

public class ForgotPasswordActivity extends AppCompatActivity {

    private Context context;
    private TextView btn_forgot_password;
    private EditText edt_forgot_email;
    private Toolbar toolbar;
    private TextInputLayout textInputLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_forgot_password);

        Utilities.ChangeStatusBar(ForgotPasswordActivity.this);

        context = ForgotPasswordActivity.this;
        toolbar = findViewById(R.id.toolbar_forgot_password);
        setSupportActionBar(toolbar);
        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close_white_24dp);
        getSupportActionBar().setTitle("");

        functionIntialization();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void functionIntialization() {

        edt_forgot_email = findViewById(R.id.edt_forgot_email);
        btn_forgot_password = findViewById(R.id.btn_forgot_password);
        textInputLayout = findViewById(R.id.textInputLayout);
        btn_forgot_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Utilities.hideKeyboard(ForgotPasswordActivity.this);

                if (edt_forgot_email.getText().toString().trim() == "" || edt_forgot_email.getText().toString().trim().equals("") || edt_forgot_email.getText().toString().trim().equals("null")) {
                    Utilities.showToast(context, "Email is required");
                    //} else if (!edt_forgot_email.getText().toString().trim().matches(Constants.emailPattern)) {
                } else if (!Utilities.isValidEmail(edt_forgot_email.getText().toString().trim() + "")) {
                    Utilities.showToast(context, "Invalid email address");
                } else {

                    if (!Utilities.isNetworkConnected(ForgotPasswordActivity.this))
                        Toast.makeText(ForgotPasswordActivity.this, Constants.NO_INTERNET_CONNECTION, Toast.LENGTH_LONG).show();
                    else
                        functionForgotPassword(view, edt_forgot_email.getText().toString().trim());
                }
            }
        });
    }

    public void functionForgotPassword(final View view, String email) {
        JSONObject obj = new JSONObject();
        try {
            obj.put("email", email);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        new VolleyApiRequest(context, true, VolleyApiRequest.REQUEST_BASEURL + "api/change_password",
                new VolleyCallBack() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String status = response.getString("status");
                            String message = response.getString("msg");
                            if (status.equals("200")) {

                                Utilities.showToast(context, message);
                                ForgotPasswordActivity.this.finish();

                            } else if (status.equals("400")) {
                                Utilities.showToast(context, message);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyApiRequest.showVolleyError(context, error);
                    }
                }).enqueRequest(obj, Request.Method.POST);
    }
}
