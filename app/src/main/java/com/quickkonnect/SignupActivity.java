package com.quickkonnect;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.Html;
import android.text.Selection;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.activities.ConfirmRegistration;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.code.linkedinapi.client.LinkedInApiClient;
import com.google.code.linkedinapi.client.LinkedInApiClientFactory;
import com.google.code.linkedinapi.client.enumeration.ProfileField;
import com.google.code.linkedinapi.client.oauth.LinkedInAccessToken;
import com.google.code.linkedinapi.schema.Person;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.interfaces.CallBackGetResult;
import com.interfaces.CallBackImageDownload;
import com.linkedin.LinkedinDialog;
import com.linkedin.platform.APIHelper;
import com.linkedin.platform.LISessionManager;
import com.linkedin.platform.errors.LIApiError;
import com.linkedin.platform.errors.LIAuthError;
import com.linkedin.platform.listeners.ApiListener;
import com.linkedin.platform.listeners.ApiResponse;
import com.linkedin.platform.listeners.AuthListener;
import com.linkedin.platform.utils.Scope;
import com.models.logindata.ModelLoggedUser;
import com.rilixtech.CountryCodePicker;
import com.services.BackgroundService;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;
import com.utilities.Constants;
import com.utilities.DownloadImage;
import com.utilities.QKPreferences;
import com.utilities.Utilities;
import com.volleyapirequest.VolleyApiRequest;
import com.volleyapirequest.VolleyCallBack;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.EnumSet;

import static android.webkit.URLUtil.isValidUrl;
import static com.linkedin.Config.LINKEDIN_CONSUMER_KEY;
import static com.linkedin.Config.LINKEDIN_CONSUMER_SECRET;
import static com.linkedin.Config.LINKEDIN_PACKAGE_NAME;


public class SignupActivity extends AppCompatActivity implements View.OnClickListener {

    private Context context;
    private EditText edt_firstname, edt_lastname, edt_email, edt_password, edt_re_password;
    private CheckBox chk_terms;
    private TextView tv_signin, btn_continue, btn_facebook, btn_linkedin_signin;
    private static SharedPreferences mSharedPreferences;
    private CallbackManager callbackManager;
    private LoginButton loginButton;
    private TextView tv_term_condition, tv_term_privacycontent;
    private String TAG = "TAG";
    final LinkedInApiClientFactory factory = LinkedInApiClientFactory
            .newInstance(LINKEDIN_CONSUMER_KEY,
                    LINKEDIN_CONSUMER_SECRET);
    private LinkedInApiClient client;
    private LinkedInAccessToken accessToken = null;
    private QKPreferences qkPreferences;

    private String LoginSocialMediaType = "";

    ProfileTracker profileTracker;

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (profileTracker != null) {
            profileTracker.stopTracking();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        //Utilities.changeStatusbar(SignupActivity.this, getWindow());
        FacebookSdk.sdkInitialize(this.getApplicationContext());
        setContentView(R.layout.activity_signup);

       /* if (Build.VERSION.SDK_INT >= 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }*/

        if (Build.VERSION.SDK_INT >= 19 && Build.VERSION.SDK_INT < 21) {
            setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, true);
        }
        if (Build.VERSION.SDK_INT >= 19) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }
        if (Build.VERSION.SDK_INT >= 21) {
            setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, false);
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }

        context = SignupActivity.this;

        qkPreferences = new QKPreferences(SignupActivity.this);

        functionInitialization();
    }

    public void functionInitialization() {

        mSharedPreferences = getSharedPreferences(SharedPreference.PREF_NAME, 0);

        edt_firstname = findViewById(R.id.edt_firstname);
        edt_lastname = findViewById(R.id.edt_lastname);
        edt_email = findViewById(R.id.edt_email);
        edt_password = findViewById(R.id.edt_password);
        edt_re_password = findViewById(R.id.edt_re_password);
        tv_signin = findViewById(R.id.tv_signin);
        btn_facebook = findViewById(R.id.btn_facebook);
        btn_linkedin_signin = findViewById(R.id.btn_linkedin_signin);
        chk_terms = findViewById(R.id.chk_terms);

        tv_term_condition = findViewById(R.id.tv_term_condition);
        tv_term_privacycontent = findViewById(R.id.tv_term_privacycontent);

        btn_continue = findViewById(R.id.btn_continue);

        btn_continue.setOnClickListener(this);
        btn_linkedin_signin.setOnClickListener(this);
        btn_facebook.setOnClickListener(this);
        tv_signin.setOnClickListener(this);

        tv_term_condition.setOnClickListener(this);
        tv_term_privacycontent.setOnClickListener(this);

        //callbackManager = CallbackManager.Factory.create();
        loginButton = findViewById(R.id.login_button);
        loginButton.setReadPermissions(AppClass.facebook_permission);
        callbackManager = CallbackManager.Factory.create();

        try {
            LoginManager.getInstance().logOut();
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Callback registration
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.d("FB TOKEN: ", "" + loginResult.getAccessToken());
                getFacebookUserData(loginResult.getAccessToken());
            }

            @Override
            public void onCancel() {
            }

            @Override
            public void onError(FacebookException exception) {
                Utilities.showToast(context, exception.getMessage());
            }
        });

        //String first = "Already have an account ?  ";
        String first = "<font color='#777777'>Already Member, </font>";
        String next = "<font color='#2ABBBE'><b>SIGN IN</b></font>";
        tv_signin.setText(Html.fromHtml(first + next));

        //String first1 = "Read  ";
        //String first1 = "I hereby accept the ";
        String next1 = "<font color='#000000'><b>Terms and Conditions</b></font>";

        tv_term_condition.setText(Html.fromHtml(next1 + ""));

        //String first2 = "have read the ";
        String next2 = "<font color='#000000'><b>Privacy Policy.</b></font>";

        tv_term_privacycontent.setText(Html.fromHtml(next2 + ""));

    }

    public static void setWindowFlag(Activity activity, final int bits, boolean on) {
        Window win = activity.getWindow();
        WindowManager.LayoutParams winParams = win.getAttributes();
        if (on) {
            winParams.flags |= bits;
        } else {
            winParams.flags &= ~bits;
        }
        win.setAttributes(winParams);
    }


    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.tv_signin:
                SignupActivity.this.finish();
                break;
            case R.id.btn_continue:

                String first_name = edt_firstname.getText().toString();
                String last_name = edt_lastname.getText().toString();
                String user_email = edt_email.getText().toString();
                String pasword = edt_password.getText().toString();
                String confirm_pass = edt_re_password.getText().toString();

                if (!Utilities.isEmpty(first_name))
                    Utilities.showToast(context, "First name is required");

                else if (!Utilities.isEmpty(last_name))
                    Utilities.showToast(context, "Last name is required");

                else if (!Utilities.isEmpty(user_email))
                    Utilities.showToast(context, "Email is required");

                else if (!user_email.trim().matches(Constants.emailPattern))
                    Utilities.showToast(context, "Invalid email address");

                else if (TextUtils.isEmpty(pasword) || pasword.length() < 6)
                    Utilities.showToast(context, "Password must contain at least 6 characters.");

                else if (!Utilities.isEmpty(confirm_pass))
                    Utilities.showToast(context, "Confirm password is required");

                else if (!pasword.trim().matches(confirm_pass.trim()))
                    Utilities.showToast(context, "Password and confirm password must be same.");

                else if (!chk_terms.isChecked())
                    Utilities.showToast(context, "Please agree with terms and condition.");

                else {
                    Utilities.hideKeyboard(SignupActivity.this);

                    if (!Utilities.isNetworkConnected(this))
                        Toast.makeText(this, Constants.NO_INTERNET_CONNECTION, Toast.LENGTH_LONG).show();
                    else
                        functionSignup(view, edt_firstname.getText().toString().trim(), edt_lastname.getText().toString().trim(), edt_email.getText().toString().trim(), edt_password.getText().toString().trim());

                }
                break;
            case R.id.btn_facebook:
                Utilities.showTermsCondpopUp(SignupActivity.this, "Alert", Constants.CONTENT_ALER_PRIVACY,
                        new CallBackGetResult() {
                            @Override
                            public void onSuccess() {
                                if (!Utilities.isNetworkConnected(SignupActivity.this))
                                    Toast.makeText(SignupActivity.this, Constants.NO_INTERNET_CONNECTION, Toast.LENGTH_LONG).show();
                                else
                                    loginButton.performClick();
                            }

                            @Override
                            public void onCancel() {

                            }
                        });
                break;

            case R.id.tv_term_condition:
                Intent termsofuse = new Intent(Intent.ACTION_VIEW, Uri.parse(Constants.URL_TERMSOFUSE));
                startActivity(termsofuse);
                break;

            case R.id.tv_term_privacycontent:
                Intent privacypolicy = new Intent(Intent.ACTION_VIEW, Uri.parse(Constants.URL_PRIVACYCONTENT));
                startActivity(privacypolicy);
                break;

            case R.id.btn_linkedin_signin:
                Utilities.showTermsCondpopUp(SignupActivity.this, "Alert", Constants.CONTENT_ALER_PRIVACY,
                        new CallBackGetResult() {
                            @Override
                            public void onSuccess() {

                                if (!Utilities.isNetworkConnected(SignupActivity.this))
                                    Toast.makeText(SignupActivity.this, Constants.NO_INTERNET_CONNECTION, Toast.LENGTH_LONG).show();
                                else {

                                    boolean isAppInstalled = Utilities.appInstalledOrNot(SignupActivity.this, LINKEDIN_PACKAGE_NAME);
                                    if (isAppInstalled) {
                                        LinkedInMapping();
                                    } else {
                                        linkedInLoginWithoutApp();
                                    }
                                }
                            }

                            @Override
                            public void onCancel() {

                            }
                        });
                break;
        }

    }


    public void functionSignup(final View view, String firstname, String lastname, String email, String password) {

        JSONObject obj = new JSONObject();
        try {
            obj.put("firstname", firstname);
            obj.put("lastname", lastname);
            obj.put("email", email);
            obj.put("password", password);
            if (FirebaseInstanceId.getInstance() != null &&
                    FirebaseInstanceId.getInstance().getToken() != null)
                obj.put("device_id", FirebaseInstanceId.getInstance().getToken());
            else
                obj.put("device_id", "");

        } catch (JSONException e) {
            e.printStackTrace();
        }
        new VolleyApiRequest(context, true, VolleyApiRequest.REQUEST_BASEURL + "api/create_user",
                new VolleyCallBack() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {
                            String status = response.getString("status");
                            String message = response.getString("msg");


                            if (status.equals("200")) {

                                JSONObject data = response.getJSONObject("data");
                                String id = data.getString("id");
                                String name = data.getString("name");

                                Utilities.showToast(context, message);
                                Intent intent = new Intent(SignupActivity.this, EmailConfirmationActivity.class);
                                intent.putExtra("email", edt_email.getText().toString().trim() + "");
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);

                            } else if (status.equals("400"))
                                Utilities.showToast(context, message);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyApiRequest.showVolleyError(context, error);
                    }
                }).enqueRequest(obj, Request.Method.POST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
        LISessionManager.getInstance(getApplicationContext()).onActivityResult(this, requestCode, resultCode, data);
    }

    public void getFacebookUserData(AccessToken accessToken) {

        profileTracker = new ProfileTracker() {
            @Override
            protected void onCurrentProfileChanged(Profile oldProfile, Profile currentProfile) {
                Profile profile = currentProfile;

                if (profile != null) {

                    JSONObject objectProfile = new JSONObject();
                    try {
                        objectProfile.put("id", profile.getId());
                        objectProfile.put("name", profile.getName());
                        objectProfile.put("first_name", profile.getFirstName());
                        objectProfile.put("last_name", profile.getLastName());
                        objectProfile.put("link", profile.getLinkUri());

                        JSONObject jsonObject = new JSONObject();
                        try {

                            String facebook_id = profile.getId();

                            jsonObject.put("social_media_user_id", facebook_id);
                            jsonObject.put("social_media_id", Constants.SOCIAL_FACEBOOK_ID);
                            jsonObject.put("device_type", "Android");
                            if (FirebaseInstanceId.getInstance() != null &&
                                    FirebaseInstanceId.getInstance().getToken() != null)
                                jsonObject.put("device_id", FirebaseInstanceId.getInstance().getToken());
                            else
                                jsonObject.put("device_id", "");

                            jsonObject.put("data", objectProfile);

                            Log.d(TAG, "request object: " + jsonObject);

                            sendSocialData(jsonObject);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        };

        profileTracker.startTracking();


//        GraphRequest request = GraphRequest.newMeRequest(accessToken.getCurrentAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
//            @Override
//            public void onCompleted(JSONObject object, GraphResponse response) {
//                JSONObject jsonObject = new JSONObject();
//                try {
//                    LoginSocialMediaType = "1";
//
//                    String facebook_id = object.optString("id");
//
//                    String link = object.optString("link");
//
//                    jsonObject.put("social_media_user_id", facebook_id);
//                    jsonObject.put("social_media_id", Constants.SOCIAL_FACEBOOK_ID);
//                    jsonObject.put("device_type", "Android");
//                    if (link != null && !link.equalsIgnoreCase("") && !link.equalsIgnoreCase("null"))
//                        object.put("publicProfileUrl", link + "");
//                    else
//                        object.put("publicProfileUrl", "");
//
//                    if (FirebaseInstanceId.getInstance() != null && FirebaseInstanceId.getInstance().getToken() != null)
//                        jsonObject.put("device_id", FirebaseInstanceId.getInstance().getToken());
//                    else
//                        jsonObject.put("device_id", "");
//
//                    jsonObject.put("data", object);
//
//                    Log.d(TAG, "request object: " + jsonObject);
//
//                    sendSocialData(jsonObject);
//
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
//        });
//        Bundle parameters = new Bundle();
//        parameters.putString("fields", "id,name,first_name,music,last_name,email,location{location},education,website,work,friendlists,friends,link,about,birthday,hometown,books,events,religion,political,tagged_places");
//        request.setParameters(parameters);
//        request.executeAsync();
    }

    // LinkedIn
    private static Scope buildScope() {
        return Scope.build(Scope.R_BASICPROFILE, Scope.R_EMAILADDRESS);
    }

    public void LinkedInMapping() {
        LISessionManager.getInstance(context).init(SignupActivity.this, buildScope(), new AuthListener() {
            @Override
            public void onAuthSuccess() {
                final APIHelper apiHelper = APIHelper.getInstance(getApplicationContext());
                apiHelper.getRequest(context, AppClass.LINKEDIN_URL, new ApiListener() {
                    @Override
                    public void onApiSuccess(final ApiResponse apiResponse) {
                        Log.d("LinkedIn ModelLogedta:", "" + apiResponse.getResponseDataAsJson());
                        JSONObject objData = apiResponse.getResponseDataAsJson();
                        try {
                            LoginSocialMediaType = "2";

                            JSONObject jsonObject = new JSONObject();

                            String linkedIn_id = objData.getString("id");

                            jsonObject.put("social_media_user_id", linkedIn_id);
                            jsonObject.put("social_media_id", Constants.SOCIAL_LINKEDIN_ID);
                            jsonObject.put("device_type", "Android");
                            if (FirebaseInstanceId.getInstance() != null &&
                                    FirebaseInstanceId.getInstance().getToken() != null)
                                jsonObject.put("device_id", FirebaseInstanceId.getInstance().getToken());
                            else
                                jsonObject.put("device_id", "");

                            jsonObject.put("data", objData);

                            Log.d(TAG, "request object: " + jsonObject);

                            sendSocialData(jsonObject);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onApiError(LIApiError liApiError) {
                        //Utilities.showToast(context, "" + liApiError.getHttpStatusCode());
                    }
                });
            }

            @Override
            public void onAuthError(LIAuthError error) {
                //Utilities.showToast(context, "" + error.toString());
            }
        }, true);

    }

    private void linkedInLoginWithoutApp() {
        ProgressDialog progressDialog = new ProgressDialog(this);
        LinkedinDialog d = new LinkedinDialog(this, progressDialog);
        d.show();
        d.setVerifierListener(new LinkedinDialog.OnVerifyListener() {
            @SuppressLint("NewApi")
            public void onVerify(String verifier) {
                Log.i("LinkedIn Verifier", "" + verifier);
                try {
                    LoginSocialMediaType = "2";

                    accessToken = LinkedinDialog.oAuthService.getOAuthAccessToken(LinkedinDialog.liToken, verifier);
                    factory.createLinkedInApiClient(accessToken);
                    client = factory.createLinkedInApiClient(accessToken);
                    Log.d("LinkedIn Token", "" + accessToken.getToken());
                    Log.d("LinkedIn Secret", "" + accessToken.getTokenSecret());
                    Log.d("LinkedIn Secret", client.getAccessToken().getTokenSecret());
                    Person p = client.getProfileForCurrentUser(EnumSet.of(
                            ProfileField.ID, ProfileField.FIRST_NAME,
                            ProfileField.PHONE_NUMBERS, ProfileField.LAST_NAME,
                            ProfileField.HEADLINE, ProfileField.INDUSTRY,
                            ProfileField.PICTURE_URL, ProfileField.PUBLIC_PROFILE_URL,
                            ProfileField.DATE_OF_BIRTH,
                            ProfileField.LOCATION_NAME, ProfileField.MAIN_ADDRESS,
                            ProfileField.LOCATION_COUNTRY));
                    Log.d("pppp", p + "");

                    JSONObject jsonObject = new JSONObject();

                    jsonObject.put("social_media_user_id", p.getId());
                    jsonObject.put("social_media_id", Constants.SOCIAL_LINKEDIN_ID);
                    jsonObject.put("device_type", "Android");

                    if (FirebaseInstanceId.getInstance() != null &&
                            FirebaseInstanceId.getInstance().getToken() != null)
                        jsonObject.put("device_id", FirebaseInstanceId.getInstance().getToken());
                    else
                        jsonObject.put("device_id", "");

                    JSONObject jsonLinkedinData = new JSONObject();

                    if (p.getPublicProfileUrl() != null)
                        jsonLinkedinData.put("publicProfileUrl", p.getPublicProfileUrl());

                    if (p.getFirstName() != null)
                        jsonLinkedinData.put("firstName", p.getFirstName());

                    if (p.getId() != null)
                        jsonLinkedinData.put("id", p.getId());
                    else
                        jsonLinkedinData.put("id", "");


                    if (p.getIndustry() != null)
                        jsonLinkedinData.put("industry", p.getIndustry());
                    else
                        jsonLinkedinData.put("industry", "");

                    if (p.getLastName() != null)
                        jsonLinkedinData.put("lastName", p.getLastName());
                    else
                        jsonLinkedinData.put("lastName", "");

                    JSONObject locationJson = new JSONObject();
                    if (p.getLocation() != null && p.getLocation().getName() != null)
                        locationJson.put("name", p.getLocation().getName());
                    else
                        locationJson.put("name", "");

                    JSONObject country = new JSONObject();
                    if (p.getLocation() != null && p.getLocation().getCountry() != null && p.getLocation().getCountry().getCode() != null)
                        country.put("code", p.getLocation().getCountry().getCode());
                    else
                        country.put("code", "");

                    locationJson.put("country", country);
                    jsonLinkedinData.put("location", locationJson);

                    jsonObject.put("data", jsonLinkedinData);

                    sendSocialData(jsonObject);


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(true);
        progressDialog.show();
    }

    private void sendSocialData(JSONObject jsonObject) {
        new VolleyApiRequest(context, true,
                VolleyApiRequest.REQUEST_BASEURL + VolleyApiRequest.API_CREATE_USER,
                new VolleyCallBack() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String status = response.getString("status");
                            String message = response.getString("msg");
                            if (status.equals("200")) {
                                qkPreferences.storeLoginFrom("SOCIAL");
                                parseResponse(response);
                            } else {
                                Utilities.showToast(context, message);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyApiRequest.showVolleyError(context, error);
                    }
                }).enqueRequest(jsonObject, Request.Method.POST);
    }

    public void parseResponse(JSONObject data) {

        try {

            ModelLoggedUser modelLoggedUser = new Gson().fromJson(data.toString(), ModelLoggedUser.class);
            if (modelLoggedUser != null && modelLoggedUser.getStatus() == 200) {

                qkPreferences.storeLoggedUser(modelLoggedUser);
                qkPreferences.storeApiToken(modelLoggedUser.getApiToken() + "");

                JSONObject qktagInfo = new JSONObject();
                qktagInfo.put(Constants.QKTAGURL, modelLoggedUser.getData().getQrCode());
                qktagInfo.put(Constants.QKMIDDLETAGURL, "");

                if (modelLoggedUser.getData().getBorderColor() != null && !modelLoggedUser.getData().getBorderColor().toString().isEmpty()
                        && !modelLoggedUser.getData().getBorderColor().toString().equals("0"))
                    qktagInfo.put(Constants.QKBORDERCOLOR, modelLoggedUser.getData().getBorderColor().toString());
                else
                    qktagInfo.put(Constants.QKBORDERCOLOR, getResources().getString(R.string.default_border));

                if (modelLoggedUser.getData().getBackgroundColor() != null && !modelLoggedUser.getData().getBackgroundColor().toString().isEmpty()
                        && !modelLoggedUser.getData().getBackgroundColor().toString().equals("0"))
                    qktagInfo.put(Constants.QKBACKCOLOR, modelLoggedUser.getData().getBackgroundColor().toString());
                else
                    qktagInfo.put(Constants.QKBACKCOLOR, getResources().getString(R.string.default_back));

                if (modelLoggedUser.getData().getPatternColor() != null && !modelLoggedUser.getData().getPatternColor().toString().isEmpty()
                        && !modelLoggedUser.getData().getPatternColor().toString().equals("0"))
                    qktagInfo.put(Constants.QKPATTERNCOLOR, modelLoggedUser.getData().getPatternColor().toString());
                else
                    qktagInfo.put(Constants.QKPATTERNCOLOR, getResources().getString(R.string.default_data));

                qkPreferences.storeQKInfo(qktagInfo.toString());

                SharedPreferences.Editor ed = mSharedPreferences.edit();
                ed.putString(SharedPreference.CURRENT_USER_ID, "" + modelLoggedUser.getData().getId());
                ed.putString(SharedPreference.CURRENT_USER_NAME, modelLoggedUser.getData().getFirstname()
                        + " " + modelLoggedUser.getData().getLastname());
                ed.putString(SharedPreference.CURRENT_USER_FIRSTTIME, "" + modelLoggedUser.getData().getFirsttime());
                ed.putString(SharedPreference.CURRENT_USER_EMAIL, modelLoggedUser.getData().getEmail());
                ed.putString(SharedPreference.CURRENT_USER_FULLNAME, modelLoggedUser.getData().getFirstname() + " " + modelLoggedUser.getData().getLastname());
                ed.putString(SharedPreference.CURRENT_USER_PROFILE_PIC, "" + modelLoggedUser.getData().getProfilePic());
                ed.putString(SharedPreference.CURRENT_UNIQU_QK_TAG_NUMBER, modelLoggedUser.getData().getUniqueCode());
                ed.putString(SharedPreference.NOTIFICATION_STATUS, "" + modelLoggedUser.getData().getNotificationStatus());
                ed.putString(Constants.QKTAGURL, "" + modelLoggedUser.getData().getQrCode());
                ed.apply();

                Object middle_tag = modelLoggedUser.getData().getMiddleTag();
                if (middle_tag != null && !middle_tag.toString().isEmpty() && !middle_tag.toString().equals("null")) {
                    new DownloadImage(context, new CallBackImageDownload() {
                        @Override
                        public void getBase64(String mData) {
                            try {
                                if (mData != null) {
                                    JSONObject jsonObject = new JSONObject(qkPreferences.getQKInfo());
                                    jsonObject.put(Constants.QKMIDDLETAGURL, mData);
                                    qkPreferences.storeQKInfo(jsonObject.toString());
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void getBitmap(Bitmap mData) {
                        }
                    }).execute(middle_tag.toString());
                }

                /*
                 *  for Connecting and login to XMPP openFire
                 */
                try {
                    // Already started in Splash therefor have to stop
                    stopService(new Intent(getApplicationContext(), BackgroundService.class));
                    // start again for connection
                    startService(new Intent(getApplicationContext(), BackgroundService.class));
                } catch (Exception e) {
                    e.printStackTrace();
                }

                /*********
                 * if status = 1 => socialmedia is integrated
                 * if status = 0 => not integrated
                 * status = 1= check public url ==> if (null){ open popup for email}else( go ahead)
                 ****/
                try {
                    if (LoginSocialMediaType != null && LoginSocialMediaType.equalsIgnoreCase("1") && !LoginSocialMediaType.equalsIgnoreCase("")) {
                        if (modelLoggedUser.getData().getFacebook() != null && modelLoggedUser.getData().getFacebook().getStatus() == 1) {
                            if (modelLoggedUser.getData().getFacebook().getData().getPublicProfileUrl() != null && !modelLoggedUser.getData().getFacebook().getData().getPublicProfileUrl().equalsIgnoreCase("") && !modelLoggedUser.getData().getFacebook().getData().getPublicProfileUrl().equalsIgnoreCase("null")) {
                                GoAhead(modelLoggedUser);
                            } else {
                                openSocialMediaEmailDialog(modelLoggedUser, 1);
                            }
                        } else {
                            GoAhead(modelLoggedUser);
                        }
                    } else if (LoginSocialMediaType != null && LoginSocialMediaType.equalsIgnoreCase("2") && !LoginSocialMediaType.equalsIgnoreCase("")) {
                        if (modelLoggedUser.getData().getLinkedin() != null && modelLoggedUser.getData().getLinkedin().getStatus() == 1) {
                            if (modelLoggedUser.getData().getLinkedin().getData().getPublicProfileUrl() != null && !modelLoggedUser.getData().getLinkedin().getData().getPublicProfileUrl().equalsIgnoreCase("") && !modelLoggedUser.getData().getLinkedin().getData().getPublicProfileUrl().equalsIgnoreCase("null")) {
                                GoAhead(modelLoggedUser);
                            } else {
                                openSocialMediaEmailDialog(modelLoggedUser, 2);
                            }
                        } else {
                            GoAhead(modelLoggedUser);
                        }
                    } else {
                        GoAhead(modelLoggedUser);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else
                Utilities.showToast(context, getResources().getString(R.string.error_process));

        } catch (Exception e) {
            Utilities.showToast(context, "" + e.getMessage());
        }
    }

    private void GoAhead(ModelLoggedUser modelLoggedUser) {
        if (modelLoggedUser.getData().getRegister() != null && modelLoggedUser.getData().getRegister() == 1) {
            try {
                modelLoggedUser.getData().setStatus(1);
                modelLoggedUser.getData().setRegister(null);
                qkPreferences.storeLoggedUser(modelLoggedUser);
            } catch (Exception e) {
                e.printStackTrace();
            }
            Intent intent = new Intent(context, AddSocialMediaAccountActivity.class);
            intent.putExtra("from", "other");
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        } else {
            if (modelLoggedUser.getData().getStatus() == 1) {
                Intent intent = new Intent(context, AddSocialMediaAccountActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            } else if (modelLoggedUser.getData().getStatus() == 2 || modelLoggedUser.getData().getStatus() == 3) {
                Intent intent = new Intent(context, ConfirmRegistration.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }

//            else if (modelLoggedUser.getData().getStatus() == 3) {
//                Intent intent = new Intent(context, UpdateQkTagActivity.class);
//                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
//                intent.putExtra("from", Constants.KEY_FROM_UPDATEPROFILE);
//                startActivity(intent);
//            }

            else if (modelLoggedUser.getData().getStatus() == 4) {
                Intent intent = new Intent(context, DashboardActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        }
        SignupActivity.this.finish();
    }

    String msg = "";
    String Url = "";
    String SOcialMediaId = "";

    private void openSocialMediaEmailDialog(final ModelLoggedUser modelLoggedUser, final int type) {

        final Dialog dialog = new Dialog(SignupActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View view = LinearLayout.inflate(SignupActivity.this, R.layout.dialog_addsocialmedia, null);
        dialog.setContentView(view);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        dialog.show();
        dialog.getWindow().setAttributes(lp);
        TextView tvAlert = view.findViewById(R.id.tvAlert);
        TextView tvDone = view.findViewById(R.id.tvDone);
        TextView tvCancel = view.findViewById(R.id.tvCancel);
        CountryCodePicker ccp = (CountryCodePicker) view.findViewById(R.id.ccp);
        LinearLayout lin_cpp = view.findViewById(R.id.lin_cpp);

        if (type == 1) {
            msg = "Add Facebook URL";
            Url = "https://www.facebook.com/";
            SOcialMediaId = "1";
        } else if (type == 2) {
            msg = "Add LinkedIn URL";
            Url = "https://www.linkedin.com/";
            SOcialMediaId = "2";
        }

        view.findViewById(R.id.divider_select_socialmedia).setVisibility(View.GONE);
        ccp.setVisibility(View.GONE);
        tvAlert.setText(msg);
        final SearchableSpinner spinnerSocialmedia = view.findViewById(R.id.spinnerSocialMedia);
        spinnerSocialmedia.setVisibility(View.GONE);
        final EditText edtSocialmediaLInk = view.findViewById(R.id.edt_socialmedia_link);
        edtSocialmediaLInk.setText(Url);
        Selection.setSelection(edtSocialmediaLInk.getText(), edtSocialmediaLInk.getText().length());

        edtSocialmediaLInk.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!s.toString().startsWith(Url)) {
                    edtSocialmediaLInk.setText(Url);
                    Selection.setSelection(edtSocialmediaLInk.getText(), edtSocialmediaLInk.getText().length());
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        tvDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    Utilities.hidekeyboard(SignupActivity.this, view);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                dialog.dismiss();
                if (!edtSocialmediaLInk.getText().toString().isEmpty() && isValidUrl(edtSocialmediaLInk.getText().toString())) {
                    String URL = edtSocialmediaLInk.getText().toString() + "";
                    UploadSocialmedia(URL + "", SOcialMediaId, dialog, modelLoggedUser);
                } else {
                    Utilities.showToast(SignupActivity.this, "Please enter valid URL");
                }
            }
        });

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    if (type == 1) {
                        Utilities.hidekeyboard(SignupActivity.this, view);
                        LoginManager.getInstance().logOut();
                    } else if (type == 2) {
                        LISessionManager.getInstance(SignupActivity.this).clearSession();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void UploadSocialmedia(String Url, String SocialMediaId, final Dialog dialog, final ModelLoggedUser modelLoggedUser) {
        try {
            //final ProgressDialog pd = Utilities.showProgress(LoginActivity.this);
            String userid = qkPreferences.getValuesInt(QKPreferences.USER_ID) + "";
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("user_id", userid);
            jsonObject.put("social_media_id", SocialMediaId + "");
            jsonObject.put("publicProfileUrl", Url + "");
            new VolleyApiRequest(context, true, VolleyApiRequest.REQUEST_BASEURL + "api/update_social_profile_url",
                    new VolleyCallBack() {
                        @Override
                        public void onResponse(JSONObject response) {
                           /* if (pd != null && pd.isShowing()) {
                                pd.dismiss();
                            }*/
                            try {
                                if (response != null && response.length() > 0) {
                                    String ststus = response.getString("status");
                                    String message = response.getString("msg");

                                    if (ststus.equalsIgnoreCase("200")) {
                                        if (dialog != null && dialog.isShowing()) {
                                            dialog.dismiss();
                                        }
                                        GoAhead(modelLoggedUser);
                                    } else
                                        Utilities.showToast(context, message + "");
                                } else

                                    Utilities.showToast(context, "Please try again");

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onErrorResponse(VolleyError error) {
                            /*if (pd != null && pd.isShowing()) {
                                pd.dismiss();
                            }*/
                            //VolleyApiRequest.showVolleyError(context, error);
                        }
                    }).enqueRequest(jsonObject, Request.Method.POST);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
