package com.quickkonnect;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by ZTLAB03 on 26-09-2017.
 */

public class PojoClasses {

    public static class Country {
        String country_id;
        String country_name;

        public Country(String country_id, String country_name) {
            this.country_id = country_id;
            this.country_name = country_name;
        }

        public Country(String country_name) {
            this.country_name = country_name;
        }

        public String getCountry_id() {
            return country_id;
        }

        public void setCountry_id(String country_id) {
            this.country_id = country_id;
        }

        public String getCountry_name() {
            return country_name;
        }

        public void setCountry_name(String country_name) {
            this.country_name = country_name;
        }

        @Override
        public String toString() {
            return country_name;
        }
    }

    public static class State {
        String state_id;
        String state_name;

        public State(String state_id, String state_name) {
            this.state_id = state_id;
            this.state_name = state_name;
        }

        public State(String state_name) {
            this.state_name = state_name;
        }

        public String getState_id() {
            return state_id;
        }

        public void setState_id(String state_id) {
            this.state_id = state_id;
        }

        public String getState_name() {
            return state_name;
        }

        public void setState_name(String state_name) {
            this.state_name = state_name;
        }

        @Override
        public String toString() {
            return state_name;
        }
    }

    public static class City {
        String id;
        String name;

        public City(String id, String name) {
            this.id = id;
            this.name = name;
        }

        public City(String name) {
            this.name = name;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        @Override
        public String toString() {
            return name;
        }
    }

    public static class SocialMediaAccount {

        int id;
        String socialMediaName;
        String socialMediaImage;
        String status;

        public SocialMediaAccount(int id, String socialMediaName, String socialMediaImage, String status) {
            this.id = id;
            this.socialMediaName = socialMediaName;
            this.socialMediaImage = socialMediaImage;
            this.status = status;
        }

        public SocialMediaAccount(String socialMediaName, String socialMediaImage, String status) {
            this.socialMediaName = socialMediaName;
            this.socialMediaImage = socialMediaImage;
            this.status = status;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getSocialMediaName() {
            return socialMediaName;
        }

        public void setSocialMediaName(String socialMediaName) {
            this.socialMediaName = socialMediaName;
        }

        public String getSocialMediaImage() {
            return socialMediaImage;
        }

        public void setSocialMediaImage(String socialMediaImage) {
            this.socialMediaImage = socialMediaImage;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        @Override
        public String toString() {
            return "SocialMediaAccount{" +
                    "id=" + id +
                    ", socialMediaName='" + socialMediaName + '\'' +
                    ", socialMediaImage='" + socialMediaImage + '\'' +
                    '}';
        }
    }

    public static class Education {
        int education_id;
        String schoolname;
        String degree;
        String fieldofstudy;
        String activities;
        String startdate;
        String enddate;


        public Education(int education_id, String schoolname, String degree, String fieldofstudy, String activities, String startdate, String enddate) {
            this.education_id = education_id;
            this.schoolname = schoolname;
            this.degree = degree;
            this.fieldofstudy = fieldofstudy;
            this.activities = activities;
            this.startdate = startdate;
            this.enddate = enddate;

        }

        public int getEducation_id() {
            return education_id;
        }

        public void setEducation_id(int education_id) {
            this.education_id = education_id;
        }

        public String getSchoolname() {
            return schoolname;
        }

        public void setSchoolname(String schoolname) {
            this.schoolname = schoolname;
        }

        public String getDegree() {
            return degree;
        }

        public void setDegree(String degree) {
            this.degree = degree;
        }

        public String getFieldofstudy() {
            return fieldofstudy;
        }

        public void setFieldofstudy(String fieldofstudy) {
            this.fieldofstudy = fieldofstudy;
        }

        public String getStartdate() {
            return startdate;
        }

        public void setStartdate(String startdate) {
            this.startdate = startdate;
        }

        public String getEnddate() {
            return enddate;
        }

        public void setEnddate(String enddate) {
            this.enddate = enddate;
        }

        public String getActivities() {
            return activities;
        }

        public void setActivities(String activities) {
            this.activities = activities;
        }

        @Override
        public String toString() {
            return "Education{" +
                    "education_id=" + education_id +
                    ", schoolname='" + schoolname + '\'' +
                    ", degree='" + degree + '\'' +
                    ", fieldofstudy='" + fieldofstudy + '\'' +
                    ", activities='" + activities + '\'' +
                    ", startdate='" + startdate + '\'' +
                    ", enddate='" + enddate + '\'' +

                    '}';
        }
    }

    public static class ContactInfo {

        int contact_id;
        String contact_type;
        String email;
        String phone;

        public ContactInfo(int contact_id, String contact_type, String email, String phone) {
            this.contact_id = contact_id;
            this.contact_type = contact_type;
            this.email = email;
            this.phone = phone;
        }

        public int getContact_id() {
            return contact_id;
        }

        public void setContact_id(int contact_id) {
            this.contact_id = contact_id;
        }

        public String getContact_type() {
            return contact_type;
        }

        public void setContact_type(String contact_type) {
            this.contact_type = contact_type;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        @Override
        public String toString() {
            return "ContactInfo{" +
                    "contact_id=" + contact_id +
                    ", contact_type='" + contact_type + '\'' +
                    ", email='" + email + '\'' +
                    ", phone='" + phone + '\'' +
                    '}';
        }
    }

    public static class Experience {
        int experience_id;
        String title;
        String company_name;
        String location;
        String start_date;
        String end_date;

        public Experience(int experience_id, String title, String company_name, String location, String start_date, String end_date) {
            this.experience_id = experience_id;
            this.title = title;
            this.company_name = company_name;
            this.location = location;
            this.start_date = start_date;
            this.end_date = end_date;
        }

        public int getExperience_id() {
            return experience_id;
        }

        public void setExperience_id(int experience_id) {
            this.experience_id = experience_id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getCompany_name() {
            return company_name;
        }

        public void setCompany_name(String company_name) {
            this.company_name = company_name;
        }

        public String getLocation() {
            return location;
        }

        public void setLocation(String location) {
            this.location = location;
        }

        public String getStart_date() {
            return start_date;
        }

        public void setStart_date(String start_date) {
            this.start_date = start_date;
        }

        public String getEnd_date() {
            return end_date;
        }

        public void setEnd_date(String end_date) {
            this.end_date = end_date;
        }

        @Override
        public String toString() {
            return "Experience{" +
                    "experience_id=" + experience_id +
                    ", title='" + title + '\'' +
                    ", company_name='" + company_name + '\'' +
                    ", location='" + location + '\'' +
                    ", start_date='" + start_date + '\'' +
                    ", end_date='" + end_date + '\'' +
                    '}';
        }
    }

    public static class Publication {
        int publication_id;
        String publication_title;
        String publisher_name;
        String date;

        public Publication(int publication_id, String publication_title, String publisher_name, String date) {
            this.publication_id = publication_id;
            this.publication_title = publication_title;
            this.publisher_name = publisher_name;
            this.date = date;
        }

        public int getPublication_id() {
            return publication_id;
        }

        public void setPublication_id(int publication_id) {
            this.publication_id = publication_id;
        }

        public String getPublication_title() {
            return publication_title;
        }

        public void setPublication_title(String publication_title) {
            this.publication_title = publication_title;
        }

        public String getPublisher_name() {
            return publisher_name;
        }

        public void setPublisher_name(String publisher_name) {
            this.publisher_name = publisher_name;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        @Override
        public String toString() {
            return "Publication{" +
                    "publication_id=" + publication_id +
                    ", publication_title='" + publication_title + '\'' +
                    ", publisher_name='" + publisher_name + '\'' +
                    ", date='" + date + '\'' +
                    '}';
        }
    }

    public static class Language {
        int langauge_id;
        String langauge_name;
        String rating;

        public Language(int langauge_id, String langauge_name, String rating) {
            this.langauge_id = langauge_id;
            this.langauge_name = langauge_name;
            this.rating = rating;
        }

        public int getLangauge_id() {
            return langauge_id;
        }

        public void setLangauge_id(int langauge_id) {
            this.langauge_id = langauge_id;
        }

        public String getLangauge_name() {
            return langauge_name;
        }

        public void setLangauge_name(String langauge_name) {
            this.langauge_name = langauge_name;
        }

        public String getRating() {
            return rating;
        }

        public void setRating(String rating) {
            this.rating = rating;
        }

        @Override
        public String toString() {
            return "Language{" +
                    "langauge_id=" + langauge_id +
                    ", langauge_name='" + langauge_name + '\'' +
                    ", rating='" + rating + '\'' +
                    '}';
        }
    }

    public static class SocialScanResult {
        String social_media_id;
        String social_email;
        String social_phone;
        String social_about;
        String social_religion;
        String social_politics;
        String social_media_count;
        String social_followers_count;
        String social_followed_by_count;
        String social_friend_list_count;
        String social_likes_count;
        String social_website;
        String publicProfileUrl;
        String social_media_user_id;


        public SocialScanResult(String social_media_user_id,String social_media_id, String social_email, String social_phone, String social_about, String social_religion, String social_politics, String social_media_count, String social_followers_count, String social_followed_by_count, String social_friend_list_count, String social_likes_count, String social_website, String publicProfileUrl) {
            this.social_media_user_id = social_media_user_id;
            this.social_media_id = social_media_id;
            this.social_email = social_email;
            this.social_phone = social_phone;
            this.social_about = social_about;
            this.social_religion = social_religion;
            this.social_politics = social_politics;
            this.social_media_count = social_media_count;
            this.social_followers_count = social_followers_count;
            this.social_followed_by_count = social_followed_by_count;
            this.social_friend_list_count = social_friend_list_count;
            this.social_likes_count = social_likes_count;
            this.social_website = social_website;
            this.publicProfileUrl = publicProfileUrl;

        }

        public String getSocial_media_id() {
            return social_media_id;
        }

        public void setSocial_media_id(String social_media_id) {
            this.social_media_id = social_media_id;
        }

        public String getPublicProfileUrl() {
            return publicProfileUrl;
        }

        public void setPublicProfileUrl(String publicProfileUrl) {
            this.publicProfileUrl = publicProfileUrl;
        }

        public String getSocial_media_user_id() {
            return social_media_user_id;
        }

        public void setSocial_media_user_id(String social_media_user_id) {
            this.social_media_user_id = social_media_user_id;
        }

        public String getSocial_email() {
            return social_email;
        }

        public void setSocial_email(String social_email) {
            this.social_email = social_email;
        }

        public String getSocial_phone() {
            return social_phone;
        }

        public void setSocial_phone(String social_phone) {
            this.social_phone = social_phone;
        }

        public String getSocial_about() {
            return social_about;
        }

        public void setSocial_about(String social_about) {
            this.social_about = social_about;
        }

        public String getSocial_religion() {
            return social_religion;
        }

        public void setSocial_religion(String social_religion) {
            this.social_religion = social_religion;
        }

        public String getSocial_politics() {
            return social_politics;
        }

        public void setSocial_politics(String social_politics) {
            this.social_politics = social_politics;
        }

        public String getSocial_media_count() {
            return social_media_count;
        }

        public void setSocial_media_count(String social_media_count) {
            this.social_media_count = social_media_count;
        }

        public String getSocial_followers_count() {
            return social_followers_count;
        }

        public void setSocial_followers_count(String social_followers_count) {
            this.social_followers_count = social_followers_count;
        }

        public String getSocial_followed_by_count() {
            return social_followed_by_count;
        }

        public void setSocial_followed_by_count(String social_followed_by_count) {
            this.social_followed_by_count = social_followed_by_count;
        }

        public String getSocial_friend_list_count() {
            return social_friend_list_count;
        }

        public void setSocial_friend_list_count(String social_friend_list_count) {
            this.social_friend_list_count = social_friend_list_count;
        }

        public String getSocial_likes_count() {
            return social_likes_count;
        }

        public void setSocial_likes_count(String social_likes_count) {
            this.social_likes_count = social_likes_count;
        }

        public String getSocial_website() {
            return social_website;
        }

        public void setSocial_website(String social_website) {
            this.social_website = social_website;
        }


        @Override
        public String toString() {
            return "SocialScanResult{" +
                    "social_media_id='" + social_media_id + '\'' +
                    ", social_email='" + social_email + '\'' +
                    ", social_phone='" + social_phone + '\'' +
                    ", social_about='" + social_about + '\'' +
                    ", social_religion='" + social_religion + '\'' +
                    ", social_politics='" + social_politics + '\'' +
                    ", social_media_count='" + social_media_count + '\'' +
                    ", social_followers_count='" + social_followers_count + '\'' +
                    ", social_followed_by_count='" + social_followed_by_count + '\'' +
                    ", social_friend_list_count='" + social_friend_list_count + '\'' +
                    ", social_likes_count='" + social_likes_count + '\'' +
                    ", social_website='" + social_website + '\'' +
                    ", publicProfileUrl='" + publicProfileUrl + '\'' +
                    '}';
        }

    }

    public static class ContactsList {
        String user_id;
        String name;
        String profile_pic;
        String scan_user_unique_code;
        String date;
        String firstname;
        String lastname;
        String scanned_by;
        String phone;
        String email;

        public ContactsList(String user_id, String name, String profile_pic, String firstname, String lastname) {
            this.user_id = user_id;
            this.name = name;
            this.profile_pic = profile_pic;
            this.profile_pic = date;
            this.firstname = firstname;
            this.lastname = lastname;

        }

        public ContactsList(String user_id, String name, String profile_pic, String scan_user_unique_code, String firstname, String lastname) {
            this.user_id = user_id;
            this.name = name;
            this.profile_pic = profile_pic;
            this.scan_user_unique_code = scan_user_unique_code;
            this.firstname = firstname;
            this.lastname = lastname;
        }

        public ContactsList(String email, String phone, String scanned_by, String user_id, String name, String profile_pic, String scan_user_unique_code, String date, String firstname, String lastname) {
            this.email = email;
            this.phone = phone;
            this.scanned_by = scanned_by;
            this.user_id = user_id;
            this.name = name;
            this.profile_pic = profile_pic;
            this.scan_user_unique_code = scan_user_unique_code;
            this.date = date;
            this.firstname = firstname;
            this.lastname = lastname;
        }

        public ContactsList(String user_id, String name, String profile_pic, String scan_user_unique_code, String date, String firstname, String lastname) {
            this.user_id = user_id;
            this.name = name;
            this.profile_pic = profile_pic;
            this.scan_user_unique_code = scan_user_unique_code;
            this.date = date;
            this.firstname = firstname;
            this.lastname = lastname;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getScanned_by() {
            return scanned_by;
        }

        public void setScanned_by(String scanned_by) {
            this.scanned_by = scanned_by;
        }

        public String getFirstname() {
            return firstname;
        }

        public void setFirstname(String firstname) {
            this.firstname = firstname;
        }

        public String getLastname() {
            return lastname;
        }

        public void setLastname(String lastname) {
            this.lastname = lastname;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getProfile_pic() {
            return profile_pic;
        }

        public void setProfile_pic(String profile_pic) {
            this.profile_pic = profile_pic;
        }

        public String getScan_user_unique_code() {
            return scan_user_unique_code;
        }

        public void setScan_user_unique_code(String scan_user_unique_code) {
            this.scan_user_unique_code = scan_user_unique_code;
        }

        @Override
        public String toString() {
            return "ContactsList{" +
                    "user_id='" + user_id + '\'' +
                    ", name='" + name + '\'' +
                    ", profile_pic='" + profile_pic + '\'' +
                    ", scan_user_unique_code='" + scan_user_unique_code + '\'' +
                    ", firstname='" + firstname + '\'' +
                    ", lastname='" + lastname + '\'' +
                    '}';
        }
    }

    public static class FollowersList_Contact {

        String user_id;
        String name;
        String email;
        String unique_code;
        String profile_pic;
        String profile_id;
        String profile_name;
        String scan_date;
        String phone;

        public FollowersList_Contact(String scan_date, String phone, String user_id, String name, String email, String unique_code, String profile_pic, String profile_id, String profile_name) {
            this.scan_date = scan_date;
            this.phone = phone;
            this.user_id = user_id;
            this.name = name;
            this.email = email;
            this.unique_code = unique_code;
            this.profile_pic = profile_pic;
            this.profile_id = profile_id;
            this.profile_name = profile_name;
        }

        public String getProfile_id() {
            return profile_id;
        }

        public void setProfile_id(String profile_id) {
            this.profile_id = profile_id;
        }

        public String getProfile_name() {
            return profile_name;
        }

        public void setProfile_name(String profile_name) {
            this.profile_name = profile_name;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getUnique_code() {
            return unique_code;
        }

        public String getScan_date() {
            return scan_date;
        }

        public void setScan_date(String scan_date) {
            this.scan_date = scan_date;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public void setUnique_code(String unique_code) {
            this.unique_code = unique_code;
        }

        public String getProfile_pic() {
            return profile_pic;
        }

        public void setProfile_pic(String profile_pic) {
            this.profile_pic = profile_pic;
        }
    }

    public static class FollowingList_Contact {
        String id;
        String name;
        String status;
        String reason;
        String type;
        String logo;
        String role;
        String unique_code;
        String border_color;
        String pattern_color;
        String background_color;
        String middle_tag;
        String qr_code;
        String background_gradient;
        String followed_on;
        String followed_at;

        public FollowingList_Contact(String id, String name, String status, String reason, String type, String logo, String role, String unique_code, String border_color, String pattern_color, String background_color, String middle_tag, String qr_code, String background_gradient, String followed_on, String followed_at) {
            this.id = id;
            this.name = name;
            this.status = status;
            this.reason = reason;
            this.type = type;
            this.logo = logo;
            this.role = role;
            this.unique_code = unique_code;
            this.border_color = border_color;
            this.pattern_color = pattern_color;
            this.background_color = background_color;
            this.middle_tag = middle_tag;
            this.qr_code = qr_code;
            this.background_gradient = background_gradient;
            this.followed_on = followed_on;
            this.followed_at = followed_at;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getReason() {
            return reason;
        }

        public void setReason(String reason) {
            this.reason = reason;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getLogo() {
            return logo;
        }

        public void setLogo(String logo) {
            this.logo = logo;
        }

        public String getRole() {
            return role;
        }

        public void setRole(String role) {
            this.role = role;
        }

        public String getUnique_code() {
            return unique_code;
        }

        public void setUnique_code(String unique_code) {
            this.unique_code = unique_code;
        }

        public String getBorder_color() {
            return border_color;
        }

        public void setBorder_color(String border_color) {
            this.border_color = border_color;
        }

        public String getPattern_color() {
            return pattern_color;
        }

        public void setPattern_color(String pattern_color) {
            this.pattern_color = pattern_color;
        }

        public String getBackground_color() {
            return background_color;
        }

        public void setBackground_color(String background_color) {
            this.background_color = background_color;
        }

        public String getMiddle_tag() {
            return middle_tag;
        }

        public void setMiddle_tag(String middle_tag) {
            this.middle_tag = middle_tag;
        }

        public String getQr_code() {
            return qr_code;
        }

        public void setQr_code(String qr_code) {
            this.qr_code = qr_code;
        }

        public String getBackground_gradient() {
            return background_gradient;
        }

        public void setBackground_gradient(String background_gradient) {
            this.background_gradient = background_gradient;
        }

        public String getFollowed_on() {
            return followed_on;
        }

        public void setFollowed_on(String followed_on) {
            this.followed_on = followed_on;
        }

        public String getFollowed_at() {
            return followed_at;
        }

        public void setFollowed_at(String followed_at) {
            this.followed_at = followed_at;
        }
    }

    public static class FollowersList {
        String user_id;
        String name;
        String email;
        String unique_code;
        String profile_pic;

        public FollowersList(String user_id, String name, String email, String unique_code, String profile_pic) {
            this.user_id = user_id;
            this.name = name;
            this.email = email;
            this.unique_code = unique_code;
            this.profile_pic = profile_pic;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getUnique_code() {
            return unique_code;
        }

        public void setUnique_code(String unique_code) {
            this.unique_code = unique_code;
        }

        public String getProfile_pic() {
            return profile_pic;
        }

        public void setProfile_pic(String profile_pic) {
            this.profile_pic = profile_pic;
        }
    }

    public static class EmployeesList {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("profile_id")
        @Expose
        private String profileId;
        @SerializedName("user_id")
        @Expose
        private String userId;
        @SerializedName("employee_no")
        @Expose
        private String employeeNo;
        @SerializedName("valid_from")
        @Expose
        private String validFrom;
        @SerializedName("valid_to")
        @Expose
        private String validTo;
        @SerializedName("designation")
        @Expose
        private String designation;
        @SerializedName("department")
        @Expose
        private String department;
        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("reason")
        @Expose
        private String reason;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("updated_at")
        @Expose
        private String updatedAt;

        @SerializedName("user1")
        @Expose
        private String user1;

        @SerializedName("username")
        @Expose
        private String username;

        @SerializedName("profile1")
        @Expose
        private String profile1;

        @SerializedName("role")
        @Expose
        private String role;

        @SerializedName("role_name")
        @Expose
        private String role_name;

        public String getProfile1() {
            return profile1;
        }

        public void setProfile1(String profile1) {
            this.profile1 = profile1;
        }

        public String getUser1() {
            return user1;
        }

        public void setUser1(String user1) {
            this.user1 = user1;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getRole() {
            return role;
        }

        public void setRole(String role) {
            this.role = role;
        }

        public String getRole_name() {
            return role_name;
        }

        public void setRole_name(String role_name) {
            this.role_name = role_name;
        }

        public EmployeesList(String username, String id, String profileId, String userId, String employeeNo, String validFrom, String validTo, String designation, String department, String status, String reason, String createdAt, String updatedAt, String user1, String role, String role_name) {
            this.username = username;
            this.id = id;
            this.profileId = profileId;
            this.userId = userId;
            this.employeeNo = employeeNo;
            this.validFrom = validFrom;
            this.validTo = validTo;
            this.designation = designation;
            this.department = department;
            this.status = status;
            this.reason = reason;
            this.createdAt = createdAt;
            this.updatedAt = updatedAt;
            this.user1 = user1;
            this.role = role;
            this.role_name = role_name;
        }

        public EmployeesList(String id, String profileId, String userId, String employeeNo, String validFrom, String validTo, String designation, String department, String status, String reason, String user1, String profile1) {
            this.id = id;
            this.profileId = profileId;
            this.userId = userId;
            this.employeeNo = employeeNo;
            this.validFrom = validFrom;
            this.validTo = validTo;
            this.designation = designation;
            this.department = department;
            this.status = status;
            this.reason = reason;
            this.user1 = user1;
            this.profile1 = profile1;
        }

        @SerializedName("user")
        @Expose
        private User user;

        @SerializedName("profile")
        @Expose
        private Profile profile;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getProfileId() {
            return profileId;
        }

        public void setProfileId(String profileId) {
            this.profileId = profileId;
        }

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public String getEmployeeNo() {
            return employeeNo;
        }

        public void setEmployeeNo(String employeeNo) {
            this.employeeNo = employeeNo;
        }

        public String getValidFrom() {
            return validFrom;
        }

        public void setValidFrom(String validFrom) {
            this.validFrom = validFrom;
        }

        public String getValidTo() {
            return validTo;
        }

        public void setValidTo(String validTo) {
            this.validTo = validTo;
        }

        public String getDesignation() {
            return designation;
        }

        public void setDesignation(String designation) {
            this.designation = designation;
        }

        public String getDepartment() {
            return department;
        }

        public void setDepartment(String department) {
            this.department = department;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getReason() {
            return reason;
        }

        public void setReason(String reason) {
            this.reason = reason;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public User getUser() {
            return user;
        }

        public void setUser(User user) {
            this.user = user;
        }

        public Profile getProfile() {
            return profile;
        }

        public void setProfile(Profile profile) {
            this.profile = profile;
        }

        public class User {

            @SerializedName("id")
            @Expose
            private String id;
            @SerializedName("firstname")
            @Expose
            private String firstname;
            @SerializedName("lastname")
            @Expose
            private String lastname;
            @SerializedName("email")
            @Expose
            private String email;
            @SerializedName("profile_pic")
            @Expose
            private String profilePic;
            @SerializedName("unique_code")
            @Expose
            private String uniqueCode;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getFirstname() {
                return firstname;
            }

            public void setFirstname(String firstname) {
                this.firstname = firstname;
            }

            public String getLastname() {
                return lastname;
            }

            public void setLastname(String lastname) {
                this.lastname = lastname;
            }

            public String getEmail() {
                return email;
            }

            public void setEmail(String email) {
                this.email = email;
            }

            public String getProfilePic() {
                return profilePic;
            }

            public void setProfilePic(String profilePic) {
                this.profilePic = profilePic;
            }

            public String getUniqueCode() {
                return uniqueCode;
            }

            public void setUniqueCode(String uniqueCode) {
                this.uniqueCode = uniqueCode;
            }
        }

        public class Profile {

            @SerializedName("id")
            @Expose
            private String id;
            @SerializedName("name")
            @Expose
            private String name;
            @SerializedName("logo")
            @Expose
            private String logo;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getLogo() {
                return logo;
            }

            public void setLogo(String logo) {
                this.logo = logo;
            }
        }

    }

    public static class BlockUserList {

        @SerializedName("user_id")
        @Expose
        private String user_id;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("unique_code")
        @Expose
        private String unique_code;
        @SerializedName("email")
        @Expose
        private String email;
        @SerializedName("profile_pic")
        @Expose
        private String profile_pic;

        public BlockUserList(String user_id, String name, String unique_code, String email, String profile_pic) {
            this.user_id = user_id;
            this.name = name;
            this.unique_code = unique_code;
            this.email = email;
            this.profile_pic = profile_pic;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getUnique_code() {
            return unique_code;
        }

        public void setUnique_code(String unique_code) {
            this.unique_code = unique_code;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getProfile_pic() {
            return profile_pic;
        }

        public void setProfile_pic(String profile_pic) {
            this.profile_pic = profile_pic;
        }
    }


    public static class RoleList {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("description")
        @Expose
        private String description;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public RoleList(String id, String name, String description) {
            this.id = id;
            this.name = name;
            this.description = description;
        }
    }

    public static class TransferProfileList {
        String id;
        String name;
        String email;
        String profile_pic;

        public TransferProfileList(String id, String name, String email, String profile_pic) {
            this.id = id;
            this.name = name;
            this.email = email;
            this.profile_pic = profile_pic;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getProfile_pic() {
            return profile_pic;
        }

        public void setProfile_pic(String profile_pic) {
            this.profile_pic = profile_pic;
        }
    }
}
