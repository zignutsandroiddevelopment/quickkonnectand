package com.quickkonnect;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.activities.UpdateDashboardBasicProfileActivity;
import com.activities.UpdateExperienceActivity;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.ceylonlabs.imageviewpopup.ImagePopup;
import com.facebook.login.LoginManager;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import com.utilities.Constants;
import com.utilities.QKPreferences;
import com.utilities.Utilities;
import com.volleyapirequest.VolleyApiRequest;
import com.volleyapirequest.VolleyCallBack;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class UpdateProfileBasicInfoActivity extends AppCompatActivity implements View.OnClickListener {

    ImageView img_basic_profile_profile_pic;
    EditText edt_basic_profile_firstname, edt_basic_profile_lastname, edt_basic_profile_story;

    private static SharedPreferences mSharedPreferences;
    String userid;


    MarshMallowPermission marshMallowPermission;
    private static final int PICK_FROM_CAMERA = 1;
    private static final int PICK_FROM_GALLERY = 2;
    private static final int PIC_CROP = 3;
    File mediaFile = null;
    File croppedFile = null;
    String strBase64 = "";
    static String mediaName = null;
    static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 5;
    ConstraintLayout view;
    Context context;

    String profile_pic, firstname, lastname, qkstory;
    BottomSheetDialog bottomSheetDialog;

    ImagePopup imagePopup;
    private QKPreferences qkPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Utilities.changeStatusbar(UpdateProfileBasicInfoActivity.this, getWindow());
        setContentView(R.layout.activity_update_profile_basic_info);
        Toolbar toolbar = findViewById(R.id.toolbar);
        //toolbar.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        toolbar.setBackgroundResource(R.drawable.toolbar_gradient);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        context = UpdateProfileBasicInfoActivity.this;

        mSharedPreferences = getSharedPreferences(SharedPreference.PREF_NAME, 0);
        //userid = mSharedPreferences.getString(SharedPreference.CURRENT_USER_ID, "");
        qkPreferences = new QKPreferences(this);
        userid =qkPreferences.getLoggedUser().getData().getId()+"";
        marshMallowPermission = new MarshMallowPermission(this);

        img_basic_profile_profile_pic = findViewById(R.id.img_basic_profile_profile_pic);
        edt_basic_profile_firstname = findViewById(R.id.edt_basic_profile_firstname);
        edt_basic_profile_lastname = findViewById(R.id.edt_basic_profile_lastname);
        edt_basic_profile_story = findViewById(R.id.edt_basic_profile_story);
        view = findViewById(R.id.constraint);

        img_basic_profile_profile_pic.setOnClickListener(this);

        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close_white_24dp);
        Bundle extras = getIntent().getExtras();

        if (extras != null) {
            profile_pic = extras.getString("profile_pic");
            firstname = extras.getString("firstname");
            lastname = extras.getString("lastname");
            qkstory = extras.getString("qkstory");

            if (profile_pic != null && !profile_pic.isEmpty() && !profile_pic.equals("null")) {
                Picasso.with(UpdateProfileBasicInfoActivity.this)
                        .load(profile_pic)
                       // .networkPolicy(NetworkPolicy.OFFLINE)
                        .error(R.drawable.ic_default_profile_photo)
                        .transform(new CircleTransform())
                        .placeholder(R.drawable.ic_default_profile_photo)
                        .fit()
                        .into(img_basic_profile_profile_pic);

                initImagepopup(profile_pic);
            }
            //edt_basic_profile_birthdate.setText(birthdate);

            if (firstname != null)
                edt_basic_profile_firstname.setText(firstname);
            if (lastname != null)
                edt_basic_profile_lastname.setText(lastname);
            if (qkstory != null)
                edt_basic_profile_story.setText(qkstory);

        }


    }


    public void initImagepopup(String userUrl) {
        imagePopup = new ImagePopup(UpdateProfileBasicInfoActivity.this);
        imagePopup.setWindowHeight(800); // Optional
        imagePopup.setWindowWidth(800); // Optional
        imagePopup.setBackgroundColor(Color.BLACK);  // Optional
        imagePopup.setFullScreen(true); // Optional
        imagePopup.setHideCloseIcon(false);  // Optional
        imagePopup.setImageOnClickClose(true);  // Optional
        imagePopup.initiatePopupWithPicasso(userUrl);
    }

    public void removePhoto() {
        croppedFile = null;
        img_basic_profile_profile_pic.setImageResource(R.drawable.ic_default_profile_photo);
    }

    public void getPhotoFromCamera() {

        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion >= Build.VERSION_CODES.M) {
            // Do something for lollipop and above versions
            checkAndRequestPermissions();
        } else {


            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            File mediaStorageDir = new File(
                    Environment.getExternalStorageDirectory()
                            + File.separator
                            + getString(R.string.directory_name_temp_staff)
                            + File.separator
                            + getString(R.string.directory_name_temp)
            );

            if (!mediaStorageDir.exists()) {
                mediaStorageDir.mkdirs();
            }

            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                    Locale.getDefault()).format(new Date());
            try {
                mediaFile = File.createTempFile(
                        "TEMP_FULL_IMG_" + timeStamp,  /* prefix */
                        ".jpg",         /* suffix */
                        mediaStorageDir      /* directory */
                );
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(mediaFile));
                startActivityForResult(takePictureIntent, PICK_FROM_CAMERA);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    public void getPhotoFromGallery() {
        if (!marshMallowPermission.checkPermissionForExternalStorage()) {
            marshMallowPermission.requestPermissionForExternalStorage();
        } else {
            try {
                Intent pickImageIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(pickImageIntent, PICK_FROM_GALLERY);
            } catch (ActivityNotFoundException exp) {
                Toast.makeText(getBaseContext(), "No File (Manager / Explorer)etc Found In Your Device", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_FROM_CAMERA) {
            if (resultCode == RESULT_OK) {
                performCrop(Uri.fromFile(mediaFile));
            } else {
                Log.i("Camera", "result cancel. Hence, deleting file: " + mediaFile.getPath());
                Log.i("File deleted ", mediaFile.delete() + "");
            }
        }

        if (requestCode == PICK_FROM_GALLERY) {
            if (resultCode == RESULT_OK) {
                performCrop(data.getData());
            } else {
                Log.i("Gallery", "result cancel");
            }
        }

        if (requestCode == PIC_CROP) {
            if (resultCode == RESULT_OK) {

                //img_basic_profile_profile_pic.setImageBitmap(BitmapFactory.decodeFile(croppedFile.getAbsolutePath()));

                Picasso.with(UpdateProfileBasicInfoActivity.this)
                        .load("file://" + croppedFile.getAbsolutePath())
                        .error(R.drawable.ic_default_profile_photo)
                        .transform(new CircleTransform())
                        .placeholder(R.drawable.ic_default_profile_photo)
                        .fit()
                        .into(img_basic_profile_profile_pic);

                initImagepopup("file://" + croppedFile.getAbsolutePath());


                if (mediaFile != null) {
                    Log.i("media", "result cancel. Hence, deleting file: " + mediaFile.getPath());
                    Log.i("File deleted ", mediaFile.delete() + "");
                }
            } else {
                if (croppedFile != null) {
                    Log.i("Camera", "result cancel. Hence, deleting file: " + croppedFile.getPath());
                    Log.i("File deleted ", croppedFile.delete() + "");
                }
                if (mediaFile != null) {
                    Log.i("Camera", "result cancel. Hence, deleting file: " + mediaFile.getPath());
                    Log.i("File deleted ", mediaFile.delete() + "");
                }
            }
        }
    }

    private void performCrop(Uri picUri) {
        try {
            Intent cropIntent = new Intent("com.android.camera.action.CROP");
            cropIntent.setDataAndType(picUri, "image/*");
            cropIntent.putExtra("crop", "true");
            cropIntent.putExtra("aspectX", 1);
            cropIntent.putExtra("aspectY", 1);
            cropIntent.putExtra("outputX", 128);
            cropIntent.putExtra("outputY", 128);
            cropIntent.putExtra("return-data", true);

            File mediaStorageDir = new File(
                    Environment.getExternalStorageDirectory()
                            + File.separator
                            + getString(R.string.directory_name_temp_staff)
                            + File.separator
                            + getString(R.string.directory_name_temp)
            );

            if (!mediaStorageDir.exists()) {
                mediaStorageDir.mkdirs();
            }

            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                    Locale.getDefault()).format(new Date());
            try {
                mediaName = "IMG_" + timeStamp + ".jpg";
                croppedFile = File.createTempFile(
                        "TEMP_CROP_IMG_" + timeStamp,
                        ".jpg",
                        mediaStorageDir
                );
                cropIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(croppedFile));
                startActivityForResult(cropIntent, PIC_CROP);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        // respond to users whose devices do not support the crop action
        catch (ActivityNotFoundException anfe) {
            // display an error message
            String errorMessage = "Whoops - your device doesn't support the crop action!";
            Toast toast = Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    private boolean checkAndRequestPermissions() {
        int permissionSendMessage = ContextCompat.checkSelfPermission(this,
                Manifest.permission.CAMERA);
        int locationPermission = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        List<String> listPermissionsNeeded = new ArrayList<>();
        if (locationPermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (permissionSendMessage != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CAMERA);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), REQUEST_ID_MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        // Log.d(TAG, "Permission callback called-------");
        switch (requestCode) {
            case REQUEST_ID_MULTIPLE_PERMISSIONS: {

                Map<String, Integer> perms = new HashMap<>();
                // Initialize the map with both permissions
                perms.put(Manifest.permission.CAMERA, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.WRITE_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                // Fill with actual results from user
                if (grantResults.length > 0) {
                    for (int i = 0; i < permissions.length; i++)
                        perms.put(permissions[i], grantResults[i]);
                    // Check for both permissions
                    if (perms.get(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
                            && perms.get(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                        // Log.d(TAG, "sms & location services permission granted");
                        // process the normal flow
                        //else any one or both the permissions are not granted
                        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        File mediaStorageDir = new File(
                                Environment.getExternalStorageDirectory()
                                        + File.separator
                                        + getString(R.string.directory_name_temp_staff)
                                        + File.separator
                                        + getString(R.string.directory_name_temp)
                        );

                        if (!mediaStorageDir.exists()) {
                            mediaStorageDir.mkdirs();
                        }

                        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                                Locale.getDefault()).format(new Date());
                        try {
                            mediaFile = File.createTempFile(
                                    "TEMP_FULL_IMG_" + timeStamp,  /* prefix */
                                    ".jpg",         /* suffix */
                                    mediaStorageDir      /* directory */
                            );
                            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(mediaFile));
                            startActivityForResult(takePictureIntent, PICK_FROM_CAMERA);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }


                    } else {

                    }
                }
            }
        }

    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.img_basic_profile_profile_pic:
                bottomSheetDialog = new BottomSheetDialog(UpdateProfileBasicInfoActivity.this);

                View view1 = getLayoutInflater().inflate(R.layout.custom_bottom_sheet_profile_pic, null);

                bottomSheetDialog.setContentView(view1);

                Button btnCancel = view1.findViewById(R.id.buttonSheet1);
                TextView TextViewSheet1 = view1.findViewById(R.id.TextViewSheet1);
                TextView TextViewSheet2 = view1.findViewById(R.id.TextViewSheet2);
                TextView TextViewSheet3 = view1.findViewById(R.id.TextViewSheet3);
                TextView TextViewSheet4 = view1.findViewById(R.id.TextViewSheet4);


                btnCancel.setOnClickListener(this);
                TextViewSheet1.setOnClickListener(this);
                TextViewSheet2.setOnClickListener(this);
                TextViewSheet3.setOnClickListener(this);
                TextViewSheet4.setOnClickListener(this);

                bottomSheetDialog.show();
                break;

            case R.id.buttonSheet1:
                bottomSheetDialog.hide();
                break;

            case R.id.TextViewSheet1:
                //Toast.makeText(getApplicationContext(),"View Photo",Toast.LENGTH_LONG).show();
                if (profile_pic != null && !profile_pic.isEmpty() && !profile_pic.equals("null")) {
                    imagePopup.viewPopup();
                    bottomSheetDialog.hide();
                } else {
                    Toast.makeText(UpdateProfileBasicInfoActivity.this, "Please choose picture first", Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.TextViewSheet2:
                //Toast.makeText(getApplicationContext(),"Upload Photo",Toast.LENGTH_LONG).show();
                getPhotoFromGallery();
                bottomSheetDialog.hide();
                break;

            case R.id.TextViewSheet3:
                //Toast.makeText(getApplicationContext(),"Take Photo",Toast.LENGTH_LONG).show();
                getPhotoFromCamera();
                bottomSheetDialog.hide();
                break;

            case R.id.TextViewSheet4:
                //Toast.makeText(getApplicationContext(),"Remove Photo",Toast.LENGTH_LONG).show();
                removePhoto();
                bottomSheetDialog.hide();
                break;
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.custom_save_update, menu);
        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {

            case android.R.id.home:
                setResult(RESULT_CANCELED);
                UpdateProfileBasicInfoActivity.this.finish();
                return true;

            case R.id.ic_save_update:

                if (edt_basic_profile_firstname.getText().toString().trim() == "" || edt_basic_profile_firstname.getText().toString().trim().equals("") || edt_basic_profile_firstname.getText().toString().trim().equals("null")) {
                    Utilities.showToast(context, "Firstname is required");
                } else if (edt_basic_profile_lastname.getText().toString().trim() == "" || edt_basic_profile_lastname.getText().toString().trim().equals("") || edt_basic_profile_lastname.getText().toString().trim().equals("null")) {
                    Utilities.showToast(context, "Lastname is required");
                } /*else if (edt_basic_profile_story.getText().toString().trim() == "" || edt_basic_profile_story.getText().toString().trim().equals("") || edt_basic_profile_story.getText().toString().trim().equals("null")) {
                    StaticVariable.functionShowMessage(view, "QK Story is required");
                }*/ else {

                    if (croppedFile == null) {

                        strBase64 = "";
                    } else {

                        Bitmap selectedImage = BitmapFactory.decodeFile(String.valueOf(croppedFile));
                        ByteArrayOutputStream stream = new ByteArrayOutputStream();
                        selectedImage.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                        byte[] byteArray = stream.toByteArray();
                        strBase64 = Base64.encodeToString(byteArray, 0);
                        Log.d("ss", strBase64);
                    }
                    if (!Utilities.isNetworkConnected(this))
                        Toast.makeText(this, Constants.NO_INTERNET_CONNECTION,Toast.LENGTH_LONG).show();
                    else
                    functionUpdateProfile(view, userid, edt_basic_profile_firstname.getText().toString().trim(), edt_basic_profile_lastname.getText().toString().trim(), edt_basic_profile_story.getText().toString().trim(), strBase64);

                }

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }


    // Save/Update Basic Profile
    public void functionUpdateProfile(final View view, String user_id, String firstname, String lastname, String qk_story, String profile_pic) {

        JSONObject obj = new JSONObject();
        try {
            obj.put("user_id", user_id);
            obj.put("firstname", firstname);
            obj.put("lastname", lastname);
            obj.put("qk_story", qk_story);
            obj.put("profile_picture", profile_pic);


        } catch (JSONException e) {
            e.printStackTrace();
        }


        new VolleyApiRequest(context, true, VolleyApiRequest.REQUEST_BASEURL + "api/change_profile_pic",
                new VolleyCallBack() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {
                    String status = response.getString("status");
                    String message = response.getString("msg");


                    if (status.equals("200")) {

                        Utilities.showToast(context, message);

                        Thread timerThread = new Thread() {
                            public void run() {
                                try {
                                    sleep(1000);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                } finally {
                                   /* Intent intent = new Intent(UpdateProfileBasicInfoActivity.this,UserProfileActivity.class);
                                    //intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(intent);
                                    finish();*/

                                    setResult(RESULT_OK);
                                    UpdateProfileBasicInfoActivity.this.finish();

                                }
                            }
                        };
                        timerThread.start();

                    } else if (status.equals("400")) {

                        Log.d("message", message + "");
                        if (message.equals("No user found")) {
                            SharedPreferences.Editor e = mSharedPreferences.edit();
                            e.clear();
                            e.remove(SharedPreference.CURRENT_USER_ID);
                            e.remove(SharedPreference.CURRENT_USER_NAME);
                            e.commit();
                            e.apply();
                            LoginManager.getInstance().logOut();

                            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                            finish();
                        } else {
                            Utilities.showToast(context,  message);
                        }
                        //Toast.makeText(LoginActivity.this, message, Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
//

                    }

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyApiRequest.showVolleyError(context, error);
                    }
                }).enqueRequest(obj, Request.Method.POST);
    }


//        final ProgressDialog mDialog = ProgressDialog.show(this, null, null, false, true);
//        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//        mDialog.setContentView(R.layout.progress_bar);
//        Log.e("Object -> ", obj.toString());
//
//
//        JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, StaticVariable.url + "api/change_profile_pic", obj, new Response.Listener<JSONObject>() {
//
//            @Override
//            public void onResponse(JSONObject response) {
//                // TODO Auto-generated method stub
//                mDialog.dismiss();
//
//
//                Log.i("Success --> ", response.toString());
//
//                try {
//                    String status = response.getString("status");
//                    String message = response.getString("msg");
//
//
//                    if (status.equals("200")) {
//
//                        StaticVariable.functionShowMessage(view, message);
//
//                        Thread timerThread = new Thread() {
//                            public void run() {
//                                try {
//                                    sleep(1000);
//                                } catch (InterruptedException e) {
//                                    e.printStackTrace();
//                                } finally {
//                                   /* Intent intent = new Intent(UpdateProfileBasicInfoActivity.this,UserProfileActivity.class);
//                                    //intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
//                                    startActivity(intent);
//                                    finish();*/
//
//                                    setResult(RESULT_OK);
//                                    UpdateProfileBasicInfoActivity.this.finish();
//
//                                }
//                            }
//                        };
//                        timerThread.start();
//
//                    } else if (status.equals("400")) {
//
//                        Log.d("message", message + "");
//                        if (message.equals("No user found")) {
//                            SharedPreferences.Editor e = mSharedPreferences.edit();
//                            e.clear();
//                            e.remove(SharedPreference.CURRENT_USER_ID);
//                            e.remove(SharedPreference.CURRENT_USER_NAME);
//                            e.commit();
//                            e.apply();
//                            LoginManager.getInstance().logOut();
//
//                            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
//                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                            startActivity(intent);
//                            finish();
//                        } else {
//                            StaticVariable.functionShowMessage(view, message);
//                        }
//                        //Toast.makeText(LoginActivity.this, message, Toast.LENGTH_SHORT).show();
//                    }
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//
//
//            }
//        }, new Response.ErrorListener() {
//
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                // TODO Auto-generated method stub
//                mDialog.dismiss();
//                Toast.makeText(getApplicationContext(), error.getMessage() + "", Toast.LENGTH_LONG).show();
//
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                Map<String, String> params = new HashMap<String, String>();
//                params.put("QK_ACCESS_KEY", getResources().getString(R.string.QK_ACCESS_KEY));
//                return params;
//            }
//        };
//        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(
//                30000,
//                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
//                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
//        Volley.newRequestQueue(UpdateProfileBasicInfoActivity.this).add(jsObjRequest);
//
//    }

}
