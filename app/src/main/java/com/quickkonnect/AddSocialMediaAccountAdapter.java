package com.quickkonnect;

import android.app.Activity;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.login.widget.LoginButton;
import com.models.ModelSocialMedia;
import com.utilities.Constants;
import com.utilities.QKPreferences;
import com.utilities.Utilities;

import java.util.ArrayList;
import java.util.List;

import static com.linkedin.Config.LINKEDIN_PACKAGE_NAME;

/**
 * Created by ZTLAB03 on 29-09-2017.
 */

public class AddSocialMediaAccountAdapter extends RecyclerView.Adapter<AddSocialMediaAccountAdapter.ViewHolder> implements View.OnClickListener {

    ArrayList<ModelSocialMedia> socialMediaAccounts;
    static Activity mActivity;
    private static SharedPreferences mSharedPreferences;
    String userId;

    QKPreferences qkPreferences;

    public AddSocialMediaAccountAdapter(Activity activity, ArrayList<ModelSocialMedia> socialMediaAccountList) {
        this.socialMediaAccounts = socialMediaAccountList;
        mActivity = activity;
        mSharedPreferences = mActivity.getSharedPreferences(SharedPreference.PREF_NAME, 0);
        userId = mSharedPreferences.getString(SharedPreference.CURRENT_USER_ID, "");
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_social_mapping_apdater, null);

        qkPreferences = new QKPreferences(mActivity);

        ViewHolder viewHolder = new ViewHolder(itemLayoutView);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        final ModelSocialMedia media = socialMediaAccounts.get(position);

        if (Utilities.isEmpty(media.getName()))
            holder.tv_social_media_name.setText(media.getName());
        else
            holder.tv_social_media_name.setText("");

        holder.img_social_media.setImageResource(media.getIc_social_icon());

        if (media.getStatus().equals("Active"))
            holder.switch1.setChecked(true);
        else
            holder.switch1.setChecked(false);

        Typeface lato_semibold = Typeface.createFromAsset(mActivity.getAssets(), "fonts/latosemibold.ttf");

        holder.tv_social_media_name.setTypeface(lato_semibold);

        holder.tv_social_media_name.setTag(media);
        holder.img_social_media.setTag(media);
        holder.tv_social_username.setTag(media);
        holder.switch1.setTag(media);

        if (Utilities.isEmpty(media.getUserName()) && media.getUserName() != null && media.getStatus().equals("Active")) {
            holder.tv_social_username.setText(media.getUserName());
        }

        if (position == 0) {
            holder.switch1.setChecked(true);
            holder.switch1.setFocusable(false);
            holder.llRootLayout.setClickable(false);
            holder.switch1.setVisibility(View.GONE);
            holder.active.setVisibility(View.VISIBLE);

            if (Utilities.isEmpty(media.getUserName()) && !media.getUserName().equalsIgnoreCase("")) {
                holder.tv_social_username.setText(media.getUserName());
            } else {
                holder.tv_social_username.setVisibility(View.GONE);
            }

        } else {

            holder.switch1.setVisibility(View.VISIBLE);
            holder.active.setVisibility(View.GONE);

            holder.tv_social_media_name.setOnClickListener(this);
            holder.img_social_media.setOnClickListener(this);
            holder.llRootLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (media.getId() == Integer.parseInt(Constants.SOCIAL_FACEBOOK_ID)) {
                        if (media.getStatus().equals("Active")) {
                            if (!Utilities.isNetworkConnected(mActivity))
                                Toast.makeText(mActivity, Constants.NO_INTERNET_CONNECTION, Toast.LENGTH_LONG).show();
                            else
                                ((AddSocialMediaAccountActivity) mActivity).onClickActiveAccount(userId,
                                        Integer.parseInt(Constants.SOCIAL_FACEBOOK_ID), "0");

                        } else {
                            if (!Utilities.isNetworkConnected(mActivity))
                                Toast.makeText(mActivity, Constants.NO_INTERNET_CONNECTION, Toast.LENGTH_LONG).show();
                            else
                                ((AddSocialMediaAccountActivity) mActivity).facebookMapping();

                        }
                    } else if (media.getId() == Integer.parseInt(Constants.SOCIAL_LINKEDIN_ID)) {
                        if (media.getStatus().equals("Active")) {

                            if (!Utilities.isNetworkConnected(mActivity))
                                Toast.makeText(mActivity, Constants.NO_INTERNET_CONNECTION, Toast.LENGTH_LONG).show();
                            else
                                ((AddSocialMediaAccountActivity) mActivity).onClickActiveAccount(userId, Integer.parseInt(Constants.SOCIAL_LINKEDIN_ID), "0");

                        } else {
                            if (!Utilities.isNetworkConnected(mActivity))
                                Toast.makeText(mActivity, Constants.NO_INTERNET_CONNECTION, Toast.LENGTH_LONG).show();
                            else {
                                //((AddSocialMediaAccountActivity) mActivity).linkedInLoginWithoutApp();
                                boolean isAppInstalled = Utilities.appInstalledOrNot(mActivity, LINKEDIN_PACKAGE_NAME);
                                if (isAppInstalled) {
                                    ((AddSocialMediaAccountActivity) mActivity).LinkedInMapping();

                                } else {
                                    ((AddSocialMediaAccountActivity) mActivity).linkedInLoginWithoutApp();
                                }
                            }

                        }
                    } else if (media.getId() == Integer.parseInt(Constants.SOCIAL_TWITTER_ID)) {
                        if (media.getStatus().equals("Active")) {
                            if (!Utilities.isNetworkConnected(mActivity))
                                Toast.makeText(mActivity, Constants.NO_INTERNET_CONNECTION, Toast.LENGTH_LONG).show();
                            else
                                ((AddSocialMediaAccountActivity) mActivity).onClickActiveAccount(userId, Integer.parseInt(Constants.SOCIAL_TWITTER_ID), "0");

                        } else {
                            if (!Utilities.isNetworkConnected(mActivity))
                                Toast.makeText(mActivity, Constants.NO_INTERNET_CONNECTION, Toast.LENGTH_LONG).show();
                            else
                                ((AddSocialMediaAccountActivity) mActivity).twitterMapping();

                        }
                    } else if (media.getId() == Integer.parseInt(Constants.SOCIAL_INSTAGRAM_ID)) {
                        if (media.getStatus().equals("Active")) {
                            if (!Utilities.isNetworkConnected(mActivity))
                                Toast.makeText(mActivity, Constants.NO_INTERNET_CONNECTION, Toast.LENGTH_LONG).show();
                            else
                                ((AddSocialMediaAccountActivity) mActivity).onClickActiveAccount(userId, Integer.parseInt(Constants.SOCIAL_INSTAGRAM_ID), "0");
                        } else {
                            if (!Utilities.isNetworkConnected(mActivity))
                                Toast.makeText(mActivity, Constants.NO_INTERNET_CONNECTION, Toast.LENGTH_LONG).show();
                            else
                                ((AddSocialMediaAccountActivity) mActivity).InstagramMapping();
                        }
                    } else if (media.getId() == Integer.parseInt(Constants.SOCIAL_GOOGLE_PLUS)) {
                        if (media.getStatus().equals("Active")) {
                            if (!Utilities.isNetworkConnected(mActivity))
                                Toast.makeText(mActivity, Constants.NO_INTERNET_CONNECTION, Toast.LENGTH_LONG).show();
                            else
                                ((AddSocialMediaAccountActivity) mActivity).onClickActiveAccount(userId, Integer.parseInt(Constants.SOCIAL_GOOGLE_PLUS), "0");
                        } else {
                            if (!Utilities.isNetworkConnected(mActivity))
                                Toast.makeText(mActivity, Constants.NO_INTERNET_CONNECTION, Toast.LENGTH_LONG).show();
                            else
                                ((AddSocialMediaAccountActivity) mActivity).GoogleIntegration();
                        }
                    } else if (media.getId() == Integer.parseInt(Constants.SOCIAL_SNAPCHAT)) {
                        if (media.getStatus().equals("Active")) {
                            if (!Utilities.isNetworkConnected(mActivity))
                                Toast.makeText(mActivity, Constants.NO_INTERNET_CONNECTION, Toast.LENGTH_LONG).show();
                            else
                                ((AddSocialMediaAccountActivity) mActivity).onClickActiveAccount(userId, Integer.parseInt(Constants.SOCIAL_SNAPCHAT), "0");
                        } else {
                            if (!Utilities.isNetworkConnected(mActivity))
                                Toast.makeText(mActivity, Constants.NO_INTERNET_CONNECTION, Toast.LENGTH_LONG).show();
                            else
                                ((AddSocialMediaAccountActivity) mActivity).SnapChatintegrating();
                        }
                    } else {
                        Toast.makeText(mActivity, "This feature implement soon", Toast.LENGTH_SHORT).show();
                    }

                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return socialMediaAccounts.size();
    }

    @Override
    public void onClick(View view) {
        ModelSocialMedia social = (ModelSocialMedia) view.getTag();
        // Toast.makeText(mActivity,"other media"+social+"",Toast.LENGTH_LONG).show();

        switch (view.getId()) {
            case R.id.tv_social_media_name:
            case R.id.img_social_media:
                if (social.getName().equals("Facebook")) {
                    ((AddSocialMediaAccountActivity) mActivity).facebookMapping();
                } /*else {
                    Toast.makeText(mActivity, "other media", Toast.LENGTH_LONG).show();
                }*/
                break;
        }
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_social_media_name, tv_social_username, active;
        public ImageView img_social_media;
        public LoginButton loginButton;
        public Switch switch1;
        public LinearLayout llRootLayout;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            tv_social_media_name = itemLayoutView.findViewById(R.id.tv_social_media_name);
            tv_social_username = itemLayoutView.findViewById(R.id.tv_social_username);
            active = itemLayoutView.findViewById(R.id.tv_active_qk);
            img_social_media = itemLayoutView.findViewById(R.id.img_social_media);
            loginButton = itemLayoutView.findViewById(R.id.login_button);
            switch1 = itemLayoutView.findViewById(R.id.switch1);
            llRootLayout = itemLayoutView.findViewById(R.id.llRootLayout);

        }
    }

    public void setFilter(List<ModelSocialMedia> countryModels) {
        socialMediaAccounts = new ArrayList<>();
        socialMediaAccounts.addAll(countryModels);
        notifyDataSetChanged();
    }


}
