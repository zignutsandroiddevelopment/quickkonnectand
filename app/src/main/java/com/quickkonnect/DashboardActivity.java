package com.quickkonnect;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.FragmentTransaction;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.OpenFire.XmppConnectionUtil;
import com.OpenFire.interfaces.ChatNewMessagelistener;
import com.activities.MatchProfileActivity;
import com.adapter.AdpEmployer;
import com.adapter.AdpGlobalSearch;
import com.adapter.AdpGlobalSearchResult;
import com.adapter.DownloadContactListAdapter;
import com.adapter.ViewPagerAdapter;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.facebook.login.LoginManager;
import com.fragments.CustomBottomSheetDialogFragment;
import com.fragments.FragContacts;
import com.fragments.FragDashboard;
import com.fragments.FragNotifications;
import com.interfaces.EmployerClick;
import com.interfaces.OnSuccessError;
import com.interfaces.SimpleGestureListener;
import com.interfaces.onGlobalSearchClick;
import com.models.ModelNotifications;
import com.models.globalsearch.Continent;
import com.models.globalsearch.ModelGlobalSearch;
import com.models.profiles.ProfilesList;
import com.realmtable.Contacts;
import com.realmtable.MyProfiles;
import com.realmtable.RealmController;
import com.retrofitservice.ApiUtils;
import com.retrofitservice.SOService;
import com.services.LoadLoggedUsers;
import com.utilities.Constants;
import com.utilities.NonSwipeableViewPager;
import com.utilities.QKPreferences;
import com.utilities.Utilities;
import com.volleyapirequest.AsynTask;
import com.volleyapirequest.VolleyApiRequest;
import com.volleyapirequest.VolleyCallBack;

import net.quikkly.android.QuikklyBuilder;

import org.jivesoftware.smack.Chat;
import org.jivesoftware.smack.packet.Message;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import io.realm.RealmResults;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static net.quikkly.android.ui.ScanActivity.EXTRA_SHOW_OVERLAY;

public class DashboardActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemClickListener, SimpleGestureListener, ChatNewMessagelistener {

    private Context context;
    private QKPreferences qkPreferences;
    private boolean isOncreate = false;
    private SimpleGestureFilter detector;
    private FrameLayout frame_fragment;
    LocationManager mLocationManager;
    RecyclerView recycleResults;
    TextView tv_nodatafound_result;
    private static BottomSheetBehavior bottomSheetBehavior;
    private NonSwipeableViewPager viewPager;
    String userid, UniqueCode = "";
    public Menu menu;

    public CoordinatorLayout coordinator;
    XmppConnectionUtil xmppConnectionUtil;

    boolean isOpen = false;
    boolean isChatOpen = false;
    ImageView image_scan;
    BottomSheetBehavior bottomSheetMoreOptions;
    ArrayList<PojoClasses.ContactInfo> contactInfoArrayList;
    DownloadContactListAdapter contactListAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    RecyclerView recyclerView_contactinfo;
    BottomSheetBehavior bottomSheetAccept;

    FrameLayout frame_home, frame_notification, frame_contcat, frame_nav;
    ImageView img_home, img_notification, img_contcat, img_nav;

    SOService mService;
    int MY_PERMISSIONS_REQUEST_LOCATION = 1100;

    // for employer dialog
    AdpEmployer adpEmployer;
    List<ProfilesList> listEmployerprofile;

    private static final int MY_CAMERA_REQUEST_CODE = 100;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.app_bar_dashboard);

        context = DashboardActivity.this;

        if (Build.VERSION.SDK_INT >= 19 && Build.VERSION.SDK_INT < 21) {
            setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, true);
        }
        if (Build.VERSION.SDK_INT >= 19) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }
        if (Build.VERSION.SDK_INT >= 21) {
            setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, false);
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }

        mService = ApiUtils.getSOService();
        detector = new SimpleGestureFilter(DashboardActivity.this, this);
        qkPreferences = new QKPreferences(DashboardActivity.this);

        userid = "" + qkPreferences.getValuesInt(QKPreferences.USER_ID);
        UniqueCode = "" + qkPreferences.getValues(QKPreferences.USER_UNIQUECODE);

        new QuikklyBuilder()
                .setApiKey(getResources().getString(R.string.quikkly_key))
                .loadDefaultBlueprintFromLibraryAssets(this)
                .build()
                .setAsDefault();

        bottomSheetAccept = BottomSheetBehavior.from(findViewById(R.id.bottomSheetApprovecontact));

        xmppConnectionUtil = new XmppConnectionUtil(DashboardActivity.this);
        xmppConnectionUtil.setChatNewMessageistener(this);

        bottomSheetBehavior = BottomSheetBehavior.from(findViewById(R.id.bottomSheetLayout));
        bottomSheetMoreOptions = BottomSheetBehavior.from(findViewById(R.id.bottomSheetMoreoption));

        SendListeners();

        //Add View Pager
        viewPager = findViewById(R.id.viewpager_main);
        frame_fragment = findViewById(R.id.frame_fragment);
        SetUpViewPager();

        image_scan = findViewById(R.id.profile_edit_fab);
        image_scan.setOnClickListener(this);

        frame_home = findViewById(R.id.frame_home);
        frame_contcat = findViewById(R.id.frame_contact);
        frame_notification = findViewById(R.id.frame_notification);
        frame_nav = findViewById(R.id.frame_nav);

        img_home = findViewById(R.id.img_home);
        img_notification = findViewById(R.id.img_notification);
        img_contcat = findViewById(R.id.img_contact);
        img_nav = findViewById(R.id.img_nav);

        img_home.setImageResource(R.drawable.home_new_vi);
        img_contcat.setImageResource(R.drawable.rank_new_invi);
        img_notification.setImageResource(R.drawable.noti_new_invi);
        img_nav.setImageResource(R.drawable.ic_menu);
        viewPager.setVisibility(View.VISIBLE);
        frame_fragment.setVisibility(View.GONE);

        frame_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                viewPager.setCurrentItem(0);
                img_home.setImageResource(R.drawable.home_new_vi);
                img_contcat.setImageResource(R.drawable.rank_new_invi);
                img_notification.setImageResource(R.drawable.noti_new_invi);
                img_nav.setImageResource(R.drawable.ic_menu);

                viewPager.setVisibility(View.VISIBLE);
                frame_fragment.setVisibility(View.GONE);

                Fragment fragment = adapter.getItem(0);
                if (fragment instanceof FragDashboard) {
                    FragDashboard fragDashboard = (FragDashboard) fragment;
                    fragDashboard.showTags(false);
                    fragDashboard.setContctPorfiles(false);
                }
                isChatOpen = false;
            }
        });
        frame_contcat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getHideReferView();
                viewPager.setCurrentItem(1);
                img_home.setImageResource(R.drawable.home_new_invi);
                img_contcat.setImageResource(R.drawable.rank_new_vi);
                img_notification.setImageResource(R.drawable.noti_new_invi);
                img_nav.setImageResource(R.drawable.ic_menu);

                viewPager.setVisibility(View.VISIBLE);
                frame_fragment.setVisibility(View.GONE);

                Fragment fragment = adapter.getItem(1);
                if (fragment instanceof FragContacts) {
                    FragContacts fragDashboard = (FragContacts) fragment;
                    fragDashboard.showContacts("name", true);
                    fragDashboard.showFollowers("name", true);
                    fragDashboard.showFollowing(true);
                }
                isChatOpen = false;

            }
        });
        frame_notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getHideReferView();
                viewPager.setCurrentItem(2);
                img_home.setImageResource(R.drawable.home_new_invi);
                img_contcat.setImageResource(R.drawable.rank_new_invi);
                img_notification.setImageResource(R.drawable.noti_new_vi);
                img_nav.setImageResource(R.drawable.ic_menu);

                viewPager.setVisibility(View.VISIBLE);
                frame_fragment.setVisibility(View.GONE);
                isChatOpen = false;
            }
        });
        frame_nav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getHideReferView();
                viewPager.setCurrentItem(3);
                img_home.setImageResource(R.drawable.home_new_invi);
                img_contcat.setImageResource(R.drawable.rank_new_invi);
                img_notification.setImageResource(R.drawable.noti_new_invi);
                img_nav.setImageResource(R.drawable.ic_new_menu_green);

                viewPager.setVisibility(View.VISIBLE);
                frame_fragment.setVisibility(View.GONE);
                isChatOpen = false;

                try {
                    Fragment fragment = adapter.getItem(3);
                    if (isOncreate && fragment instanceof Navigation_Activity) {
                        isOncreate = false;
                        Navigation_Activity navigation_activity = (Navigation_Activity) fragment;
                        navigation_activity.showFirst3Profiles();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        Intent msgIntent = new Intent(this, LoadLoggedUsers.class);
        msgIntent.putExtra(LoadLoggedUsers.PARAM_IN_MSG, "Hello, Service");
        startService(msgIntent);

        mLocationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        try {

            if (ActivityCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            } else {
                mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 0, mLocationListener);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            Fragment fragment = adapter.getItem(0);
            if (fragment instanceof FragDashboard) {
                FragDashboard fragDashboard = (FragDashboard) fragment;
                fragDashboard.setContctPorfiles(true);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        getDefaultNotifications(getIntent());

        getUserQRCode();

        checkiffromemailverification(getIntent());
    }

    public void checkiffromemailverification(Intent intent) {

        if (intent != null && intent.getExtras() != null) {

            final String path = getIntent().getExtras().getString("url");

            if (path != null && !path.isEmpty() && path.contains("email_token")) {

                String split_path[] = path.split("/");
                boolean isSameUser = false;
                if (split_path != null && split_path.length > 5) {
                    String user_id = path.split("/")[5];
                    if (user_id != null && !user_id.isEmpty())
                        if (user_id.equalsIgnoreCase(userid)) {
                            isSameUser = true;
                        }
                }

                if (!isSameUser) {

                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    builder.setTitle(getResources().getString(R.string.alert));
                    builder.setMessage(getResources().getString(R.string.emailverifyalreadylogin));
                    builder.setPositiveButton(getResources().getString(R.string.logout), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            try {

                                qkPreferences.clearQKPreference();

                                RealmController.with(DashboardActivity.this).clearChatConversation();
                                RealmController.with(DashboardActivity.this).clearChatuserList();
                                RealmController.with(DashboardActivity.this).clearAllTables();

                                try {
                                    LoginManager.getInstance().logOut();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                                Intent intent = new Intent(context, Introduction_Activity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                intent.putExtra("url", path);
                                startActivity(intent);

                                DashboardActivity.this.finish();

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });
                    builder.setNegativeButton(getResources().getString(R.string.cancel), null);
                    builder.show();

                } else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    builder.setTitle(getResources().getString(R.string.alert));
                    builder.setMessage(getResources().getString(R.string.youaresameuser));
                    builder.setNegativeButton(getResources().getString(R.string.cancel), null);
                    builder.show();
                }
            } else if (path != null && !path.isEmpty() && path.contains("d")) {

                String split_path[] = path.split("/");
                if (split_path != null && split_path.length > 5) {
                    String user_id = path.split("/")[5];
                    startActivity(
                            new Intent(context, MatchProfileActivity.class)
                                    .putExtra("unique_code", user_id)
                                    .putExtra(Constants.KEY, Constants.KEY_OTHER));
                }
            }
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        getDefaultNotifications(intent);
    }

    public void getDefaultNotifications(Intent intent) {
        if (intent != null && intent.getExtras() != null) {

            String key = intent.getExtras().getString(Constants.KEY);
            if (key != null && !key.isEmpty() && key.equalsIgnoreCase(Constants.KEY_GENERAL_NOTIFICATION)) {
                getHideReferView();
                viewPager.setCurrentItem(2);
                img_home.setImageResource(R.drawable.home_new_invi);
                img_contcat.setImageResource(R.drawable.rank_new_invi);
                img_notification.setImageResource(R.drawable.noti_new_vi);
                img_nav.setImageResource(R.drawable.ic_menu);

                viewPager.setVisibility(View.VISIBLE);
                frame_fragment.setVisibility(View.GONE);
                isChatOpen = false;
            }
        }
    }

    public void getHideReferView() {
        Fragment fragment = adapter.getItem(0);
        if (fragment instanceof FragDashboard) {
            FragDashboard fragDashboard = (FragDashboard) fragment;
            if (fragDashboard.mBottomSheetBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED)
                fragDashboard.hideReferView();
        }
    }


    public static void setWindowFlag(Activity activity, final int bits, boolean on) {
        Window win = activity.getWindow();
        WindowManager.LayoutParams winParams = win.getAttributes();
        if (on) {
            winParams.flags |= bits;
        } else {
            winParams.flags &= ~bits;
        }
        win.setAttributes(winParams);
    }


    public void showAcceptReject(final ModelNotifications notifications, final int pos) {

        final CharSequence[] items = {"Approve", "Reject"};
        AlertDialog.Builder builder = new AlertDialog.Builder(DashboardActivity.this);
        builder.setTitle("Make your selection");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                // Do something with the selection
                if (item == 0) {
                    // Approve
                    showDownloadContacts(notifications, pos);

                } else if (item == 1) {
                    //Reject
                    new AsynTask(context, new OnSuccessError() {
                        @Override
                        public void onSuccess(JSONObject object) {
                            Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.container);
                            if (fragment != null && fragment instanceof FragNotifications) {
                                ((FragNotifications) fragment).refreshNotifications(pos);
                            }
                        }

                        @Override
                        public void onCancel(String error_message) {
                            Utilities.showToast(context, error_message);
                        }
                    }).fnRejectContact(qkPreferences.getLoggedUserid(), "" + notifications.getSenderId(), notifications.getNotificationId());
                }
            }
        });
        builder.setPositiveButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        builder.show();
    }

    public void showDownloadContacts(final ModelNotifications notifications, final int pos) {

        if (bottomSheetAccept.getState() == BottomSheetBehavior.STATE_COLLAPSED) {
            bottomSheetAccept.setState(BottomSheetBehavior.STATE_EXPANDED);

            findViewById(R.id.imgApprove).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (DownloadContactListAdapter.tempArray != null && DownloadContactListAdapter.tempArray.size() > 0) {
                        if (bottomSheetAccept.getState() == BottomSheetBehavior.STATE_EXPANDED) {
                            bottomSheetAccept.setState(BottomSheetBehavior.STATE_COLLAPSED);
                        }
                        try {
                            JSONObject jsonObject = new JSONObject();
                            JSONArray array = new JSONArray();
                            for (int i = 0; i < DownloadContactListAdapter.tempArray.size(); i++) {
                                JSONObject internalObject = new JSONObject();
                                internalObject.put("contact_id", DownloadContactListAdapter.tempArray.get(i));
                                array.put(internalObject);
                            }
                            jsonObject.put("sender_id", notifications.getSenderId());
                            jsonObject.put("contact_approve", array);
                            jsonObject.put("notification_id", notifications.getNotificationId());

                            if (!Utilities.isNetworkConnected(context))
                                Utilities.showToast(context, getResources().getString(R.string.no_internat_connection));
                            else {
                                new AsynTask(context, new OnSuccessError() {
                                    @Override
                                    public void onSuccess(JSONObject jsonObject) {
                                        Fragment fragment = adapter.getItem(viewPager.getCurrentItem());
                                        if (fragment != null && fragment instanceof FragNotifications) {
                                            ((FragNotifications) fragment).refreshNotifications(pos);
                                        }
                                    }

                                    @Override
                                    public void onCancel(String error_message) {
                                        Utilities.showToast(context, error_message);
                                    }
                                }).fnApproveContact(jsonObject);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else
                        Utilities.showToast(context, getResources().getString(R.string.nocontactsselected));
                }
            });

            findViewById(R.id.imgCancel).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (bottomSheetAccept.getState() == BottomSheetBehavior.STATE_EXPANDED) {
                        bottomSheetAccept.setState(BottomSheetBehavior.STATE_COLLAPSED);
                    }
                }
            });

            try {
                if (contactInfoArrayList != null && contactInfoArrayList.size() > 0) {
                    contactInfoArrayList.clear();
                    contactListAdapter.notifyDataSetChanged();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (!Utilities.isNetworkConnected(this))
                Utilities.showToast(context, getResources().getString(R.string.no_internat_connection));
            else {
                new AsynTask(context, new OnSuccessError() {
                    @Override
                    public void onSuccess(JSONObject response) {
                        try {
                            String status = response.getString("status");
                            if (status.equals("200")) {
                                recyclerView_contactinfo.setVisibility(View.VISIBLE);
                                findViewById(R.id.tv_no_contact_avlbl).setVisibility(View.GONE);
                                JSONArray jsnArrContactInfo = response.getJSONArray("data");
                                contactInfoArrayList = new ArrayList<>();
                                if (jsnArrContactInfo != null && jsnArrContactInfo.length() > 0) {
                                    for (int i = 0; i < jsnArrContactInfo.length(); i++) {
                                        JSONObject obj = jsnArrContactInfo.getJSONObject(i);

                                        String email = obj.getString("email");
                                        String phone = obj.getString("email");

                                        //  if (Utilities.isEmpty(email) || Utilities.isEmpty(phone)) {

                                        Log.i("Success obj--> ", obj.toString());
                                        String is_approved = obj.optString("is_approved");
                                        if (is_approved != null) {
                                            if (is_approved.equals("0"))
                                                contactInfoArrayList.add(new PojoClasses.ContactInfo(obj.getInt("contact_id"), obj.getString("contact_type"), obj.getString("email"), obj.getString("phone")));
                                        } else
                                            contactInfoArrayList.add(new PojoClasses.ContactInfo(obj.getInt("contact_id"), obj.getString("contact_type"), obj.getString("email"), obj.getString("phone")));
                                        //  }
                                    }

                                    if (contactInfoArrayList != null && contactInfoArrayList.size() > 0) {
                                        contactListAdapter = new DownloadContactListAdapter(DashboardActivity.this, contactInfoArrayList);
                                        recyclerView_contactinfo.setAdapter(contactListAdapter);
                                    } else {
                                        Utilities.showToast(context, context.getResources().getString(R.string.donthaveemailphonecontact));
                                        bottomSheetAccept.setState(BottomSheetBehavior.STATE_COLLAPSED);
                                        deleteSignleNotification(pos, context, String.valueOf(notifications.getNotificationId()));
                                    }

                                } else {
                                    bottomSheetAccept.setState(BottomSheetBehavior.STATE_COLLAPSED);
                                    recyclerView_contactinfo.setVisibility(View.GONE);
                                    findViewById(R.id.tv_no_contact_avlbl).setVisibility(View.VISIBLE);
                                }
                            } else {
                                bottomSheetAccept.setState(BottomSheetBehavior.STATE_COLLAPSED);
                                recyclerView_contactinfo.setVisibility(View.GONE);
                                findViewById(R.id.tv_no_contact_avlbl).setVisibility(View.VISIBLE);
                            }
                        } catch (JSONException e) {
                            bottomSheetAccept.setState(BottomSheetBehavior.STATE_COLLAPSED);
                            recyclerView_contactinfo.setVisibility(View.GONE);
                            findViewById(R.id.tv_no_contact_avlbl).setVisibility(View.VISIBLE);
                        }
                    }

                    @Override
                    public void onCancel(String error_message) {
                        bottomSheetAccept.setState(BottomSheetBehavior.STATE_COLLAPSED);
                        Utilities.showToast(context, error_message);
                    }
                }).functionGetContactList(userid, String.valueOf(notifications.getSenderId()));
            }
        }
    }

    // Delete Single Notification
    public void deleteSignleNotification(final int pos, Context context, String notficationId) {
        new VolleyApiRequest(context, false, VolleyApiRequest.REQUEST_BASEURL + "api/delete_notification/" + notficationId,
                new VolleyCallBack() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Fragment fragment = adapter.getItem(viewPager.getCurrentItem());
                            if (fragment != null && fragment instanceof FragNotifications) {
                                ((FragNotifications) fragment).refreshNotifications(pos);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // VolleyApiRequest.showVolleyError(context, error);
                    }
                }).enqueRequest(null, Request.Method.GET);
    }

    private void SendListeners() {

        recyclerView_contactinfo = findViewById(R.id.recyclerView_approvecontacts);
        recyclerView_contactinfo.addItemDecoration(new SimpleDividerItemDecoration(this));
        recyclerView_contactinfo.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        recyclerView_contactinfo.setLayoutManager(mLayoutManager);

        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        android.app.Fragment fragment = new CustomBottomSheetDialogFragment();
        fragmentTransaction.replace(R.id.frame_fragment_btmshit, fragment, null);
        fragmentTransaction.commitAllowingStateLoss();
        findViewById(R.id.frame_fragment_btmshit).setVisibility(View.VISIBLE);

    }

    ViewPagerAdapter adapter;

    private void SetUpViewPager() {
        adapter = new ViewPagerAdapter(getSupportFragmentManager());

        FragDashboard frag_dashboard = new FragDashboard().getInstance();
        FragContacts frag_contacts = new FragContacts();
        FragNotifications frag_notification = new FragNotifications();
        Navigation_Activity frag_sidemenu = new Navigation_Activity();

        adapter.addFragment(frag_dashboard);
        adapter.addFragment(frag_contacts);
        adapter.addFragment(frag_notification);
        adapter.addFragment(frag_sidemenu);

        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(4);

    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent me) {
        this.detector.onTouchEvent(me);
        return super.dispatchTouchEvent(me);
    }

    public static Context mStaticcontext;

    @Override
    protected void onResume() {
        super.onResume();

        isOncreate = true;
        try {
            mStaticcontext = DashboardActivity.this;
            LocalBroadcastManager.getInstance(DashboardActivity.this).registerReceiver((listener),
                    new IntentFilter(Constants.ACTION_REFRESH_USERDATA)
            );

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private BroadcastReceiver listener = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.container);
                if (fragment != null) {
                    if (fragment instanceof FragDashboard) {
                        FragDashboard fragDashboard = (FragDashboard) fragment;

                    } else if (fragment instanceof FragContacts) {
                        FragContacts fragContacts = (FragContacts) fragment;
                        fragContacts.getContactsList(false);
                        fragContacts.getFollowers(false, 1);

                    } else if (fragment instanceof FragNotifications) {
                        FragNotifications fragNotifications = (FragNotifications) fragment;


                    } else if (fragment instanceof Navigation_Activity) {
                        Navigation_Activity fragnavigation = (Navigation_Activity) fragment;
                        fragnavigation.showFirst3Profiles();
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    @Override
    protected void onPause() {
        super.onPause();
        try {
            mStaticcontext = null;
            LocalBroadcastManager.getInstance(context).unregisterReceiver(listener);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void CloseBottomSheet() {
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == Constants.RESULT_ON_UPDATEUSERPROFILE) {
//                updateInfo();
            } else {
                android.app.Fragment fragment = getFragmentManager().findFragmentById(R.id.frame_fragment_btmshit);
                if (fragment != null && fragment instanceof CustomBottomSheetDialogFragment) {
                    ((CustomBottomSheetDialogFragment) fragment).onActivityResult(requestCode, resultCode, data);
                }
            }
        }
        if (resultCode == RESULT_OK && requestCode == Constants.RCODE_UPDATEQKTAG) {
            FragDashboard firstFrag = new FragDashboard().getInstance();
            if (firstFrag != null && firstFrag instanceof FragDashboard) {
                ((FragDashboard) firstFrag).onActivityResult(requestCode, resultCode, data);
            }
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.profile_edit_fab:

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(new String[]{Manifest.permission.CAMERA}, MY_CAMERA_REQUEST_CODE);
                    } else {
                        showScanEmployer();
                    }
                } else {
                    showScanEmployer();
                }
                break;
        }
    }

    @Override

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == MY_CAMERA_REQUEST_CODE) {
            try {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    showScanEmployer();
                } else {
                    Toast.makeText(this, "Please enable camera permission.", Toast.LENGTH_LONG).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (requestCode == MY_PERMISSIONS_REQUEST_LOCATION) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                // permission was granted, yay! Do the
                // location-related task you need to do.
                if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                    mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 0, mLocationListener);
                } else {
                    Toast.makeText(this, "Please enable location permission.", Toast.LENGTH_LONG).show();
                }
            }
        }
    }

    public void showScanEmployer() {

        dialogEmployer = new Dialog(DashboardActivity.this);
        dialogEmployer.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View view = LinearLayout.inflate(DashboardActivity.this, R.layout.dialog_employerprofile, null);
        dialogEmployer.setContentView(view);
        dialogEmployer.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialogEmployer.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialogEmployer.getWindow().setGravity(Gravity.BOTTOM);

        RecyclerView recyclerView = dialogEmployer.findViewById(R.id.recycleEmployer);
        recyclerView.addItemDecoration(new SimpleDividerItemDecoration(context));
        TextView tv_no_record_found_employer = dialogEmployer.findViewById(R.id.tv_no_record_found_employer);
        TextView tv_close_employer = dialogEmployer.findViewById(R.id.tv_close_employer);
        final EditText edtContactSearch = dialogEmployer.findViewById(R.id.edtContactSearch_employers);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));

        RealmResults<MyProfiles> myProfiles = RealmController.with(DashboardActivity.this).getMyProfiles();
        listEmployerprofile = new ArrayList<>();
        if (myProfiles != null && myProfiles.size() > 0) {
            String uniquecode = qkPreferences.getValues(QKPreferences.USER_UNIQUECODE) + "";
            for (MyProfiles profiles: myProfiles) {
                ProfilesList profilesList = RealmController.getRealmtoPojo(profiles);
                if (isMyProfile(profiles.getUnique_code())) {
                    if (profiles.getType() != null && profiles.getType().equals("4")/* && profiles.getIsOwnProfile() != null && profiles.getIsOwnProfile().equals("true")*/) {
                        listEmployerprofile.add(profilesList);
                    } else if (profiles.getUnique_code().equalsIgnoreCase(uniquecode + "")) {
                        listEmployerprofile.add(0, profilesList);
                    }
                }
            }

            if (listEmployerprofile.size() == 1) {
                Intent intent = new Intent(DashboardActivity.this, ScanQkTagActivity.class);
                intent.putExtra("PROFILE_ID", "0");
                startActivity(intent);
            } else {
                if (dialogEmployer != null)
                    dialogEmployer.show();
            }


            if (listEmployerprofile != null && listEmployerprofile.size() > 0) {
                tv_no_record_found_employer.setVisibility(View.GONE);
                recyclerView.setVisibility(View.VISIBLE);
                adpEmployer = new AdpEmployer(context, listEmployerprofile, new EmployerClick() {
                    @Override
                    public void onEmployerclick(int pos, String unique_code, String profile_id) {
                        if (dialogEmployer != null && dialogEmployer.isShowing())
                            dialogEmployer.dismiss();
                        Intent intent = new Intent(DashboardActivity.this, ScanQkTagActivity.class);
                        intent.putExtra(EXTRA_SHOW_OVERLAY, true);
                        intent.putExtra("PROFILE_ID", profile_id + "");
                        startActivity(intent);
                    }
                });
                recyclerView.setAdapter(adpEmployer);
            } else {
                tv_no_record_found_employer.setVisibility(View.VISIBLE);
                recyclerView.setVisibility(View.GONE);
            }
        } else {
            Intent intent = new Intent(DashboardActivity.this, ScanQkTagActivity.class);
            intent.putExtra("PROFILE_ID", "0");
            startActivity(intent);
        }
        tv_close_employer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (dialogEmployer != null && dialogEmployer.isShowing()) {
                    dialogEmployer.dismiss();
                }
            }
        });

        edtContactSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override

            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                edtContactSearch.setCursorVisible(true);
                if (adapter != null) {
                    String text = editable.toString();
                    ArrayList<ProfilesList> filterdNames = new ArrayList<>();
                    for (ProfilesList s: listEmployerprofile) {
                        if (s.getName().equalsIgnoreCase(text) || s.getName().contains(text) || s.getName().contains(text.toLowerCase()) || s.getName().contains(text.toUpperCase())) {
                            filterdNames.add(s);
                        }
                    }
                    adpEmployer.filterList(filterdNames);
                }
            }
        });
    }


    public void showContact(int type) {
        // type = 1 = follower
        // type = 2 = following

        try {
            viewPager.setCurrentItem(1);
            img_home.setImageResource(R.drawable.home_new_invi);
            img_contcat.setImageResource(R.drawable.rank_new_vi);
            img_notification.setImageResource(R.drawable.noti_new_invi);
            img_nav.setImageResource(R.drawable.ic_menu);

            viewPager.setVisibility(View.VISIBLE);
            frame_fragment.setVisibility(View.GONE);
            Fragment fragment = adapter.getItem(1);
            if (fragment instanceof FragContacts) {
                FragContacts fragDashboard = (FragContacts) fragment;
                fragDashboard.showContacts("name", true);
                fragDashboard.showFollowers("name", true);
                fragDashboard.showFollowing( true);

                if (type == 1) {
                    fragDashboard.ShowFollower();
                } else if (type == 2) {
                    fragDashboard.ShowFollowing();
                } else if (type == 0) {
                    fragDashboard.ShowContact();
                }

            }
            isChatOpen = false;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void UpdateInfo() {

        try {
            Fragment fragment1 = adapter.getItem(0);
            if (fragment1 instanceof FragDashboard) {
                FragDashboard fragDashboard = (FragDashboard) fragment1;
                fragDashboard.updateInfo();
            }
            Fragment fragment3 = adapter.getItem(2);
            if (fragment3 instanceof FragNotifications) {
                FragNotifications fragDashboard = (FragNotifications) fragment3;
                fragDashboard.updateInfo();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void HideChatFragment() {
        isChatOpen = false;
        viewPager.setVisibility(View.VISIBLE);
        frame_fragment.setVisibility(View.GONE);
    }

    public void ShowPager(String title) {
        viewPager.setVisibility(View.VISIBLE);
        frame_fragment.setVisibility(View.GONE);
        isOpen = false;
    }

    public boolean isMyProfile(String uniquecode) {
        List<String> myprofiles = qkPreferences.getMyProfilesIds();
        if (myprofiles != null)
            for (String s: myprofiles) {
                if (s.equals(uniquecode))
                    return true;
            }

        return false;
    }

    @Override
    public void onBackPressed() {

        if (bottomSheetAccept.getState() == BottomSheetBehavior.STATE_EXPANDED)
            bottomSheetAccept.setState(BottomSheetBehavior.STATE_COLLAPSED);

        Fragment fragment = adapter.getItem(0);
        boolean isReferearnOpen = false;
        if (fragment instanceof FragDashboard) {
            FragDashboard fragDashboard = (FragDashboard) fragment;
            if (fragDashboard.mBottomSheetBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
                isReferearnOpen = true;
                fragDashboard.hideReferView();
            }
        }

        if (!isReferearnOpen && isChatOpen) {
            isChatOpen = false;
            viewPager.setVisibility(View.VISIBLE);
            frame_fragment.setVisibility(View.GONE);

        } else if (!isReferearnOpen) {
            if (isOpen) {
                ShowPager("");
            }
            if (bottomSheetBehavior != null && bottomSheetBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);

            } else if (viewPager != null && viewPager.getCurrentItem() != 0) {
                viewPager.setCurrentItem(0);
                img_home.setImageResource(R.drawable.home_new_vi);
                img_contcat.setImageResource(R.drawable.rank_new_invi);
                img_notification.setImageResource(R.drawable.noti_new_invi);
                img_nav.setImageResource(R.drawable.ic_menu);
                try {
                    if (fragment instanceof FragDashboard) {
                        FragDashboard fragDashboard = (FragDashboard) fragment;
                        fragDashboard.showTags(false);
                        fragDashboard.setContctPorfiles(false);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (dialog != null) {

            } else {
                try {
                    if (viewPager.getCurrentItem() == 0) {
                        this.finish();
                    } else {
                        viewPager.setCurrentItem(0);
                        img_home.setImageResource(R.drawable.home_new_vi);
                        img_contcat.setImageResource(R.drawable.rank_new_invi);
                        img_notification.setImageResource(R.drawable.noti_new_invi);
                        img_nav.setImageResource(R.drawable.ic_menu);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        Log.d("position", i + "");
    }

    @Override
    public void onSwipe(int direction) {
        switch (direction) {
            case SimpleGestureFilter.SWIPE_RIGHT:
                break;
            case SimpleGestureFilter.SWIPE_LEFT:
                break;
            case SimpleGestureFilter.SWIPE_DOWN:
                break;
            case SimpleGestureFilter.SWIPE_UP:
                /*if (viewPager.getCurrentItem() == 0) {
                 *//*android.app.Fragment fragment = getFragmentManager().findFragmentById(R.id.frame_fragment);
                    if (fragment == null || !(fragment instanceof FragChats)) {
                        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                    }*//*
                    if (viewPager != null && viewPager.getVisibility() == View.VISIBLE) {
                        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                    }
                }*/
                break;
        }
    }

    @Override
    public void onDoubleTap() {
    }

    public static void showProfileinfo() {
        try {
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

//    public void changePageritem(Activity context) {
//        viewPager.setCurrentItem(1);
//    }

    public void updateRecentlyContact() {
        try {
            Fragment fragment = adapter.getItem(0);
            if (fragment instanceof FragDashboard) {
                FragDashboard fragDashboard = (FragDashboard) fragment;
                fragDashboard.setContctPorfiles(true);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

//    public void UpdateNotifications() {
//        try {
//            Fragment fragment3 = adapter.getItem(2);
//            if (fragment3 instanceof FragNotifications) {
//                FragNotifications fragDashboard = (FragNotifications) fragment3;
//                fragDashboard.getUserNotification(userid + "", 1, false, false);
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }

    public void updateinfofrombottomsheet() {

        try {
            Fragment fragment = adapter.getItem(viewPager.getCurrentItem());
            if (fragment instanceof FragDashboard) {
                FragDashboard fragDashboard = (FragDashboard) fragment;
                fragDashboard.updateInfo();
            } else if (fragment instanceof FragNotifications) {
                FragNotifications fragDashboard = (FragNotifications) fragment;
                fragDashboard.updateInfo();
            } else if (fragment instanceof Navigation_Activity) {
                Navigation_Activity fragDashboard = (Navigation_Activity) fragment;
                fragDashboard.showFirst3Profiles();
            }

            try {

                Fragment fragment1 = adapter.getItem(0);
                if (fragment1 instanceof FragDashboard) {
                    FragDashboard fragDashboard = (FragDashboard) fragment1;
                    fragDashboard.updateInfo();
                }

                Fragment fragment3 = adapter.getItem(2);
                if (fragment3 instanceof FragNotifications) {
                    FragNotifications fragDashboard = (FragNotifications) fragment3;
                    fragDashboard.updateInfo();
                }

                Fragment fragment4 = adapter.getItem(3);
                if (fragment4 instanceof Navigation_Activity) {
                    Navigation_Activity fragDashboard = (Navigation_Activity) fragment4;
                    fragDashboard.showFirst3Profiles();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

//    public void getLoginuserData() {
//        String token = "";
//        if (qkPreferences != null) {
//            token = qkPreferences.getApiToken() + "";
//        }
//
//        Map<String, String> params = new HashMap<>();
//        params.put("Authorization", "Bearer " + token + "");
//        params.put("Accept", "application/json");
//        params.put("QK_ACCESS_KEY", getResources().getString(R.string.QK_ACCESS_KEY) + "");
//
//        mService.getLoginUsersData(params, qkPreferences.getValuesInt(QKPreferences.USER_ID)).enqueue(new Callback<LoginPojo>() {
//            @Override
//            public void onResponse(Call<LoginPojo> call, Response<LoginPojo> response) {
//                Log.e("Login-Success", "" + response.body());
//                try {
//
//                    if (response != null && response.body() != null) {
//                        LoginPojo loginPojo = response.body();
//                        if (loginPojo.getStatus() == 200) {
//
//                            JSONObject jsonObject = new JSONObject(new Gson().toJson(loginPojo));
//                            ModelLoggedUser modelLoggedUser = new Gson().fromJson(jsonObject.toString(), ModelLoggedUser.class);
//                            qkPreferences.storeLoggedUser(modelLoggedUser);
//
//                            Fragment fragment = adapter.getItem(0);
//                            if (fragment instanceof FragDashboard) {
//                                FragDashboard fragDashboard = (FragDashboard) fragment;
//                                fragDashboard.updateInfo();
//                            }
//                        }
//                    }
//
//                } catch (Exception e) {
//                    Log.e("Login-Error", "" + e.getMessage());
//                }
//            }
//
//            @Override
//            public void onFailure(Call<LoginPojo> call, Throwable t) {
//                Log.e("Login-Error", "" + t.getLocalizedMessage());
//            }
//        });
//    }

    /*
    Store user current location
     */
    public LocationListener mLocationListener = new LocationListener() {
        @Override
        public void onLocationChanged(final Location location) {
            try {
                qkPreferences.storeLatLong(String.valueOf(location.getLatitude()), String.valueOf(location.getLongitude()));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onStatusChanged(String s, int i, Bundle bundle) {
        }

        @Override
        public void onProviderEnabled(String s) {
        }

        @Override
        public void onProviderDisabled(String s) {
        }
    };

    private TextView tv_no_record_found_gsearch;
    private AdpGlobalSearch adpGlobalSearch;
    private ExpandableListView expListViewSearch;
    private ArrayList<Continent> continentList;
    private EditText edtSearchGlobal;
    private ImageView imgClose;

    public static String MYPROFILES = "Profiles";
    public static String FOLLOWERS = "Followers";
    public static String COMPANY = "1";
    public static String ENTERPRISE = "2";
    public static String CELEBRITY = "3";
    public static String CONTACTS = "Contacts";
    public static String MORERESULTS = "More Results";
    List<ModelCategories> listCategories;
    private RecyclerView recycleCategories;
    private AdpCategories adpCategories;
    public static ProgressBar progresbarLoading;
    TextView tvSearchText;
    LinearLayout linearCategorySearch, linearSearch;
    Dialog dialog = null, dialogEmployer = null;
    boolean isCloseSearch = false;

    public void setupGlobalSearch() {

        dialog = new Dialog(context, android.R.style.Theme_Translucent_NoTitleBar);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.content_global_search);
        //dialog.setCancelable(false);

        Window window = dialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();

        wlp.gravity = Gravity.CENTER;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_BLUR_BEHIND;
        window.setAttributes(wlp);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        Utilities.changeStatusbar(context, dialog.getWindow());
        dialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialogInterface, int keyCode, KeyEvent event) {
                Log.e("Event", "" + event.getKeyCode());
                switch (keyCode) {
                    case KeyEvent.KEYCODE_BACK:
                        if (linearCategorySearch != null && linearCategorySearch.getVisibility() == View.VISIBLE) {
                            isCloseSearch = true;
                            linearCategorySearch.setVisibility(View.GONE);
                            linearSearch.setVisibility(View.VISIBLE);
                        } else if (isCloseSearch) {
                            isCloseSearch = false;

                        } else if (linearSearch != null && linearSearch.getVisibility() == View.VISIBLE) {
                            dialog.dismiss();
                        }
                        Log.e("Event", "1");
                        break;
                    case KeyEvent.KEYCODE_2:
                        Log.e("Event", "2");
                        break;
                    case KeyEvent.KEYCODE_3:
                        Log.e("Event", "3");
                        break;
                }

                return true;
            }
        });
        dialog.show();

        expListViewSearch = dialog.findViewById(R.id.lvExpSearch);
        edtSearchGlobal = dialog.findViewById(R.id.edtSearchGlobal);
        recycleCategories = dialog.findViewById(R.id.recycleCategories);
        tv_no_record_found_gsearch = dialog.findViewById(R.id.tv_no_record_found_gsearch);
        progresbarLoading = dialog.findViewById(R.id.progresbarLoading);
        tvSearchText = dialog.findViewById(R.id.tvSearchText);

        linearCategorySearch = dialog.findViewById(R.id.linearCategorySearch);
        linearSearch = dialog.findViewById(R.id.linearSearch);

        imgClose = dialog.findViewById(R.id.imgClose);
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                edtSearchGlobal.setText("");
            }
        });

        linearCategorySearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                linearCategorySearch.setVisibility(View.GONE);
                linearSearch.setVisibility(View.VISIBLE);

                try {
                    Fragment fragment = adapter.getItem(1);
                    if (fragment instanceof FragContacts) {
                        FragContacts fragDashboard = (FragContacts) fragment;
                        fragDashboard.showFollowing(true);
                        fragDashboard.showContacts("name", true);
                        fragDashboard.showFollowers("name", true);
                    }
                    updateRecentlyContact();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        edtSearchGlobal.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                Log.e("Search", "" + editable.toString());
                if (adpGlobalSearch != null) {
                    adpGlobalSearch.filterData(String.valueOf(editable.toString()));
                }
                try {
                    int count = adpGlobalSearch.getGroupCount();
                    if (count > 0) {
                        for (int j = 0; j < count; j++) {
                            expListViewSearch.expandGroup(j);
                        }
                        tv_no_record_found_gsearch.setVisibility(View.GONE);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        recycleResults = dialog.findViewById(R.id.recycleResults);
        recycleResults.setLayoutManager(new LinearLayoutManager(this));

        tv_nodatafound_result = dialog.findViewById(R.id.tv_nodatafound_result);

        listCategories = new ArrayList<>();

        ModelCategories modelCategories1 = new ModelCategories();
        modelCategories1.setCategory_name("All");
        modelCategories1.setSelected(true);
        listCategories.add(modelCategories1);

        modelCategories1 = new ModelCategories();
        modelCategories1.setCategory_name("Contacts");
        modelCategories1.setSelected(false);
        listCategories.add(modelCategories1);

        modelCategories1 = new ModelCategories();
        modelCategories1.setCategory_name("Company");
        modelCategories1.setSelected(false);
        listCategories.add(modelCategories1);

        modelCategories1 = new ModelCategories();
        modelCategories1.setCategory_name("Enterprise");
        modelCategories1.setSelected(false);
        listCategories.add(modelCategories1);

        modelCategories1 = new ModelCategories();
        modelCategories1.setCategory_name("Celebrity");
        modelCategories1.setSelected(false);
        listCategories.add(modelCategories1);

        recycleCategories.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        adpCategories = new AdpCategories(listCategories, new onClickCategories() {
            @Override
            public void onClickCategories(ModelCategories modelCategories, int pos) {
                try {
                    modelCategories.setSelected(true);
                    listCategories.set(pos, modelCategories);
                    for (int i = 0; i < listCategories.size(); i++) {
                        if (i != pos) {
                            ModelCategories modelCategories2 = listCategories.get(i);
                            modelCategories2.setSelected(false);
                            listCategories.set(i, modelCategories2);
                        }
                    }

                    if (adpCategories != null)
                        adpCategories.notifyDataSetChanged();

                    List<ModelGlobalSearch> updatedList = new ArrayList<>();

                    switch (pos) {
                        case 0://All
                            updatedList.addAll(listAll);
                            break;
                        case 1://Contacts
                            for (int i = 0; i < listAll.size(); i++) {
                                ModelGlobalSearch modelGlobalSearch = listAll.get(i);
                                if (modelGlobalSearch.getKey().equals(CONTACTS)
                                        || modelGlobalSearch.getKey().equals(FOLLOWERS)) {
                                    updatedList.add(modelGlobalSearch);
                                }
                            }
                            break;
                        case 2://Company
                            for (int i = 0; i < listAll.size(); i++) {
                                ModelGlobalSearch modelGlobalSearch = listAll.get(i);
                                if (modelGlobalSearch.getType() != null && modelGlobalSearch.getType().equals(COMPANY)) {
                                    updatedList.add(modelGlobalSearch);
                                }
                            }
                            break;
                        case 3://Enterprise
                            for (int i = 0; i < listAll.size(); i++) {
                                ModelGlobalSearch modelGlobalSearch = listAll.get(i);
                                if (modelGlobalSearch.getType() != null && modelGlobalSearch.getType().equals(ENTERPRISE)) {
                                    updatedList.add(modelGlobalSearch);
                                }
                            }
                            break;
                        case 4://Celebrity
                            for (int i = 0; i < listAll.size(); i++) {
                                ModelGlobalSearch modelGlobalSearch = listAll.get(i);
                                if (modelGlobalSearch.getType() != null && modelGlobalSearch.getType().equals(CELEBRITY)) {
                                    updatedList.add(modelGlobalSearch);
                                }
                            }
                            break;
                    }

                    AdpGlobalSearchResult adpGlobalSearchResult = new AdpGlobalSearchResult(context, updatedList, searchClick);
                    recycleResults.setAdapter(adpGlobalSearchResult);

                    if (updatedList != null && updatedList.size() > 0) {
                        tv_nodatafound_result.setVisibility(View.GONE);
                    } else {
                        tv_nodatafound_result.setVisibility(View.VISIBLE);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        recycleCategories.setAdapter(adpCategories);

        showGlobalSearch();
    }

    onGlobalSearchClick searchClick = new onGlobalSearchClick() {
        @Override
        public void onChildClick(ModelGlobalSearch modelGlobalSearch) {

            if (modelGlobalSearch.getKey().equalsIgnoreCase(CONTACTS)
                    || modelGlobalSearch.getType().equalsIgnoreCase(CONTACTS)) {

                if (modelGlobalSearch.getScan_request() == null || modelGlobalSearch.getScan_request().isEmpty()
                        || modelGlobalSearch.getScan_request().equalsIgnoreCase(Constants.CONTACT_PENDING)
                        || modelGlobalSearch.getScan_request().equalsIgnoreCase(Constants.CONTACT_REJECTED)) {

                    if (Utilities.isNetworkConnected(context)) {

//                        startActivity(new Intent(context, UnScanUsersActivity.class)
//                                .putExtra("scan_result", modelGlobalSearch.getScan_request())
//                                .putExtra("unique_code", modelGlobalSearch.getUnique_code()));

                        Intent intent = new Intent(context, MatchProfileActivity.class);
                        intent.putExtra("unique_code", modelGlobalSearch.getUnique_code());
                        intent.putExtra(Constants.KEY, Constants.KEY_SCANREQUEST);
                        startActivity(intent);

                    } else
                        Utilities.showToast(context, getResources().getString(R.string.no_internat_connection));

                } else if (modelGlobalSearch.getScan_request().equalsIgnoreCase(Constants.CONTACT_APPROVED)) {

                    Intent intent = new Intent(context, MatchProfileActivity.class);
                    intent.putExtra("unique_code", modelGlobalSearch.getUnique_code());
                    intent.putExtra(Constants.KEY, Constants.KEY_OTHER);
                    startActivity(intent);
                }

            } else if (modelGlobalSearch.getKey().equalsIgnoreCase(FOLLOWERS)) {

                Intent intent = new Intent(context, MatchProfileActivity.class);
                intent.putExtra("unique_code", modelGlobalSearch.getUnique_code());
                intent.putExtra("ProfileId", modelGlobalSearch.getProfileId().replace(".0", ""));
                intent.putExtra("UserId_Follower", modelGlobalSearch.getUser_id() + "");
                intent.putExtra(Constants.KEY, Constants.KEY_FOLLOWER);
                startActivity(intent);

            } else if (modelGlobalSearch.getKey().equalsIgnoreCase(MYPROFILES)) {

                if (modelGlobalSearch.getType().equalsIgnoreCase("4")) {
                    Intent intent = new Intent(context, EmployeeDetails.class);
                    intent.putExtra("unique_code", "" + modelGlobalSearch.getUnique_code());
                    intent.putExtra(Constants.KEY, Constants.KEY_SCANQK);
                    startActivity(intent);

                } else {

                    Intent intent = new Intent(context, NewScanProfileActivity.class);
                    intent.putExtra(Constants.KEY, Constants.KEY_GLOBAL_SEARCH);
                    intent.putExtra("unique_code", "" + modelGlobalSearch.getUnique_code());
                    intent.putExtra("profile_id", "" + modelGlobalSearch.getId());
                    intent.putExtra("user_id", "" + modelGlobalSearch.getUser_id());
                    intent.putExtra("isFollowed", "" + modelGlobalSearch.getIs_followed());
                    startActivity(intent);
                }

            } else if (modelGlobalSearch.getKey().equalsIgnoreCase(MORERESULTS)
                    || modelGlobalSearch.getKey().equalsIgnoreCase("profile")) {

                if (modelGlobalSearch.getType().equalsIgnoreCase("4")) {
                    Intent intent = new Intent(context, EmployeeDetails.class);
                    intent.putExtra("unique_code", "" + modelGlobalSearch.getUnique_code());
                    intent.putExtra(Constants.KEY, Constants.KEY_SCANQK);
                    startActivity(intent);
                } else {
                    Intent intent = new Intent(context, NewScanProfileActivity.class);
                    intent.putExtra(Constants.KEY, Constants.KEY_GLOBAL_SEARCH);
                    //intent.putExtra(Constants.KEY, Constants.KEY_SCANQK_GLOBAL_SEARCH);
                    intent.putExtra("unique_code", "" + modelGlobalSearch.getUnique_code());
                    intent.putExtra("profile_id", "" + modelGlobalSearch.getId());
                    intent.putExtra("user_id", "" + modelGlobalSearch.getUser_id());
                    intent.putExtra("isFollowed", "" + modelGlobalSearch.getIs_followed());
                    startActivity(intent);
                }
            }
        }

        @Override
        public void onMoreresultRefresh(Continent nContinent) {

        }

        @Override
        public void onListEmpty(boolean b) {

        }
    };

    List<ModelGlobalSearch> listAll;

    public void showGlobalSearch() {

        String login_user_unique_code = qkPreferences.getValues(QKPreferences.USER_UNIQUECODE);

        continentList = new ArrayList<>();

        RealmResults<Contacts> listcontacts = RealmController.with(DashboardActivity.this).getContacts();
        if (listcontacts != null && listcontacts.size() > 0) {

            ArrayList<ModelGlobalSearch> my_contacts = new ArrayList<>();
            ArrayList<ModelGlobalSearch> my_followers = new ArrayList<>();

            for (Contacts contacts: listcontacts) {

                ModelGlobalSearch modelGlobalSearch = new ModelGlobalSearch();

                modelGlobalSearch.setUsername(contacts.getUsername());
                modelGlobalSearch.setProfile_pic(contacts.getProfile_pic());
                modelGlobalSearch.setUnique_code(contacts.getUnique_code());
                modelGlobalSearch.setUser_id(contacts.getUser_id());
                modelGlobalSearch.setScan_request(Constants.CONTACT_APPROVED);

                if (contacts.getProfile_id() != null)
                    modelGlobalSearch.setProfileId(contacts.getProfile_id());

                if (contacts.getUnique_code() != null && !contacts.getUnique_code().equals(login_user_unique_code)) {
                    if (contacts.getContact_type() != null && contacts.getContact_type().equals(Constants.TYPE_CONTACTS)) {
                        modelGlobalSearch.setKey(CONTACTS);
                        my_contacts.add(modelGlobalSearch);

                    } else if (contacts.getContact_type() != null && contacts.getContact_type().equals(Constants.TYPE_FOLLOWERS)) {
                        modelGlobalSearch.setKey(FOLLOWERS);
                        my_followers.add(modelGlobalSearch);

                    }
                }
            }

            continentList.add(new Continent(CONTACTS, my_contacts));
            continentList.add(new Continent(FOLLOWERS, my_followers));
        }

        RealmResults<MyProfiles> listMyProfiles = RealmController.with(DashboardActivity.this).getMyProfiles();
        if (listMyProfiles != null && listMyProfiles.size() > 0) {

            ArrayList<ModelGlobalSearch> globalSearchList = new ArrayList<>();

            for (MyProfiles myProfiles: listMyProfiles) {

                ModelGlobalSearch modelGlobalSearch = new ModelGlobalSearch();
                modelGlobalSearch.setUsername(myProfiles.getName());
                modelGlobalSearch.setProfile_pic(myProfiles.getLogo());
                modelGlobalSearch.setUnique_code(myProfiles.getUnique_code());
                modelGlobalSearch.setId(myProfiles.getId());
                modelGlobalSearch.setType(myProfiles.getType());
                modelGlobalSearch.setScan_request(Constants.CONTACT_APPROVED);
                modelGlobalSearch.setUser_id(myProfiles.getUser_id());
                modelGlobalSearch.setKey(MYPROFILES);
                if (myProfiles.getType() != null && !myProfiles.getType().equals("0") && !myProfiles.getType().equals("4") && myProfiles.getIsOwnProfile() != null &&
                        myProfiles.getIsOwnProfile().equals("true")) {
                    globalSearchList.add(modelGlobalSearch);
                }
            }

            continentList.add(new Continent(MYPROFILES, globalSearchList));
        }

        adpGlobalSearch = new AdpGlobalSearch(this, continentList, new onGlobalSearchClick() {
            @Override
            public void onChildClick(ModelGlobalSearch modelGlobalSearch) {

                linearCategorySearch.setVisibility(View.VISIBLE);
                linearSearch.setVisibility(View.GONE);
                tvSearchText.setText(edtSearchGlobal.getText().toString());

                Utilities.hidekeyboard(context, dialog.getWindow().getDecorView());

                if (listCategories != null && listCategories.size() > 0) {

                    for (int i = 0; i < listCategories.size(); i++) {
                        ModelCategories modelCategories = listCategories.get(i);
                        if (i == 0) {
                            modelCategories.setSelected(true);
                        } else modelCategories.setSelected(false);
                        listCategories.set(i, modelCategories);
                    }
                    if (adpCategories != null) {
                        adpCategories.notifyDataSetChanged();
                    }

                    // modified list as category selected
                    ArrayList<Continent> continentArrayList = adpGlobalSearch.getSearchList();
                    listAll = new ArrayList<>();
                    String search_key = modelGlobalSearch.getUsername();
                    List<String> listaddedIds = new ArrayList<>();
                    if (continentArrayList != null && continentArrayList.size() > 0) {
                        for (int i = 0; i < continentArrayList.size(); i++) {
                            List<ModelGlobalSearch> globalSearchList = continentArrayList.get(i).getCountryList();
                            if (globalSearchList != null && globalSearchList.size() > 0) {
                                for (int j = 0; j < globalSearchList.size(); j++) {
                                    if (search_key.equals(globalSearchList.get(j).getUsername())) {
                                        if (listaddedIds.size() == 0) {
                                            listaddedIds.add(globalSearchList.get(j).getUnique_code());
                                            listAll.add(globalSearchList.get(j));
                                        } else {
                                            if (!listaddedIds.contains(globalSearchList.get(j).getUnique_code())) {
                                                listAll.add(globalSearchList.get(j));
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        Log.e("List All", "" + listAll.size());
                        AdpGlobalSearchResult adpGlobalSearchResult = new AdpGlobalSearchResult(context, listAll, searchClick);
                        recycleResults.setAdapter(adpGlobalSearchResult);
                    }
                }
            }

            @Override
            public void onListEmpty(boolean isEmpty) {

                if (isEmpty) {
                    tv_no_record_found_gsearch.setText("Sorry, We didn't find any results matching this search");
                    tv_no_record_found_gsearch.setVisibility(View.VISIBLE);
                } else {
                    tv_no_record_found_gsearch.setVisibility(View.GONE);
                }
            }

            @Override
            public void onMoreresultRefresh(Continent nContinent) {
                int count = adpGlobalSearch.getGroupCount();
                if (count > 0) {
                    for (int j = 0; j < count; j++) {
                        expListViewSearch.expandGroup(j);
                    }
                    tv_no_record_found_gsearch.setVisibility(View.GONE);
                }
            }
        });

        expListViewSearch.setAdapter(adpGlobalSearch);
    }

    @Override
    public void newMessagereceived(Chat chat, final Message msg) {
        try {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (msg != null && msg.getBodies().size() > 0) {
                        try {
                            qkPreferences.StoreUnreadCount(1);
                            Fragment fragment1 = adapter.getItem(0);
                            if (fragment1 instanceof FragDashboard) {
                                FragDashboard fragDashboard = (FragDashboard) fragment1;
                                fragDashboard.SetUnReadCount();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public class AdpCategories extends RecyclerView.Adapter<AdpCategories.MyCateHolder> {

        List<ModelCategories> modelCategories;
        private onClickCategories onClickCategories;

        public AdpCategories(List<ModelCategories> modelCategories, onClickCategories onClickCategories) {
            this.modelCategories = modelCategories;
            this.onClickCategories = onClickCategories;
        }

        @Override
        public MyCateHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_categories, null);
            MyCateHolder viewHolder = new MyCateHolder(itemLayoutView);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(MyCateHolder holder, final int position) {

            final ModelCategories itemCategory = modelCategories.get(position);
            holder.tvCatname.setText(itemCategory.getCategory_name());
            if (itemCategory.isSelected) {
                holder.tvCatname.setTextColor(Color.parseColor("#2ABBBE"));
                holder.tvCatname.setBackgroundResource(R.drawable.drawable_cate_selected);

            } else {
                holder.tvCatname.setTextColor(Color.parseColor("#FFFFFF"));
                holder.tvCatname.setBackgroundResource(R.drawable.drawable_cate_unselected);
            }

            holder.linearRoot.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onClickCategories.onClickCategories(itemCategory, position);
                }
            });
        }


        @Override
        public int getItemCount() {
            return modelCategories.size();
        }

        public class MyCateHolder extends RecyclerView.ViewHolder {
            TextView tvCatname;
            LinearLayout linearRoot;

            public MyCateHolder(View itemView) {
                super(itemView);
                tvCatname = itemView.findViewById(R.id.tvCatname);
                linearRoot = itemView.findViewById(R.id.linearRoot);
            }
        }
    }

    public interface onClickCategories {
        void onClickCategories(ModelCategories modelCategories, int pos);
    }

    public class ModelCategories {

        private String category_name;
        private boolean isSelected = false;

        public String getCategory_name() {
            return category_name;
        }

        public void setCategory_name(String category_name) {
            this.category_name = category_name;
        }

        public boolean isSelected() {
            return isSelected;
        }

        public void setSelected(boolean selected) {
            isSelected = selected;
        }
    }

    public void getUserQRCode() {
        if (Utilities.isNetworkConnected(context)) {
            String login_user_id = qkPreferences.getLoggedUserid();
            String url = ApiUtils.BASE_URL_LIVE + "/api/user_qr_code/" + login_user_id;
            mService.getUserQRCode(url).enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    try {
                        if (response != null && response.body() != null) {
                            JSONObject jsonObject = new JSONObject(response.body().string());
                            String status = jsonObject.optString("status");
                            if (status != null && !status.isEmpty() && status.equalsIgnoreCase("200")) {
                                JSONObject objectData = jsonObject.getJSONObject("data");
                                String qr_code = objectData.optString("qr_code");
                                qkPreferences.setValues(Constants.SHARE_USER_QRCODE, qr_code);
                                String qr_link = objectData.optString("qr_link");
                                qkPreferences.setValues(Constants.SHARE_USER_PUBLIC_URL, qr_link);
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {

                }
            });
        }
    }
}
