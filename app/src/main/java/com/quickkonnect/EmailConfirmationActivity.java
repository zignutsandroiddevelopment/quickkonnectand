package com.quickkonnect;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.utilities.Utilities;

public class EmailConfirmationActivity extends AppCompatActivity {

    TextView btn_gotoSignin;
    String email;
    TextView tv_text, textViewEmail, textView3, textView4, textView5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Utilities.changeStatusbar(EmailConfirmationActivity.this, getWindow());
        setContentView(R.layout.activity_email_confirmation);

        btn_gotoSignin = findViewById(R.id.btn_gotosignin);
        tv_text = findViewById(R.id.textView2);
        textViewEmail = findViewById(R.id.textViewEmail);
        textView3 = findViewById(R.id.textView3);
        textView4 = findViewById(R.id.textView4);
        textView5 = findViewById(R.id.textView5);
        btn_gotoSignin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(EmailConfirmationActivity.this, LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });

        Bundle extras = getIntent().getExtras();

        if (extras != null) {

            email = extras.getString("email");

            Log.d("iemail", email + "");

            // tv_text.setText("We have send an email to your address:"+email+".Please click on the link to confirm your account activation.");
        }
        SetFontStyle();

    }

    public void SetFontStyle() {
//        Typeface lato_regular = Typeface.createFromAsset(getAssets(), "fonts/latoregular.ttf");
//        Typeface lato_bold = Typeface.createFromAsset(getAssets(), "fonts/latobold.ttf");
//        btn_gotoSignin.setTypeface(lato_regular);
        /*email = "jatind@zignuts.com";*/
        String first = "We have sent an email to your ";
        String next = "address " + "<font color='#2ABBBE'>" + email + "</font>";
        tv_text.setText(Html.fromHtml(first + next));

//        tv_text.setTypeface(lato_bold);
//        textViewEmail.setTypeface(lato_bold);
//        textView3.setTypeface(lato_regular);
//        textView4.setTypeface(lato_regular);
//        textView5.setTypeface(lato_bold);


    }
}
