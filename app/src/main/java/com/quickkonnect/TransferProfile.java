package com.quickkonnect;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.cunoraz.gifview.library.GifView;
import com.facebook.internal.Utility;
import com.google.gson.Gson;
import com.models.BasicDetail;
import com.models.InviteQKPojo;
import com.retrofitservice.ApiUtils;
import com.retrofitservice.SOService;
import com.squareup.picasso.Picasso;
import com.utilities.QKPreferences;
import com.utilities.Utilities;
import com.volleyapirequest.VolleyApiRequest;
import com.volleyapirequest.VolleyCallBack;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ZTLAB-12 on 25-07-2018.
 */

public class TransferProfile extends AppCompatActivity {

    EditText email;
    TextView search;
    String profile_id = "";
    String invite_emailid = "";
    String user_id = "", id = "";
    QKPreferences qkPreferences;
    private SOService mService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transfer);

        Utilities.ChangeStatusBar(TransferProfile.this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_transfer_profile);
        toolbar.setTitle("Transfer profile");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close_white_24dp);

        qkPreferences = new QKPreferences(TransferProfile.this);
        mService = ApiUtils.getSOService();
        user_id = qkPreferences.getValuesInt(QKPreferences.USER_ID) + "";

        try {
            profile_id = getIntent().getExtras().getString("PROFILE_ID");
        } catch (Exception e) {
            e.printStackTrace();
        }

        email = findViewById(R.id.edt_email_transfer_profile);
        search = findViewById(R.id.tvSearch_transfer_profile);

        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    Utilities.hidekeyboard(TransferProfile.this, email);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (!email.getText().toString().isEmpty() && Utilities.isValidEmail(email.getText().toString())) {
                    getUserFromEmail(email.getText().toString());
                    email.setText("");
                } else {
                    Utilities.showToast(TransferProfile.this, "Please enter valid email");
                }
            }
        });

    }

    private void getUserFromEmail(final String email1) {
        try {
            JSONObject obj = new JSONObject();
            try {
                obj.put("email", email1);
                obj.put("user_id", user_id + "");
            } catch (Exception e) {
                e.printStackTrace();
            }
            new VolleyApiRequest(TransferProfile.this, true, VolleyApiRequest.REQUEST_BASEURL + "api/member/check_member",
                    new VolleyCallBack() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.e("Request Data", "" + response);
                            try {
                                String msg = response.getString("msg");
                                String status = response.getString("status");
                                if (status.equalsIgnoreCase("200")) {
                                    JSONObject data = response.getJSONObject("data");
                                    JSONObject basicdetail = data.getJSONObject("basic_detail");
                                    BasicDetail basicDetail = new Gson().fromJson(basicdetail.toString(), BasicDetail.class);
                                    if (basicDetail != null && !basicDetail.toString().equalsIgnoreCase("null")) {
                                        if (basicDetail.getUserId() != null && !basicDetail.getUserId().toString().equalsIgnoreCase("null")) {
                                            id = basicDetail.getUserId().toString();
                                        }
                                        if (user_id.equalsIgnoreCase(id + "")) {
                                            Utilities.showToast(TransferProfile.this, "This profile is already yours");
                                        } else {
                                            OpenSuccessDialog(basicDetail);
                                        }
                                    } else {
                                        //No user found invite
                                        invitePeople(email1);
                                    }
                                } else {
                                    //invite user
                                    invitePeople(email1);
                                }
                            } catch (Exception e) {
                                Utilities.showToast(TransferProfile.this, "This email is not registered with quickkonnect.");
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onErrorResponse(VolleyError error) {
                            VolleyApiRequest.showVolleyError(TransferProfile.this, error);
                        }
                    }).enqueRequest(obj, Request.Method.POST);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void OpenSuccessDialog(BasicDetail basicDetail) {
        final Dialog dialog = new Dialog(TransferProfile.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View view = LinearLayout.inflate(TransferProfile.this, R.layout.success_dialog_transfer_profile, null);
        dialog.setContentView(view);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        dialog.show();
        dialog.getWindow().setAttributes(lp);
        final TextView username = dialog.findViewById(R.id.tv_success_username);
        TextView msg = dialog.findViewById(R.id.tvmessage_success);
        TextView accept = dialog.findViewById(R.id.tv_transfer_accept);
        TextView reject = dialog.findViewById(R.id.tv_transfer_reject);
        ImageView profielpic = dialog.findViewById(R.id.img_success_profile);

        reject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                }
              /*  setResult(AppCompatActivity.RESULT_OK);
                TransferProfile.this.finish();*/
            }
        });
        accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog.Builder builder = new AlertDialog.Builder(TransferProfile.this);
                builder.setTitle("Alert!");
                String msg = "";
                if (!username.getText().toString().isEmpty() && !username.getText().toString().equalsIgnoreCase("") && !username.getText().toString().equalsIgnoreCase("null"))
                    msg = "Are you sure want to transfer this profile to " + username.getText().toString() + "?";
                else
                    msg = "Are you sure want to transfer this profile?";
                builder.setMessage(msg);
                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                        transferProfile(id + "", dialog);
                    }
                });
                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                        if (dialog != null && dialog.isShowing()) {
                            dialog.dismiss();
                        }
                    }
                });
                builder.show();
            }
        });

        if (basicDetail.getProfilePic() != null && !basicDetail.getProfilePic().isEmpty() && !basicDetail.getProfilePic().equals("null")) {
            Picasso.with(TransferProfile.this)
                    .load(basicDetail.getProfilePic())
                    .error(R.drawable.ic_default_profile_photo)
                    .placeholder(R.drawable.ic_default_profile_photo)
                    .transform(new CircleTransform())
                    .fit()
                    .into(profielpic);
        } else {
            profielpic.setImageResource(R.drawable.ic_default_profile_photo);
        }
        username.setText(basicDetail.getFirstname() + " " + basicDetail.getLastname());
        if (basicDetail.getEmail() != null && !basicDetail.getEmail().equalsIgnoreCase("") && !basicDetail.getEmail().equalsIgnoreCase("null"))
            msg.setText(basicDetail.getEmail());

    }

    public void invitePeople(String emailid) {

        final Dialog dialog = new Dialog(TransferProfile.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View view = LinearLayout.inflate(TransferProfile.this, R.layout.dialog_transfer_profile, null);
        dialog.setContentView(view);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        dialog.show();
        dialog.getWindow().setAttributes(lp);

        final EditText email, search_email, edtMsg;
        TextView title, subtitle, no_user, cancel, search, confirm, invite, username, tv_invite_new, tv_cancel_invite;
        final TextView tvCountMsglength;
        LinearLayout linearInvitePeople, linear_profile, lin_aprove_reject;
        ImageView image_emp = dialog.findViewById(R.id.image_emp);
        image_emp.setVisibility(View.GONE);

        tv_invite_new = dialog.findViewById(R.id.tv_invite_new);
        tv_cancel_invite = dialog.findViewById(R.id.tv_cancel_invite);
        lin_aprove_reject = dialog.findViewById(R.id.lin_aprove_reject);

        email = dialog.findViewById(R.id.edt_email_transfer);
        title = dialog.findViewById(R.id.tvAlert_dg);
        subtitle = dialog.findViewById(R.id.tvAlert_subtitle);
        no_user = dialog.findViewById(R.id.tv_nouser_transfer);
        edtMsg = dialog.findViewById(R.id.edtMsg);
        linearInvitePeople = dialog.findViewById(R.id.linearInvitePeople);
        cancel = dialog.findViewById(R.id.tvCancel_transfer);
        search = dialog.findViewById(R.id.tvSearch_transfer);
        confirm = dialog.findViewById(R.id.tvConfirm_transfer);
        invite = dialog.findViewById(R.id.tvInvite_transfer);
        username = dialog.findViewById(R.id.tv_name_transfer);
        linear_profile = dialog.findViewById(R.id.lin_profile_transfer);
        search_email = dialog.findViewById(R.id.edt_search_mail_transfer);
        tvCountMsglength = dialog.findViewById(R.id.tvCountMsglength);

        title.setVisibility(View.GONE);
        search.setVisibility(View.VISIBLE);
        cancel.setVisibility(View.GONE);
        invite.setVisibility(View.GONE);
        confirm.setVisibility(View.GONE);
        email.setVisibility(View.VISIBLE);
        subtitle.setVisibility(View.VISIBLE);

        lin_aprove_reject.setVisibility(View.VISIBLE);

        //textimput_email_transfer.setVisibility(View.VISIBLE);
        no_user.setVisibility(View.GONE);
        no_user.setText("This email is not registered with QuickKonnect.");
        search_email.setText("");
        username.setText("");

        edtMsg.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence != null && charSequence.length() > 0) {
                    int remain = 200 - charSequence.length();
                    tvCountMsglength.setText("" + remain + " Characters");
                } else {
                    tvCountMsglength.setText("200 Characters");
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        invite_emailid = emailid;
        linearInvitePeople.setVisibility(View.VISIBLE);
        search.setVisibility(View.GONE);
        cancel.setVisibility(View.GONE);
        invite.setVisibility(View.GONE);
        confirm.setVisibility(View.GONE);
        linear_profile.setVisibility(View.GONE);
        email.setVisibility(View.GONE);
        subtitle.setVisibility(View.GONE);
        search_email.setText("");
        String html = "<u><font color='#2ABBBE'>" + emailid + "</font></u> is not registered with Quickkonnect.";
        no_user.setText(Html.fromHtml(html));
        username.setText("");
        no_user.setVisibility(View.VISIBLE);

        tv_cancel_invite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    Utilities.hidekeyboard(TransferProfile.this, view);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (dialog != null && dialog.isShowing())
                    dialog.dismiss();
            }
        });

        tv_invite_new.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    Utilities.hidekeyboard(TransferProfile.this, view);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (invite_emailid != null && !invite_emailid.isEmpty()) {
                    final ProgressDialog pd = Utilities.showProgress(TransferProfile.this);
                    String message = edtMsg.getText().toString();
                    if (message != null && !message.isEmpty()) {

                        String token = "";
                        if (qkPreferences != null) {
                            token = qkPreferences.getApiToken() + "";
                        }

                        Map<String, String> params = new HashMap<>();
                        params.put("Authorization", "Bearer " + token + "");
                        params.put("Accept", "application/json");
                        params.put("QK_ACCESS_KEY", getResources().getString(R.string.QK_ACCESS_KEY) + "");

                        mService.invite_employee(params, invite_emailid, message).enqueue(new Callback<InviteQKPojo>() {
                            @Override
                            public void onResponse(Call<InviteQKPojo> call, Response<InviteQKPojo> response) {
                                if (pd != null && pd.isShowing())
                                    pd.dismiss();
                                if (dialog != null && dialog.isShowing())
                                    dialog.dismiss();
                                Log.e("updateSetting-Success", "" + response);
                                if (response != null && response.body() != null) {
                                    InviteQKPojo status200 = response.body();
                                    Utilities.showToast(getApplicationContext(), "" + status200.getMsg());
                                }
                            }

                            @Override
                            public void onFailure(Call<InviteQKPojo> call, Throwable t) {
                                Log.e("updateSetting-Error", "" + t.getLocalizedMessage());
                                if (pd != null && pd.isShowing())
                                    pd.dismiss();
                                if (dialog != null && dialog.isShowing())
                                    dialog.dismiss();
                            }
                        });
                    }
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent i = new Intent();
                i.putExtra("Result", "0");
                setResult(RESULT_OK, i);
                TransferProfile.this.finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void transferProfile(String userid, final Dialog dialog) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("profile_id", profile_id + "");
            jsonObject.put("user_id", userid);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        new VolleyApiRequest(TransferProfile.this, true, VolleyApiRequest.REQUEST_BASEURL + "api/profile/transfer_request",
                new VolleyCallBack() {
                    @Override
                    public void onResponse(JSONObject response) {
                        String responseStatus = null;
                        try {
                            responseStatus = response.getString("status");
                            String msg = response.getString("msg");
                            Utilities.showToast(TransferProfile.this, msg + "");
                            if (responseStatus.equals("200")) {
                                // Success
                                /*tv_pending_transfer.setVisibility(View.VISIBLE);
                                rel_unread_followers.setVisibility(View.VISIBLE);
                                isClick = false;*/
                                if (dialog != null && dialog.isShowing())
                                    dialog.dismiss();
                                Intent i = new Intent();
                                i.putExtra("Result", "1");
                                setResult(RESULT_OK, i);
                                TransferProfile.this.finish();
                            } else {
                                // Fail
                                if (dialog != null && dialog.isShowing())
                                    dialog.dismiss();
                                Intent i = new Intent();
                                i.putExtra("Result", "0");
                                setResult(RESULT_OK, i);
                                TransferProfile.this.finish();
                                /*tv_pending_transfer.setVisibility(View.GONE);
                                rel_unread_followers.setVisibility(View.GONE);
                                isClick = true;*/
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyApiRequest.showVolleyError(TransferProfile.this, error);
                    }
                }).enqueRequest(jsonObject, Request.Method.POST);
    }
}

