package com.quickkonnect;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.Image;
import android.os.Build;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.activities.CreateProfileActivity;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.fragments.createprofileintro.FragProfileTypeName;
import com.google.gson.Gson;
import com.models.BasicDetail;
import com.models.InviteQKPojo;
import com.realmtable.Employees;
import com.retrofitservice.ApiUtils;
import com.retrofitservice.SOService;
import com.utilities.QKPreferences;
import com.utilities.Utilities;
import com.volleyapirequest.VolleyApiRequest;
import com.volleyapirequest.VolleyCallBack;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EmployeeSearch extends AppCompatActivity {

    EditText email;
    TextView search;

    String profile_id = "";
    String invite_emailid = "";
    String user_id = "";
    QKPreferences qkPreferences;
    String UserBasicDetail;
    private SOService mService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_employee_search);

        if (Build.VERSION.SDK_INT >= 19 && Build.VERSION.SDK_INT < 21) {
            setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, true);
        }
        if (Build.VERSION.SDK_INT >= 19) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }
        if (Build.VERSION.SDK_INT >= 21) {
            setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, false);
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Add Employee");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close_white_24dp);

        qkPreferences = new QKPreferences(EmployeeSearch.this);
        mService = ApiUtils.getSOService();
        user_id = qkPreferences.getValuesInt(QKPreferences.USER_ID) + "";

        try {
            profile_id = getIntent().getExtras().getString("PROFILE_ID");
        } catch (Exception e) {
            e.printStackTrace();
        }

        email = findViewById(R.id.edt_email_transfer);
        search = findViewById(R.id.tvSearch_transfer);

        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    Utilities.hidekeyboard(EmployeeSearch.this, email);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (!email.getText().toString().isEmpty() && Utilities.isValidEmail(email.getText().toString())) {
                    getUserFromEmail(email.getText().toString());
                    email.setText("");
                } else {
                    Utilities.showToast(EmployeeSearch.this, "Please enter valid email");
                }
            }
        });
    }

    public static void setWindowFlag(Activity activity, final int bits, boolean on) {
        Window win = activity.getWindow();
        WindowManager.LayoutParams winParams = win.getAttributes();
        if (on) {
            winParams.flags |= bits;
        } else {
            winParams.flags &= ~bits;
        }
        win.setAttributes(winParams);
    }

    private void getUserFromEmail(final String email1) {
        try {
            JSONObject obj = new JSONObject();
            try {
                obj.put("email", email1 + "");
                obj.put("user_id", user_id + "");
                obj.put("profile_id", profile_id + "");
            } catch (Exception e) {
                e.printStackTrace();
            }
            new VolleyApiRequest(EmployeeSearch.this, true, VolleyApiRequest.REQUEST_BASEURL + "api/member/check_member",
                    new VolleyCallBack() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.e("Request Data", "" + response);
                            try {
                                String msg = response.getString("msg") + "";
                                String status = response.getString("status") + "";

                                if (status.equalsIgnoreCase("200")) {
                                    String is_added = response.getString("is_added") + "";
                                    if (!is_added.equalsIgnoreCase("") && !is_added.equalsIgnoreCase("null") && !is_added.equalsIgnoreCase("1")) {
                                        JSONObject data = response.getJSONObject("data");
                                        JSONObject basicdetail = data.getJSONObject("basic_detail");
                                        BasicDetail basicDetail = new Gson().fromJson(basicdetail.toString(), BasicDetail.class);
                                        UserBasicDetail = basicdetail.toString();

                                        Intent i = new Intent(EmployeeSearch.this, EmployeesProfile.class);
                                        i.putExtra("KEY", "HEADER");
                                        i.putExtra("USER_ID", profile_id + "");
                                        i.putExtra("USER_BASIC_DETAIL", UserBasicDetail + "");
                                        startActivity(i);
                                        EmployeeSearch.this.finish();

                                        //startActivityForResult(i, HEADER_RESULT);
                                    } else {
                                        Utilities.showToast(EmployeeSearch.this, "This profile is already added in your profile !");
                                    }
                                } else {
                                    /*1 - invite_user
                                    2 - inactive_user
                                    3 - admin_owner_user
                                    4 - blocked_user
                                    5 - added_user*/
                                    String type = response.getString("type");

                                    if (type.equalsIgnoreCase("invite_user")) {
                                        invitePeople(email1);
                                    } else if (type.equalsIgnoreCase("inactive_user")) {
                                        ShowMessageFormWeb(msg);
                                    } else if (type.equalsIgnoreCase("admin_owner_user")) {
                                        ShowMessageFormWeb(msg);
                                    } else if (type.equalsIgnoreCase("blocked_user")) {
                                        ShowMessageFormWeb(msg);
                                    } else if (type.equalsIgnoreCase("added_user")) {
                                        ShowMessageFormWeb(msg);
                                    } else {
                                        ShowMessageFormWeb(msg);
                                    }
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                                invitePeople(email1);
                            }
                        }

                        @Override
                        public void onErrorResponse(VolleyError error) {
                            UserBasicDetail = "";
                        }
                    }).enqueRequest(obj, Request.Method.POST);
        } catch (Exception e) {
            UserBasicDetail = "";
            e.printStackTrace();
        }
    }

    private void ShowMessageFormWeb(String msg) {
        Utilities.showToast(EmployeeSearch.this, msg);
    }

    public void invitePeople(String emailid) {

        final Dialog dialog = new Dialog(EmployeeSearch.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View view = LinearLayout.inflate(EmployeeSearch.this, R.layout.dialog_transfer_profile, null);
        dialog.setContentView(view);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        dialog.show();
        dialog.getWindow().setAttributes(lp);

        final EditText email, search_email, edtMsg;
        TextView title, subtitle, no_user, cancel, search, confirm, invite, username, tv_invite_new, tv_cancel_invite;
        final TextView tvCountMsglength;
        LinearLayout linearInvitePeople, linear_profile, lin_aprove_reject;
        ImageView image_emp = dialog.findViewById(R.id.image_emp);
        image_emp.setVisibility(View.GONE);

        tv_invite_new = dialog.findViewById(R.id.tv_invite_new);
        tv_cancel_invite = dialog.findViewById(R.id.tv_cancel_invite);
        lin_aprove_reject = dialog.findViewById(R.id.lin_aprove_reject);

        email = dialog.findViewById(R.id.edt_email_transfer);
        title = dialog.findViewById(R.id.tvAlert_dg);
        subtitle = dialog.findViewById(R.id.tvAlert_subtitle);
        no_user = dialog.findViewById(R.id.tv_nouser_transfer);
        edtMsg = dialog.findViewById(R.id.edtMsg);
        linearInvitePeople = dialog.findViewById(R.id.linearInvitePeople);
        cancel = dialog.findViewById(R.id.tvCancel_transfer);
        search = dialog.findViewById(R.id.tvSearch_transfer);
        confirm = dialog.findViewById(R.id.tvConfirm_transfer);
        invite = dialog.findViewById(R.id.tvInvite_transfer);
        username = dialog.findViewById(R.id.tv_name_transfer);
        linear_profile = dialog.findViewById(R.id.lin_profile_transfer);
        search_email = dialog.findViewById(R.id.edt_search_mail_transfer);
        tvCountMsglength = dialog.findViewById(R.id.tvCountMsglength);

        title.setVisibility(View.GONE);

        search.setVisibility(View.VISIBLE);
        cancel.setVisibility(View.GONE);
        invite.setVisibility(View.GONE);
        confirm.setVisibility(View.GONE);
        email.setVisibility(View.VISIBLE);
        subtitle.setVisibility(View.VISIBLE);

        lin_aprove_reject.setVisibility(View.VISIBLE);

        //textimput_email_transfer.setVisibility(View.VISIBLE);
        no_user.setVisibility(View.GONE);
        no_user.setText("This email is not registered with QuickKonnect.");
        search_email.setText("");
        username.setText("");

        edtMsg.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence != null && charSequence.length() > 0) {
                    int remain = 200 - charSequence.length();
                    tvCountMsglength.setText("" + remain + " Characters");
                } else {
                    tvCountMsglength.setText("200 Characters");
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        invite_emailid = emailid;
        linearInvitePeople.setVisibility(View.VISIBLE);
        UserBasicDetail = "";
        search.setVisibility(View.GONE);
        cancel.setVisibility(View.VISIBLE);
        invite.setVisibility(View.VISIBLE);
        confirm.setVisibility(View.GONE);
        linear_profile.setVisibility(View.GONE);
        email.setVisibility(View.GONE);
        subtitle.setVisibility(View.GONE);
        search_email.setText("");
        String html = "<u><font color='#2ABBBE'>" + emailid + "</font></u> is not registered with Quickkonnect";
        no_user.setText(Html.fromHtml(html));
        username.setText("");
        no_user.setVisibility(View.VISIBLE);

        tv_cancel_invite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    Utilities.hidekeyboard(EmployeeSearch.this, view);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (dialog != null && dialog.isShowing())
                    dialog.dismiss();
            }
        });

        tv_invite_new.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    Utilities.hidekeyboard(EmployeeSearch.this, view);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (invite_emailid != null && !invite_emailid.isEmpty()) {
                    final ProgressDialog pd = Utilities.showProgress(EmployeeSearch.this);
                    String message = edtMsg.getText().toString();
                    if (message != null && !message.isEmpty()) {

                        String token = "";
                        if (qkPreferences != null) {
                            token = qkPreferences.getApiToken() + "";
                        }

                        Map<String, String> params = new HashMap<>();
                        params.put("Authorization", "Bearer " + token + "");
                        params.put("Accept", "application/json");
                        params.put("QK_ACCESS_KEY", getResources().getString(R.string.QK_ACCESS_KEY)+"");

                        mService.invite_employee(params,invite_emailid, message).enqueue(new Callback<InviteQKPojo>() {
                            @Override
                            public void onResponse(Call<InviteQKPojo> call, Response<InviteQKPojo> response) {
                                if (pd != null && pd.isShowing())
                                    pd.dismiss();
                                if (dialog != null && dialog.isShowing())
                                    dialog.dismiss();
                                Log.e("updateSetting-Success", "" + response);
                                if (response != null && response.body() != null) {
                                    InviteQKPojo status200 = response.body();
                                    Utilities.showToast(getApplicationContext(), "" + status200.getMsg());
                                }
                            }

                            @Override
                            public void onFailure(Call<InviteQKPojo> call, Throwable t) {
                                Log.e("updateSetting-Error", "" + t.getLocalizedMessage());
                                if (pd != null && pd.isShowing())
                                    pd.dismiss();
                                if (dialog != null && dialog.isShowing())
                                    dialog.dismiss();
                            }
                        });
                    }
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                setResult(RESULT_OK);
                EmployeeSearch.this.finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }


}
