package com.quickkonnect;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.models.UpgradePlan;
import com.stripe.android.CardUtils;
import com.stripe.android.TokenCallback;
import com.stripe.android.model.Card;
import com.stripe.android.model.Token;
import com.utilities.QKPreferences;
import com.utilities.StripeIntegration;
import com.utilities.Utilities;
import com.volleyapirequest.VolleyApiRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class PaymentDetails extends AppCompatActivity {

    TextView card_details, name, cvv, expiry_date, submit;
    EditText ed_number, ed_name, ed_date, ed_cvv;
    ImageView close;
    String mLastInput = " ", trial = "", Package_Name = "", login_user_id = "", Key_From = "";
    StripeIntegration stripeIntegration;
    boolean isFromTransfer = false;
    private QKPreferences qkPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_details);

        qkPreferences = new QKPreferences(PaymentDetails.this);

        stripeIntegration = StripeIntegration.getInstance(PaymentDetails.this);
        stripeIntegration.initStripe();

        login_user_id = "" + qkPreferences.getValuesInt(QKPreferences.USER_ID);

        try {
            trial = getIntent().getExtras().getString("TRIAL");
            Package_Name = getIntent().getExtras().getString("NAME");
            Key_From = getIntent().getExtras().getString("KEY_FROM");
            if (Key_From != null && Key_From.equalsIgnoreCase("Transfer_Profile")) {
                // from transfer profile only generate the stripe token.
                isFromTransfer = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            Utilities.ChangeStatusBar(PaymentDetails.this);
        } catch (Exception e) {
            e.printStackTrace();
        }

        InitView();
    }

    private void InitView() {

        card_details = findViewById(R.id.tv_card_detail);
        expiry_date = findViewById(R.id.tv_expiry_date);
        cvv = findViewById(R.id.tv_cvv);
        name = findViewById(R.id.tv_card_name);
        ed_number = findViewById(R.id.ed_card_number);
        ed_name = findViewById(R.id.ed_name_card);
        ed_date = findViewById(R.id.ed_expiry_date);
        ed_cvv = findViewById(R.id.ed_cvv);
        submit = findViewById(R.id.tv_next_card_details);
        close = findViewById(R.id.img_card_details_close);

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Utilities.hideKeyboard(PaymentDetails.this);
                PaymentDetails.this.finish();
            }
        });
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Utilities.hideKeyboard(PaymentDetails.this);
                GenerateToken();
            }
        });

        ed_date.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                String input = s.toString();
                SimpleDateFormat formatter = new SimpleDateFormat("MM/yy", Locale.getDefault());
                Calendar expiryDateDate = Calendar.getInstance();
                try {
                    expiryDateDate.setTime(formatter.parse(input));
                    expiry_date.setText(ed_date.getText().toString());
                } catch (Exception e) {
                    if (s.length() == 2 && !mLastInput.endsWith("/")) {
                        int month = Integer.parseInt(input);
                        if (month <= 12) {
                            ed_date.setText(ed_date.getText().toString() + "/");
                            ed_date.setSelection(ed_date.getText().toString().length());
                        }
                    } else if (s.length() == 2 && mLastInput.endsWith("/")) {
                        int month = Integer.parseInt(input);
                        if (month <= 12) {
                            ed_date.setText(ed_date.getText().toString().substring(0, 1));
                            ed_date.setSelection(ed_date.getText().toString().length());
                        } else {
                            ed_date.setText("");
                            ed_date.setSelection(ed_date.getText().toString().length());
                            Toast.makeText(getApplicationContext(), "Enter a valid month", Toast.LENGTH_LONG).show();
                        }
                    } else if (s.length() == 1) {
                        int month = Integer.parseInt(input);
                        if (month > 1) {
                            ed_date.setText("0" + ed_date.getText().toString() + "/");
                            ed_date.setSelection(ed_date.getText().toString().length());
                        }
                    }
                    mLastInput = ed_date.getText().toString();
                    expiry_date.setText(ed_date.getText().toString());
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }
        });

        ed_cvv.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                cvv.setText(ed_cvv.getText().toString());
            }
        });

        ed_name.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                name.setText(ed_name.getText().toString());
            }
        });

        ed_number.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                ed_number.addTextChangedListener(new FourDigitCardFormatWatcher());
                card_details.setText(ed_number.getText().toString());
            }
        });


    }

    private void GenerateToken() {

        String cvv = ed_cvv.getText().toString();
        String expirydate = ed_date.getText().toString();
        String card_num = ed_number.getText().toString();
        int month = 0;
        int year = 0;

       /* if (cvv.isEmpty()) {
            Toast.makeText(this, "Please Enter Valid CVV", Toast.LENGTH_SHORT).show();
        } else if (expirydate.isEmpty()) {
            Toast.makeText(this, "Please Enter Valid Expiration Date", Toast.LENGTH_SHORT).show();
        } else if (card_num.isEmpty()) {
            Toast.makeText(this, "Please Enter Valid Card Number", Toast.LENGTH_SHORT).show();
        } else {*/

        try {
            if (!expirydate.isEmpty()) {
                String dates[] = expirydate.split("/");
                if (dates.length > 0) {
                    month = Integer.parseInt(dates[0]);
                    year = Integer.parseInt(dates[1]);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        Card card = new Card(card_num, month, year, cvv);
        if (!card.validateNumber()) {
            Toast.makeText(this, "Please Enter Valid Card Number", Toast.LENGTH_SHORT).show();
        } else if (!card.validateCVC()) {
            Toast.makeText(this, "Please Enter Valid CVV", Toast.LENGTH_SHORT).show();
        } else if (!card.validateExpiryDate()) {
            Toast.makeText(this, "Please Enter Valid Expiration Date", Toast.LENGTH_SHORT).show();
        } else {
            final ProgressDialog pd = Utilities.showProgress(PaymentDetails.this);
            stripeIntegration.generateStripeToken(card, new TokenCallback() {
                @Override
                public void onError(Exception error) {
                    if (pd != null && pd.isShowing())
                        pd.dismiss();
                    Toast.makeText(PaymentDetails.this, error.getMessage() + "", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onSuccess(Token token) {
                    if (pd != null && pd.isShowing())
                        pd.dismiss();

                    String user_stripe_token = token.getId();
                    qkPreferences.storeUserStripeToken(user_stripe_token);
                    if (isFromTransfer) {
                       /* Intent i = new Intent();
                        i.putExtra("Stripe_Token", user_stripe_token + "");*/
                        setResult(RESULT_OK);
                        PaymentDetails.this.finish();
                    } else {
                        JSONObject j = new JSONObject();
                        try {
                            j.put("id", login_user_id + "");
                            j.put("plan", Package_Name + "");
                            j.put("trial", trial + "");
                            j.put("stripe_token", user_stripe_token);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        CallApiForUpgradePackage(j);
                    }
                }
            });
        }
    }

    public void CallApiForUpgradePackage(JSONObject j) {
        final ProgressDialog pd = Utilities.showProgress(PaymentDetails.this);
        try {
            String url = VolleyApiRequest.REQUEST_BASEURL + "api/web/stripe_checkout";
            JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, url, j, new com.android.volley.Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    if (pd != null && pd.isShowing())
                        pd.dismiss();
                    try {
                        String status = response.getString("status");
                        String msg = response.getString("msg");

                        if (status.equals("200")) {
                            try {
                                JSONObject jsonObject = response.getJSONObject("data");
                                JSONObject PlanDetails = jsonObject.getJSONObject("plan_detail");
                                if (PlanDetails != null) {
                                    String maxEmployee = PlanDetails.getString("max_employee_limit");
                                    //qkPreferences.setValues(QKPreferences.MAX_EMPLOYEE, maxEmployee + "");
                                    qkPreferences.setValues(QKPreferences.MAX_NEW_EMPLOYEE, maxEmployee + "");

                                    String TotalEmployee = jsonObject.getString("added_employees");
                                    qkPreferences.setValues(QKPreferences.TOTAL_EMPLOYEE, TotalEmployee + "");
                                } else {
                                    qkPreferences.setValues(QKPreferences.MAX_NEW_EMPLOYEE, "0");
                                    //qkPreferences.setValues(QKPreferences.TOTAL_EMPLOYEE, "0");
                                }
                                Toast.makeText(PaymentDetails.this, msg + "", Toast.LENGTH_SHORT).show();
                                setResult(RESULT_OK);
                                PaymentDetails.this.finish();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }, new com.android.volley.Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if (pd != null && pd.isShowing())
                        pd.dismiss();
                    Toast.makeText(PaymentDetails.this, "Please try again", Toast.LENGTH_SHORT).show();
                    Log.d("Response", error.getLocalizedMessage() + "");
                    error.printStackTrace();
                }
            }) {
                @Override
                public Map<String, String> getHeaders() {


                    Map<String, String> params = new HashMap<String, String>();
                    params.put("Authorization", "Bearer " + qkPreferences.getApiToken() + "");
                    params.put("Accept", "application/json");
                    params.put("QK_ACCESS_KEY", getResources().getString(R.string.QK_ACCESS_KEY) + "");
                    return params;
                }
            };
            Volley.newRequestQueue(PaymentDetails.this).add(jsonRequest);
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(PaymentDetails.this, "Please try again", Toast.LENGTH_SHORT).show();
        }
    }

    public class FourDigitCardFormatWatcher implements TextWatcher {

        // Change this to what you want... ' ', '-' etc..
        private static final char space = ' ';

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void afterTextChanged(Editable s) {
            // Remove spacing char
            if (s.length() > 0 && (s.length() % 5) == 0) {
                final char c = s.charAt(s.length() - 1);
                if (space == c) {
                    s.delete(s.length() - 1, s.length());
                }
            }
            // Insert char where needed.
            if (s.length() > 0 && (s.length() % 5) == 0) {
                char c = s.charAt(s.length() - 1);
                // Only if its a digit where there should be a space we insert a space
                if (Character.isDigit(c) && TextUtils.split(s.toString(), String.valueOf(space)).length <= 3) {
                    s.insert(s.length() - 1, String.valueOf(space));
                }
            }
        }
    }
}
