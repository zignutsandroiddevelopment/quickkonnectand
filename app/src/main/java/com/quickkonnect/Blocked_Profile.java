package com.quickkonnect;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.activities.BlockedContactsActivity;
import com.activities.MatchProfileActivity;
import com.adapter.BlockProfileListAdapter;
import com.adapter.ContactListAdapter;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.interfaces.BlockedProfile;
import com.interfaces.OnContactClick;
import com.realmtable.BlockEmpList;
import com.realmtable.BlockList;
import com.realmtable.RealmController;
import com.utilities.Constants;
import com.utilities.Utilities;
import com.volleyapirequest.VolleyApiRequest;
import com.volleyapirequest.VolleyCallBack;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.TimeZone;

import io.realm.RealmResults;

public class Blocked_Profile extends AppCompatActivity {

    private Context context;
    RecyclerView recyclerView;
    private static SharedPreferences mSharedPreferences;
    private RecyclerView.LayoutManager mLayoutManager;
    BlockProfileListAdapter adapter;
    String userid;
    SwipeRefreshLayout swipeRefreshLayout;
    ArrayList<PojoClasses.EmployeesList> mlist;
    TextView tv_no_record_found;
    int FROM_NEW_SCAN = 1203;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_blocked_profile);

        Utilities.ChangeStatusBar(Blocked_Profile.this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_blocked_prof);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        context = Blocked_Profile.this;
        getSupportActionBar().setTitle("Blocked Profiles");

        mSharedPreferences = context.getSharedPreferences(SharedPreference.PREF_NAME, 0);
        userid = mSharedPreferences.getString(SharedPreference.CURRENT_USER_ID, "");
        Log.e("UserId", userid + "");

        swipeRefreshLayout = findViewById(R.id.swipeRefreshLayout_prof);
        swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary,
                android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_blue_dark);
        tv_no_record_found = (TextView) findViewById(R.id.tv_no_record_found_Blocked_prof);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView_Blocked_prof);
        recyclerView.addItemDecoration(new SimpleDividerItemDecoration(context));
        recyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(mLayoutManager);

        if (Utilities.isNetworkConnected(Blocked_Profile.this))
            getBlockedList();
        else
            getList();

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getBlockedList();
            }
        });
    }

    private void getBlockedList() {
        swipeRefreshLayout.setRefreshing(true);
        JSONObject jobj = new JSONObject();
        try {
            jobj.put("user_id", userid + "");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        new VolleyApiRequest(context, false, VolleyApiRequest.REQUEST_BASEURL + "api/member/block_profile_list",
                new VolleyCallBack() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("Userid", userid + "");
                        swipeRefreshLayout.setRefreshing(false);
                        parseResponse(response);
                    }

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyApiRequest.showVolleyError(context, error);
                    }
                }).enqueRequest(jobj, Request.Method.POST);
    }

    public void parseResponse(JSONObject response) {
        try {
            String responseStatus = response.getString("status");
            String msg = response.getString("msg");
            RealmController.with(Blocked_Profile.this).clearAllBlockedEmpoyee();

            if (responseStatus.equals("200")) {
                try {
                    JSONArray jsnArr = response.getJSONArray("data");
                    recyclerView.setVisibility(View.VISIBLE);
                    mlist = new ArrayList<PojoClasses.EmployeesList>();
                    for (int i = 0; i < jsnArr.length(); i++) {
                        JSONObject obj = jsnArr.getJSONObject(i);
                        JSONObject user = obj.getJSONObject("user");
                        JSONObject profile = obj.getJSONObject("profile");

                        BlockEmpList bl = new BlockEmpList();
                        bl.setId(obj.getString("id"));
                        bl.setProfile_id(obj.getString("profile_id"));
                        bl.setUser_id(obj.getString("user_id"));
                        bl.setEmployee_no(obj.getString("employee_no"));
                        bl.setValid_from(obj.getString("valid_from"));
                        bl.setValid_to(obj.getString("valid_to"));
                        bl.setDesignation(obj.getString("designation"));
                        bl.setDepartment(obj.getString("department"));
                        bl.setStatus(obj.getString("status"));
                        bl.setReason(obj.getString("reason"));
                        bl.setUser1(user.toString());
                        bl.setProfile1(profile.toString());

                        RealmController.with(Blocked_Profile.this).createUpdateBlockedEmp(bl);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                getList();

            } else {
                recyclerView.setVisibility(View.GONE);
                tv_no_record_found.setVisibility(View.VISIBLE);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void getList() {
        RealmResults<BlockEmpList> blockLists = RealmController.with(Blocked_Profile.this).getBlockEmp();
        if (blockLists != null && blockLists.size() > 0) {
            mlist = new ArrayList<>();
            for (BlockEmpList bl : blockLists) {
                mlist.add(new PojoClasses.EmployeesList(
                        bl.getId(),
                        bl.getProfile_id(),
                        bl.getUser_id(),
                        bl.getEmployee_no(),
                        bl.getValid_from(),
                        bl.getValid_to(),
                        bl.getDesignation(),
                        bl.getDepartment(),
                        bl.getStatus(),
                        bl.getReason(),
                        bl.getUser1(),
                        bl.getProfile1()));
            }
            adapter = new BlockProfileListAdapter(Blocked_Profile.this, mlist, blockedProfile);
            recyclerView.setAdapter(adapter);
        } else {
            getBlockedList();
        }
    }

    BlockedProfile blockedProfile = new BlockedProfile() {
        @Override
        public void onBlockedProfile(int pos, PojoClasses.EmployeesList contactsList, String profile, String user) {
            try {
                PojoClasses.EmployeesList.Profile userlist = new Gson().fromJson(contactsList.getProfile1(), PojoClasses.EmployeesList.Profile.class);
                PojoClasses.EmployeesList.Profile profilelist = new Gson().fromJson(contactsList.getProfile1(), PojoClasses.EmployeesList.Profile.class);
                Intent intent = new Intent(context, NewScanProfileActivity.class);
                //intent.putExtra("unique_code", "" + modelNotifications.getUniqueCode());
                intent.putExtra("user_id", "" + profilelist.getId());
                //intent.putExtra("notification_id", "" + modelNotifications.getNotificationId());
                intent.putExtra(Constants.KEY, Constants.KEY_FROM_BLOCK_PROFILE);
                startActivityForResult(intent, FROM_NEW_SCAN);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK && requestCode == FROM_NEW_SCAN) {
            getBlockedList();
        }
    }
}
