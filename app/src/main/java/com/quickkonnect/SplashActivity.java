package com.quickkonnect;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;

import com.activities.ConfirmRegistration;
import com.crashlytics.android.Crashlytics;
import com.models.countries.CountryPojo;
import com.models.countries.Datum;
import com.models.logindata.ModelLoggedUser;
import com.realmtable.Country;
import com.realmtable.RealmController;
import com.retrofitservice.ApiUtils;
import com.retrofitservice.SOService;
import com.services.BackgroundService;
import com.utilities.Foreground;
import com.utilities.QKPreferences;
import com.utilities.Utilities;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import io.fabric.sdk.android.Fabric;
import io.realm.Realm;
import io.realm.RealmResults;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SplashActivity extends AppCompatActivity {

    private QKPreferences qkPreferences;
    private SOService mService;
    private Handler handler;
    private Context context;
    private static int REQUEST_WRITE_EXTERNAL_STORAGE = 1;
    private static int REQUEST_READ_EXTERNAL_STORAGE = 2;
    //TextView version;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        //Utilities.changeStatusbar(SplashActivity.this, getWindow());
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_splash);
        context = SplashActivity.this;

        try {
            // for User analytics
            Foreground.init(getApplication());
            startService(new Intent(getApplicationContext(), BackgroundService.class));
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            File images_folder = new File(Environment.getExternalStorageDirectory() +
                    File.separator + "/Quickkonnect/Screenshots");
            if (!images_folder.exists()) {
                images_folder.mkdirs();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        //version = findViewById(R.id.tv_splash_version);

        handler = new Handler();

        mService = ApiUtils.getSOService();

        qkPreferences = new QKPreferences(SplashActivity.this);

//        String versionname = BuildConfig.VERSION_NAME;
        //version.setText("Version : " + versionname + "");

        /*try {
            // Add user to firebase and update user details
            String UniqueCode = "" + qkPreferences.getValues(QKPreferences.USER_UNIQUECODE);
            String user_profilepic = qkPreferences.getValues(QKPreferences.USER_PROFILEIMG);
            String name = qkPreferences.getValues(QKPreferences.USER_FULLNAME);
            String userid = "" + qkPreferences.getValuesInt(QKPreferences.USER_ID);
            String token = FirebaseInstanceId.getInstance().getToken() + "";
            if (!UniqueCode.equalsIgnoreCase("") && !UniqueCode.equalsIgnoreCase("null")) {
                ChatUserModel cum = new ChatUserModel(name, user_profilepic, "1", userid, token);
                ChatConfig.addUser(SplashActivity.this, UniqueCode, cum);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }*/

        try {
            // store user start time
            String date = Calendar.getInstance().getTimeInMillis() + "";
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String newdate = formatter.format(new Date(Long.parseLong(date + "")));
            qkPreferences.storeStartTime(newdate + "");
        } catch (Exception e) {
            e.printStackTrace();
        }
        requestPermission();

        Utilities.generateHashkey(SplashActivity.this);
    }

    public void requestPermission() {
        if (ContextCompat.checkSelfPermission(SplashActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(SplashActivity.this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    REQUEST_WRITE_EXTERNAL_STORAGE);

        } else if (ContextCompat.checkSelfPermission(SplashActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(SplashActivity.this,
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                    REQUEST_READ_EXTERNAL_STORAGE);

        } else {

            try {
                RealmResults<Country> countries = RealmController.with(SplashActivity.this).getCountry();
                Log.e("Country Size", "" + countries.size());
                if (countries == null || countries.size() == 0) {
                    if (Utilities.isNetworkConnected(context))
                        functionGetCountry();
                    else
                        runHandler();
                } else
                    runHandler();
            } catch (Exception e) {
                e.printStackTrace();
                runHandler();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        requestPermission();
    }

    public void runHandler() {

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                redirecttoActivity();
            }
        }, 2200);
    }

    public void functionGetCountry() {
        mService.getCountries().enqueue(new Callback<CountryPojo>() {
            @Override
            public void onResponse(Call<CountryPojo> call, Response<CountryPojo> response) {
                Log.e("Country-Success", "" + response.body());
                try {

                    if (response != null && response.body() != null) {
                        CountryPojo countryPojo = response.body();
                        if (countryPojo != null && countryPojo.getStatus() == 200) {
                            if (countryPojo.getData() != null && countryPojo.getData().size() > 0) {

                                for (Datum datum: countryPojo.getData()) {

                                    Country country = new Country();

                                    country.setCountry_id("" + datum.getCode());
                                    country.setCountry_name(datum.getName());

                                    Realm realm = Realm.getDefaultInstance();
                                    realm.beginTransaction();
                                    realm.copyToRealm(country);
                                    realm.commitTransaction();
                                }

                                redirecttoActivity();

                            } else redirecttoActivity();

                        } else redirecttoActivity();

                    } else redirecttoActivity();

                } catch (Exception e) {
                    Log.e("Country-Error", "" + e.getMessage());
                    redirecttoActivity();
                }
            }

            @Override
            public void onFailure(Call<CountryPojo> call, Throwable t) {
                Log.e("Country-Error", "" + t.getLocalizedMessage());
                redirecttoActivity();
            }
        });
    }

    public void redirecttoActivity() {

        ModelLoggedUser modelLoggedUser = qkPreferences.getLoggedUser();
        if (modelLoggedUser == null || modelLoggedUser.getData() == null || modelLoggedUser.getData().getId() == null || modelLoggedUser.getData().getId().equals("")) {

            startActivity(new Intent(SplashActivity.this, Introduction_Activity.class));
            SplashActivity.this.finish();

        } else {
            GoAhead(modelLoggedUser);

//            try {
//                if (modelLoggedUser.getData().getFacebook() != null && modelLoggedUser.getData().getFacebook().getStatus() == 1) {
//                    if (modelLoggedUser.getData().getFacebook().getData().getPublicProfileUrl() != null && !modelLoggedUser.getData().getFacebook().getData().getPublicProfileUrl().equalsIgnoreCase("") && !modelLoggedUser.getData().getFacebook().getData().getPublicProfileUrl().equalsIgnoreCase("null")) {
//                        GoAhead(modelLoggedUser);
//                    } else {
//                        Intent i = new Intent(SplashActivity.this, Introduction_Activity.class);
//                        startActivity(i);
//                        SplashActivity.this.finish();
//                    }
//                } else {
//                    GoAhead(modelLoggedUser);
//                }
//
//            } catch (Exception e) {
//                GoAhead(modelLoggedUser);
//                e.printStackTrace();
//            }
        }
    }

    private void GoAhead(ModelLoggedUser modelLoggedUser) {
        try {

            String qk_tag_info = "" + modelLoggedUser.getData().getQrCode();

            if (qk_tag_info != null && Utilities.isEmpty(qk_tag_info)) {
                startActivity(new Intent(SplashActivity.this, DashboardActivity.class));

            } else {

                if (modelLoggedUser.getData().getStatus() == 1) {

                    Intent intent = new Intent(context, AddSocialMediaAccountActivity.class);
                    intent.putExtra("from", "other");
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);

                } else if (modelLoggedUser.getData().getStatus() == 2
                        || modelLoggedUser.getData().getStatus() == 3) {

                    Intent intent = new Intent(context, ConfirmRegistration.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);

                } else {

                    Intent intent = new Intent(context, Introduction_Activity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                }
            }

            SplashActivity.this.finish();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
