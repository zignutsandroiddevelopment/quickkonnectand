package com.quickkonnect;

import android.Manifest;
import android.animation.Animator;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.RequiresApi;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.activities.MatchProfileActivity;
import com.adapter.NewScanViewPagerAdapter;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.fragments.NewScanFragment;
import com.fragments.ScanFragSetting;
import com.google.gson.Gson;
import com.models.createprofile.CreateProfileData;
import com.models.createprofile.ModelCreateProfile;
import com.realmtable.MyProfiles;
import com.realmtable.RealmController;
import com.services.LoadProfiles;
import com.squareup.picasso.Picasso;
import com.utilities.CompressImage;
import com.utilities.Constants;
import com.utilities.QKPreferences;
import com.utilities.Utilities;
import com.volleyapirequest.VolleyApiRequest;
import com.volleyapirequest.VolleyCallBack;
import com.yalantis.ucrop.UCrop;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Calendar;
import java.util.Objects;
import java.util.TimeZone;

import io.reactivex.annotations.NonNull;

public class NewScanProfileActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private TabLayout tabLayout;
    public ViewPager viewPager;
    private LinearLayout lin_acc_reject;
    public ImageView img_company_logo_main, img_company_logo, image_edit_new_scan, img_block_new_scan, img_follow_new_scan;
    public TextView tv_company_name, tv_about, accept_profile, reject_profile, tv_follow_submsg, tv_follower_msg, tv_followedon_msg;
    private Context context;
    private QKPreferences qkPreferences;
    private String login_user_id, profile_user_id = "";
    private String unique_code, user_id, key_from = "", profile_pic = "", notification_id = "",
            role = "", role_name = "", FollowedAt = "", strBase64 = "", profile_id = "";
    private boolean isAcceptReject = false, isFromScan = false, isFromScan_Contact = false, is_from_global_search = false;
    public static boolean isEditinfo = false;
    public static NewScanViewPagerAdapter adapter;
    public static int mYear, mMonth, mDay;
    public static Calendar c;
    public static File file;
    public EditText edt_name, edt_about;
    private Uri fileUri;
    String[] title;
    static final int REQUEST_STORAGE_READ_ACCESS_PERMISSION = 6;
    private static final int REQUEST_SELECT_PICTURE = 100;
    private static final int PICK_FROM_CAMERA = 101;
    public Bitmap selectedImage = null;
    private BottomSheetDialog bottomSheetDialog;
    private boolean isAlreadyFollowed = false;
    private Menu menu;
    private boolean isTabLoded = false;

    public static String Employee_Unique_Code = "";
    public static String Profile_Type = "";

    private int SUBSCRIBE_PACKAGE = 1415;

    //for Employer scan
    private String company_id = "0", Main_About_Text = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.e("Click Activity", "Start" + Calendar.getInstance().getTime());
        //requestWindowFeature(Window.FEATURE_NO_TITLE);
        //getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        //this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_new_scan_profile);
        context = NewScanProfileActivity.this;

        if (Build.VERSION.SDK_INT >= 19 && Build.VERSION.SDK_INT < 21) {
            setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, true);
        }
        if (Build.VERSION.SDK_INT >= 19) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }
        if (Build.VERSION.SDK_INT >= 21) {
            setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, false);
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }

        toolbar = findViewById(R.id.toolbar_new_scan);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close_white_24dp);
        getSupportActionBar().setTitle("");

        qkPreferences = new QKPreferences(context);

        login_user_id = "" + qkPreferences.getValuesInt(QKPreferences.USER_ID);

        key_from = getIntent().getExtras().getString(Constants.KEY);

        try {
            unique_code = getIntent().getExtras().getString("unique_code");
            user_id = getIntent().getExtras().getString("user_id");
            profile_id = getIntent().getExtras().getString("profile_id");

            qkPreferences.storeProfileID(profile_id + "");

            company_id = getIntent().getExtras().getString("company_id");

        } catch (Exception e) {
            e.printStackTrace();
        }

        initWidget();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                try {
                    startService(new Intent(getApplicationContext(), LoadProfiles.class));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, 2000);
    }

    public static void setWindowFlag(Activity activity, final int bits, boolean on) {
        Window win = activity.getWindow();
        WindowManager.LayoutParams winParams = win.getAttributes();
        if (on) {
            winParams.flags |= bits;
        } else {
            winParams.flags &= ~bits;
        }
        win.setAttributes(winParams);
    }

    public void initWidget() {

        img_block_new_scan = findViewById(R.id.img_block_new_scan);
        tv_followedon_msg = findViewById(R.id.tv_followedon_msg);
        tv_followedon_msg.setVisibility(View.GONE);
        tv_follower_msg = findViewById(R.id.tv_follower_msg);
        tv_follow_submsg = findViewById(R.id.tv_follow_submsg);
        img_follow_new_scan = findViewById(R.id.img_follow_new_scan);
        img_follow_new_scan.setVisibility(View.GONE);
        img_block_new_scan.setVisibility(View.GONE);
        tv_follower_msg.setVisibility(View.GONE);
        tv_follow_submsg.setVisibility(View.GONE);

        img_company_logo_main = findViewById(R.id.img_company_logo_new_scan);
        tv_company_name = findViewById(R.id.tv_company_name_new_scan);
        tv_about = findViewById(R.id.tv_about_new_scan);
        viewPager = findViewById(R.id.viewpager_new_scan);

        lin_acc_reject = findViewById(R.id.lin_acc_reject);
        accept_profile = findViewById(R.id.tv_profile_accept_transfer);
        reject_profile = findViewById(R.id.tv_profile_reject_transfer);
        image_edit_new_scan = findViewById(R.id.image_edit_new_scan);
        image_edit_new_scan.setVisibility(View.GONE);

        accept_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CheckSubscription();
            }
        });

        reject_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AcceptRejectTransferProfile("R", "");
            }
        });

        image_edit_new_scan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showeditBasicCompanyinfo1();
            }
        });

        img_block_new_scan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(NewScanProfileActivity.this);
                String msg = "You want to unblock this profile?";
                builder.setMessage(msg);
                builder.setTitle("Are you sure?");
                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                        UnblockPorifle();
                    }
                });
                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                builder.show();
            }
        });

        MyProfiles myProfiles = RealmController.with(NewScanProfileActivity.this).getMyProfielDataByProfileId(unique_code);
        /*if (myProfiles != null) {
            SetupForTablayoutWithRole(myProfiles.getRole(), myProfiles.getRole_name());
            isTabLoded = true;
        }*/

        if (key_from != null && key_from.equals(Constants.KEY_SCANQK)) {

            if (myProfiles != null)
                showDataOffline(true);

            else if (login_user_id != null && Utilities.isNetworkConnected(NewScanProfileActivity.this))
                fnScanUser(unique_code, false);

        }
        // todo remove if not use
        else if (key_from != null && key_from.equals(Constants.KEY_FROM_NOTIFICATIONS)) {
            getOtherUserData(profile_id, false);
        } else if (key_from != null && key_from.equals(Constants.KEY_GLOBAL_SEARCH)) {
            if (user_id != null && login_user_id != null && Objects.equals(user_id, login_user_id)) {
                showDataOffline(true);
            } else {
                getOtherUserData(profile_id, false);
            }
        } else {
            if (myProfiles != null)
                showDataOffline(true);
            else
                getOtherUserData(profile_id, false);
        }

        /*tv_about.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    if (tv_about.getText().toString().length() > 90) {
                        AlertDialog alertDialog = new AlertDialog.Builder(NewScanProfileActivity.this).create();
                        alertDialog.setMessage(tv_about.getText().toString() + "");
                        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });
                        alertDialog.show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });*/
    }

    public void fnScanUser(String unique_code, boolean isLoadershow) {

        try {
            if (Utilities.isNetworkConnected(NewScanProfileActivity.this)) {
                JSONObject obj = new JSONObject();
                try {
                    obj.put("unique_code", unique_code);
                    obj.put("user_id", login_user_id);
                    obj.put("timezone", "" + TimeZone.getDefault().getID());
                    obj.put("latitude", qkPreferences.getLati());
                    obj.put("longitude", qkPreferences.getLongi());
                    obj.put("device_type", "Android");
                    obj.put("company_id", company_id + "");
                    String model = Build.MODEL;
                    int version = Build.VERSION.SDK_INT;
                    if (model.isEmpty())
                        obj.put("model", "z-404");
                    else
                        obj.put("model", model + "");
                    obj.put("version", version + "");
                } catch (Exception e) {
                    e.printStackTrace();
                }
                new VolleyApiRequest(NewScanProfileActivity.this, isLoadershow, VolleyApiRequest.REQUEST_BASEURL + "api/scan_user_store",
                        new VolleyCallBack() {
                            @Override
                            public void onResponse(JSONObject response) {
                                parseResponse(response);
                            }

                            @Override
                            public void onErrorResponse(VolleyError error) {
                                VolleyApiRequest.showVolleyError(NewScanProfileActivity.this, error);
                            }
                        }).enqueRequest(obj, Request.Method.POST);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getOtherUserData(String profile_id, boolean isLoadershow) {
        try {
            if (Utilities.isNetworkConnected(NewScanProfileActivity.this)) {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("profile_id", profile_id);
                jsonObject.put("user_id", login_user_id);
                new VolleyApiRequest(NewScanProfileActivity.this, isLoadershow, VolleyApiRequest.REQUEST_BASEURL + "api/profile/profile_view",
                        new VolleyCallBack() {
                            @Override
                            public void onResponse(JSONObject response) {
                                parseResponse(response);
                            }

                            @Override
                            public void onErrorResponse(VolleyError error) {
                                VolleyApiRequest.showVolleyError(NewScanProfileActivity.this, error);
                            }
                        }).enqueRequest(jsonObject, Request.Method.POST);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void parseResponse(JSONObject response) {

        try {
            ModelCreateProfile modelCreateProfile = new Gson().fromJson(response.toString(), ModelCreateProfile.class);

            if (modelCreateProfile.getStatus() == 200) {

                if (modelCreateProfile.getData() != null) {

                    CreateProfileData createProfileData = modelCreateProfile.getData();

                    qkPreferences.storeProfileID(createProfileData.getId() + "");

                    MyProfiles myProfiles = new MyProfiles();

                    profile_user_id = createProfileData.getUserId() + "";

                    myProfiles.setId("" + createProfileData.getId());
                    myProfiles.setUser_id("" + createProfileData.getUserId());
                    myProfiles.setType("" + createProfileData.getType());
                    myProfiles.setName(createProfileData.getName());
                    myProfiles.setAddress(createProfileData.getAddress());
                    myProfiles.setLatitude(createProfileData.getLatitude());
                    myProfiles.setLongitude(createProfileData.getLongitude());
                    myProfiles.setTag("" + createProfileData.getTag());
                    myProfiles.setSub_tag("" + createProfileData.getSubTag());
                    myProfiles.setLogo(createProfileData.getLogo());
                    myProfiles.setAbout(createProfileData.getAbout());
                    myProfiles.setStatus(createProfileData.getStatus());
                    myProfiles.setCompany_team_size("" + createProfileData.getCompanyTeamSize());
                    myProfiles.setEstablish_date(createProfileData.getEstablishDate());
                    myProfiles.setUnique_code(createProfileData.getUniqueCode());
                    myProfiles.setCurrent_ceo(createProfileData.getCurrent_ceo());
                    myProfiles.setCurrent_cfo(createProfileData.getCurrent_cfo());
                    myProfiles.setDob(createProfileData.getDob());
                    myProfiles.setRole(createProfileData.getRole());
                    myProfiles.setRole_name(createProfileData.getRole_name());
                    myProfiles.setIs_transfer(createProfileData.getIs_transfer());
                    myProfiles.setTransfer_status(createProfileData.getTransfer_status());
                    myProfiles.setTransfer_status(createProfileData.getTransfer_status());
                    myProfiles.setProfile_view_code(createProfileData.getProfile_view_code() + "");
                    myProfiles.setIs_follower(createProfileData.getIs_follower() + "");

                    Employee_Unique_Code = createProfileData.getEmployee_unique_code() + "";
                    Profile_Type = createProfileData.getType() + "";

                    String social_media = new Gson().toJson(createProfileData.getSocialMedia());
                    myProfiles.setSocial_media(social_media);

                    String social_phone = new Gson().toJson(createProfileData.getPhone());
                    myProfiles.setPhone(social_phone);

                    String social_email = new Gson().toJson(createProfileData.getEmail());
                    myProfiles.setEmail(social_email);

                    String otheraddress = new Gson().toJson(createProfileData.getOtherAddress());
                    myProfiles.setOtheraddress(otheraddress);

                    String social_weblink = new Gson().toJson(createProfileData.getWeblink());
                    myProfiles.setWeblink(social_weblink);

                    String social_founder = new Gson().toJson(createProfileData.getFounders());
                    myProfiles.setFounders(social_founder);

                    RealmController.with(NewScanProfileActivity.this).updateMyProfiles(myProfiles);

                    try {

                        Fragment fragment = new ScanFragSetting();

                        ScanFragSetting frg = (ScanFragSetting) fragment;

                        frg.getData(myProfiles);

                        qkPreferences.storeIsTransfer(myProfiles.getIs_transfer() + "");
                        qkPreferences.storeTransfer_status(myProfiles.getTransfer_status() + "");
                        qkPreferences.storeStatus(myProfiles.getStatus() + "");

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    showDataOffline(false);
                }
            } else {
                Utilities.showToast(NewScanProfileActivity.this, modelCreateProfile.getMsg());
                if (modelCreateProfile.getStatus() == 400 && modelCreateProfile.getMsg().equalsIgnoreCase("Profile View not available")) {
                    NewScanProfileActivity.this.finish();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void changeTabsFont() {
        try {
            ViewGroup vg = (ViewGroup) tabLayout.getChildAt(0);
            int tabsCount = vg.getChildCount();
            for (int j = 0; j < tabsCount; j++) {
                ViewGroup vgTab = (ViewGroup) vg.getChildAt(j);
                int tabChildsCount = vgTab.getChildCount();
                for (int i = 0; i < tabChildsCount; i++) {
                    View tabViewChild = vgTab.getChildAt(i);
                    if (tabViewChild instanceof TextView) {
                        ((TextView) tabViewChild).setAllCaps(false);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    String tabcode = "";

    public void showDataOffline(final boolean isRealodData) {
        try {
            final MyProfiles myProfiles = RealmController.with(NewScanProfileActivity.this).getMyProfielDataByProfileId(unique_code);

            if (myProfiles != null) {

                Employee_Unique_Code = myProfiles.getEmployee_unique_code() + "";
                Profile_Type = myProfiles.getType() + "";

                SetupForTablayoutWithRole(myProfiles.getRole(), myProfiles.getRole_name());

                /*if (!isTabLoded) {
                    if (myProfiles.getRole().equalsIgnoreCase("0") || myProfiles.getRole().equalsIgnoreCase("4")) {
                        if (myProfiles.getType() != null && !myProfiles.getType().equalsIgnoreCase("") && !myProfiles.getType().equalsIgnoreCase("null")) {
                            getTabLayout(myProfiles, myProfiles.getType(), myProfiles.getStatus());
                            isTabLoded = true;
                        }
                    } else {
                        if (myProfiles.getType() != null && !myProfiles.getType().equalsIgnoreCase("") && !myProfiles.getType().equalsIgnoreCase("null")) {
                            getTabForRole(myProfiles);
                            isTabLoded = true;
                        }
                    }
                }*/

                if (myProfiles.getProfile_view_code() != null && !myProfiles.getProfile_view_code().equalsIgnoreCase("null") && !myProfiles.getProfile_view_code().equalsIgnoreCase("")) {
                    if (!tabcode.equalsIgnoreCase(myProfiles.getProfile_view_code()))
                        getTabFromApi(myProfiles.getProfile_view_code() + "", myProfiles, myProfiles.getType(), myProfiles.getStatus());
                    tabcode = myProfiles.getProfile_view_code() + "";
                } else {
                    if (!isTabLoded) {
                        if (myProfiles.getRole().equalsIgnoreCase("0") || myProfiles.getRole().equalsIgnoreCase("4")) {
                            if (myProfiles.getType() != null && !myProfiles.getType().equalsIgnoreCase("") && !myProfiles.getType().equalsIgnoreCase("null")) {
                                getTabLayout(myProfiles, myProfiles.getType(), myProfiles.getStatus());
                                isTabLoded = true;
                            }
                        } else {
                            if (myProfiles.getType() != null && !myProfiles.getType().equalsIgnoreCase("") && !myProfiles.getType().equalsIgnoreCase("null")) {
                                getTabForRole(myProfiles);
                                isTabLoded = true;
                            }
                        }
                    }
                }

                try {
                    if (myProfiles.getIs_follower() != null && myProfiles.getIs_follower().equalsIgnoreCase("1")) {
                        // already followed
                        isAlreadyFollowed = true;
                    } else {
                        isAlreadyFollowed = false;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                if (key_from.equalsIgnoreCase(Constants.KEY_SCANQK_CONTACT)) {
                    isFromScan_Contact = true;
                    FollowedAt = getIntent().getExtras().getString("FollowedAt");

                } else if (key_from.equalsIgnoreCase(Constants.KEY_GLOBAL_SEARCH)) {
                    //} else if (key_from.equalsIgnoreCase(Constants.KEY_SCANQK_GLOBAL_SEARCH)) {
                    if (user_id != null && login_user_id != null && Objects.equals(user_id, login_user_id)) {
                    } else {
                        is_from_global_search = true;
                        isEditinfo = false;
                    }


                } else if (key_from.equalsIgnoreCase(Constants.KEY_SCANQK_CONTACT)) {
                    isEditinfo = false;
                    isFromScan_Contact = true;
                    FollowedAt = getIntent().getExtras().getString("FollowedAt");
                }

                if (isAcceptReject)
                    lin_acc_reject.setVisibility(View.VISIBLE);
                else
                    lin_acc_reject.setVisibility(View.GONE);

                try {
                    MenuInflater inflater = getMenuInflater();
                    inflater.inflate(R.menu.new_save_menu, menu);
                    MenuItem item_save = menu.findItem(R.id.ic_save_update_new_save);

                    if (isEditinfo) {
                        item_save.setVisible(true);
                    } else {
                        item_save.setVisible(false);

                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

                FollowUnFollowFunc();

                // Name
                if (myProfiles.getName() != null) {
                    tv_company_name.setText(myProfiles.getName());
                } else
                    tv_company_name.setVisibility(View.GONE);

                String msg = "You just started following " + myProfiles.getName();
                tv_follower_msg.setText(msg + "");

                // About
                if (myProfiles.getAbout() != null) {
                    tv_about.setText(myProfiles.getAbout());
                    Main_About_Text = myProfiles.getAbout();
                } else {
                    tv_about.setVisibility(View.GONE);
                    Main_About_Text = "";
                }

                try {
                    //makeTextViewResizable(tv_designation, 6, "Read More", true);
                    //String text = tv_about.getText().toString();
                    String text = Main_About_Text + "";
                    Utilities.addReadMore(NewScanProfileActivity.this, text, tv_about);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                profile_id = myProfiles.getId() + "";

                // Company Logo
                profile_pic = myProfiles.getLogo();

                try {

                    if (profile_pic != null && Utilities.isEmpty(profile_pic)) {
                        Picasso.with(NewScanProfileActivity.this)
                                .load(profile_pic)
                                .error(R.drawable.ic_logo)
                                .transform(new CircleTransform())
                                .placeholder(R.drawable.ic_logo)
                                .fit()
                                .into(img_company_logo_main);
                    } else {
                        img_company_logo_main.setImageResource(R.drawable.ic_logo);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

                /*try {
                    Fragment fragment = adapter.getItem(0);
                    if (fragment instanceof NewScanFragment) {
                        NewScanFragment newScanFragment = (NewScanFragment) fragment;
                        newScanFragment.showData(myProfiles);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }*/

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Log.e("100", "1");
                        /*if (myProfiles.getRole().equalsIgnoreCase("0")) {
                            Log.e("100", "2");

                            if (myProfiles.getType() != null && !myProfiles.getType().equalsIgnoreCase("") && !myProfiles.getType().equalsIgnoreCase("null"))
                                getTabLayout(myProfiles, myProfiles.getType(), myProfiles.getStatus());

                        } else {
                            Log.e("100", "3");
                            if (myProfiles.getType() != null && !myProfiles.getType().equalsIgnoreCase("") && !myProfiles.getType().equalsIgnoreCase("null"))
                                getTabForRole(myProfiles);
                        }*/

                        if (isRealodData) {
                            if (key_from != null && key_from.equals(Constants.KEY_SCANQK))
                                fnScanUser(unique_code, false);
                            else
                                getOtherUserData(profile_id, false);
                        }
                    }
                }, 500);
            } else {
                Utilities.showToast(NewScanProfileActivity.this, "No internet connection, please try after some time.");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void SetupForTablayoutWithRole(String Role, String Role_name) {
        try {
            role = Role + "";
            role_name = Role_name + "";

            if (Role != null && !Role.equalsIgnoreCase("") && !Role.equalsIgnoreCase("null")) {

                if (Role.equalsIgnoreCase("0") || Role.equalsIgnoreCase("4")) {

                    if (key_from.equals(Constants.KEY_FROM_MANAGEPROFILES)) {
                        isEditinfo = true;

                    } else if (key_from.equals(Constants.KEY_FROM_NOTIFICATIONS)) {
                        isAcceptReject = true;

                        //todo remove this if not necessary
                        isEditinfo = false;

                        try {
                            notification_id = getIntent().getExtras().getString("notification_id");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    } else if (key_from.equalsIgnoreCase(Constants.KEY_FROM_BLOCK_PROFILE)) {

                        isAcceptReject = false;
                        isEditinfo = false;
                        img_block_new_scan.setVisibility(View.VISIBLE);

                    } else if (key_from.equalsIgnoreCase(Constants.KEY_SCANQK)) {
                        isFromScan = true;
                        //todo remove it added on 6-7-2018
                        isEditinfo = false;

                    } else if (key_from.equalsIgnoreCase(Constants.KEY_GLOBAL_SEARCH)) {

                        if (user_id != null && login_user_id != null && Objects.equals(user_id, login_user_id)) {
                            isEditinfo = true;
                        } else {
                            isEditinfo = false;
                        }
                        //isEditinfo = false;
                    }
                    //todo remove elseif  it added on 6-7-2018
                    else if (key_from.equalsIgnoreCase(Constants.KEY_SCANQK_CONTACT)) {
                        isEditinfo = false;
                        try {
                            isFromScan_Contact = true;
                            FollowedAt = getIntent().getExtras().getString("FollowedAt");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                } else {

                    if (Role.equalsIgnoreCase("2") || Role.equalsIgnoreCase("3")) {
                        isEditinfo = true;
                    } else {
                        isEditinfo = false;
                    }
                }

            } else {

                if (key_from.equals(Constants.KEY_FROM_MANAGEPROFILES)) {
                    isEditinfo = true;

                } else if (key_from.equals(Constants.KEY_FROM_NOTIFICATIONS)) {
                    isAcceptReject = true;
                    try {
                        notification_id = getIntent().getExtras().getString("notification_id");
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }

                } else if (key_from.equalsIgnoreCase(Constants.KEY_FROM_BLOCK_PROFILE)) {
                    isAcceptReject = false;
                    isEditinfo = false;
                    img_block_new_scan.setVisibility(View.VISIBLE);

                } else if (key_from.equalsIgnoreCase(Constants.KEY_SCANQK)) {
                    isFromScan = true;
                }
            }

        } catch (Exception e) {
            e.printStackTrace();

            if (key_from.equals(Constants.KEY_FROM_MANAGEPROFILES)) {
                isEditinfo = true;

            } else if (key_from.equals(Constants.KEY_FROM_NOTIFICATIONS)) {
                isAcceptReject = true;
                try {
                    notification_id = getIntent().getExtras().getString("notification_id");
                } catch (Exception e1) {
                    e1.printStackTrace();
                }

            } else if (key_from.equalsIgnoreCase(Constants.KEY_FROM_BLOCK_PROFILE)) {
                isAcceptReject = false;
                isEditinfo = false;
                img_block_new_scan.setVisibility(View.VISIBLE);

            } else if (key_from.equalsIgnoreCase(Constants.KEY_SCANQK)) {

                isFromScan = true;
            }
        }
    }

    private void FollowUnFollowFunc() {

        if (isFromScan) {
            img_follow_new_scan.setVisibility(View.VISIBLE);
            tv_follower_msg.setVisibility(View.VISIBLE);
            tv_follow_submsg.setVisibility(View.VISIBLE);
            tv_follow_submsg.setText("Unfollow");
        } else {
            img_follow_new_scan.setVisibility(View.GONE);
            tv_follower_msg.setVisibility(View.GONE);
            tv_follow_submsg.setVisibility(View.GONE);
        }

        if (isFromScan_Contact) {
            img_follow_new_scan.setVisibility(View.VISIBLE);
            tv_follower_msg.setVisibility(View.GONE);
            tv_followedon_msg.setVisibility(View.VISIBLE);
            tv_followedon_msg.setText("Followed on : " + FollowedAt);
            tv_follow_submsg.setVisibility(View.VISIBLE);
            tv_follow_submsg.setText("Unfollow");
        }

        if (is_from_global_search) {
            if (isAlreadyFollowed) {
                img_follow_new_scan.setVisibility(View.VISIBLE);
                tv_follower_msg.setVisibility(View.GONE);
                img_follow_new_scan.setImageResource(R.drawable.ic_unfollow);
                tv_followedon_msg.setVisibility(View.GONE);
                tv_follow_submsg.setVisibility(View.VISIBLE);
                tv_follow_submsg.setText("Unfollow");
            } else {
                img_follow_new_scan.setVisibility(View.VISIBLE);
                tv_follower_msg.setVisibility(View.GONE);
                img_follow_new_scan.setImageResource(R.drawable.ic_follow);
                tv_followedon_msg.setVisibility(View.GONE);
                tv_follow_submsg.setVisibility(View.VISIBLE);
                tv_follow_submsg.setText("Follow");
            }
        }

        img_follow_new_scan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FollowUnFolllow();
            }
        });
    }

    private void getTabForRole(MyProfiles myProfiles) {
        try {
            String TabType = "";
            Bundle bundle = null;
            try {
                bundle = new Bundle();
                Bundle extras = getIntent().getExtras();
                String from = extras.getString(Constants.KEY);
                if (from != null && from.equals(Constants.KEY_SCANQK)) {
                    bundle.putString("unique_code", unique_code);
                } else {
                    bundle.putString("user_id", profile_id);
                }
                bundle.putString(Constants.KEY, key_from);
                bundle.putString("PROFILE_ID", profile_id + "");
                bundle.putString("KEY_FROM", key_from + "");
                bundle.putString("COMPANY_ID", company_id + "");
                bundle.putString("unique_code1", unique_code + "");
                bundle.putString("ROLE", myProfiles.getRole() + "");
                bundle.putString("ROLE_NAME", myProfiles.getRole_name() + "");
                bundle.putBoolean("isEditinfo", isEditinfo);
            } catch (Exception e) {
                e.printStackTrace();
            }
            Log.e("100", "Profile: " + myProfiles.getType());
            if (myProfiles.getType().equalsIgnoreCase("3")) {
                //TabType = "BFA";
                TabType = "BF";
                //title = new String[]{"Basic Details", "Followers", "Analytics"};
                title = new String[]{"Basic Details", "Followers"};
                if (isEditinfo) {
                    image_edit_new_scan.setVisibility(View.VISIBLE);
                } else {
                    image_edit_new_scan.setVisibility(View.GONE);
                }
            } else if (myProfiles.getType().equalsIgnoreCase("1")) {
                TabType = "BFE";
                //TabType = "BFEA";
                //title = new String[]{"Basic Details", "Followers", "Employees", "Analytics"};
                title = new String[]{"Basic Details", "Followers", "Employees"};
                if (isEditinfo) {
                    image_edit_new_scan.setVisibility(View.VISIBLE);
                } else {
                    image_edit_new_scan.setVisibility(View.GONE);
                }
            } else if (myProfiles.getType().equalsIgnoreCase("2")) {
                TabType = "BFE";
                // TabType = "BFEA";
                // title = new String[]{"Basic Details", "Followers", "Employees", "Analytics"};
                title = new String[]{"Basic Details", "Followers", "Employees"};
                if (isEditinfo) {
                    image_edit_new_scan.setVisibility(View.VISIBLE);
                } else {
                    image_edit_new_scan.setVisibility(View.GONE);
                }
            }
            adapter = new NewScanViewPagerAdapter(myProfiles, getSupportFragmentManager(), bundle, title, TabType, myProfiles.getType() + "", myProfiles.getStatus() + "");
            viewPager.setAdapter(adapter);
            tabLayout = findViewById(R.id.tabs_new_scan);
            tabLayout.setupWithViewPager(viewPager);
            changeTabsFont();
            viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                public void onPageScrollStateChanged(int state) {
                }

                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                    changeTabsFont();
                }

                public void onPageSelected(int position) {
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getTabLayout(MyProfiles myProfiles1, String type, String status) {
        try {
            String TabType = "";
            Bundle bundle = null;
            try {
                bundle = new Bundle();
                Bundle extras = getIntent().getExtras();
                String from = extras.getString(Constants.KEY);
                if (from != null && from.equals(Constants.KEY_SCANQK)) {
                    bundle.putString("unique_code", unique_code);
                } else {
                    bundle.putString("user_id", profile_id);
                }
                bundle.putString("user_id", profile_id);
                bundle.putString(Constants.KEY, key_from);
                bundle.putString("PROFILE_ID", profile_id + "");
                bundle.putString("KEY_FROM", key_from + "");
                bundle.putString("COMPANY_ID", company_id + "");
                bundle.putString("unique_code1", unique_code + "");
                bundle.putString("ROLE", role + "");
                bundle.putString("ROLE_NAME", role_name + "");
                bundle.putBoolean("isEditinfo", isEditinfo);
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (type.equalsIgnoreCase("3")) {
                if (isEditinfo) {
                    //TabType = "BFAS";
                    TabType = "BFS";
                    //title = new String[]{"Basic Details", "Followers", "Analytics", "Settings"};
                    title = new String[]{"Basic Details", "Followers", "Settings"};
                    image_edit_new_scan.setVisibility(View.VISIBLE);
                } else {
                    TabType = "BF";
                    title = new String[]{"Basic Details", "Followers"};
                    image_edit_new_scan.setVisibility(View.GONE);
                }
            } else if (type.equalsIgnoreCase("1")) {
                if (isEditinfo) {
                    // TabType = "BFEAS";
                    TabType = "BFES";
                    //title = new String[]{"Basic Details", "Followers", "Employees", "Analytics", "Settings"};
                    title = new String[]{"Basic Details", "Followers", "Employees", "Settings"};
                    image_edit_new_scan.setVisibility(View.VISIBLE);
                } else {
                    TabType = "BF";
                    title = new String[]{"Basic Details", "Followers"};
                    image_edit_new_scan.setVisibility(View.GONE);
                }
            } else if (type.equalsIgnoreCase("2")) {
                if (isEditinfo) {
                    if (status.equalsIgnoreCase("A")) {
                        //TabType = "BFEAS";
                        TabType = "BFES";
                        //title = new String[]{"Basic Details", "Followers", "Employees", "Analytics", "Settings"};
                        title = new String[]{"Basic Details", "Followers", "Employees", "Settings"};
                    } else {
                        //TabType = "BFAS";
                        TabType = "BFS";
                        //title = new String[]{"Basic Details", "Followers", "Analytics", "Settings"};
                        title = new String[]{"Basic Details", "Followers", "Settings"};
                    }
                    image_edit_new_scan.setVisibility(View.VISIBLE);
                } else {
                    TabType = "BF";
                    title = new String[]{"Basic Details", "Followers"};
                    image_edit_new_scan.setVisibility(View.GONE);
                }
            }
            adapter = new NewScanViewPagerAdapter(myProfiles1, getSupportFragmentManager(), bundle, title, TabType, type, status);
            viewPager.setAdapter(adapter);
            tabLayout = findViewById(R.id.tabs_new_scan);
            tabLayout.setupWithViewPager(viewPager);
            changeTabsFont();
            viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                public void onPageScrollStateChanged(int state) {
                }

                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                    changeTabsFont();
                }

                public void onPageSelected(int position) {
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getTabFromApi(String code, MyProfiles myProfiles1, String type, String status) {
        try {
            String TabType = code;
            Bundle bundle = null;
            try {
                bundle = new Bundle();
                Bundle extras = getIntent().getExtras();
                String from = extras.getString(Constants.KEY);
                if (from != null && from.equals(Constants.KEY_SCANQK)) {
                    bundle.putString("unique_code", unique_code);
                } else {
                    bundle.putString("user_id", profile_id);
                }
                bundle.putString("user_id", profile_id);
                bundle.putString(Constants.KEY, key_from);
                bundle.putString("unique_code1", unique_code + "");
                bundle.putString("ROLE", role + "");
                bundle.putString("ROLE_NAME", role_name + "");
                bundle.putBoolean("isEditinfo", isEditinfo);
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (TabType.equalsIgnoreCase("BF")) {
                title = new String[]{"Basic Details", "Followers"};
            } else if (TabType.equalsIgnoreCase("BFS")) {
                title = new String[]{"Basic Details", "Followers", "Settings"};
            } else if (TabType.equalsIgnoreCase("BFE")) {
                title = new String[]{"Basic Details", "Followers", "Employees"};
            } else if (TabType.equalsIgnoreCase("BFES")) {
                title = new String[]{"Basic Details", "Followers", "Employees", "Settings"};
            }

            if (isEditinfo) {
                image_edit_new_scan.setVisibility(View.VISIBLE);
            } else {
                image_edit_new_scan.setVisibility(View.GONE);
            }

            adapter = new NewScanViewPagerAdapter(myProfiles1, getSupportFragmentManager(), bundle, title, TabType, type, status);
            viewPager.setAdapter(adapter);
            tabLayout = findViewById(R.id.tabs_new_scan);
            tabLayout.setupWithViewPager(viewPager);
            changeTabsFont();
            viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                public void onPageScrollStateChanged(int state) {
                }

                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                    changeTabsFont();
                }

                public void onPageSelected(int position) {
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.menu = menu;
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                setResult(RESULT_OK);
                NewScanProfileActivity.this.finish();
                break;
            case R.id.ic_save_update_new_save:
                if (Utilities.isNetworkConnected(NewScanProfileActivity.this)) {
                    Fragment fragment = adapter.getItem(viewPager.getCurrentItem());
                    if (fragment instanceof NewScanFragment) {
                        NewScanFragment frg = (NewScanFragment) fragment;
                        frg.submitData(NewScanProfileActivity.this, strBase64 + "",
                                tv_company_name.getText().toString() + "",
                                Main_About_Text + "");
                    }
                } else
                    Utilities.showToast(NewScanProfileActivity.this, "No internet connection, please try after some time.");
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void showeditBasicCompanyinfo1() {

        Utilities.hidekeyboard(context, getWindow().getDecorView());

        c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        View view = LinearLayout.inflate(context, R.layout.dialog_edit_companybasicinfo, null);
        dialog.setContentView(view);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;

        dialog.show();
        dialog.getWindow().setAttributes(lp);

        TextView tvAlert = view.findViewById(R.id.tvAlert);
        TextView tvDone = view.findViewById(R.id.tvDone);
        TextView tvCancel = view.findViewById(R.id.tvCancel);
        TextView tv_counter_udbp_dialog = view.findViewById(R.id.tv_counter_udbp_dialog);

        edt_name = view.findViewById(R.id.edt_profile_name);
        edt_about = view.findViewById(R.id.edt_about);
        img_company_logo = view.findViewById(R.id.img_basic_profile_profile_pic);

       /* try {
            Drawable drawable = img_company_logo_main.getDrawable();
            Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
            //Bitmap bmp = BitmapFactory.decodeByteArray(bary, 0, bary.length);
            img_company_logo.setImageBitmap(bitmap);
        } catch (Exception e) {
            e.printStackTrace();
        }*/

        Utilities.TextWatcher(edt_about, tv_counter_udbp_dialog, "400");

        String name = tv_company_name.getText().toString();
        if (name != null && !name.isEmpty())
            edt_name.setText(name.replace(":", ""));

        //String about = tv_about.getText().toString();
        String about = Main_About_Text;
        if (about != null && !about.isEmpty())
            edt_about.setText(about.replace(":", ""));

        tvAlert.setText("Company Information");

        if (profile_pic != null && !profile_pic.isEmpty()) {
            Picasso.with(context)
                    .load(profile_pic)
                    .error(R.drawable.ic_logo_blue_qk)
                    .placeholder(R.drawable.ic_logo_blue_qk)
                    .transform(new CircleTransform())
                    .fit()
                    .into(img_company_logo);
        } else {
            Picasso.with(context)
                    .load(R.drawable.ic_logo_blue_qk)
                    .error(R.drawable.ic_logo_blue_qk)
                    .placeholder(R.drawable.ic_logo_blue_qk)
                    .transform(new CircleTransform())
                    .fit()
                    .into(img_company_logo);
        }

        view.findViewById(R.id.rlimgupload).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialog = new BottomSheetDialog(context);

                View view1 = getLayoutInflater().inflate(R.layout.custom_bottom_sheet_profile_pic, null);

                bottomSheetDialog.setContentView(view1);

                Button btnCancel = view1.findViewById(R.id.buttonSheet1);
                btnCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        bottomSheetDialog.dismiss();
                    }
                });

                TextView tvViewPhoto = view1.findViewById(R.id.TextViewSheet1);
                tvViewPhoto.setVisibility(View.GONE);
                view1.findViewById(R.id.divider_viewphoto).setVisibility(View.GONE);

                TextView tvUploadPhoto = view1.findViewById(R.id.TextViewSheet2);
                tvUploadPhoto.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        bottomSheetDialog.dismiss();
                        pickFromGallery();
                    }
                });

                TextView tvTakePhoto = view1.findViewById(R.id.TextViewSheet3);
                tvTakePhoto.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        bottomSheetDialog.dismiss();

                        if (ContextCompat.checkSelfPermission(NewScanProfileActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED)
                            ActivityCompat.requestPermissions(NewScanProfileActivity.this, new String[]{Manifest.permission.CAMERA}, 1596);
                        else
                            launchCamera();
                    }
                });
                bottomSheetDialog.show();
            }
        });

        tvDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                Utilities.hidekeyboard(context, getWindow().getDecorView());
                try {
                    tv_company_name.setText(edt_name.getText().toString());
                    tv_about.setText(edt_about.getText().toString());
                    Main_About_Text = edt_about.getText().toString();
                    try {
                        Utilities.addReadMore(NewScanProfileActivity.this, Main_About_Text, tv_about);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (selectedImage != null) {
                        ByteArrayOutputStream stream = new ByteArrayOutputStream();
                        selectedImage.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                        byte[] byteArray = stream.toByteArray();
                        strBase64 = Base64.encodeToString(byteArray, 0);
                        Log.d("ss", strBase64);
                        img_company_logo_main.setImageBitmap(selectedImage);
                        img_company_logo.setImageBitmap(selectedImage);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                Utilities.hidekeyboard(context, getWindow().getDecorView());
            }
        });
        dialog.show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == 1596) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                launchCamera();
            } else {
                Toast.makeText(this, "permission required to open camera", Toast.LENGTH_LONG).show();
            }
        }
    }

    private void pickFromGallery() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN && ActivityCompat.checkSelfPermission(context, android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(NewScanProfileActivity.this, new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE},
                    REQUEST_STORAGE_READ_ACCESS_PERMISSION);

        } else {
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            intent.addCategory(Intent.CATEGORY_OPENABLE);
            startActivityForResult(Intent.createChooser(intent, getString(R.string.label_select_picture)), REQUEST_SELECT_PICTURE);
        }
    }

    private void handleCropResult(@NonNull Intent result) {
        if (result != null) {
            final Uri resultUri = UCrop.getOutput(result);
            if (resultUri != null) {
                try {
                    final InputStream imageStream = getContentResolver().openInputStream(resultUri);
                    selectedImage = BitmapFactory.decodeStream(imageStream);
                    int orientation = Utilities.getCameraPhotoOrientation(NewScanProfileActivity.this, resultUri, file.getPath());
                    img_company_logo.setImageBitmap(selectedImage);
                    img_company_logo.setRotation(orientation);

                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                Utilities.showToast(context, "" + R.string.toast_cannot_retrieve_cropped_image);
            }
        }
    }

    private void startCropActivity(@NonNull Uri uri) {
        String destinationFileName = "" + System.currentTimeMillis();
        destinationFileName += ".png";
        UCrop uCrop = UCrop.of(uri, Uri.fromFile(new File(context.getCacheDir(), destinationFileName)));
        uCrop.withAspectRatio(1, 1);
        uCrop.start(NewScanProfileActivity.this);
    }

    public void launchCamera() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        file = new File(context.getExternalCacheDir(), String.valueOf(System.currentTimeMillis()) + ".jpg");

        fileUri = Uri.fromFile(file);

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
            intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
        } else {
            file = new File(file.getPath());
            Uri photoUri = FileProvider.getUriForFile(context.getApplicationContext(), context.getApplicationContext().getPackageName() + ".provider", file);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
        }
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        if (intent.resolveActivity(context.getApplicationContext().getPackageManager()) != null) {
            try {
                startActivityForResult(intent, PICK_FROM_CAMERA);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Utilities.hidekeyboard(context, getWindow().getDecorView());
        if (resultCode == Activity.RESULT_OK && requestCode == SUBSCRIBE_PACKAGE) {
            String stripe_token = qkPreferences.getUserStripeToken() + "";
            AcceptRejectTransferProfile("A", stripe_token + "");
        } else if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_SELECT_PICTURE) {
            fileUri = data.getData();
            file = new File(fileUri.getPath());
            if (fileUri != null) {
                try {
                    String compressImg = new CompressImage(context).compressImage("" + fileUri);
                    Uri compressUri = Uri.fromFile(new File(compressImg));
                    startCropActivity(compressUri);
                } catch (Exception e) {
                    startCropActivity(fileUri);
                    e.printStackTrace();
                }
            } else {
                Utilities.showToast(context, "" + R.string.toast_cannot_retrieve_selected_image);
            }
        } else if (resultCode == Activity.RESULT_OK && requestCode == PICK_FROM_CAMERA) {
            if (fileUri != null) {
                startCropActivity(fileUri);
            } else {
                Utilities.showToast(context, "" + R.string.toast_cannot_retrieve_selected_image);
            }
        } else if (requestCode == UCrop.REQUEST_CROP) {
            handleCropResult(data);
        } else {
            Fragment fragment = adapter.getItem(viewPager.getCurrentItem());
            fragment.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void CheckSubscription() {
        new VolleyApiRequest(NewScanProfileActivity.this, true, VolleyApiRequest.REQUEST_BASEURL + "api/get_user_plan_detail/" + profile_user_id + "",
                new VolleyCallBack() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("Request Data", "" + response);
                        if (response != null) {
                            try {
                                String status = response.getString("status");
                                String message = response.getString("msg");
                                if (!status.equalsIgnoreCase("") && status.equalsIgnoreCase("200")) {
                                    JSONObject data = response.getJSONObject("data");
                                    getUserPlanDetails(data);
                                } else {
                                    // user not subscribed with any plan so continue
                                    AcceptRejectTransferProfile("A", "");
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyApiRequest.showVolleyError(NewScanProfileActivity.this, error);
                    }
                }).
                enqueRequest(null, Request.Method.GET);
    }

    private void ShowPaymentDialog(JSONObject data) {

        if (data != null) {
            final Dialog dialog = new Dialog(NewScanProfileActivity.this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            View view = LinearLayout.inflate(NewScanProfileActivity.this, R.layout.dialog_show_payment_details, null);
            dialog.setContentView(view);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            lp.copyFrom(dialog.getWindow().getAttributes());
            lp.width = WindowManager.LayoutParams.MATCH_PARENT;
            dialog.show();
            dialog.getWindow().setAttributes(lp);
            TextView title = dialog.findViewById(R.id.tv_pay_title);
            TextView planDetail = dialog.findViewById(R.id.tv_pay_plandetails);
            TextView paynow = dialog.findViewById(R.id.tv_pay_paynow);
            TextView skip = dialog.findViewById(R.id.tv_pay_skip);

            String plan_name = "";
            String start_date = "";
            String end_date = "";
            try {
                plan_name = data.getString("plan_name");
                start_date = data.getString("start_date");
                end_date = data.getString("end_date");
            } catch (JSONException e) {
                e.printStackTrace();
            }

            String ttl = "";
            if (!tv_company_name.getText().toString().equalsIgnoreCase(""))
                ttl = "<html><font color='#c1c1c1'>The profile </font><font color='#000000'>" + tv_company_name.getText().toString() + "</font><font color='#c1c1c1'> has an active subscription plan as below.If you would like to continue using the plan, kindly provide us your credit card details for billing purpose.with current plan details.</font></html>";
            else
                ttl = "<html><font color='#c1c1c1'>This has an active subscription plan as below.  If you would like to continue using the plan, kindly provide us your credit card details for billing purpose. with current plan details.</font></html>";
            title.setText(Html.fromHtml(ttl + ""));

            String html = "<html><font color='#c1c1c1'>Plan name : </font><font color='#000000'>" + plan_name + "</font><br/>" +
                    "<font color='#c1c1c1'>Start date : </font><font color='#000000'>" + start_date + "</font><br/>" +
                    "<font color='#c1c1c1'>End date : </font><font color='#000000'>" + end_date + "</font></html>";
            planDetail.setText(Html.fromHtml(html + ""));

            paynow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (dialog != null && dialog.isShowing()) {
                        dialog.dismiss();
                    }
                    Intent i = new Intent(NewScanProfileActivity.this, PaymentDetails.class);
                    i.putExtra("KEY_FROM", "Transfer_Profile");
                    startActivityForResult(i, SUBSCRIBE_PACKAGE);
                }
            });
            skip.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (dialog != null && dialog.isShowing()) {
                        dialog.dismiss();
                    }
                    AcceptRejectTransferProfile("A", "");
                }
            });
        }
    }

    private void AcceptRejectTransferProfile(String type, String stripe_token) {
        JSONObject obj = new JSONObject();
        try {
            obj.put("user_id", login_user_id);
            obj.put("status", type);
            obj.put("notification_id", notification_id + "");
            obj.put("stripe_token", stripe_token + "");
        } catch (Exception e) {
            e.printStackTrace();
        }
        Log.e("Param-ScanUser", obj.toString());
        new VolleyApiRequest(NewScanProfileActivity.this, true, VolleyApiRequest.REQUEST_BASEURL + "api/profile/transfer_request_status",
                new VolleyCallBack() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("Request Data", "" + response);
                        if (response != null && !response.equals("") && !response.equals("null")) {
                            try {
                                String status = response.getString("status");
                                String message = response.getString("msg");
                                if (!status.equalsIgnoreCase("") && status.equalsIgnoreCase("200")) {

                                    if (!NewScanProfileActivity.this.isFinishing()) {
                                        Utilities.showToast(getApplicationContext(), message + "");
                                    }
                                    Intent returnIntent = new Intent();
                                    returnIntent.putExtra("notification_id", notification_id + "");
                                    setResult(Activity.RESULT_OK, returnIntent);
                                    NewScanProfileActivity.this.finish();
                                } else {
                                    Utilities.showToast(getApplicationContext(), message + "");
                                    NewScanProfileActivity.this.finish();
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyApiRequest.showVolleyError(NewScanProfileActivity.this, error);
                    }
                }).
                enqueRequest(obj, Request.Method.POST);
    }

    private void UnblockPorifle() {
        JSONObject obj = new JSONObject();
        try {
            obj.put("user_id", login_user_id);
            obj.put("profile_id", profile_id + "");
        } catch (Exception e) {
            e.printStackTrace();
        }
        Log.e("Param-ScanUser", obj.toString());
        new VolleyApiRequest(NewScanProfileActivity.this, true, VolleyApiRequest.REQUEST_BASEURL + "api/member/unblock_profile",
                new VolleyCallBack() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("Request Data", "" + response);
                        if (response != null && !response.equals("") && !response.equals("null")) {
                            try {
                                String status = response.getString("status");
                                String message = response.getString("msg");
                                if (!status.equalsIgnoreCase("") && status.equalsIgnoreCase("200")) {
                                    Utilities.showToast(getApplicationContext(), message + "");
                                    setResult(RESULT_OK);
                                    NewScanProfileActivity.this.finish();
                                } else {
                                    Utilities.showToast(getApplicationContext(), message + "");
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyApiRequest.showVolleyError(NewScanProfileActivity.this, error);
                    }
                }).
                enqueRequest(obj, Request.Method.POST);
    }

    public void HideKeyboard() {
        Utilities.hideKeyboard(NewScanProfileActivity.this);
    }

    public void FollowUnFolllow() {
        String msg = "You just started following " + tv_company_name.getText().toString();
        tv_follower_msg.setText(msg + "");

        if (tv_follow_submsg.getText().toString().equalsIgnoreCase("Unfollow")) {
            AlertDialog.Builder builder = new AlertDialog.Builder(NewScanProfileActivity.this);
            String msg1 = "";
            if (!tv_follow_submsg.getText().toString().isEmpty() && !tv_company_name.getText().toString().isEmpty())
                msg1 = "You want to " + tv_follow_submsg.getText().toString() + " " + tv_company_name.getText().toString() + "?";
            else
                msg1 = "You want to " + tv_follow_submsg.getText().toString() + " this profile?";
            builder.setMessage(msg1);
            builder.setTitle("Are you sure?");
            builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                    CreateFollowUnFollow(0);
                }
            });
            builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });
            builder.show();
        } else if (tv_follow_submsg.getText().toString().equalsIgnoreCase("Follow")) {
            CreateFollowUnFollow(1);
        }
    }

    private void CreateFollowUnFollow(final int type) {
        JSONObject obj = new JSONObject();
        try {
            obj.put("is_follow_from_search", "1");
            obj.put("user_id", qkPreferences.getLoggedUserid() + "");
            obj.put("profile_id", profile_id + "");
            obj.put("status", type + "");
        } catch (Exception e) {
            e.printStackTrace();
        }
        new VolleyApiRequest(NewScanProfileActivity.this, true, VolleyApiRequest.REQUEST_BASEURL + "api/profile/follow_unfollow",
                new VolleyCallBack() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if (response != null && !response.equals("") && !response.equals("null")) {
                            try {
                                String status = response.getString("status");
                                String message = response.getString("msg");
                                Utilities.showToast(getApplicationContext(), message + "");
                                if (!status.equalsIgnoreCase("") && status.equalsIgnoreCase("200")) {
                                    if (type == 0) {
                                        img_follow_new_scan.setVisibility(View.VISIBLE);
                                        img_follow_new_scan.setImageResource(R.drawable.ic_follow);
                                        tv_follower_msg.setVisibility(View.GONE);
                                        tv_follow_submsg.setVisibility(View.VISIBLE);
                                        tv_follow_submsg.setText("Follow");
                                    } else if (type == 1) {
                                        img_follow_new_scan.setVisibility(View.VISIBLE);
                                        img_follow_new_scan.setImageResource(R.drawable.ic_unfollow);
                                        tv_follower_msg.setVisibility(View.VISIBLE);
                                        tv_follow_submsg.setVisibility(View.VISIBLE);
                                        tv_follow_submsg.setText("Unfollow");
                                    }
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyApiRequest.showVolleyError(NewScanProfileActivity.this, error);
                    }
                }).
                enqueRequest(obj, Request.Method.POST);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void revealShow(final LinearLayout lin, View view) {

        int w = lin.getWidth();
        int h = lin.getHeight();

        int endRadius = (int) Math.hypot(w, h);

        int cx = (int) (view.getX() + (view.getWidth() / 2));
        int cy = (int) (view.getY()) + view.getHeight() + 56;

        Animator revealAnimator = ViewAnimationUtils.createCircularReveal(lin, cx, cy, 5f, endRadius);
        lin.setVisibility(View.VISIBLE);
        revealAnimator.setDuration(800);
        revealAnimator.start();
    }

    public void getUserPlanDetails(final JSONObject data) {
        if (data != null) {
            String usersid = qkPreferences.getValuesInt(QKPreferences.USER_ID) + "";
            new VolleyApiRequest(NewScanProfileActivity.this, true, VolleyApiRequest.REQUEST_BASEURL + "api/get_user_plan_detail/" + usersid + "",
                    new VolleyCallBack() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.e("Request Data", "" + response);
                            if (response != null) {
                                try {
                                    String status = response.getString("status");
                                    if (status != null && status.equalsIgnoreCase("200")) {
                                        // current user already subscribed so continue transfer
                                        AcceptRejectTransferProfile("A", "");
                                    } else {
                                        // current user not have any subscription so show dialog for payment
                                        ShowPaymentDialog(data);
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }

                        @Override
                        public void onErrorResponse(VolleyError error) {
                            VolleyApiRequest.showVolleyError(NewScanProfileActivity.this, error);
                        }
                    }).
                    enqueRequest(null, Request.Method.GET);
        }
    }


//    public void showData(MyProfiles myProfiles) {
//        try {
//            try {
//                role = myProfiles.getRole() + "";
//                role_name = myProfiles.getRole_name() + "";
//
//                if (myProfiles.getRole() != null && !myProfiles.getRole().equalsIgnoreCase("") && !myProfiles.getRole().equalsIgnoreCase("null")) {
//                    if (myProfiles.getRole().equalsIgnoreCase("0")) {
//                        if (key_from.equals(Constants.KEY_FROM_MANAGEPROFILES)) {
//                            isEditinfo = true;
//                        } else if (key_from.equals(Constants.KEY_FROM_NOTIFICATIONS)) {
//                            isAcceptReject = true;
//                            try {
//                                notification_id = getIntent().getExtras().getString("notification_id");
//                            } catch (Exception e) {
//                                e.printStackTrace();
//                            }
//                        } else if (key_from.equalsIgnoreCase(Constants.KEY_FROM_BLOCK_PROFILE)) {
//                            isAcceptReject = false;
//                            isEditinfo = false;
//                            isNotification = true;
//                            img_block_new_scan.setVisibility(View.VISIBLE);
//                        } else if (key_from.equalsIgnoreCase(Constants.KEY_SCANQK)) {
//                            isFromScan = true;
//                        }
//                    } else {
//                        if (myProfiles.getRole().equalsIgnoreCase("2") || myProfiles.getRole().equalsIgnoreCase("3")) {
//                            isEditinfo = true;
//                        } else {
//                            isEditinfo = false;
//                        }
//                    }
//                } else {
//                    if (key_from.equals(Constants.KEY_FROM_MANAGEPROFILES)) {
//                        isEditinfo = true;
//                    } else if (key_from.equals(Constants.KEY_FROM_NOTIFICATIONS)) {
//                        isAcceptReject = true;
//                        try {
//                            notification_id = getIntent().getExtras().getString("notification_id");
//                        } catch (Exception e1) {
//                            e1.printStackTrace();
//                        }
//                    } else if (key_from.equalsIgnoreCase(Constants.KEY_FROM_BLOCK_PROFILE)) {
//                        isAcceptReject = false;
//                        isEditinfo = false;
//                        isNotification = true;
//                        img_block_new_scan.setVisibility(View.VISIBLE);
//                    } else if (key_from.equalsIgnoreCase(Constants.KEY_SCANQK)) {
//                        isFromScan = true;
//                    }
//                }
//            } catch (Exception e) {
//                if (key_from.equals(Constants.KEY_FROM_MANAGEPROFILES)) {
//                    isEditinfo = true;
//                } else if (key_from.equals(Constants.KEY_FROM_NOTIFICATIONS)) {
//                    isAcceptReject = true;
//                    try {
//                        notification_id = getIntent().getExtras().getString("notification_id");
//                    } catch (Exception e1) {
//                        e1.printStackTrace();
//                    }
//                } else if (key_from.equalsIgnoreCase(Constants.KEY_FROM_BLOCK_PROFILE)) {
//                    isAcceptReject = false;
//                    isEditinfo = false;
//                    isNotification = true;
//                    img_block_new_scan.setVisibility(View.VISIBLE);
//                } else if (key_from.equalsIgnoreCase(Constants.KEY_SCANQK)) {
//                    isFromScan = true;
//                }
//                e.printStackTrace();
//            }
//
//            if (key_from.equalsIgnoreCase(Constants.KEY_SCANQK_CONTACT)) {
//                isFromScan_Contact = true;
//                FollowedAt = getIntent().getExtras().getString("FollowedAt");
//            }
//
//            if (isAcceptReject)
//                lin_acc_reject.setVisibility(View.VISIBLE);
//            else
//                lin_acc_reject.setVisibility(View.GONE);
//
//            try {
//                MenuInflater inflater = getMenuInflater();
//                inflater.inflate(R.menu.new_save_menu, menu);
//                MenuItem item_save = menu.findItem(R.id.ic_save_update_new_save);
//                if (isEditinfo) {
//                    item_save.setVisible(true);
//                } else {
//                    item_save.setVisible(false);
//                }
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//
//            FollowUnFollowFunc();
//
//            if (myProfiles.getRole().equalsIgnoreCase("0")) {
//                if (myProfiles.getType() != null && !myProfiles.getType().equalsIgnoreCase("") && !myProfiles.getType().equalsIgnoreCase("null"))
//                    getTabLayout(myProfiles, myProfiles.getType(), myProfiles.getStatus());
//            } else {
//                if (myProfiles.getType() != null && !myProfiles.getType().equalsIgnoreCase("") && !myProfiles.getType().equalsIgnoreCase("null"))
//                    getTabForRole(myProfiles);
//            }
//
//            // Name
//            if (myProfiles.getName() != null) {
//                tv_company_name.setText(myProfiles.getName());
//            } else
//                tv_company_name.setVisibility(View.GONE);
//
//            String msg = "You just started following " + myProfiles.getName();
//            tv_follower_msg.setText(msg + "");
//
//            // About
//            if (myProfiles.getAbout() != null) {
//                tv_about.setText(myProfiles.getAbout());
//            } else
//                tv_about.setVisibility(View.GONE);
//
//            profile_id = myProfiles.getId() + "";
//            // Company Logo
//            profile_pic = myProfiles.getLogo();
//            try {
//                if (profile_pic != null && Utilities.isEmpty(profile_pic)) {
//                    Picasso.with(NewScanProfileActivity.this)
//                            .load(profile_pic)
//                            .error(R.drawable.ic_default_profile_photo)
//                            .transform(new CircleTransform())
//                            .placeholder(R.drawable.ic_default_profile_photo)
//                            .fit()
//                            .into(img_company_logo_main);
//                } else {
//                    img_company_logo_main.setImageResource(R.drawable.ic_default_profile_photo);
//                }
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }

}
