package com.quickkonnect;

import android.animation.Animator;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.cunoraz.gifview.library.GifView;
import com.google.gson.Gson;
import com.models.BasicDetail;
import com.realmtable.Employees;
import com.squareup.picasso.Picasso;
import com.utilities.Constants;
import com.utilities.QKPreferences;
import com.utilities.Utilities;
import com.volleyapirequest.VolleyApiRequest;
import com.volleyapirequest.VolleyCallBack;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class EmployeesProfile extends AppCompatActivity {

    private Toolbar toolbar;
    private LinearLayout lin_edit, lin_acc_reject, llEMPVALIDFROM, llEMPVALIDTO, llEMPNO, llEMPDESIGNATION, llEMPDEPT;
    //CardView carddetail;
    CoordinatorLayout cord_emp;
    EditText edt_emp_number, edt_emp_depart, edt_designation, edt_validate_from, edt_validate_to;
    TextView /*tvmessage,*/ tvsubmessage, tv_edt_emp_number, tv_edt_emp_depart, tv_edt_designation, tv_edt_validate_from, tv_edt_validate_to;
    public ImageView img_company_logo_main, img_company_logo_main_2, image_edit_new_scan;
    public TextView tv_company_name, tv_about, tv_company_name_2, tv_about_2, accept_profile, submit, tv_emp_reject_1;
    public TextView tv_empno, tv_department, tv_designation, tv_validatefrom, tv_validateto;
    private Context context;
    FrameLayout frame1, frame2, frame_detail_1, frame_detail_2;
    private QKPreferences qkPreferences;
    private boolean isEditinfo = false;
    private boolean isAcceptReject = false;
    private boolean isNotiAR = false;
    private int mYear, mMonth, mDay;
    CardView card_1_emp, card_2_emp;
    Calendar c;
    String Profile_Json = "", STATUS = "";
    String Key = "", UserID = "", UserDetails = "", id = "", mlistdata = "", empid = "", notificationid = "", noti_u_id = "", noti_prof_id = "", ProfileName = "";

    String user_dialog_profilepic = "", user_dialog_name = "";

    // Open Dialog
    EditText email, search_email;
    TextView title, no_user, cancel, search, confirm, invite, username;
    ImageView profileImage, search_icon;
    LinearLayout linear_profile, lin_search;

    //New Dialog
    LinearLayout lin_emo_role_dialog_main;
    RadioGroup radioGroup;
    TextView close_role, submit_role;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_employees_profile);

        if (Build.VERSION.SDK_INT >= 19 && Build.VERSION.SDK_INT < 21) {
            setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, true);
        }
        if (Build.VERSION.SDK_INT >= 19) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }
        if (Build.VERSION.SDK_INT >= 21) {
            setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, false);
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }


        toolbar = findViewById(R.id.toolbar_emp);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close_white_24dp);
        getSupportActionBar().setTitle("");

        try {
            Bundle b = getIntent().getExtras();
            Key = b.getString("KEY");
            UserID = b.getString("USER_ID");
            if (Key.equalsIgnoreCase("EDIT")) {
                mlistdata = b.getString("USER_BASIC_DETAIL");
                isEditinfo = true;
            } else if (Key.equalsIgnoreCase("NOTIFICATION")) {
                isAcceptReject = true;
                notificationid = b.getString("notification_id");
            } else if (Key.equalsIgnoreCase("NOTIFICATION_AR")) {
                isAcceptReject = true;
                isNotiAR = true;
                notificationid = b.getString("notification_id");
            } else {
                UserDetails = b.getString("USER_BASIC_DETAIL");
                isEditinfo = false;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        context = EmployeesProfile.this;
        qkPreferences = new QKPreferences(context);
        initWidget();
    }

    public static void setWindowFlag(Activity activity, final int bits, boolean on) {
        Window win = activity.getWindow();
        WindowManager.LayoutParams winParams = win.getAttributes();
        if (on) {
            winParams.flags |= bits;
        } else {
            winParams.flags &= ~bits;
        }
        win.setAttributes(winParams);
    }

    public void initWidget() {

        edt_emp_number = findViewById(R.id.edt_emp_number);
        edt_emp_depart = findViewById(R.id.edt_emp_depart);
        edt_designation = findViewById(R.id.edt_designation);
        edt_validate_from = findViewById(R.id.edt_validate_from);
        edt_validate_to = findViewById(R.id.edt_validate_to);

        cord_emp = findViewById(R.id.cord_emp);

        card_1_emp = findViewById(R.id.card_1_emp);
        card_2_emp = findViewById(R.id.card_2_emp);

        //tvmessage = findViewById(R.id.tvmessage);
        tvsubmessage = findViewById(R.id.tvsubmessage);

        tv_edt_emp_number = findViewById(R.id.edt_emp_number_2);
        tv_edt_emp_depart = findViewById(R.id.edt_emp_depart_2);
        tv_edt_designation = findViewById(R.id.edt_designation_2);
        tv_edt_validate_from = findViewById(R.id.edt_validate_from_2);
        tv_edt_validate_to = findViewById(R.id.edt_validate_to_2);

        frame1 = findViewById(R.id.frame_1);
        frame2 = findViewById(R.id.frame_2);
        frame_detail_1 = findViewById(R.id.frame_detail_1);
        frame_detail_2 = findViewById(R.id.frame_detail_2);

        img_company_logo_main = findViewById(R.id.img_company_logo_emp);
        img_company_logo_main_2 = findViewById(R.id.img_company_logo_emp_2);
        tv_company_name = findViewById(R.id.tv_company_name_emp);
        tv_about = findViewById(R.id.tv_about_emp);
        tv_company_name_2 = findViewById(R.id.tv_company_name_emp_2);
        tv_about_2 = findViewById(R.id.tv_about_emp_2);
        lin_acc_reject = findViewById(R.id.lin_acc_reject_emp);
        lin_edit = findViewById(R.id.lin_emp_edit);
        lin_edit.setVisibility(View.VISIBLE);

        accept_profile = findViewById(R.id.tv_emp_accept);
        submit = findViewById(R.id.tv_emp_reject);
        tv_emp_reject_1 = findViewById(R.id.tv_emp_reject_1);
        tv_emp_reject_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ValidateAndNext();
            }
        });

        edt_validate_from.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String editextStartDate = edt_validate_from.getText().toString().trim();
                if (editextStartDate != null && !editextStartDate.isEmpty() && !editextStartDate.equals("null")) {
                    String[] parts = editextStartDate.split("/");
                    mDay = Integer.parseInt(parts[0]);
                    mMonth = Integer.parseInt(parts[1]) - 1;
                    mYear = Integer.parseInt(parts[2]);
                }
                DatePickerDialog datePickerDialog = new DatePickerDialog(EmployeesProfile.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                int month = monthOfYear + 1;
                                String fm = "" + month;
                                String fd = "" + dayOfMonth;
                                if (month < 10) {
                                    fm = "0" + month;
                                }
                                if (dayOfMonth < 10) {
                                    fd = "0" + dayOfMonth;
                                }
                                edt_validate_from.setText(fd + "/" + fm + "/" + year);
                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.getDatePicker().setMaxDate(c.getTimeInMillis());
                datePickerDialog.show();
            }
        });
        edt_validate_to.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String editextEndDate = edt_validate_to.getText().toString().trim();
                if (editextEndDate != null && !editextEndDate.isEmpty() && !editextEndDate.equals("null")) {
                    String[] parts = editextEndDate.split("/");
                    mDay = Integer.parseInt(parts[0]);
                    mMonth = Integer.parseInt(parts[1]) - 1;
                    mYear = Integer.parseInt(parts[2]);
                }
                DatePickerDialog datePickerDialog = new DatePickerDialog(EmployeesProfile.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                int month = monthOfYear + 1;
                                String fm = "" + month;
                                String fd = "" + dayOfMonth;
                                if (month < 10) {
                                    fm = "0" + month;
                                }
                                if (dayOfMonth < 10) {
                                    fd = "0" + dayOfMonth;
                                }
                                edt_validate_to.setText(fd + "/" + fm + "/" + year);
                            }
                        }, mYear, mMonth, mDay);
                try {
                    SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
                    Date d = f.parse(edt_validate_from.getText().toString());
                    long milliseconds = d.getTime();
                    datePickerDialog.getDatePicker().setMinDate(milliseconds);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                datePickerDialog.show();
            }
        });

        if (isAcceptReject) {
            LoadNotificationData();
        } else {
            if (isEditinfo) {
                tv_emp_reject_1.setText("Update");
                LoadData();
            } else {
                tv_emp_reject_1.setText("Submit");
                getData();
            }
        }
    }

    private void LoadData() {
        try {
            if (mlistdata != null && !mlistdata.equalsIgnoreCase("") && !mlistdata.equalsIgnoreCase("null")) {
                try {
                    PojoClasses.EmployeesList emp = new Gson().fromJson(mlistdata, PojoClasses.EmployeesList.class);
                    PojoClasses.EmployeesList.User usr = new Gson().fromJson(emp.getUser1(), PojoClasses.EmployeesList.User.class);
                    if (emp.getUserId() != null && !emp.getUserId().equalsIgnoreCase("null")) {
                        id = emp.getUserId() + "";
                    }
                    if (emp.getId() != null && !emp.getId().equalsIgnoreCase("null")) {
                        empid = emp.getId() + "";
                    }
                    if (usr.getProfilePic() != null && !usr.getProfilePic().isEmpty() && !usr.getProfilePic().equals("null")) {
                        user_dialog_profilepic = usr.getProfilePic();
                        Picasso.with(EmployeesProfile.this)
                                .load(usr.getProfilePic())
                                .error(R.drawable.ic_default_profile_photo)
                                .placeholder(R.drawable.ic_default_profile_photo)
                                .transform(new CircleTransform())
                                .fit()
                                .into(img_company_logo_main);
                    } else {
                        img_company_logo_main.setImageResource(R.drawable.ic_default_profile_photo);
                    }
                    tv_company_name.setText(usr.getFirstname() + " " + usr.getLastname());
                    if (usr.getEmail() != null && !usr.getEmail().equalsIgnoreCase("") && !usr.getEmail().equalsIgnoreCase("null"))
                        tv_about.setText(usr.getEmail());
                    if (emp.getEmployeeNo() != null && !emp.getEmployeeNo().equalsIgnoreCase("") && !emp.getEmployeeNo().equalsIgnoreCase("null"))
                        edt_emp_number.setText(emp.getEmployeeNo() + "");
                    if (emp.getDepartment() != null && !emp.getDepartment().equalsIgnoreCase("") && !emp.getDepartment().equalsIgnoreCase("null"))
                        edt_emp_depart.setText(emp.getDepartment() + "");
                    if (emp.getDesignation() != null && !emp.getDesignation().equalsIgnoreCase("") && !emp.getDesignation().equalsIgnoreCase("null"))
                        edt_designation.setText(emp.getDesignation() + "");
                    if (emp.getValidFrom() != null && !emp.getValidFrom().equalsIgnoreCase("") && !emp.getValidFrom().equalsIgnoreCase("null")) {
                        String dobConvert = emp.getValidFrom();
                        String[] separated = dobConvert.split("/");
                        String year = separated[0];
                        String month = separated[1];
                        String day = separated[2];
                        dobConvert = day + "/" + month + "/" + year;
                        edt_validate_from.setText(dobConvert + "");
                    }
                    if (emp.getValidTo() != null && !emp.getValidTo().equalsIgnoreCase("") && !emp.getValidTo().equalsIgnoreCase("null")) {
                        String dobConvert = emp.getValidTo();
                        String[] separated = dobConvert.split("/");
                        String year = separated[0];
                        String month = separated[1];
                        String day = separated[2];
                        dobConvert = day + "/" + month + "/" + year;
                        edt_validate_to.setText(dobConvert + "");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                Log.d("error", "not in");
            }

            frame1.setVisibility(View.VISIBLE);
            frame2.setVisibility(View.GONE);
            frame_detail_1.setVisibility(View.VISIBLE);
            frame_detail_2.setVisibility(View.GONE);
            //tvmessage.setVisibility(View.GONE);
            tvsubmessage.setVisibility(View.GONE);
            try {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    cord_emp.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
                        @TargetApi(Build.VERSION_CODES.LOLLIPOP)
                        @Override
                        public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                            v.removeOnLayoutChangeListener(this);
                            revealShow(card_1_emp, frame1);
                        }
                    });
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getData() {

        if (UserDetails != null && !UserDetails.equalsIgnoreCase("")) {
            BasicDetail basicDetail = new Gson().fromJson(UserDetails.toString(), BasicDetail.class);
            if (basicDetail != null && !basicDetail.toString().equalsIgnoreCase("null")) {
                if (basicDetail.getUserId() != null && !basicDetail.getUserId().toString().equalsIgnoreCase("null")) {
                    id = basicDetail.getUserId().toString();
                }
                if (basicDetail.getProfilePic() != null && !basicDetail.getProfilePic().isEmpty() && !basicDetail.getProfilePic().equals("null")) {
                    user_dialog_profilepic = basicDetail.getProfilePic();
                    Picasso.with(EmployeesProfile.this)
                            .load(basicDetail.getProfilePic())
                            .error(R.drawable.ic_default_profile_photo)
                            .placeholder(R.drawable.ic_default_profile_photo)
                            .transform(new CircleTransform())
                            .fit()
                            .into(img_company_logo_main);
                } else {
                    img_company_logo_main.setImageResource(R.drawable.ic_default_profile_photo);
                }
                tv_company_name.setText(basicDetail.getFirstname() + " " + basicDetail.getLastname());
                if (basicDetail.getEmail() != null && !basicDetail.getEmail().equalsIgnoreCase("") && !basicDetail.getEmail().equalsIgnoreCase("null"))
                    tv_about.setText(basicDetail.getEmail());

                frame1.setVisibility(View.VISIBLE);
                frame2.setVisibility(View.GONE);
                frame_detail_1.setVisibility(View.VISIBLE);
                frame_detail_2.setVisibility(View.GONE);
                //tvmessage.setVisibility(View.GONE);
                tvsubmessage.setVisibility(View.GONE);
                try {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        cord_emp.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
                            @TargetApi(Build.VERSION_CODES.LOLLIPOP)
                            @Override
                            public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                                v.removeOnLayoutChangeListener(this);
                                revealShow(card_1_emp, frame1);
                            }
                        });
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void ValidateAndNext() {
        if (edt_emp_number.getText().toString().isEmpty() || edt_emp_number.getText().toString().equalsIgnoreCase("")) {
            Utilities.showToast(getApplicationContext(), "Employee Number is require");
        }/* else if (edt_emp_depart.getText().toString().isEmpty() || edt_emp_depart.getText().toString().equalsIgnoreCase("")) {
            Utilities.showToast(getApplicationContext(), "Employee Department is require");
        } else if (edt_designation.getText().toString().isEmpty() || edt_designation.getText().toString().equalsIgnoreCase("")) {
            Utilities.showToast(getApplicationContext(), "Employee Designation is require");
        }*/ else if (edt_validate_from.getText().toString().isEmpty() || edt_validate_from.getText().toString().equalsIgnoreCase("")) {
            Utilities.showToast(getApplicationContext(), "Valid from date is require");
        } else if (edt_validate_to.getText().toString().isEmpty() || edt_validate_to.getText().toString().equalsIgnoreCase("")) {
            Utilities.showToast(getApplicationContext(), "Valid to date is require");
        } else {
            final String empNumber = edt_emp_number.getText().toString().trim();
            final String empDepart = edt_emp_depart.getText().toString().trim();
            final String empDesignation = edt_designation.getText().toString().trim();
            String startdate = edt_validate_from.getText().toString().trim();
            String finalstartdate = null;
            String enddate = edt_validate_to.getText().toString().trim();
            String finalenddate = null;
            Utilities.hideKeyboard(EmployeesProfile.this);
            if ((startdate != null && !startdate.isEmpty() && !startdate.equals("null")) && (enddate != null && !enddate.isEmpty() && !enddate.equals("null"))) {
                if (!isDateAfter(startdate, enddate)) {
                    Utilities.showToast(context, "Valid to date should be greater than Validate from date");
                } else {
                    if (startdate == "" || startdate.equals("") || startdate.equals("null")) {
                        finalstartdate = "";
                    } else {
                        String[] separated = startdate.split("/");
                        String day = separated[0];
                        String month = separated[1];
                        String year = separated[2];
                        finalstartdate = year + "-" + month + "-" + day;
                    }
                    if (enddate == "" || enddate.equals("") || enddate.equals("null")) {
                        finalenddate = "";
                    } else {
                        String[] separated1 = enddate.split("/");
                        String day1 = separated1[0];
                        String month1 = separated1[1];
                        String year1 = separated1[2];
                        finalenddate = year1 + "-" + month1 + "-" + day1;
                    }
                    if (!Utilities.isNetworkConnected(this))
                        Toast.makeText(this, Constants.NO_INTERNET_CONNECTION, Toast.LENGTH_LONG).show();
                    else {
                        String username = qkPreferences.getValues(QKPreferences.USER_FULLNAME) + "";
                        if (username.equalsIgnoreCase(tv_company_name.getText().toString() + "")) {
                            final String finalStartdate = finalstartdate;
                            final String finalEnddate = finalenddate;
                            functionAddEmployee(empNumber, empDepart, empDesignation, finalStartdate, finalEnddate);
                        } else {
                            AlertDialog.Builder builder = new AlertDialog.Builder(EmployeesProfile.this);
                            String msg = "";
                            if (!tv_company_name.getText().toString().equalsIgnoreCase("")) {
                                if (isEditinfo)
                                    msg = "Are you sure want to update " + tv_company_name.getText().toString() + "'s details";
                                else
                                    msg = "Are you sure want to add " + tv_company_name.getText().toString() + " to your company ?";
                            } else {
                                if (isEditinfo)
                                    msg = "Are you sure want to update this employee detail?";
                                else
                                    msg = "Are you sure want to add this employee to your company?";
                            }

                            builder.setMessage(msg);
                            final String finalStartdate = finalstartdate;
                            final String finalEnddate = finalenddate;
                            builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.dismiss();
                                    functionAddEmployee(empNumber, empDepart, empDesignation, finalStartdate, finalEnddate);
                                }
                            });
                            builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.dismiss();
                                }
                            });
                            builder.show();
                            //ShowPopup(empNumber, empDepart, empDesignation, finalstartdate, finalenddate);
                        }
                    }
                }
            } else {
                Utilities.showToast(context, "Validate to date and Validate from date are require");
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.new_save_menu, menu);
        MenuItem item_save = menu.findItem(R.id.ic_save_update_new_save);
        item_save.setVisible(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (!isAcceptReject) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(EmployeesProfile.this);
                    String msg = getResources().getString(R.string.msg_discard_changes)+"";
                    builder.setMessage(msg);
                    builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                            Intent returnIntent = new Intent();
                            setResult(AppCompatActivity.RESULT_OK, returnIntent);
                            EmployeesProfile.this.finish();
                        }
                    });
                    builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    });
                    builder.show();
                } else {
                    finish();
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (!isAcceptReject) {
            AlertDialog.Builder builder = new AlertDialog.Builder(EmployeesProfile.this);
            String msg = getResources().getString(R.string.msg_discard_changes)+"";
            builder.setMessage(msg);
            builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                    Intent returnIntent = new Intent();
                    setResult(AppCompatActivity.RESULT_OK, returnIntent);
                    EmployeesProfile.this.finish();
                }
            });
            builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });
            builder.show();
        } else {
            finish();
        }
    }

    public static boolean isDateAfter(String startDate, String endDate) {
        try {
            String myFormatString = "dd/MM/yyyy"; // for example
            SimpleDateFormat df = new SimpleDateFormat(myFormatString);
            Date date1 = df.parse(endDate);
            Date startingDate = df.parse(startDate);

            return date1.after(startingDate);
        } catch (Exception e) {

            return false;
        }
    }

    private void functionAddEmployee(String empNumber, String empDepart, String empDesignation, String finalstartdate, String finalenddate) {
        JSONObject obj = new JSONObject();
        try {
            obj.put("profile_id", UserID);
            obj.put("user_id", id);
            if (isEditinfo)
                obj.put("id", empid);
            obj.put("employee_no", empNumber);
            obj.put("valid_from", finalstartdate);
            obj.put("valid_to", finalenddate);
            obj.put("designation", empDesignation);
            obj.put("department", empDepart);
        } catch (Exception e) {
            e.printStackTrace();
        }
        new VolleyApiRequest(context, true, VolleyApiRequest.REQUEST_BASEURL + "api/member/add_edit_member",
                new VolleyCallBack() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String responseStatus = response.getString("status");
                            String msg = response.getString("msg");
                            if (responseStatus.equals("200")) {
                                if (isEditinfo) {
                                    Utilities.showToast(context, msg);
                                    Intent returnIntent = new Intent();
                                    setResult(AppCompatActivity.RESULT_OK, returnIntent);
                                    EmployeesProfile.this.finish();
                                } else {
                                    ShowSuccessDialog();
                                }
                                //Utilities.showToast(context, msg);
                               /* Intent returnIntent = new Intent();
                                setResult(AppCompatActivity.RESULT_OK, returnIntent);
                                EmployeesProfile.this.finish();*/
                            } else if (responseStatus.equalsIgnoreCase("400")) {

                                String message = getResources().getString(R.string.already_subscribed);
                                String type = response.optString("type");
                                if (type != null && type.equalsIgnoreCase("limit_over")) {
                                    AlertDialog.Builder builder = new AlertDialog.Builder(EmployeesProfile.this);
                                    builder.setMessage(message + "");
                                    builder.setPositiveButton("Subscribe", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            Uri uriUrl = Uri.parse("https://quickkonnect.com");
                                            Intent launchBrowser = new Intent(Intent.ACTION_VIEW, uriUrl);
                                            startActivity(launchBrowser);
                                        }
                                    });
                                    builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            EmployeesProfile.this.finish();
                                        }
                                    });

                                    builder.show();
                                } else if (msg != null && msg.equalsIgnoreCase("Please subscribe our latest plan for adding employees")) {
                                    String message1 = getResources().getString(R.string.subscribe_plane);

                                    AlertDialog.Builder builder = new AlertDialog.Builder(EmployeesProfile.this);
                                    builder.setMessage(message1 + "");
                                    builder.setPositiveButton("Subscribe", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            Uri uriUrl = Uri.parse("https://quickkonnect.com");
                                            Intent launchBrowser = new Intent(Intent.ACTION_VIEW, uriUrl);
                                            startActivity(launchBrowser);
                                        }
                                    });
                                    builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            EmployeesProfile.this.finish();
                                        }
                                    });
                                    builder.show();
                                } else {
                                    AlertDialog.Builder builder = new AlertDialog.Builder(EmployeesProfile.this);

                                    builder.setMessage(msg);
                                    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            dialogInterface.dismiss();
                                        }
                                    });
                                    builder.show();
                                }
                            } else {
                                Utilities.showToast(context, msg);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyApiRequest.showVolleyError(context, error);
                    }
                }).enqueRequest(obj, Request.Method.POST);
    }

    private void ShowSuccessDialog() {

        final Dialog dialog = new Dialog(EmployeesProfile.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View view = LinearLayout.inflate(EmployeesProfile.this, R.layout.layout_sucess_dialog_employee, null);
        dialog.setContentView(view);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        dialog.show();
        dialog.getWindow().setAttributes(lp);

        TextView tvmessage = dialog.findViewById(R.id.tvmessage);

        final GifView gifView1 = (GifView) view.findViewById(R.id.image_suc_gif);
        gifView1.setVisibility(View.VISIBLE);
        gifView1.play();
        gifView1.setGifResource(R.drawable.done);
        gifView1.getGifResource();
        try {
            Handler h = new Handler();
            h.postDelayed(new Runnable() {
                @Override
                public void run() {
                    gifView1.pause();
                }
            }, 2000);
        } catch (Exception e) {
            e.printStackTrace();
        }

        String msg = "";
        if (tv_company_name.getText().toString() != null)
            msg = "You have successfully registered " + tv_company_name.getText().toString() + " as an employee of your company. Once " + tv_company_name.getText().toString() + " accepts the request, you will be notified.";
        else
            msg = "You have successfully registered " + "this profile" + " as an employee of your company. Once " + "this profile" + " accepts the request, you will be notified.";

        tvmessage.setText(msg + "");
        TextView textView = dialog.findViewById(R.id.tv_ok);
        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                }
                //Intent returnIntent = new Intent();
                setResult(AppCompatActivity.RESULT_OK);
                EmployeesProfile.this.finish();
            }
        });

    }

    private void LoadNotificationData() {

        accept_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ViweRejectionDialog();

            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String name = tv_company_name_2.getText().toString() + "";
                Intent intent = new Intent(getApplicationContext(), Accept_Employee.class);
                intent.putExtra(Constants.KEY, "FROM_ACCEPT");
                intent.putExtra("PROFILE_NAME", name + "");
                intent.putExtra("PROFILE_DATA", Profile_Json + "");
                startActivity(intent);

            }
        });

        if (isNotiAR) {
            //tvmessage.setVisibility(View.GONE);
            tvsubmessage.setVisibility(View.GONE);
            lin_acc_reject.setVisibility(View.INVISIBLE);
        } else {
            //tvmessage.setVisibility(View.VISIBLE);
            tvsubmessage.setVisibility(View.VISIBLE);
            lin_acc_reject.setVisibility(View.VISIBLE);
        }

        getNotificationData();
    }

    private void ViweRejectionDialog() {
        final Dialog dialog = new Dialog(EmployeesProfile.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View view = LinearLayout.inflate(EmployeesProfile.this, R.layout.dialog_employee_rejectlist, null);
        dialog.setContentView(view);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        dialog.show();
        dialog.getWindow().setAttributes(lp);

        lin_emo_role_dialog_main = dialog.findViewById(R.id.lin_emo_role_dialog_main);
        radioGroup = dialog.findViewById(R.id.radio_group_role);
        close_role = dialog.findViewById(R.id.tvCancel_role);
        submit_role = dialog.findViewById(R.id.tvSubmit_role);

        RadioButton block = dialog.findViewById(R.id.role4);
        String blocktxt = "Block " + ProfileName + "";
        block.setText(blocktxt + "");

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            view.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
                @TargetApi(Build.VERSION_CODES.LOLLIPOP)
                @Override
                public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                    v.removeOnLayoutChangeListener(this);
                    revealShow(lin_emo_role_dialog_main, frame2);
                }
            });
        }
        close_role.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (dialog.isShowing())
                    dialog.dismiss();
            }
        });
        submit_role.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int selectedid = radioGroup.getCheckedRadioButtonId();
                switch (selectedid) {
                    case R.id.role1:
                        RejectEmployee(1);
                        break;
                    case R.id.role2:
                        RejectEmployee(2);
                        break;
                    case R.id.role3:
                        RejectEmployee(3);
                        break;
                    case R.id.role4:
                        BlockProfile();
                        break;
                }
                if (dialog.isShowing())
                    dialog.dismiss();
            }
        });
    }

    private void getNotificationData() {
        JSONObject obj = new JSONObject();
        try {
            obj.put("notification_id", notificationid);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        new VolleyApiRequest(context, true, VolleyApiRequest.REQUEST_BASEURL + "api/member/member_detail",
                new VolleyCallBack() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String responseStatus = response.getString("status");
                            String msg = response.getString("msg");
                            if (responseStatus.equals("200")) {
                                try {
                                    JSONObject data = response.getJSONObject("data");
                                    JSONObject user = data.getJSONObject("user");
                                    JSONObject profile = data.getJSONObject("profile");
                                    noti_u_id = data.getString("user_id");
                                    noti_prof_id = data.getString("profile_id");
                                    ProfileName = profile.getString("name");

                                    Profile_Json = profile.toString();

                                    STATUS = data.getString("status");

                                    //tvmessage.setText(ProfileName + "");
                                    //tvsubmessage.setText(ProfileName +" wants to add you as an employee");
                                    tvsubmessage.setText(ProfileName + " would like to add you as an Employee. Would you like to accept it?");
                                    //tvmessage.setVisibility(View.GONE);

                                    PojoClasses.EmployeesList emp = new Gson().fromJson(data.toString(), PojoClasses.EmployeesList.class);
                                    PojoClasses.EmployeesList.User usr = new Gson().fromJson(user.toString(), PojoClasses.EmployeesList.User.class);
                                    if (emp.getUserId() != null && !emp.getUserId().equalsIgnoreCase("null")) {
                                        id = emp.getUserId() + "";
                                    }
                                    if (emp.getId() != null && !emp.getId().equalsIgnoreCase("null")) {
                                        empid = emp.getId() + "";
                                    }
                                    if (usr.getProfilePic() != null && !usr.getProfilePic().isEmpty() && !usr.getProfilePic().equals("null")) {
                                        user_dialog_profilepic = usr.getProfilePic();
                                        Picasso.with(EmployeesProfile.this)
                                                .load(usr.getProfilePic())
                                                .error(R.drawable.ic_default_profile_photo)
                                                .placeholder(R.drawable.ic_default_profile_photo)
                                                .transform(new CircleTransform())
                                                .fit()
                                                .into(img_company_logo_main_2);
                                    } else {
                                        img_company_logo_main_2.setImageResource(R.drawable.ic_default_profile_photo);
                                    }
                                    tv_company_name_2.setText(usr.getFirstname() + " " + usr.getLastname());
                                    if (usr.getEmail() != null && !usr.getEmail().equalsIgnoreCase("") && !usr.getEmail().equalsIgnoreCase("null"))
                                        tv_about_2.setText(usr.getEmail());
                                    if (emp.getEmployeeNo() != null && !emp.getEmployeeNo().equalsIgnoreCase("") && !emp.getEmployeeNo().equalsIgnoreCase("null")) {
                                        String str = "<font color='#868686'>Employee Number :  </font><b color='#000'>" + emp.getEmployeeNo() + "</b>";
                                        tv_edt_emp_number.setText(Html.fromHtml(str));
                                    } else {
                                        tv_edt_emp_number.setVisibility(View.GONE);
                                    }
                                    if (emp.getDepartment() != null && !emp.getDepartment().equalsIgnoreCase("") && !emp.getDepartment().equalsIgnoreCase("null")) {
                                        String str = "<font color='#868686'>Department : </font><b color='#000'>" + emp.getDepartment() + "</b>";
                                        tv_edt_emp_depart.setText(Html.fromHtml(str));
                                    } else {
                                        tv_edt_emp_depart.setVisibility(View.GONE);
                                    }
                                    if (emp.getDesignation() != null && !emp.getDesignation().equalsIgnoreCase("") && !emp.getDesignation().equalsIgnoreCase("null")) {
                                        String str = "<font color='#868686'>Designation :  </font><b color='#000'>" + emp.getDesignation() + "</b>";
                                        tv_edt_designation.setText(Html.fromHtml(str));
                                    } else {
                                        tv_edt_designation.setVisibility(View.GONE);
                                    }
                                    if (emp.getValidFrom() != null && !emp.getValidFrom().equalsIgnoreCase("") && !emp.getValidFrom().equalsIgnoreCase("null")) {
                                        String dobConvert = emp.getValidFrom();
                                        String[] separated = dobConvert.split("/");
                                        String year = separated[0];
                                        String month = separated[1];
                                        String day = separated[2];
                                        dobConvert = day + "/" + month + "/" + year;
                                        String str = "<font color='#868686'>Valid from :  </font><b color='#000'>" + dobConvert + "</b>";
                                        tv_edt_validate_from.setText(Html.fromHtml(str));
                                    } else {
                                        tv_edt_validate_from.setVisibility(View.GONE);
                                    }
                                    if (emp.getValidTo() != null && !emp.getValidTo().equalsIgnoreCase("") && !emp.getValidTo().equalsIgnoreCase("null")) {
                                        String dobConvert = emp.getValidTo();
                                        String[] separated = dobConvert.split("/");
                                        String year = separated[0];
                                        String month = separated[1];
                                        String day = separated[2];
                                        dobConvert = day + "/" + month + "/" + year;
                                        String str = "<font color='#868686'>Valid to : </font><b color='#000'>" + dobConvert + "</b>";
                                        tv_edt_validate_to.setText(Html.fromHtml(str));
                                    } else {
                                        tv_edt_validate_to.setVisibility(View.GONE);
                                    }

                                    frame1.setVisibility(View.GONE);
                                    frame2.setVisibility(View.VISIBLE);
                                    frame_detail_1.setVisibility(View.GONE);
                                    frame_detail_2.setVisibility(View.VISIBLE);
                                    //tvmessage.setVisibility(View.VISIBLE);
                                    tvsubmessage.setVisibility(View.VISIBLE);

                                    if (isNotiAR) {
                                        //tvmessage.setVisibility(View.GONE);
                                        tvsubmessage.setVisibility(View.GONE);
                                        lin_acc_reject.setVisibility(View.INVISIBLE);
                                    } /*else {
                                        tvmessage.setVisibility(View.VISIBLE);
                                        tvsubmessage.setVisibility(View.VISIBLE);
                                        lin_acc_reject.setVisibility(View.VISIBLE);
                                    }*/

                                    if (STATUS != null && !STATUS.equalsIgnoreCase("P")) {
                                        tvsubmessage.setVisibility(View.GONE);
                                        lin_acc_reject.setVisibility(View.INVISIBLE);
                                    }

                                    try {
                                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                            cord_emp.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
                                                @TargetApi(Build.VERSION_CODES.LOLLIPOP)
                                                @Override
                                                public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                                                    v.removeOnLayoutChangeListener(this);
                                                    revealShow(card_2_emp, frame2);
                                                }
                                            });
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }

                                } catch (Exception e) {
                                    lin_acc_reject.setVisibility(View.INVISIBLE);
                                    e.printStackTrace();
                                }
                            } else {
                                lin_acc_reject.setVisibility(View.INVISIBLE);
                                Utilities.showToast(getApplicationContext(), msg);
                            }
                        } catch (Exception e) {
                            lin_acc_reject.setVisibility(View.INVISIBLE);
                            Utilities.showToast(getApplicationContext(), "Profile not found");
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        lin_acc_reject.setVisibility(View.INVISIBLE);
                        VolleyApiRequest.showVolleyError(context, error);
                    }
                }).enqueRequest(obj, Request.Method.POST);
    }

    private void RejectEmployee(int i) {
        JSONObject obj = new JSONObject();
        try {
            obj.put("status", "R");
            obj.put("profile_id", noti_prof_id);
            obj.put("user_id", noti_u_id);
            obj.put("reason", i + "");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        new VolleyApiRequest(context, true, VolleyApiRequest.REQUEST_BASEURL + "api/member/status",
                new VolleyCallBack() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String responseStatus = response.getString("status");
                            String msg = response.getString("msg");
                            Utilities.showToast(context, msg);
                            if (responseStatus.equals("200")) {
                                Intent returnIntent = new Intent();
                                returnIntent.putExtra("notification_id", notificationid + "");
                                setResult(Activity.RESULT_OK, returnIntent);
                                finish();
                                EmployeesProfile.this.finish();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyApiRequest.showVolleyError(context, error);
                    }
                }).enqueRequest(obj, Request.Method.POST);
    }

    private void BlockProfile() {
        JSONObject obj = new JSONObject();
        try {
            obj.put("status", "B");
            obj.put("profile_id", noti_prof_id);
            obj.put("user_id", noti_u_id);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        new VolleyApiRequest(context, true, VolleyApiRequest.REQUEST_BASEURL + "api/member/status",
                new VolleyCallBack() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String responseStatus = response.getString("status");
                            String msg = response.getString("msg");
                            Utilities.showToast(context, msg);
                            if (responseStatus.equals("200")) {
                                Intent returnIntent = new Intent();
                                setResult(AppCompatActivity.RESULT_OK, returnIntent);
                                EmployeesProfile.this.finish();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyApiRequest.showVolleyError(context, error);
                    }
                }).enqueRequest(obj, Request.Method.POST);
    }

    private void AcceptEmployee() {
        JSONObject obj = new JSONObject();
        try {
            obj.put("status", "A");
            obj.put("profile_id", noti_prof_id);
            obj.put("user_id", noti_u_id);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        new VolleyApiRequest(context, true, VolleyApiRequest.REQUEST_BASEURL + "api/member/status",
                new VolleyCallBack() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String responseStatus = response.getString("status");
                            String msg = response.getString("msg");
                            Utilities.showToast(context, msg);
                            if (responseStatus.equals("200")) {
                                Intent returnIntent = new Intent();
                                setResult(AppCompatActivity.RESULT_OK, returnIntent);
                                EmployeesProfile.this.finish();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyApiRequest.showVolleyError(context, error);
                    }
                }).enqueRequest(obj, Request.Method.POST);
    }

    private void ShowPopup(final String empNumber, final String empDepart, final String empDesignation, final String finalStartdate, final String finalEnddate) {
        final Dialog dialog = new Dialog(EmployeesProfile.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View view = LinearLayout.inflate(EmployeesProfile.this, R.layout.dialog_transfer_profile, null);
        dialog.setContentView(view);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        dialog.show();
        dialog.getWindow().setAttributes(lp);

        email = dialog.findViewById(R.id.edt_email_transfer);
        email = dialog.findViewById(R.id.edt_email_transfer);
        title = dialog.findViewById(R.id.tvAlert_dg);
        no_user = dialog.findViewById(R.id.tv_nouser_transfer);
        cancel = dialog.findViewById(R.id.tvCancel_transfer);
        search = dialog.findViewById(R.id.tvSearch_transfer);
        confirm = dialog.findViewById(R.id.tvConfirm_transfer);
        invite = dialog.findViewById(R.id.tvInvite_transfer);
        username = dialog.findViewById(R.id.tv_name_transfer);
        profileImage = dialog.findViewById(R.id.img_profile_transfer);
        linear_profile = dialog.findViewById(R.id.lin_profile_transfer);
        search_email = dialog.findViewById(R.id.edt_search_mail_transfer);
        search_icon = dialog.findViewById(R.id.img_search_transfer);
        lin_search = dialog.findViewById(R.id.lin_search_transfer);

        String msg = "";
        if (!tv_company_name.getText().toString().equalsIgnoreCase(""))
            msg = "Are you sure want to add " + tv_company_name.getText().toString() + " to your company?";
        else
            msg = "Are you sure want to add this employee to your company?";
        title.setText(msg);

        search.setVisibility(View.GONE);
        title.setVisibility(View.VISIBLE);
        cancel.setVisibility(View.VISIBLE);
        invite.setVisibility(View.GONE);
        confirm.setVisibility(View.VISIBLE);
        linear_profile.setVisibility(View.VISIBLE);
        lin_search.setVisibility(View.GONE);
        email.setVisibility(View.GONE);
        no_user.setVisibility(View.GONE);
        search_email.setText("");
        username.setText("");

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (dialog.isShowing())
                    dialog.dismiss();
            }
        });
        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                functionAddEmployee(empNumber, empDepart, empDesignation, finalStartdate, finalEnddate);
            }
        });

        if (user_dialog_profilepic != null && !user_dialog_profilepic.isEmpty() && !user_dialog_profilepic.equals("null")) {
            Picasso.with(EmployeesProfile.this)
                    .load(user_dialog_profilepic)
                    .error(R.drawable.ic_default_profile_photo)
                    .placeholder(R.drawable.ic_default_profile_photo)
                    .transform(new CircleTransform())
                    .fit()
                    .into(profileImage);
        } else {
            profileImage.setImageResource(R.drawable.ic_default_profile_photo);
        }
        username.setText(tv_company_name.getText().toString() + "");
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void revealShow(final CardView lin, View view) {

        int w = lin.getWidth();
        int h = lin.getHeight();

        int endRadius = (int) Math.hypot(w, h);

        int cx = (int) (view.getX() + (view.getWidth() / 2));
        int cy = (int) (view.getY()) + view.getHeight() + 56;

        Animator revealAnimator = ViewAnimationUtils.createCircularReveal(lin, cx, cy, 5f, endRadius);
        lin.setVisibility(View.VISIBLE);
        revealAnimator.setDuration(800);
        revealAnimator.start();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void revealShow(final LinearLayout lin, View view) {

        int w = lin.getWidth();
        int h = lin.getHeight();

        int endRadius = (int) Math.hypot(w, h);

        int cx = (int) (view.getX() + (view.getWidth() / 2));
        int cy = (int) (view.getY()) + view.getHeight() + 56;

        Animator revealAnimator = ViewAnimationUtils.createCircularReveal(lin, cx, cy, 5f, endRadius);
        lin.setVisibility(View.VISIBLE);
        revealAnimator.setDuration(800);
        revealAnimator.start();
    }
}