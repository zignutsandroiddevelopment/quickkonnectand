package com.quickkonnect;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by ZTLAB03 on 02-11-2017.
 */

public class ProfileScanResultAdapter extends RecyclerView.Adapter<ProfileScanResultAdapter.ViewHolder> implements View.OnClickListener {

    ArrayList<PojoClasses.SocialScanResult> socialScanResults;
    static Activity mActivity;

    public ProfileScanResultAdapter(Activity activity, ArrayList<PojoClasses.SocialScanResult> list) {
        this.socialScanResults = list;
        mActivity = activity;
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.custom_social_scan_result_view, null);

        ProfileScanResultAdapter.ViewHolder viewHolder = new ProfileScanResultAdapter.ViewHolder(itemLayoutView);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

       /* PojoClasses.SocialScanResult  scanResult = socialScanResults.get(position);

        if (scanResult.getSocial_name() != null && !scanResult.getSocial_name() .isEmpty() && !scanResult.getSocial_name().equals("null")) {

            holder.tv_social_title.setText(scanResult.getSocial_name());
        }else {
            holder.tv_social_title.setText("");
            holder.tv_social_title.setVisibility(View.GONE);
        }



        if (scanResult.getSocial_friends_mutual() != null && !scanResult.getSocial_friends_mutual() .isEmpty() && !scanResult.getSocial_friends_mutual().equals("null")) {

            String first = "<font color='#414141'>Mutual:</font>";
            String sourceString = "<b>" +  first  + "</b> " + scanResult.getSocial_friends_mutual();
            holder.tv_friends_mutual.setText(Html.fromHtml(sourceString));
        }else {
            holder.tv_friends_mutual.setText("");
            holder.tv_friends_mutual.setVisibility(View.GONE);
        }


        if (scanResult.getSocial_friends_total() != null && !scanResult.getSocial_friends_total() .isEmpty() && !scanResult.getSocial_friends_total().equals("null")) {

            String first = "<font color='#414141'>Total:</font>";
            String sourceString = "<b>" +  first  + "</b> " + scanResult.getSocial_friends_total();
            holder.tv_friends_total.setText(Html.fromHtml(sourceString));
        }else {
            holder.tv_friends_total.setText("");
            holder.tv_friends_total.setVisibility(View.GONE);
        }

        if (scanResult.getSocial_event_total() != null && !scanResult.getSocial_event_total() .isEmpty() && !scanResult.getSocial_event_total().equals("null")) {

            String first = "<font color='#414141'>Total:</font>";
            String sourceString = "<b>" +  first  + "</b> " + scanResult.getSocial_event_total();
            holder.tv_events_total.setText(Html.fromHtml(sourceString));

        }else {
            holder.tv_events_total.setText("");
            holder.tv_events_total.setVisibility(View.GONE);
        }

        if (scanResult.getSocial_event_comman() != null && !scanResult.getSocial_event_comman() .isEmpty() && !scanResult.getSocial_event_comman().equals("null")) {

            String first = "<font color='#414141'>Comman:</font>";
            String sourceString = "<b>" +  first  + "</b> " + scanResult.getSocial_event_comman();
            holder.tv_events_comman.setText(Html.fromHtml(sourceString));
        }else {
            holder.tv_events_comman.setText("");
            holder.tv_events_comman.setVisibility(View.GONE);
        }

        if (scanResult.getSocial_interest() != null && !scanResult.getSocial_interest() .isEmpty() && !scanResult.getSocial_interest().equals("null")) {

            String first = "<font color='#414141'>Interest:</font>";
            String sourceString = "<b>" +  first  + "</b> " + scanResult.getSocial_interest();
            holder.tv_interest.setText(Html.fromHtml(sourceString));
        }else {
            holder.tv_interest.setText("");
            holder.tv_interest.setVisibility(View.GONE);
        }

        if (scanResult.getSocial_tagged_places() != null && !scanResult.getSocial_tagged_places() .isEmpty() && !scanResult.getSocial_tagged_places().equals("null")) {

            String first = "<font color='#414141'>Tagged Places:</font>";
            String sourceString = "<b>" +  first  + "</b> " + scanResult.getSocial_tagged_places();
            holder.tv_tagged_places.setText(Html.fromHtml(sourceString));
        }else {
            holder.tv_tagged_places.setText("");
            holder.tv_tagged_places.setVisibility(View.GONE);
        }

        holder.btn_connect_view.setTag(scanResult);
        holder.btn_connect_view.setOnClickListener(this);
*/    }

    @Override
    public int getItemCount() {
        return socialScanResults.size();
    }

    @Override
    public void onClick(View view) {
        PojoClasses.SocialScanResult scanResult = (PojoClasses.SocialScanResult) view.getTag();

    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_social_title,tv_friends_title,tv_friends_total,tv_friends_mutual,tv_events_title,tv_events_total,tv_events_comman,tv_interest,tv_tagged_places;
        Button btn_connect_view;


        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            tv_social_title = itemLayoutView.findViewById(R.id.tv_social_title);
            tv_friends_title = itemLayoutView.findViewById(R.id.tv_friends_title);
            tv_friends_total = itemLayoutView.findViewById(R.id.tv_friends_total);
            tv_friends_mutual = itemLayoutView.findViewById(R.id.tv_friends_mutual);
            tv_events_title = itemLayoutView.findViewById(R.id.tv_events_title);
            tv_events_total = itemLayoutView.findViewById(R.id.tv_events_total);
            tv_events_comman = itemLayoutView.findViewById(R.id.tv_events_comman);
            tv_interest = itemLayoutView.findViewById(R.id.tv_interest);
            tv_tagged_places = itemLayoutView.findViewById(R.id.tv_tagged_places);
            btn_connect_view = itemLayoutView.findViewById(R.id.btn_connect_view);




        }
    }
}
