package com.quickkonnect;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.Chat.NewChatActivity;
import com.activities.ManageQKProfilesActivity;
import com.activities.MatchProfileActivity;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.services.DeletProfileService;
import com.services.LoadMyContacts;
import com.services.LoadMyFollowers;
import com.services.LoadMyFollowing;
import com.services.LoadNotificationsService;
import com.services.LoadProfiles;
import com.utilities.Constants;
import com.volleyapirequest.VolleyApiRequest;

import java.util.Map;

/**
 * Created by ZTLAB03 on 14-11-2017.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "FCM Service";
    public static NotificationManager notificationManager;

    public String receipient_id = "", chatref = "", image = "", users = "", userid = "", usredeviceid = "", from_token = "", to_token = "";

    //private static QKPreferences qkPreferences;
    private String message = "Quickkonnect";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // TODO: Handle FCM messages here.
        Log.d(TAG, "From: " + remoteMessage.getFrom() + " __\n" + remoteMessage.getData() + "");
        Log.d(TAG, "Notification Message Body: " + remoteMessage.getData().get("body") + "");

        Map<String, String> data = remoteMessage.getData();
        if (data != null && data.size() != 0) {
            try {
                String msg = remoteMessage.getData().get("body");
                if (msg != null && !msg.equalsIgnoreCase("") && !msg.equalsIgnoreCase("null")) {
                    msg = remoteMessage.getData().get("body");
                } else {
                    msg = remoteMessage.getNotification().getBody();
                }
                sendNotification(msg + "", data);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            if (remoteMessage.getNotification() != null) {
                Map<String, String> data1 = remoteMessage.getData();
            }
        }
    }

    private void sendNotification(String messageBody, Map<String, String> data) {

        String notification_type = data.get("notification_type");
        String notification_id = data.get("notification_id");
        String user_id = data.get("user_id");
        String profile_id = data.get("profile_id");
        String sender_id = "";
        String unique_code = data.get("unique_code");
        String fname = data.get("nickname");
        String profilepic = data.get("profile_picture");
        String receiveruniquecode = data.get("receiver_unique_code");

        /*
        if (notification_type != null && notification_type.equals(Constants.NOTIFICATION_CHAT)) {

        }*/

        try {
            LocalBroadcastManager localBroadcastManager = LocalBroadcastManager.getInstance(getApplicationContext());
            Intent localIntent = new Intent(Constants.ACTION_REFRESH_USERDATA);
            localBroadcastManager.sendBroadcast(localIntent);
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            startService(new Intent(getApplicationContext(), LoadNotificationsService.class));
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (notification_type.equalsIgnoreCase(Constants.NOTIFICATION_TYPE_PROFAPPROVED) || notification_type.equalsIgnoreCase(Constants.NOTIFICATION_TYPE_PROFREJECT)) {
            sender_id = data.get("profile_id");
            unique_code = data.get("unique_code");
        } else {
            sender_id = data.get("sender_id");
        }

        Intent intent = null;
        switch (notification_type) {
            case Constants.NOTIFICATION_CHAT:
                message = "New message from " + fname + "";
                String image = VolleyApiRequest.REQUEST_BASEURL_PROFILE_PIC + profilepic + "";
                intent = new Intent(this, NewChatActivity.class);
                intent.putExtra("fJID", unique_code + "@quickkonnect.com");
                intent.putExtra("fNickName", fname + "");
                intent.putExtra("fAvatarByte", image + "");
                intent.putExtra("mJID", receiveruniquecode + "@quickkonnect.com");
                intent.putExtra("KEYFROM", "NOTIFICATIONCHAT");
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                        Intent.FLAG_ACTIVITY_SINGLE_TOP |
                        Intent.FLAG_ACTIVITY_NEW_TASK);

                break;

//            case Constants.NOTIFICATION_TYPE_CONTREQ:
//                intent = new Intent(this, DownloadContactListActivity.class);
//                intent.putExtra("sender_id", sender_id);
//                try {
//                    intent.putExtra("notification_id", Integer.parseInt(notification_id));
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
//                        Intent.FLAG_ACTIVITY_SINGLE_TOP |
//                        Intent.FLAG_ACTIVITY_NEW_TASK);
//                break;

            case Constants.NOTIFICATION_TYPE_APPROVED:
//                intent = new Intent(this, ApproveContactListActivity.class);
//                intent.putExtra("scan_user_id", user_id);
//                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
//                        Intent.FLAG_ACTIVITY_SINGLE_TOP |
//                        Intent.FLAG_ACTIVITY_NEW_TASK);
//                break;

            case Constants.NOTIFICATION_SCAN_APRROVED:
            case Constants.NOTIFICATION_TYPE_SCAN:
                intent = new Intent(this, MatchProfileActivity.class);
                intent.putExtra("unique_code", data.get("unique_code"));
                intent.putExtra(Constants.KEY, Constants.KEY_OTHER);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP |
                        Intent.FLAG_ACTIVITY_NEW_TASK);
                break;
            case Constants.NOTIFICATION_TYPE_FOLLOW:
                intent = new Intent(this, NewScanProfileActivity.class);
                try {
                    intent.putExtra("unique_code", "" + data.get("unique_code"));
                    intent.putExtra("profile_id", "" + data.get("profile_id"));
                    intent.putExtra("user_id", "" + data.get("user_id"));
                    intent.putExtra("notification_id", "" + Integer.parseInt(notification_id));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                intent.putExtra(Constants.KEY, "");
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                break;

            case Constants.NOTIFICATION_TYPE_PROFAPPROVED:
            case Constants.NOTIFICATION_TYPE_PROFREJECT:
                /*intent = new Intent(this, NewScanProfileActivity.class);
                intent.putExtra("unique_code", "" + data.get("unique_code"));
                intent.putExtra("profile_id", "" + data.get("profile_id"));
                intent.putExtra("user_id", "" + data.get("user_id"));
                intent.putExtra("notification_id", "" + Integer.parseInt(notification_id));
                intent.putExtra(Constants.KEY, Constants.KEY_FROM_MANAGEPROFILES);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP |
                        Intent.FLAG_ACTIVITY_NEW_TASK);*/
                intent = new Intent(this, ManageQKProfilesActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                break;
            case Constants.NOTIFICATION_TYPE_TRANSFER:
                intent = new Intent(this, NewScanProfileActivity.class);
                try {
                    intent.putExtra("unique_code", "" + data.get("unique_code"));
                    intent.putExtra("profile_id", "" + profile_id);
                    intent.putExtra("user_id", "" + data.get("user_id"));
                    intent.putExtra("notification_id", "" + notification_id);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                intent.putExtra(Constants.KEY, Constants.KEY_FROM_NOTIFICATIONS);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                break;
            case Constants.NOTIFICATION_TYPE_TRANSFER_APPROVE:

                try {
                    startService(new Intent(getApplicationContext(), DeletProfileService.class).putExtra("UNIQUECODE", data.get("unique_code") + ""));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                /*intent = new Intent(this, NewScanProfileActivity.class);
                try {
                    intent.putExtra("unique_code", "" + data.get("unique_code"));
                    intent.putExtra("profile_id", "" + profile_id);
                    intent.putExtra("user_id", "" + data.get("user_id"));
                    intent.putExtra("notification_id", "" + notification_id);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                intent.putExtra(Constants.KEY, "");
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);*/
                break;
            case Constants.NOTIFICATION_TYPE_TRANSFER_REJECT:
                intent = new Intent(this, NewScanProfileActivity.class);
                try {
                    intent.putExtra("unique_code", "" + data.get("unique_code"));
                    intent.putExtra("profile_id", "" + data.get("profile_id"));
                    intent.putExtra("user_id", "" + data.get("user_id"));
                    intent.putExtra("notification_id", "" + notification_id);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                intent.putExtra(Constants.KEY, "");
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                break;
            case Constants.NOTIFICATION_TYPE_EMPLOYY_REQUEST:
                intent = new Intent(this, EmployeesProfile.class);
                try {
                    intent.putExtra("notification_id", "" + Integer.parseInt(notification_id));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                intent.putExtra("KEY", "NOTIFICATION");
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                break;
            case Constants.NOTIFICATION_TYPE_EMPLOYY_ACCEPT:
                /*intent = new Intent(this, EmployeesProfile.class);
                try {
                    intent.putExtra("notification_id", "" + Integer.parseInt(notification_id));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                intent.putExtra("KEY", "NOTIFICATION_AR");
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);*/
                break;
            case Constants.NOTIFICATION_TYPE_EMPLOYY_REJECT:
                intent = new Intent(this, EmployeesProfile.class);
                try {
                    intent.putExtra("notification_id", "" + Integer.parseInt(notification_id));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                intent.putExtra("KEY", "NOTIFICATION_AR");
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                break;
            case Constants.NOTIFICATION_TYPE_CHANGE_ROLE:
                intent = new Intent(this, NewScanProfileActivity.class);
                try {
                    intent.putExtra("unique_code", "" + data.get("unique_code"));
                    intent.putExtra("profile_id", "" + data.get("profile_id"));
                    intent.putExtra("user_id", "" + data.get("user_id"));
                    intent.putExtra("notification_id", "" + Integer.parseInt(notification_id));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                intent.putExtra(Constants.KEY, "");
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                break;
           /* case Constants.NOTIFICATION_TYPE_TRIAL_EXPIRATION:
                intent = new Intent(this, NewScanProfileActivity.class);
                break;
            case Constants.NOTIFICATION_TYPE_CHANGE_PLAN_CANCEL:
                break;
            case Constants.NOTIFICATION_TYPE_CHANGE_PAYMENT_FAIL:
                break;
            case Constants.NOTIFICATION_TYPE_CHANGE_EMPLOYEE_DELETE:
                break;
            case Constants.NOTIFICATION_TYPE_PROFILE_DELETE:
                break;*/
            default:
                intent = new Intent(this, DashboardActivity.class);
                intent.putExtra(Constants.KEY, Constants.KEY_GENERAL_NOTIFICATION);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                break;
        }

        showpush(intent, messageBody);

        try {
            startService(new Intent(getApplicationContext(), LoadMyContacts.class));
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            startService(new Intent(getApplicationContext(), LoadMyFollowers.class));
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            startService(new Intent(getApplicationContext(), LoadMyFollowing.class));
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            startService(new Intent(getApplicationContext(), LoadProfiles.class));
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    String CHANNEL_ID = "my_channel_01";// The id of the channel.

    public void showpush(Intent intent, String messageBody) {
        Log.e("Message-Body", "" + messageBody);
        int requestCode = (int) System.currentTimeMillis();
        if (intent == null) {
            intent = new Intent(this, SplashActivity.class);
        }
        PendingIntent pendingIntent = PendingIntent.getActivity(this, requestCode, intent, 0);
        Uri sound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder noBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.notification_logo)
                .setContentTitle(message + "")
                .setStyle(new NotificationCompat.BigTextStyle().bigText(messageBody))
                .setContentText(messageBody)
                .setSound(sound)
                .setChannelId(CHANNEL_ID)
                .setAutoCancel(true)
                .setContentIntent(pendingIntent);

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            // only for gingerbread and newer versions
            createChannel();
        }

        notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(requestCode, noBuilder.build()); //0 = ID of notification
    }

    @RequiresApi(26)
    public void createChannel() {
        try {
            CharSequence name = getString(R.string.app_name);// The user-visible name of the channel.
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
            NotificationManager mNotificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            mNotificationManager.createNotificationChannel(mChannel);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
