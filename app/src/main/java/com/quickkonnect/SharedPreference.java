package com.quickkonnect;

import android.content.SharedPreferences;

public class SharedPreference {

    public static final String PREF_NAME = "Quick Konnect App";

    public static final String CURRENT_USER_ID = "id";
    public static final String CURRENT_USER_NAME= "name";
    public static final String CURRENT_USER_FIRSTTIME= "firsttime";
    public  static final String CURRENT_USER_REGISTER= "register";
    public  static final String CURRENT_UNIQU_QK_TAG_NUMBER ="unique_code";
    public static final String CURRENT_USER_EMAIL="email";
    public  static final String CURRENT_USER_FULLNAME="fullname";
    public  static final String CURRENT_USER_PROFILE_PIC="profile_pic";
    public static final String CURRENT_USER_URL="url";

    static final String isOpenActivity ="isOpenActivity";
    public static final String isUserProfile ="userprofile";
    public static final String isQkTag ="qkTag";
    public static final String isDashboard ="dashboard";
    public static final String isLogin ="login";

    public static final String BackgroundColor ="background_color";
    public static final String BorderColor ="border_color";
    public static final String PatternColor ="pattern_color";
    public static final String NOTIFICATION_STATUS ="notification_status";
    public static final String CONTACT_ALL ="contact_all";
    public static final String CONTACT_WEAK ="contact_week";
    public static final String CONTACT_MONTH ="contact_month";
    public static final String NOTIFICATION_UNREAD_COUNT ="notification_unread";

    public static final String FCM_DEVICE_REGISTRATION_TOKEN="fcm_device_token";


//    static final String passwordPattern = "^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=]).{8,32}$";
//    static final String strExpression = "^[789]\\d{9}$";

   /* ^                 # start-of-string
            (?=.*[0-9])       # a digit must occur at least once
            (?=.*[a-z])       # a lower case letter must occur at least once
            (?=.*[A-Z])       # an upper case letter must occur at least once
            (?=.*[@#$%^&+=])  # a special character must occur at least once you can replace with your special characters
            (?=\\S+$)          # no whitespace allowed in the entire string
            .{4,}             # anything, at least six places though
    $                 # end-of-string*/


    static final String gcmNotificationKey = "gcm_notification_key";
}
