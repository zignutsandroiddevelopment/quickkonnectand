package com.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.Chat.NewChatActivity;
import com.activities.MatchProfileActivity;
import com.adapter.FollowerContactAdapter;
import com.adapter.FollowingContactAdapter;
import com.adapter.NewContactListAdapter;
import com.adapter.ProfileAdapter;
import com.interfaces.OnContactClick;
import com.interfaces.ProfileSelection;
import com.models.contacts.ContactsPojo;
import com.models.contacts.Datum;
import com.models.logindata.ModelLoggedData;
import com.models.logindata.ModelLoggedUser;
import com.quickkonnect.DashboardActivity;
import com.quickkonnect.EmployeeDetails;
import com.quickkonnect.NewScanProfileActivity;
import com.quickkonnect.PojoClasses;
import com.quickkonnect.R;
import com.realmtable.Contacts;
import com.realmtable.Follower_contact;
import com.realmtable.Following_contact;
import com.realmtable.MyProfiles;
import com.realmtable.RealmController;
import com.retrofitservice.ApiUtils;
import com.retrofitservice.SOService;
import com.utilities.Constants;
import com.utilities.QKPreferences;
import com.utilities.RecyclerItemClickListener;
import com.utilities.Utilities;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import io.realm.RealmResults;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ZTLAB09 on 12-12-2017.
 */

public class FragContacts extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    private static Context context;
    private SOService mService;
    private View rootView;
    private static QKPreferences qkPreferences;
    private FrameLayout frame_export;
    private ProgressDialog mProgressDialog;
    private BroadcastReceiver receiver;
    boolean isMultiSelect = false;
    boolean isMultiSelect_followers = false;
    public static SwipeRefreshLayout mSwipeRefreshLayout;
    public static ImageView img_filter;

    public int selected = 0;
    public int selected_rec = 0;
    public boolean isSelectedFromRecycler = false;
//    ProgressDialog pd;

    //private Animation fabOpenAnimation;
    //private Animation fabCloseAnimation;

    Dialog dialog;
    ProfileAdapter adapter_profile;
    private List<MyProfiles> ProfileList;
    ImageView close_img_dialog_contact;
    FrameLayout lin_dialog_main_contact;
    TextView tv_title_contact_dialog;
    RecyclerView rec_dialog_profile;
    LinearLayout tv_filter_atoz, tv_filter_ztoa, tv_filter_Orderdby, lin_profiles_dialog, lin_profilestypes_dialog, tv_profiletype_compony, tv_profiletype_enterprise, tv_profiletype_celebrity;
    ImageView tv_filter_clearfilters, imag_select_atoz, imag_select_ztoa, imag_select_orderby, imag_select_compony, imag_select_enterprise, imag_select_celebrity;
    private TabLayout tabLayout;

    public static RelativeLayout relativeLayoutContacts, relativeLayoutFollowers, relativeLayoutFollowing;
    public static RecyclerView recycleContacts, recycleFollowers, recycleFollowing;
    private static TextView tv_no_record_found_Contacts, tv_no_record_found_Follower, tv_no_record_found_Following;

    public static ArrayList<Datum> mlist_multiselect;
    public static ArrayList<Datum> mlist;
    public static NewContactListAdapter newContactListAdapter;

    public static ArrayList<PojoClasses.FollowersList_Contact> followerList_multiselect;
    public static ArrayList<PojoClasses.FollowersList_Contact> followerList;
    public static FollowerContactAdapter followeradapter;

    public static ArrayList<PojoClasses.FollowingList_Contact> followingList;
    public static FollowingContactAdapter followingadapter;

    public static FragContacts getInstance() {
        FragContacts fragChats = new FragContacts();
        return fragChats;
    }

    String OpenItem = "1";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.frag_contacts, null);
        context = getActivity();

        mService = ApiUtils.getSOService();

        qkPreferences = QKPreferences.getInstance(getActivity());


        //fabOpenAnimation = AnimationUtils.loadAnimation(getActivity(), R.anim.fab_open);
        //fabCloseAnimation = AnimationUtils.loadAnimation(getActivity(), R.anim.fab_close);

        tabLayout = (TabLayout) rootView.findViewById(R.id.tabs);

        relativeLayoutContacts = (RelativeLayout) rootView.findViewById(R.id.relativeLayoutContacts);
        relativeLayoutFollowers = (RelativeLayout) rootView.findViewById(R.id.relativeLayoutFollowers);
        relativeLayoutFollowing = (RelativeLayout) rootView.findViewById(R.id.relativeLayoutFollowing);

        recycleContacts = (RecyclerView) rootView.findViewById(R.id.recycleContacts);
        //recycleContacts.addItemDecoration(new SimpleDividerItemDecoration(context));
        recycleFollowers = (RecyclerView) rootView.findViewById(R.id.recycleFollowers);
        //recycleFollowers.addItemDecoration(new SimpleDividerItemDecoration(context));
        recycleFollowing = (RecyclerView) rootView.findViewById(R.id.recycleFollowing);
        //recycleFollowing.addItemDecoration(new SimpleDividerItemDecoration(context));

        recycleContacts.setLayoutManager(new LinearLayoutManager(getActivity()));
        recycleFollowers.setLayoutManager(new LinearLayoutManager(getActivity()));
        recycleFollowing.setLayoutManager(new LinearLayoutManager(getActivity()));

        tv_no_record_found_Contacts = rootView.findViewById(R.id.tv_no_record_found_Contacts);
        tv_no_record_found_Follower = rootView.findViewById(R.id.tv_no_record_found_Follower);
        tv_no_record_found_Following = rootView.findViewById(R.id.tv_no_record_found_Following);

        frame_export = rootView.findViewById(R.id.frame_export);
        img_filter = rootView.findViewById(R.id.img_filter);

        mSwipeRefreshLayout = rootView.findViewById(R.id.swipeRefreshLayout);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary,
                android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_blue_dark);

        // Open Global Search
        rootView.findViewById(R.id.linearSearch).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((DashboardActivity) getActivity()).setupGlobalSearch();
            }
        });

        // Open Filter
        img_filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setFilter();
            }
        });

        // Export Data
        frame_export.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (frame_export.getVisibility() == View.VISIBLE) {
                    //frame_export.startAnimation(fabCloseAnimation);
                    frame_export.setVisibility(View.GONE);
                }
                new ExportToCsv().execute();
            }
        });

        tabLayout.addTab(tabLayout.newTab().setText("Contacts"));
        tabLayout.addTab(tabLayout.newTab().setText("Followers"));
        tabLayout.addTab(tabLayout.newTab().setText("Following"));
        changeTabsFont();

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                Log.e("onTabSelected", "" + tab.getPosition());

                mlist_multiselect.clear();
                isMultiSelect = false;
                followerList_multiselect.clear();
                isMultiSelect_followers = false;

                if (newContactListAdapter != null)
                    newContactListAdapter.notifyDataSetChanged();

                if (followeradapter != null)
                    followeradapter.notifyDataSetChanged();

                if (tab.getPosition() == 0) {
                    OpenItem = "1";
                    relativeLayoutContacts.setVisibility(View.VISIBLE);
                    relativeLayoutFollowers.setVisibility(View.GONE);
                    relativeLayoutFollowing.setVisibility(View.GONE);

                } else if (tab.getPosition() == 1) {
                    OpenItem = "2";
                    relativeLayoutContacts.setVisibility(View.GONE);
                    relativeLayoutFollowers.setVisibility(View.VISIBLE);
                    relativeLayoutFollowing.setVisibility(View.GONE);

                } else if (tab.getPosition() == 2) {
                    OpenItem = "3";
                    relativeLayoutContacts.setVisibility(View.GONE);
                    relativeLayoutFollowers.setVisibility(View.GONE);
                    relativeLayoutFollowing.setVisibility(View.VISIBLE);
                }

                //if (frame_export.getVisibility() == View.VISIBLE) {
                // frame_export.startAnimation(fabCloseAnimation);
                frame_export.setVisibility(View.GONE);
                //}
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                Log.e("onTabUnselected", "" + tab.getPosition());
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                Log.e("onTabReselected", "" + tab.getPosition());
            }
        });

        relativeLayoutContacts.setVisibility(View.VISIBLE);

        setupLongClickListner();

        showContacts("name", true);
        showFollowers("name", true);
        showFollowing(true);

        IntentFilter filter = new IntentFilter();
        filter.addAction("BROADCAST_FROM_SCAN_QK");

        try {
            receiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    showContacts("name", true);
                    try {
                        ((DashboardActivity) getActivity()).updateRecentlyContact();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    showFollowers("name", true);
                    showFollowing(true);
                }
            };
            getActivity().registerReceiver(receiver, filter);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return rootView;
    }

    private void changeTabsFont() {
        try {
            ViewGroup vg = (ViewGroup) tabLayout.getChildAt(0);
            int tabsCount = vg.getChildCount();
            for (int j = 0; j < tabsCount; j++) {
                ViewGroup vgTab = (ViewGroup) vg.getChildAt(j);
                int tabChildsCount = vgTab.getChildCount();
                for (int i = 0; i < tabChildsCount; i++) {
                    View tabViewChild = vgTab.getChildAt(i);
                    if (tabViewChild instanceof TextView) {
                        ((TextView) tabViewChild).setAllCaps(false);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onDestroy() {
        try {
            if (receiver != null) {
                getActivity().unregisterReceiver(receiver);
                receiver = null;
            }
            super.onDestroy();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setupLongClickListner() {

        recycleFollowers.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(),
                recycleFollowers, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, final int position) {
                if (followerList_multiselect != null && followerList_multiselect.size() == 0) {
                    isMultiSelect_followers = false;
                    followerList_multiselect.clear();
                    frame_export.setVisibility(View.GONE);
                }
                if (isMultiSelect_followers)
                    multi_select_follower(position);
            }

            @Override
            public void onItemLongClick(View view, final int position) {

                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (!isMultiSelect_followers) {
                            followerList_multiselect.clear();
                            isMultiSelect_followers = true;
                            frame_export.setVisibility(View.VISIBLE);
                            //frame_export.startAnimation(fabOpenAnimation);
                        }
                        multi_select_follower(position);
                    }
                }, 1000);
            }
        }));

        recycleContacts.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(),
                recycleContacts, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, final int position) {
                if (mlist_multiselect != null && mlist_multiselect.size() == 0) {
                    isMultiSelect = false;
                    mlist_multiselect.clear();
                    frame_export.setVisibility(View.GONE);
                }
                if (isMultiSelect)
                    multi_select(position);
            }

            @Override
            public void onItemLongClick(View view, final int position) {

                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (!isMultiSelect) {
                            mlist_multiselect.clear();
                            isMultiSelect = true;
                            frame_export.setVisibility(View.VISIBLE);
                            //frame_export.startAnimation(fabOpenAnimation);
                        }
                        multi_select(position);
                    }
                }, 1000);
            }
        }));
    }

    public void multi_select(int position) {
        try {
            ArrayList<Datum> datumArrayList = newContactListAdapter.getCurrentList();
            if (mlist_multiselect.contains(datumArrayList.get(position))) {
                mlist_multiselect.remove(datumArrayList.get(position));
                if (mlist_multiselect != null && mlist_multiselect.size() == 0) {
                    isMultiSelect = false;
                    //frame_export.startAnimation(fabCloseAnimation);
                    frame_export.setVisibility(View.GONE);
                    mlist_multiselect.clear();
                }
            } else {
                mlist_multiselect.add(datumArrayList.get(position));
            }
            refreshAdapter();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void multi_select_follower(int position) {
        try {
            ArrayList<PojoClasses.FollowersList_Contact> follower_contactArrayList = followeradapter.getCurrentList();
            if (followerList_multiselect.contains(follower_contactArrayList.get(position))) {
                followerList_multiselect.remove(follower_contactArrayList.get(position));
                if (followerList_multiselect != null && followerList_multiselect.size() == 0) {
                    isMultiSelect_followers = false;
                    //frame_export.startAnimation(fabCloseAnimation);
                    frame_export.setVisibility(View.GONE);
                    followerList_multiselect.clear();
                }
            } else {
                followerList_multiselect.add(follower_contactArrayList.get(position));
            }
            refreshAdapterFollower();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void refreshAdapter() {
        try {
            newContactListAdapter.contactsLists_Selected = mlist_multiselect;
            newContactListAdapter.contactsLists = newContactListAdapter.getCurrentList();
            newContactListAdapter.notifyDataSetChanged();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void refreshAdapterFollower() {
        try {
            followeradapter.followerlist_Selected = followerList_multiselect;
            followeradapter.followerlist = followeradapter.getCurrentList();
            followeradapter.notifyDataSetChanged();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    ProfileSelection profileSelection = new ProfileSelection() {
        @Override
        public void onProfileSelected(int pos, MyProfiles myProfiles, String name) {
            try {

                if (dialog != null && dialog.isShowing())
                    dialog.dismiss();

                if (OpenItem.equalsIgnoreCase("2")) {

                    ArrayList<PojoClasses.FollowersList_Contact> filterdNames = new ArrayList<>();

                    for (PojoClasses.FollowersList_Contact s: followerList) {
                        if (s.getProfile_name().equalsIgnoreCase(name + "")) {
                            filterdNames.add(s);
                        }
                    }

                    followeradapter.filterList(filterdNames);
                    if (followeradapter != null && followeradapter.getItemCount() > 0) {
                        recycleFollowers.setVisibility(View.VISIBLE);
                        tv_no_record_found_Follower.setVisibility(View.GONE);
                    } else {
                        recycleFollowers.setVisibility(View.GONE);
                        tv_no_record_found_Follower.setVisibility(View.VISIBLE);
                    }
                }

                try {
                    selected_rec = pos;
                    isSelectedFromRecycler = true;
                } catch (Exception e) {
                    e.printStackTrace();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    private void AccendingFilter(Dialog dialog) {
        if (OpenItem.equalsIgnoreCase("1")) {
            try {
                Collections.sort(mlist, new Comparator<Datum>() {
                    @Override
                    public int compare(Datum list, Datum t1) {
                        String s1 = String.valueOf(list.getScanUserName());
                        String s2 = String.valueOf(t1.getScanUserName());
                        return s1.compareToIgnoreCase(s2);
                    }
                });
                newContactListAdapter.notifyDataSetChanged();
            } catch (Exception e) {
                e.printStackTrace();
            }
            dialog.dismiss();
        } else if (OpenItem.equalsIgnoreCase("2")) {
            try {
                Collections.sort(followerList, new Comparator<PojoClasses.FollowersList_Contact>() {
                    @Override
                    public int compare(PojoClasses.FollowersList_Contact followersList, PojoClasses.FollowersList_Contact t1) {
                        String s1 = String.valueOf(followersList.getName());
                        String s2 = String.valueOf(t1.getName());
                        return s1.compareToIgnoreCase(s2);
                    }
                });
                followeradapter.notifyDataSetChanged();
            } catch (Exception e) {
                e.printStackTrace();
            }
            dialog.dismiss();
        } else if (OpenItem.equalsIgnoreCase("3")) {
            try {
                Collections.sort(followingList, new Comparator<PojoClasses.FollowingList_Contact>() {
                    @Override
                    public int compare(PojoClasses.FollowingList_Contact list, PojoClasses.FollowingList_Contact t1) {
                        String s1 = String.valueOf(list.getName().charAt(0));
                        String s2 = String.valueOf(t1.getName().charAt(0));
                        return s1.compareToIgnoreCase(s2);
                    }
                });
                followingadapter.notifyDataSetChanged();
            } catch (Exception e) {
                e.printStackTrace();
            }
            dialog.dismiss();
        }
    }

    private void DescendingFilter(Dialog dialog) {
        if (OpenItem.equalsIgnoreCase("1")) {
            try {
                Collections.sort(mlist, new Comparator<Datum>() {
                    @Override
                    public int compare(Datum list, Datum t1) {
                        String s1 = String.valueOf(list.getScanUserName());
                        String s2 = String.valueOf(t1.getScanUserName());
                        return s2.compareToIgnoreCase(s1);
                    }
                });
                newContactListAdapter.notifyDataSetChanged();
            } catch (Exception e) {
                e.printStackTrace();
            }
            dialog.dismiss();
        } else if (OpenItem.equalsIgnoreCase("2")) {
            try {
                Collections.sort(followerList, new Comparator<PojoClasses.FollowersList_Contact>() {
                    @Override
                    public int compare(PojoClasses.FollowersList_Contact followersList, PojoClasses.FollowersList_Contact t1) {
                        String s1 = String.valueOf(followersList.getName());
                        String s2 = String.valueOf(t1.getName());
                        return s2.compareToIgnoreCase(s1);
                    }
                });
                followeradapter.notifyDataSetChanged();
            } catch (Exception e) {
                e.printStackTrace();
            }
            dialog.dismiss();
        } else if (OpenItem.equalsIgnoreCase("3")) {
            try {
                Collections.sort(followingList, new Comparator<PojoClasses.FollowingList_Contact>() {
                    @Override
                    public int compare(PojoClasses.FollowingList_Contact list, PojoClasses.FollowingList_Contact t1) {
                        String s1 = String.valueOf(list.getName().charAt(0));
                        String s2 = String.valueOf(t1.getName().charAt(0));
                        return s2.compareToIgnoreCase(s1);
                    }
                });
                followingadapter.notifyDataSetChanged();
            } catch (Exception e) {
                e.printStackTrace();
            }
            dialog.dismiss();
        }
    }

    private void OrderByrecentFilter(Dialog dialog) {

        //todo checkednotificatiom for contact list, check for following and follower list

        try {
            if (OpenItem.equalsIgnoreCase("1")) {
                try {
                    Utilities.shortContact(mlist, "date");
                    newContactListAdapter.notifyDataSetChanged();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                dialog.dismiss();
            } else if (OpenItem.equalsIgnoreCase("2")) {
                try {
                    Collections.sort(followerList, new Comparator<PojoClasses.FollowersList_Contact>() {
                        @Override
                        public int compare(PojoClasses.FollowersList_Contact followersList, PojoClasses.FollowersList_Contact t1) {
                            Date date1 = Utilities.formatDate(followersList.getScan_date());
                            Date date2 = Utilities.formatDate(t1.getScan_date());
                            return date2.compareTo(date1);
                        }
                    });
                    followeradapter.notifyDataSetChanged();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                dialog.dismiss();
            } else if (OpenItem.equalsIgnoreCase("3")) {
                try {
                    Collections.sort(followingList, new Comparator<PojoClasses.FollowingList_Contact>() {
                        @Override
                        public int compare(PojoClasses.FollowingList_Contact list, PojoClasses.FollowingList_Contact t1) {
                            Date date1 = Utilities.formatDate(list.getFollowed_on());
                            Date date2 = Utilities.formatDate(t1.getFollowed_on());
                            return date2.compareTo(date1);
                        }
                    });
                    followingadapter.notifyDataSetChanged();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                dialog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private BroadcastReceiver mNotificationReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
//            getFollowingList();
            showContacts("name", true);
        }
    };

    @Override
    public void onResume() {
        super.onResume();
        isMultiSelect = false;
        mlist_multiselect.clear();
        isMultiSelect_followers = false;
        followerList_multiselect.clear();
        frame_export.setVisibility(View.GONE);
        getActivity().registerReceiver(mNotificationReceiver, new IntentFilter("BROADCAST_FROM_SCAN_QK"));
        Utilities.precenceAvailable(getActivity());
    }

    @Override
    public void onPause() {
        super.onPause();
        getActivity().unregisterReceiver(mNotificationReceiver);
        Utilities.precenceNotAvailable(getActivity());
    }

//    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
//    private void revealShow(final FrameLayout lin) {
//
//        int w = lin.getWidth();
//        int h = lin.getHeight();
//
//        int endRadius = (int) Math.hypot(w, h);
//
//        int cx = (int) (img_filter.getX() + (img_filter.getWidth() / 2));
//        int cy = (int) (img_filter.getY()) + img_filter.getHeight() + 56;
//
//        Animator revealAnimator = ViewAnimationUtils.createCircularReveal(lin, cx, cy, 5f, endRadius);
//        lin.setVisibility(View.VISIBLE);
//        revealAnimator.setDuration(500);
//        revealAnimator.start();
//    }

    OnContactClick onContactClick = new OnContactClick() {

        @Override
        public void onLoadMore(String type) {

            if (type.equalsIgnoreCase(Constants.CONTACTS_TYPE_FOLLOWERS) && isLoadmoreFollowers && !isMultiSelect_followers) {

                pageNoFollowers = pageNoFollowers + 1;
                getFollowers(true, pageNoFollowers);

            } else if (type.equalsIgnoreCase(Constants.CONTACTS_TYPE_FOLLOWING) && isLoadmoreFollowing) {

//                pageNoFollowing = pageNoFollowing + 1;
                getFollowings(true);//, pageNoFollowing);
            }
        }

        @Override
        public void getContact(Datum contactsList, int type) {

            frame_export.setVisibility(View.GONE);

            if (type == Constants.TYPE_CLICK_ON_PROFILE_PIC) {

//                String username = qkPreferences.getValues(QKPreferences.USER_FULLNAME) + "";
//                String uniqueid = qkPreferences.getValues(QKPreferences.USER_UNIQUECODE) + "";

                /*Intent chatIntent = new Intent(context, NewChatActivity.class);
                chatIntent.putExtra(Constants.EXTRA_CURRENT_USER_ID, uniqueid + "");
                chatIntent.putExtra(Constants.EXTRA_RECIPIENT_ID, contactsList.getScanUserUniqueCode() + "");
                chatIntent.putExtra(Constants.EXTRA_CHAT_REF, contactsList.getScanUserUniqueCode() + "");
                chatIntent.putExtra(Constants.EXTRA_BLOCK_USER_ID, "0");
                chatIntent.putExtra(Constants.EXTRA_CURRENT_USER_NAME, username + "");
                chatIntent.putExtra(Constants.EXTRA_RECIPIENT_IMAGE, contactsList.getScanUserProfileUrl() + "");
                chatIntent.putExtra(Constants.EXTRA_RECIPIENT_NAME, contactsList.getScanUserName() + "");
                chatIntent.putExtra(Constants.EXTRA_USER_ID, contactsList.getScanUserId() + "");
                chatIntent.putExtra(Constants.EXTRA_SCAN_USER_DEVICE_ID, contactsList.getScan_user_device_id() + "");
                context.startActivity(chatIntent);*/

                Intent intent = new Intent(getActivity(), NewChatActivity.class);
                intent.putExtra("fJID", contactsList.getScanUserUniqueCode() + "@quickkonnect.com");
                intent.putExtra("fNickName", contactsList.getScanUserName());
                intent.putExtra("fAvatarByte", contactsList.getScanUserProfileUrl() + "");
                startActivity(intent);

            } else {
                if (contactsList.getIs_employer() != null && contactsList.getIs_personal() != null) {
                    /*if (contactsList.getIs_employer().equalsIgnoreCase("0")) {
                        // As it is
                        Intent intent = new Intent(context, MatchProfileActivity.class);
                        intent.putExtra("unique_code", contactsList.getScanUserUniqueCode() + "");
                        intent.putExtra(Constants.EXTRA_USER_ID, contactsList.getScanUserId() + "");
                        intent.putExtra(Constants.EXTRA_SCAN_USER_DEVICE_ID, contactsList.getScan_user_device_id() + "");
                        intent.putExtra(Constants.KEY, Constants.KEY_OTHER);
                        startActivityForResult(intent, 101);
                    } else {
                        if (!contactsList.getIs_personal().equalsIgnoreCase("1")) {
                            //Contact Detail wth Employee detail
                            Intent intent = new Intent(context, MatchProfileActivity.class);
                            intent.putExtra("unique_code", contactsList.getScanUserUniqueCode() + "");
                            intent.putExtra(Constants.EXTRA_USER_ID, contactsList.getScanUserId() + "");
                            intent.putExtra(Constants.EXTRA_SCAN_USER_DEVICE_ID, contactsList.getScan_user_device_id() + "");
                            intent.putExtra("PROFILE_ID", contactsList.getIs_employer() + "");
                            intent.putExtra(Constants.KEY, Constants.KEY_OTHER_WITH_EMPLOYEE);
                            startActivityForResult(intent, 101);
                        } else {// only employee detail
                            Intent intent = new Intent(getActivity(), EmployeeDetails.class);
                            intent.putExtra("profile_id", "" + contactsList.getIs_employer());
                            intent.putExtra(Constants.KEY, Constants.KEY_CONTACT_EMPLOYEER);
                            startActivityForResult(intent, 101);
                        }
                    }*/

                    if (!contactsList.getIs_personal().equalsIgnoreCase("0")) {
                        // Show Employee details
                        if (!contactsList.getIs_employer().equalsIgnoreCase("0")) {
                            //Contact Details having Employee details and redirect to employee
                            Intent intent = new Intent(context, MatchProfileActivity.class);
                            intent.putExtra("unique_code", contactsList.getScanUserUniqueCode() + "");
                            intent.putExtra(Constants.EXTRA_USER_ID, contactsList.getScanUserId() + "");
                            intent.putExtra(Constants.EXTRA_SCAN_USER_DEVICE_ID, contactsList.getScan_user_device_id() + "");
                            intent.putExtra("PROFILE_ID", contactsList.getIs_employer() + "");
                            intent.putExtra(Constants.KEY, Constants.KEY_OTHER_WITH_EMPLOYEE);
                            startActivityForResult(intent, 101);
                        } else {
                            // Remain Same no change
                            Intent intent = new Intent(context, MatchProfileActivity.class);
                            intent.putExtra("unique_code", contactsList.getScanUserUniqueCode() + "");
                            intent.putExtra(Constants.EXTRA_USER_ID, contactsList.getScanUserId() + "");
                            intent.putExtra(Constants.EXTRA_SCAN_USER_DEVICE_ID, contactsList.getScan_user_device_id() + "");
                            intent.putExtra(Constants.KEY, Constants.KEY_OTHER);
                            startActivityForResult(intent, 101);
                        }
                    } else {
                        Intent intent = new Intent(getActivity(), EmployeeDetails.class);
                        intent.putExtra("profile_id", "" + contactsList.getIs_employer());
                        intent.putExtra(Constants.KEY, Constants.KEY_CONTACT_EMPLOYEER);
                        intent.putExtra("scan_user_id", contactsList.getScanUserId() + "");
                        startActivityForResult(intent, 101);

                    }
                } else {
                    Intent intent = new Intent(context, MatchProfileActivity.class);
                    intent.putExtra("unique_code", contactsList.getScanUserUniqueCode() + "");
                    intent.putExtra(Constants.EXTRA_USER_ID, contactsList.getScanUserId() + "");
                    intent.putExtra(Constants.EXTRA_SCAN_USER_DEVICE_ID, contactsList.getScan_user_device_id() + "");
                    intent.putExtra(Constants.KEY, Constants.KEY_OTHER);
                    startActivityForResult(intent, 101);
                }
            }
        }

        @Override
        public void getFollower(PojoClasses.FollowersList_Contact followerlist) {
            frame_export.setVisibility(View.GONE);
            Intent intent = new Intent(context, MatchProfileActivity.class);
            intent.putExtra("unique_code", followerlist.getUnique_code() + "");
            intent.putExtra("ProfileId", followerlist.getProfile_id() + "");
            intent.putExtra("UserId_Follower", followerlist.getUser_id() + "");
            intent.putExtra(Constants.KEY, Constants.KEY_FOLLOWER);
            startActivityForResult(intent, 102);
        }

        @Override
        public void getFollowing(PojoClasses.FollowingList_Contact followinglist) {
            Intent intent = new Intent(context, NewScanProfileActivity.class);
            intent.putExtra("unique_code", "" + followinglist.getUnique_code());
            intent.putExtra(Constants.KEY, Constants.KEY_SCANQK_CONTACT);
            intent.putExtra("profile_id", "" + followinglist.getId());//Profile Id here
            intent.putExtra("FollowedAt", "" + followinglist.getFollowed_on());
            startActivityForResult(intent, 103);
        }
    };

    int pageNoFollowers = 1;//, pageNoFollowing = 1;
    boolean isLoadmoreFollowers = true, isLoadmoreFollowing = true;

    @Override
    public void onRefresh() {

        if (OpenItem.equalsIgnoreCase("1")) {//Contacts

            mlist_multiselect.clear();
            isMultiSelect = false;
            getContactsList(true);

        } else if (OpenItem.equalsIgnoreCase("2")) {//Followers

            followerList_multiselect.clear();
            isMultiSelect_followers = false;
            frame_export.setVisibility(View.GONE);
            pageNoFollowers = 1;
            getFollowers(true, pageNoFollowers);

        } else if (OpenItem.equalsIgnoreCase("3")) {//Following

//            pageNoFollowing = 1;
            getFollowings(true);//, pageNoFollowing);
        }

        frame_export.setVisibility(View.GONE);
    }

    public void showContacts(String filterBy, boolean isReload) {

        //String unique_code = qkPreferences.getValues(QKPreferences.USER_UNIQUECODE);

        RealmResults<Contacts> contacts = RealmController.with(getActivity()).getContacts();

        mlist = new ArrayList<>();
        mlist_multiselect = new ArrayList<>();

        if (contacts != null && contacts.size() > 0) {

            for (Contacts contacts1: contacts) {

                if (!String.valueOf(qkPreferences.getLoginUniqueCode()).equals(contacts1.getUnique_code()) && contacts1.getContact_type() != null && contacts1.getContact_type().equals(Constants.TYPE_CONTACTS)) {

                    Datum datum = new Datum();

                    datum.setEmail(contacts1.getEmail());
                    datum.setPhone(contacts1.getPhone());
                    datum.setScannedBy(contacts1.getScanned_by());
                    datum.setScanUserId(Integer.parseInt(contacts1.getUser_id().replace(".0", "")));

                    //String cap = contacts1.getUsername().substring(0, 1).toUpperCase() + contacts1.getUsername().substring(1);
                    String cap = contacts1.getUsername();
                    datum.setScanUserName(cap);

                    datum.setScanUserProfileUrl(contacts1.getProfile_pic());
                    datum.setScanUserUniqueCode(contacts1.getUnique_code());
                    datum.setScan_user_device_id(contacts1.getScan_user_device_id() + "");
                    /*if (contacts1.getScan_date() != null && contacts1.getScan_date().length() > 10) {
                        try {
                            String datemain[] = contacts1.getScan_date().split(",");
                            datum.setScanDate(Utilities.changeDateFormat("dd MMM yyyy hh:mm", "dd MMM yyyy", datemain[1] + ""));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {*/
                    datum.setScanDate(contacts1.getScan_date());
                    //}
                    datum.setFirstname(contacts1.getFirstname());
                    datum.setLastname(contacts1.getLastname());
                    datum.setIs_employer(contacts1.getIs_employer());
                    datum.setIs_personal(contacts1.getIs_personal());

                    mlist.add(datum);
                }
            }
        }

        if (mlist != null && mlist.size() > 0) {

            recycleContacts.setVisibility(View.VISIBLE);
            tv_no_record_found_Contacts.setVisibility(View.GONE);

            //todo removed contact short by name
            try {
                Utilities.shortContact(mlist, "date");
            } catch (Exception e) {
                e.printStackTrace();
            }

            newContactListAdapter = new NewContactListAdapter(getActivity(), mlist, mlist_multiselect, false, onContactClick);
            recycleContacts.setAdapter(newContactListAdapter);

        } else {
            recycleContacts.setVisibility(View.GONE);
            tv_no_record_found_Contacts.setVisibility(View.VISIBLE);
        }

        /*if (pd != null && pd.isShowing()) {
            pd.dismiss();
        }*/

        if (isReload) {
            getContactsList(false);
        }
    }

    public void getContactsList(boolean isRefresh) {
        if (isRefresh)
            mSwipeRefreshLayout.setRefreshing(true);

       /* try {
            pd = Utilities.showProgress(getActivity());
        } catch (Exception e) {
            e.printStackTrace();
        }*/
//        String token = "";
//        if (qkPreferences != null) {
//            token = qkPreferences.getApiToken() + "";
//        }

        Map<String, String> headerparams = Utilities.getHeaderParameter(context);

//        params.put("Authorization", "Bearer " + token + "");
//        params.put("Accept", "application/json");
//        params.put("QK_ACCESS_KEY", getResources().getString(R.string.QK_ACCESS_KEY) + "");

        mService.getContactsList(headerparams, qkPreferences.getLoggedUserid(), TimeZone.getDefault().getID()).enqueue(new Callback<ContactsPojo>() {
            @Override
            public void onResponse(Call<ContactsPojo> call, Response<ContactsPojo> response) {
                mSwipeRefreshLayout.setRefreshing(false);
                try {
                    if (response != null && response.body() != null) {
                        ContactsPojo contactsPojo = response.body();
                        if (contactsPojo != null && contactsPojo.getStatus() == 200) {
                            parseContacts(response.body());
                        } else {
                            RealmController.with(getActivity()).clearContacts();
                            showContacts("name", false);
                        }
                    } else
                        showContacts("name", false);

                } catch (Exception e) {
                    showContacts("name", false);
                }
            }

            @Override
            public void onFailure(Call<ContactsPojo> call, Throwable t) {
                Log.e("Contacts:Error", "" + t.getLocalizedMessage());
                mSwipeRefreshLayout.setRefreshing(false);
                showContacts("name", false);
            }
        });
    }

    public void parseContacts(ContactsPojo contactsPojo) {
        List<Datum> datumList = contactsPojo.getData();
        if (datumList != null && datumList.size() > 0) {
            String UniqueCode = qkPreferences.getValues(QKPreferences.USER_UNIQUECODE) + "";
            String Usrename = qkPreferences.getValues(QKPreferences.USER_FULLNAME) + "";

            RealmController.with(getActivity()).clearContacts();

            for (Datum datum: datumList) {

                Contacts contacts = new Contacts();

                contacts.setUser_id("" + datum.getScanUserId());
                contacts.setUsername(datum.getScanUserName());
                contacts.setProfile_pic(datum.getScanUserProfileUrl());
                contacts.setFirstname(datum.getFirstname());
                contacts.setLastname(datum.getLastname());
                contacts.setUnique_code(datum.getScanUserUniqueCode());
                contacts.setScanned_by(datum.getScannedBy());
                contacts.setPhone(datum.getPhone());
                contacts.setEmail(datum.getEmail());
                contacts.setProfile_id("" + datum.getProfileId());
                contacts.setProfile_name("" + datum.getProfileName());
                contacts.setContact_type(Constants.TYPE_CONTACTS);
                contacts.setScan_user_device_id("" + datum.getScan_user_device_id());
                contacts.setIs_employer("" + datum.getIs_employer());
                contacts.setIs_personal("" + datum.getIs_personal());

                try {
                    String scanDate = datum.getScanDate();
                    if (Utilities.isEmpty(scanDate))
                        contacts.setScan_date(scanDate);
                    //contacts.setScan_date(Utilities.changeDateFormat("yyyy-MM-dd hh:mm:ss", "dd MMM yyyy", scanDate));

                } catch (Exception e) {
                    e.printStackTrace();
                }
                RealmController.with(getActivity()).updateContacts(contacts);

                /*try {
                    LastMessage lastMessage = new LastMessage();
                    lastMessage.setToName(datum.getScanUserName() + "");
                    lastMessage.setToId(datum.getScanUserUniqueCode() + "");
                    lastMessage.setFromName(Usrename + "");
                    lastMessage.setFromId(UniqueCode + "");
                    lastMessage.setMsg("");
                    lastMessage.setIsRead("0");
                    lastMessage.setTimeStamp("");
                    lastMessage.setPhoto(datum.getScanUserProfileUrl() + "");
                    //lastMessage.setTimeStamp(Calendar.getInstance().getTimeInMillis() + "");

                    ChatUserContactModel chatUserContactModel = new ChatUserContactModel();
                    chatUserContactModel.setChatLocation("");
                    chatUserContactModel.setUnreadCount("0");
                    chatUserContactModel.setLastMessage(lastMessage);
                    ChatConfig.addContacts(UniqueCode + "", datum.getScanUserUniqueCode() + "", chatUserContactModel);
                } catch (Exception e) {
                    e.printStackTrace();
                }*/

            }
        } else {
            try {
                RealmController.with(getActivity()).clearContacts();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        try {
            ModelLoggedUser modelLoggedUser = qkPreferences.getLoggedUser();
            ModelLoggedData modelLoggedData = modelLoggedUser.getData();
            if (contactsPojo.getContactCount().getContactAll() != null)
                modelLoggedData.getContactCount().setContactAll(contactsPojo.getContactCount().getContactAll());

            if (contactsPojo.getContactCount().getContactWeek() != null)
                modelLoggedData.getContactCount().setContactWeek(contactsPojo.getContactCount().getContactWeek());

            if (contactsPojo.getContactCount().getContactMonth() != null)
                modelLoggedData.getContactCount().setContactMonth(contactsPojo.getContactCount().getContactMonth());

            modelLoggedUser.setData(modelLoggedData);
            qkPreferences.storeLoggedUser(modelLoggedUser);

        } catch (Exception e) {
            e.printStackTrace();
        }

        showContacts("name", false);
    }

    public void showFollowers(String filterBy, boolean isReload) {

        RealmResults<Follower_contact> contacts = RealmController.with(getActivity()).getFollower_contact();

        followerList = new ArrayList<>();
        followerList_multiselect = new ArrayList<>();

        if (contacts != null && contacts.size() > 0) {

            for (Follower_contact contacts1: contacts) {

                followerList.add(new PojoClasses.FollowersList_Contact(
                        contacts1.getScan_date(),
                        contacts1.getPhone(),
                        contacts1.getUser_id(),
                        contacts1.getUsername(),
                        contacts1.getEmail(),
                        contacts1.getUnique_code(),
                        contacts1.getProfile_pic(),
                        contacts1.getProfile_id(),
                        contacts1.getProfile_name()));

            }
        }

        if (followerList != null && followerList.size() > 0) {
            recycleFollowers.setVisibility(View.VISIBLE);
            tv_no_record_found_Follower.setVisibility(View.GONE);

            followeradapter = new FollowerContactAdapter(getActivity(), followerList, followerList_multiselect, onContactClick);
            recycleFollowers.setAdapter(followeradapter);

        } else {
            recycleFollowers.setVisibility(View.GONE);
            tv_no_record_found_Follower.setVisibility(View.VISIBLE);

        }

        if (isReload) {
            pageNoFollowers = 1;
            getFollowers(false, pageNoFollowers);
        }
    }

    public void getFollowers(boolean isRefresh, final int pageNoFollowers) {

        try {
            frame_export.setVisibility(View.GONE);
            isMultiSelect_followers = false;
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (isRefresh)
            mSwipeRefreshLayout.setRefreshing(true);
        String token = "";
        if (qkPreferences != null) {
            token = qkPreferences.getApiToken() + "";
        }

        Map<String, String> headerparams = Utilities.getHeaderParameter(context);

        mService.getFollowersList(headerparams, Constants.CONTACTS_TYPE_FOLLOWERS,
                qkPreferences.getValuesInt(QKPreferences.USER_ID), pageNoFollowers).enqueue(new Callback<ContactsPojo>() {
            @Override
            public void onResponse(Call<ContactsPojo> call, Response<ContactsPojo> response) {
                mSwipeRefreshLayout.setRefreshing(false);
                Log.e("Follower:Success", "" + response.body());
                try {
                    if (response != null && response.body() != null) {
                        ContactsPojo contactsPojo = response.body();
                        if (contactsPojo != null && contactsPojo.getStatus() == 200) {

                            if (pageNoFollowers == 1)
                                RealmController.with(getActivity()).clearFollowers();

                            if (contactsPojo.getData() == null || contactsPojo.getData().size() < 20)
                                isLoadmoreFollowers = false;
                            else
                                isLoadmoreFollowers = true;

                            parseFollowers(contactsPojo);

                        } else {

                            RealmController.with(getActivity()).clearFollowers();
                            showFollowers("name", false);
                        }

                    } else
                        showFollowers("name", false);

                } catch (Exception e) {
                    showFollowers("name", false);
                }
            }

            @Override
            public void onFailure(Call<ContactsPojo> call, Throwable t) {
                mSwipeRefreshLayout.setRefreshing(false);
                showFollowers("name", false);
            }
        });
    }

    public void parseFollowers(ContactsPojo contactsPojo) {

        List<Datum> datumList = contactsPojo.getData();

        if (datumList != null && datumList.size() > 0) {

            //RealmController.with(getActivity()).clearFollowers();

            for (Datum datum: datumList) {

                Follower_contact contacts = new Follower_contact();

                contacts.setUser_id("" + datum.getScanUserId());
                contacts.setUsername(datum.getScanUserName());
                contacts.setProfile_pic(datum.getScanUserProfileUrl());
                contacts.setFirstname(datum.getFirstname());
                contacts.setLastname(datum.getLastname());
                contacts.setUnique_code(datum.getScanUserUniqueCode());
                contacts.setScan_date(datum.getScannedBy());
                contacts.setPhone(datum.getPhone());
                contacts.setEmail(datum.getEmail());
                contacts.setProfile_id("" + datum.getProfileId());
                contacts.setProfile_name("" + datum.getProfileName());

                try {
                    String scanDate = datum.getScanDate();
                    if (Utilities.isEmpty(scanDate))
                        contacts.setScan_date(Utilities.changeDateFormat("yyyy-MM-dd hh:mm:ss", "dd-MM-yyyy", scanDate));

                } catch (Exception e) {
                    e.printStackTrace();
                }

                RealmController.with(getActivity()).updateFollower_Contact(contacts);
            }
        }

        showFollowers("name", false);
    }

    public void ShowFollower() {
        try {
            tabLayout.setScrollPosition(1, 0, false);
            OpenItem = "2";
            relativeLayoutContacts.setVisibility(View.GONE);
            relativeLayoutFollowers.setVisibility(View.VISIBLE);
            relativeLayoutFollowing.setVisibility(View.GONE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void ShowFollowing() {
        try {
            tabLayout.setScrollPosition(2, 0, false);
            OpenItem = "3";
            relativeLayoutContacts.setVisibility(View.GONE);
            relativeLayoutFollowers.setVisibility(View.GONE);
            relativeLayoutFollowing.setVisibility(View.VISIBLE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void ShowContact() {
        try {
            tabLayout.setScrollPosition(0, 0, false);
            OpenItem = "1";
            relativeLayoutContacts.setVisibility(View.VISIBLE);
            relativeLayoutFollowers.setVisibility(View.GONE);
            relativeLayoutFollowing.setVisibility(View.GONE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void showFollowing(boolean isReload) {

        RealmResults<Following_contact> contacts = RealmController.with(getActivity()).getFollowing_contact();

        followingList = new ArrayList<>();

        if (contacts != null && contacts.size() > 0) {

            for (Following_contact contacts1: contacts) {

                followingList.add(new PojoClasses.FollowingList_Contact(
                        contacts1.getId(),
                        contacts1.getName(),
                        contacts1.getStatus(),
                        contacts1.getReason(),
                        contacts1.getType(),
                        contacts1.getLogo(),
                        contacts1.getRole(),
                        contacts1.getUnique_code(),
                        contacts1.getBorder_color(),
                        contacts1.getPattern_color(),
                        contacts1.getBackground_color(),
                        contacts1.getMiddle_tag(),
                        contacts1.getQr_code(),
                        contacts1.getBackground_gradient(),
                        contacts1.getFollowed_on(),
                        contacts1.getFollowed_at()));
            }
        }

        if (followerList != null && contacts.size() > 0) {

            recycleFollowing.setVisibility(View.VISIBLE);
            tv_no_record_found_Following.setVisibility(View.GONE);

            followingadapter = new FollowingContactAdapter(getActivity(), followingList, onContactClick);
            recycleFollowing.setAdapter(followingadapter);

        } else {

            recycleFollowing.setVisibility(View.GONE);
            tv_no_record_found_Following.setVisibility(View.VISIBLE);

        }

        if (isReload) {
//            pageNoFollowing = 1;
            getFollowings(false);//, pageNoFollowing);
        }
    }

    public void getFollowings(boolean isRefresh){//, final int pageNoFollowing) {
        try {

            if (isRefresh)
                mSwipeRefreshLayout.setRefreshing(isRefresh);

//            JSONObject jsonObject = new JSONObject();
//            try {
//                jsonObject.put("user_id", qkPreferences.getValuesInt(QKPreferences.USER_ID) + "");
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }

//             new VolleyApiRequest(context, false, VolleyApiRequest.REQUEST_BASEURL + "api/profile/getAllfollowing",
//                    new VolleyCallBack() {
//                        @Override
//                        public void onResponse(JSONObject response) {
//                            mSwipeRefreshLayout.setRefreshing(false);
//                            parseFollowing(response);
//                        }
//
//                        @Override
//                        public void onErrorResponse(VolleyError error) {
//                            mSwipeRefreshLayout.setRefreshing(false);
//                            VolleyApiRequest.showVolleyError(context, error);
//                        }
//                    }).enqueRequest(jsonObject, Request.Method.POST);

            Map<String, String> headerparams = Utilities.getHeaderParameter(context);
            mService.getAllFollowing(headerparams, qkPreferences.getLoggedUserid()).enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    mSwipeRefreshLayout.setRefreshing(false);
                    if (response != null && response.body() != null) {

                        try {

                            JSONObject jsonObject = new JSONObject(response.body().string());

//                            if (pageNoFollowing == 1)
                            RealmController.with(getActivity()).clearFollowing();

                            parseFollowing(jsonObject);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    mSwipeRefreshLayout.setRefreshing(false);
                    Utilities.showToast(context, t.getLocalizedMessage());
                }
            });


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void parseFollowing(JSONObject response) {
        try {

            String responseStatus = response.getString("status");

            if (responseStatus.equals(ApiUtils.RESPONSE_CODE_SUCCESS)) {

                JSONArray jsnArr = response.getJSONArray("data");

                if (jsnArr != null && jsnArr.length() > 0) {

//                    followingList.clear();

                    for (int i = 0; i < jsnArr.length(); i++) {

                        JSONObject obj = jsnArr.getJSONObject(i);

                        Following_contact contacts = new Following_contact();

                        contacts.setId(obj.getString("id"));
                        contacts.setName(obj.getString("name"));
                        contacts.setStatus(obj.getString("status"));
                        contacts.setReason(obj.getString("reason"));
                        contacts.setType(obj.getString("type"));
                        contacts.setLogo(obj.getString("logo"));
                        contacts.setRole(obj.getString("role"));
                        contacts.setUnique_code(obj.getString("unique_code"));

                        JSONObject qktag = obj.getJSONObject("qk_tag_info");
                        contacts.setBorder_color(qktag.getString("border_color"));
                        contacts.setPattern_color(qktag.getString("pattern_color"));
                        contacts.setBackground_color(qktag.getString("background_color"));
                        contacts.setMiddle_tag(qktag.getString("middle_tag"));
                        contacts.setQr_code(qktag.getString("qr_code"));
                        contacts.setBackground_gradient(qktag.getString("background_gradient"));

                        contacts.setFollowed_at(obj.getString("followed_at"));
                        try {
                            String scanDate = obj.getString("followed_on");
                            if (Utilities.isEmpty(scanDate))
                                contacts.setFollowed_on(Utilities.changeDateFormat("yyyy-MM-dd hh:mm:ss", "EEE, dd MMM yyyy hh:mm", scanDate));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        RealmController.with(getActivity()).updateFollowing_Contact(contacts);
                    }

//                    if (jsnArr == null || jsnArr.length() < 20)
//                        isLoadmoreFollowing = false;
//                    else
//                        isLoadmoreFollowing = true;

                    showFollowing(false);

                } else {
                    recycleFollowing.setVisibility(View.GONE);
                    tv_no_record_found_Following.setVisibility(View.VISIBLE);
                }
            } else {
                recycleFollowing.setVisibility(View.GONE);
                tv_no_record_found_Following.setVisibility(View.VISIBLE);
            }
        } catch (Exception e) {
            e.printStackTrace();
            recycleFollowing.setVisibility(View.GONE);
            tv_no_record_found_Following.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK && requestCode == 101) {
            showContacts("name", false);
        } else if (requestCode == 102) {
            showFollowers("name", false);
        } else if (requestCode == 103) {
            showFollowing(true);
        }
    }

    private void setFilter() {

        dialog = new Dialog(getActivity(), R.style.PauseDialogAnimation);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View view = LinearLayout.inflate(getActivity(), R.layout.dialog_filter_contact, null);
        dialog.setContentView(view);
        Window window = dialog.getWindow();

      /*  try {
            if (Build.VERSION.SDK_INT >= 19 && Build.VERSION.SDK_INT < 21) {
                WindowManager.LayoutParams winParams = window.getAttributes();
                winParams.flags |= WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS;
                window.setAttributes(winParams);
            }
            if (Build.VERSION.SDK_INT >= 19) {
                window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
            }
            if (Build.VERSION.SDK_INT >= 21) {
                WindowManager.LayoutParams winParams = window.getAttributes();
                winParams.flags &= ~WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS;
                window.setAttributes(winParams);
                window.setStatusBarColor(getResources().getColor(R.color.colorPrimary));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }*/

        /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            dialog.getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimary));
        }*/

        //WindowManager.LayoutParams wlp = window.getAttributes();
        //wlp.gravity = Gravity.CENTER;
        //wlp.flags &= ~WindowManager.LayoutParams.FLAG_BLUR_BEHIND;
        //window.setAttributes(wlp);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        dialog.show();

        lin_dialog_main_contact = dialog.findViewById(R.id.lin_dialog_main_contact);
        lin_dialog_main_contact.setVisibility(View.VISIBLE);

        close_img_dialog_contact = dialog.findViewById(R.id.close_img_dialog_contact);
        tv_title_contact_dialog = dialog.findViewById(R.id.tv_title_contact_dialog);
        rec_dialog_profile = dialog.findViewById(R.id.rec_dialog_profile);
        tv_filter_atoz = dialog.findViewById(R.id.tv_filter_atoz);
        tv_filter_ztoa = dialog.findViewById(R.id.tv_filter_ztoa);
        tv_filter_Orderdby = dialog.findViewById(R.id.tv_filter_Orderdby);
        tv_filter_clearfilters = dialog.findViewById(R.id.tv_filter_clearfilters);
        lin_profiles_dialog = dialog.findViewById(R.id.lin_profiles_dialog);
        lin_profilestypes_dialog = dialog.findViewById(R.id.lin_profilestypes_dialog);
        tv_profiletype_compony = dialog.findViewById(R.id.tv_profiletype_compony);
        tv_profiletype_enterprise = dialog.findViewById(R.id.tv_profiletype_enterprise);
        tv_profiletype_celebrity = dialog.findViewById(R.id.tv_profiletype_celebrity);

        imag_select_atoz = dialog.findViewById(R.id.imag_select_atoz);
        imag_select_ztoa = dialog.findViewById(R.id.imag_select_ztoa);
        imag_select_orderby = dialog.findViewById(R.id.imag_select_orderby);
        imag_select_compony = dialog.findViewById(R.id.imag_select_compony);
        imag_select_enterprise = dialog.findViewById(R.id.imag_select_enterprise);
        imag_select_celebrity = dialog.findViewById(R.id.imag_select_celebrity);

        setSelectionFilter(selected);

        close_img_dialog_contact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        ProfileList = new ArrayList<>();
        List<MyProfiles> myProfiles = RealmController.with(getActivity()).getMyProfiles();
        if (myProfiles != null && myProfiles.size() > 0) {

            try {
                for (int i = 0; i < myProfiles.size(); i++) {
                    MyProfiles profiles = myProfiles.get(i);
                    if (profiles.getStatus().equalsIgnoreCase("A")) {
                        if (profiles.getType() != null && !profiles.getType().equals("0") && profiles.getIsOwnProfile() != null &&
                                profiles.getIsOwnProfile().equals("true")) {
                            boolean isAlready = false;
                            for (int j = 0; j < ProfileList.size(); j++) {
                                if (ProfileList.get(j).getName().equalsIgnoreCase(profiles.getName())) {
                                    isAlready = true;
                                }
                            }
                            if (!isAlready) {
                                ProfileList.add(profiles);
                            }
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            adapter_profile = new ProfileAdapter(getActivity(), ProfileList, profileSelection, selected_rec, isSelectedFromRecycler);
            rec_dialog_profile.setLayoutManager(new LinearLayoutManager(getActivity()));
            rec_dialog_profile.setAdapter(adapter_profile);

        } else {
            lin_profiles_dialog.setVisibility(View.GONE);
        }

        if (OpenItem.equalsIgnoreCase("1")) {
            lin_profiles_dialog.setVisibility(View.GONE);
            lin_profilestypes_dialog.setVisibility(View.GONE);

        } else if (OpenItem.equalsIgnoreCase("2")) {
            lin_profiles_dialog.setVisibility(View.VISIBLE);
            lin_profilestypes_dialog.setVisibility(View.GONE);

        } else if (OpenItem.equalsIgnoreCase("3")) {
            lin_profiles_dialog.setVisibility(View.GONE);
            lin_profilestypes_dialog.setVisibility(View.VISIBLE);

        }

        tv_filter_atoz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selected = 1;
                isSelectedFromRecycler = false;
                AccendingFilter(dialog);
            }
        });

        tv_filter_ztoa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                selected = 2;
                isSelectedFromRecycler = false;
                DescendingFilter(dialog);
            }
        });

        tv_filter_Orderdby.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selected = 3;
                isSelectedFromRecycler = false;
                OrderByrecentFilter(dialog);
            }
        });

        tv_filter_clearfilters.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                selected = 0;
                isSelectedFromRecycler = false;

                dialog.dismiss();
                if (OpenItem.equalsIgnoreCase("1")) {
                    showContacts("name", false);

                } else if (OpenItem.equalsIgnoreCase("2")) {
                    showFollowers("name", false);

                } else if (OpenItem.equalsIgnoreCase("3")) {
                    showFollowing(false);
                }
            }
        });

        lin_profilestypes_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                if (OpenItem.equalsIgnoreCase("3")) {
                    selected = 4;
                    isSelectedFromRecycler = false;
                    FilterProfielType(1);
                }
            }
        });
        tv_profiletype_enterprise.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                if (OpenItem.equalsIgnoreCase("3")) {
                    selected = 5;
                    isSelectedFromRecycler = false;
                    FilterProfielType(2);
                }
            }
        });
        tv_profiletype_celebrity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                if (OpenItem.equalsIgnoreCase("3")) {
                    selected = 6;
                    isSelectedFromRecycler = false;
                    FilterProfielType(3);
                }
            }
        });
    }

    private void setSelectionFilter(int selected) {
        if (isSelectedFromRecycler) {
            imag_select_atoz.setImageResource(R.drawable.notcheckedfilter);
            imag_select_ztoa.setImageResource(R.drawable.notcheckedfilter);
            imag_select_orderby.setImageResource(R.drawable.notcheckedfilter);
            imag_select_compony.setImageResource(R.drawable.notcheckedfilter);
            imag_select_enterprise.setImageResource(R.drawable.notcheckedfilter);
            imag_select_celebrity.setImageResource(R.drawable.notcheckedfilter);
           /* try {
                adapter_profile.posselected = selected_rec;
                adapter_profile.notifyDataSetChanged();
            } catch (Exception e) {
                e.printStackTrace();
            }*/
        } else {
            switch (selected) {
                case 0:
                    imag_select_atoz.setImageResource(R.drawable.notcheckedfilter);
                    imag_select_ztoa.setImageResource(R.drawable.notcheckedfilter);
                    imag_select_orderby.setImageResource(R.drawable.notcheckedfilter);
                    imag_select_compony.setImageResource(R.drawable.notcheckedfilter);
                    imag_select_enterprise.setImageResource(R.drawable.notcheckedfilter);
                    imag_select_celebrity.setImageResource(R.drawable.notcheckedfilter);
                    break;
                case 1:
                    imag_select_atoz.setImageResource(R.drawable.checkedfilter);
                    imag_select_ztoa.setImageResource(R.drawable.notcheckedfilter);
                    imag_select_orderby.setImageResource(R.drawable.notcheckedfilter);
                    imag_select_compony.setImageResource(R.drawable.notcheckedfilter);
                    imag_select_enterprise.setImageResource(R.drawable.notcheckedfilter);
                    imag_select_celebrity.setImageResource(R.drawable.notcheckedfilter);
                    break;
                case 2:
                    imag_select_atoz.setImageResource(R.drawable.notcheckedfilter);
                    imag_select_ztoa.setImageResource(R.drawable.checkedfilter);
                    imag_select_orderby.setImageResource(R.drawable.notcheckedfilter);
                    imag_select_compony.setImageResource(R.drawable.notcheckedfilter);
                    imag_select_enterprise.setImageResource(R.drawable.notcheckedfilter);
                    imag_select_celebrity.setImageResource(R.drawable.notcheckedfilter);
                    break;
                case 3:
                    imag_select_atoz.setImageResource(R.drawable.notcheckedfilter);
                    imag_select_ztoa.setImageResource(R.drawable.notcheckedfilter);
                    imag_select_orderby.setImageResource(R.drawable.checkedfilter);
                    imag_select_compony.setImageResource(R.drawable.notcheckedfilter);
                    imag_select_enterprise.setImageResource(R.drawable.notcheckedfilter);
                    imag_select_celebrity.setImageResource(R.drawable.notcheckedfilter);
                    break;
                case 4:
                    imag_select_atoz.setImageResource(R.drawable.notcheckedfilter);
                    imag_select_ztoa.setImageResource(R.drawable.notcheckedfilter);
                    imag_select_orderby.setImageResource(R.drawable.notcheckedfilter);
                    imag_select_compony.setImageResource(R.drawable.checkedfilter);
                    imag_select_enterprise.setImageResource(R.drawable.notcheckedfilter);
                    imag_select_celebrity.setImageResource(R.drawable.notcheckedfilter);
                    break;
                case 5:
                    imag_select_atoz.setImageResource(R.drawable.notcheckedfilter);
                    imag_select_ztoa.setImageResource(R.drawable.notcheckedfilter);
                    imag_select_orderby.setImageResource(R.drawable.notcheckedfilter);
                    imag_select_compony.setImageResource(R.drawable.notcheckedfilter);
                    imag_select_enterprise.setImageResource(R.drawable.checkedfilter);
                    imag_select_celebrity.setImageResource(R.drawable.notcheckedfilter);
                    break;
                case 6:
                    imag_select_atoz.setImageResource(R.drawable.notcheckedfilter);
                    imag_select_ztoa.setImageResource(R.drawable.notcheckedfilter);
                    imag_select_orderby.setImageResource(R.drawable.notcheckedfilter);
                    imag_select_compony.setImageResource(R.drawable.notcheckedfilter);
                    imag_select_enterprise.setImageResource(R.drawable.notcheckedfilter);
                    imag_select_celebrity.setImageResource(R.drawable.checkedfilter);
                    break;
                case 7:
                    imag_select_atoz.setImageResource(R.drawable.notcheckedfilter);
                    imag_select_ztoa.setImageResource(R.drawable.notcheckedfilter);
                    imag_select_orderby.setImageResource(R.drawable.notcheckedfilter);
                    imag_select_compony.setImageResource(R.drawable.notcheckedfilter);
                    imag_select_enterprise.setImageResource(R.drawable.notcheckedfilter);
                    imag_select_celebrity.setImageResource(R.drawable.notcheckedfilter);
                    break;
            }
        }
    }

    private void FilterProfielType(int type) {
        try {
            ArrayList<PojoClasses.FollowingList_Contact> filterdNames = new ArrayList<>();
            for (PojoClasses.FollowingList_Contact s: followingList) {
                if (s.getType().equalsIgnoreCase(type + "")) {
                    filterdNames.add(s);
                }
            }
            followingadapter.filterList(filterdNames);
            if (followingadapter != null && followingadapter.getItemCount() > 0) {
                recycleFollowing.setVisibility(View.VISIBLE);
                tv_no_record_found_Following.setVisibility(View.GONE);
            } else {
                recycleFollowing.setVisibility(View.GONE);
                tv_no_record_found_Following.setVisibility(View.VISIBLE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void craeteProgress() {
        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setMessage("Exporting File, Please Wait..");
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();
    }

    String File_Path = "";

    class ExportToCsv extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            craeteProgress();
        }

        @Override
        protected String doInBackground(String... strings) {

            File folder = new File(Environment.getExternalStorageDirectory() + "/QuickKonnect/Documents");
            boolean var = false;
            if (!folder.exists())
                var = folder.mkdir();
            String name = Calendar.getInstance().getTimeInMillis() + "";
            System.out.println("" + var);
            String filename = "";
            if (!OpenItem.equalsIgnoreCase("") && OpenItem.equalsIgnoreCase("1")) {
                filename = folder.toString() + "/" + "Qk_Contact_" + name + ".csv";
            } else if (!OpenItem.equalsIgnoreCase("") && OpenItem.equalsIgnoreCase("2")) {
                filename = folder.toString() + "/" + "Qk_Follower_" + name + ".csv";
            }
            File_Path = filename;
            if (!OpenItem.equalsIgnoreCase("") && OpenItem.equalsIgnoreCase("1")) {
                /***
                 // Export to csv for Contact
                 ***/
                try {
                    FileWriter fw = new FileWriter(filename);

                    fw.append("Index");
                    fw.append(',');

                    fw.append("Name");
                    fw.append(',');

                    fw.append("Phone");
                    fw.append(',');

                    fw.append("Email");
                    fw.append(',');

                    fw.append("Date");
                    fw.append(',');

                    fw.append('\n');

                    for (int i = 0; i < mlist_multiselect.size(); i++) {
                        fw.append(i + 1 + "");
                        fw.append(',');
                        //Name
                        if (mlist_multiselect.get(i).getScanUserName() != null && !mlist_multiselect.get(i).getScanUserName().equalsIgnoreCase("")) {
                            fw.append(mlist_multiselect.get(i).getScanUserName() + "");
                        } else {
                            fw.append("");
                        }
                        fw.append(',');
                        //Phone
                        if (mlist_multiselect.get(i).getPhone() != null && !mlist_multiselect.get(i).getPhone().equalsIgnoreCase("")) {
                            fw.append(mlist_multiselect.get(i).getPhone() + "");
                        } else {
                            fw.append("");
                        }
                        fw.append(',');
                        //Email
                        if (mlist_multiselect.get(i).getEmail() != null && !mlist_multiselect.get(i).getEmail().equalsIgnoreCase("")) {
                            fw.append(mlist_multiselect.get(i).getEmail() + "");
                        } else {
                            fw.append("");
                        }
                        fw.append(',');
                        //Date
                        if (mlist_multiselect.get(i).getScanDate() != null && !mlist_multiselect.get(i).getScanDate().equalsIgnoreCase("")) {
                            String date = mlist_multiselect.get(i).getScanDate();
                            date = date.replace(",", "-");
                            fw.append(date + "");
                        } else {
                            fw.append("");
                        }
                        fw.append(',');
                        fw.append('\n');
                    }
                    // fw.flush();
                    fw.close();
                } catch (Exception e) {
                    File_Path = "";
                    e.printStackTrace();
                }
            } else if (!OpenItem.equalsIgnoreCase("") && OpenItem.equalsIgnoreCase("2")) {
                /***
                 // Export to csv for follower
                 ***/
                try {
                    FileWriter fw = new FileWriter(filename);

                    fw.append("Index");
                    fw.append(',');

                    fw.append("Name");
                    fw.append(',');

                    fw.append("Phone");
                    fw.append(',');

                    fw.append("Email");
                    fw.append(',');

                    fw.append("Date");
                    fw.append(',');

                    fw.append('\n');

                    for (int i = 0; i < followerList_multiselect.size(); i++) {
                        fw.append(i + 1 + "");
                        fw.append(',');
                        //Name
                        if (followerList_multiselect.get(i).getName() != null && !followerList_multiselect.get(i).getName().equalsIgnoreCase("")) {
                            fw.append(followerList_multiselect.get(i).getName() + "");
                        } else {
                            fw.append("");
                        }
                        fw.append(',');
                        //Phone
                        if (followerList_multiselect.get(i).getPhone() != null && !followerList_multiselect.get(i).getPhone().equalsIgnoreCase("")) {
                            fw.append(followerList_multiselect.get(i).getPhone() + "");
                        } else {
                            fw.append("");
                        }
                        fw.append(',');
                        //Email
                        if (followerList_multiselect.get(i).getEmail() != null && !followerList_multiselect.get(i).getEmail().equalsIgnoreCase("")) {
                            fw.append(followerList_multiselect.get(i).getEmail() + "");
                        } else {
                            fw.append("");
                        }
                        fw.append(',');
                        //Date
                        if (followerList_multiselect.get(i).getScan_date() != null && !followerList_multiselect.get(i).getScan_date().equalsIgnoreCase("")) {
                            String date = followerList_multiselect.get(i).getScan_date();
                            date = date.replace(",", "-");
                            fw.append(date + "");
                        } else {
                            fw.append("");
                        }
                        fw.append(',');
                        fw.append('\n');
                    }
                    // fw.flush();
                    fw.close();
                } catch (Exception e) {
                    File_Path = "";
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String unused) {

            try {

                mlist_multiselect.clear();
                isMultiSelect = false;
                followerList_multiselect.clear();
                isMultiSelect_followers = false;
                frame_export.setVisibility(View.GONE);

                if (newContactListAdapter != null)
                    newContactListAdapter.notifyDataSetChanged();

                if (followeradapter != null) {
                    followeradapter.notifyDataSetChanged();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            if (mProgressDialog.isShowing()) {
                mProgressDialog.dismiss();
                AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
                alertDialog.setMessage("Successfully Exported to CSV");
                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                File_Path = "";
                                dialog.dismiss();
                            }
                        });
                alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Share",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

//                                String path  = Environment.getExternalStorageDirectory().getPath() + "/QuickKonnect/Documents/" + File_Path+"";
//                                Uri uri = Uri.parse(Environment.getExternalStorageDirectory().getPath() + "/QuickKonnect/Documents/" + File_Path+"");
//                                final Intent shareIntent = new Intent(Intent.ACTION_SEND);
//                                shareIntent.setType("message/rfc822");
//                                shareIntent.putExtra(Intent.EXTRA_STREAM,Uri.parse("file//"+File_Path));
//                                startActivity(Intent.createChooser(shareIntent, "Share file using"));
//                                dialog.dismiss();

                                Intent i = new Intent(Intent.ACTION_SEND);
                                i.setType("text/csv");
                                try {
                                    String to = "";
                                    String subject = "Contact" + Calendar.getInstance().getTime() + ".csv";
                                    String message = "Contact " + Calendar.getInstance().getTime();

                                    i.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(new File(File_Path)));
                                    i.putExtra(Intent.EXTRA_EMAIL, new String[]{to});
                                    i.putExtra(Intent.EXTRA_SUBJECT, subject);
                                    i.putExtra(Intent.EXTRA_TEXT, message);
                                    startActivity(Intent.createChooser(i, "E-mail"));

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                            }
                        });
//                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Open File",
//                        new DialogInterface.OnClickListener() {
//                            public void onClick(DialogInterface dialog, int which) {
//                                try {
//                                    Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
//                                    Uri uri = Uri.parse(Environment.getExternalStorageDirectory().getPath() + "/QuickKonnect/Documents/" + File_Path + "");
//                                    intent.setDataAndType(uri, "text/csv");
//                                    startActivity(Intent.createChooser(intent, "Open folder"));
////                                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
////                                Uri uri = Uri.parse(Environment.getExternalStorageDirectory().getPath() + "/QuickKonnect/Documents/");
////                                intent.setDataAndType(uri, "text/csv");
////                                startActivity(intent);
//                                    File_Path = "";
//                                } catch (Exception e) {
//                                    e.printStackTrace();
//                                }
//                            }
//                        });
                alertDialog.show();
            }
        }
    }

//    public FileWriter generateCsvFile(File sFileName, String fileContent) {
//        FileWriter writer = null;
//
//        try {
//            writer = new FileWriter(sFileName);
//            writer.append(fileContent);
//            writer.flush();
//
//        } catch (IOException e) {
//            // TODO Auto-generated catch block
//            e.printStackTrace();
//        } finally {
//            try {
//                writer.close();
//            } catch (IOException e) {
//                // TODO Auto-generated catch block
//                e.printStackTrace();
//            }
//        }
//        return writer;
//    }

    //    public void initWidget() {
//
//        UserId = qkPreferences.getLoggedUserid();
//
//        viewContacts = rootView.findViewById(R.id.viewContacts);
//        viewFollowers = rootView.findViewById(R.id.viewFollowers);
//        viewFollowing = rootView.findViewById(R.id.viewFollowing);
//
//        linearContacts = rootView.findViewById(R.id.linearContacts);
//        linearFollowers = rootView.findViewById(R.id.linearFollowers);
//        linerFollowing = rootView.findViewById(R.id.linerFollowing);
//
//        tvContacts = rootView.findViewById(R.id.tvContacts);
//        tvFollowers = rootView.findViewById(R.id.tvFollowers);
//        tvFollowing = rootView.findViewById(R.id.tvFollowing);
//
//        lineContacts = rootView.findViewById(R.id.lineContacts);
//        lineFollowers = rootView.findViewById(R.id.lineFollowers);
//        lineFollowing = rootView.findViewById(R.id.lineFollowing);
//
//        frame_follower = rootView.findViewById(R.id.frame_follower);
//        frame_contact = rootView.findViewById(R.id.frame_contact);
//        frame_following = rootView.findViewById(R.id.frame_following);
//
//        frame_contact.setVisibility(View.VISIBLE);
//        frame_follower.setVisibility(View.GONE);
//        frame_following.setVisibility(View.GONE);
//
//        tv_no_record_found = rootView.findViewById(R.id.tv_no_record_found);
//        tv_no_record_found_Follower = rootView.findViewById(R.id.tv_no_record_found_Follower);
//        tv_no_record_found_following = rootView.findViewById(R.id.tv_no_record_found_following);
//        frame_export = rootView.findViewById(R.id.frame_export);
//        frame_export.setVisibility(View.GONE);
//        //edtContactSearch = rootView.findViewById(R.id.edtContactSearch);
//        recyclerView = rootView.findViewById(R.id.recyclerView);
//        recyclerView.setItemAnimator(new DefaultItemAnimator());
//        recyclerView_follower = rootView.findViewById(R.id.recyclerView_follower);
//        recyclerView_following = rootView.findViewById(R.id.recyclerView_following);
//
//        recyclerView.addItemDecoration(new SimpleDividerItemDecoration(getActivity()));
//        recyclerView_follower.addItemDecoration(new SimpleDividerItemDecoration(getActivity()));
//        recyclerView_following.addItemDecoration(new SimpleDividerItemDecoration(getActivity()));
//        recyclerView.setHasFixedSize(true);
//        recyclerView_follower.setHasFixedSize(true);
//        recyclerView_following.setHasFixedSize(true);
//
//        linearContacts.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                OpenItem = "1";
//                changeTabs(1);
//            }
//        });
//
//        linearFollowers.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                OpenItem = "2";
//                changeTabs(2);
//            }
//        });
//
//        linerFollowing.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                OpenItem = "3";
//                changeTabs(3);
//            }
//        });
//
////        tabLayout.addOnTabcheckedListener(new TabLayout.OnTabSelectedListener() {
////            @Override
////            public void onTabSelected(TabLayout.Tab tab) {
//
////                mlist_multiselect.clear();
////                isMultiSelect = false;
////                followerList_multiselect.clear();
////                isMultiSelect_followers = false;
////                frame_export.setVisibility(View.GONE);
////
////                if (tabLayout.getSelectedTabPosition() == 0) {
////                    OpenItem = "1";
////                    frame_contact.setVisibility(View.VISIBLE);
////                    frame_follower.setVisibility(View.GONE);
////                    frame_following.setVisibility(View.GONE);
////                    //getContacts("name");
////                } else if (tabLayout.getSelectedTabPosition() == 1) {
////                    OpenItem = "2";
////                    frame_contact.setVisibility(View.GONE);
////                    frame_follower.setVisibility(View.VISIBLE);
////                    frame_following.setVisibility(View.GONE);
////                    //getFollowerList();mlist_multiselect.clear();
////                } else if (tabLayout.getSelectedTabPosition() == 2) {
////                    OpenItem = "3";
////                    frame_contact.setVisibility(View.GONE);
////                    frame_follower.setVisibility(View.GONE);
////                    frame_following.setVisibility(View.VISIBLE);
////                    //getFollowingList();
////                }
////            }
////
////            @Override
////            public void onTabUnselected(TabLayout.Tab tab) {
////                mlist_multiselect.clear();
////                isMultiSelect = false;
////                frame_export.setVisibility(View.GONE);
////                followerList_multiselect.clear();
////                isMultiSelect_followers = false;
////            }
////
////            @Override
////            public void onTabReselected(TabLayout.Tab tab) {
////            }
////        });
//
//        mlist = new ArrayList<>();
//        followerList = new ArrayList<>();
//        followingList = new ArrayList<>();
//
//        mlist_multiselect = new ArrayList<>();
//        followerList_multiselect = new ArrayList<>();
//
//        frame_export.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                new ExportToCsv().execute();
//            }
//        });
//
//        recyclerView_follower.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), recyclerView_follower, new RecyclerItemClickListener.OnItemClickListener() {
//            @Override
//            public void onItemClick(View view, int position) {
//                if (followerList_multiselect != null && followerList_multiselect.size() == 0) {
//                    isMultiSelect_followers = false;
//                    followerList_multiselect.clear();
//                    frame_export.setVisibility(View.GONE);
//                }
//                if (isMultiSelect_followers)
//                    multi_select_follower(position);
//            }
//
//            @Override
//            public void onItemLongClick(View view, int position) {
//                if (!isMultiSelect_followers) {
//                    followerList_multiselect.clear();
//                    isMultiSelect_followers = true;
//                    frame_export.setVisibility(View.VISIBLE);
//                    frame_export.startAnimation(fabOpenAnimation);
//                }
//                multi_select_follower(position);
//            }
//        }));
//
//        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), recyclerView, new RecyclerItemClickListener.OnItemClickListener() {
//            @Override
//            public void onItemClick(View view, int position) {
//                if (mlist_multiselect != null && mlist_multiselect.size() == 0) {
//                    isMultiSelect = false;
//                    mlist_multiselect.clear();
//                    frame_export.setVisibility(View.GONE);
//                }
//                if (isMultiSelect)
//                    multi_select(position);
//            }
//
//            @Override
//            public void onItemLongClick(View view, int position) {
//                if (!isMultiSelect) {
//                    mlist_multiselect.clear();
//                    isMultiSelect = true;
//                    frame_export.setVisibility(View.VISIBLE);
//                    frame_export.startAnimation(fabOpenAnimation);
//                }
//                multi_select(position);
//            }
//        }));
//
//        img_filter = rootView.findViewById(R.id.img_filter);
//        mSwipeRefreshLayout = rootView.findViewById(R.id.swipeRefreshLayout);
//        mSwipeRefreshLayout.setOnRefreshListener(this);
//        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary,
//                android.R.color.holo_green_dark,
//                android.R.color.holo_orange_dark,
//                android.R.color.holo_blue_dark);
//
//        img_filter.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                setFilter();
//            }
//        });
//        mLayoutManager = new LinearLayoutManager(context);
//        recyclerView.setLayoutManager(mLayoutManager);
//
//        LinearLayout linearSearch = rootView.findViewById(R.id.linearSearch);
//        linearSearch.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                ((DashboardActivity) getActivity()).setupGlobalSearch();
//            }
//        });
//
//        addFollower();
//        addFollowing();
//
//        showContacts("name", true);
//        //getFollowingList();
//        showFollowers(true);
//    }

//    private void addFollower() {
//        followeradapter = new FollowerContactAdapter(getActivity(), followerList, followerList_multiselect, onContactClick);
//        recyclerView_follower.setLayoutManager(new LinearLayoutManager(getActivity()));
//        recyclerView_follower.setAdapter(followeradapter);
//    }
//
//    private void addFollowing() {
//        followingadapter = new FollowingContactAdapter(getActivity(), followingList, onContactClick);
//        recyclerView_following.setLayoutManager(new LinearLayoutManager(getActivity()));
//        recyclerView_following.setAdapter(followingadapter);
//    }


//    private void parseResponseFollower(JSONObject response) {
//        try {
//            String responseStatus = response.getString("status");
//            if (responseStatus.equals("200")) {
//                Realm realm = Realm.getDefaultInstance();
//                realm.beginTransaction();
//                realm.clear(Follower_contact.class);
//                realm.commitTransaction();
//                JSONArray jsnArr = response.getJSONArray("data");
//                if (jsnArr != null && jsnArr.length() > 0) {
//                    followerList.clear();
//                    for (int i = 0; i < jsnArr.length(); i++) {
//                        JSONObject obj = jsnArr.getJSONObject(i);
//                        Follower_contact contacts = new Follower_contact();
//                        contacts.setUser_id(obj.getString("user_id"));
//                        contacts.setName(obj.getString("name"));
//                        contacts.setEmail(obj.getString("email"));
//                        contacts.setUnique_code(obj.getString("unique_code"));
//                        contacts.setProfile_pic(obj.getString("profile_pic"));
//                        contacts.setProfile_id(obj.getString("profile_id"));
//                        contacts.setPhone(obj.getString("phone"));
//                        contacts.setScan_date(obj.getString("scan_date"));
//                        contacts.setProfile_name(obj.getString("profile_name"));
//
//                        RealmController.with(getActivity()).updateFollower_Contact(contacts);
//                    }
//                    recyclerView_follower.setVisibility(View.VISIBLE);
//                    tv_no_record_found_Follower.setVisibility(View.GONE);
//                    getFollowerList();
//                } else {
//                    recyclerView_follower.setVisibility(View.GONE);
//                    tv_no_record_found_Follower.setVisibility(View.VISIBLE);
//                }
//            } else {
//                recyclerView_follower.setVisibility(View.GONE);
//                tv_no_record_found_Follower.setVisibility(View.VISIBLE);
//            }
//        } catch (Exception e) {
//            recyclerView_follower.setVisibility(View.GONE);
//            tv_no_record_found_Follower.setVisibility(View.VISIBLE);
//            e.printStackTrace();
//        }
//    }

//    public void changeTabs(int pos) {
//
//        mlist_multiselect.clear();
//        isMultiSelect = false;
//        followerList_multiselect.clear();
//        isMultiSelect_followers = false;
//        frame_export.setVisibility(View.GONE);
//
//        switch (pos) {
//            case 1:
//                tvContacts.setTextColor(getResources().getColor(R.color.contact_tabs_selected));
//                tvFollowers.setTextColor(getResources().getColor(R.color.contact_tabs_normal));
//                tvFollowing.setTextColor(getResources().getColor(R.color.contact_tabs_normal));
//
//                lineContacts.setBackgroundColor(getResources().getColor(R.color.contact_tabs_selected));
//                lineFollowers.setBackgroundColor(Color.TRANSPARENT);
//                lineFollowing.setBackgroundColor(Color.TRANSPARENT);
//
//                viewContacts.setVisibility(View.INVISIBLE);
//                viewFollowers.setVisibility(View.VISIBLE);
//                viewFollowing.setVisibility(View.VISIBLE);
//
//                frame_contact.setVisibility(View.VISIBLE);
//                frame_follower.setVisibility(View.GONE);
//                frame_following.setVisibility(View.GONE);
//                break;
//            case 2:
//                tvFollowers.setTextColor(getResources().getColor(R.color.contact_tabs_selected));
//                tvContacts.setTextColor(getResources().getColor(R.color.contact_tabs_normal));
//                tvFollowing.setTextColor(getResources().getColor(R.color.contact_tabs_normal));
//
//                lineFollowers.setBackgroundColor(getResources().getColor(R.color.contact_tabs_selected));
//                lineContacts.setBackgroundColor(Color.TRANSPARENT);
//                lineFollowing.setBackgroundColor(Color.TRANSPARENT);
//
//                viewFollowers.setVisibility(View.INVISIBLE);
//                viewContacts.setVisibility(View.VISIBLE);
//                viewFollowing.setVisibility(View.VISIBLE);
//
//                frame_contact.setVisibility(View.GONE);
//                frame_follower.setVisibility(View.VISIBLE);
//                frame_following.setVisibility(View.GONE);
//                break;
//            case 3:
//                tvFollowing.setTextColor(getResources().getColor(R.color.contact_tabs_selected));
//                tvContacts.setTextColor(getResources().getColor(R.color.contact_tabs_normal));
//                tvFollowers.setTextColor(getResources().getColor(R.color.contact_tabs_normal));
//
//                lineFollowing.setBackgroundColor(getResources().getColor(R.color.contact_tabs_selected));
//                lineContacts.setBackgroundColor(Color.TRANSPARENT);
//                lineFollowers.setBackgroundColor(Color.TRANSPARENT);
//
//                viewFollowing.setVisibility(View.INVISIBLE);
//                viewContacts.setVisibility(View.VISIBLE);
//                viewFollowers.setVisibility(View.VISIBLE);
//
//                frame_contact.setVisibility(View.GONE);
//                frame_follower.setVisibility(View.GONE);
//                frame_following.setVisibility(View.VISIBLE);
//                break;
//        }
//    }
}