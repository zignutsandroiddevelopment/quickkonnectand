package com.fragments;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.activities.BlockedContactsActivity;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.facebook.login.LoginManager;
import com.quickkonnect.ChangePasswordActivity;
import com.quickkonnect.LoginActivity;
import com.quickkonnect.QuickKonnect_Webview;
import com.quickkonnect.R;
import com.quickkonnect.SharedPreference;
import com.utilities.Constants;
import com.utilities.QKPreferences;
import com.utilities.Utilities;
import com.volleyapirequest.VolleyApiRequest;
import com.volleyapirequest.VolleyCallBack;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by ZTLAB09 on 12-12-2017.
 */

public class FragSetting extends Fragment implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {

    Context context;
    View rootView;
    RecyclerView recyclerView;
    private TextView tv_change_password, tv_subscribe_list, tv_notification, tv_help_center, tv_report_problem, tv_app_activation, tv_block;
    private LinearLayout linear_layout;
    private static SharedPreferences mSharedPreferences;
    String userid;
    Switch switch_notification;
    String notification_status;

    private QKPreferences qkPreferences;

    public static FragSetting getInstance() {

        FragSetting fragChats = new FragSetting();
        return fragChats;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.frag_setting, null);
        context = getActivity();

        tv_change_password = rootView.findViewById(R.id.tv_change_password);
        tv_subscribe_list = rootView.findViewById(R.id.tv_subscribe_list);
        tv_notification = rootView.findViewById(R.id.tv_notification);
        tv_help_center = rootView.findViewById(R.id.tv_help_center);
        tv_report_problem = rootView.findViewById(R.id.tv_report_problem);
        tv_app_activation = rootView.findViewById(R.id.tv_app_activation);
        tv_block = rootView.findViewById(R.id.tv_block);
        linear_layout = rootView.findViewById(R.id.linear_layout);
        switch_notification = rootView.findViewById(R.id.switch_notification);

        tv_change_password.setOnClickListener(this);
        tv_subscribe_list.setOnClickListener(this);
        tv_notification.setOnClickListener(this);
        tv_help_center.setOnClickListener(this);
        tv_report_problem.setOnClickListener(this);
        tv_app_activation.setOnClickListener(this);
        tv_block.setOnClickListener(this);

        qkPreferences = new QKPreferences(context);

        mSharedPreferences = getActivity().getSharedPreferences(SharedPreference.PREF_NAME, 0);

        // userid = mSharedPreferences.getString(SharedPreference.CURRENT_USER_ID, "");
        userid = qkPreferences.getLoggedUser().getData().getId() + "";
        notification_status = mSharedPreferences.getString(SharedPreference.NOTIFICATION_STATUS, "");

        if (notification_status.equals("0")) {
            switch_notification.setChecked(false);
        } else {
            switch_notification.setChecked(true);
        }

        switch_notification.setOnCheckedChangeListener(this);
        return rootView;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.tv_block:
                Intent block = new Intent(getActivity(), BlockedContactsActivity.class);
                startActivity(block);
                break;
            case R.id.tv_change_password:
                Intent changepassword = new Intent(getActivity(), ChangePasswordActivity.class);
                startActivity(changepassword);
                break;
            case R.id.tv_subscribe_list:
                break;
            case R.id.tv_notification:
                break;
            case R.id.tv_help_center:
                Intent help_center = new Intent(context, QuickKonnect_Webview.class);
                help_center.putExtra("help_center", Constants.URL_HELP);
                startActivity(help_center);
                break;
            case R.id.tv_report_problem:
                final AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                final EditText edittext = new EditText(getActivity());
                alert.setTitle("Report A Problem");
                edittext.setHint("Enter Your Comments");
                edittext.setMinLines(3);
                edittext.setImeOptions(EditorInfo.IME_ACTION_DONE);
                //  alert.setTitle("Enter Your Comments");

                alert.setView(edittext);
                alert.setPositiveButton("Send Report", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        //What ever you want to do with the value
                        String msg = edittext.getText().toString().trim();
                        if (msg.equals("") || msg.isEmpty() || msg.equals("null")) {
                            Utilities.showToast(context, "Please input valid text");
                        } else {
                            Utilities.hideKeyboard(getActivity());
                            if (!Utilities.isNetworkConnected(context))
                                Toast.makeText(context, Constants.NO_INTERNET_CONNECTION, Toast.LENGTH_LONG).show();
                            else
                                sendReport(userid, msg);
                        }
                    }
                });

                alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        // what ever you want to do with No option.
                        Utilities.hideKeyboard(getActivity());
                    }
                });

                alert.show();

                break;
            case R.id.tv_app_activation:
                final CharSequence[] items = {"Deactivate Account", "Delete Account"};
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle("Make your selection");
                builder.setItems(items, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int item) {
                        // Do something with the selection
                        if (item == 0) {
                            // Deactivate Account
                            if (!Utilities.isNetworkConnected(context))
                                Toast.makeText(context, Constants.NO_INTERNET_CONNECTION, Toast.LENGTH_LONG).show();
                            else
                                showDeleteconfirmation(false);
                        } else if (item == 1) {
                            //Delete  Account
                            if (!Utilities.isNetworkConnected(context))
                                Toast.makeText(context, Constants.NO_INTERNET_CONNECTION, Toast.LENGTH_LONG).show();
                            else
                                showDeleteconfirmation(true);
                        }
                    }
                });
                builder.setPositiveButton("Close", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                builder.show();
                break;
        }
    }

    public void showDeleteconfirmation(final boolean isDelete) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Alert!");
        if (isDelete) {
            builder.setMessage("By Delete account, Your account will be deleted permanently\n Are you sure, Want to Delete Account?");
        } else {
            builder.setMessage("By Delete account, Your account will be disabled temporary\n Are you sure, Want to Deactivate Account?");
        }
        builder.setPositiveButton("Continue", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                if (isDelete) {
                    deleteUserAccount(userid, "P");
                } else {
                    deleteUserAccount(userid, "T");
                }

            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        builder.show();
    }

    // Send Report
    public void sendReport(String user_id, String msg) {

        JSONObject obj = new JSONObject();
        try {
            obj.put("user_id", user_id);
            obj.put("msg", msg);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.e("Object -> ", obj.toString());

        final ProgressDialog mDialog = ProgressDialog.show(getActivity(), null, null, false, true);
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        mDialog.setContentView(R.layout.progress_bar);
        JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, VolleyApiRequest.REQUEST_BASEURL + "api/user_feedback", obj, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                // TODO Auto-generated method stub
                //mDialog.dismiss();

                mDialog.dismiss();
                Log.i("Success --> ", response.toString());

                try {
                    String status = response.getString("status");
                    String message = response.getString("msg");
                    Utilities.showToast(context, message);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                // TODO Auto-generated method stub
                mDialog.dismiss();


                NetworkResponse networkResponse = error.networkResponse;
                if (error instanceof TimeoutError) {
                    Toast.makeText(getActivity(), "Time Out,Network is slow.", Toast.LENGTH_SHORT).show();
                } else if (error instanceof NoConnectionError) {
                    Toast.makeText(getActivity(), "Please Check Your Internet Connection.", Toast.LENGTH_SHORT).show();
                } else if (error instanceof AuthFailureError) {
                    Toast.makeText(getActivity(), "Authentication Error.", Toast.LENGTH_SHORT).show();
                } else if (error instanceof ServerError) {
                    Toast.makeText(getActivity(), "Server Not Connected.", Toast.LENGTH_SHORT).show();
                } else if (error instanceof NetworkError) {
                    Toast.makeText(getActivity(), "Network Error.", Toast.LENGTH_SHORT).show();
                } else if (error instanceof ParseError) {
                    Toast.makeText(getActivity(), "Parse Error.", Toast.LENGTH_SHORT).show();
                }
                Log.i("error --> ", error.toString());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                String token = "";
                if (qkPreferences != null) {
                    token = qkPreferences.getApiToken() + "";
                }

                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "Bearer "+token + "");
                params.put("Accept", "application/json");
                params.put("QK_ACCESS_KEY", context.getResources().getString(R.string.QK_ACCESS_KEY));
                return params;
            }
        };
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(
                30000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Volley.newRequestQueue(getActivity()).add(jsObjRequest);

    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

        if (b) {
            if (!Utilities.isNetworkConnected(context))
                Toast.makeText(context, Constants.NO_INTERNET_CONNECTION, Toast.LENGTH_LONG).show();
            else
                notificationStatusChange(userid, "1");

        } else {
            if (!Utilities.isNetworkConnected(context))
                Toast.makeText(context, Constants.NO_INTERNET_CONNECTION, Toast.LENGTH_LONG).show();
            else
                notificationStatusChange(userid, "0");
        }
    }

    // Change Notification Status ON/OFF
    public void notificationStatusChange(String user_id, String notification_status) {

        JSONObject obj = new JSONObject();
        try {
            obj.put("user_id", user_id);
            obj.put("notification_status", notification_status);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.e("Object -> ", obj.toString());

      /*  final ProgressDialog mDialog = new ProgressDialog(getActivity());
        mDialog.setTitle("Loading...");
        mDialog.setMessage("Please wait!");
        mDialog.setIndeterminate(true);
        mDialog.setCancelable(false);
        mDialog.show();*/
        final ProgressDialog mDialog = ProgressDialog.show(getActivity(), null, null, false, true);
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        mDialog.setContentView(R.layout.progress_bar);
        JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, VolleyApiRequest.REQUEST_BASEURL + "api/notification_toggle", obj, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                // TODO Auto-generated method stub
                //mDialog.dismiss();

                mDialog.dismiss();
                Log.i("Success --> ", response.toString());

                try {
                    String status = response.getString("status");
                    String message = response.getString("msg");
                    Utilities.showToast(context, message);
                    if (status.equals("200")) {

                        SharedPreferences.Editor ed = mSharedPreferences.edit();
                        if (message.equals("Notification Off")) {
                            ed.putString(SharedPreference.NOTIFICATION_STATUS, "0");
                        } else {
                            ed.putString(SharedPreference.NOTIFICATION_STATUS, "1");
                        }

                        ed.commit();
                        Utilities.showToast(context, message);
                       /* SharedPreferences.Editor ed = mSharedPreferences.edit();
                        ed.putString(SharedPreference.NOTIFICATION_STATUS, "");*/

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                // TODO Auto-generated method stub
                mDialog.dismiss();


                NetworkResponse networkResponse = error.networkResponse;
                if (error instanceof TimeoutError) {
                    Toast.makeText(getActivity(), "Time Out,Network is slow.", Toast.LENGTH_SHORT).show();
                } else if (error instanceof NoConnectionError) {
                    Toast.makeText(getActivity(), "Please Check Your Internet Connection.", Toast.LENGTH_SHORT).show();
                } else if (error instanceof AuthFailureError) {
                    Toast.makeText(getActivity(), "Authentication Error.", Toast.LENGTH_SHORT).show();
                } else if (error instanceof ServerError) {
                    Toast.makeText(getActivity(), "Server Not Connected.", Toast.LENGTH_SHORT).show();
                } else if (error instanceof NetworkError) {
                    Toast.makeText(getActivity(), "Network Error.", Toast.LENGTH_SHORT).show();
                } else if (error instanceof ParseError) {
                    Toast.makeText(getActivity(), "Parse Error.", Toast.LENGTH_SHORT).show();
                }
                Log.i("error --> ", error.toString());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String token = "";
                if (qkPreferences != null) {
                    token = qkPreferences.getApiToken() + "";
                }

                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "Bearer "+token + "");
                params.put("Accept", "application/json");
                params.put("QK_ACCESS_KEY", context.getResources().getString(R.string.QK_ACCESS_KEY));
                return params;
            }
        };
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(
                30000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Volley.newRequestQueue(getActivity()).add(jsObjRequest);

    }


    // User Account Temporary Delete
    public void deleteUserAccount(String user_id, String delete_type) {

        JSONObject obj = new JSONObject();
        try {
            obj.put("user_id", user_id);
            obj.put("delete_type", delete_type);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.e("Object -> ", obj.toString());


        new VolleyApiRequest(context, true, VolleyApiRequest.REQUEST_BASEURL + "api/user_account_delete",
                new VolleyCallBack() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {

                            String status = response.getString("status");
                            String message = response.getString("msg");

                            if (status.equals("200")) {

                                //StaticVariable.functionShowMessage(r1, message);
                                Toast.makeText(getActivity(), message + "", Toast.LENGTH_LONG).show();

                                SharedPreferences.Editor e = mSharedPreferences.edit();
                                e.clear();
                                e.remove(SharedPreference.CURRENT_USER_ID);
                                e.remove(SharedPreference.CURRENT_USER_NAME);
                                e.commit();
                                e.apply();
                                LoginManager.getInstance().logOut();
                                qkPreferences.clearQKPreference();
                                Intent intent = new Intent(getActivity(), LoginActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);
                                getActivity().finish();

                            } else if (status.equals("400")) {
                                //  StaticVariable.functionShowMessage(linear_layout, message);
                                Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyApiRequest.showVolleyError(context, error);
                    }
                }).enqueRequest(obj, Request.Method.POST);

      /*  final ProgressDialog mDialog = ProgressDialog.show(getActivity(), null, null, false, true);
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        mDialog.setContentView(R.layout.progress_bar);
        JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, StaticVariable.url + "api/user_account_delete/", obj, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                // TODO Auto-generated method stub
                //mDialog.dismiss();

                mDialog.dismiss();
                Log.i("Success --> ", response.toString());

                try {
                    String status = response.getString("status");
                    String message = response.getString("msg");

                    if (status.equals("200")) {

                        //StaticVariable.functionShowMessage(r1, message);
                        Toast.makeText(getActivity(), message + "", Toast.LENGTH_LONG).show();

                        SharedPreferences.Editor e = mSharedPreferences.edit();
                        e.clear();
                        e.remove(SharedPreference.CURRENT_USER_ID);
                        e.remove(SharedPreference.CURRENT_USER_NAME);
                        e.commit();
                        e.apply();
                        LoginManager.getInstance().logOut();

                        Intent intent = new Intent(getActivity(), LoginActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        getActivity().finish();

                    } else if (status.equals("400")) {
                        //  StaticVariable.functionShowMessage(linear_layout, message);
                        //Toast.makeText(LoginActivity.this, message, Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                // TODO Auto-generated method stub
                mDialog.dismiss();


                NetworkResponse networkResponse = error.networkResponse;
                if (error instanceof TimeoutError) {
                    Toast.makeText(getActivity(), "Time Out,Network is slow.", Toast.LENGTH_SHORT).show();
                } else if (error instanceof NoConnectionError) {
                    Toast.makeText(getActivity(), "Please Check Your Internet Connection.", Toast.LENGTH_SHORT).show();
                } else if (error instanceof AuthFailureError) {
                    Toast.makeText(getActivity(), "Authentication Error.", Toast.LENGTH_SHORT).show();
                } else if (error instanceof ServerError) {
                    Toast.makeText(getActivity(), "Server Not Connected.", Toast.LENGTH_SHORT).show();
                } else if (error instanceof NetworkError) {
                    Toast.makeText(getActivity(), "Network Error.", Toast.LENGTH_SHORT).show();
                } else if (error instanceof ParseError) {
                    Toast.makeText(getActivity(), "Parse Error.", Toast.LENGTH_SHORT).show();
                }
                Log.i("error --> ", error.toString());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("QK_ACCESS_KEY", getResources().getString(R.string.QK_ACCESS_KEY));
                return params;
            }
        };
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(
                30000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Volley.newRequestQueue(getActivity()).add(jsObjRequest);
*/
    }
}