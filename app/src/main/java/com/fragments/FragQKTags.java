package com.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.interfaces.OnQkTagInfo;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.quickkonnect.R;
import com.realmtable.MyProfiles;

import org.json.JSONObject;

public final class FragQKTags extends Fragment {

    private View rootView;
    private MyProfiles myProfiles;
    private ImageView imgTags;
    private TextView tv_cardname;
    DisplayImageOptions imageOptions;
    ImageLoader imageLoader;
    OnQkTagInfo onQkTagInfo;

    public static FragQKTags newInstance(MyProfiles myProfiles, OnQkTagInfo onQkTagInfo) {
        FragQKTags fragment = new FragQKTags();
        fragment.myProfiles = myProfiles;
        fragment.onQkTagInfo = onQkTagInfo;
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.frag_qktags, null);

        imageLoader = ImageLoader.getInstance();

        imgTags = rootView.findViewById(R.id.imgTags);
        tv_cardname = rootView.findViewById(R.id.tv_cardname);

        imageOptions = new DisplayImageOptions.Builder()
                .showImageForEmptyUri(R.drawable.ic_logo_blue_qk)
                .showImageOnFail(R.drawable.ic_logo_blue_qk)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .build();

        try {
            if (myProfiles != null && myProfiles.getName() != null) {
                tv_cardname.setText(myProfiles.getName());
            }
            JSONObject jsonObject = new JSONObject(myProfiles.getQktaginfo());
            if (jsonObject.optString("qr_code") != null && !jsonObject.optString("qr_code").isEmpty())
                imageLoader.displayImage(jsonObject.optString("qr_code"), imgTags, imageOptions);

            imgTags.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // onQkTagInfo.OnQKTaginfo(myProfiles);
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

        return rootView;
    }
}
