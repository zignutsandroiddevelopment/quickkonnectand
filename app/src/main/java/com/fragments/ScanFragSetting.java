package com.fragments;

import android.animation.Animator;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.models.BasicDetail;
import com.models.createprofile.ModelCreateProfile;
import com.models.logindata.ModelLoggedUser;
import com.quickkonnect.BlockUser;
import com.quickkonnect.CircleTransform;
import com.quickkonnect.NewScanProfileActivity;
import com.quickkonnect.R;
import com.quickkonnect.TransferProfile;
import com.quickkonnect.UpdateQkTagActivity;
import com.realmtable.MyProfiles;
import com.realmtable.RealmController;
import com.squareup.picasso.Picasso;
import com.utilities.Constants;
import com.utilities.QKPreferences;
import com.utilities.Utilities;
import com.volleyapirequest.VolleyApiRequest;
import com.volleyapirequest.VolleyCallBack;

import org.json.JSONException;
import org.json.JSONObject;

public class ScanFragSetting extends Fragment {

    View rootView;
    LinearLayout lin_set_as_default, lin_delete, lin_block_user, lin_edit_qk_tag;
    static LinearLayout lin_transfer;
    static TextView tv_pending_transfer;
    String user_id;
    static RelativeLayout rel_unread_followers;
    QKPreferences qkPreferences;
    private String unique_code;
    private ModelLoggedUser modelLoggedUser;
    private int REQUEST_TRANSFER = 3535;
    static boolean isClick = false;
    private String Prof_id = "";
    public MyProfiles myProfiles;

    public String Employee_Unique_Code = "";
    public String Profile_Type = "";

    // Transfer Dialog

    EditText email, search_email;
    TextView title, subtitle, no_user, cancel, search, confirm, invite, username;
    ImageView profileImage, search_icon, header;
    LinearLayout linear_profile, lin_search, lin_emp_dialog_main, linearInvitePeople;
    //TextInputLayout textimput_email_transfer;

    String id = "";

    public static Fragment getInstance(int position, Bundle bundle) {
        bundle.putInt("pos", position);
        ScanFragSetting scanFragSetting = new ScanFragSetting();
        scanFragSetting.setArguments(bundle);
        return scanFragSetting;
    }

    public static ScanFragSetting newInstance(int position, Bundle bundle, MyProfiles myProfiles) {
        bundle.putInt("pos", position);
        ScanFragSetting scanFragSetting = new ScanFragSetting();
        scanFragSetting.myProfiles = myProfiles;
        scanFragSetting.setArguments(bundle);
        return scanFragSetting;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_scan_frag_setting, container, false);

        lin_delete = rootView.findViewById(R.id.lin_delete);
        lin_block_user = rootView.findViewById(R.id.lin_block_user);
        lin_edit_qk_tag = rootView.findViewById(R.id.lin_edit_qk_tag);
        lin_set_as_default = rootView.findViewById(R.id.lin_set_as_default);
        lin_transfer = rootView.findViewById(R.id.lin_transfer);
        rel_unread_followers = rootView.findViewById(R.id.rel_unread_followers);
        tv_pending_transfer = rootView.findViewById(R.id.tv_pending_transfer);

        tv_pending_transfer.setVisibility(View.GONE);
        rel_unread_followers.setVisibility(View.GONE);

        final Bundle extras = getArguments();
        final String from = extras.getString(Constants.KEY);
        qkPreferences = new QKPreferences(getActivity());
        modelLoggedUser = qkPreferences.getLoggedUser();

        try {
            unique_code = extras.getString("unique_code1");
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (from != null && from.equals(Constants.KEY_SCANQK)) {
            if (modelLoggedUser != null && Utilities.isNetworkConnected(getActivity())) {
                //fnScanUser(unique_code, false);
            }
        } else {
            user_id = extras.getString("user_id");
            //getData(modelLoggedUser.getData().getId());
            //getOtherUserData(user_id);
        }
        SetUpForSetting();

        lin_block_user.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Prof_id = qkPreferences.getProfileID();
                Intent blockuser = new Intent(getActivity(), BlockUser.class);
                blockuser.putExtra("ProfileId", Prof_id + "");
                startActivity(blockuser);
            }
        });

        lin_edit_qk_tag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (myProfiles != null) {
                    Prof_id = qkPreferences.getProfileID();
                    JSONObject jsonObject = new JSONObject();
                    String middle_tag = "";
                    try {
                        jsonObject = new JSONObject(myProfiles.getQktaginfo());
                        middle_tag = jsonObject.optString("middle_tag");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    Intent editQKTag = new Intent(getActivity(), UpdateQkTagActivity.class);
                    editQKTag.putExtra(Constants.KEY_FROM, Constants.KEY_FROM_DASHBOARD);
                    editQKTag.putExtra(Constants.KEY_QKTAGINFO, "" + jsonObject);
                    editQKTag.putExtra(Constants.KEY_PROFILEID, Prof_id + "");
                    editQKTag.putExtra(Constants.KEY_PROFILELEUSERID, user_id);
                    editQKTag.putExtra(Constants.KEY_PROFILETYPE, myProfiles.getType() + "");
                    editQKTag.putExtra(Constants.KEY_UNQUECODE, myProfiles.getUnique_code() + "");
                    editQKTag.putExtra(Constants.KEY_MIDDLETAG, middle_tag);
                    editQKTag.putExtra(Constants.KEY_BACKGROUND_GRADIENT, myProfiles.getBackground_gradient() + "");
                    startActivity(editQKTag);
                }
            }
        });

        loadData();
        return rootView;
    }

    private void loadData() {
        String trnsfer = qkPreferences.getIsTransfer();
        String trnsfer_status = qkPreferences.getTransfer_status();
        String status = qkPreferences.getStatus();
        if (trnsfer != null && trnsfer_status != null && status != null) {
            if (trnsfer != null) {
                if (status.equalsIgnoreCase("A")) {
                    lin_transfer.setVisibility(View.VISIBLE);
                    if (trnsfer.equalsIgnoreCase("0")) {
                        isClick = true;
                    } else if (trnsfer.equalsIgnoreCase("1")) {
                        if (trnsfer_status.equalsIgnoreCase("0")) {
                            tv_pending_transfer.setVisibility(View.VISIBLE);
                            rel_unread_followers.setVisibility(View.VISIBLE);
                            isClick = false;
                        } else if (trnsfer_status.equalsIgnoreCase("1")) {
                            tv_pending_transfer.setVisibility(View.GONE);
                            rel_unread_followers.setVisibility(View.GONE);
                            isClick = true;
                        }
                    }
                } else {
                    lin_transfer.setVisibility(View.GONE);
                }
            }
        } else {
            isClick = true;
        }

    }

    public void getData(MyProfiles id) {
        try {
            //MyProfiles myProfiles = RealmController.with(getActivity()).getProfileByID(id+"");
            //MyProfiles  myProfiles = RealmController.with(getActivity()).getProfileByUniqeCode(unique_code);
            if (id != null) {
                if (id.getIs_transfer() != null) {

                    /*switch (id.getStatus()) {
                        case "A":
                            ValidateView();
                            break;
                        case "R":
                            myViewHolder.img_profile_strip.setImageResource(R.drawable.noti_reject);
                            break;
                        case "P":
                            //myViewHolder.img_profile_strip.setVisibility(View.GONE);
                            break;
                    }*/
                    if (id.getStatus().equalsIgnoreCase("A")) {
                        lin_transfer.setVisibility(View.VISIBLE);
                        if (id.getIs_transfer().equalsIgnoreCase("0")) {
                            isClick = true;
                        } else if (id.getIs_transfer().equalsIgnoreCase("1")) {
                            if (id.getTransfer_status().equalsIgnoreCase("0")) {
                                tv_pending_transfer.setVisibility(View.VISIBLE);
                                rel_unread_followers.setVisibility(View.VISIBLE);
                                isClick = false;
                            } else if (id.getTransfer_status().equalsIgnoreCase("1")) {
                                tv_pending_transfer.setVisibility(View.GONE);
                                rel_unread_followers.setVisibility(View.GONE);
                                isClick = true;
                            }
                        }
                    } else {
                        lin_transfer.setVisibility(View.GONE);
                    }
                }
            } else {
                isClick = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void SetUpForSetting() {
        lin_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle("Alert!");
                builder.setMessage("If you delete this profile then your company profile will disappear as well");
                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                        DeleteProfile();
                    }
                });
                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                builder.show();
            }
        });
        lin_set_as_default.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getActivity(), "Coming soon", Toast.LENGTH_SHORT).show();
            }
        });
        lin_transfer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isClick) {
                    Prof_id = qkPreferences.getProfileID();
                    Intent i = new Intent(getActivity(), TransferProfile.class);
                    i.putExtra("PROFILE_ID", Prof_id + "");
                    startActivityForResult(i, REQUEST_TRANSFER);
                    //OpenTransferDialog();
                } else {
                    //Toast.makeText(getActivity(), "Pending", Toast.LENGTH_SHORT).show();
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    String username = "";
                    String msg = "Your profile transfer to " + username + " is in progress, do you want to cancel transfer request?";
                    builder.setTitle("Alert!");
                    builder.setMessage(msg);
                    builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                            DeleteTransferRequest();
                        }
                    });
                    builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    });
                    builder.show();
                }
            }
        });
    }

    private void OpenTransferDialog() {

        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View view = LinearLayout.inflate(getActivity(), R.layout.dialog_transfer_profile, null);
        dialog.setContentView(view);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        dialog.show();
        dialog.getWindow().setAttributes(lp);

        lin_emp_dialog_main = dialog.findViewById(R.id.lin_emp_dialog_main);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            view.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
                @TargetApi(Build.VERSION_CODES.LOLLIPOP)
                @Override
                public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                    v.removeOnLayoutChangeListener(this);
                    revealShow(lin_emp_dialog_main);
                }
            });
        }
        email = dialog.findViewById(R.id.edt_email_transfer);
        no_user = dialog.findViewById(R.id.tv_nouser_transfer);
        linearInvitePeople = dialog.findViewById(R.id.linearInvitePeople);
        title = dialog.findViewById(R.id.tvAlert_dg);
        title.setVisibility(View.VISIBLE);
        subtitle = dialog.findViewById(R.id.tvAlert_subtitle);
        subtitle.setText("Enter the email of user, you would like to transfer profile.");
        cancel = dialog.findViewById(R.id.tvCancel_transfer);
        search = dialog.findViewById(R.id.tvSearch_transfer);
        confirm = dialog.findViewById(R.id.tvConfirm_transfer);
        invite = dialog.findViewById(R.id.tvInvite_transfer);
        username = dialog.findViewById(R.id.tv_name_transfer);
        profileImage = dialog.findViewById(R.id.img_profile_transfer);
        linear_profile = dialog.findViewById(R.id.lin_profile_transfer);
        search_email = dialog.findViewById(R.id.edt_search_mail_transfer);
        search_icon = dialog.findViewById(R.id.img_search_transfer);
        header = dialog.findViewById(R.id.image_emp);
        header.setVisibility(View.GONE);
        lin_search = dialog.findViewById(R.id.lin_search_transfer);
        //textimput_email_transfer = dialog.findViewById(R.id.textimput_email_transfer);

        search.setVisibility(View.VISIBLE);
        cancel.setVisibility(View.VISIBLE);
        invite.setVisibility(View.GONE);
        subtitle.setVisibility(View.VISIBLE);
        confirm.setVisibility(View.GONE);
        linear_profile.setVisibility(View.GONE);
        lin_search.setVisibility(View.GONE);
        email.setVisibility(View.VISIBLE);
        //textimput_email_transfer.setVisibility(View.VISIBLE);
        no_user.setVisibility(View.GONE);
        search_email.setText("");
        username.setText("");

        try {
            search_email.addTextChangedListener(new TextWatcher() {
                public void afterTextChanged(Editable s) {
                    search_email.setCursorVisible(true);
                }

                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                public void onTextChanged(CharSequence s, int start, int before, int count) {
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

        search_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    Utilities.hidekeyboard(getActivity(), search_email);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (!search_email.getText().toString().isEmpty() && Utilities.isValidEmail(search_email.getText().toString())) {
                    getUserFromEmail(search_email.getText().toString());
                    search_email.setText("");
                    username.setText("");
                } else {
                    Utilities.showToast(getActivity(), "Please enter valid email");
                }
            }
        });

        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    Utilities.hidekeyboard(getActivity(), email);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (!email.getText().toString().isEmpty() && Utilities.isValidEmail(email.getText().toString())) {
                    getUserFromEmail(email.getText().toString());
                    search_email.setText("");
                    username.setText("");
                    email.setText("");
                } else {
                    Utilities.showToast(getActivity(), "Please enter valid email");
                }
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (dialog.isShowing())
                    dialog.dismiss();
            }
        });
        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (modelLoggedUser.getData().getId().toString().equalsIgnoreCase(id + "")) {
                    Utilities.showToast(getActivity(), "This profile is already yours");
                } else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setTitle("Alert!");
                    String msg = "";
                    if (!username.getText().toString().isEmpty() && !username.getText().toString().equalsIgnoreCase("") && !username.getText().toString().equalsIgnoreCase("null"))
                        msg = "Are you sure want to transfer this profile to " + username.getText().toString() + "?";
                    else
                        msg = "Are you sure want to transfer this profile?";
                    builder.setMessage(msg);
                    builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                            TransferProfile(id + "");
                            if (dialog.isShowing())
                                dialog.dismiss();
                        }
                    });
                    builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    });
                    builder.show();
                }
            }
        });
        invite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getActivity(), "Coming Soon", Toast.LENGTH_SHORT).show();
                /*shareItem("https://play.google.com/store/apps/details?id=com.quickkonnect");*/
            }
        });
    }

    public void shareItem(String url) {
        if (url != null && !url.isEmpty() && !url.equals("")) {
            Intent share = new Intent(android.content.Intent.ACTION_SEND);
            share.setType("text/plain");
            share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
            share.putExtra(Intent.EXTRA_SUBJECT, "QuickKonnect");
            share.putExtra(Intent.EXTRA_TEXT, url + "");
            startActivity(Intent.createChooser(share, "The Quickkonnect app connects users through a personalized QK-Identification-Code (QK-Tag)."));
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void revealShow(final LinearLayout lin) {
        int w = lin.getWidth();
        int h = lin.getHeight();

        int endRadius = (int) Math.hypot(w, h);
        int cx = (int) (lin_transfer.getX() + (lin_transfer.getWidth() / 2));
        int cy = (int) (lin_transfer.getY()) + lin_transfer.getHeight() + 56;

        Animator revealAnimator = ViewAnimationUtils.createCircularReveal(lin, cx, cy, 5f, endRadius);
        lin.setVisibility(View.VISIBLE);
        revealAnimator.setDuration(500);
        revealAnimator.start();
    }

    private void TransferProfile(String userid) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("profile_id", user_id);
            jsonObject.put("user_id", userid);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        new VolleyApiRequest(getActivity(), true, VolleyApiRequest.REQUEST_BASEURL + "api/profile/transfer_request",
                new VolleyCallBack() {
                    @Override
                    public void onResponse(JSONObject response) {
                        String responseStatus = null;
                        try {
                            responseStatus = response.getString("status");
                            String msg = response.getString("msg");
                            if (responseStatus.equals("200")) {
                                tv_pending_transfer.setVisibility(View.VISIBLE);
                                rel_unread_followers.setVisibility(View.VISIBLE);
                                isClick = false;
                            } else {
                                tv_pending_transfer.setVisibility(View.GONE);
                                rel_unread_followers.setVisibility(View.GONE);
                                isClick = true;
                            }
                            Utilities.showToast(getActivity(), msg);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyApiRequest.showVolleyError(getActivity(), error);
                    }
                }).enqueRequest(jsonObject, Request.Method.POST);
    }

    private void getUserFromEmail(final String email1) {
        try {
            JSONObject obj = new JSONObject();
            try {
                obj.put("email", email1);
                obj.put("user_id", qkPreferences.getLoggedUserid() + "");
            } catch (Exception e) {
                e.printStackTrace();
            }
            new VolleyApiRequest(getActivity(), true, VolleyApiRequest.REQUEST_BASEURL + "api/member/check_member",
                    new VolleyCallBack() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.e("Request Data", "" + response);
                            try {
                                String msg = response.getString("msg");
                                String status = response.getString("status");
                                if (status.equalsIgnoreCase("200")) {
                                    JSONObject data = response.getJSONObject("data");
                                    JSONObject basicdetail = data.getJSONObject("basic_detail");
                                    BasicDetail basicDetail = new Gson().fromJson(basicdetail.toString(), BasicDetail.class);
                                    if (basicDetail != null && !basicDetail.toString().equalsIgnoreCase("null")) {
                                        if (basicDetail.getUserId() != null && !basicDetail.getUserId().toString().equalsIgnoreCase("null")) {
                                            id = basicDetail.getUserId().toString();
                                        }
                                        if (basicDetail.getProfilePic() != null && !basicDetail.getProfilePic().isEmpty() && !basicDetail.getProfilePic().equals("null")) {
                                            Picasso.with(getActivity())
                                                    .load(basicDetail.getProfilePic())
                                                    .error(R.drawable.ic_default_profile_photo)
                                                    .placeholder(R.drawable.ic_default_profile_photo)
                                                    .transform(new CircleTransform())
                                                    .fit()
                                                    .into(profileImage);
                                        } else {
                                            profileImage.setImageResource(R.drawable.ic_default_profile_photo);
                                        }
                                        username.setText(basicDetail.getFirstname() + " " + basicDetail.getLastname());
                                        if (basicDetail.getEmail() != null && !basicDetail.getEmail().equalsIgnoreCase("") && !basicDetail.getEmail().equalsIgnoreCase("null"))
                                            search_email.setText(basicDetail.getEmail());
                                        search.setVisibility(View.GONE);
                                        cancel.setVisibility(View.VISIBLE);
                                        invite.setVisibility(View.GONE);
                                        linearInvitePeople.setVisibility(View.GONE);
                                        confirm.setVisibility(View.VISIBLE);
                                        linear_profile.setVisibility(View.VISIBLE);
                                        email.setVisibility(View.GONE);
                                        lin_search.setVisibility(View.GONE);
                                        header.setVisibility(View.GONE);
                                        title.setVisibility(View.GONE);
                                        subtitle.setVisibility(View.GONE);
                                        //textimput_email_transfer.setVisibility(View.GONE);
                                        no_user.setVisibility(View.GONE);
                                    } else {
                                        linearInvitePeople.setVisibility(View.GONE);
                                        search.setVisibility(View.GONE);
                                        cancel.setVisibility(View.VISIBLE);
                                        invite.setVisibility(View.VISIBLE);
                                        confirm.setVisibility(View.GONE);
                                        linear_profile.setVisibility(View.GONE);
                                        email.setVisibility(View.GONE);
                                        lin_search.setVisibility(View.GONE);
                                        search_email.setText("");
                                        subtitle.setVisibility(View.GONE);
                                        username.setText("");
                                        header.setVisibility(View.GONE);
                                        title.setVisibility(View.GONE);
                                        //textimput_email_transfer.setVisibility(View.GONE);
                                        no_user.setVisibility(View.VISIBLE);
                                    }
                                } else {
                                   /* search.setVisibility(View.GONE);
                                    cancel.setVisibility(View.VISIBLE);
                                    invite.setVisibility(View.VISIBLE);
                                    confirm.setVisibility(View.GONE);
                                    linear_profile.setVisibility(View.GONE);
                                    email.setVisibility(View.GONE);
                                    lin_search.setVisibility(View.GONE);
                                    search_email.setText("");
                                    username.setText("");
                                    header.setVisibility(View.GONE);
                                    subtitle.setVisibility(View.GONE);
                                    title.setVisibility(View.GONE);
                                    if (msg != null && !msg.equalsIgnoreCase(""))
                                        no_user.setText(msg + "");
                                    else
                                        no_user.setText("This email is not registered with QuickKonnect.");
                                    //textimput_email_transfer.setVisibility(View.GONE);
                                    no_user.setVisibility(View.VISIBLE);
                                    //Utilities.showToast(getActivity(), msg + "");*/
                                    search.setVisibility(View.GONE);
                                    cancel.setVisibility(View.VISIBLE);
                                    invite.setVisibility(View.VISIBLE);
                                    invite.setVisibility(View.VISIBLE);
                                    confirm.setVisibility(View.GONE);
                                    linear_profile.setVisibility(View.GONE);
                                    email.setVisibility(View.GONE);
                                    lin_search.setVisibility(View.GONE);
                                    subtitle.setVisibility(View.VISIBLE);
                                    search_email.setText("");
                                    header.setVisibility(View.GONE);
                                    subtitle.setText(msg);
                                    linearInvitePeople.setVisibility(View.GONE);
                                    username.setText("");
                                    //textimput_email_transfer.setVisibility(View.GONE);
                                    no_user.setVisibility(View.VISIBLE);
                                }
                            } catch (JSONException e) {
                                search.setVisibility(View.GONE);
                                cancel.setVisibility(View.VISIBLE);
                                invite.setVisibility(View.VISIBLE);
                                confirm.setVisibility(View.GONE);
                                linear_profile.setVisibility(View.GONE);
                                email.setVisibility(View.GONE);
                                lin_search.setVisibility(View.GONE);
                                search_email.setText("");
                                header.setVisibility(View.GONE);
                                subtitle.setVisibility(View.GONE);
                                title.setVisibility(View.GONE);
                                username.setText("");
                                linearInvitePeople.setVisibility(View.GONE);
                                no_user.setText("This email is not registered with quickkonnect.");
                                //textimput_email_transfer.setVisibility(View.GONE);
                                no_user.setVisibility(View.VISIBLE);
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onErrorResponse(VolleyError error) {
                            VolleyApiRequest.showVolleyError(getActivity(), error);
                        }
                    }).enqueRequest(obj, Request.Method.POST);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void DeleteTransferRequest() {
        try {
            JSONObject obj = new JSONObject();
            try {
                if (user_id != null && !user_id.equalsIgnoreCase("")) {
                    delet_profile_id = user_id;
                    obj.put("profile_id", user_id);
                } else {
                    delet_profile_id = qkPreferences.getProfileID();
                    obj.put("profile_id", qkPreferences.getProfileID());
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.e("request", obj + "");
            new VolleyApiRequest(getActivity(), true, VolleyApiRequest.REQUEST_BASEURL + "api/profile/cancel_transfer_request",
                    new VolleyCallBack() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.e("Request Data", "" + response);
                            ModelCreateProfile modelCreateProfile = new Gson().fromJson(response.toString(), ModelCreateProfile.class);
                            if (modelCreateProfile.getStatus() == 200) {
                                Utilities.showToast(getActivity(), modelCreateProfile.getMsg());
                                //RealmController.with(getActivity()).deleteProfile(delet_profile_id);
                                getActivity().finish();
                            } else {
                                Utilities.showToast(getActivity(), modelCreateProfile.getMsg());
                            }
                        }

                        @Override
                        public void onErrorResponse(VolleyError error) {
                            VolleyApiRequest.showVolleyError(getActivity(), error);
                        }
                    }).enqueRequest(obj, Request.Method.POST);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    String delet_profile_id = "";

    private void DeleteProfile() {

       /* AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Alert!");
        builder.setMessage("You have successfully deleted the company account. For security reasons we have a 24 h waiting period in place for this action to be undone.");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();

            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        builder.show();*/
        JSONObject obj = new JSONObject();

        try {
            if (user_id != null && !user_id.equalsIgnoreCase("")) {
                delet_profile_id = user_id;
                obj.put("profile_id", user_id);
            } else {
                delet_profile_id = qkPreferences.getProfileID();
                obj.put("profile_id", qkPreferences.getProfileID());
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.e("request", obj + "");
        new VolleyApiRequest(getActivity(), true, VolleyApiRequest.REQUEST_BASEURL + "api/profile/profile_delete",
                new VolleyCallBack() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("Request Data", "" + response);
                        ModelCreateProfile modelCreateProfile = new Gson().fromJson(response.toString(), ModelCreateProfile.class);
                        if (modelCreateProfile.getStatus() == 200) {
                            Utilities.showToast(getActivity(), modelCreateProfile.getMsg());
                            //RealmController.with(getActivity()).deleteProfile(delet_profile_id);
                            getActivity().setResult(Activity.RESULT_OK);
                            RealmController.with(getActivity()).deleteProfile(unique_code);

                            try {
                                Employee_Unique_Code = NewScanProfileActivity.Employee_Unique_Code + "";
                                Profile_Type = NewScanProfileActivity.Profile_Type + "";
                                // todo also change for profile type = 2 for enterprise profile.
                                if (Profile_Type != null && Profile_Type.equalsIgnoreCase("1")) {
                                    RealmController.with(getActivity()).deleteProfile(Employee_Unique_Code);
                                }
                                Intent intent = new Intent("BROADCAST_FROM_DELETE_PROFILE");
                                getActivity().sendBroadcast(intent);

                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            ((NewScanProfileActivity) getActivity()).finish();

                        } else {
                            Utilities.showToast(getActivity(), modelCreateProfile.getMsg());
                        }
                    }

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyApiRequest.showVolleyError(getActivity(), error);
                    }
                }).
                enqueRequest(obj, Request.Method.POST);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_TRANSFER && data != null) {
            String respons = data.getStringExtra("Result");
            if (respons.equalsIgnoreCase("1")) {
                tv_pending_transfer.setVisibility(View.VISIBLE);
                rel_unread_followers.setVisibility(View.VISIBLE);
                isClick = false;
            } else if (respons.equalsIgnoreCase("0")) {
                tv_pending_transfer.setVisibility(View.GONE);
                rel_unread_followers.setVisibility(View.GONE);
                isClick = true;
            }
        }
    }
}
