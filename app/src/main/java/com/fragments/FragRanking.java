package com.fragments;

import android.support.v4.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.quickkonnect.R;

/**
 * Created by ZTLAB09 on 12-12-2017.
 */

public class FragRanking extends Fragment {

    Context context;
    View rootView;
    RecyclerView recyclerView;

    public static FragRanking getInstance() {

        FragRanking fragChats = new FragRanking();
        return fragChats;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.frag_ranking, null);
        context = getActivity();

        return rootView;
    }
}