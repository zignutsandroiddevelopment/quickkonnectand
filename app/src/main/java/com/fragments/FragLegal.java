package com.fragments;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.quickkonnect.QuickKonnect_Webview;
import com.quickkonnect.R;
import com.utilities.Constants;

/**
 * Created by ZTLAB09 on 12-12-2017.
 */

public class FragLegal extends Fragment implements View.OnClickListener {

    private Context context;
    private View rootView;
    private TextView tv_term_condition, tv_privacy_contract, tv_quickkonnect_ug, tv_press;
    private ImageView img_term_condition, img_privacy_contract, img_quickkonnect_ug, img_press;

    public static FragLegal getInstance() {
        FragLegal fragChats = new FragLegal();
        return fragChats;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.frag_legal, null);

        context = getActivity();

        tv_term_condition = rootView.findViewById(R.id.tv_term_condition);
        tv_privacy_contract = rootView.findViewById(R.id.tv_privacy_contract);
        tv_quickkonnect_ug = rootView.findViewById(R.id.tv_quickkonnect_ug);
        tv_press = rootView.findViewById(R.id.tv_press);

        img_term_condition = rootView.findViewById(R.id.img_term_condition);
        img_privacy_contract = rootView.findViewById(R.id.img_privacy_contract);
        img_quickkonnect_ug = rootView.findViewById(R.id.img_quickkonnect_ug);
        img_press = rootView.findViewById(R.id.img_press);

        context = getActivity();

        tv_term_condition.setOnClickListener(this);
        tv_privacy_contract.setOnClickListener(this);
        tv_quickkonnect_ug.setOnClickListener(this);
        tv_press.setOnClickListener(this);

        img_term_condition.setOnClickListener(this);
        img_privacy_contract.setOnClickListener(this);
        img_quickkonnect_ug.setOnClickListener(this);
        img_press.setOnClickListener(this);


        return rootView;
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.tv_term_condition:
            case R.id.img_term_condition:
                Intent webviewintent = new Intent(context, QuickKonnect_Webview.class);
                webviewintent.putExtra("terms_condition", Constants.URL_TERMSOFUSE);
                startActivity(webviewintent);
                break;

            case R.id.tv_privacy_contract:
            case R.id.img_privacy_contract:
                Intent privacypolicy = new Intent(context, QuickKonnect_Webview.class);
                privacypolicy.putExtra("privacypolicy", Constants.URL_PRIVACYCONTENT);
                startActivity(privacypolicy);
                break;

            case R.id.tv_quickkonnect_ug:
            case R.id.img_quickkonnect_ug:
                Intent quickkonnect_ug = new Intent(context, QuickKonnect_Webview.class);
                quickkonnect_ug.putExtra("quickkonnect_ug", Constants.URL_UG);
                startActivity(quickkonnect_ug);
                break;

            case R.id.tv_press:
            case R.id.img_press:
//                Intent press = new Intent(Intent.ACTION_VIEW, Uri.parse(Constants.URL_PRESS));
//                startActivity(press);

                Intent press = new Intent(context, QuickKonnect_Webview.class);
                press.putExtra("press", Constants.URL_PRESS);
                startActivity(press);

                break;
        }
    }
}