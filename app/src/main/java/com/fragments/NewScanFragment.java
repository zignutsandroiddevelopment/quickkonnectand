package com.fragments;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.Selection;
import android.text.Spanned;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.activities.AddFoundersActivity;
import com.adapter.AdpFounders;
import com.adapter.AdpOtherAddress;
import com.adapter.AdpSingleEmail;
import com.adapter.AdpSinglePhone;
import com.adapter.AdpSingleWebLink;
import com.adapter.AdpSocialMedium;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.interfaces.OnEditFounder;
import com.interfaces.OnEditMedia;
import com.interfaces.OnSingleEdit;
import com.interfaces.getPosForAddress;
import com.models.createprofile.CreateProfileData;
import com.models.createprofile.Email;
import com.models.createprofile.Founder;
import com.models.createprofile.ModelCreateProfile;
import com.models.createprofile.OtherAddresModel;
import com.models.createprofile.Phone;
import com.models.createprofile.SocialMedium;
import com.models.createprofile.Weblink;
import com.models.logindata.ModelLoggedUser;
import com.quickkonnect.NewScanProfileActivity;
import com.quickkonnect.R;
import com.realmtable.MyProfiles;
import com.realmtable.RealmController;
import com.rilixtech.CountryCodePicker;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;
import com.utilities.Constants;
import com.utilities.QKPreferences;
import com.utilities.Utilities;
import com.volleyapirequest.VolleyApiRequest;
import com.volleyapirequest.VolleyCallBack;
import com.yalantis.ucrop.UCrop;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by ZTLAB03 on 27-03-2018.
 */

public class NewScanFragment extends Fragment implements OnSingleEdit, OnEditFounder, OnEditMedia {

    int position;

    String user_id;

    private CountryCodePicker ccp;

    IntentFilter intentFilter, intentFilterpager;
    public static final String REC_DATA = "REC_DATA";
    public static final String REC_DATA_PAGER = "REC_DATA_PAGER";
    private QKPreferences qkPreferences;
    public static ModelLoggedUser modelLoggedUser;
    public RecyclerView.LayoutManager mLayoutManager;

    public static TextView tv_address, tv_establishmentdate, tv_numberofemplyees, tv_currentCto, tv_date_of_birth;
    public static LinearLayout llAddress, llDOEsta, llNofEmployees;
    public static LinearLayout llPhone, llEmail, llWeblink, llSocialMedia, llFounders, llDOCto, llDODob;
    public static LinearLayout lin_set_as_default, lin_transfer, lin_delete;
    public static LinearLayout lin_basic, lin_followers, lin_setting;
    public static TextView tv_socialmedia_add_info, tv_oa_add_info, tv_add_other_address, tv_founders_add_info, tv_phone_add_info, tv_email_add_info, tv_weblink_add_info, tv_add_socialmedia, tv_add_founders, tv_add_weblink, tv_add_phone, tv_add_email, tv_company_information;

    private static final int PLACE_PICKER_REQUEST_ADDRESS = 1010;
    private static final int REQUEST_SELECT_ADDFOUNDERS = 102;
    private static final int PLACE_PICKER_REQUEST = 103;

    //showeditCompanyinfo
    public static int mYear, mMonth, mDay;
    public static Calendar c;
    public static EditText edt_doestablishment, edt_teamsize, edt_address, edt_dob, edt_ceo/*, edt_cfo*/;
    public static View view;

    // Email
    public static RecyclerView recyclerView_email;
    public static List<Email> listEmail;
    public static AdpSingleEmail adpSingleTextEmail;

    //Other Address
    public static RecyclerView recyclerView_oa;
    public static List<OtherAddresModel> other_address;
    public static AdpOtherAddress adpOtherAddress;

    //Phone
    public static RecyclerView recyclerView_phone;
    public static List<Phone> listPhones;
    public static AdpSinglePhone adpSingleTextPhone;

    //Web Link
    public static RecyclerView recyclerView_weblink;
    public static List<Weblink> listWeblink;
    public static AdpSingleWebLink adpSingleTextWeblink;

    //Social Media
    public static RecyclerView recyclerView_socialmedia;
    public static List<SocialMedium> socialMediumList;
    public static AdpSocialMedium adpSocialMedium;

    //Founders
    public static RecyclerView recyclerView_founders;
    public static List<Founder> foundersList;
    public static AdpFounders adpfounders;

    private boolean isEditinfo = false;
    //    private String key_from = "";
    public static MyProfiles myProfiles;

    int other_add_pos = -1;
    private String editString = "", message = "";
    private int editPos = -1;
    private SocialMedium editsocialMedium = null;
    private String Unique_Code = "", KEY_FROM = "", PROFILE_ID = "", login_user_id = "", compony_id = "";

    public static Fragment getInstance(int position, Bundle bundle) {
        bundle.putInt("pos", position);
        NewScanFragment tabFragment = new NewScanFragment();
        tabFragment.setArguments(bundle);
        return tabFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        position = getArguments().getInt("pos");
        intentFilter = new IntentFilter(REC_DATA);
        intentFilterpager = new IntentFilter(REC_DATA_PAGER);
    }

    public static NewScanFragment newInstance(int position, Bundle bundle, MyProfiles myProfiles) {
        bundle.putInt("pos", position);
        NewScanFragment newScanFragment = new NewScanFragment();
        NewScanFragment.myProfiles = myProfiles;
        newScanFragment.setArguments(bundle);
        return newScanFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_new_scan, container, false);

        lin_basic = view.findViewById(R.id.lin_basic);
        lin_setting = view.findViewById(R.id.lin_setting);
        lin_followers = view.findViewById(R.id.lin_followers);

        final Bundle extras = getArguments();
//        final String from = extras.getString(Constants.KEY);
//        key_from = extras.getString(Constants.KEY);
//        final String role = extras.getString("ROLE");
//        final String role_name = extras.getString("ROLE_NAME");

        qkPreferences = new QKPreferences(getActivity());
        modelLoggedUser = qkPreferences.getLoggedUser();

        try {
            Unique_Code = extras.getString("unique_code1");
            PROFILE_ID = extras.getString("PROFILE_ID");
            KEY_FROM = extras.getString("KEY_FROM");
            compony_id = extras.getString("COMPANY_ID");
            login_user_id = "" + qkPreferences.getValuesInt(QKPreferences.USER_ID);
            PROFILE_ID = "" + qkPreferences.getProfileID();
        } catch (Exception e) {
            e.printStackTrace();
        }

        initWidget(view);

        lin_basic.setVisibility(View.VISIBLE);
        lin_setting.setVisibility(View.GONE);
        lin_followers.setVisibility(View.GONE);

        return view;
    }

    public void showData(Boolean isRefresh) {
        try {

            final MyProfiles myProfiles = RealmController.with(getActivity()).getMyProfielDataByProfileId(Unique_Code);

            if (myProfiles != null) {
                isEditinfo = NewScanProfileActivity.isEditinfo;

//                try {
//
//                    if (myProfiles.getRole().equalsIgnoreCase("0")) {
//
//                        if (key_from.equals(Constants.KEY_FROM_MANAGEPROFILES)) {
//                            this.isEditinfo = true;
//                        }
//
//                    } else {
//
//                        if (myProfiles.getRole().equalsIgnoreCase("2") || myProfiles.getRole().equalsIgnoreCase("3")) {
//                            this.isEditinfo = true;
//
//                        } else {
//                            this.isEditinfo = false;
//                        }
//                    }
//
//                } catch (Exception e) {
//                    e.printStackTrace();
//
//                    if (key_from.equals(Constants.KEY_FROM_MANAGEPROFILES)) {
//                        this.isEditinfo = true;
//                    }
//                }

                if (isEditinfo) {
                    tv_company_information.setTag("1");
                    tv_company_information.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_edit_black_24dp, 0);

                } else {
                    tv_company_information.setTag("0");
                }

                if (myProfiles.getType() != null && (Integer.parseInt(myProfiles.getType()) == Constants.TYPE_PROFILE_COMPANY || Integer.parseInt(myProfiles.getType()) == Constants.TYPE_PROFILE_ENTERPRISE)) {
                    tv_company_information.setText("Company Information");

                } else {
                    tv_company_information.setText("Basic Details");
                }

                if (myProfiles.getType() != null) {
                    try {
                        int profiletype = Integer.parseInt(myProfiles.getType());

                        switch (profiletype) {
                            case Constants.TYPE_PROFILE_COMPANY:
                                //Company
                                llAddress.setVisibility(View.VISIBLE);
                                llDOEsta.setVisibility(View.VISIBLE);
                                llNofEmployees.setVisibility(View.VISIBLE);
                                llDOCto.setVisibility(View.VISIBLE);
                                //llDOCfo.setVisibility(View.VISIBLE);
                                llDODob.setVisibility(View.GONE);
                                break;
                            case Constants.TYPE_PROFILE_ENTERPRISE:
                                //Enterprise
                                llAddress.setVisibility(View.VISIBLE);
                                llDOEsta.setVisibility(View.VISIBLE);
                                llNofEmployees.setVisibility(View.VISIBLE);
                                llDOCto.setVisibility(View.VISIBLE);
                                //llDOCfo.setVisibility(View.VISIBLE);
                                llDODob.setVisibility(View.GONE);
                                break;
                            case Constants.TYPE_PROFILE_CELEBRITY:
                                //Celebrity/Public Figure
                                llAddress.setVisibility(View.VISIBLE);
                                llDOEsta.setVisibility(View.GONE);
                                llNofEmployees.setVisibility(View.GONE);
                                llDOCto.setVisibility(View.GONE);
                                //llDOCfo.setVisibility(View.GONE);
                                llDODob.setVisibility(View.VISIBLE);
                                break;
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                // Number of Employees
                if (myProfiles.getCompany_team_size() != null && Utilities.isEmpty(myProfiles.getCompany_team_size()) && (myProfiles.getType().equals("" + Constants.TYPE_PROFILE_COMPANY) || myProfiles.getType().equals("" + Constants.TYPE_PROFILE_ENTERPRISE))) {
                    llNofEmployees.setVisibility(View.VISIBLE);
                    tv_numberofemplyees.setText(myProfiles.getCompany_team_size());
                    if (myProfiles.getCompany_team_size().equalsIgnoreCase("0")) {
                        llNofEmployees.setVisibility(View.GONE);
                    } else {
                        llNofEmployees.setVisibility(View.VISIBLE);
                    }
                } else {
                    llNofEmployees.setVisibility(View.GONE);
                }

                // Address
                if (myProfiles.getAddress() != null && Utilities.isEmpty(myProfiles.getAddress())) {
                    llAddress.setVisibility(View.VISIBLE);
                    tv_address.setText(myProfiles.getAddress());
                    if (myProfiles.getLatitude() != null && myProfiles.getLongitude() != null) {
                        tv_address.setTag("" + myProfiles.getLatitude() + "," + myProfiles.getLongitude());
                    }
                } else {
                    llAddress.setVisibility(View.GONE);
                }

                //Dob
                if (myProfiles.getDob() != null && Utilities.isEmpty(myProfiles.getDob())) {
                    llDODob.setVisibility(View.VISIBLE);
                    tv_date_of_birth.setText(myProfiles.getDob());
                } else {
                    llDODob.setVisibility(View.GONE);
                }

                //Date of Establish
                if ((myProfiles.getType() != null && (myProfiles.getType().equals("" + Constants.TYPE_PROFILE_COMPANY) || myProfiles.getType().equals("" + Constants.TYPE_PROFILE_ENTERPRISE)) && myProfiles.getEstablish_date() != null)
                        && Utilities.isEmpty(myProfiles.getEstablish_date())) {
                    ((TextView) view.findViewById(R.id.tvdate)).setText("Established Date : ");
                    llDOEsta.setVisibility(View.VISIBLE);
                    if (!myProfiles.getEstablish_date().equalsIgnoreCase("")) {
                        String endDate = myProfiles.getEstablish_date();
                        try {
                            String format_date[] = endDate.split("-");
                            endDate = format_date[2] + "/" + format_date[1] + "/" + format_date[0];
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        tv_establishmentdate.setText(endDate);
                        llDOEsta.setVisibility(View.VISIBLE);
                    } else {
                        llDOEsta.setVisibility(View.GONE);
                    }

                } else if ((myProfiles.getType() != null && myProfiles.getType().equals("" + Constants.TYPE_PROFILE_CELEBRITY) && myProfiles.getEstablish_date() != null)) {
                    llDOEsta.setVisibility(View.GONE);
                } else {
                    llDOEsta.setVisibility(View.GONE);
                }

                //CEO as CTO
                if (myProfiles.getCurrent_ceo() != null && Utilities.isEmpty(myProfiles.getCurrent_ceo())) {
                    llDOCto.setVisibility(View.VISIBLE);
                    tv_currentCto.setText(myProfiles.getCurrent_ceo());
                } else {
                    llDOCto.setVisibility(View.GONE);
                }

                initAdaptors(myProfiles.getId(), myProfiles.getUser_id());

                // OtherAddress
                String otheraddress = myProfiles.getOtheraddress();
                if (otheraddress != null && otheraddress.length() > 0) {
                    if (other_address != null)
                        other_address.clear();
                    Type type = new TypeToken<List<OtherAddresModel>>() {
                    }.getType();
                    List<OtherAddresModel> phoneList = new Gson().fromJson(otheraddress, type);
                    if (phoneList != null && phoneList.size() > 0) {
                        view.findViewById(R.id.lin_oa).setVisibility(View.VISIBLE);
                        other_address.addAll(phoneList);
                        if (adpOtherAddress != null)
                            adpOtherAddress.notifyDataSetChanged();
                        if (isEditinfo) {
                            tv_add_other_address.setVisibility(View.VISIBLE);
                            tv_add_other_address.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_add_black_24dp, 0);
                            tv_oa_add_info.setVisibility(View.GONE);
                            tv_oa_add_info.setTag("1");
                        }
                    } else {
                        if (isEditinfo) {
                            tv_add_other_address.setVisibility(View.VISIBLE);
                            tv_add_other_address.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                            tv_oa_add_info.setVisibility(View.VISIBLE);
                            tv_oa_add_info.setTag("0");
                            view.findViewById(R.id.lin_oa).setVisibility(View.VISIBLE);

                        } else
                            view.findViewById(R.id.lin_oa).setVisibility(View.GONE);
                    }
                } else {
                    if (isEditinfo) {
                        tv_add_other_address.setVisibility(View.VISIBLE);
                        tv_add_other_address.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                        tv_oa_add_info.setVisibility(View.VISIBLE);
                        tv_oa_add_info.setTag("0");
                        view.findViewById(R.id.lin_oa).setVisibility(View.VISIBLE);

                    } else
                        view.findViewById(R.id.lin_oa).setVisibility(View.GONE);
                }

                // Phones
                String phones = myProfiles.getPhone();
                if (phones != null && phones.length() > 0) {
                    if (listPhones != null)
                        listPhones.clear();
                    Type type = new TypeToken<List<Phone>>() {
                    }.getType();
                    List<Phone> phoneList = new Gson().fromJson(phones, type);
                    if (phoneList != null && phoneList.size() > 0) {
                        llPhone.setVisibility(View.VISIBLE);
                        listPhones.addAll(phoneList);
                        if (adpSingleTextPhone != null)
                            adpSingleTextPhone.notifyDataSetChanged();
                        if (isEditinfo) {
                            tv_phone_add_info.setVisibility(View.GONE);
                            tv_add_phone.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_add_black_24dp, 0);
                            tv_add_phone.setTag("1");
                        }
                    } else {

                        if (isEditinfo) {
                            tv_phone_add_info.setVisibility(View.VISIBLE);
                            tv_add_phone.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                            tv_add_phone.setTag("0");
                            llPhone.setVisibility(View.VISIBLE);

                        } else llPhone.setVisibility(View.GONE);
                    }
                } else {
                    if (isEditinfo) {
                        tv_phone_add_info.setVisibility(View.VISIBLE);
                        tv_add_phone.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                        tv_add_phone.setTag("0");
                        llPhone.setVisibility(View.VISIBLE);

                    } else llPhone.setVisibility(View.GONE);
                }

                // Emails
                String emails = myProfiles.getEmail();
                if (emails != null && emails.length() > 0) {
                    if (listEmail != null)
                        listEmail.clear();
                    Type type = new TypeToken<List<Email>>() {
                    }.getType();
                    List<Email> emailList = new Gson().fromJson(emails, type);
                    if (emailList != null && emailList.size() > 0) {
                        llEmail.setVisibility(View.VISIBLE);
                        listEmail.addAll(emailList);
                        if (adpSingleTextEmail != null)
                            adpSingleTextEmail.notifyDataSetChanged();

                        if (isEditinfo) {
                            tv_email_add_info.setVisibility(View.GONE);
                            tv_add_email.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_add_black_24dp, 0);
                            tv_add_email.setTag("1");
                        }

                    } else {
                        if (isEditinfo) {
                            tv_email_add_info.setVisibility(View.VISIBLE);
                            tv_add_email.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                            tv_add_email.setTag("0");
                            llEmail.setVisibility(View.VISIBLE);

                        } else llEmail.setVisibility(View.GONE);
                    }
                } else {
                    if (isEditinfo) {
                        tv_email_add_info.setVisibility(View.VISIBLE);
                        tv_add_email.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                        tv_add_email.setTag("0");
                        llEmail.setVisibility(View.VISIBLE);

                    } else llEmail.setVisibility(View.GONE);
                }

                // Web Link
                String weblinks = myProfiles.getWeblink();
                if (weblinks != null && weblinks.length() > 0) {
                    if (listWeblink != null)
                        listWeblink.clear();
                    Type type = new TypeToken<List<Weblink>>() {
                    }.getType();
                    List<Weblink> weblinkList = new Gson().fromJson(weblinks, type);
                    if (weblinkList != null && weblinkList.size() > 0) {
                        llWeblink.setVisibility(View.VISIBLE);
                        listWeblink.addAll(weblinkList);
                        if (adpSingleTextWeblink != null)
                            adpSingleTextWeblink.notifyDataSetChanged();

                        if (isEditinfo) {
                            tv_weblink_add_info.setVisibility(View.GONE);
                            tv_add_weblink.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_add_black_24dp, 0);
                            tv_add_weblink.setTag("1");
                        }

                    } else {

                        if (isEditinfo) {
                            tv_weblink_add_info.setVisibility(View.VISIBLE);
                            tv_add_weblink.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                            tv_add_weblink.setTag("0");
                            llWeblink.setVisibility(View.VISIBLE);

                        } else llWeblink.setVisibility(View.GONE);
                    }

                } else {
                    if (isEditinfo) {
                        tv_weblink_add_info.setVisibility(View.VISIBLE);
                        tv_add_weblink.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                        tv_add_weblink.setTag("0");
                        llWeblink.setVisibility(View.VISIBLE);

                    } else llWeblink.setVisibility(View.GONE);
                }

                // Social Media
                String socialmedia = myProfiles.getSocial_media();
                if (socialmedia != null && socialmedia.length() > 0) {
                    if (socialMediumList != null)
                        socialMediumList.clear();
                    Type type = new TypeToken<List<SocialMedium>>() {
                    }.getType();
                    List<SocialMedium> socialMediumList1 = new Gson().fromJson(socialmedia, type);
                    if (socialMediumList1 != null && socialMediumList1.size() > 0) {
                        llSocialMedia.setVisibility(View.VISIBLE);
                        socialMediumList.addAll(socialMediumList1);
                        if (adpSocialMedium != null)
                            adpSocialMedium.notifyDataSetChanged();

                        if (isEditinfo) {
                            tv_socialmedia_add_info.setVisibility(View.GONE);
                            tv_add_socialmedia.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_add_black_24dp, 0);
                            tv_add_socialmedia.setTag("1");
                        }

                    } else {
                        if (isEditinfo) {
                            tv_socialmedia_add_info.setVisibility(View.VISIBLE);
                            tv_add_socialmedia.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                            tv_add_socialmedia.setTag("0");
                            llSocialMedia.setVisibility(View.VISIBLE);

                        } else llSocialMedia.setVisibility(View.GONE);
                    }

                } else {
                    if (isEditinfo) {
                        tv_socialmedia_add_info.setVisibility(View.VISIBLE);
                        tv_add_socialmedia.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                        tv_add_socialmedia.setTag("0");
                        llSocialMedia.setVisibility(View.VISIBLE);

                    } else llSocialMedia.setVisibility(View.GONE);
                }

                // Founders
                String founders = myProfiles.getFounders();
                if (myProfiles.getType() != null && (Integer.parseInt(myProfiles.getType()) == Constants.TYPE_PROFILE_COMPANY
                        || Integer.parseInt(myProfiles.getType()) == Constants.TYPE_PROFILE_ENTERPRISE)) {
                    if (founders != null && founders.length() > 0) {
                        if (foundersList != null)
                            foundersList.clear();
                        Type type = new TypeToken<List<Founder>>() {
                        }.getType();
                        List<Founder> founderList = new Gson().fromJson(founders, type);
                        if (founderList != null && founderList.size() > 0) {
                            llFounders.setVisibility(View.VISIBLE);
                            foundersList.addAll(founderList);
                            if (adpfounders != null)
                                adpfounders.notifyDataSetChanged();

                            if (isEditinfo) {
                                tv_founders_add_info.setVisibility(View.GONE);
                                tv_add_founders.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_add_black_24dp, 0);
                                tv_add_founders.setTag("1");
                            }

                        } else {
                            if (isEditinfo) {
                                tv_founders_add_info.setVisibility(View.VISIBLE);
                                tv_add_founders.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                                tv_add_founders.setTag("0");
                                llFounders.setVisibility(View.VISIBLE);

                            } else llFounders.setVisibility(View.GONE);
                        }
                    } else {
                        if (isEditinfo) {
                            tv_founders_add_info.setVisibility(View.VISIBLE);
                            tv_add_founders.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                            tv_add_founders.setTag("0");
                            llFounders.setVisibility(View.VISIBLE);

                        } else llFounders.setVisibility(View.GONE);
                    }
                }
            } else {
                Utilities.showToast(getActivity(), "No internet connection, please try after some time.");
            }

            if (isRefresh) {
                if (KEY_FROM != null && KEY_FROM.equals(Constants.KEY_SCANQK))
                    fnScanUser(Unique_Code, false);
                else
                    getOtherUserData(PROFILE_ID, false);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initAdaptors(String visited_profile_id, String visited_user_id) {

        listEmail = new ArrayList<>();
        adpSingleTextEmail = new AdpSingleEmail(getActivity(), listEmail, this, isEditinfo);
        recyclerView_email.setAdapter(adpSingleTextEmail);


        listPhones = new ArrayList<>();
        adpSingleTextPhone = new AdpSinglePhone(getActivity(), listPhones, this, isEditinfo);
        recyclerView_phone.setAdapter(adpSingleTextPhone);


        listWeblink = new ArrayList<>();
        adpSingleTextWeblink = new AdpSingleWebLink(getActivity(), listWeblink, this, isEditinfo, visited_user_id, visited_profile_id);
        recyclerView_weblink.setAdapter(adpSingleTextWeblink);


        socialMediumList = new ArrayList<>();
        adpSocialMedium = new AdpSocialMedium(getActivity(), socialMediumList, this, isEditinfo, visited_user_id, visited_profile_id);
        recyclerView_socialmedia.setAdapter(adpSocialMedium);


        foundersList = new ArrayList<>();
        adpfounders = new AdpFounders(getActivity(), foundersList, this, isEditinfo);
        recyclerView_founders.setAdapter(adpfounders);


        other_address = new ArrayList<>();
        adpOtherAddress = new AdpOtherAddress(getActivity(), other_address, this, isEditinfo, posadapwet);
        recyclerView_oa.setAdapter(adpOtherAddress);
    }

    public void initWidget(View view) {

        llPhone = view.findViewById(R.id.llPhone);
        llEmail = view.findViewById(R.id.llEmail);
        llWeblink = view.findViewById(R.id.llWeblink);
        llSocialMedia = view.findViewById(R.id.llSocialmedia);
        llFounders = view.findViewById(R.id.llFounder);
        //llDOCfo = view.findViewById(R.id.llDOCfo);
        llDOCto = view.findViewById(R.id.llDOCto);
        llDODob = view.findViewById(R.id.llDODob);

        lin_delete = view.findViewById(R.id.lin_delete);
        lin_set_as_default = view.findViewById(R.id.lin_set_as_default);
        lin_transfer = view.findViewById(R.id.lin_transfer);

        SetUpForSetting();

        tv_oa_add_info = view.findViewById(R.id.tv_oa_add_info);
        tv_add_other_address = view.findViewById(R.id.tv_add_other_address);
        tv_address = view.findViewById(R.id.tv_address);
        tv_establishmentdate = view.findViewById(R.id.tv_establishmentdate);
        tv_numberofemplyees = view.findViewById(R.id.tv_numberofemplyees);
        tv_currentCto = view.findViewById(R.id.tv_currentCto);
        //tv_currentCfo = view.findViewById(R.id.tv_currentCfo);
        tv_date_of_birth = view.findViewById(R.id.tv_date_of_birth);

        tv_company_information = view.findViewById(R.id.tv_company_information);

        tv_add_founders = view.findViewById(R.id.tv_add_founder);
        tv_add_socialmedia = view.findViewById(R.id.tv_add_socialmedia);
        tv_add_weblink = view.findViewById(R.id.tv_add_weblink);
        tv_add_phone = view.findViewById(R.id.tv_add_phone);
        tv_add_email = view.findViewById(R.id.tv_add_email);

        tv_founders_add_info = view.findViewById(R.id.tv_founder_add_info);
        tv_socialmedia_add_info = view.findViewById(R.id.tv_socialmedia_add_info);
        tv_weblink_add_info = view.findViewById(R.id.tv_weblink_add_info);
        tv_phone_add_info = view.findViewById(R.id.tv_phone_add_info);
        tv_email_add_info = view.findViewById(R.id.tv_email_add_info);

        llAddress = view.findViewById(R.id.llAddress);
        llDOEsta = view.findViewById(R.id.llDOEsta);
        llNofEmployees = view.findViewById(R.id.llNofEmployees);

        tv_company_information.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (view.getTag() != null && view.getTag().toString().equals("1")) {
                    showeditCompanyinfo();
                }
            }
        });

        tv_add_email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (view.getTag() != null && view.getTag().equals("1")) {
                    showSingleTextPopup(OnSingleEdit.TYPE_EMAIL, false);
                }
            }
        });
        tv_email_add_info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showSingleTextPopup(OnSingleEdit.TYPE_EMAIL, false);
            }
        });

        tv_add_phone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (view.getTag() != null && view.getTag().equals("1")) {
                    showSingleTextPopup(OnSingleEdit.TYPE_PHONE, false);
                }
            }
        });
        tv_phone_add_info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showSingleTextPopup(OnSingleEdit.TYPE_PHONE, false);
            }
        });

        tv_add_weblink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (view.getTag() != null && view.getTag().equals("1")) {
                    showSingleTextPopup(OnSingleEdit.TYPE_WEBLINK, false);
                }
            }
        });
        tv_weblink_add_info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showSingleTextPopup(OnSingleEdit.TYPE_WEBLINK, false);
            }
        });

        tv_add_socialmedia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (view.getTag() != null && view.getTag().equals("1")) {
                    showpopupSociamMedia(false);
                }
            }
        });

        tv_socialmedia_add_info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showpopupSociamMedia(false);
            }
        });

        tv_add_founders.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (view.getTag() != null && view.getTag().equals("1")) {
                    addFounders(-1, null);
                }
            }
        });

        tv_founders_add_info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addFounders(-1, null);
            }
        });

        // Plus
        tv_oa_add_info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Open_Place_Picker();
            }
        });

        // Add Address
        tv_add_other_address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Open_Place_Picker();
            }
        });

        recyclerView_oa = view.findViewById(R.id.recyclerView_oa);
        recyclerView_phone = view.findViewById(R.id.recyclerView_phone);
        recyclerView_email = view.findViewById(R.id.recyclerView_email);
        recyclerView_weblink = view.findViewById(R.id.recyclerView_weblink);
        recyclerView_socialmedia = view.findViewById(R.id.recyclerView_socialmedia);
        recyclerView_founders = view.findViewById(R.id.recyclerView_founders);

        // Phone
        //recyclerView_phone.addItemDecoration(new SimpleDividerItemDecoration(getActivity()));
        recyclerView_phone.setNestedScrollingEnabled(false);
        recyclerView_phone.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView_phone.setLayoutManager(mLayoutManager);

        // Email
        //recyclerView_email.addItemDecoration(new SimpleDividerItemDecoration(getActivity()));
        recyclerView_email.setNestedScrollingEnabled(false);
        recyclerView_email.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView_email.setLayoutManager(mLayoutManager);

        // Web Link
        //recyclerView_weblink.addItemDecoration(new SimpleDividerItemDecoration(getActivity()));
        recyclerView_weblink.setNestedScrollingEnabled(false);
        recyclerView_weblink.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView_weblink.setLayoutManager(mLayoutManager);

        // Social Media
        //recyclerView_socialmedia.addItemDecoration(new SimpleDividerItemDecoration(getActivity()));
        recyclerView_socialmedia.setNestedScrollingEnabled(false);
        recyclerView_socialmedia.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView_socialmedia.setLayoutManager(mLayoutManager);

        // Other Address
        //recyclerView_oa.addItemDecoration(new SimpleDividerItemDecoration(getActivity()));
        recyclerView_oa.setNestedScrollingEnabled(false);
        recyclerView_oa.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView_oa.setLayoutManager(mLayoutManager);

        // Founders
        //recyclerView_founders.addItemDecoration(new SimpleDividerItemDecoration(getActivity()));
        recyclerView_founders.setNestedScrollingEnabled(false);
        recyclerView_founders.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView_founders.setLayoutManager(mLayoutManager);


        MyProfiles myProfiles = RealmController.with(getActivity()).getMyProfielDataByProfileId(Unique_Code);

        if (KEY_FROM != null && KEY_FROM.equals(Constants.KEY_SCANQK)) {
            if (myProfiles != null)
                showData(true);
            else if (login_user_id != null && Utilities.isNetworkConnected(getActivity()))
                fnScanUser(Unique_Code, true);

        }
        // todo remove if not use
        else if (KEY_FROM != null && KEY_FROM.equals(Constants.KEY_FROM_NOTIFICATIONS)) {
            getOtherUserData(PROFILE_ID, true);
        } else {

            if (myProfiles != null)
                showData(true);
            else
                getOtherUserData(PROFILE_ID, true);
        }
        //showData(myProfiles);

    }

    private void SetUpForSetting() {
        lin_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DeleteProfile();
            }
        });
        lin_set_as_default.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getActivity(), "default", Toast.LENGTH_SHORT).show();
            }
        });
        lin_transfer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getActivity(), "transfer", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void DeleteAddress(int pos) {
        other_address.remove(pos);
        adpOtherAddress.notifyDataSetChanged();
        if (other_address != null && other_address.size() > 0) {
            recyclerView_oa.setVisibility(View.VISIBLE);
            tv_oa_add_info.setVisibility(View.GONE);
            tv_add_other_address.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_add_black_24dp, 0);
        } else {
            recyclerView_oa.setVisibility(View.GONE);
            tv_oa_add_info.setVisibility(View.VISIBLE);
            //tv_add_other_address.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_add_black_24dp, 0);
        }
    }

    public void addFounders(int pos, Founder founder) {
        if (founder != null) {
            startActivityForResult(new Intent(getActivity(), AddFoundersActivity.class)
                    .putExtra("data", founder)
                    .putExtra("pos", pos), REQUEST_SELECT_ADDFOUNDERS);
        } else {
            startActivityForResult(new Intent(getActivity(), AddFoundersActivity.class), REQUEST_SELECT_ADDFOUNDERS);
        }
    }

    public void Open_Place_Picker(int pos) {
        other_add_pos = pos;
        try {
            PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
            startActivityForResult(builder.build(getActivity()), PLACE_PICKER_REQUEST_ADDRESS);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    getPosForAddress posadapwet = new getPosForAddress() {
        @Override
        public void getPOsforAddress(int pos, String type) {
            if (type.equalsIgnoreCase("DELETE")) {
                DeleteAddress(pos);
            } else if (type.equalsIgnoreCase("ADDRESS")) {
                Open_Place_Picker(pos);
            }
        }
    };

    private void Open_Place_Picker() {
        try {
            other_add_pos = -1;
            PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
            startActivityForResult(builder.build(getActivity()), PLACE_PICKER_REQUEST_ADDRESS);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean isAlreadySocialAdded(String type) {
        if (socialMediumList != null && socialMediumList.size() > 0) {
            for (SocialMedium socialMedium: socialMediumList) {
                if (socialMedium.getType().equals(type)) {
                    return true;
                }
            }
        }
        return false;
    }

    String Text_message = "";

    public void showpopupSociamMedia(final boolean isEdit) {

        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        View view = LinearLayout.inflate(getActivity(), R.layout.dialog_addsocialmedia, null);
        dialog.setContentView(view);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;

        dialog.show();
        dialog.getWindow().setAttributes(lp);
        TextView tvAlert = view.findViewById(R.id.tvAlert);
        TextView tvDone = view.findViewById(R.id.tvDone);
        TextView tvCancel = view.findViewById(R.id.tvCancel);
        ccp = (CountryCodePicker) view.findViewById(R.id.ccp);
        LinearLayout lin_cpp = view.findViewById(R.id.lin_cpp);

        view.findViewById(R.id.divider_select_socialmedia).setVisibility(View.VISIBLE);

        ccp.enablePhoneAutoFormatter(true);
        tvAlert.setText("Add social media");

        final SearchableSpinner spinnerSocialmedia = view.findViewById(R.id.spinnerSocialMedia);

        final EditText edtSocialmediaLInk = view.findViewById(R.id.edt_socialmedia_link);

        String items[] = {"Facebook", "LinkedIn", "Twitter", "Instagram", "Google Plus"};
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, items);
        spinnerSocialmedia.setTitle("Select social media");
        spinnerSocialmedia.setAdapter(spinnerArrayAdapter);

        if (isEdit && editsocialMedium != null) {
            edtSocialmediaLInk.setText(editsocialMedium.getName());
            int media_type = Integer.parseInt(editsocialMedium.getType());
            spinnerSocialmedia.setSelection(media_type - 1);
            int pos = spinnerSocialmedia.getSelectedItemPosition();
            Text_message = getSocialMediaText(pos);
        }

        spinnerSocialmedia.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                //Toast.makeText(getActivity(), spinnerSocialmedia.getSelectedItem().toString() + "", Toast.LENGTH_SHORT).show();
                int pos = spinnerSocialmedia.getSelectedItemPosition();
                Text_message = getSocialMediaText(pos);
                edtSocialmediaLInk.setText(Text_message + "");
                Selection.setSelection(edtSocialmediaLInk.getText(), edtSocialmediaLInk.getText().length());
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        int pos = spinnerSocialmedia.getSelectedItemPosition();
        Text_message = getSocialMediaText(pos);
        edtSocialmediaLInk.setText(Text_message + "");
        Selection.setSelection(edtSocialmediaLInk.getText(), edtSocialmediaLInk.getText().length());

        edtSocialmediaLInk.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!s.toString().startsWith(Text_message + "")) {
                    edtSocialmediaLInk.setText(Text_message + "");
                    Selection.setSelection(edtSocialmediaLInk.getText(), edtSocialmediaLInk.getText().length());
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        tvDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    Utilities.hidekeyboard(getActivity(), view);
                    //((NewScanProfileActivity) getActivity()).HideKeyboard();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                dialog.dismiss();
                //Utilities.hidekeyboard(getActivity(), getActivity().getWindow().getDecorView());
                if (!edtSocialmediaLInk.getText().toString().isEmpty() && isValidUrl(edtSocialmediaLInk.getText().toString())) {
                    int pos = spinnerSocialmedia.getSelectedItemPosition();
                    if (edtSocialmediaLInk != null && !Utilities.isEmpty(edtSocialmediaLInk.getText().toString())) {
                        Utilities.showToast(getActivity(), "Social media link should not empty");
                    } else if (!isEdit && isAlreadySocialAdded("" + (pos + 1))) {
                        String message = "Social Media ";
                        switch (pos) {
                            case 0:
                                message = message + "Facebook already added";
                                break;
                            case 1:
                                message = message + "LinkedIn already added";
                                break;
                            case 2:
                                message = message + "Twitter already added";
                                break;
                            case 3:
                                message = message + "Instagram already added";
                                break;
                            case 4:
                                message = message + "Google plus already added";
                                break;
                        }
                        Utilities.showToast(getActivity(), message);
                    } else {
                        SocialMedium socialMedium = new SocialMedium();
                        socialMedium.setName(edtSocialmediaLInk.getText().toString());
                        switch (pos) {
                            case 0:
                                socialMedium.setType(Constants.SOCIAL_FACEBOOK_ID);
                                break;
                            case 1:
                                socialMedium.setType(Constants.SOCIAL_LINKEDIN_ID);
                                break;
                            case 2:
                                socialMedium.setType(Constants.SOCIAL_TWITTER_ID);
                                break;
                            case 3:
                                socialMedium.setType(Constants.SOCIAL_INSTAGRAM_ID);
                                break;
                            case 4:
                                socialMedium.setType(Constants.SOCIAL_GOOGLE_PLUS);
                                break;
                        }

                        if (isEdit) {
                            socialMediumList.set(editPos, socialMedium);
                        } else {
                            socialMediumList.add(socialMedium);
                        }
                        tv_socialmedia_add_info.setVisibility(View.GONE);
                        tv_add_socialmedia.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_add_black_24dp, 0);
                        tv_add_socialmedia.setTag("1");
                        recyclerView_socialmedia.setVisibility(View.VISIBLE);
                        if (adpSocialMedium != null)
                            adpSocialMedium.notifyDataSetChanged();

                    }

                } else {
                    Utilities.showToast(getActivity(), "Please enter valid url");
                }
            }
        });

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    Utilities.hidekeyboard(getActivity(), view);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private String getSocialMediaText(int pos) {
        String Text_message = "";
        switch (pos) {
            case 0:
                // Facebook
                Text_message = Constants.FACEBOOK_URL;
                break;
            case 1:
                // Linkedin
                Text_message = Constants.LINKEDIN_URL;
                break;
            case 2:
                //  Twitter
                Text_message = Constants.TWITTER_URL;
                break;
            case 3:
                //  Instagram
                Text_message = Constants.INSTA_URL;
                break;
            case 4:
                //  Google Plus
                Text_message = Constants.GOOGLE_PLUS_URL;
                break;
        }
        return Text_message;
    }


    public void showSingleTextPopup(int type, final boolean isEdit) {

        final int field_type = type;
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        View view = LinearLayout.inflate(getActivity(), R.layout.dialog_addsocialmedia, null);
        dialog.setContentView(view);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;

        dialog.show();
        dialog.getWindow().setAttributes(lp);
        TextView tvAlert = view.findViewById(R.id.tvAlert);
        TextView tvDone = view.findViewById(R.id.tvDone);
        TextView tvCancel = view.findViewById(R.id.tvCancel);
        ccp = (CountryCodePicker) view.findViewById(R.id.ccp);
        LinearLayout lin_cpp = view.findViewById(R.id.lin_cpp);

        ccp.enablePhoneAutoFormatter(true);

        final EditText edtSocialmediaLInk = view.findViewById(R.id.edt_socialmedia_link);

        if (field_type == 2) {
            edtSocialmediaLInk.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    if (edtSocialmediaLInk.getText().toString().length() == 1 && edtSocialmediaLInk.getText().toString().equalsIgnoreCase("0")) {
                        edtSocialmediaLInk.setText("");
                    } else {
                        edtSocialmediaLInk.setFilters(new InputFilter[]{filter1, new InputFilter.LengthFilter(15)});
                    }
                }

                @Override
                public void afterTextChanged(Editable editable) {
                }
            });
        }

        if (isEdit && editString != null && Utilities.isEmpty(editString)) {
            try {
                edtSocialmediaLInk.setText(editString);
                if (field_type == 2) {
                    String CurrentString = editString;
                    String[] separated = CurrentString.split(" ");
                    ccp.setCountryForPhoneCode(Integer.parseInt(separated[0]));
                    edtSocialmediaLInk.setText(separated[1]);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (field_type == 1) {
            view.findViewById(R.id.divider_select_socialmedia).setVisibility(View.GONE);
            ccp.setVisibility(View.GONE);
            lin_cpp.setVisibility(View.GONE);
            message = "Email should not empty";
            tvAlert.setText("Add email");
            edtSocialmediaLInk.setHint("Enter email");
            edtSocialmediaLInk.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);

        } else if (field_type == 2) {
            view.findViewById(R.id.divider_select_socialmedia).setVisibility(View.GONE);
            ccp.setVisibility(View.VISIBLE);
            lin_cpp.setVisibility(View.VISIBLE);
            message = "Phone should not empty";
            tvAlert.setText("Add phone");
            edtSocialmediaLInk.setHint("Enter phone");
            edtSocialmediaLInk.setInputType(InputType.TYPE_CLASS_NUMBER);

        } else if (field_type == 3) {
            view.findViewById(R.id.divider_select_socialmedia).setVisibility(View.GONE);
            ccp.setVisibility(View.GONE);
            lin_cpp.setVisibility(View.GONE);
            message = "Web link should not empty";
            tvAlert.setText("Add web link");
            edtSocialmediaLInk.setHint("Enter web link");
            edtSocialmediaLInk.setInputType(InputType.TYPE_CLASS_TEXT);
        }

        view.findViewById(R.id.spinnerSocialMedia).setVisibility(View.GONE);

        tvDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //Utilities.hidekeyboard(getActivity(), getActivity().getWindow().getDecorView());
                try {
                    //((NewScanProfileActivity) getActivity()).HideKeyboard();
                    Utilities.hidekeyboard(getActivity(), view);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                if (edtSocialmediaLInk != null && !Utilities.isEmpty(edtSocialmediaLInk.getText().toString())) {
                    dialog.dismiss();
                    Utilities.showToast(getActivity(), message);
                } else {
                    if (field_type == 1) {
                        dialog.dismiss();
                        if (Utilities.isValidEmail(edtSocialmediaLInk.getText().toString())) {
                            Email email = new Email();
                            email.setName(edtSocialmediaLInk.getText().toString());
                            if (isEdit && editPos != -1)
                                listEmail.set(editPos, email);
                            else
                                listEmail.add(email);
                            tv_email_add_info.setVisibility(View.GONE);
                            if (listEmail != null && listEmail.size() == 5) {
                                tv_add_email.setTag("0");
                                tv_add_email.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                            } else {
                                tv_add_email.setTag("1");
                                tv_add_email.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_add_black_24dp, 0);
                            }
                            recyclerView_email.setVisibility(View.VISIBLE);
                            if (adpSingleTextEmail != null)
                                adpSingleTextEmail.notifyDataSetChanged();
                        } else
                            Utilities.showToast(getActivity(), "Please enter valid email address");
                    } else if (field_type == 2) {
                        if (!isValidPhoneNumber(edtSocialmediaLInk.getText().toString())) {
                            Toast.makeText(getActivity(), "Please enter valid phone number", Toast.LENGTH_SHORT).show();
                        } else {
                            dialog.dismiss();
                            Phone email = new Phone();
                            email.setName("+" + ccp.getSelectedCountryCode().toString() + " " + edtSocialmediaLInk.getText().toString());
                            if (isEdit && editPos != -1)
                                listPhones.set(editPos, email);
                            else
                                listPhones.add(email);
                            tv_phone_add_info.setVisibility(View.GONE);
                            if (listPhones != null && listPhones.size() == 5) {
                                tv_add_phone.setTag("0");
                                tv_add_phone.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                            } else {
                                tv_add_phone.setTag("1");
                                tv_add_phone.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_add_black_24dp, 0);
                            }
                            recyclerView_phone.setVisibility(View.VISIBLE);
                            if (adpSingleTextPhone != null)
                                adpSingleTextPhone.notifyDataSetChanged();
                        }
                    } else if (field_type == 3) {
                        if (!edtSocialmediaLInk.getText().toString().isEmpty() && isValidUrl(edtSocialmediaLInk.getText().toString())) {
                            dialog.dismiss();
                            Weblink email = new Weblink();
                            email.setName(edtSocialmediaLInk.getText().toString());
                            if (isEdit && editPos != -1)
                                listWeblink.set(editPos, email);
                            else
                                listWeblink.add(email);
                            tv_weblink_add_info.setVisibility(View.GONE);
                            if (listWeblink != null && listWeblink.size() == 5) {
                                tv_add_weblink.setTag("0");
                                tv_add_weblink.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                            } else {
                                tv_add_weblink.setTag("1");
                                tv_add_weblink.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_add_black_24dp, 0);
                            }
                            recyclerView_weblink.setVisibility(View.VISIBLE);
                            if (adpSingleTextWeblink != null)
                                adpSingleTextWeblink.notifyDataSetChanged();
                        } else {
                            Utilities.showToast(getActivity(), "Please enter valid url");
                        }
                    }
                }
            }
        });

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    Utilities.hidekeyboard(getActivity(), view);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    InputFilter filter1 = new InputFilter() {
        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
            for (int i = start; i < end; ++i) {
                if (!Pattern.compile("[0123456789]*").matcher(String.valueOf(source.charAt(i))).matches()) {
                    return "";
                }
            }
            return null;
        }
    };

    private boolean isValidPhoneNumber(CharSequence phoneNumber) {
        String num = phoneNumber.toString();
        if (num.startsWith("1") || num.startsWith("2") || num.startsWith("3") || num.startsWith("4") || num.startsWith("5") || num.startsWith("6") || num.startsWith("7") || num.startsWith("8") || num.startsWith("9")) {
            return true;
        } else {
            return false;
        }
    }

    private boolean isValidUrl(String url) {
        Pattern p = Patterns.WEB_URL;
        Matcher m = p.matcher(url.toLowerCase());
        if (m.matches())
            return true;
        else
            return false;
    }

    public void showeditCompanyinfo() {

        Utilities.hidekeyboard(getActivity(), getActivity().getWindow().getDecorView());
        c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        View view = LinearLayout.inflate(getActivity(), R.layout.dialog_edit_companyinfo, null);
        dialog.setContentView(view);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;

        dialog.show();
        dialog.getWindow().setAttributes(lp);

        TextView tvAlert = view.findViewById(R.id.tvAlert);
        TextView tvDone = view.findViewById(R.id.tvDone);
        TextView tvCancel = view.findViewById(R.id.tvCancel);

        edt_doestablishment = view.findViewById(R.id.edt_doestablishment);
        edt_teamsize = view.findViewById(R.id.edt_teamsize);
        edt_address = view.findViewById(R.id.edt_address);
        edt_dob = view.findViewById(R.id.edt_dob);
        //edt_cfo = view.findViewById(R.id.edt_cfo);
        edt_ceo = view.findViewById(R.id.edt_ceo);

        if (myProfiles.getType() != null && (myProfiles.getType().equals("" + Constants.TYPE_PROFILE_COMPANY) || myProfiles.getType().equals("" + Constants.TYPE_PROFILE_ENTERPRISE))) {

            String doe = tv_establishmentdate.getText().toString();
            if (doe != null && !doe.isEmpty()) {
                /*String estd = doe.replace(":", "");*/
                edt_doestablishment.setText(doe);
            }

            String teamsize = tv_numberofemplyees.getText().toString();
            if (teamsize != null && !teamsize.isEmpty()) {
                /*String team = teamsize.replace(":", "");*/
                edt_teamsize.setText(getTrimStraing(teamsize));
            }

            String ceo = tv_currentCto.getText().toString();
            if (ceo != null && !ceo.isEmpty()) {
                /*String strceo = ceo.replace(":", "");*/
                edt_ceo.setText(getTrimStraing(ceo));
            }

            /*String cfo = tv_currentCfo.getText().toString();
            if (cfo != null && !cfo.isEmpty()) {
                String strcfo = cfo.replace(":", "");
                edt_cfo.setText(getTrimStraing(strcfo));
            }*/

            edt_doestablishment.setVisibility(View.VISIBLE);
            edt_teamsize.setVisibility(View.VISIBLE);
            edt_address.setVisibility(View.VISIBLE);
            edt_dob.setVisibility(View.GONE);
            //edt_cfo.setVisibility(View.VISIBLE);
            edt_ceo.setVisibility(View.VISIBLE);


        } else if (myProfiles.getType() != null && myProfiles.getType().equals("" + Constants.TYPE_PROFILE_CELEBRITY)) {

            String dob = tv_date_of_birth.getText().toString();
            if (dob != null && !dob.isEmpty())
                /*edt_dob.setText(dob.replace(":", ""));*/
                edt_dob.setText(dob);

            edt_doestablishment.setVisibility(View.GONE);
            edt_teamsize.setVisibility(View.GONE);
            edt_address.setVisibility(View.VISIBLE);
            edt_dob.setVisibility(View.VISIBLE);
            //edt_cfo.setVisibility(View.GONE);
            edt_ceo.setVisibility(View.GONE);
        }

        String address = tv_address.getText().toString();
        if (address != null && !address.isEmpty()) {
            /*edt_address.setText(address.replace(":", ""));*/
            edt_address.setText(address);
            if (tv_address.getTag() != null) {
                edt_address.setTag(tv_address.getTag().toString());
            }
        }

        if (myProfiles.getType() != null && (myProfiles.getType().equals("" + Constants.TYPE_PROFILE_COMPANY) || myProfiles.getType().equals("" + Constants.TYPE_PROFILE_ENTERPRISE))) {
            tvAlert.setText("Company Information");
        } else {
            tvAlert.setText("Basic Information");
        }

        edt_doestablishment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(),
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                int month = monthOfYear + 1;
                                String fm = "" + month;
                                String fd = "" + dayOfMonth;
                                if (month < 10) {
                                    fm = "0" + month;
                                }
                                if (dayOfMonth < 10) {
                                    fd = "0" + dayOfMonth;
                                }
                                edt_doestablishment.setText(fd + "/" + fm + "/" + year);
                                tv_establishmentdate.setText(myProfiles.getEstablish_date());
                                llDOEsta.setVisibility(View.VISIBLE);

                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.getDatePicker().setMaxDate(c.getTimeInMillis());
                datePickerDialog.show();
            }
        });

        edt_dob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(),
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                int month = monthOfYear + 1;
                                String fm = "" + month;
                                String fd = "" + dayOfMonth;
                                if (month < 10) {
                                    fm = "0" + month;
                                }
                                if (dayOfMonth < 10) {
                                    fd = "0" + dayOfMonth;
                                }
                                edt_dob.setText(year + "-" + fm + "-" + fd);
                                tv_date_of_birth.setText(myProfiles.getDob());

                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.getDatePicker().setMaxDate(c.getTimeInMillis());
                datePickerDialog.show();
            }
        });

        edt_address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
                    startActivityForResult(builder.build(getActivity()), PLACE_PICKER_REQUEST);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        tvDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    Utilities.hidekeyboard(getActivity(), view);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                dialog.dismiss();
                try {
                    if (!edt_ceo.getText().toString().isEmpty()) {
                        tv_currentCto.setText(getTrimStraing(edt_ceo.getText().toString()));
                        llDOCto.setVisibility(View.VISIBLE);
                    } else {
                        tv_currentCto.setText("");
                        llDOCto.setVisibility(View.GONE);
                    }
                    /*if (!edt_cfo.getText().toString().isEmpty()) {
                        tv_currentCfo.setText(": " + getTrimStraing(edt_cfo.getText().toString()));
                        llDOCfo.setVisibility(View.VISIBLE);
                    } else {
                        tv_currentCfo.setText("");
                        llDOCfo.setVisibility(View.GONE);
                    }*/
                    if (!edt_dob.getText().toString().isEmpty()) {
                        tv_date_of_birth.setText(getTrimStraing(edt_dob.getText().toString()));
                        llDODob.setVisibility(View.VISIBLE);
                    } else {
                        tv_date_of_birth.setText("");
                        llDODob.setVisibility(View.GONE);
                    }
                    if (!edt_doestablishment.getText().toString().isEmpty()) {
                        tv_establishmentdate.setText(getTrimStraing(edt_doestablishment.getText().toString()));
                        llDOEsta.setVisibility(View.VISIBLE);
                    } else {
                        tv_establishmentdate.setText("");
                        llDOEsta.setVisibility(View.GONE);
                    }
                    if (!edt_teamsize.getText().toString().isEmpty()) {
                        tv_numberofemplyees.setText(getTrimStraing(edt_teamsize.getText().toString()));
                        llNofEmployees.setVisibility(View.VISIBLE);
                    } else {
                        tv_numberofemplyees.setText("");
                        llNofEmployees.setVisibility(View.GONE);
                    }

                    tv_address.setText(getTrimStraing(edt_address.getText().toString()));
                    if (edt_address.getTag() != null) {
                        tv_address.setTag(edt_address.getTag().toString());
                    }

                    /*if (!tv_currentCfo.getText().toString().isEmpty())
                        llDOCfo.setVisibility(View.VISIBLE);
                    else
                        llDOCfo.setVisibility(View.GONE);*/

                    if (!tv_currentCto.getText().toString().isEmpty())
                        llDOCto.setVisibility(View.VISIBLE);
                    else
                        llDOCto.setVisibility(View.GONE);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    Utilities.hidekeyboard(getActivity(), view);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private String getTrimStraing(String s) {
        String s1 = "";
        if (s != null && !s.equalsIgnoreCase("")) {
            s1 = s.replace(" ", "");
            return s1;
        } else {
            return s1;
        }
    }

    @Override
    public void getEditFounder(int pos, Founder founder, int action) {
        if (action == OnSingleEdit.DELETE) {
            foundersList.remove(pos);
            if (adpfounders != null)
                adpfounders.notifyDataSetChanged();
            if (foundersList != null && foundersList.size() == 0) {
                tv_founders_add_info.setVisibility(View.VISIBLE);
                tv_add_founders.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                tv_add_founders.setTag("0");
            }
        } else if (action == OnSingleEdit.EDIT) {
            addFounders(pos, founder);
        }
    }

    @Override
    public void getEditMedia(int pos, SocialMedium socialMedium, int action) {
        if (action == OnSingleEdit.DELETE) {
            socialMediumList.remove(pos);
            if (adpSocialMedium != null)
                adpSocialMedium.notifyDataSetChanged();
            if (socialMediumList != null && socialMediumList.size() == 0) {
                tv_socialmedia_add_info.setVisibility(View.VISIBLE);
                tv_add_socialmedia.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                tv_add_socialmedia.setTag("0");
            }
        } else if (action == OnSingleEdit.EDIT) {
            editsocialMedium = socialMedium;
            editPos = pos;
            showpopupSociamMedia(true);
        }
    }

    @Override
    public void getEditSingle(int pos, String data, int action, int type) {
        if (action == OnSingleEdit.DELETE) {
            if (type == OnSingleEdit.TYPE_EMAIL) {
                listEmail.remove(pos);
                if (adpSingleTextEmail != null)
                    adpSingleTextEmail.notifyDataSetChanged();
                if (listEmail != null && listEmail.size() == 0) {
                    tv_email_add_info.setVisibility(View.VISIBLE);
                    tv_add_email.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                    tv_add_email.setTag("0");
                }

            } else if (type == OnSingleEdit.TYPE_PHONE) {
                listPhones.remove(pos);
                if (adpSingleTextPhone != null)
                    adpSingleTextPhone.notifyDataSetChanged();
                if (listPhones != null && listPhones.size() == 0) {
                    tv_phone_add_info.setVisibility(View.VISIBLE);
                    tv_add_phone.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                    tv_add_phone.setTag("0");
                }

            } else if (type == OnSingleEdit.TYPE_WEBLINK) {
                listWeblink.remove(pos);
                if (adpSingleTextWeblink != null)
                    adpSingleTextWeblink.notifyDataSetChanged();
                if (listWeblink != null && listWeblink.size() == 0) {
                    tv_weblink_add_info.setVisibility(View.VISIBLE);
                    tv_add_weblink.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                    tv_add_weblink.setTag("0");
                }
            }
        } else if (action == OnSingleEdit.EDIT) {
            editString = data;
            editPos = pos;
            showSingleTextPopup(type, true);
        }
    }

    public static void submitData(Context context, final String base64, final String cname, final String cabout) {
        try {
            CreateProfileData createProfileData = new CreateProfileData();
            if (myProfiles != null && myProfiles.getId() != null) {
                createProfileData.setUserId(modelLoggedUser.getData().getId());
                createProfileData.setType(Integer.parseInt(myProfiles.getType()));

                createProfileData.setId(Integer.parseInt(myProfiles.getId()));

                createProfileData.setName(cname + "");

                String str_about = cabout + "";
                String str_address = tv_address.getText().toString();
                String str_teamsize = tv_numberofemplyees.getText().toString();
                String str_establishdate = tv_establishmentdate.getText().toString();
                String str_dob = tv_date_of_birth.getText().toString();
                String str_ceo = tv_currentCto.getText().toString();
                //String str_cfo = tv_currentCfo.getText().toString();

                if (str_about != null && Utilities.isEmpty(str_about)) {
                    createProfileData.setAbout(str_about.replace(":", ""));
                } else
                    createProfileData.setAbout("");

                if (str_dob != null && Utilities.isEmpty(str_dob)) {
                    createProfileData.setDob(str_dob.replace(":", "").replace(" ", ""));
                } else
                    createProfileData.setDob("");

                if (str_ceo != null && Utilities.isEmpty(str_ceo)) {
                    createProfileData.setCurrent_ceo(str_ceo.replace(":", "").replace(" ", ""));
                } else
                    createProfileData.setCurrent_ceo("");

               /* if (str_cfo != null && Utilities.isEmpty(str_cfo)) {
                    createProfileData.setCurrent_cfo(str_cfo.replace(":", "").replace(" ", ""));
                } else
                    createProfileData.setCurrent_cfo("");*/

                if (base64 != null && !base64.isEmpty())
                    createProfileData.setLogo(base64);

                if (str_address != null && Utilities.isEmpty(str_address)) {
                    createProfileData.setAddress(str_address.replace(":", "").replace(" ", ""));

                    if (tv_address.getTag() != null && Utilities.isEmpty(tv_address.getTag().toString())) {
                        createProfileData.setLatitude(tv_address.getTag().toString().split(",")[0]);
                        createProfileData.setLongitude(tv_address.getTag().toString().split(",")[1]);
                    }
                }

                createProfileData.setUniqueCode(myProfiles.getUnique_code());

                if (str_establishdate != null && Utilities.isEmpty(str_establishdate)) {
                    String estDate = str_establishdate.replace(":", "").replace(" ", "");
                    try {
                        String format_date[] = estDate.split("/");
                        estDate = format_date[2] + "-" + format_date[1] + "-" + format_date[0];
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    createProfileData.setEstablishDate(estDate);

                } else
                    createProfileData.setEstablishDate("");

                if (str_teamsize != null && Utilities.isEmpty(str_teamsize)) {
                    createProfileData.setCompanyTeamSize(Integer.parseInt(str_teamsize.replace(":", "").replace(" ", "")));
                } else {
                    createProfileData.setCompanyTeamSize(0);
                }

                if (listEmail != null && listEmail.size() > 0) {
                    createProfileData.setEmail(listEmail);
                } else {
                    createProfileData.setEmail(new ArrayList<Email>());
                }

                if (listPhones != null && listPhones.size() > 0) {
                    createProfileData.setPhone(listPhones);
                } else {
                    createProfileData.setPhone(new ArrayList<Phone>());
                }

                if (other_address != null && other_address.size() > 0) {
                    createProfileData.setOtherAddress(other_address);
                } else {
                    createProfileData.setOtherAddress(new ArrayList<OtherAddresModel>());
                }

                if (listWeblink != null && listWeblink.size() > 0) {
                    createProfileData.setWeblink(listWeblink);
                } else {
                    createProfileData.setWeblink(new ArrayList<Weblink>());
                }

                if (socialMediumList != null && socialMediumList.size() > 0) {
                    createProfileData.setSocialMedia(socialMediumList);
                } else {
                    createProfileData.setSocialMedia(new ArrayList<SocialMedium>());
                }

                if (foundersList != null && foundersList.size() > 0) {
                    createProfileData.setFounders(foundersList);
                } else {
                    createProfileData.setFounders(new ArrayList<Founder>());
                }

                createProfileData.setTag(Integer.parseInt(myProfiles.getTag()));
                createProfileData.setSubTag(Integer.parseInt(myProfiles.getSub_tag()));

                if (listEmail != null && listEmail.size() > 0) {

                    Log.e("Data", "" + new Gson().toJson(createProfileData));
                    submitData(context, new JSONObject(new Gson().toJson(createProfileData)));

                } else Utilities.showToast(context, "Email should not empty");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_SELECT_ADDFOUNDERS) {
            int index = data.getExtras().getInt("index");
            if (index != -1) {
                // EditData
                Founder founder = data.getParcelableExtra("data");
                foundersList.set(index, founder);
                tv_founders_add_info.setVisibility(View.GONE);
                tv_add_founders.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_add_black_24dp, 0);
                tv_add_founders.setTag("1");
                recyclerView_founders.setVisibility(View.VISIBLE);
                if (adpfounders != null)
                    adpfounders.notifyDataSetChanged();
            } else {
                // Add CreateProfileData
                Founder founder = data.getParcelableExtra("data");
                foundersList.add(founder);
                tv_founders_add_info.setVisibility(View.GONE);
                tv_add_founders.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_add_black_24dp, 0);
                tv_add_founders.setTag("1");
                recyclerView_founders.setVisibility(View.VISIBLE);
                if (adpfounders != null)
                    adpfounders.notifyDataSetChanged();
            }
        } else if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == Activity.RESULT_OK) {
                Place place = PlacePicker.getPlace(getActivity(), data);
                String toastMsg = "";
                if (place.getAddress() != null && !place.getAddress().toString().equalsIgnoreCase("") && !place.getAddress().toString().equalsIgnoreCase("null"))
                    toastMsg = String.format("%s", place.getAddress());
                else
                    toastMsg = "Unknown";
                edt_address.setText(toastMsg);
                double latitude = place.getLatLng().latitude;
                double longtitude = place.getLatLng().longitude;
                edt_address.setTag("" + latitude + "," + longtitude);
            }
        } else if (requestCode == PLACE_PICKER_REQUEST_ADDRESS) {
            if (resultCode == Activity.RESULT_OK) {
                if (other_add_pos == -1) {
                    OtherAddresModel otherAddresModel = new OtherAddresModel();
                    Place place = PlacePicker.getPlace(getActivity(), data);
                    String toastMsg = "";
                    if (place.getAddress() != null && !place.getAddress().toString().equalsIgnoreCase("") && !place.getAddress().toString().equalsIgnoreCase("null"))
                        toastMsg = String.format("%s", place.getAddress());
                    else
                        toastMsg = "Unknown";
                    double latitude = place.getLatLng().latitude;
                    double longtitude = place.getLatLng().longitude;
                    otherAddresModel.setAddress(toastMsg);
                    if (place.getAddress() != null) {
                        otherAddresModel.setSubadd(place.getAddress().toString());
                    }
                    otherAddresModel.setLati(latitude);
                    otherAddresModel.setLongi(longtitude);
                    other_address.add(otherAddresModel);
                    adpOtherAddress.notifyDataSetChanged();
                    recyclerView_oa.setVisibility(View.VISIBLE);
                    tv_oa_add_info.setVisibility(View.GONE);
                    tv_add_other_address.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_add_black_24dp, 0);
                } else {
                    OtherAddresModel otherAddresModel = new OtherAddresModel();
                    Place place = PlacePicker.getPlace(getActivity(), data);
                    String toastMsg = "";
                    if (place.getAddress() != null && !place.getAddress().toString().equalsIgnoreCase("") && !place.getAddress().toString().equalsIgnoreCase("null"))
                        toastMsg = String.format("%s", place.getAddress());
                    else
                        toastMsg = "Unknown";
                    double latitude = place.getLatLng().latitude;
                    double longtitude = place.getLatLng().longitude;
                    otherAddresModel.setAddress(toastMsg);
                    if (place.getAddress() != null) {
                        otherAddresModel.setSubadd(place.getAddress().toString());
                    }
                    otherAddresModel.setLati(latitude);
                    otherAddresModel.setLongi(longtitude);
                    other_address.set(other_add_pos, otherAddresModel);
                    adpOtherAddress.notifyDataSetChanged();
                    recyclerView_oa.setVisibility(View.VISIBLE);
                    tv_oa_add_info.setVisibility(View.GONE);
                    tv_add_other_address.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_add_black_24dp, 0);
                }
            }
        } else if (requestCode == UCrop.REQUEST_CROP) {

        }
    }

    public static void submitData(final Context context, JSONObject jsonObject) {
        try {
            Log.e("Request Data", "" + jsonObject);
            new VolleyApiRequest(context, true, VolleyApiRequest.REQUEST_BASEURL + VolleyApiRequest.API_CREATE_PROFILE,
                    new VolleyCallBack() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.e("Request Data", "" + response);
                            try {
                                ModelCreateProfile modelCreateProfile = new Gson().fromJson(response.toString(), ModelCreateProfile.class);
                                if (modelCreateProfile.getStatus() == 200) {

                                    if (modelCreateProfile.getData() != null) {

                                        MyProfiles myProfiles = new MyProfiles();

                                        CreateProfileData createProfileData = modelCreateProfile.getData();

                                        myProfiles.setId("" + createProfileData.getId());
                                        myProfiles.setUser_id("" + createProfileData.getUserId());
                                        myProfiles.setType("" + createProfileData.getType());
                                        myProfiles.setName(createProfileData.getName());
                                        myProfiles.setAddress(createProfileData.getAddress());
                                        myProfiles.setLatitude(createProfileData.getLatitude());
                                        myProfiles.setLongitude(createProfileData.getLongitude());
                                        myProfiles.setTag("" + createProfileData.getTag());
                                        myProfiles.setSub_tag("" + createProfileData.getSubTag());
                                        myProfiles.setLogo(createProfileData.getLogo());
                                        myProfiles.setAbout(createProfileData.getAbout());
                                        myProfiles.setCompany_team_size("" + createProfileData.getCompanyTeamSize());
                                        myProfiles.setEstablish_date(createProfileData.getEstablishDate());
                                        myProfiles.setUnique_code(createProfileData.getUniqueCode());
                                        myProfiles.setCurrent_ceo(createProfileData.getCurrent_ceo());
                                        myProfiles.setCurrent_cfo(createProfileData.getCurrent_cfo());
                                        myProfiles.setDob(createProfileData.getDob());

                                        String social_media = new Gson().toJson(createProfileData.getSocialMedia());
                                        myProfiles.setSocial_media(social_media);

                                        String social_phone = new Gson().toJson(createProfileData.getPhone());
                                        myProfiles.setPhone(social_phone);

                                        String social_email = new Gson().toJson(createProfileData.getEmail());
                                        myProfiles.setEmail(social_email);

                                        String social_weblink = new Gson().toJson(createProfileData.getWeblink());
                                        myProfiles.setWeblink(social_weblink);

                                        String social_founder = new Gson().toJson(createProfileData.getFounders());
                                        myProfiles.setFounders(social_founder);

                                        RealmController.with((NewScanProfileActivity) context).updateMyProfiles(myProfiles);

                                        ((NewScanProfileActivity) context).finish();
                                    }
                                } else
                                    Utilities.showToast(context, modelCreateProfile.getMsg());
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onErrorResponse(VolleyError error) {
                            VolleyApiRequest.showVolleyError(context, error);
                        }
                    }).enqueRequest(jsonObject, Request.Method.POST);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //*  delete profile  *//
    private void DeleteProfile() {
        JSONObject obj = new JSONObject();
        try {
            if (user_id != null && !user_id.equalsIgnoreCase(""))
                obj.put("profile_id", user_id);
            else
                obj.put("profile_id", qkPreferences.getProfileID());

        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.e("request", obj + "");
        new VolleyApiRequest(getActivity(), true, VolleyApiRequest.REQUEST_BASEURL + "api/profile/profile_delete",
                new VolleyCallBack() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("Request Data", "" + response);
                        ModelCreateProfile modelCreateProfile = new Gson().fromJson(response.toString(), ModelCreateProfile.class);
                        if (modelCreateProfile.getStatus() == 200) {
                            Utilities.showToast(getActivity(), modelCreateProfile.getMsg());
                            getActivity().finish();
                        } else {
                            Utilities.showToast(getActivity(), modelCreateProfile.getMsg());
                        }
                    }

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyApiRequest.showVolleyError(getActivity(), error);
                    }
                }).
                enqueRequest(obj, Request.Method.POST);
    }


    public void fnScanUser(String unique_code, boolean isLoadershow) {

        try {
            if (Utilities.isNetworkConnected(getActivity())) {
                JSONObject obj = new JSONObject();
                try {
                    obj.put("unique_code", unique_code);
                    obj.put("user_id", login_user_id);
                    obj.put("timezone", "" + TimeZone.getDefault().getID());
                    obj.put("latitude", qkPreferences.getLati());
                    obj.put("longitude", qkPreferences.getLongi());
                    obj.put("device_type", "Android");
                    obj.put("company_id", compony_id + "");
                    String model = Build.MODEL;
                    int version = Build.VERSION.SDK_INT;
                    if (model.isEmpty())
                        obj.put("model", "z-404");
                    else
                        obj.put("model", model + "");
                    obj.put("version", version + "");
                } catch (Exception e) {
                    e.printStackTrace();
                }
                new VolleyApiRequest(getActivity(), isLoadershow, VolleyApiRequest.REQUEST_BASEURL + "api/scan_user_store",
                        new VolleyCallBack() {
                            @Override
                            public void onResponse(JSONObject response) {
                                parseResponse(response);
                            }

                            @Override
                            public void onErrorResponse(VolleyError error) {
                            }
                        }).enqueRequest(obj, Request.Method.POST);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getOtherUserData(String profile_id, boolean isLoadershow) {
        try {
            if (Utilities.isNetworkConnected(getActivity())) {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("profile_id", profile_id);
                jsonObject.put("user_id", login_user_id);
                new VolleyApiRequest(getActivity(), isLoadershow, VolleyApiRequest.REQUEST_BASEURL + "api/profile/profile_view",
                        new VolleyCallBack() {
                            @Override
                            public void onResponse(JSONObject response) {
                                parseResponse(response);
                            }

                            @Override
                            public void onErrorResponse(VolleyError error) {
                                VolleyApiRequest.showVolleyError(getActivity(), error);
                            }
                        }).enqueRequest(jsonObject, Request.Method.POST);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void parseResponse(JSONObject response) {
        try {
            ModelCreateProfile modelCreateProfile = new Gson().fromJson(response.toString(), ModelCreateProfile.class);

            if (modelCreateProfile.getStatus() == 200) {

                if (modelCreateProfile.getData() != null) {

                    CreateProfileData createProfileData = modelCreateProfile.getData();

                    qkPreferences.storeProfileID(createProfileData.getId() + "");

                    MyProfiles myProfiles = new MyProfiles();

                    myProfiles.setId("" + createProfileData.getId());
                    myProfiles.setUser_id("" + createProfileData.getUserId());
                    myProfiles.setType("" + createProfileData.getType());
                    myProfiles.setName(createProfileData.getName());
                    myProfiles.setAddress(createProfileData.getAddress());
                    myProfiles.setLatitude(createProfileData.getLatitude());
                    myProfiles.setLongitude(createProfileData.getLongitude());
                    myProfiles.setTag("" + createProfileData.getTag());
                    myProfiles.setSub_tag("" + createProfileData.getSubTag());
                    myProfiles.setLogo(createProfileData.getLogo());
                    myProfiles.setAbout(createProfileData.getAbout());
                    myProfiles.setStatus(createProfileData.getStatus());
                    myProfiles.setCompany_team_size("" + createProfileData.getCompanyTeamSize());
                    myProfiles.setEstablish_date(createProfileData.getEstablishDate());
                    myProfiles.setUnique_code(createProfileData.getUniqueCode());
                    myProfiles.setCurrent_ceo(createProfileData.getCurrent_ceo());
                    myProfiles.setCurrent_cfo(createProfileData.getCurrent_cfo());
                    myProfiles.setDob(createProfileData.getDob());
                    myProfiles.setRole(createProfileData.getRole());
                    myProfiles.setRole_name(createProfileData.getRole_name());
                    myProfiles.setIs_transfer(createProfileData.getIs_transfer());
                    myProfiles.setTransfer_status(createProfileData.getTransfer_status());

                    String social_media = new Gson().toJson(createProfileData.getSocialMedia());
                    myProfiles.setSocial_media(social_media);

                    String social_phone = new Gson().toJson(createProfileData.getPhone());
                    myProfiles.setPhone(social_phone);

                    String social_email = new Gson().toJson(createProfileData.getEmail());
                    myProfiles.setEmail(social_email);

                    String otheraddress = new Gson().toJson(createProfileData.getOtherAddress());
                    myProfiles.setOtheraddress(otheraddress);

                    String social_weblink = new Gson().toJson(createProfileData.getWeblink());
                    myProfiles.setWeblink(social_weblink);

                    String social_founder = new Gson().toJson(createProfileData.getFounders());
                    myProfiles.setFounders(social_founder);

                    RealmController.with(getActivity()).updateMyProfiles(myProfiles);

                    try {
                        Fragment fragment = new ScanFragSetting();
                        ScanFragSetting frg = (ScanFragSetting) fragment;
                        frg.getData(myProfiles);
                        qkPreferences.storeIsTransfer(myProfiles.getIs_transfer() + "");
                        qkPreferences.storeTransfer_status(myProfiles.getTransfer_status() + "");
                        qkPreferences.storeStatus(myProfiles.getStatus() + "");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    showData(false);
                }
            } else {

                try {
                   /* if (modelCreateProfile.getStatus() == 400 && modelCreateProfile.getMsg().equalsIgnoreCase("Profile View not available")) {
                        Utilities.showToast(getActivity(), modelCreateProfile.getMsg());
                        *//*RealmController.with(getActivity()).deleteProfile(Unique_Code + "");
                        getActivity().finish();*//*
                    }else {*/
                    Utilities.showToast(getActivity(), modelCreateProfile.getMsg());
                    //}
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}