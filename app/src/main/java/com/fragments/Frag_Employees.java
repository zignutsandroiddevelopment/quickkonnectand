package com.fragments;

import android.animation.Animator;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.LinearSnapHelper;
import android.support.v7.widget.PagerSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SnapHelper;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.adapter.EmployeeAdapter;
import com.adapter.NewUpgradePlaneAdapter;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.customviews.LinePagerIndicatorDecoration;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.interfaces.EmployeeClicked;
import com.interfaces.Upgradepackagelistener;
import com.models.BasicDetail;
import com.models.InviteQKPojo;
import com.models.UpgradePlan;
import com.models.profilesemployee.Datum;
import com.models.profilesemployee.ProfileEmpPojo;
import com.quickkonnect.EmployeeSearch;
import com.quickkonnect.EmployeesProfile;
import com.quickkonnect.ManageRoles;
import com.quickkonnect.PaymentDetails;
import com.quickkonnect.PojoClasses;
import com.quickkonnect.R;
import com.realmtable.Employees;
import com.realmtable.RealmController;
import com.retrofitservice.ApiUtils;
import com.retrofitservice.SOService;
import com.stripe.android.TokenCallback;
import com.stripe.android.model.Card;
import com.stripe.android.model.Token;
import com.stripe.android.view.CardMultilineWidget;
import com.utilities.QKPreferences;
import com.utilities.StripeIntegration;
import com.utilities.Utilities;
import com.volleyapirequest.VolleyApiRequest;
import com.volleyapirequest.VolleyCallBack;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.realm.RealmResults;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ZTLAB03 on 09-04-2018.
 */

public class Frag_Employees extends Fragment {

    private SOService mService;
    private View rootView;
    private String login_user_id, profile_id, role;
    private QKPreferences qkPreferences;
    private EmployeeAdapter adapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private Context context;
    static ImageView filter;
    private ArrayList<PojoClasses.EmployeesList> mlist;
    private String UserBasicDetail = "";

    boolean cancelPressed = false;

    private RecyclerView recyclerView;
    private TextView no_recored_found;
    private EditText edtContactSearch_employees;
    int HEADER_RESULT = 100;
    boolean isEdit = true;

    // Open Dialog
    EditText email, search_email, edtMsg;
    TextView title, subtitle, no_user, cancel, search, confirm, invite, username, tvCountMsglength;
    LinearLayout linearInvitePeople;
    ImageView profileImage, search_icon, header;
    LinearLayout linear_profile, lin_search, lin_emp_dialog_main;
    String id = "", invite_emailid = "";

    Dialog dialogup;
    boolean isSubscribebefore = false;
    private int SUBSCRIBE_PACKAGE = 1010;

    public static Fragment getInstance(int position, Bundle bundle) {
        bundle.putInt("pos", position);
        Frag_Employees frag_employees = new Frag_Employees();
        frag_employees.setArguments(bundle);
        return frag_employees;
    }

    public static Frag_Employees newInstance(int position, Bundle bundle) {
        bundle.putInt("pos", position);
        Frag_Employees frag_employees = new Frag_Employees();
        frag_employees.setArguments(bundle);
        return frag_employees;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.frag_employees, container, false);

        context = getActivity();

        mlist = new ArrayList<>();

        final Bundle extras = getArguments();

        qkPreferences = new QKPreferences(getActivity());
        mService = ApiUtils.getSOService();

        login_user_id = "" + qkPreferences.getValuesInt(QKPreferences.USER_ID);

        try {
            profile_id = extras.getString("user_id");
            role = extras.getString("ROLE");
            if (role != null) {
                if (role.equalsIgnoreCase("2") || role.equalsIgnoreCase("1")) {
                    isEdit = false;
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        getPackageDetails(false, 0);

        recyclerView = rootView.findViewById(R.id.recyclerView_employees);
        no_recored_found = rootView.findViewById(R.id.tv_no_record_found_employees);
        edtContactSearch_employees = rootView.findViewById(R.id.edtContactSearch_employees);
        filter = rootView.findViewById(R.id.img_filter_emp);

        mLayoutManager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(mLayoutManager);

        edtContactSearch_employees.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                edtContactSearch_employees.setCursorVisible(true);
                if (adapter != null) {
                    filter(editable.toString());
                }
            }
        });

        filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setFilter();
            }
        });

        if (isEdit)
            rootView.findViewById(R.id.lin_header_emp).setVisibility(View.VISIBLE);
        else
            rootView.findViewById(R.id.lin_header_emp).setVisibility(View.GONE);

        showEmployees(true);

        //*************
        // upgradedialog = 1 = show trial
        // upgradedialog = 0 = hide trial
        //*************

        rootView.findViewById(R.id.lin_header_emp).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Utilities.isNetworkConnected(getActivity())) {
                    String max_employee = qkPreferences.getValues(QKPreferences.MAX_NEW_EMPLOYEE);
                    String toemp = qkPreferences.getValues(QKPreferences.TOTAL_EMPLOYEE);
                    int total_emp = 0;
                    try {
                        total_emp = Integer.parseInt(toemp);
                    } catch (NumberFormatException e) {
                        e.printStackTrace();
                    }
                    if (max_employee != null && !max_employee.isEmpty() && !max_employee.equals("0")) {
                        try {
                            if (max_employee.equalsIgnoreCase("unlimited")) {
                                OpenEmployeeDialog();
                            } else {
                                int maxEmp = Integer.parseInt(max_employee);
                                if (total_emp < maxEmp) {
                                    OpenEmployeeDialog();
                                } else {
                                    isSubscribebefore = true;
                                    UpgradePlaneDialog(0);
                                }
                            }
                        } catch (Exception e) {
                            isSubscribebefore = false;
                            UpgradePlaneDialog(1);
                        }
                    } else if (max_employee != null && max_employee.equalsIgnoreCase("0")) {
                        isSubscribebefore = false;
                        UpgradePlaneDialog(1);
                    } else {
                        isSubscribebefore = false;
                        UpgradePlaneDialog(1);
                    }
                } else {
                    Toast.makeText(context, "No internet connection, please try after some time.", Toast.LENGTH_SHORT).show();
                }
            }
        });

        /*
        Old Method for all employeess
        ***
        rootView.findViewById(R.id.lin_header_emp).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String max_employee = qkPreferences.getValues(QKPreferences.MAX_EMPLOYEE);
                if (max_employee != null && !max_employee.isEmpty() && !max_employee.equals("0")) {
                    try {
                        if (max_employee.equalsIgnoreCase("unlimited")) {
                            OpenEmployeeDialog();
                        } else {
                            int maxEmp = Integer.parseInt(max_employee);
                            if (mlist != null && mlist.size() < maxEmp) {
                                OpenEmployeeDialog();
                            } else {
                                isSubscribebefore = true;
                                UpgradePlaneDialog(0);
                            }
                        }
                    } catch (Exception e) {
                        isSubscribebefore = false;
                        UpgradePlaneDialog(1);
                    }
                } else if (max_employee != null && max_employee.equalsIgnoreCase("0")) {
                    isSubscribebefore = false;
                    UpgradePlaneDialog(1);
                } else {
                    isSubscribebefore = false;
                    UpgradePlaneDialog(1);
                }
            }
        });*/

        return rootView;
    }

    private void getPackageDetails(final boolean reload, final int type) {
        JSONObject j = new JSONObject();
        String url = VolleyApiRequest.REQUEST_BASEURL + "api/get_plan_detail";
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.GET, url, j, new com.android.volley.Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    String status = response.getString("status");
                    if (status.equals("200")) {
                        //UpgradePlan up = new Gson().fromJson(response.toString(), UpgradePlan.class);
                        qkPreferences.storeUpgradePlan(response.toString() + "");

                        if (reload)
                            UpgradePlaneDialog(type);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("Response", error.getLocalizedMessage() + "");
                error.printStackTrace();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() {

                String token = "";
                if (qkPreferences != null) {
                    token = qkPreferences.getApiToken() + "";
                }

                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "Bearer " + token + "");
                params.put("Accept", "application/json");
                params.put("QK_ACCESS_KEY", context.getResources().getString(R.string.QK_ACCESS_KEY));
                return params;
            }
        };
        Volley.newRequestQueue(getActivity()).add(jsonRequest);
    }

    //ViewPager viewPager_up;
    TabLayout tab_up;
    RecyclerView rec_upgrade_plan;

    ArrayList<UpgradePlan.Datum> upList;
    NewUpgradePlaneAdapter newUpgradePlaneAdapter;

    private void UpgradePlaneDialog(int type) {

        try {
            dialogup = new Dialog(getActivity());
            dialogup.requestWindowFeature(Window.FEATURE_NO_TITLE);
            View view = LinearLayout.inflate(getActivity(), R.layout.dialog_upgrade_plan, null);
            dialogup.setContentView(view);
            dialogup.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            lp.copyFrom(dialogup.getWindow().getAttributes());
            lp.width = WindowManager.LayoutParams.MATCH_PARENT;
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
            dialogup.show();
            dialogup.getWindow().setAttributes(lp);

            ImageView close_dialog_payment = dialogup.findViewById(R.id.close_dialog_payment);
            close_dialog_payment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (dialogup != null && dialogup.isShowing()) {
                        dialogup.dismiss();
                    }
                }
            });
            rec_upgrade_plan = dialogup.findViewById(R.id.rec_upgrade_plan);
            upList = new ArrayList<>();
            String plan = qkPreferences.getUpgradePlan() + "";
            UpgradePlan up = new Gson().fromJson(plan + "", UpgradePlan.class);
            if (up != null && up.getStatus().equalsIgnoreCase("200")) {
                upList.addAll(up.getData());
            } else {
                getPackageDetails(true, type);
            }
            try {
                if (type == 1) {
                    UpgradePlan.Datum datum = new UpgradePlan.Datum();
                    datum.setName("Trial-Package");
                    datum.setAmount("0.0");
                    datum.setCostPerEmployee("0.0");
                    datum.setMax_employee_cost("0.0");
                    datum.setDescription("TRIAL PACKAGE");
                    datum.setMaxEmployeeLimit("5");
                    datum.setMinEmployeeLimit("1");
                    upList.add(0, datum);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            newUpgradePlaneAdapter = new NewUpgradePlaneAdapter(upList, getActivity(), upgradepackagelistener);
            final LinearLayoutManager horizontalLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
            rec_upgrade_plan.setLayoutManager(horizontalLayoutManager);
            rec_upgrade_plan.setAdapter(newUpgradePlaneAdapter);
            SnapHelper snapHelper = new PagerSnapHelper();
            snapHelper.attachToRecyclerView(rec_upgrade_plan);
            rec_upgrade_plan.addItemDecoration(new LinePagerIndicatorDecoration());

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    Upgradepackagelistener upgradepackagelistener = new Upgradepackagelistener() {
        @Override
        public void onUpgradePackage(int pos, String name, String maxemp) {
            if (name != null) {
                if (!isSubscribebefore) {
                    //OpenDialogForPaymet(name);
                    String Trial = "";
                    if (name.equalsIgnoreCase("Trial-Package")) {
                        Trial = "1";
                        if (upList != null && upList.get(1).getName() != null) {
                            name = upList.get(1).getName() + "";
                        }
                    } else {
                        Trial = "0";
                    }

                    if (dialogup != null && dialogup.isShowing()) {
                        dialogup.dismiss();
                    }

                    Intent i = new Intent(getActivity(), PaymentDetails.class);
                    i.putExtra("TRIAL", Trial + "");
                    i.putExtra("NAME", name + "");
                    startActivityForResult(i, SUBSCRIBE_PACKAGE);

                } else {
                    JSONObject j = new JSONObject();
                    try {
                        j.put("id", login_user_id + "");
                        j.put("plan", name + "");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    CallApiForUpgradePackage(j);
                }
            }
        }
    };

    private void OpenDialogForPaymet(final String name) {

        final Dialog dialog_payment = new Dialog(getActivity());
        dialog_payment.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View view = LinearLayout.inflate(getActivity(), R.layout.dialog_stripe, null);
        dialog_payment.setContentView(view);
        dialog_payment.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog_payment.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        dialog_payment.show();
        dialog_payment.getWindow().setAttributes(lp);

        final StripeIntegration stripeIntegration;
        final CardMultilineWidget cardMultilineWidget;
        Button btnToken;

        cardMultilineWidget = dialog_payment.findViewById(R.id.card_input_widget);
        btnToken = dialog_payment.findViewById(R.id.btnToken);
        stripeIntegration = StripeIntegration.getInstance(getActivity());
        stripeIntegration.initStripe();

        btnToken.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try {
                    Utilities.hideKeyboard(getActivity());

                    final ProgressDialog pd = Utilities.showProgress(getActivity());
                    final String Trial;
                    if (name.equalsIgnoreCase("Trial-Package"))
                        Trial = "1";
                    else
                        Trial = "0";

                    if (cardMultilineWidget.getCard() != null) {
                        String cvv = cardMultilineWidget.getCard().getCVC();
                        int exp = cardMultilineWidget.getCard().getExpMonth();
                        int exp_year = cardMultilineWidget.getCard().getExpYear();
                        String card_num = cardMultilineWidget.getCard().getNumber();
                        Card card = new Card(card_num, exp, exp_year, cvv);

                        stripeIntegration.generateStripeToken(card, new TokenCallback() {
                            @Override
                            public void onError(Exception error) {
                                if (pd != null && pd.isShowing())
                                    pd.dismiss();

                                Toast.makeText(getActivity(), error.getMessage() + "", Toast.LENGTH_SHORT).show();
                                //Snackbar.make(rootView, error.getMessage(), Snackbar.LENGTH_SHORT).show();
                            }

                            @Override
                            public void onSuccess(Token token) {
                                if (pd != null && pd.isShowing())
                                    pd.dismiss();

                                if (dialog_payment != null && dialog_payment.isShowing())
                                    dialog_payment.dismiss();

                                String user_stripe_token = token.getId();
                                qkPreferences.storeUserStripeToken(user_stripe_token);

                                JSONObject j = new JSONObject();
                                try {
                                    j.put("id", login_user_id + "");
                                    j.put("plan", name + "");
                                    j.put("trial", Trial + "");
                                    j.put("stripe_token", user_stripe_token);

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                CallApiForUpgradePackage(j);
                                //Snackbar.make(rootView, token.getId(), Snackbar.LENGTH_SHORT).show();
                            }
                        });
                    } else {
                        if (pd != null && pd.isShowing())
                            pd.dismiss();
                        Toast.makeText(getActivity(), "Please Try again", Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

    }

    public void CallApiForUpgradePackage(JSONObject j) {
        final ProgressDialog pd = Utilities.showProgress(getActivity());

        try {
            String url = VolleyApiRequest.REQUEST_BASEURL + "api/web/stripe_checkout";
            JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, url, j, new com.android.volley.Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    if (pd != null && pd.isShowing())
                        pd.dismiss();
                    try {
                        String status = response.getString("status");
                        String msg = response.getString("msg");

                        if (dialogup != null && dialogup.isShowing()) {
                            dialogup.dismiss();
                        }
                        Toast.makeText(context, msg + "", Toast.LENGTH_SHORT).show();
                        if (status.equals("200")) {
                            try {
                                JSONObject jsonObject = response.getJSONObject("data");
                                JSONObject PlanDetails = jsonObject.getJSONObject("plan_detail");
                                if (PlanDetails != null) {
                                    String maxEmployee = PlanDetails.getString("max_employee_limit");
                                    //qkPreferences.setValues(QKPreferences.MAX_EMPLOYEE, maxEmployee + "");
                                    qkPreferences.setValues(QKPreferences.MAX_NEW_EMPLOYEE, maxEmployee);

                                    String TotalEmployee = jsonObject.getString("added_employees");
                                    qkPreferences.setValues(QKPreferences.TOTAL_EMPLOYEE, TotalEmployee + "");
                                } else {
                                    qkPreferences.setValues(QKPreferences.MAX_NEW_EMPLOYEE, "0");
                                    //qkPreferences.setValues(QKPreferences.TOTAL_EMPLOYEE, "0");
                                }
                                /*try {
                                    if (profileEmpPojo.getPlan_detail() != null) {
                                        qkPreferences.setValues(QKPreferences.MAX_NEW_EMPLOYEE, profileEmpPojo.getPlan_detail().getMax_employee());
                                        qkPreferences.setValues(QKPreferences.TOTAL_EMPLOYEE, profileEmpPojo.getPlan_detail().getAdded_employees());
                                    } else {
                                        qkPreferences.setValues(QKPreferences.MAX_NEW_EMPLOYEE, "0");
                                        qkPreferences.setValues(QKPreferences.TOTAL_EMPLOYEE, "0");
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }*/
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }, new com.android.volley.Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if (pd != null && pd.isShowing())
                        pd.dismiss();
                    Toast.makeText(context, "Please try again.", Toast.LENGTH_SHORT).show();
                    Log.d("Response", error.getLocalizedMessage() + "");
                    error.printStackTrace();
                }
            }) {
                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("Authorization", "Bearer " + qkPreferences.getApiToken() + "");
                    params.put("Accept", "application/json");
                    params.put("QK_ACCESS_KEY", context.getResources().getString(R.string.QK_ACCESS_KEY));
                    return params;
                }
            };
            Volley.newRequestQueue(getActivity()).add(jsonRequest);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void openSubscriptionDialog(String Message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Alert");
        builder.setMessage(Message + "");
        builder.setPositiveButton("Subscribe", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Uri uriUrl = Uri.parse("https://quickkonnect.com");
                Intent launchBrowser = new Intent(Intent.ACTION_VIEW, uriUrl);
                startActivity(launchBrowser);
            }
        });
        builder.setNegativeButton("CANCEL", null);
        builder.show();
    }

    public void setEmpAdp() {
        adapter = new EmployeeAdapter(login_user_id, getActivity(), mlist, employeeClicked, isEdit);
        recyclerView.setAdapter(adapter);
    }

    EmployeeClicked employeeClicked = new EmployeeClicked() {
        @Override
        public void onEmployeeClicked(String Type, int pos, String data, String name, String type, String role, String rolename) {
            if (!Type.equalsIgnoreCase("") && Type.equalsIgnoreCase("HEADER")) {
                OpenEmployeeDialog();
            } else if (!Type.equalsIgnoreCase("") && Type.equalsIgnoreCase("IMG_MORE")) {
                setEditDelete(pos, data, name, type, role, rolename);
            } else if (!Type.equalsIgnoreCase("") && Type.equalsIgnoreCase("EDIT")) {
                Intent i = new Intent(getActivity(), EmployeesProfile.class);
                i.putExtra("KEY", "EDIT");
                i.putExtra("USER_ID", profile_id + "");
                String json_data = new Gson().toJson(mlist.get(pos));
                i.putExtra("USER_BASIC_DETAIL", json_data + "");
                startActivityForResult(i, HEADER_RESULT);
            }
        }
    };

    private void setEditDelete(final int pos, final String data, final String name, final String type, final String role, final String rolename) {
        final CharSequence[] items;
        if (type.equalsIgnoreCase("A")) {
            items = new CharSequence[]{"Manage Roles", "Edit", "Delete"};
        } else {
            items = new CharSequence[]{"Edit", "Delete"};
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                if (type.equalsIgnoreCase("A")) {
                    if (item == 0) {
                        Intent i = new Intent(getActivity(), ManageRoles.class);
                        i.putExtra("KEY", "EDIT");
                        i.putExtra("USER_ID", profile_id + "");
                        i.putExtra("ROLE_ID", role + "");
                        i.putExtra("ROLE", rolename + "");
                        String json_data = new Gson().toJson(mlist.get(pos));
                        i.putExtra("USER_BASIC_DETAIL", json_data + "");
                        startActivityForResult(i, HEADER_RESULT);
                    } else if (item == 1) {
                        Intent i = new Intent(getActivity(), EmployeesProfile.class);
                        i.putExtra("KEY", "EDIT");
                        i.putExtra("USER_ID", profile_id + "");
                        String json_data = new Gson().toJson(mlist.get(pos));
                        i.putExtra("USER_BASIC_DETAIL", json_data + "");
                        startActivityForResult(i, HEADER_RESULT);
                    } else if (item == 2) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                        String msg = "";
                        if (name.equalsIgnoreCase(""))
                            msg = "Are you sure want to delete this employee?";
                        else
                            msg = "Are you sure want to delete " + name + " from employee ?";
                        builder.setMessage(msg);
                        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                                DeleteEmployee(data, pos);
                            }
                        });
                        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        });
                        builder.show();
                    }
                } else {
                    if (item == 0) {
                        Intent i = new Intent(getActivity(), EmployeesProfile.class);
                        i.putExtra("KEY", "EDIT");
                        i.putExtra("USER_ID", profile_id + "");
                        String json_data = new Gson().toJson(mlist.get(pos));
                        i.putExtra("USER_BASIC_DETAIL", json_data + "");
                        startActivityForResult(i, HEADER_RESULT);
                    } else if (item == 1) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                        String msg = "";
                        if (name.equalsIgnoreCase(""))
                            msg = "Are you sure want to delete this employee?";
                        else
                            msg = "Are you sure want to delete " + name + " from employee?";
                        builder.setMessage(msg);
                        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                                DeleteEmployee(data, pos);
                            }
                        });
                        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        });
                        builder.show();
                    }
                }
            }
        });
        builder.setPositiveButton("Close", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        builder.show();
    }

    private void DeleteEmployee(String data, final int pos) {
        try {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("id", data + "");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            new VolleyApiRequest(context, true, VolleyApiRequest.REQUEST_BASEURL + "api/member/delete_member",
                    new VolleyCallBack() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                String msg = response.getString("msg");
                                String status = response.getString("status");
                                Utilities.showToast(getActivity(), msg + "");
                                if (status.equalsIgnoreCase("200")) {
                                    //getEmployeesList(true);
                                    mlist.remove(pos);
                                    if (adapter != null)
                                        adapter.notifyDataSetChanged();
                                    if (mlist != null && mlist.size() > 0)
                                        no_recored_found.setVisibility(View.GONE);
                                    else
                                        no_recored_found.setVisibility(View.VISIBLE);

                                    String total_emp = null;
                                    try {
                                        total_emp = response.getString("added_employees");
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    if (total_emp != null && !total_emp.equalsIgnoreCase("") && !total_emp.equalsIgnoreCase("null")) {
                                        qkPreferences.setValues(QKPreferences.TOTAL_EMPLOYEE, total_emp + "");
                                    } else {
                                        try {
                                            String toemp = qkPreferences.getValues(QKPreferences.TOTAL_EMPLOYEE);
                                            int total_count = Integer.parseInt(toemp);
                                            total_count = total_count - 1;
                                            qkPreferences.setValues(QKPreferences.TOTAL_EMPLOYEE, total_count + "");
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onErrorResponse(VolleyError error) {
                            VolleyApiRequest.showVolleyError(context, error);
                        }
                    }).enqueRequest(jsonObject, Request.Method.POST);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setFilter() {
        final CharSequence[] items = {"Ascending", "Descending"};
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Filter By");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                if (item == 0) {
                    filterBy(0);
                } else if (item == 1) {
                    filterBy(1);
                }
            }
        });
        builder.setPositiveButton("Close", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        builder.show();
    }

    private void filterBy(int i) {
        switch (i) {
            case 0:
                //Ascending
                try {
                    Collections.sort(mlist, new Comparator<PojoClasses.EmployeesList>() {
                        @Override
                        public int compare(PojoClasses.EmployeesList employeesList, PojoClasses.EmployeesList t1) {
                            String s1 = String.valueOf(employeesList.getUsername().charAt(0));
                            String s2 = String.valueOf(t1.getUsername().charAt(0));
                            return s1.compareToIgnoreCase(s2);
                        }
                    });
                    adapter.notifyDataSetChanged();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case 1:
                //Descending
                try {
                    Collections.sort(mlist, new Comparator<PojoClasses.EmployeesList>() {
                        @Override
                        public int compare(PojoClasses.EmployeesList employeesList, PojoClasses.EmployeesList t1) {
                            String s1 = String.valueOf(employeesList.getUsername().charAt(0));
                            String s2 = String.valueOf(t1.getUsername().charAt(0));
                            return s2.compareToIgnoreCase(s1);
                        }
                    });
                    adapter.notifyDataSetChanged();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
        }
    }

    private void filter(String text) {
        try {
            ArrayList<PojoClasses.EmployeesList> filterdNames = new ArrayList<>();
            for (PojoClasses.EmployeesList s : mlist) {
                PojoClasses.EmployeesList.User user = new Gson().fromJson(s.getUser1(), PojoClasses.EmployeesList.User.class);
                if (user.getFirstname().equalsIgnoreCase(text) || user.getFirstname().equalsIgnoreCase(text) || user.getFirstname().contains(text) || user.getFirstname().contains(text.toLowerCase()) || user.getFirstname().contains(text.toUpperCase()) || user.getLastname().equalsIgnoreCase(text) || user.getLastname().equalsIgnoreCase(text) || user.getLastname().contains(text) || user.getLastname().contains(text.toLowerCase()) || user.getLastname().contains(text.toUpperCase()) || user.getEmail().contains(text.toLowerCase()) || user.getEmail().contains(text.toUpperCase())) {
                    filterdNames.add(s);
                }
            }
            adapter.filterList(filterdNames);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void showEmployees(boolean isReload) {

        RealmResults<Employees> employees = RealmController.with(getActivity()).getEmployeeByProfileId(profile_id);

        if (employees != null && employees.size() > 0) {

            mlist.clear();

            try {
                for (Employees foloo1 : employees) {

                    mlist.add(new PojoClasses.EmployeesList(
                            foloo1.getUsername(),
                            foloo1.getId(),
                            foloo1.getProfile_id(),
                            foloo1.getUser_id(),
                            foloo1.getEmployee_no(),
                            foloo1.getValid_from(),
                            foloo1.getValid_to(),
                            foloo1.getDesignation(),
                            foloo1.getDepartment(),
                            foloo1.getStatus(),
                            foloo1.getReason(),
                            foloo1.getCreated_at(),
                            foloo1.getUpdated_at(),
                            foloo1.getUser1(),
                            foloo1.getRole(),
                            foloo1.getRole_name()));
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            if (mlist.size() > 0) {
                setEmpAdp();
                no_recored_found.setVisibility(View.GONE);
            } else
                no_recored_found.setVisibility(View.VISIBLE);
        }

        if (isReload) {
            getEmployeesList();
        }
    }

    private void getEmployeesList() {
        try {

            String token = "";
            if (qkPreferences != null) {
                token = qkPreferences.getApiToken() + "";
            }

            Log.e("Employee data = ", "ProfileData= " + profile_id + "_loginuser_id=" + login_user_id + "");

            Map<String, String> params = new HashMap<>();
            params.put("Authorization", "Bearer " + token + "");
            params.put("Accept", "application/json");
            params.put("QK_ACCESS_KEY", getResources().getString(R.string.QK_ACCESS_KEY) + "");

            mService.getCompanyEmployeeList(params, profile_id, login_user_id, 1).enqueue(new Callback<ProfileEmpPojo>() {
                @Override
                public void onResponse(Call<ProfileEmpPojo> call, Response<ProfileEmpPojo> response) {
                    Log.e("Employee-Error", "" + response.body());
                    if (response != null && response.body() != null) {
                        ProfileEmpPojo profileEmpPojo = response.body();
                        if (profileEmpPojo.getStatus() == 200) {
                            List<Datum> datumList = profileEmpPojo.getData();

                            if (datumList != null && datumList.size() > 0) {
                                try {
                                    RealmController.with(getActivity()).DeleteEmployeeFromProfileId(profile_id + "");
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                                for (Datum datum : datumList) {
                                    Employees emp = new Employees();

                                    emp.setId("" + datum.getId());
                                    emp.setProfile_id("" + datum.getProfileId());
                                    emp.setUser_id("" + datum.getUserId());
                                    emp.setEmployee_no("" + datum.getEmployeeNo());
                                    emp.setValid_to(datum.getValidTo());
                                    emp.setValid_from(datum.getValidFrom());
                                    emp.setDesignation(datum.getDesignation());
                                    emp.setDepartment(datum.getDepartment());
                                    emp.setStatus(datum.getStatus());
                                    emp.setReason("" + datum.getReason());
                                    if (datum.getUser() != null && datum.getUser().getFirstname() != null && datum.getUser().getLastname() != null)
                                        emp.setUsername(datum.getUser().getFirstname() + " " + datum.getUser().getLastname());
                                    else
                                        emp.setUsername(datum.getUser().getFirstname());
                                    emp.setUser1(new Gson().toJson(datum.getUser()));
                                    emp.setRole("" + datum.getRole());
                                    emp.setRole_name(datum.getRoleName());

                                    RealmController.with(getActivity()).updateEmployee(emp);
                                }
                            }
                            // todo need to change
                            try {
                                if (profileEmpPojo.getPlan_detail() != null) {
                                    qkPreferences.setValues(QKPreferences.MAX_NEW_EMPLOYEE, profileEmpPojo.getPlan_detail().getMax_employee());
                                    qkPreferences.setValues(QKPreferences.TOTAL_EMPLOYEE, profileEmpPojo.getPlan_detail().getAdded_employees());
                                } else {
                                    qkPreferences.setValues(QKPreferences.MAX_NEW_EMPLOYEE, "0");
                                    qkPreferences.setValues(QKPreferences.TOTAL_EMPLOYEE, "0");
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else
                            Log.e("Employee-Error", "Error");

                    } else
                        Log.e("Employee-Error", "Error");

                    showEmployees(false);
                }

                @Override
                public void onFailure(Call<ProfileEmpPojo> call, Throwable t) {
                    Log.e("Employee-Error", "" + t.getLocalizedMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            getEmployeesList();
          /*  if (requestCode == SUBSCRIBE_PACKAGE) {
                if (data != null && data.getExtras() != null) {

                }
            }*/
        }
        getEmployeesList();
    }

    private void OpenEmployeeDialog() {
        Intent i = new Intent(getActivity(), EmployeeSearch.class);
        if (profile_id != null && !profile_id.equalsIgnoreCase("")) {
            i.putExtra("PROFILE_ID", profile_id + "");
        } else {
            i.putExtra("PROFILE_ID", qkPreferences.getProfileID() + "");
        }
        startActivityForResult(i, HEADER_RESULT);
    }
}


//        try {
//            JSONObject jsonObject = new JSONObject();
//            try {
//                jsonObject.put("profile_id", profile_id);
//                jsonObject.put("user_id", login_user_id);
//                jsonObject.put("page", 1 + "");
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//            new VolleyApiRequest(context, false, VolleyApiRequest.REQUEST_BASEURL + "api/member/member_list",
//                    new VolleyCallBack() {
//                        @Override
//                        public void onResponse(JSONObject response) {
//                            parseResponse(response);
//                        }
//
//                        @Override
//                        public void onErrorResponse(VolleyError error) {
//                            VolleyApiRequest.showVolleyError(context, error);
//                        }
//                    }).enqueRequest(jsonObject, Request.Method.POST);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//   }

//    public void parseResponse(JSONObject response) {
//        try {
//            String responseStatus = response.getString("status");
//            if (responseStatus.equals("200")) {
//                RealmController.with(getActivity()).clearEmployee();
//                JSONArray jsnArr = response.getJSONArray("data");
//                if (jsnArr != null && jsnArr.length() > 0) {
//                    mlist = new ArrayList<>();
//                    for (int i = 0; i < jsnArr.length(); i++) {
//                        JSONObject obj = jsnArr.getJSONObject(i);
//                        JSONObject user = obj.getJSONObject("user");
//
//                        Employees emp = new Employees();
//
//                        emp.setId(obj.getString("id"));
//                        emp.setProfile_id(obj.getString("profile_id"));
//                        emp.setUser_id(obj.getString("user_id"));
//                        emp.setEmployee_no(obj.getString("employee_no"));
//                        emp.setValid_to(obj.getString("valid_to"));
//                        emp.setValid_from(obj.getString("valid_from"));
//                        emp.setDesignation(obj.getString("designation"));
//                        emp.setDepartment(obj.getString("department"));
//                        emp.setStatus(obj.getString("status"));
//                        emp.setReason(obj.getString("reason"));
//                        emp.setUsername(user.getString("firstname"));
//                        emp.setUser1(user.toString());
//                        emp.setRole(obj.getString("role"));
//                        emp.setRole_name(obj.getString("role_name"));
//
//                        RealmController.with(getActivity()).updateEmployee(emp);
//                    }
//                    if (mlist.size() > 0)
//                        no_recored_found.setVisibility(View.GONE);
//
//                    else
//                        no_recored_found.setVisibility(View.VISIBLE);
//
//                } else {
//                    //recyclerView.setVisibility(View.GONE);
//                    no_recored_found.setVisibility(View.VISIBLE);
//                }
//            } else {
//                Realm realm = Realm.getDefaultInstance();
//                realm.beginTransaction();
//                realm.clear(Employees.class);
//                realm.commitTransaction();
//                //recyclerView.setVisibility(View.GONE);
//                no_recored_found.setVisibility(View.VISIBLE);
//            }
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//        getEmployees();
//    }


















/*



    public void shareItem() {
        Intent share = new Intent(android.content.Intent.ACTION_SEND);
        share.setType("text/plain");
        share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
        share.putExtra(Intent.EXTRA_SUBJECT, "QuickKonnect");
        share.putExtra(Intent.EXTRA_TEXT, "https://play.google.com/store/apps/details?id=com.quickkonnect");
        startActivity(Intent.createChooser(share, "The Quickkonnect app connects users through a personalized QK-Identification-Code (QK-Tag)."));
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void revealShow(final LinearLayout lin) {

        int w = lin.getWidth();
        int h = lin.getHeight();

        LinearLayout linearLayout = rootView.findViewById(R.id.lin_header_emp);
        int endRadius = (int) Math.hypot(w, h);

        int cx = (int) (linearLayout.getX() + (linearLayout.getWidth() / 2));
        int cy = (int) (linearLayout.getY()) + linearLayout.getHeight() + 56;

        Animator revealAnimator = ViewAnimationUtils.createCircularReveal(lin, cx, cy, 5f, endRadius);
        lin.setVisibility(View.VISIBLE);
        revealAnimator.setDuration(500);
        revealAnimator.start();
    }

    private void ShowMessageFormWeb(String msg) {
        search.setVisibility(View.GONE);
        cancel.setVisibility(View.VISIBLE);
        invite.setVisibility(View.VISIBLE);
        confirm.setVisibility(View.GONE);
        linear_profile.setVisibility(View.GONE);
        email.setVisibility(View.GONE);
        lin_search.setVisibility(View.GONE);
        search_email.setText("");
        username.setText("");
        subtitle.setVisibility(View.GONE);
        header.setVisibility(View.GONE);
        no_user.setVisibility(View.VISIBLE);
        no_user.setText(msg + "");
        cancelPressed = true;
    }

    public void invitePeople(String emailid) {
        invite_emailid = emailid;
        linearInvitePeople.setVisibility(View.VISIBLE);
        UserBasicDetail = "";
        search.setVisibility(View.GONE);
        cancel.setVisibility(View.VISIBLE);
        invite.setVisibility(View.VISIBLE);
        invite.setVisibility(View.VISIBLE);
        confirm.setVisibility(View.GONE);
        linear_profile.setVisibility(View.GONE);
        email.setVisibility(View.GONE);
        lin_search.setVisibility(View.GONE);
        subtitle.setVisibility(View.GONE);
        search_email.setText("");
        header.setVisibility(View.GONE);
        String html = "This <u><font color='#2ABBBE'>" + emailid + "</font></u> is not registered with QuickKonnect.";
        no_user.setText(Html.fromHtml(html));
        username.setText("");
        //textimput_email_transfer.setVisibility(View.GONE);
        no_user.setVisibility(View.VISIBLE);
    }










    private void getUserFromEmail(final String email1, final Dialog dialog) {
        UserBasicDetail = "";
        try {
            JSONObject obj = new JSONObject();
            try {
                obj.put("email", email1);
                obj.put("user_id", qkPreferences.getLoggedUserid() + "");
                if (profile_id != null && !profile_id.equalsIgnoreCase("")) {
                    obj.put("profile_id", profile_id);
                } else {
                    obj.put("profile_id", qkPreferences.getProfileID());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            new VolleyApiRequest(getActivity(), true, VolleyApiRequest.REQUEST_BASEURL + "api/member/check_member",
                    new VolleyCallBack() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.e("Request Data", "" + response);
                            try {
                                String msg = response.getString("msg") + "";
                                String status = response.getString("status") + "";

                                if (status.equalsIgnoreCase("200")) {

                                    String is_added = response.getString("is_added") + "";

                                    if (is_added != null && !is_added.equalsIgnoreCase("") &&
                                            !is_added.equalsIgnoreCase("null") && is_added.equalsIgnoreCase("0") || !is_added.equalsIgnoreCase("null") && is_added.equalsIgnoreCase("1")) {

                                        JSONObject data = response.getJSONObject("data");
                                        JSONObject basicdetail = data.getJSONObject("basic_detail");
                                        BasicDetail basicDetail = new Gson().fromJson(basicdetail.toString(), BasicDetail.class);
                                        UserBasicDetail = basicdetail.toString();

                                       /* if (basicDetail.getUserId() != null && !basicDetail.getUserId().toString().equalsIgnoreCase("null")) {
                                            id = basicDetail.getUserId().toString();
                                        }
                                        if (login_user_id.toString().equalsIgnoreCase(id + "")) {
                                            search.setVisibility(View.GONE);
                                            cancel.setVisibility(View.VISIBLE);
                                            invite.setVisibility(View.VISIBLE);
                                            confirm.setVisibility(View.GONE);
                                            linear_profile.setVisibility(View.GONE);
                                            email.setVisibility(View.GONE);
                                            lin_search.setVisibility(View.GONE);
                                            search_email.setText("");
                                            username.setText("");
                                            header.setVisibility(View.GONE);
                                            subtitle.setVisibility(View.GONE);
                                            //textimput_email_transfer.setVisibility(View.GONE);
                                            no_user.setVisibility(View.VISIBLE);
                                            no_user.setText("You are an owner of this Profile");
                                            cancelPressed = true;

                                        } else {*/


                                     /*   try{
                                                if(dialog.isShowing())
                                                dialog.dismiss();
                                                }catch(Exception e){
                                                e.printStackTrace();
                                                }
                                                Intent i=new Intent(getActivity(),EmployeesProfile.class);
        i.putExtra("KEY","HEADER");
        i.putExtra("USER_ID",profile_id+"");
        i.putExtra("USER_BASIC_DETAIL",UserBasicDetail+"");
        startActivityForResult(i,HEADER_RESULT);
        // }
        }else{
        search.setVisibility(View.GONE);
        cancel.setVisibility(View.VISIBLE);
        invite.setVisibility(View.VISIBLE);
        confirm.setVisibility(View.GONE);
        linear_profile.setVisibility(View.GONE);
        email.setVisibility(View.GONE);
        lin_search.setVisibility(View.GONE);
        search_email.setText("");
        username.setText("");
        subtitle.setVisibility(View.GONE);
        header.setVisibility(View.GONE);
        //textimput_email_transfer.setVisibility(View.GONE);
        no_user.setVisibility(View.VISIBLE);
        no_user.setText("This profile is already added in your profile !");
        cancelPressed=true;

        //Utilities.showToast(getActivity(), "This profile is already added in your profile !");
        }
        }else{
                                    *//*1 - invite_user
                                    2 - inactive_user
                                    3 - admin_owner_user
                                    4 - blocked_user
                                    5 - added_user*//*
        String type=response.getString("type");

        if(type.equalsIgnoreCase("invite_user")){
        invitePeople(email1);
        }else if(type.equalsIgnoreCase("inactive_user")){
        ShowMessageFormWeb(msg);
        }else if(type.equalsIgnoreCase("admin_owner_user")){
        ShowMessageFormWeb(msg);
        }else if(type.equalsIgnoreCase("blocked_user")){
        ShowMessageFormWeb(msg);
        }else if(type.equalsIgnoreCase("added_user")){
        ShowMessageFormWeb(msg);
        }
        }
        }catch(Exception e){
        e.printStackTrace();
        invitePeople(email1);
        }
        }

@Override
public void onErrorResponse(VolleyError error){
        UserBasicDetail="";
        VolleyApiRequest.showVolleyError(getActivity(),error);
        }
        }).enqueRequest(obj,Request.Method.POST);
        }catch(Exception e){
        UserBasicDetail="";
        e.printStackTrace();
        }
        }


final Dialog dialog=new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View view=LinearLayout.inflate(getActivity(),R.layout.dialog_transfer_profile,null);
        dialog.setContentView(view);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        WindowManager.LayoutParams lp=new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width=WindowManager.LayoutParams.MATCH_PARENT;
        dialog.show();
        dialog.getWindow().setAttributes(lp);

        lin_emp_dialog_main=dialog.findViewById(R.id.lin_emp_dialog_main);

        if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.LOLLIPOP){
        view.addOnLayoutChangeListener(new View.OnLayoutChangeListener(){
@TargetApi(Build.VERSION_CODES.LOLLIPOP)
@Override
public void onLayoutChange(View v,int left,int top,int right,int bottom,int oldLeft,int oldTop,int oldRight,int oldBottom){
        v.removeOnLayoutChangeListener(this);
        revealShow(lin_emp_dialog_main);
        }
        });
        }
        email=dialog.findViewById(R.id.edt_email_transfer);
        title=dialog.findViewById(R.id.tvAlert_dg);
        subtitle=dialog.findViewById(R.id.tvAlert_subtitle);
        no_user=dialog.findViewById(R.id.tv_nouser_transfer);
        linearInvitePeople=dialog.findViewById(R.id.linearInvitePeople);
        cancel=dialog.findViewById(R.id.tvCancel_transfer);
        search=dialog.findViewById(R.id.tvSearch_transfer);
        confirm=dialog.findViewById(R.id.tvConfirm_transfer);
        invite=dialog.findViewById(R.id.tvInvite_transfer);
        edtMsg=dialog.findViewById(R.id.edtMsg);
        tvCountMsglength=dialog.findViewById(R.id.tvCountMsglength);
        username=dialog.findViewById(R.id.tv_name_transfer);
        profileImage=dialog.findViewById(R.id.img_profile_transfer);
        linear_profile=dialog.findViewById(R.id.lin_profile_transfer);
        search_email=dialog.findViewById(R.id.edt_search_mail_transfer);
        search_icon=dialog.findViewById(R.id.img_search_transfer);
        header=dialog.findViewById(R.id.image_emp);
        lin_search=dialog.findViewById(R.id.lin_search_transfer);
        //textimput_email_transfer = dialog.findViewById(R.id.textimput_email_transfer);
        subtitle.setText("Enter the email of user, you would like to add as an employee.");
        title.setText("Add Employee");
        title.setVisibility(View.GONE);

        search.setVisibility(View.VISIBLE);
        cancel.setVisibility(View.VISIBLE);
        invite.setVisibility(View.GONE);
        confirm.setVisibility(View.GONE);
        linear_profile.setVisibility(View.GONE);
        lin_search.setVisibility(View.GONE);
        email.setVisibility(View.VISIBLE);
        subtitle.setVisibility(View.VISIBLE);
        //textimput_email_transfer.setVisibility(View.VISIBLE);
        no_user.setVisibility(View.GONE);
        no_user.setText("This email is not registered with QuickKonnect.");
        search_email.setText("");
        username.setText("");
        try{
        search_email.addTextChangedListener(new TextWatcher(){
public void afterTextChanged(Editable s){
        search_email.setCursorVisible(true);
        }

public void beforeTextChanged(CharSequence s,int start,int count,int after){
        }

public void onTextChanged(CharSequence s,int start,int before,int count){
        }
        });
        }catch(Exception e){
        e.printStackTrace();
        }
        search_icon.setOnClickListener(new View.OnClickListener(){
@Override
public void onClick(View view){
        try{
        Utilities.hidekeyboard(getActivity(),search_email);
        }catch(Exception e){
        e.printStackTrace();
        }
        if(!search_email.getText().toString().isEmpty()&&Utilities.isValidEmail(search_email.getText().toString())){
        getUserFromEmail(search_email.getText().toString(),dialog);
        search_email.setText("");
        username.setText("");
        }else{
        Utilities.showToast(getActivity(),"Please Enter Valid Email");
        }
        }
        });

        edtMsg.addTextChangedListener(new TextWatcher(){
@Override
public void beforeTextChanged(CharSequence charSequence,int i,int i1,int i2){

        }

@Override
public void onTextChanged(CharSequence charSequence,int i,int i1,int i2){
        if(charSequence!=null&&charSequence.length()>0){
        int remain=200-charSequence.length();
        tvCountMsglength.setText(""+remain+" Characters");
        }else{
        tvCountMsglength.setText("200 Characters");
        }
        }

@Override
public void afterTextChanged(Editable editable){

        }
        });

        search.setOnClickListener(new View.OnClickListener(){
@Override
public void onClick(View view){
        try{
        Utilities.hidekeyboard(getActivity(),email);
        }catch(Exception e){
        e.printStackTrace();
        }
        if(!email.getText().toString().isEmpty()&&Utilities.isValidEmail(email.getText().toString())){
        getUserFromEmail(email.getText().toString(),dialog);
        search_email.setText("");
        username.setText("");
        email.setText("");
        }else{
        Utilities.showToast(getActivity(),"Please Enter Valid Email");
        }
        }
        });
        cancel.setOnClickListener(new View.OnClickListener(){
@Override
public void onClick(View view){
        if(cancelPressed){
        search.setVisibility(View.VISIBLE);
        cancel.setVisibility(View.VISIBLE);
        invite.setVisibility(View.GONE);
        confirm.setVisibility(View.GONE);
        linear_profile.setVisibility(View.GONE);
        lin_search.setVisibility(View.GONE);
        email.setVisibility(View.VISIBLE);
        header.setVisibility(View.VISIBLE);
        subtitle.setVisibility(View.VISIBLE);
        no_user.setVisibility(View.GONE);
        no_user.setText("This email is not registered with QuickKonnect.");
        search_email.setText("");
        username.setText("");
        cancelPressed=false;
        }else{
        if(dialog.isShowing())
        dialog.dismiss();
        }
        }
        });
        confirm.setOnClickListener(new View.OnClickListener(){
@Override
public void onClick(View view){
        */
/*if (login_user_id.equalsIgnoreCase(id + "")) {
                    Utilities.showToast(getActivity(), "You are an owner of this Profile");
                } else {*//*

        if (dialog.isShowing())
        dialog.dismiss();
        Intent i = new Intent(getActivity(), EmployeesProfile.class);
        i.putExtra("KEY", "HEADER");
        i.putExtra("USER_ID", profile_id + "");
        i.putExtra("USER_BASIC_DETAIL", UserBasicDetail + "");
        startActivityForResult(i, HEADER_RESULT);
        // }
        }
        });
        invite.setOnClickListener(new View.OnClickListener() {
@Override
public void onClick(View view) {
        if (invite_emailid != null && !invite_emailid.isEmpty()) {
        String message = edtMsg.getText().toString();
        if (message != null && !message.isEmpty()) {

        mService.invite_employee(invite_emailid, message).enqueue(new Callback<InviteQKPojo>() {
@Override
public void onResponse(Call<InviteQKPojo> call, Response<InviteQKPojo> response) {
        if (dialog != null && dialog.isShowing())
        dialog.dismiss();
        Log.e("updateSetting-Success", "" + response);
        if (response != null && response.body() != null) {

        InviteQKPojo status200 = response.body();
        Utilities.showToast(context, "" + status200.getMsg());
        }
        }

@Override
public void onFailure(Call<InviteQKPojo> call, Throwable t) {
        Log.e("updateSetting-Error", "" + t.getLocalizedMessage());
        if (dialog != null && dialog.isShowing())
        dialog.dismiss();
        }
        });
        }
        }
        }
        });*/
