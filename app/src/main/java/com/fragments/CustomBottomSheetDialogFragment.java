package com.fragments;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.RequiresApi;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.activities.UpdateBasicProfileActivity;
import com.activities.UpdateContactInfoActivity;
import com.activities.UpdateDashboardBasicProfileActivity;
import com.activities.UpdateEducationActivity;
import com.activities.UpdateExperienceActivity;
import com.activities.UpdateLanguageActivity;
import com.activities.UpdatePublicationActivity;
import com.adapter.UpdateContactInfoAdapter;
import com.adapter.UpdateEducationListAdapter;
import com.adapter.UpdateExperienceListAdapter;
import com.adapter.UpdateLanguageListAdapter;
import com.adapter.UpdatePublicationListAdapter;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.ceylonlabs.imageviewpopup.ImagePopup;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.models.logindata.BasicDetail;
import com.models.logindata.ContactDetail;
import com.models.logindata.EducationDetail;
import com.models.logindata.ExperienceDetail;
import com.models.logindata.LanguageDetail;
import com.models.logindata.ModelLoggedUser;
import com.models.logindata.ModelUserProfile;
import com.models.logindata.PublicationDetail;
import com.quickkonnect.CircleTransform;
import com.quickkonnect.DashboardActivity;
import com.quickkonnect.NewScanProfileActivity;
import com.quickkonnect.PojoClasses;
import com.quickkonnect.R;
import com.quickkonnect.SimpleDividerItemDecoration;
import com.quickkonnect.UpdateQkTagActivity;
import com.realmtable.MyProfiles;
import com.realmtable.RealmController;
import com.squareup.picasso.Picasso;
import com.utilities.CompressImage;
import com.utilities.Constants;
import com.utilities.QKPreferences;
import com.utilities.Utilities;
import com.volleyapirequest.VolleyApiRequest;
import com.volleyapirequest.VolleyCallBack;
import com.yalantis.ucrop.UCrop;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import io.reactivex.annotations.NonNull;

import static android.app.Activity.RESULT_CANCELED;

/**
 * Created by ZTLAB03 on 08-03-2018.
 */

public class CustomBottomSheetDialogFragment extends Fragment implements View.OnClickListener {

    private Context context;
    private QKPreferences qkPreferences;
    private String userid;
    private NestedScrollView scrollView;
    //private FloatingActionButton fab_up;
    private TextView tv_basicProfile_add_info, tv_language_add_info, tv_education_add_info, tv_contact_add_info, tv_experiences_add_info, tv_publication_add_info, tv_skip;
    private TextView tv_basicProfile_gender, tv_basicProfile_username, tv_basicProfile_qk_story, tv_basicProfile_birthdate, tv_basicProfile_location, tv_basic_info_title;
    private TextView tv_add_education, tv_add_contact, tv_add_experiences, tv_add_publication, tv_add_languages, tv_update_profile;
    private ImageView img_basic_profile_profile_pic;
    private String country_id, state_id, city_id, gender;
    private RecyclerView recyclerView_education, recyclerView_contactinfo, recyclerView_experience, recyclerView_publication, recyclerView_languages;
    public RecyclerView.LayoutManager mLayoutManager;

    public UpdateEducationListAdapter adapter;
    public UpdateContactInfoAdapter contactInfoAdapter;
    public UpdateExperienceListAdapter experienceListAdapter;
    public UpdatePublicationListAdapter publicationListAdapter;
    public UpdateLanguageListAdapter languageListAdapter;

    private String birthdate, hometown;
    private String firstname, lastname, profile_pic, qkstory;
    //private View divider_education, divider_contact, divider_experience, divider_publication, divider_languages;
    private ImageView img_update_profile;
    private TextView tv_next;
    private Bundle extras;
    LinearLayout lldob, llhometown, llgender;

    // for upload image
    private BottomSheetDialog bottomSheetDialog;
    private Bitmap selectedImage = null;
    private ImagePopup imagePopup;
    private File croppedFile = null;
    private Uri fileUri;
    private File file;
    static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 5;
    private static final int REQUEST_SELECT_PICTURE = 100;
    static final int REQUEST_STORAGE_READ_ACCESS_PERMISSION = 6;
    private static final int PICK_FROM_CAMERA = 101;
    private String strBase64 = "";

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.content_update_user_profile, container, false);

        initViews(v);
        return v;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void initViews(View view) {
        context = getActivity();
        qkPreferences = new QKPreferences(getActivity());

        //fab_up = view.findViewById(R.id.fab_up);
        scrollView = view.findViewById(R.id.scrollView);
        //fab_up.setVisibility(View.GONE);
        /*scrollView.setOnScrollChangeListener(new View.OnScrollChangeListener() {
            @Override
            public void onScrollChange(View view, int scrollX, int scrollY, int i2, int i3) {
                if (scrollX == 0 && scrollY == 0) {
                    fab_up.setVisibility(View.GONE);
                } else
                    fab_up.setVisibility(View.VISIBLE);
            }
        });

        fab_up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Handler handler;
                handler = new Handler();

                final Runnable r = new Runnable() {
                    public void run() {
                        scrollView.smoothScrollTo(-50, -50);
                    }
                };
                handler.postDelayed(r, 200);
            }
        });*/

        Handler h = new Handler();
        h.postDelayed(new Runnable() {
            @Override
            public void run() {
                scrollView.scrollTo(-50, -50);
            }
        }, 250);

        userid = qkPreferences.getLoggedUser().getData().getId() + "";

        // Basic Info
        tv_basicProfile_username = view.findViewById(R.id.tv_basicProfile_username);
        tv_basicProfile_gender = view.findViewById(R.id.tv_basicProfile_gender);
        tv_basicProfile_qk_story = view.findViewById(R.id.tv_basicProfile_qk_story);
        tv_basicProfile_birthdate = view.findViewById(R.id.tv_basicProfile_birthdate);
        tv_basicProfile_location = view.findViewById(R.id.tv_basicProfile_location);
        tv_basic_info_title = view.findViewById(R.id.tv_basic_info_title);

        lldob = view.findViewById(R.id.lldob);
        llgender = view.findViewById(R.id.llgender);
        llhometown = view.findViewById(R.id.llhometown);
        // Show/Hide Add Information
        tv_basicProfile_add_info = view.findViewById(R.id.tv_basicProfile_add_info);
        tv_education_add_info = view.findViewById(R.id.tv_education_add_info);
        tv_contact_add_info = view.findViewById(R.id.tv_contact_add_info);
        tv_experiences_add_info = view.findViewById(R.id.tv_experiences_add_info);
        tv_publication_add_info = view.findViewById(R.id.tv_publication_add_info);
        tv_language_add_info = view.findViewById(R.id.tv_language_add_info);
        //Title Text
        tv_next = view.findViewById(R.id.tv_next);
        tv_skip = view.findViewById(R.id.tv_skip);
        tv_update_profile = view.findViewById(R.id.tv_update_profile);
        img_update_profile = view.findViewById(R.id.img_update_profile);
        //Basic Profile EditText
        img_basic_profile_profile_pic = view.findViewById(R.id.img_basic_profile_profile_pic);
        img_basic_profile_profile_pic.setOnClickListener(this);

        tv_skip.setOnClickListener(this);
        tv_next.setOnClickListener(this);
        tv_update_profile.setOnClickListener(this);
        img_update_profile.setOnClickListener(this);

        // Add Education Exeprience etc
        tv_add_education = view.findViewById(R.id.tv_add_education);
        tv_add_contact = view.findViewById(R.id.tv_add_contact);
        tv_add_experiences = view.findViewById(R.id.tv_add_experiences);
        tv_add_publication = view.findViewById(R.id.tv_add_publication);
        tv_add_languages = view.findViewById(R.id.tv_add_languages);

        tv_add_education.setOnClickListener(this);
        tv_add_contact.setOnClickListener(this);
        tv_add_experiences.setOnClickListener(this);
        tv_add_publication.setOnClickListener(this);
        tv_add_languages.setOnClickListener(this);

        tv_basicProfile_add_info.setOnClickListener(this);
        tv_education_add_info.setOnClickListener(this);
        tv_contact_add_info.setOnClickListener(this);
        tv_experiences_add_info.setOnClickListener(this);
        tv_publication_add_info.setOnClickListener(this);
        tv_language_add_info.setOnClickListener(this);
        tv_basic_info_title.setOnClickListener(this);

        /*//View
        divider_contact = view.findViewById(R.id.divider_contact);
        divider_education = view.findViewById(R.id.divider_education);
        divider_experience = view.findViewById(R.id.divider_experience);
        divider_publication = view.findViewById(R.id.divider_publication);
        divider_languages = view.findViewById(R.id.divider_languages);

        divider_contact.setVisibility(View.GONE);
        divider_education.setVisibility(View.GONE);
        divider_experience.setVisibility(View.GONE);
        divider_publication.setVisibility(View.GONE);
        divider_languages.setVisibility(View.GONE);*/

        recyclerView_education = view.findViewById(R.id.recyclerView_education);
        recyclerView_education.setNestedScrollingEnabled(false);
        //recyclerView_education.addItemDecoration(new SimpleDividerItemDecoration(context));
        recyclerView_education.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(context);
        recyclerView_education.setLayoutManager(mLayoutManager);

        recyclerView_contactinfo = view.findViewById(R.id.recyclerView_contactinfo);
        recyclerView_contactinfo.setNestedScrollingEnabled(false);
        //recyclerView_contactinfo.addItemDecoration(new SimpleDividerItemDecoration(context));
        recyclerView_contactinfo.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(context);
        recyclerView_contactinfo.setLayoutManager(mLayoutManager);


        recyclerView_experience = view.findViewById(R.id.recyclerView_experience);
        recyclerView_experience.setNestedScrollingEnabled(false);
        //recyclerView_experience.addItemDecoration(new SimpleDividerItemDecoration(context));
        recyclerView_experience.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(context);
        recyclerView_experience.setLayoutManager(mLayoutManager);

        recyclerView_publication = view.findViewById(R.id.recyclerView_publication);
        recyclerView_publication.setNestedScrollingEnabled(false);
        //recyclerView_publication.addItemDecoration(new SimpleDividerItemDecoration(context));
        recyclerView_publication.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(context);
        recyclerView_publication.setLayoutManager(mLayoutManager);

        recyclerView_languages = view.findViewById(R.id.recyclerView_languages);
        recyclerView_languages.setNestedScrollingEnabled(false);
        //recyclerView_languages.addItemDecoration(new SimpleDividerItemDecoration(context));
        recyclerView_languages.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(context);
        recyclerView_languages.setLayoutManager(mLayoutManager);

        tv_skip.setVisibility(View.VISIBLE);

        tv_skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DashboardActivity.CloseBottomSheet();
            }
        });

        /*try {
            Utilities.makeTextViewResizable(tv_basicProfile_qk_story, 4, "More", true);
        } catch (Exception e) {
            e.printStackTrace();
        }*/


        /*tv_basicProfile_qk_story.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    if (tv_basicProfile_qk_story.getText().toString().length() > 90) {
                        AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
                        alertDialog.setMessage(tv_basicProfile_qk_story.getText().toString() + "");
                        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });
                        alertDialog.show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });*/


        /*extras = getIntent().getExtras();
        if (extras != null) {
            tv_next.setVisibility(View.INVISIBLE);
            tv_skip.setVisibility(View.VISIBLE);
        } else {
            tv_next.setVisibility(View.VISIBLE);
            tv_skip.setVisibility(View.INVISIBLE);
        }*/

        parseResponse();
//        functionGetUserProfile(userid);
    }

    @Override
    public void onClick(View view) {
        Intent intent;
        switch (view.getId()) {
            case R.id.img_basic_profile_profile_pic:

                bottomSheetDialog = new BottomSheetDialog(getActivity());
                View view1 = getActivity().getLayoutInflater().inflate(R.layout.custom_bottom_sheet_profile_pic, null);
                bottomSheetDialog.setContentView(view1);
                Button btnCancel = view1.findViewById(R.id.buttonSheet1);
                TextView TextViewSheet1 = view1.findViewById(R.id.TextViewSheet1);
                View divider_viewphoto = view1.findViewById(R.id.divider_viewphoto);
                TextView TextViewSheet2 = view1.findViewById(R.id.TextViewSheet2);
                TextView TextViewSheet3 = view1.findViewById(R.id.TextViewSheet3);
                TextView TextViewSheet4 = view1.findViewById(R.id.TextViewSheet4);

                if (profile_pic != null && Utilities.isEmpty("" + profile_pic)) {
                    TextViewSheet1.setVisibility(View.VISIBLE);
                    divider_viewphoto.setVisibility(View.VISIBLE);
                } else {
                    TextViewSheet1.setVisibility(View.GONE);
                    divider_viewphoto.setVisibility(View.GONE);
                }

                btnCancel.setOnClickListener(this);
                TextViewSheet1.setOnClickListener(this);
                TextViewSheet2.setOnClickListener(this);
                TextViewSheet3.setOnClickListener(this);
                TextViewSheet4.setOnClickListener(this);

                bottomSheetDialog.show();
                break;

            case R.id.buttonSheet1:
                bottomSheetDialog.hide();
                break;

            case R.id.TextViewSheet1:
                bottomSheetDialog.hide();
                if (selectedImage != null && !selectedImage.equals("null"))
                    initImagepopup(null, fileUri);
                else if (profile_pic != null && Utilities.isEmpty("" + profile_pic))
                    initImagepopup("" + profile_pic, null);
                else
                    Utilities.showToast(context, "Please select profile for view");
                break;

            case R.id.TextViewSheet2:
                bottomSheetDialog.hide();
                pickFromGallery();
                break;

            case R.id.TextViewSheet3:
                bottomSheetDialog.hide();
                getPhotoFromCamera();
                break;

            case R.id.TextViewSheet4:
                bottomSheetDialog.hide();
                removePhoto();
                break;

            case R.id.tv_add_education:
            case R.id.tv_education_add_info:
                intent = new Intent(context, UpdateEducationActivity.class);
                intent.putExtra(Constants.KEY_UPDATE, Constants.ADD_DATA);
                startActivityForResult(intent, Constants.RCODE_UPDATEEDUCATION);
                break;

            case R.id.tv_add_contact:
            case R.id.tv_contact_add_info:
                intent = new Intent(context, UpdateContactInfoActivity.class);
                intent.putExtra(Constants.KEY_UPDATE, Constants.ADD_DATA);
                startActivityForResult(intent, Constants.RCODE_UPDATEEDUCATION);
                break;

            case R.id.tv_add_experiences:
            case R.id.tv_experiences_add_info:
                intent = new Intent(context, UpdateExperienceActivity.class);
                intent.putExtra(Constants.KEY_UPDATE, Constants.ADD_DATA);
                startActivityForResult(intent, Constants.RCODE_UPDATEEDUCATION);
                break;

            case R.id.tv_add_publication:
            case R.id.tv_publication_add_info:
                intent = new Intent(context, UpdatePublicationActivity.class);
                intent.putExtra(Constants.KEY_UPDATE, Constants.ADD_DATA);
                startActivityForResult(intent, Constants.RCODE_UPDATEEDUCATION);
                break;

            case R.id.tv_add_languages:
            case R.id.tv_language_add_info:
                intent = new Intent(context, UpdateLanguageActivity.class);
                intent.putExtra(Constants.KEY_UPDATE, Constants.ADD_DATA);
                startActivityForResult(intent, Constants.RCODE_UPDATEEDUCATION);
                break;

            /*case R.id.tv_skip:
                if (isRefresh) {
                    setResult(RESULT_OK);
                    context.finish();
                } else
                    UpdateUserProfileActivity.this.finish();
                break;*/
            case R.id.tv_next:
                intent = new Intent(context, UpdateQkTagActivity.class);
                intent.putExtra(Constants.KEY_FROM, Constants.KEY_FROM_UPDATEPROFILE);
                startActivity(intent);
                break;

            case R.id.tv_basic_info_title:
                intent = new Intent(context, UpdateBasicProfileActivity.class);
                intent.putExtra("birthdate", birthdate);
                intent.putExtra("hometown", hometown);
                intent.putExtra("country_id", country_id);
                intent.putExtra("state_id", state_id);
                intent.putExtra("gender", gender);
                intent.putExtra("city_id", city_id);
                intent.putExtra(Constants.KEY_UPDATE, Constants.EDIT_DATA);
                startActivityForResult(intent, Constants.RCODE_UPDATEEDUCATION);
                break;

            case R.id.img_update_profile:
                intent = new Intent(context, UpdateDashboardBasicProfileActivity.class);
                if (profile_pic != null && !profile_pic.equalsIgnoreCase("") && !profile_pic.equalsIgnoreCase("null")) {
                    intent.putExtra("profile_pic", profile_pic);

                    /*try {
                        Drawable drawable = img_update_profile.getDrawable();
                        Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
                        ByteArrayOutputStream baos = new ByteArrayOutputStream();
                        bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
                        byte[] b = baos.toByteArray();
                        intent.putExtra("picture", b);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }*/

                } else {
                    intent.putExtra("profile_pic", profile_pic);
                }
                intent.putExtra("firstname", firstname);
                intent.putExtra("lastname", lastname);
                intent.putExtra("qkstory", qkstory);
                startActivityForResult(intent, Constants.RCODE_UPDATEEDUCATION);
                break;

            case R.id.tv_basicProfile_add_info:
                intent = new Intent(context, UpdateBasicProfileActivity.class);
                intent.putExtra(Constants.KEY_UPDATE, Constants.ADD_DATA);
                startActivityForResult(intent, Constants.RCODE_UPDATEEDUCATION);
                break;

//            case R.id.fab_up:
//                final Handler handler;
//                handler = new Handler();
//                final Runnable r = new Runnable() {
//                    public void run() {
//                        scrollView.smoothScrollTo(-50, -50);
//                    }
//                };
//                handler.postDelayed(r, 200);
//                break;
        }
    }

    public void initImagepopup(String userUrl, Uri fileUri) {
        imagePopup = new ImagePopup(getActivity());
        imagePopup.setWindowHeight(800); // Optional
        imagePopup.setWindowWidth(800); // Optional
        imagePopup.setBackgroundColor(Color.BLACK); // Optional
        imagePopup.setFullScreen(true); // Optional
        imagePopup.setHideCloseIcon(false);  // Optional
        imagePopup.setImageOnClickClose(true);  // Optional
        if (Utilities.isEmpty(userUrl))
            imagePopup.initiatePopupWithPicasso(userUrl);
        else if (fileUri != null && Utilities.isEmpty(fileUri.getPath()))
            imagePopup.initiatePopupWithPicasso(fileUri);

        imagePopup.viewPopup();
    }

    public void removePhoto() {
        croppedFile = null;
        img_basic_profile_profile_pic.setImageResource(R.drawable.ic_default_profile_photo);
    }

    public void functionGetUserProfile(final String userid, boolean isProcess) {
        new VolleyApiRequest(context, isProcess, VolleyApiRequest.REQUEST_BASEURL + "api/get_user_detail/" + userid,
                new VolleyCallBack() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String status = response.getString("status");
                            String message = response.getString("msg");
                            if (status.equals("200")) {
                                JSONObject dataObject = response.getJSONObject("data");
                                ModelUserProfile object = new Gson().fromJson(dataObject.toString(), ModelUserProfile.class);
                                ModelLoggedUser modelLoggedUser = qkPreferences.getLoggedUser();
                                modelLoggedUser.getData().setBasicDetail(object.getBasicDetail());
                                modelLoggedUser.getData().setContactDetail(object.getContactDetail());
                                modelLoggedUser.getData().setEducationDetail(object.getEducationDetail());
                                modelLoggedUser.getData().setExperienceDetail(object.getExperienceDetail());
                                modelLoggedUser.getData().setPublicationDetail(object.getPublicationDetail());
                                modelLoggedUser.getData().setLanguageDetail(object.getLanguageDetail());
                                qkPreferences.storeLoggedUser(modelLoggedUser);
                                String filePath = qkPreferences.getValues(Constants.QKTAGLOCALURL);
                                try {
                                    if (Utilities.isEmpty(filePath)) {
                                        File file = new File(filePath);
                                        boolean isDelete = file.delete();
                                        Log.e("File Delete", "" + isDelete);
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                qkPreferences.storeValues(Constants.QKTAGLOCALURL, null);
                                parseResponse();
                            } else {
                                Utilities.showToast(context, message);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyApiRequest.showVolleyError(context, error);
                    }
                }).enqueRequest(null, Request.Method.GET);
    }

    public void parseResponse() {
        try {
            ModelLoggedUser modelLoggedUser = qkPreferences.getLoggedUser();
            if (modelLoggedUser.getData() != null && modelLoggedUser.getData().getBasicDetail() != null) {
                BasicDetail basicDetail = modelLoggedUser.getData().getBasicDetail();
                if (basicDetail.getProfilePic() != null && !basicDetail.getProfilePic().toString().isEmpty()) {
                    Picasso.with(context)
                            .load("" + basicDetail.getProfilePic())
                            .error(R.drawable.ic_default_profile_photo)
                            .placeholder(R.drawable.ic_default_profile_photo)
                            .transform(new CircleTransform())
                            // .networkPolicy(NetworkPolicy.OFFLINE)
                            .fit()
                            .into(img_basic_profile_profile_pic);
                    profile_pic = "" + basicDetail.getProfilePic();
                } else {
                    img_basic_profile_profile_pic.setImageResource(R.drawable.ic_default_profile_photo);
                }
                if (basicDetail.getFirstname() != null && basicDetail.getFirstname().length() > 0 && !basicDetail.getFirstname().equals("null")) {
                    firstname = basicDetail.getFirstname();
                    if (basicDetail.getLastname() != null && basicDetail.getLastname().length() > 0 && !basicDetail.getLastname().equals("null")) {
                        lastname = basicDetail.getLastname();
                        tv_basicProfile_username.setText(basicDetail.getFirstname() + " " + basicDetail.getLastname());

                    } else
                        tv_basicProfile_username.setText(basicDetail.getFirstname());
                }
                if (basicDetail.getQkStory() != null && basicDetail.getQkStory().toString().length() > 0 && !basicDetail.getQkStory().equals("null")) {
                    tv_basicProfile_qk_story.setText("" + basicDetail.getQkStory());
                }

                qkstory = "" + basicDetail.getQkStory();

                if (!Utilities.isEmpty("" + basicDetail.getQkStory())) {
                    tv_basicProfile_qk_story.setText("");
                }

                try {
                    String text = tv_basicProfile_qk_story.getText().toString();
                    Utilities.addReadMore(getActivity(), text, tv_basicProfile_qk_story);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                boolean isBirthEmpty = false, isHometownEmpt = false, isGenderEmpty = false;

                if (basicDetail.getBirthdate() != null && basicDetail.getBirthdate().length() > 0 && !basicDetail.getBirthdate().equals("null") && Utilities.isEmpty(basicDetail.getBirthdate())) {
                    isBirthEmpty = true;
                    lldob.setVisibility(View.VISIBLE);
                    tv_basicProfile_add_info.setVisibility(View.GONE);
                    //String first = "<font color='#414141'>Date of Birth:</font>";
                    //String sourceString = "<b>" + first + "</b> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + modelUserProfile.getBasicDetail().getBirthdate();
                    tv_basicProfile_birthdate.setText(basicDetail.getBirthdate());
                    birthdate = basicDetail.getBirthdate();
                } else
                    lldob.setVisibility(View.GONE);

                if (basicDetail.getGender() != null && !basicDetail.getGender().equals("null") && Utilities.isEmpty(basicDetail.getGender())) {
                    isGenderEmpty = true;
                    llgender.setVisibility(View.GONE);
                    tv_basicProfile_add_info.setVisibility(View.GONE);
                    switch (basicDetail.getGender()) {
                        case "M":
                            tv_basicProfile_gender.setText("Male");
                            llgender.setVisibility(View.VISIBLE);
                            gender = "Male";
                            break;
                        case "F":
                            tv_basicProfile_gender.setText("Female");
                            llgender.setVisibility(View.VISIBLE);
                            gender = "Female";
                            break;
                        case "O":
                            tv_basicProfile_gender.setText("Other");
                            llgender.setVisibility(View.VISIBLE);
                            gender = "Other";
                            break;
                    }
                    tv_basicProfile_gender.setVisibility(View.VISIBLE);
                    birthdate = basicDetail.getBirthdate();
                    gender = basicDetail.getGender();
                } else
                    llgender.setVisibility(View.GONE);

                if (basicDetail.getHometown() != null && basicDetail.getHometown().length() > 0 && !basicDetail.getHometown().equals("null") && Utilities.isEmpty(basicDetail.getHometown())) {
                    isHometownEmpt = true;
                    llhometown.setVisibility(View.VISIBLE);
                    tv_basicProfile_location.setText(basicDetail.getHometown());
                    hometown = basicDetail.getHometown();
                } else llhometown.setVisibility(View.GONE);

                if (isBirthEmpty || isHometownEmpt || isGenderEmpty) {
                    tv_basic_info_title.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_edit_black_24dp, 0);
                    tv_basicProfile_add_info.setVisibility(View.GONE);
                    if (!Utilities.isEmpty(basicDetail.getHometown()))
                        tv_basicProfile_location.setText("");
                    if (!Utilities.isEmpty(basicDetail.getBirthdate()))
                        tv_basicProfile_birthdate.setText("");
                    if (!Utilities.isEmpty(basicDetail.getGender()))
                        tv_basicProfile_gender.setText("");
                    //llgender.setVisibility(View.VISIBLE);
                } else {
                    tv_basicProfile_location.setText("");
                    tv_basicProfile_birthdate.setText("");
                    tv_basicProfile_gender.setText("");
                    //llgender.setVisibility(View.GONE);
                    tv_basicProfile_add_info.setVisibility(View.VISIBLE);
                    tv_basic_info_title.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                }

                if (basicDetail.getCountryId() != null) {
                    country_id = basicDetail.getCountryId() + "";
                }
                if (basicDetail.getCityId() != null) {
                    city_id = basicDetail.getCityId() + "";
                }
                if (basicDetail.getStateId() != null) {
                    state_id = basicDetail.getStateId() + "";
                }
            }

            //Education
            if (modelLoggedUser.getData() != null && modelLoggedUser.getData().getEducationDetail() != null && modelLoggedUser.getData().getEducationDetail().size() > 0) {
                tv_add_education.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_add_black_24dp, 0);
                tv_education_add_info.setVisibility(View.GONE);
                recyclerView_education.setVisibility(View.VISIBLE);
                adapter = new UpdateEducationListAdapter(getActivity(), modelLoggedUser.getData().getEducationDetail());
                recyclerView_education.setAdapter(adapter);
            } else {
                List<EducationDetail> educationDetailList = new ArrayList<>();
                adapter = new UpdateEducationListAdapter(getActivity(), educationDetailList);
                recyclerView_education.setAdapter(adapter);
                //divider_education.setVisibility(View.GONE);
                tv_add_education.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                tv_education_add_info.setVisibility(View.VISIBLE);
            }
            //Contact Information
            if (modelLoggedUser.getData() != null && modelLoggedUser.getData().getContactDetail() != null && modelLoggedUser.getData().getContactDetail().size() > 0) {
                tv_add_contact.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_add_black_24dp, 0);
                tv_contact_add_info.setVisibility(View.GONE);
                contactInfoAdapter = new UpdateContactInfoAdapter(getActivity(), modelLoggedUser.getData().getContactDetail());
                recyclerView_contactinfo.setAdapter(contactInfoAdapter);
            } else {
                List<ContactDetail> contactDetails = new ArrayList<>();
                contactInfoAdapter = new UpdateContactInfoAdapter(getActivity(), contactDetails);
                recyclerView_contactinfo.setAdapter(contactInfoAdapter);
                //divider_contact.setVisibility(View.GONE);
                tv_add_contact.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                tv_contact_add_info.setVisibility(View.VISIBLE);
            }
            // Experience
            if (modelLoggedUser.getData().getExperienceDetail() != null && modelLoggedUser.getData().getExperienceDetail().size() > 0) {
                tv_add_experiences.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_add_black_24dp, 0);
                tv_experiences_add_info.setVisibility(View.GONE);
                experienceListAdapter = new UpdateExperienceListAdapter(getActivity(), modelLoggedUser.getData().getExperienceDetail());
                recyclerView_experience.setAdapter(experienceListAdapter);
            } else {
                List<ExperienceDetail> experienceDetails = new ArrayList<>();
                experienceListAdapter = new UpdateExperienceListAdapter(getActivity(), experienceDetails);
                recyclerView_experience.setAdapter(experienceListAdapter);
                //divider_experience.setVisibility(View.GONE);
                tv_add_experiences.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                tv_experiences_add_info.setVisibility(View.VISIBLE);
            }


            //Publication
            if (modelLoggedUser.getData().getPublicationDetail() != null && modelLoggedUser.getData().getPublicationDetail().size() > 0) {
                tv_add_publication.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_add_black_24dp, 0);
                tv_publication_add_info.setVisibility(View.GONE);
                publicationListAdapter = new UpdatePublicationListAdapter(getActivity(), modelLoggedUser.getData().getPublicationDetail(), true, userid + "");
                recyclerView_publication.setAdapter(publicationListAdapter);

            } else {
                List<PublicationDetail> publicationDetails = new ArrayList<>();
                publicationListAdapter = new UpdatePublicationListAdapter(getActivity(), publicationDetails, true, userid + "");
                recyclerView_publication.setAdapter(publicationListAdapter);
                //divider_publication.setVisibility(View.GONE);
                tv_add_publication.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                tv_publication_add_info.setVisibility(View.VISIBLE);
            }

            //Language
            if (modelLoggedUser.getData().getLanguageDetail() != null && modelLoggedUser.getData().getLanguageDetail().size() > 0) {
                tv_add_languages.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_add_black_24dp, 0);
                tv_language_add_info.setVisibility(View.GONE);
                languageListAdapter = new UpdateLanguageListAdapter((DashboardActivity) context, modelLoggedUser.getData().getLanguageDetail(), true);

                try {
                    Collections.sort(modelLoggedUser.getData().getLanguageDetail(), new Comparator<LanguageDetail>() {
                        @Override
                        public int compare(LanguageDetail languageDetail, LanguageDetail t1) {

                            Float first = Float.parseFloat(languageDetail.getRating());
                            Float second = Float.parseFloat(t1.getRating());

                            if (first.floatValue() > second.floatValue()) {
                                return -1;
                            } else if (first.floatValue() < second.floatValue()) {
                                return 1;
                            } else {
                                return 0;
                            }
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }

                recyclerView_languages.setAdapter(languageListAdapter);
            } else {
                List<LanguageDetail> languageDetails = new ArrayList<>();
                languageListAdapter = new UpdateLanguageListAdapter((DashboardActivity) context, languageDetails, true);
                try {
                    Collections.sort(modelLoggedUser.getData().getLanguageDetail(), new Comparator<LanguageDetail>() {
                        @Override
                        public int compare(LanguageDetail languageDetail, LanguageDetail t1) {

                            Float first = Float.parseFloat(languageDetail.getRating());
                            Float second = Float.parseFloat(t1.getRating());

                            if (first.floatValue() > second.floatValue()) {
                                return -1;
                            } else if (first.floatValue() < second.floatValue()) {
                                return 1;
                            } else {
                                return 0;
                            }
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
                recyclerView_languages.setAdapter(languageListAdapter);
                //divider_languages.setVisibility(View.GONE);
                tv_add_languages.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                tv_language_add_info.setVisibility(View.VISIBLE);
            }

            //getActivity().startService(new Intent(getActivity(), MessageService.class));
            //Intent intent = new Intent("action.update.profile");
            // You can also include some extra data.
            //intent.putExtra("message", "This is my message!");
            //LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
            try {
                ((DashboardActivity) getActivity()).updateinfofrombottomsheet();
            } catch (Exception e) {
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

//    public void functionGetUserProfile(final String userid) {
//        new VolleyApiRequest(context, false, VolleyApiRequest.REQUEST_BASEURL + "api/get_user_detail/" + userid,
//                new VolleyCallBack() {
//                    @Override
//                    public void onResponse(JSONObject response) {
//                        try {
//                            Log.e("Result", "" + response);
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                        }
//                    }
//
//                    @Override
//                    public void onErrorResponse(VolleyError error) {
//                    }
//                }).enqueRequest(null, Request.Method.GET);
//    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK && requestCode == Constants.RCODE_UPDATEEDUCATION) {
            parseResponse();
        }
        if (resultCode != RESULT_CANCELED) {
            if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_SELECT_PICTURE) {
                fileUri = data.getData();
                file = new File(fileUri.getPath());
                if (fileUri != null) {
                    try {
                        String compressImg = new CompressImage(context).compressImage("" + fileUri);
                        Uri compressUri = Uri.fromFile(new File(compressImg));
                        startCropActivity(compressUri);
                    } catch (Exception e) {
                        e.printStackTrace();
                        startCropActivity(fileUri);
                    }
                } else {
                    Utilities.showToast(context, "" + R.string.toast_cannot_retrieve_selected_image);
                }
            } else if (resultCode == Activity.RESULT_OK && requestCode == PICK_FROM_CAMERA) {
                if (fileUri != null) {
                    startCropActivity(fileUri);
                } else {
                    Utilities.showToast(context, "" + R.string.toast_cannot_retrieve_selected_image);
                }
            } else if (requestCode == UCrop.REQUEST_CROP) {
                handleCropResult(data);
            }
        }
    }

    private void pickFromGallery() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN
                && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                    REQUEST_STORAGE_READ_ACCESS_PERMISSION);

        } else {
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            intent.addCategory(Intent.CATEGORY_OPENABLE);
            startActivityForResult(Intent.createChooser(intent, getString(R.string.label_select_picture)), REQUEST_SELECT_PICTURE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_STORAGE_READ_ACCESS_PERMISSION:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    pickFromGallery();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private void startCropActivity(@NonNull Uri uri) {

        String destinationFileName = "" + System.currentTimeMillis();
        destinationFileName += ".png";

        UCrop uCrop = UCrop.of(uri, Uri.fromFile(new File(context.getCacheDir(), destinationFileName)));
        uCrop.withAspectRatio(1, 1);
        uCrop.start(getActivity());

    }

    private boolean checkAndRequestPermissions() {
        int permissionSendMessage = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA);
        int locationPermission = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE);
        List<String> listPermissionsNeeded = new ArrayList<>();
        if (locationPermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (permissionSendMessage != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CAMERA);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(getActivity(), listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), REQUEST_ID_MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }

    private void handleCropResult(@NonNull Intent result) {

        if (result != null) {
            final Uri resultUri = UCrop.getOutput(result);
            if (resultUri != null) {

                try {
                    final InputStream imageStream = getActivity().getContentResolver().openInputStream(resultUri);
                    selectedImage = BitmapFactory.decodeStream(imageStream);
                    int orientation = Utilities.getCameraPhotoOrientation(context, resultUri, file.getPath());
                    img_basic_profile_profile_pic.setImageBitmap(selectedImage);
                    img_basic_profile_profile_pic.setRotation(orientation);

                    ModelLoggedUser modelLoggedUser = qkPreferences.getLoggedUser();
                    if (modelLoggedUser.getData() != null && modelLoggedUser.getData().getBasicDetail() != null) {
                        BasicDetail basicDetail = modelLoggedUser.getData().getBasicDetail();

                        String firstname = basicDetail.getFirstname();
                        String lastname = basicDetail.getLastname();
                        String stories = basicDetail.getQkStory() + "";

                        if (selectedImage == null)
                            strBase64 = "";
                        else {
                            ByteArrayOutputStream stream = new ByteArrayOutputStream();
                            selectedImage.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                            byte[] byteArray = stream.toByteArray();
                            strBase64 = Base64.encodeToString(byteArray, 0);
                            Log.d("ss", strBase64);
                        }
                        if (!Utilities.isNetworkConnected(getActivity()))
                            Utilities.showToast(context, Constants.NO_INTERNET_CONNECTION);
                        else
                            functionUpdateProfile(userid, firstname, lastname, stories, strBase64);

                    }
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }

            } else {
                Utilities.showToast(getActivity(), "" + R.string.toast_cannot_retrieve_cropped_image);
            }
        }

    }

    public void functionUpdateProfile(final String user_id, String firstname, String lastname, final String qk_story, String profile_pic) {

        JSONObject obj = new JSONObject();
        try {
            obj.put("user_id", user_id);
            obj.put("firstname", firstname);
            obj.put("lastname", lastname);
            obj.put("qk_story", qk_story);
            obj.put("profile_picture", profile_pic);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        new VolleyApiRequest(context, true, VolleyApiRequest.REQUEST_BASEURL + "api/change_profile_pic",
                new VolleyCallBack() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String status = response.getString("status");
                            String message = response.getString("msg");
                            if (status.equals("200")) {

                                JSONObject object = response.getJSONObject("data");

                                ModelLoggedUser modelUserProfile = qkPreferences.getLoggedUser();

                                BasicDetail basicDetail = modelUserProfile.getData().getBasicDetail();

                                basicDetail.setFirstname(object.getString("firstname"));
                                basicDetail.setLastname(object.getString("lastname"));
                                basicDetail.setQkStory(object.getString("qk_story"));
                                basicDetail.setProfilePic(object.getString("profile_pic"));

                                modelUserProfile.getData().setName(object.getString("firstname") + " " + object.getString("lastname"));
                                modelUserProfile.getData().setBasicDetail(basicDetail);
                                modelUserProfile.getData().setProfilePic(object.getString("profile_pic"));
                                String ProfilePic = object.getString("profile_pic") + "";

                                qkPreferences.storeLoggedUser(modelUserProfile);
                                String unique_code = qkPreferences.getValues(QKPreferences.USER_UNIQUECODE);

                                try {
                                    ((DashboardActivity) getActivity()).UpdateInfo();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                                MyProfiles contacts = RealmController.with(getActivity()).getMyProfielDataByProfileId(unique_code);
                                if (contacts != null) {
                                    MyProfiles update_contact = RealmController.getRealmtoRealmMyProfiles(contacts);
                                    update_contact.setName(object.getString("firstname") + " " + object.getString("lastname"));
                                    RealmController.with(getActivity()).updateMyProfiles(update_contact);
                                }
                            } else
                                Utilities.showToast(context, "" + message);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyApiRequest.showVolleyError(context, error);
                    }
                }).enqueRequest(obj, Request.Method.POST);

    }


    public void getPhotoFromCamera() {
        int currentapiVersion = Build.VERSION.SDK_INT;
        if (currentapiVersion >= Build.VERSION_CODES.M) {
            if (checkAndRequestPermissions())
                launchCamera();

        } else
            launchCamera();
    }

    public void launchCamera() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        file = new File(getActivity().getExternalCacheDir(), String.valueOf(System.currentTimeMillis()) + ".jpg");

        fileUri = Uri.fromFile(file);


        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
            intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
        } else {
            file = new File(file.getPath());
            Uri photoUri = FileProvider.getUriForFile(getActivity(), getActivity().getPackageName() + ".provider", file);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
        }
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
            startActivityForResult(intent, PICK_FROM_CAMERA);
        }
    }

}
