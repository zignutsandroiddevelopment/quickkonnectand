package com.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.adapter.FollowerAdapter;
import com.models.profilesfollower.Datum;
import com.models.profilesfollower.ProfileFollowerPojo;
import com.quickkonnect.PojoClasses;
import com.quickkonnect.R;
import com.realmtable.Followers;
import com.realmtable.RealmController;
import com.retrofitservice.ApiUtils;
import com.retrofitservice.SOService;
import com.utilities.QKPreferences;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.realm.RealmResults;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Frag_Followers extends Fragment {

    private View rootView;
    private SOService mService;
    private String profile_id;
    private QKPreferences qkPreferences;
    private FollowerAdapter adapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private Context context;
    private ArrayList<PojoClasses.FollowersList> mlist;
    private RecyclerView recyclerView;
    private TextView no_recored_found;
    private EditText edtContactSearch_followers;

    public static Fragment getInstance(int position, Bundle bundle) {
        bundle.putInt("pos", position);
        Frag_Followers frag_followers = new Frag_Followers();
        frag_followers.setArguments(bundle);
        return frag_followers;
    }

    public static Frag_Followers newInstance(int position, Bundle bundle) {
        bundle.putInt("pos", position);
        Frag_Followers frag_followers = new Frag_Followers();
        frag_followers.setArguments(bundle);
        return frag_followers;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.frag__followers, container, false);

        context = getActivity();

        final Bundle extras = getArguments();

        qkPreferences = new QKPreferences(getActivity());

        mService = ApiUtils.getSOService();

        profile_id = extras.getString("user_id");

        recyclerView = rootView.findViewById(R.id.recyclerView_follower);
        no_recored_found = rootView.findViewById(R.id.tv_no_record_found_followers);
        edtContactSearch_followers = rootView.findViewById(R.id.edtContactSearch_followers);

        mLayoutManager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(mLayoutManager);

        edtContactSearch_followers.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override

            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                edtContactSearch_followers.setCursorVisible(true);
                if (adapter != null) {
                    filter(editable.toString());
                }
            }
        });

        recyclerView.setAdapter(adapter);

        showFollowers(true);

        return rootView;
    }

    private void filter(String text) {
        ArrayList<PojoClasses.FollowersList> filterdNames = new ArrayList<>();
        for (PojoClasses.FollowersList s : mlist) {
            if (s.getEmail().equalsIgnoreCase(text) || s.getName().equalsIgnoreCase(text) || s.getName().contains(text) || s.getName().contains(text.toLowerCase()) || s.getName().contains(text.toUpperCase()) || s.getEmail().contains(text.toLowerCase()) || s.getEmail().contains(text.toUpperCase())) {
                filterdNames.add(s);
            }
        }
        adapter.filterList(filterdNames);
    }

    public void showFollowers(boolean isReload) {

       /* if (profile_id.equalsIgnoreCase("") || profile_id.equalsIgnoreCase("null") || profile_id.equals(null))
            profile_id = qkPreferences.getProfileID() + "";*/

        RealmResults<Followers> followers = RealmController.with(getActivity()).getFollowersbyProfileId(profile_id);
        if (followers != null && followers.size() > 0) {

            mlist = new ArrayList<>();

            for (Followers foloo1 : followers) {

                mlist.add(new PojoClasses.FollowersList(
                        foloo1.getUser_id(),
                        foloo1.getName(),
                        foloo1.getEmail(),
                        foloo1.getUnique_code(),
                        foloo1.getProfile_pic()));
            }


            if (mlist != null && mlist.size() > 0) {
                no_recored_found.setVisibility(View.GONE);
                recyclerView.setVisibility(View.VISIBLE);

            } else {
                no_recored_found.setVisibility(View.VISIBLE);
                recyclerView.setVisibility(View.GONE);

            }

            adapter = new FollowerAdapter(getActivity(), mlist);
            recyclerView.setAdapter(adapter);

        } else {
            no_recored_found.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        }

        if (isReload) {
            getFollowerList();
        }
    }

    private void getFollowerList() {
        String token = "";
        if (qkPreferences != null) {
            token = qkPreferences.getApiToken() + "";
        }

        Map<String, String> params = new HashMap<>();
        params.put("Authorization", "Bearer " + token + "");
        params.put("Accept", "application/json");
        params.put("QK_ACCESS_KEY", getResources().getString(R.string.QK_ACCESS_KEY)+"");

        mService.getCompanyFollowersList(params,"follower", profile_id, qkPreferences.getValuesInt(QKPreferences.USER_ID)).enqueue(new Callback<ProfileFollowerPojo>() {
            @Override
            public void onResponse(Call<ProfileFollowerPojo> call, Response<ProfileFollowerPojo> response) {
                Log.e("Pro-Followe:Success", "" + response.body());
                if (response != null && response.body() != null) {
                    ProfileFollowerPojo profileFollowerPojo = response.body();
                    if (profileFollowerPojo != null) {
                        if (profileFollowerPojo.getStatus() == 200) {

                            List<Datum> datumList = profileFollowerPojo.getData();
                            if (datumList != null && datumList.size() > 0) {
                                for (Datum datum : datumList) {

                                    Followers followers = new Followers();

                                    followers.setUser_id("" + datum.getUserId());
                                    followers.setName(datum.getName());
                                    followers.setEmail(datum.getEmail());
                                    followers.setUnique_code(datum.getUniqueCode());
                                    followers.setProfile_pic(datum.getProfilePic());
                                    followers.setProfile_id(profile_id);

                                    RealmController.with(getActivity()).updateFollowers(followers);
                                }
                            }
                        }

                    } else
                        Log.e("Pro-Follower:Error", "Error");

                } else
                    Log.e("Pro-Follower:Error", "Error");

                showFollowers(false);
            }

            @Override
            public void onFailure(Call<ProfileFollowerPojo> call, Throwable t) {
                Log.e("Pro-Follower:Error", "" + t.getLocalizedMessage());
            }
        });
    }
}
