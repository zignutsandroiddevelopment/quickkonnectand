package com.fragments;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.activities.ManageQKProfilesActivity;
import com.activities.MatchProfileActivity;
import com.adapter.NotificationAdapter;
import com.adapter.RecyclerItemTouchHelper;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.interfaces.CallBackGetResult;
import com.interfaces.CallBackReadNoti;
import com.interfaces.OnFragLoad;
import com.interfaces.OnSuccessError;
import com.models.ModelNotifications;
import com.models.logindata.ModelLoggedUser;
import com.quickkonnect.CircleTransform;
import com.quickkonnect.DashboardActivity;
import com.quickkonnect.EmployeesProfile;
import com.quickkonnect.NewScanProfileActivity;
import com.quickkonnect.R;
import com.realmtable.Notifications;
import com.realmtable.RealmController;
import com.retrofitservice.ApiUtils;
import com.retrofitservice.SOService;
import com.squareup.picasso.Picasso;
import com.utilities.Constants;
import com.utilities.QKPreferences;
import com.utilities.Utilities;
import com.volleyapirequest.AsynTask;
import com.volleyapirequest.VolleyApiRequest;
import com.volleyapirequest.VolleyCallBack;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ZTLAB09 on 12-12-2017.
 */

public class FragNotifications extends Fragment implements View.OnClickListener, RecyclerItemTouchHelper.RecyclerItemTouchHelperListener {

    static Context context;
    private View rootView;
    private SOService mSoService;
    private QKPreferences qkPreferences;
    static private String userid;
    private RecyclerView recyclerView_notification;
    private TextView tv_no_record_found;
    private RecyclerView.LayoutManager mLayoutManager;
    private static NotificationAdapter notificationAdapter;
    public static ArrayList<ModelNotifications> modelNotifications1;
    private int pageNumber;
    private SwipeRefreshLayout swipeRefreshLayout;
    //    private int visibleThreshold = 5;
//    private int lastVisibleItem, totalItemCount;
    private boolean isLoading, isNext = true;
    public static final int NOTIFICATION_DOWNLOAD = 101;
    public static final int NOTIFICATION_REFRESH = 10101;
    private OnFragLoad onFragLoad;
    private ImageView img_profile_pic, img_notification;
    public static TextView tvactionbarTitle;

    public final String BROADCAST_ACTION = "NOTIFICATION_CHANGE";
    public MyNewBroadCastReceiver myBroadCastReceiver;

    public static FragNotifications getInstance(OnFragLoad onFragLoad) {
        FragNotifications fragChats = new FragNotifications();
        fragChats.onFragLoad = onFragLoad;
        return fragChats;
    }

    private class MyNewBroadCastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                showNotifications();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.frag_notifications, null);

        context = getActivity();

        qkPreferences = QKPreferences.getInstance(context);
        mSoService = ApiUtils.getSOService();

        userid = "" + qkPreferences.getValuesInt(QKPreferences.USER_ID);

        tv_no_record_found = rootView.findViewById(R.id.tv_no_record_found);
        recyclerView_notification = rootView.findViewById(R.id.recyclerView_notification);
        //recyclerView_notification.addItemDecoration(new SimpleDividerItemDecoration(getActivity()));
        recyclerView_notification.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView_notification.setLayoutManager(mLayoutManager);
        swipeRefreshLayout = rootView.findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary,
                android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_blue_dark);

        img_profile_pic = rootView.findViewById(R.id.img_profile_pic);
        img_notification = rootView.findViewById(R.id.img_notification);
        tvactionbarTitle = rootView.findViewById(R.id.tvactionbarTitle);
        img_notification.setOnClickListener(this);
        myBroadCastReceiver = new MyNewBroadCastReceiver();

        pageNumber = 1;

        img_profile_pic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    DashboardActivity.showProfileinfo();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        showNotifications();

        ItemTouchHelper.SimpleCallback itemTouchHelperCallback = new RecyclerItemTouchHelper(0, ItemTouchHelper.LEFT, this);
        new ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(recyclerView_notification);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                try {
                    isLoading = true;
                    pageNumber = 1;
                    getUserNotification(userid, pageNumber, false, true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        try {
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(BROADCAST_ACTION);
            getActivity().registerReceiver(myBroadCastReceiver, intentFilter);
        } catch (Exception e) {
            e.printStackTrace();
        }

        updateInfo();

        return rootView;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            getActivity().unregisterReceiver(myBroadCastReceiver);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void showNotifications() {

        try {
            modelNotifications1 = new ArrayList<>();

            notificationAdapter = new NotificationAdapter(context, modelNotifications1, callBackReadNoti, recyclerView_notification);
            recyclerView_notification.setAdapter(notificationAdapter);

            List<Notifications> notificationsList = RealmController.with(getActivity()).getNotifications();

            if (notificationsList != null && notificationsList.size() > 0) {

                for (Notifications notifications: notificationsList) {

                    ModelNotifications modelNotifications = new ModelNotifications();

                    modelNotifications.setNotificationId(Integer.parseInt(notifications.getNotification_id()));
                    modelNotifications.setNotification_type(notifications.getNotification_type());
                    modelNotifications.setSenderId(Integer.parseInt(notifications.getSender_id()));
                    modelNotifications.setUsername(notifications.getUsername());
                    modelNotifications.setUserEmail(notifications.getUser_email());
                    modelNotifications.setProfile_pic(notifications.getProfile_pic());
                    modelNotifications.setStatus(notifications.getStatus());
                    modelNotifications.setIsRead(Integer.parseInt(notifications.getIs_read()));
                    modelNotifications.setCreatedTime(notifications.getCreated_time());
                    modelNotifications.setUniqueCode(notifications.getUnique_code());
                    modelNotifications.setMessage(notifications.getMessage());
                    modelNotifications.setProfile_id(notifications.getProfile_id());
                    modelNotifications.setProfile_name(notifications.getProfile_name());

                    modelNotifications1.add(modelNotifications);
                }
                if (notificationAdapter != null) {
                    notificationAdapter.notifyDataSetChanged();
                }
            }
            getUserNotification(userid, pageNumber, false, false);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // Get Notification List
    public void getUserNotification(String user_id, int page_no, final boolean isLoadmore, boolean isSwipe) {
        try {
            swipeRefreshLayout.setRefreshing(isSwipe);
            String timezone = "?timezone=" + TimeZone.getDefault().getID();

            new VolleyApiRequest(context, false, VolleyApiRequest.REQUEST_BASEURL + "api/notification_list/" + user_id + "/" + page_no + timezone,
                    new VolleyCallBack() {
                        @Override
                        public void onResponse(JSONObject response) {
                            swipeRefreshLayout.setRefreshing(false);
                            try {
                                RealmController.with(getActivity()).clearNotifications();
                                parseResponse(response, isLoadmore);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onErrorResponse(VolleyError error) {
                            swipeRefreshLayout.setRefreshing(false);
                            //VolleyApiRequest.showVolleyError(context, error);
                        }
                    }).enqueRequest(null, Request.Method.GET);
        } catch (Exception e) {
            if (isSwipe)
                swipeRefreshLayout.setRefreshing(false);
            e.printStackTrace();
        }
    }

    public void parseResponse(JSONObject response, boolean isLoadmore) {
        try {

            isLoading = false;

            String status = response.getString("status");
            String message = response.getString("msg");

            if (status.equals("200")) {

                JSONArray notilist = response.getJSONArray("data");

                if (notilist != null && notilist.length() > 0) {

                    Type type = new TypeToken<List<ModelNotifications>>() {
                    }.getType();

                    ArrayList<ModelNotifications> modelNotifications = new Gson().fromJson(notilist.toString(), type);

                    if (!isLoadmore) {

                        if (modelNotifications != null && modelNotifications.size() > 0) {
                            createUpdatedata(modelNotifications);
                        } else {
                            tv_no_record_found.setVisibility(View.VISIBLE);
                            recyclerView_notification.setVisibility(View.GONE);
                        }

                        if (modelNotifications != null && (modelNotifications.size() == 0 || modelNotifications.size() < 20))
                            isNext = false;

                    } else {

                        if (modelNotifications != null && modelNotifications.size() > 0) {
                            createUpdatedata(modelNotifications);
                        }

                        if (modelNotifications != null && (modelNotifications.size() == 0 || modelNotifications.size() < 20))
                            isNext = false;
                    }

                    if (modelNotifications != null && modelNotifications.size() == 0) {
                        qkPreferences.storeUnreadNotification("0");
                    }

                    if (onFragLoad != null) {
                        onFragLoad.onFragLoad(FragNotifications.this);
                    }

                } else {
                    if (notificationAdapter == null || (notificationAdapter != null && notificationAdapter.getItemCount() == 0)) {
                        tv_no_record_found.setVisibility(View.VISIBLE);
                        tv_no_record_found.setText(message);
                        recyclerView_notification.setVisibility(View.GONE);
                    }
                }

            } else if (status.equals("400")) {
                if (notificationAdapter == null || (notificationAdapter != null && notificationAdapter.getItemCount() == 0)) {
                    tv_no_record_found.setVisibility(View.VISIBLE);
                    tv_no_record_found.setText(message);
                    recyclerView_notification.setVisibility(View.GONE);
                }
            } else {
                if (notificationAdapter == null || (notificationAdapter != null && notificationAdapter.getItemCount() == 0)) {
                    tv_no_record_found.setVisibility(View.VISIBLE);
                    tv_no_record_found.setText(message);
                    recyclerView_notification.setVisibility(View.GONE);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void createUpdatedata(ArrayList<ModelNotifications> notificationsArrayList) {

        RealmController.with(getActivity()).clearNotifications();
        for (ModelNotifications notifications: notificationsArrayList) {
            RealmController.with(getActivity()).createUpdateNotifications(notifications);
        }

        List<Notifications> notificationsList = RealmController.with(getActivity()).getNotifications();

        tv_no_record_found.setVisibility(View.GONE);
        recyclerView_notification.setVisibility(View.VISIBLE);

        if (notificationsList != null && notificationsList.size() > 0) {
            modelNotifications1.clear();
            for (Notifications notifications: notificationsList) {

                ModelNotifications modelNotifications = new ModelNotifications();

                modelNotifications.setNotificationId(Integer.parseInt(notifications.getNotification_id()));
                modelNotifications.setNotification_type(notifications.getNotification_type());
                modelNotifications.setSenderId(Integer.parseInt(notifications.getSender_id()));
                modelNotifications.setProfile_id(notifications.getProfile_id());
                modelNotifications.setUsername(notifications.getUsername());
                modelNotifications.setUserEmail(notifications.getUser_email());
                modelNotifications.setProfile_pic(notifications.getProfile_pic());
                modelNotifications.setStatus(notifications.getStatus());
                modelNotifications.setIsRead(Integer.parseInt(notifications.getIs_read()));
                modelNotifications.setCreatedTime(notifications.getCreated_time());
                modelNotifications.setUniqueCode(notifications.getUnique_code());
                modelNotifications.setMessage(notifications.getMessage());
                modelNotifications.setProfile_name(notifications.getProfile_name());

                modelNotifications1.add(modelNotifications);
            }

            if (notificationAdapter != null) {
                notificationAdapter.notifyDataSetChanged();
            }
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.img_notification:
                if (modelNotifications1 != null && modelNotifications1.size() > 0) {
                    Utilities.showTermsCondpopUp(getActivity(), "Alert!", "Are you sure you want to delete all previous notifications?",
                            new CallBackGetResult() {
                                @Override
                                public void onSuccess() {
                                    if (!Utilities.isNetworkConnected(context))
                                        Toast.makeText(context, Constants.NO_INTERNET_CONNECTION, Toast.LENGTH_LONG).show();
                                    else
                                        deleteAllNotification();
                                }

                                @Override
                                public void onCancel() {
                                }
                            });
                }
                break;
            case R.id.recyclerView_notification:

                break;
        }
    }

    // Delete All Notification
    public void deleteAllNotification() {
        new VolleyApiRequest(context, true, VolleyApiRequest.REQUEST_BASEURL + "api/clear_notifications/" + userid,
                new VolleyCallBack() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {
                            String status = response.getString("status");
                            String message = response.getString("msg");
                            if (status.equals("200")) {
                                modelNotifications1.clear();
                                notificationAdapter.notifyDataSetChanged();
                                RealmController.with(getActivity()).clearNotifications();
                                isLoading = true;
                                pageNumber = 1;
                                getUserNotification(userid, pageNumber, false, true);
                            } else {
                                Utilities.showToast(context, message);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyApiRequest.showVolleyError(context, error);
                    }
                }).enqueRequest(null, Request.Method.GET);
    }

    // Delete Single Notification
    public static void deleteSignleNotification(String notficationId) {
        new VolleyApiRequest(context, false, VolleyApiRequest.REQUEST_BASEURL + "api/delete_notification/" + notficationId,
                new VolleyCallBack() {
                    @Override
                    public void onResponse(JSONObject response) {
//                        try {
//                            String message = response.getString("msg");
//                            Utilities.showToast(context, message);
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
                    }

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // VolleyApiRequest.showVolleyError(context, error);
                    }
                }).enqueRequest(null, Request.Method.GET);
    }

    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction, int position) {
        final int noti_id = modelNotifications1.get(viewHolder.getAdapterPosition()).getNotificationId();
        deleteSignleNotification(noti_id + "");
        notificationAdapter.removeItem(viewHolder.getAdapterPosition());
    }

    //Read Notification
    public void readNotification(final boolean isIntent, int notificationID, final int pos, final ModelNotifications modelNotifications) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("notification_ids", notificationID);
            jsonObject.put("status", "1");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        new VolleyApiRequest(context, false, VolleyApiRequest.REQUEST_BASEURL + "api/read_notifications",
                new VolleyCallBack() {
                    @Override

                    public void onResponse(JSONObject response) {
                        if (isIntent)
                            onClickNotification(modelNotifications, pos);
                    }

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // VolleyApiRequest.showVolleyError(context, error);
                    }
                }).enqueRequest(jsonObject, Request.Method.POST);
    }

    CallBackReadNoti callBackReadNoti = new CallBackReadNoti() {
        @Override
        public void onLoadmore() {
            try {
                if (isNext) {
                    isNext = false;
                    isLoading = true;
                    pageNumber += 1;
                    getUserNotification(userid, pageNumber, true, false);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onAccept(final ModelNotifications modelNotifications, final int pos) {
            if (modelNotifications.getNotification_type().equalsIgnoreCase(Constants.NOTIFICATION_SCAN_CONTREQ)) {
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle(getResources().getString(R.string.alert));
                builder.setMessage(getResources().getString(R.string.suretoapproverequ));
                builder.setPositiveButton(getResources().getString(R.string.approve), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        approveScanRequest(modelNotifications.getUniqueCode(), pos);
                    }
                });
                builder.setNegativeButton(getResources().getString(R.string.close), null);
                builder.show();

            } else if (modelNotifications.getNotification_type().equals(Constants.NOTIFICATION_TYPE_CONTREQ)
                    && modelNotifications.getStatus().equals(Constants.NOTI_PENDING)) {

                try {
                    ((DashboardActivity) getActivity()).showAcceptReject(modelNotifications, pos);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }

        @Override
        public void onReject(final ModelNotifications modelNotifications, final int pos) {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle(getResources().getString(R.string.alert));
            builder.setMessage(getResources().getString(R.string.suretorejectrequ));
            builder.setPositiveButton(getResources().getString(R.string.reject), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    if (modelNotifications.getNotification_type().equalsIgnoreCase(Constants.NOTIFICATION_TYPE_CONTREQ)) {
                        try {
                            new AsynTask(context, new OnSuccessError() {
                                @Override
                                public void onSuccess(JSONObject object) {
                                    refreshNotifications(pos);
                                    readNotification(false, modelNotifications.getNotificationId(), pos, modelNotifications);
                                }

                                @Override
                                public void onCancel(String error_message) {
                                    Utilities.showToast(context, error_message);
                                }
                            }).fnRejectContact(qkPreferences.getLoggedUserid(), "" + modelNotifications.getSenderId(), modelNotifications.getNotificationId());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    } else if (modelNotifications.getNotification_type().equalsIgnoreCase(Constants.NOTIFICATION_TYPE_TRANSFER)) {
                        AcceptRejectTransferProfile("R", modelNotifications.getNotificationId() + "", modelNotifications, pos);

                    } else if (modelNotifications.getNotification_type().equalsIgnoreCase(Constants.NOTIFICATION_TYPE_EMPLOYY_REQUEST)) {
                        ViweRejectionDialog(modelNotifications.getProfile_name(), modelNotifications.getProfile_id(), modelNotifications, pos);

                    } else if (modelNotifications.getNotification_type().equalsIgnoreCase(Constants.NOTIFICATION_SCAN_CONTREQ)) {
                        rejectScanRequest(modelNotifications.getSenderId(), modelNotifications.getNotificationId(), pos);
                    }
                }
            });
            builder.setNegativeButton(getResources().getString(R.string.close), null);
            builder.show();
        }

        @Override
        public void actionNotification(ModelNotifications modelNotifications, int pos) {
            if (modelNotifications.getIsRead() == 0) {
                if (Utilities.isNetworkConnected(context))
                    readNotification(true, modelNotifications.getNotificationId(), pos, modelNotifications);
                else {
                    onClickNotification(modelNotifications, pos);
                }

            } else {
                CallIntent(modelNotifications, pos);
            }
        }
    };


    public void onClickNotification(ModelNotifications modelNotifications, int pos) {

        ModelNotifications notifications = modelNotifications1.get(pos);
        notifications.setIsRead(1);
        modelNotifications1.set(pos, notifications);
        if (notificationAdapter != null) {
            notificationAdapter.notifyDataSetChanged();
        }

        String Unread_noti = qkPreferences.getNotificationInfo();
        try {
            if (Integer.parseInt(Unread_noti) > 0) {
                int total_count = Integer.parseInt(Unread_noti) - 1;
                qkPreferences.storeUnreadNotification("" + total_count);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (modelNotifications.getStatus().equals(Constants.NOTI_PENDING) && modelNotifications.getNotification_type().equals(Constants.NOTIFICATION_TYPE_CONTREQ)) {

            ((DashboardActivity) getActivity()).showAcceptReject(modelNotifications, pos);

        } else if (modelNotifications.getNotification_type().equals(Constants.NOTIFICATION_TYPE_PROFAPPROVED) || modelNotifications.getNotification_type().equals(Constants.NOTIFICATION_TYPE_PROFREJECT)) {
            //Intent intent = new Intent(context, ScanProfileDetailActivity.class);
            /*Intent intent = new Intent(context, NewScanProfileActivity.class);
            intent.putExtra("unique_code", "" + modelNotifications.getUniqueCode());
            intent.putExtra("profile_id", "" + modelNotifications.getProfile_id());
            intent.putExtra("notification_id", "" + modelNotifications.getNotificationId());
            intent.putExtra("user_id", "" + modelNotifications.getSenderId());
            intent.putExtra(Constants.KEY, Constants.KEY_FROM_MANAGEPROFILES);
            startActivity(intent);*/
            Intent intent = new Intent(context, ManageQKProfilesActivity.class);
            startActivity(intent);

        } else if ((modelNotifications.getStatus().equals(Constants.NOTI_APPRVOED)
                && modelNotifications.getNotification_type().equals(Constants.NOTIFICATION_TYPE_APPROVED))
                || modelNotifications.getNotification_type().equals(Constants.NOTIFICATION_TYPE_SCAN)) {

            Intent intent = new Intent(context, MatchProfileActivity.class);
            intent.putExtra(Constants.KEY, Constants.KEY_OTHER);
            intent.putExtra("unique_code", "" + modelNotifications.getUniqueCode());
            startActivity(intent);

        } else
            CallIntent(modelNotifications, pos);

    }

    private void CallIntent(ModelNotifications modelNotifications, int pos) {

        if (modelNotifications.getNotification_type().equals(Constants.NOTIFICATION_TYPE_APPROVED)
                || modelNotifications.getNotification_type().equals(Constants.NOTIFICATION_SCAN_REJECTED)
                || modelNotifications.getNotification_type().equals(Constants.NOTIFICATION_TYPE_SCAN)
                || modelNotifications.getNotification_type().equals(Constants.NOTIFICATION_SCAN_APRROVED)
                || (modelNotifications.getNotification_type().equals(Constants.NOTIFICATION_TYPE_CONTREQ)
                && modelNotifications.getStatus().equals(Constants.NOTI_APPRVOED))) {

            Intent intent = new Intent(context, MatchProfileActivity.class);
            intent.putExtra(Constants.KEY, Constants.KEY_OTHER);
            intent.putExtra("unique_code", "" + modelNotifications.getUniqueCode());
            startActivity(intent);

        } else if (modelNotifications.getNotification_type().equals(Constants.NOTIFICATION_TYPE_PROFAPPROVED)) {

           /* Intent intent = new Intent(context, NewScanProfileActivity.class);
            intent.putExtra("unique_code", "" + modelNotifications.getUniqueCode());
            intent.putExtra("profile_id", "" + modelNotifications.getProfile_id());
            intent.putExtra("notification_id", "" + modelNotifications.getNotificationId());
            intent.putExtra("user_id", "" + modelNotifications.getSenderId());
            intent.putExtra(Constants.KEY, Constants.KEY_FROM_MANAGEPROFILES);
            startActivity(intent);*/
            Intent intent = new Intent(context, ManageQKProfilesActivity.class);
            startActivity(intent);

        } else if (modelNotifications.getNotification_type().equals(Constants.NOTIFICATION_TYPE_PROFREJECT)) {

            /*Intent intent = new Intent(context, NewScanProfileActivity.class);
            intent.putExtra("unique_code", "" + modelNotifications.getUniqueCode());
            intent.putExtra("profile_id", "" + modelNotifications.getProfile_id());
            intent.putExtra("notification_id", "" + modelNotifications.getNotificationId());
            intent.putExtra("user_id", "" + modelNotifications.getSenderId());
            intent.putExtra(Constants.KEY, Constants.KEY_FROM_MANAGEPROFILES);
            startActivity(intent);*/
            Intent intent = new Intent(context, ManageQKProfilesActivity.class);
            startActivity(intent);

        }

//        else if (modelNotifications.getNotification_type().equals(Constants.NOTIFICATION_TYPE_CONTREQ)
//                && modelNotifications.getStatus().equals(Constants.NOTI_PENDING)) {
//
//            try {
//                ((DashboardActivity) getActivity()).showAcceptReject(modelNotifications, pos);
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//
//        }

        else if (modelNotifications.getNotification_type().equals(Constants.NOTIFICATION_TYPE_TRANSFER)) {

            Intent intent = new Intent(context, NewScanProfileActivity.class);
            intent.putExtra("unique_code", "" + modelNotifications.getUniqueCode());
            intent.putExtra("profile_id", "" + modelNotifications.getProfile_id());
            intent.putExtra("notification_id", "" + modelNotifications.getNotificationId());
            intent.putExtra("user_id", "" + modelNotifications.getSenderId());
            intent.putExtra(Constants.KEY, Constants.KEY_FROM_NOTIFICATIONS);
            startActivityForResult(intent, NOTIFICATION_REFRESH);

        }

//        else if (modelNotifications.getNotification_type().equals(Constants.NOTIFICATION_SCAN_REJECTED)) {
//
//            startActivity(new Intent(context, UnScanUsersActivity.class)
//                    .putExtra("scan_result", Constants.CONTACT_REJECTED)
//                    .putExtra("unique_code", modelNotifications.getUniqueCode()));
//
//        }

        else if (modelNotifications.getNotification_type().equals(Constants.NOTIFICATION_TYPE_TRANSFER_REJECT)) {

            Intent intent = new Intent(context, NewScanProfileActivity.class);
            intent.putExtra("unique_code", "" + modelNotifications.getUniqueCode());
            intent.putExtra("profile_id", "" + modelNotifications.getProfile_id());
            intent.putExtra("notification_id", "" + modelNotifications.getNotificationId());
            intent.putExtra("user_id", "" + modelNotifications.getSenderId());
            intent.putExtra(Constants.KEY, "");
            startActivity(intent);

        } else if (modelNotifications.getNotification_type().equals(Constants.NOTIFICATION_TYPE_EMPLOYY_REQUEST)) {

            //deleteSignleNotification(notificationId);
            Intent intent = new Intent(context, EmployeesProfile.class);
            intent.putExtra("KEY", "" + "NOTIFICATION");
            intent.putExtra("notification_id", "" + modelNotifications.getNotificationId());
            startActivityForResult(intent, NOTIFICATION_REFRESH);

        } else if (modelNotifications.getNotification_type().equals(Constants.NOTIFICATION_TYPE_EMPLOYY_ACCEPT)) {

            /*Intent intent = new Intent(context, EmployeesProfile.class);
            intent.putExtra("KEY", "" + "NOTIFICATION_AR");
            intent.putExtra("notification_id", "" + modelNotifications.getNotificationId());

            startActivity(intent);*/

        } else if (modelNotifications.getNotification_type().equals(Constants.NOTIFICATION_TYPE_EMPLOYY_REJECT)) {

            Intent intent = new Intent(context, EmployeesProfile.class);
            intent.putExtra("KEY", "" + "NOTIFICATION_AR");
            intent.putExtra("notification_id", "" + modelNotifications.getNotificationId());
            startActivity(intent);

        } else if (modelNotifications.getNotification_type().equals(Constants.NOTIFICATION_TYPE_CHANGE_ROLE)) {

            Intent intent = new Intent(context, NewScanProfileActivity.class);
            intent.putExtra("unique_code", "" + modelNotifications.getUniqueCode());
            intent.putExtra("profile_id", "" + modelNotifications.getProfile_id());
            intent.putExtra("notification_id", "" + modelNotifications.getNotificationId());
            intent.putExtra("user_id", "" + modelNotifications.getSenderId());
            intent.putExtra(Constants.KEY, "");
            startActivity(intent);
        } else if (modelNotifications.getNotification_type().equals(Constants.NOTIFICATION_TYPE_FOLLOW)) {

            Intent intent = new Intent(context, NewScanProfileActivity.class);
            intent.putExtra("unique_code", "" + modelNotifications.getUniqueCode());
            intent.putExtra("profile_id", "" + modelNotifications.getProfile_id());
            intent.putExtra("notification_id", "" + modelNotifications.getNotificationId());
            intent.putExtra("user_id", "" + modelNotifications.getSenderId());
            intent.putExtra(Constants.KEY, Constants.KEY_FROM_MANAGEPROFILES);
            startActivity(intent);
        }
    }

    public void refreshNotifications(int pos) {
        try {

            modelNotifications1.remove(pos);

            if (notificationAdapter != null) {
                notificationAdapter.notifyDataSetChanged();

                if (notificationAdapter.getItemCount() == 0) {
                    tv_no_record_found.setVisibility(View.VISIBLE);
                } else tv_no_record_found.setVisibility(View.GONE);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == NOTIFICATION_DOWNLOAD && resultCode == Activity.RESULT_OK) {
            int notification_id = data.getExtras().getInt("notification_id");
            if (modelNotifications1 != null && modelNotifications1.size() > 0) {
                for (int i = 0; i < modelNotifications1.size(); i++) {
                    ModelNotifications modelNotifications = modelNotifications1.get(i);
                    if (modelNotifications.getNotificationId() == notification_id) {
                        modelNotifications1.remove(i);
                        break;
                    }
                }
                if (notificationAdapter != null)
                    notificationAdapter.notifyDataSetChanged();
            }
        }
        if (requestCode == NOTIFICATION_REFRESH) {
            try {
                String result = data.getStringExtra("notification_id");
                deleteSignleNotification(result + "");
                getUserNotification(userid, pageNumber, false, false);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void updateInfo() {
        String user_profilepic = qkPreferences.getValues(QKPreferences.USER_PROFILEIMG);
        if (user_profilepic != null && !user_profilepic.isEmpty()) {
            Picasso.with(context)
                    .load(user_profilepic)
                    .transform(new CircleTransform())
                    .placeholder(R.drawable.ic_default_profile_photo)
                    .fit()
                    .into(img_profile_pic);
        } else img_profile_pic.setImageResource(R.drawable.user_profile_pic);
    }

    private void AcceptRejectTransferProfile(String type, final String notification_id, final ModelNotifications modelNotifications, final int pos) {
        try {
            JSONObject obj = new JSONObject();
            try {
                obj.put("user_id", userid);
                obj.put("status", type);
                obj.put("notification_id", notification_id + "");
            } catch (Exception e) {
                e.printStackTrace();
            }
            Log.e("Param-ScanUser", obj.toString());
            new VolleyApiRequest(getActivity(), true, VolleyApiRequest.REQUEST_BASEURL + "api/profile/transfer_request_status",
                    new VolleyCallBack() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.e("Request Data", "" + response);
                            if (response != null && !response.equals("") && !response.equals("null")) {
                                try {
                                    String status = response.getString("status");
                                    String message = response.getString("msg");
                                    Utilities.showToast(getActivity(), message + "");
                                    if (!status.equalsIgnoreCase("") && status.equalsIgnoreCase("200")) {

                                        readNotification(false, modelNotifications.getNotificationId(), pos, modelNotifications);
                                        deleteSignleNotification(notification_id + "");
                                        getUserNotification(userid, pageNumber, false, false);

                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }

                        @Override
                        public void onErrorResponse(VolleyError error) {
                            // VolleyApiRequest.showVolleyError(NewScanProfileActivity.this, error);
                        }
                    }).
                    enqueRequest(obj, Request.Method.POST);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void ViweRejectionDialog(String ProfileName, final String ProfileID, final ModelNotifications modelNotifications, final int pos) {
        try {
            final Dialog dialog = new Dialog(getActivity());
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            View view = LinearLayout.inflate(getActivity(), R.layout.dialog_employee_rejectlist, null);
            dialog.setContentView(view);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            lp.copyFrom(dialog.getWindow().getAttributes());
            lp.width = WindowManager.LayoutParams.MATCH_PARENT;
            dialog.show();
            dialog.getWindow().setAttributes(lp);

            LinearLayout lin_emo_role_dialog_main;
            final RadioGroup radioGroup;
            TextView close_role, submit_role;

            lin_emo_role_dialog_main = dialog.findViewById(R.id.lin_emo_role_dialog_main);
            radioGroup = dialog.findViewById(R.id.radio_group_role);
            close_role = dialog.findViewById(R.id.tvCancel_role);
            submit_role = dialog.findViewById(R.id.tvSubmit_role);

            RadioButton block = dialog.findViewById(R.id.role4);
            String blocktxt = "Block " + ProfileName + "";
            block.setText(blocktxt + "");

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                view.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
                    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
                    @Override
                    public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                        v.removeOnLayoutChangeListener(this);
                    }
                });
            }
            close_role.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (dialog.isShowing())
                        dialog.dismiss();
                }
            });
            submit_role.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int selectedid = radioGroup.getCheckedRadioButtonId();
                    switch (selectedid) {
                        case R.id.role1:
                            RejectEmployee(1, ProfileID, modelNotifications, pos);
                            break;
                        case R.id.role2:
                            RejectEmployee(2, ProfileID, modelNotifications, pos);
                            break;
                        case R.id.role3:
                            RejectEmployee(3, ProfileID, modelNotifications, pos);
                            break;
                        case R.id.role4:
                            BlockProfile(ProfileID, modelNotifications, pos);
                            break;
                    }
                    if (dialog.isShowing())
                        dialog.dismiss();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void RejectEmployee(int i, String profile_id, final ModelNotifications modelNotifications, final int pos) {
        try {
            readNotification(false, modelNotifications.getNotificationId(), pos, modelNotifications);
            JSONObject obj = new JSONObject();
            try {
                obj.put("status", "R");
                obj.put("profile_id", profile_id);
                obj.put("user_id", userid + "");
                obj.put("reason", i + "");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            new VolleyApiRequest(context, true, VolleyApiRequest.REQUEST_BASEURL + "api/member/status",
                    new VolleyCallBack() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                String responseStatus = response.getString("status");
                                String msg = response.getString("msg");
                                Utilities.showToast(context, msg);
                                if (responseStatus.equals("200")) {
                                    getUserNotification(userid, pageNumber, false, false);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onErrorResponse(VolleyError error) {
                            //VolleyApiRequest.showVolleyError(context, error);
                        }
                    }).enqueRequest(obj, Request.Method.POST);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void BlockProfile(String profile_id, final ModelNotifications modelNotifications, final int pos) {
        try {
            readNotification(false, modelNotifications.getNotificationId(), pos, modelNotifications);
            JSONObject obj = new JSONObject();
            try {
                obj.put("status", "B");
                obj.put("profile_id", profile_id);
                obj.put("user_id", userid + "");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            new VolleyApiRequest(context, true, VolleyApiRequest.REQUEST_BASEURL + "api/member/status",
                    new VolleyCallBack() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                String responseStatus = response.getString("status");
                                String msg = response.getString("msg");
                                Utilities.showToast(context, msg);
                                if (responseStatus.equals("200")) {
                                    getUserNotification(userid, pageNumber, false, false);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onErrorResponse(VolleyError error) {
                            //VolleyApiRequest.showVolleyError(context, error);
                        }
                    }).enqueRequest(obj, Request.Method.POST);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void rejectScanRequest(int opposite_user_id, int notification_id, final int pos) {

        String token = String.valueOf(qkPreferences.getApiToken());

        ModelLoggedUser modelLoggedUser = qkPreferences.getLoggedUser();
        String login_user_id = String.valueOf(modelLoggedUser.getData().getId());

        Map<String, String> header = new HashMap<>();
        header.put("Authorization", "Bearer " + token);
        header.put("Accept", getResources().getString(R.string.acceptjson));
        header.put("QK_ACCESS_KEY", getResources().getString(R.string.QK_ACCESS_KEY) + "");

        final ProgressDialog progressDialog = Utilities.showProgress(context);

        mSoService.rejectScanRequest(header, login_user_id, opposite_user_id, notification_id).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();
                try {

                    JSONObject jsonObject = new JSONObject(response.body().string());
                    String message = jsonObject.optString("msg");
                    Utilities.showToast(context, message);

                    String status = jsonObject.optString("status");
                    if (status != null && !status.isEmpty() && status.equalsIgnoreCase("200")) {
                        refreshNotifications(pos);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();
            }
        });
    }

//    public void approveScanRequest(int opposite_user_id, int notification_id, final int pos) {
//
//        String token = String.valueOf(qkPreferences.getApiToken());
//
//        ModelLoggedUser modelLoggedUser = qkPreferences.getLoggedUser();
//        String login_user_id = String.valueOf(modelLoggedUser.getData().getId());
//
//        Map<String, String> header = new HashMap<>();
//        header.put("Authorization", "Bearer " + token);
//        header.put("Accept", getResources().getString(R.string.acceptjson));
//        header.put("QK_ACCESS_KEY", getResources().getString(R.string.QK_ACCESS_KEY) + "");
//
//        final ProgressDialog progressDialog = Utilities.showProgress(context);
//
//        mSoService.approveScanRequest(header, login_user_id, opposite_user_id, notification_id).enqueue(new Callback<ResponseBody>() {
//            @Override
//            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
//                if (progressDialog != null && progressDialog.isShowing())
//                    progressDialog.dismiss();
//                try {
//
//                    JSONObject jsonObject = new JSONObject(response.body().string());
//                    String message = jsonObject.optString("msg");
//                    Utilities.showToast(context, message);
//
//                    String status = jsonObject.optString("status");
//                    if (status != null && !status.isEmpty() && status.equalsIgnoreCase("200")) {
//                        refreshNotifications(pos);
//                    }
//
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//
//            @Override
//            public void onFailure(Call<ResponseBody> call, Throwable t) {
//                if (progressDialog != null && progressDialog.isShowing())
//                    progressDialog.dismiss();
//            }
//        });
//    }

    public void approveScanRequest(String opposite_uniquecode, final int pos) {

        String token = String.valueOf(qkPreferences.getApiToken());

        ModelLoggedUser modelLoggedUser = qkPreferences.getLoggedUser();
        String login_user_id = String.valueOf(modelLoggedUser.getData().getId());

        Map<String, String> header = new HashMap<>();
        header.put("Authorization", "Bearer " + token);
        header.put("Accept", getResources().getString(R.string.acceptjson));
        header.put("QK_ACCESS_KEY", getResources().getString(R.string.QK_ACCESS_KEY) + "");

        final ProgressDialog progressDialog = Utilities.showProgress(context);

        mSoService.approveScanRequest(header, login_user_id, opposite_uniquecode,
                String.valueOf(TimeZone.getDefault().getID()), qkPreferences.getLati(),
                qkPreferences.getLati(), "android","1").enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();
                try {

                    JSONObject jsonObject = new JSONObject(response.body().string());
                    String message = jsonObject.optString("msg");
                    Utilities.showToast(context, message);

                    String status = jsonObject.optString("status");
                    if (status != null && !status.isEmpty() && status.equalsIgnoreCase("200")) {
                        refreshNotifications(pos);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();
            }
        });
    }
}