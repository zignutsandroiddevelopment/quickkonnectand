package com.fragments;

import android.animation.Animator;
import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.OvershootInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.OpenFire.ChatActivity;
import com.activities.MatchProfileActivity;
import com.adapter.MainAdapter;
import com.adapter.Rec_DashBoard_main;
import com.ceylonlabs.imageviewpopup.ImagePopup;
import com.google.gson.Gson;
import com.interfaces.DashboardContact;
import com.interfaces.OnQkTagInfo;
import com.models.contacts.ContactsPojo;
import com.models.contacts.Datum;
import com.models.logindata.ModelLoggedUser;
import com.models.profiles.ModelProfileList;
import com.models.profiles.ProfilesList;
import com.quickkonnect.Accept_Employee;
import com.quickkonnect.CircleTransform;
import com.quickkonnect.DashboardActivity;
import com.quickkonnect.EmployeeDetails;
import com.quickkonnect.NewScanProfileActivity;
import com.quickkonnect.R;
import com.quickkonnect.UpdateQkTagActivity;
import com.quickkonnect.ViewAndShareQkTag;
import com.realmtable.Contacts;
import com.realmtable.MyProfiles;
import com.realmtable.RealmController;
import com.retrofitservice.ApiUtils;
import com.retrofitservice.SOService;
import com.squareup.picasso.Picasso;
import com.utilities.Constants;
import com.utilities.PerfectViewPager;
import com.utilities.QKPreferences;
import com.utilities.RecyclerEdited;
import com.utilities.Utilities;
import com.volleyapirequest.VolleyApiRequest;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import io.realm.RealmResults;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ZTLAB09 on 12-12-2017.
 */

public class FragDashboard extends Fragment {

    private Context context;
    private SOService mService;
    private View rootView;
    private ImagePopup imagePopup;
    private QKPreferences qkPreferences;
    private RecyclerView newrec;
    private RecyclerView rec_main_botttom_sheet;
    private List<MyProfiles> myProfileslist;
    public MainAdapter mainAdapter;
    private Rec_DashBoard_main newAdapter;
    //int current_selected = 0, currentpos = 0;
    public static RelativeLayout rlnotification;
    public static ImageView img_profile_pic, img_notification, img_view_tag;
    public static TextView tvactionbarTitle;
    public static TextView notification;
    ImageView imgTags;

    public LinearLayout lin_follower_main, lin_following_main;
    public TextView tv_extracount, tv_no_contact_found;

    ArrayList<Datum> profileimages;

    MyBroadCastReceiver myBroadCastReceiver;

    TabLayout tabLayout;
//    private int[] colors = new int[]{};

    private ImageView img_be_a_friend;


    // for share and view option
    String qktagurlmain = "", mainname = "", background = "";

    public static BottomSheetBehavior mBottomSheetBehavior;
    int posheight = 0;

    public static FragDashboard getInstance() {
        FragDashboard fragDashboard = new FragDashboard();
        return fragDashboard;
    }

    public static PerfectViewPager mJazzy;

    //Fab Animation and layout
    LinearLayout view_tag, edit_tag, share_tag, view_profile_tag, frametop;
    FloatingActionButton fab_tag_close, view_tag_fab, edit_tag_fab, share_tag_fab, view_profile_tag_fab;
    RelativeLayout activity_main_fab;
    //    private Animation fabOpenAnimation;
//    private Animation fabCloseAnimation;
    private boolean isFabMenuOpen = false;
    public static int current = 0;

    public int START_ACTIVITY_FOR_CHAT = 1050;

//    public final String BROADCAST_ACTION = "CHATCOUNT";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.frag_dashboard, null);

        context = getActivity();

        mService = ApiUtils.getSOService();
        qkPreferences = QKPreferences.getInstance(context);

        myBroadCastReceiver = new MyBroadCastReceiver();

        getAnimations();

        initWidget();

        return rootView;
    }

    private void getAnimations() {

        view_tag = rootView.findViewById(R.id.view_tag);
        edit_tag = rootView.findViewById(R.id.edit_tag);
        share_tag = rootView.findViewById(R.id.share_tag);
        view_profile_tag = rootView.findViewById(R.id.view_profile_tag);
        view_tag_fab = rootView.findViewById(R.id.view_tag_fab);
        edit_tag_fab = rootView.findViewById(R.id.edit_tag_fab);
        share_tag_fab = rootView.findViewById(R.id.share_tag_fab);
        view_profile_tag_fab = rootView.findViewById(R.id.view_profile_tag_fab);
        fab_tag_close = rootView.findViewById(R.id.fab_tag_close);

        activity_main_fab = rootView.findViewById(R.id.activity_main_fab);
        activity_main_fab.setVisibility(View.GONE);

//        fabOpenAnimation = AnimationUtils.loadAnimation(getActivity(), R.anim.fab_open);
//        fabCloseAnimation = AnimationUtils.loadAnimation(getActivity(), R.anim.fab_close);
        activity_main_fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                collapseFabMenu();
            }
        });

        fab_tag_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                collapseFabMenu();
            }
        });
    }

//    private void expandFabMenu() {
//        activity_main_fab.setVisibility(View.VISIBLE);
//        ViewCompat.animate(fab_tag_close).rotation(45.0F).withLayer().setDuration(300).setInterpolator(new OvershootInterpolator(10.0F)).start();
//        isFabMenuOpen = true;
//    }

    private void collapseFabMenu() {


        ViewCompat.animate(fab_tag_close).rotation(0.0F).withLayer().setDuration(300).setInterpolator(new OvershootInterpolator(10.0F)).start();
        activity_main_fab.setVisibility(View.GONE);
        isFabMenuOpen = false;
    }

    public void initWidget() {

        newrec = rootView.findViewById(R.id.rec_dashboard);
        rec_main_botttom_sheet = rootView.findViewById(R.id.rec_main_botttom_sheet);

        lin_follower_main = rootView.findViewById(R.id.lin_follower_main);
        lin_following_main = rootView.findViewById(R.id.lin_following_main);
        tv_extracount = rootView.findViewById(R.id.tv_extracount);
        tv_no_contact_found = rootView.findViewById(R.id.tv_no_contact_found);

        frametop = rootView.findViewById(R.id.frametop);

        profileimages = new ArrayList<>();
        View bottomSheet = rootView.findViewById(R.id.bottom_sheet_main_dashboard_fragment);
//        CoordinatorLayout coordinatorLayout = rootView.findViewById(R.id.main_content);
//        final FrameLayout llRootLayout = rootView.findViewById(R.id.llRootLayout);

        mBottomSheetBehavior = BottomSheetBehavior.from(bottomSheet);
        mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);

        img_profile_pic = rootView.findViewById(R.id.img_profile_pic);
        img_notification = rootView.findViewById(R.id.img_notification);
        img_view_tag = rootView.findViewById(R.id.img_view_tag);
        tvactionbarTitle = rootView.findViewById(R.id.tvactionbarTitle);
        notification = rootView.findViewById(R.id.tv_cart_badge);
        rlnotification = rootView.findViewById(R.id.rlnotification);

        img_be_a_friend = rootView.findViewById(R.id.img_be_a_friend);

        img_be_a_friend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try {
                    //todo change url
                    String url = VolleyApiRequest.REQUEST_BASEURL + "qk_app";

                    Intent i = new Intent(Intent.ACTION_SEND);
                    i.setType("text/plain");
                    i.putExtra(Intent.EXTRA_SUBJECT, R.string.app_name);
                    String sAux = "\nI'd like to invite you to the Quickkonnect app.\n\n";
                    sAux = sAux + "Get the app:" + url + " \n\n";
                    i.putExtra(Intent.EXTRA_TEXT, sAux);
                    startActivity(Intent.createChooser(i, "choose one"));


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        img_profile_pic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    DashboardActivity.showProfileinfo();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        rlnotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getActivity(), ChatActivity.class);
                startActivityForResult(i, START_ACTIVITY_FOR_CHAT);
            }
        });

        lin_follower_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((DashboardActivity) getActivity()).showContact(1);
            }
        });
        lin_following_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((DashboardActivity) getActivity()).showContact(2);
            }
        });

        tv_extracount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((DashboardActivity) getActivity()).showContact(0);
            }
        });

        LinearLayoutManager layoutManager1 = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        rec_main_botttom_sheet.setLayoutManager(layoutManager1);

        myProfileslist = new ArrayList<>();

        setContctPorfiles(true);

        setupJazziness(RecyclerEdited.TransitionEffect.CubeOut, rootView);

        updateInfo();

        showTags(true);

        // open view Qk tag && share dialog
        img_view_tag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    int pos = mJazzy.getCurrentItem();
                    openViewAndShareOption(pos);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        SetUnReadCount();
    }

    public void SetUnReadCount() {
        try {
            int count = qkPreferences.getUnreadCount();
            if (count != 0 && count > 0) {
                notification.setVisibility(View.VISIBLE);
            } else {
                notification.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void openViewAndShareOption(int pos) {
        try {
            JSONObject jsonObject = new JSONObject(myProfileslist.get(pos).getQktaginfo());
            qktagurlmain = jsonObject.optString("qr_code");
            mainname = myProfileslist.get(pos).getName();
            background = myProfileslist.get(pos).getBackground_gradient();

            Intent i = new Intent(getActivity(), ViewAndShareQkTag.class);
            i.putExtra("QKTAG", qktagurlmain + "");
            i.putExtra("MAIN_NAME", mainname + "");
            i.putExtra("BACKGROUND", background + "");
            startActivity(i);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void openViewAndShareOption(int pos, View view, ImageView imageView) {
        try {
            JSONObject jsonObject = new JSONObject(myProfileslist.get(pos).getQktaginfo());
            qktagurlmain = jsonObject.optString("qr_code");
            mainname = myProfileslist.get(pos).getName();
            background = myProfileslist.get(pos).getBackground_gradient();

            Intent i = null;
            try {
                Drawable drawable = imageView.getDrawable();
                Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
                byte[] b = baos.toByteArray();


                i = new Intent(getActivity(), ViewAndShareQkTag.class);
                i.putExtra("QKTAG", qktagurlmain + "");
                i.putExtra("picture", b);
                i.putExtra("MAIN_NAME", mainname + "");
                i.putExtra("BACKGROUND", background + "");
            } catch (Exception e) {
                e.printStackTrace();
                i = new Intent(getActivity(), ViewAndShareQkTag.class);
                i.putExtra("QKTAG", qktagurlmain + "");
                i.putExtra("MAIN_NAME", mainname + "");
                i.putExtra("BACKGROUND", background + "");
            }

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(getActivity(), view, "image_transition_qktag");
                startActivity(i, options.toBundle());
            } else {
                startActivity(i);
            }
            //startActivity(i);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setContctPorfiles(boolean isReload) {
        try {
            String unique_code = qkPreferences.getValues(QKPreferences.USER_UNIQUECODE);
            profileimages.clear();
            RealmResults<Contacts> contacts = RealmController.with(getActivity()).getContacts();
            if (contacts != null && contacts.size() > 0) {
                for (Contacts contacts1: contacts) {

                    if (!String.valueOf(unique_code).equals(contacts1.getUnique_code()) && contacts1.getContact_type() != null && contacts1.getContact_type().equals(Constants.TYPE_CONTACTS)) {

                        Datum datum = new Datum();
                        datum.setEmail(contacts1.getEmail());
                        datum.setPhone(contacts1.getPhone());
                        datum.setScannedBy(contacts1.getScanned_by());
                        datum.setScanUserId(Integer.parseInt(contacts1.getUser_id().replace(".0", "")));
                        //String cap = contacts1.getUsername().substring(0, 1).toUpperCase() + contacts1.getUsername().substring(1);
                        String cap = contacts1.getUsername();
                        datum.setScanUserName(cap);
                        datum.setScanUserProfileUrl(contacts1.getProfile_pic());
                        datum.setScanUserUniqueCode(contacts1.getUnique_code());
                        datum.setScan_user_device_id(contacts1.getScan_user_device_id() + "");
                        datum.setScanDate(contacts1.getScan_date());
                        datum.setFirstname(contacts1.getFirstname());
                        datum.setLastname(contacts1.getLastname());
                        datum.setIs_employer(contacts1.getIs_employer());
                        datum.setIs_personal(contacts1.getIs_personal());
                        /*if (contacts1.getScan_date() != null && contacts1.getScan_date().length() > 10) {
                            try {
                                //Utilities.changeDateFormat("dd MMM yyyy hh:mm", "dd MMM yyyy", datemain[1] + "")
                                String datemain = contacts1.getScan_date().split(",");
                                datum.setScanDate();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {

                        }*/
                        datum.setFirstname(contacts1.getFirstname());
                        datum.setLastname(contacts1.getLastname());
                        profileimages.add(datum);
                    }
                }
            }
            //newAdapter.notifyDataSetChanged();
            try {
                if (profileimages != null && profileimages.size() > 0) {
                    Utilities.shortContact(profileimages, "date");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            newAdapter = new Rec_DashBoard_main(getActivity(), profileimages, new DashboardContact() {
                @Override
                public void OnUnMatchedContact(Datum contactsList, String Uniquecode, String Deviceid, String userid) {
                    /*Intent intent = new Intent(context, MatchProfileActivity.class);
                    intent.putExtra("unique_code", Uniquecode + "");
                    intent.putExtra(Constants.EXTRA_USER_ID, userid + "");
                    intent.putExtra(Constants.EXTRA_SCAN_USER_DEVICE_ID, Deviceid + "");
                    intent.putExtra(Constants.KEY, Constants.KEY_OTHER);
                    startActivityForResult(intent, 101);*/
                    if (contactsList.getIs_employer() != null && contactsList.getIs_personal() != null) {
                        if (!contactsList.getIs_personal().equalsIgnoreCase("0")) {
                            // Show Employee details
                            if (!contactsList.getIs_employer().equalsIgnoreCase("0")) {
                                //Contact Details having Employee details and redirect to employee
                                Intent intent = new Intent(context, MatchProfileActivity.class);
                                intent.putExtra("unique_code", contactsList.getScanUserUniqueCode() + "");
                                intent.putExtra(Constants.EXTRA_USER_ID, contactsList.getScanUserId() + "");
                                intent.putExtra(Constants.EXTRA_SCAN_USER_DEVICE_ID, contactsList.getScan_user_device_id() + "");
                                intent.putExtra("PROFILE_ID", contactsList.getIs_employer() + "");
                                intent.putExtra(Constants.KEY, Constants.KEY_OTHER_WITH_EMPLOYEE);
                                startActivityForResult(intent, 101);
                            } else {
                                // Remain Same no change
                                Intent intent = new Intent(context, MatchProfileActivity.class);
                                intent.putExtra("unique_code", contactsList.getScanUserUniqueCode() + "");
                                intent.putExtra(Constants.EXTRA_USER_ID, contactsList.getScanUserId() + "");
                                intent.putExtra(Constants.EXTRA_SCAN_USER_DEVICE_ID, contactsList.getScan_user_device_id() + "");
                                intent.putExtra(Constants.KEY, Constants.KEY_OTHER);
                                startActivityForResult(intent, 101);
                            }
                        } else {
                            Intent intent = new Intent(getActivity(), EmployeeDetails.class);
                            intent.putExtra("profile_id", "" + contactsList.getIs_employer());
                            intent.putExtra(Constants.KEY, Constants.KEY_CONTACT_EMPLOYEER);
                            intent.putExtra("scan_user_id", contactsList.getScanUserId() + "");
                            startActivityForResult(intent, 101);

                        }
                    } else {
                        Intent intent = new Intent(context, MatchProfileActivity.class);
                        intent.putExtra("unique_code", contactsList.getScanUserUniqueCode() + "");
                        intent.putExtra(Constants.EXTRA_USER_ID, contactsList.getScanUserId() + "");
                        intent.putExtra(Constants.EXTRA_SCAN_USER_DEVICE_ID, contactsList.getScan_user_device_id() + "");
                        intent.putExtra(Constants.KEY, Constants.KEY_OTHER);
                        startActivityForResult(intent, 101);
                    }

                }
            });
            rec_main_botttom_sheet.setAdapter(newAdapter);

            if (profileimages != null && profileimages.size() > 0) {
                tv_no_contact_found.setVisibility(View.GONE);
                rec_main_botttom_sheet.setVisibility(View.VISIBLE);

                if (profileimages != null && profileimages.size() > 4) {
                    tv_extracount.setVisibility(View.VISIBLE);
                    int size = profileimages.size() - 4;
                    tv_extracount.setText("+" + size + " other");
                } else {
                    tv_extracount.setVisibility(View.GONE);
                }
            } else {
                tv_no_contact_found.setVisibility(View.VISIBLE);
                rec_main_botttom_sheet.setVisibility(View.GONE);
                if (isReload)
                    LoadContact();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void LoadContact() {

//        String token = "";
//        if (qkPreferences != null) {
//            token = qkPreferences.getApiToken() + "";
//        }

        Map<String, String> params = Utilities.getHeaderParameter(context);

        mService.getContactsList(params, qkPreferences.getLoggedUserid(), TimeZone.getDefault().getID()).enqueue(new Callback<ContactsPojo>() {
            @Override
            public void onResponse(Call<ContactsPojo> call, Response<ContactsPojo> response) {
                Log.e("Contacts:Success", "" + response.body());
                try {
                    if (response != null && response.body() != null) {
                        ContactsPojo contactsPojo = response.body();
                        if (contactsPojo != null && contactsPojo.getStatus() == 200) {
                            List<Datum> datumList = contactsPojo.getData();
                            if (datumList != null && datumList.size() > 0) {
                                for (Datum datum: datumList) {
                                    Contacts contacts = new Contacts();
                                    contacts.setUser_id("" + datum.getScanUserId());
                                    contacts.setUsername(datum.getScanUserName());
                                    contacts.setProfile_pic(datum.getScanUserProfileUrl());
                                    contacts.setFirstname(datum.getFirstname());
                                    contacts.setLastname(datum.getLastname());
                                    contacts.setUnique_code(datum.getScanUserUniqueCode());
                                    contacts.setScanned_by(datum.getScannedBy());
                                    contacts.setPhone(datum.getPhone());
                                    contacts.setEmail(datum.getEmail());
                                    contacts.setProfile_id("" + datum.getProfileId());
                                    contacts.setProfile_name("" + datum.getProfileName());
                                    contacts.setContact_type(Constants.TYPE_CONTACTS);
                                    contacts.setScan_user_device_id("" + datum.getScan_user_device_id());
                                    contacts.setScan_date(datum.getScanDate());
                                    /*try {
                                        String scanDate = datum.getScanDate();
                                        if (Utilities.isEmpty(scanDate))
                                            contacts.setScan_date(Utilities.changeDateFormat("yyyy-MM-dd hh:mm:ss", "dd-MM-yyyy", scanDate));

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }*/
                                    RealmController.with(getActivity()).updateContacts(contacts);
                                    setContctPorfiles(false);
                                }
                            } else {
                                try {
                                    RealmController.with(getActivity()).clearContacts();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        } else {
                            RealmController.with(getActivity()).clearContacts();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ContactsPojo> call, Throwable t) {
                Log.e("Contacts:Error", "" + t.getLocalizedMessage());
            }
        });
    }

    private void setupJazziness(RecyclerEdited.TransitionEffect effect, View view) {
        try {
            mJazzy = view.findViewById(R.id.recycler_pager);
            //mJazzy = new PerfectViewPager(getActivity());

            setAdapterdata();

            //mJazzy.setOffscreenPageLimit(3);
            tabLayout = (TabLayout) view.findViewById(R.id.tab_layout);
            tabLayout.setupWithViewPager(mJazzy, true);
            if (myProfileslist.size() == 0) {
                tabLayout.setVisibility(View.GONE);
            } else {
                tabLayout.setVisibility(View.VISIBLE);
            }
            mJazzy.setPageMargin(0);

            /*colorFrom = getResources().getColor(R.color.colorPrimary);
            colorTo = getResources().getColor(R.color.color2);
            colorAnimation = ValueAnimator.ofObject(new ArgbEvaluator(), colorFrom, colorTo);
            colorAnimation.setDuration(100);
            colorAnimation.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator animator) {
                    imageView.setBackgroundColor((int) animator.getAnimatedValue());
                }
            });
            colorAnimation.start();*/

            /*mJazzy.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                }
                @Override
                public void onPageSelected(int position) {
                }
                @Override
                public void onPageScrollStateChanged(int state) {
                }
            });*/

            mJazzy.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @SuppressLint("Range")
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                    String backgr = "";
                    String pre = "";
                    int back = 0;
                    int backpre = 0;

                    try {
                        final MyProfiles myProfiles = myProfileslist.get(position);
                        JSONObject jsonObject = new JSONObject(myProfiles.getQktaginfo());
                        if (jsonObject.optString("background_gradient") != null && !jsonObject.optString("background_gradient").isEmpty()) {

                            if (myProfiles.getBackground_gradient() != null || !myProfiles.getBackground_gradient().equalsIgnoreCase("") || !myProfiles.getBackground_gradient().equalsIgnoreCase("null")) {
                                if (myProfiles.getBackground_gradient() != null || !myProfiles.getBackground_gradient().equalsIgnoreCase("null")) {
                                    backgr = myProfiles.getBackground_gradient();
                                    back = Color.parseColor(backgr);
                                } else {
                                    back = ContextCompat.getColor(getActivity(), R.color.colorPrimary);
                                }

                            } else {
                                back = ContextCompat.getColor(getActivity(), R.color.colorPrimary);
                            }

                          /*  backgr = myProfiles.getBackground_gradient();
                            back = Color.parseColor(backgr);*/
                        }

                        //if (position == myProfileslist.size()) {
                        try {
                            final MyProfiles myProfiles1 = myProfileslist.get(position + 1);
                            JSONObject jsonObject1 = new JSONObject(myProfiles.getQktaginfo());
                            if (jsonObject1.optString("background_gradient") != null && !jsonObject1.optString("background_gradient").isEmpty()) {
                                //backpre = Integer.parseInt(myProfiles.getBackground_gradient() + "");
                                if (myProfiles1.getBackground_gradient() != null || !myProfiles1.getBackground_gradient().equalsIgnoreCase("") || !myProfiles1.getBackground_gradient().equalsIgnoreCase("null"))
                                    backpre = Color.parseColor(myProfiles1.getBackground_gradient() + "");
                                else
                                    backpre = ContextCompat.getColor(getActivity(), R.color.colorPrimary);
                            } else {
                                backpre = ContextCompat.getColor(getActivity(), R.color.colorPrimary);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            backpre = ContextCompat.getColor(getActivity(), R.color.colorPrimary);
                        }
                        // } else {
                        //backpre = ContextCompat.getColor(getActivity(), R.color.colorPrimary);
                        //}


                        /*try {
                            if (backpre == 0)
                                backpre = ContextCompat.getColor(getActivity(), R.color.colorPrimary);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }*/

                        int nextColor = backpre;
                        int currentColor = back;
                        float currentRed = currentColor >> 16 & 0xff;
                        float currentGreen = currentColor >> 8 & 0xff;
                        float currentBlue = currentColor & 0xff;
                        float nextRed = nextColor >> 16 & 0xff;
                        float nextGreen = nextColor >> 8 & 0xff;
                        float nextBlue = nextColor & 0xff;

                        int newRed = (int) (currentRed + ((nextRed - currentRed) * positionOffset));
                        int newGreen = (int) (currentGreen + ((nextGreen - currentGreen) * positionOffset));
                        int newBlue = (int) (currentBlue + ((nextBlue - currentBlue) * positionOffset));
                        mJazzy.setBackgroundColor(Color.rgb(newRed, newGreen, newBlue));

                        Log.e("rgb", "red : " + newRed + "_ green : " + newGreen + "_ blue : " + newBlue);

                    } catch (Exception e) {
                        mJazzy.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                        e.printStackTrace();
                    }
                }

                @Override
                public void onPageSelected(int position) {
                    if (tabLayout != null && tabLayout.getVisibility() == View.INVISIBLE) {
                        tabLayout.setVisibility(View.VISIBLE);
                        frametop.setVisibility(View.VISIBLE);
                    }
                }

                @Override
                public void onPageScrollStateChanged(int state) {
                   /* if (state == ViewPager.SCROLL_STATE_IDLE) {
                        int position = mJazzy.getCurrentItem();
                        if (position == 0) {
                            mJazzy.setCurrentItem(colors.length, false);
                        } else if (position == myProfileslist.size() - 1) {
                            mJazzy.setCurrentItem(1, false);
                        }
                    }*/
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setAdapterdata() {
        if (getActivity() != null) {
            mainAdapter = new MainAdapter(getActivity(), myProfileslist, new OnQkTagInfo() {
                @Override
                public void OnQKTaginfo(MyProfiles myProfiles, final int pos, View mainview) {
                    //openQKTagsOptions(myProfiles, pos);
                    final RelativeLayout rel_qk_cover;
                    final LinearLayout lin_edit, lin_share, lin_profile, lin_name_emp;
                    final TextView text, name;
                    final ImageView image;
                    lin_edit = mainview.findViewById(R.id.lin_qk_edit);
                    lin_share = mainview.findViewById(R.id.lin_qk_share);
                    lin_profile = mainview.findViewById(R.id.lin_qk_profile);
                    rel_qk_cover = mainview.findViewById(R.id.rel_qk_cover);
                    text = mainview.findViewById(R.id.tv_tap_for_tag);
                    //name = mainview.findViewById(R.id.tv_cardname);
                    image = mainview.findViewById(R.id.imgTags);
                    lin_name_emp = mainview.findViewById(R.id.lin_name_emp);

                    //lin_edit.setTag(0);
                    //int isOpen = (int) lin_edit.getTag();

                    mainview.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (lin_profile.getVisibility() == View.VISIBLE) {
                                rel_qk_cover.animate()
                                        .scaleXBy(0.3f)
                                        .scaleYBy(0.3f)
                                        .translationY(0)
                                        .setDuration(300)
                                        .setListener(new Animator.AnimatorListener() {
                                            @Override
                                            public void onAnimationStart(Animator animation) {
                                                lin_share.animate()
                                                        .translationY(0)
                                                        .translationX(0)
                                                        .alpha(0.2f)
                                                        .setDuration(300)
                                                        .start();
                                                lin_edit.animate()
                                                        .translationY(0)
                                                        .translationX(0)
                                                        .alpha(0.2f)
                                                        .setDuration(300)
                                                        .start();
                                                lin_profile.animate()
                                                        .translationY(0)
                                                        .translationX(0)
                                                        .alpha(0.2f)
                                                        .setDuration(300)
                                                        .start();
                                                lin_name_emp.animate()
                                                        .scaleXBy(0.3f)
                                                        .scaleYBy(0.3f)
                                                        .translationY(0)
                                                        .setDuration(300)
                                                        .start();
                                            }

                                            @Override
                                            public void onAnimationEnd(Animator animation) {
                                                lin_share.setVisibility(View.GONE);
                                                lin_profile.setVisibility(View.GONE);
                                                lin_edit.setVisibility(View.GONE);
                                                text.setVisibility(View.VISIBLE);
                                                //tabLayout.setVisibility(View.VISIBLE);
                                                if (myProfileslist.size() == 1) {
                                                    tabLayout.setVisibility(View.GONE);
                                                } else {
                                                    tabLayout.setVisibility(View.VISIBLE);
                                                }
                                                frametop.setVisibility(View.VISIBLE);
                                                image.setAlpha(1f);
                                            }

                                            @Override
                                            public void onAnimationCancel(Animator animation) {
                                            }

                                            @Override
                                            public void onAnimationRepeat(Animator animation) {
                                            }
                                        }).start();
                            }
                        }
                    });


                    if (lin_profile.getVisibility() == View.GONE) {
                        rel_qk_cover.animate()
                                .scaleXBy(-0.3f)
                                .scaleYBy(-0.3f)
                                .translationY(160)
                                .setDuration(300)
                                .setListener(new Animator.AnimatorListener() {
                                    @Override
                                    public void onAnimationStart(Animator animation) {
                                        lin_share.setVisibility(View.VISIBLE);
                                        lin_edit.setVisibility(View.VISIBLE);
                                        lin_profile.setVisibility(View.VISIBLE);
                                        text.setVisibility(View.INVISIBLE);
                                        tabLayout.setVisibility(View.INVISIBLE);
                                        frametop.setVisibility(View.INVISIBLE);

                                        image.setAlpha(0.4f);

                                        lin_share.animate()
                                                .translationY(-210)
                                                .translationX(0)
                                                .alpha(1f)
                                                .setDuration(300)
                                                .start();
                                        lin_edit.animate()
                                                .translationY(-70)
                                                .translationX(-280)
                                                .alpha(1f)
                                                .setDuration(300)
                                                .start();
                                        lin_profile.animate()
                                                .translationY(-70)
                                                .translationX(280)
                                                .alpha(1f)
                                                .setDuration(300)
                                                .start();
                                        lin_name_emp.animate()
                                                .scaleXBy(-0.3f)
                                                .scaleYBy(-0.3f)
                                                .translationY(100)
                                                .setDuration(300)
                                                .start();
                                    }

                                    @Override
                                    public void onAnimationEnd(Animator animation) {

                                    }

                                    @Override
                                    public void onAnimationCancel(Animator animation) {
                                    }

                                    @Override
                                    public void onAnimationRepeat(Animator animation) {
                                    }
                                }).start();
                        //lin_edit.setTag(1);
                    } else if (lin_profile.getVisibility() == View.VISIBLE) {
                        rel_qk_cover.animate()
                                .scaleXBy(0.3f)
                                .scaleYBy(0.3f)
                                .translationY(0)
                                .setDuration(300)
                                .setListener(new Animator.AnimatorListener() {
                                    @Override
                                    public void onAnimationStart(Animator animation) {
                                        lin_share.animate()
                                                .translationY(0)
                                                .translationX(0)
                                                .alpha(0.2f)
                                                .setDuration(300)
                                                .start();
                                        lin_edit.animate()
                                                .translationY(0)
                                                .translationX(0)
                                                .alpha(0.2f)
                                                .setDuration(300)
                                                .start();
                                        lin_profile.animate()
                                                .translationY(0)
                                                .translationX(0)
                                                .alpha(0.2f)
                                                .setDuration(300)
                                                .start();
                                        lin_name_emp.animate()
                                                .scaleXBy(0.3f)
                                                .scaleYBy(0.3f)
                                                .translationY(0)
                                                .setDuration(300)
                                                .start();
                                    }

                                    @Override
                                    public void onAnimationEnd(Animator animation) {
                                        lin_share.setVisibility(View.GONE);
                                        lin_profile.setVisibility(View.GONE);
                                        lin_edit.setVisibility(View.GONE);
                                        text.setVisibility(View.VISIBLE);
                                        //tabLayout.setVisibility(View.VISIBLE);
                                        if (myProfileslist.size() == 1) {
                                            tabLayout.setVisibility(View.GONE);
                                        } else {
                                            tabLayout.setVisibility(View.VISIBLE);
                                        }
                                        frametop.setVisibility(View.VISIBLE);
                                        image.setAlpha(1f);
                                    }

                                    @Override
                                    public void onAnimationCancel(Animator animation) {
                                    }

                                    @Override
                                    public void onAnimationRepeat(Animator animation) {
                                    }
                                }).start();
                        //lin_edit.setTag(1);
                    }

                    lin_edit.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            image.setAlpha(1f);
                            //Toast.makeText(context, "edit : " + pos, Toast.LENGTH_SHORT).show();
                            rel_qk_cover.animate()
                                    .scaleXBy(0.3f)
                                    .scaleYBy(0.3f)
                                    .translationY(0)
                                    .setDuration(300)
                                    .setListener(new Animator.AnimatorListener() {
                                        @Override
                                        public void onAnimationStart(Animator animation) {
                                            lin_share.animate()
                                                    .translationY(0)
                                                    .translationX(0)
                                                    .alpha(0.2f)
                                                    .setDuration(300)
                                                    .start();
                                            lin_edit.animate()
                                                    .translationY(0)
                                                    .translationX(0)
                                                    .alpha(0.2f)
                                                    .setDuration(300)
                                                    .start();
                                            lin_profile.animate()
                                                    .translationY(0)
                                                    .translationX(0)
                                                    .alpha(0.2f)
                                                    .setDuration(300)
                                                    .start();
                                            lin_name_emp.animate()
                                                    .scaleXBy(0.3f)
                                                    .scaleYBy(0.3f)
                                                    .translationY(0)
                                                    .setDuration(300)
                                                    .start();
                                        }

                                        @Override
                                        public void onAnimationEnd(Animator animation) {
                                            lin_share.setVisibility(View.GONE);
                                            lin_profile.setVisibility(View.GONE);
                                            lin_edit.setVisibility(View.GONE);
                                            text.setVisibility(View.VISIBLE);
                                            //tabLayout.setVisibility(View.VISIBLE);
                                            if (myProfileslist.size() == 1) {
                                                tabLayout.setVisibility(View.GONE);
                                            } else {
                                                tabLayout.setVisibility(View.VISIBLE);
                                            }
                                            frametop.setVisibility(View.VISIBLE);
                                        }

                                        @Override
                                        public void onAnimationCancel(Animator animation) {
                                        }

                                        @Override
                                        public void onAnimationRepeat(Animator animation) {
                                        }
                                    }).start();
                            openEdit(pos);
                        }
                    });
                    lin_share.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            image.setAlpha(1f);
                            rel_qk_cover.animate()
                                    .scaleXBy(0.3f)
                                    .scaleYBy(0.3f)
                                    .translationY(0)
                                    .setDuration(300)
                                    .setListener(new Animator.AnimatorListener() {
                                        @Override
                                        public void onAnimationStart(Animator animation) {
                                            lin_share.animate()
                                                    .translationY(0)
                                                    .translationX(0)
                                                    .alpha(0.2f)
                                                    .setDuration(300)
                                                    .start();
                                            lin_edit.animate()
                                                    .translationY(0)
                                                    .translationX(0)
                                                    .alpha(0.2f)
                                                    .setDuration(300)
                                                    .start();
                                            lin_profile.animate()
                                                    .translationY(0)
                                                    .translationX(0)
                                                    .alpha(0.2f)
                                                    .setDuration(300)
                                                    .start();
                                            lin_name_emp.animate()
                                                    .scaleXBy(0.3f)
                                                    .scaleYBy(0.3f)
                                                    .translationY(0)
                                                    .setDuration(300)
                                                    .start();
                                        }

                                        @Override
                                        public void onAnimationEnd(Animator animation) {
                                            lin_share.setVisibility(View.GONE);
                                            lin_profile.setVisibility(View.GONE);
                                            lin_edit.setVisibility(View.GONE);
                                            text.setVisibility(View.VISIBLE);
                                            //tabLayout.setVisibility(View.VISIBLE);
                                            if (myProfileslist.size() == 1) {
                                                tabLayout.setVisibility(View.GONE);
                                            } else {
                                                tabLayout.setVisibility(View.VISIBLE);
                                            }
                                            frametop.setVisibility(View.VISIBLE);
                                        }

                                        @Override
                                        public void onAnimationCancel(Animator animation) {
                                        }

                                        @Override
                                        public void onAnimationRepeat(Animator animation) {
                                        }
                                    }).start();
                            openViewAndShareOption(pos, image, image);
                        }
                    });
                    lin_profile.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            //Toast.makeText(context, "profile : " + pos, Toast.LENGTH_SHORT).show();
                            image.setAlpha(1f);
                            rel_qk_cover.animate()
                                    .scaleXBy(0.3f)
                                    .scaleYBy(0.3f)
                                    .translationY(0)
                                    .setDuration(300)
                                    .setListener(new Animator.AnimatorListener() {
                                        @Override
                                        public void onAnimationStart(Animator animation) {
                                            lin_share.animate()
                                                    .translationY(0)
                                                    .translationX(0)
                                                    .alpha(0.2f)
                                                    .setDuration(300)
                                                    .start();
                                            lin_edit.animate()
                                                    .translationY(0)
                                                    .translationX(0)
                                                    .alpha(0.2f)
                                                    .setDuration(300)
                                                    .start();
                                            lin_profile.animate()
                                                    .translationY(0)
                                                    .translationX(0)
                                                    .alpha(0.2f)
                                                    .setDuration(300)
                                                    .start();
                                            lin_name_emp.animate()
                                                    .scaleXBy(0.3f)
                                                    .scaleYBy(0.3f)
                                                    .translationY(0)
                                                    .setDuration(300)
                                                    .start();
                                        }

                                        @Override
                                        public void onAnimationEnd(Animator animation) {
                                            lin_share.setVisibility(View.GONE);
                                            lin_profile.setVisibility(View.GONE);
                                            lin_edit.setVisibility(View.GONE);
                                            text.setVisibility(View.VISIBLE);
                                            //tabLayout.setVisibility(View.VISIBLE);
                                            if (myProfileslist.size() == 1) {
                                                tabLayout.setVisibility(View.GONE);
                                            } else {
                                                tabLayout.setVisibility(View.VISIBLE);
                                            }
                                            frametop.setVisibility(View.VISIBLE);
                                        }

                                        @Override
                                        public void onAnimationCancel(Animator animation) {
                                        }

                                        @Override
                                        public void onAnimationRepeat(Animator animation) {
                                        }
                                    }).start();
                            viewProfile(pos);
                        }
                    });
                }
            });
            mJazzy.setAdapter(mainAdapter);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        SetUnReadCount();

        if (frametop != null)
            frametop.setVisibility(View.VISIBLE);

        if (requestCode == 101) {
            setContctPorfiles(true);
           /* try {
                getActivity().startService(new Intent(getActivity(), LoadMyContacts.class));
            } catch (Exception e) {
                e.printStackTrace();
            }*/
        } else {
            showTags(true);
        }
    }

//    public void shareItem(String url) {
//        if (url != null && !url.isEmpty() && !url.equals("")) {
//            Picasso.with(getActivity()).load(url).into(new Target() {
//                @Override
//                public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
//                    Intent i = new Intent(Intent.ACTION_SEND);
//                    i.setType("image/*");
//                    i.putExtra(Intent.EXTRA_STREAM, Utilities.getLocalBitmapUri(context, bitmap));
//                    startActivity(Intent.createChooser(i, "Share Image"));
//                }
//
//                @Override
//                public void onBitmapFailed(Drawable errorDrawable) {
//                }
//
//                @Override
//                public void onPrepareLoad(Drawable placeHolderDrawable) {
//                }
//            });
//        }
//    }
//
//    public void initImagepopup(String qktagUrl) {
//        imagePopup = new ImagePopup(context);
//        imagePopup.setWindowHeight(800); // Optional
//        imagePopup.setWindowWidth(800); // Optional
//        imagePopup.setBackgroundColor(Color.BLACK);  // Optional
//        imagePopup.setFullScreen(true); // Optional
//        imagePopup.setHideCloseIcon(false);  // Optional
//        imagePopup.setImageOnClickClose(true);  // Optional
//        imagePopup.initiatePopupWithPicasso(qktagUrl);
//        imagePopup.viewPopup();
//    }

    @Override
    public void onResume() {
        super.onResume();
        if (frametop != null)
            frametop.setVisibility(View.VISIBLE);
    }

    public void getList() {
        String token = "";
        if (qkPreferences != null) {
            token = qkPreferences.getApiToken() + "";
        }

        Map<String, String> params = new HashMap<>();
        params.put("Authorization", "Bearer " + token + "");
        params.put("Accept", "application/json");
        params.put("QK_ACCESS_KEY", context.getResources().getString(R.string.QK_ACCESS_KEY));
        mService.getProfileList(params, qkPreferences.getValuesInt(QKPreferences.USER_ID)).enqueue(new Callback<ModelProfileList>() {
            @Override
            public void onResponse(Call<ModelProfileList> call, Response<ModelProfileList> response) {
                Log.e("Profiles-Success", "" + response.body());
                try {
                    if (response != null && response.body() != null) {

                        ModelProfileList modelProfileList = response.body();

                        if (modelProfileList.getPlanDetail() != null) {

                            if (modelProfileList.getPlanDetail().getMaxEmployee() != null) {
                                qkPreferences.setValues(QKPreferences.MAX_EMPLOYEE, modelProfileList.getPlanDetail().getMaxEmployee());
                            } else {
                                qkPreferences.setValues(QKPreferences.MAX_EMPLOYEE, "0");
                            }

                        } else {

                            qkPreferences.setValues(QKPreferences.MAX_EMPLOYEE, "0");
                        }

                        ModelLoggedUser modelLoggedUser = qkPreferences.getLoggedUser();
                        modelLoggedUser.getData().setIs_public(modelProfileList.isIs_public());
                        qkPreferences.storeLoggedUser(modelLoggedUser);

                        if (modelProfileList != null)
                            parseResponse(modelProfileList);
                    }
                } catch (Exception e) {
                    Log.e("Profiles-Error", "" + e.getMessage());
                }
            }

            @Override
            public void onFailure(Call<ModelProfileList> call, Throwable t) {
                Log.e("Profiles-Error", "" + t.getLocalizedMessage());
            }
        });
    }

    private void parseResponse(ModelProfileList modelProfileList) {
        try {
            if (modelProfileList.getStatus() == 200) {

                List<ProfilesList> profilesLists = modelProfileList.getData();

                if (profilesLists != null && profilesLists.size() > 0) {

                    Log.e("Profile-Data", "" + modelProfileList.getData().size());

                    List<String> myprofiles = new ArrayList<>();

                    for (ProfilesList profilesList: profilesLists) {

                        MyProfiles myProfiles = new MyProfiles();

                        myProfiles.setId("" + profilesList.getId());
                        myProfiles.setName(profilesList.getName());
                        myProfiles.setStatus(profilesList.getStatus());
                        myProfiles.setType("" + profilesList.getType());
                        myProfiles.setLogo("" + profilesList.getLogo());
                        myProfiles.setBackground_gradient("" + profilesList.getQkTagInfo().getBackground_gradient());
                        myProfiles.setReason(profilesList.getResson() + "");
                        myProfiles.setRole_name(profilesList.getRole_name() + "");
                        myProfiles.setRole(profilesList.getRole() + "");
                        myProfiles.setUnique_code("" + profilesList.getUniqueCode());
                        String qktaginfo = new Gson().toJson(profilesList.getQkTagInfo());
                        myProfiles.setQktaginfo(qktaginfo);
                        myProfiles.setIsOwnProfile("true");

                        myprofiles.add(profilesList.getUniqueCode());

                        RealmController.with(getActivity()).updateMyProfiles(myProfiles);
                    }

                    qkPreferences.storeMyProfiles(myprofiles);

                    showTags(false);
                }

            } else Utilities.showToast(context, modelProfileList.getMsg());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void showTags(boolean isRefresh) {
        try {

            List<MyProfiles> myProfiles = RealmController.with(getActivity()).getMyProfiles();

            if (myProfiles != null && myProfiles.size() > 0) {

                myProfileslist.clear();

                try {
                    MyProfiles primaryProfile1 = new MyProfiles();

                    for (MyProfiles profiles: myProfiles) {
                        if (qkPreferences.getValues(QKPreferences.USER_UNIQUECODE).equalsIgnoreCase(profiles.getUnique_code())) {
                            primaryProfile1 = profiles;
                        } else {
                            if (profiles.getIsOwnProfile() != null && profiles.getIsOwnProfile().equals("true")) {
                                Log.e("MYTag", "" + profiles.getQktaginfo());
                                if (isMyProfile(profiles.getUnique_code())) {

                                    if (profiles.getType() != null && profiles.getType().equalsIgnoreCase("4")) {
                                        String companyProfile = profiles.getProfile();
                                        if (companyProfile != null && !companyProfile.isEmpty()) {
                                            try {
                                                JSONObject jsonObject = new JSONObject(companyProfile);
                                                String profile_name = jsonObject.optString("name");
                                                profiles.setName(profile_name + "");
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                        }
                                        myProfileslist.add(profiles);
                                    } else {
                                        myProfileslist.add(profiles);

                                    }
                                }
                            }
                        }
                    }

                    if (primaryProfile1 != null) {
                        myProfileslist.add(0, primaryProfile1);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    myProfileslist.addAll(myProfiles);
                }

                try {
                    mJazzy.setOffscreenPageLimit(myProfileslist.size());
                } catch (Exception e) {
                    e.printStackTrace();
                }

                /*if (mainAdapter != null)
                    mainAdapter.notifyDataSetChanged();*/
                setAdapterdata();

                if (myProfileslist.size() == 1) {
                    tabLayout.setVisibility(View.GONE);
                } else {
                    tabLayout.setVisibility(View.VISIBLE);
                }
            }

            if (isRefresh) {
                if (Utilities.isNetworkConnected(context))
                    getList();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean isMyProfile(String uniquecode) {
        List<String> myprofiles = qkPreferences.getMyProfilesIds();
        if (myprofiles != null)
            for (String s: myprofiles) {
                if (s.equals(uniquecode))
                    return true;
            }

        return false;
    }

    private void viewProfile(int pos) {
        String myuniqueid = qkPreferences.getValues(QKPreferences.USER_UNIQUECODE) + "";
        if (myProfileslist != null) {
            MyProfiles myProfiles = myProfileslist.get(pos);
            if (myProfiles != null) {
                try {
                    final String profile_id = myProfiles.getId();
                    final String profile_uniquecode = myProfiles.getUnique_code();
                    final String profile_name = myProfiles.getName();
                    if (myProfiles.getUnique_code().equalsIgnoreCase(myuniqueid + "")) {
                        try {
                            DashboardActivity.showProfileinfo();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        if (myProfiles.getType() != null && myProfiles.getType().equalsIgnoreCase("4")) {
                            Intent intent = new Intent(context, Accept_Employee.class);
                            intent.putExtra(Constants.KEY, Constants.KEY_FROM_MANAGEPROFILES);
                            intent.putExtra("PROFILE_NAME", profile_name + "");
                            intent.putExtra("unique_id", profile_uniquecode + "");
                            intent.putExtra("profile_id", "" + profile_id);
                            startActivityForResult(intent, Constants.DELETE_EMPLOYEE_PROFILE);
                        } else {
                            Intent intent = new Intent(context, NewScanProfileActivity.class);
                            intent.putExtra("unique_code", "" + profile_uniquecode);
                            intent.putExtra(Constants.KEY, Constants.KEY_FROM_MANAGEPROFILES);
                            intent.putExtra("profile_id", "" + profile_id);
                            startActivity(intent);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void openEdit(int pos) {

        if (myProfileslist != null) {
            MyProfiles myProfiles = myProfileslist.get(pos);
            if (myProfiles != null) {
                try {
                    final JSONObject jsonObject = new JSONObject(myProfiles.getQktaginfo());
                    final String middle_tag = jsonObject.optString("middle_tag");
                    final String profile_id = myProfiles.getId();
                    final String user_id = myProfiles.getUser_id();
                    final String profile_type = myProfiles.getType();
                    final String profile_uniquecode = myProfiles.getUnique_code();

                    Intent editQKTag = new Intent(context, UpdateQkTagActivity.class);
                    editQKTag.putExtra(Constants.KEY_FROM, Constants.KEY_FROM_DASHBOARD);
                    editQKTag.putExtra(Constants.KEY_QKTAGINFO, "" + jsonObject);
                    editQKTag.putExtra(Constants.KEY_PROFILEID, profile_id);
                    editQKTag.putExtra(Constants.KEY_PROFILELEUSERID, user_id);
                    editQKTag.putExtra(Constants.KEY_PROFILETYPE, profile_type);
                    editQKTag.putExtra(Constants.KEY_UNQUECODE, profile_uniquecode);
                    editQKTag.putExtra(Constants.KEY_MIDDLETAG, middle_tag);
                    editQKTag.putExtra(Constants.KEY_BACKGROUND_GRADIENT, myProfiles.getBackground_gradient() + "");

                    startActivityForResult(editQKTag, Constants.RCODE_UPDATEQKTAG);

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }
    }


    public void openQKTagsOptions(final MyProfiles myProfilessdfsdf, final int pos) {
        try {

            // List<MyProfiles> myProfiles1 = RealmController.with(getActivity()).getMyProfiles();
            if (myProfilessdfsdf != null) {

                /*final List<MyProfiles> myProfileslist1 = new ArrayList<>();
                myProfileslist1.clear();
                myProfileslist1.addAll(myProfiles1);

                if (myProfileslist1 != null && myProfileslist1.size() > 1) {
                    MyProfiles primaryProfile = myProfileslist1.get(myProfileslist1.size() - 1);
                    myProfileslist1.remove(myProfileslist1.size() - 1);
                    myProfileslist1.add(0, primaryProfile);
                }*/

                view_tag = rootView.findViewById(R.id.view_tag);
                edit_tag = rootView.findViewById(R.id.edit_tag);
                share_tag = rootView.findViewById(R.id.share_tag);
                view_profile_tag = rootView.findViewById(R.id.view_profile_tag);
                view_tag_fab = rootView.findViewById(R.id.view_tag_fab);
                edit_tag_fab = rootView.findViewById(R.id.edit_tag_fab);
                share_tag_fab = rootView.findViewById(R.id.share_tag_fab);
                view_profile_tag_fab = rootView.findViewById(R.id.view_profile_tag_fab);
                fab_tag_close = rootView.findViewById(R.id.fab_tag_close);

                final JSONObject jsonObject = new JSONObject(myProfilessdfsdf.getQktaginfo());
                final String qktag_url = jsonObject.optString("qr_code");
                final String middle_tag = jsonObject.optString("middle_tag");
                final String profile_id = myProfilessdfsdf.getId();
                final String user_id = myProfilessdfsdf.getUser_id();
                final String profile_type = myProfilessdfsdf.getType();
                final String profile_uniquecode = myProfilessdfsdf.getUnique_code();
                final String profile_name = myProfilessdfsdf.getName();

                qktagurlmain = qktag_url;
                mainname = profile_name;
                background = myProfilessdfsdf.getBackground_gradient();

                if (myProfilessdfsdf.getRole().equalsIgnoreCase("0") || myProfilessdfsdf.getRole().equalsIgnoreCase("3")) {  // None  || Admin
                    view_tag.setVisibility(View.VISIBLE);
                    edit_tag.setVisibility(View.VISIBLE);
                    share_tag.setVisibility(View.VISIBLE);
                    view_profile_tag.setVisibility(View.VISIBLE);
                } else if (myProfilessdfsdf.getRole().equalsIgnoreCase("1")) {  // View
                    view_tag.setVisibility(View.VISIBLE);
                    edit_tag.setVisibility(View.GONE);
                    share_tag.setVisibility(View.GONE);
                    view_profile_tag.setVisibility(View.VISIBLE);
                } else if (myProfilessdfsdf.getRole().equalsIgnoreCase("2")) {  // View/Edit
                    view_tag.setVisibility(View.VISIBLE);
                    edit_tag.setVisibility(View.VISIBLE);
                    share_tag.setVisibility(View.GONE);
                    view_profile_tag.setVisibility(View.VISIBLE);
                }

                view_tag_fab.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        collapseFabMenu();
                        //initImagepopup(qktag_url);
                        //openViewAndShareOption(pos);
                    }
                });
                edit_tag_fab.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        collapseFabMenu();
                        Intent editQKTag = new Intent(context, UpdateQkTagActivity.class);
                        editQKTag.putExtra(Constants.KEY_FROM, Constants.KEY_FROM_DASHBOARD);
                        editQKTag.putExtra(Constants.KEY_QKTAGINFO, "" + jsonObject);
                        editQKTag.putExtra(Constants.KEY_PROFILEID, profile_id);
                        editQKTag.putExtra(Constants.KEY_PROFILELEUSERID, user_id);
                        editQKTag.putExtra(Constants.KEY_PROFILETYPE, profile_type);
                        editQKTag.putExtra(Constants.KEY_UNQUECODE, profile_uniquecode);
                        editQKTag.putExtra(Constants.KEY_MIDDLETAG, middle_tag);
                        editQKTag.putExtra(Constants.KEY_BACKGROUND_GRADIENT, myProfilessdfsdf.getBackground_gradient() + "");

                        startActivityForResult(editQKTag, Constants.RCODE_UPDATEQKTAG);
                    }
                });
                share_tag_fab.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        collapseFabMenu();
                        //shareItem(qktag_url);
                        //openViewAndShareOption(pos);
                    }
                });
                view_profile_tag_fab.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        try {
                            collapseFabMenu();
                            if (pos == 0) {
                                try {
                                    DashboardActivity.showProfileinfo();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            } else {
                                if (myProfilessdfsdf.getType() != null && myProfilessdfsdf.getType().equalsIgnoreCase("4")) {
                                    /*Intent intent = new Intent(context, EmployeeDetails.class);
                                    intent.putExtra("unique_code", "" + profile_uniquecode);
                                    intent.putExtra(Constants.KEY, Constants.KEY_FROM_MANAGEPROFILES);
                                    intent.putExtra("profile_id", "" + profile_id);
                                    startActivity(intent);*/
                                    Intent intent = new Intent(context, Accept_Employee.class);
                                    intent.putExtra(Constants.KEY, Constants.KEY_FROM_MANAGEPROFILES);
                                    intent.putExtra("PROFILE_NAME", profile_name + "");
                                    intent.putExtra("unique_id", profile_uniquecode + "");
                                    intent.putExtra("profile_id", "" + profile_id);
                                    startActivityForResult(intent, Constants.DELETE_EMPLOYEE_PROFILE);
                                } else {
                                    Intent intent = new Intent(context, NewScanProfileActivity.class);
                                    intent.putExtra("unique_code", "" + profile_uniquecode);
                                    intent.putExtra(Constants.KEY, Constants.KEY_FROM_MANAGEPROFILES);
                                    intent.putExtra("profile_id", "" + profile_id);
                                    startActivity(intent);
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateInfo() {
        String user_profilepic = qkPreferences.getValues(QKPreferences.USER_PROFILEIMG);
        if (user_profilepic != null && !user_profilepic.isEmpty()) {
            Picasso.with(context)
                    .load(user_profilepic)
                    .transform(new CircleTransform())
                    .placeholder(R.drawable.ic_default_profile_photo)
                    .fit()
                    .into(img_profile_pic);
        } else img_profile_pic.setImageResource(R.drawable.user_profile_pic);
        showTags(false);
    }

    private class MyBroadCastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                String type = intent.getExtras().getString("data");
                if (type.equalsIgnoreCase("1")) {
                    notification.setVisibility(View.VISIBLE);
                } else {
                    notification.setVisibility(View.GONE);
                }
                Log.d("100", "onReceive() called");
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            getActivity().unregisterReceiver(myBroadCastReceiver);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void hideReferView() {
        mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
    }
}