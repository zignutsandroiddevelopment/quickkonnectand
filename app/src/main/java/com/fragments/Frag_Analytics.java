package com.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.adapter.FollowerAdapter;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.models.logindata.ModelLoggedData;
import com.models.logindata.ModelLoggedUser;
import com.quickkonnect.PojoClasses;
import com.quickkonnect.R;
import com.realmtable.Followers;
import com.realmtable.RealmController;
import com.utilities.Constants;
import com.utilities.QKPreferences;
import com.utilities.Utilities;
import com.volleyapirequest.VolleyApiRequest;
import com.volleyapirequest.VolleyCallBack;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by ZTLAB03 on 09-04-2018.
 */

public class Frag_Analytics extends Fragment {

    View rootView;
    QKPreferences qkPreferences;
    Context context;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.frag_analytics, container, false);

        context = getActivity();
        qkPreferences = new QKPreferences(getActivity());

        return rootView;
    }

    public static Frag_Analytics newInstance(int position, Bundle bundle) {
        bundle.putInt("pos", position);
        Frag_Analytics frag_analytics = new Frag_Analytics();
        frag_analytics.setArguments(bundle);
        return frag_analytics;
    }

}