package com.fragments.createprofileintro;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.quickkonnect.R;

/**
 * Created by ZTLAB-10 on 14-02-2018.
 */

public class FragInformation extends Fragment {

    private Context context;
    private View rootView;

    public static FragInformation getInstance(Bundle bundle) {
        FragInformation fragInformation = new FragInformation();
        fragInformation.setArguments(bundle);
        return fragInformation;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.item_createprofileintro, null);

        context = getActivity();

        initWidget();

        return rootView;
    }

    public void initWidget() {
    }
}
