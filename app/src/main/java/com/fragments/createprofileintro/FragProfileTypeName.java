package com.fragments.createprofileintro;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.InputFilter;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;

import com.models.createprofile.CreateProfileData;
import com.quickkonnect.R;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;
import com.utilities.Utilities;

import java.util.regex.Pattern;

/**
 * Created by ZTLAB-10 on 14-02-2018.
 */

public class FragProfileTypeName extends Fragment {

    private Context context;
    private View rootView;
    private String blockCharacterSet = "~#^|$%*!<>/%";
    public static SearchableSpinner spinnerProfileType;
    public static EditText edt_profile_name;

    private InputFilter filter = new InputFilter() {
        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
            if (source != null && blockCharacterSet.contains(("" + source))) {
                return "";
            }
            return null;
        }
    };
   /* InputFilter filter = new InputFilter() {
        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
            for (int i = start; i < end; ++i) {
                if (!Pattern.compile("[ABCDEFGHIJKLMNNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890()]*").matcher(String.valueOf(source.charAt(i))).matches()) {
                    if (!String.valueOf(source.charAt(i)).equalsIgnoreCase(" "))
                        return "";
                }
            }
            return null;
        }
    };*/

    public static FragProfileTypeName getInstance(Bundle bundle) {
        FragProfileTypeName fragInformation = new FragProfileTypeName();
        fragInformation.setArguments(bundle);
        return fragInformation;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.item_createprofiletype, null);

        context = getActivity();

        initWidget();

        return rootView;
    }

    public void initWidget() {

        spinnerProfileType = rootView.findViewById(R.id.spinnerProfileType);
        edt_profile_name = rootView.findViewById(R.id.edt_profile_name);

        edt_profile_name.setFilters(new InputFilter[]{filter});

        String items[] = {"Company", "Enterprise", "Public Figure/Celebrity"};
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, items);
        spinnerProfileType.setTitle("Select Profile Type");
        spinnerProfileType.setAdapter(spinnerArrayAdapter);
    }

    public CreateProfileData getNameType(CreateProfileData modelCreateProfile) {

        if (edt_profile_name != null && Utilities.isEmpty(edt_profile_name.getText().toString())) {
            modelCreateProfile.setName(edt_profile_name.getText().toString());
        }
        if (spinnerProfileType != null) {
            int selectedItempos = spinnerProfileType.getSelectedItemPosition();
            if (selectedItempos == 0) {
                modelCreateProfile.setType(1);
            } else if (selectedItempos == 1) {
                modelCreateProfile.setType(2);
            } else if (selectedItempos == 2) {
                modelCreateProfile.setType(3);
            }
        }
        return modelCreateProfile;
    }

    public String getValidation() {
        String type = "NO";
        if (edt_profile_name != null && Utilities.isEmpty(edt_profile_name.getText().toString()) || (spinnerProfileType != null && !spinnerProfileType.getSelectedItem().toString().equalsIgnoreCase(""))) {
            type = "YES";
        }
        return type;
    }
}
