package com.fragments.createprofileintro;


import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.Selection;
import android.text.Spanned;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.activities.AddFoundersActivity;
import com.activities.CreateProfileActivity;
import com.adapter.AdpFounders;
import com.adapter.AdpOtherAddress;
import com.adapter.AdpSingleEmail;
import com.adapter.AdpSinglePhone;
import com.adapter.AdpSingleWebLink;
import com.adapter.AdpSocialMedium;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.interfaces.OnEditFounder;
import com.interfaces.OnEditMedia;
import com.interfaces.OnSingleEdit;
import com.interfaces.getPosForAddress;
import com.models.createprofile.CreateProfileData;
import com.models.createprofile.Email;
import com.models.createprofile.Founder;
import com.models.createprofile.OtherAddresModel;
import com.models.createprofile.Phone;
import com.models.createprofile.SocialMedium;
import com.models.createprofile.Weblink;
import com.quickkonnect.CircleTransform;
import com.quickkonnect.NewScanProfileActivity;
import com.quickkonnect.R;
import com.quickkonnect.SimpleDividerItemDecoration;
import com.rilixtech.Country;
import com.rilixtech.CountryCodePicker;
import com.squareup.picasso.Picasso;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;
import com.utilities.CompressImage;
import com.utilities.Constants;
import com.utilities.Utilities;
import com.yalantis.ucrop.UCrop;


import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import io.reactivex.annotations.NonNull;

/**
 * Created by ZTLAB-10 on 14-02-2018.
 */

public class FragProfInfo extends Fragment implements OnSingleEdit, OnEditMedia, OnEditFounder {

    private Context context;
    private View rootView, divider_socialmedia, divider_founders, divider_weblink;
    int other_add_pos = -1;
    public TextView tv_socialmedia_add_info, tv_oa_add_info, tv_add_other_address, tv_founders_add_info, tv_phone_add_info, tv_email_add_info, tv_weblink_add_info, tv_add_socialmedia, tv_add_founders, tv_add_weblink, tv_add_phone, tv_add_email;

    public static TextInputLayout input_about, input_address, input_teamsize, input_doe, input_dob, input_ceo/*, input_cto*/;

    public EditText edt_profile_name;
    int PLACE_PICKER_REQUEST_ADDRESS = 1010;
    public static EditText edt_about, edt_address, edt_teamsize, edt_doestablishment, edt_dob, /*edt_currentcfo*/
            edt_currentceo;
    EditText edtSocialmediaLInk;
    public static LinearLayout linear_founder;
    public static RelativeLayout rel_check_box;
    public static TextView btn_save_final_data;

    private RecyclerView.LayoutManager mLayoutManager;

    // Email
    private RecyclerView recyclerView_email;
    private static List<Email> listEmail;
    private AdpSingleEmail adpSingleTextEmail;

    //Phone
    private RecyclerView recyclerView_phone;
    private static List<Phone> listPhones;
    private AdpSinglePhone adpSingleTextPhone;

    //Web Link
    private RecyclerView recyclerView_weblink;
    private static List<Weblink> listWeblink;
    private AdpSingleWebLink adpSingleTextWeblink;

    //Social Media
    private RecyclerView recyclerView_socialmedia;
    private static List<SocialMedium> socialMediumList;
    private AdpSocialMedium adpSocialMedium;

    //Other Address
    private RecyclerView recyclerView_oa;
    private static List<OtherAddresModel> other_address;
    private AdpOtherAddress adpOtherAddress;

    // Founders
    private RecyclerView recyclerView_founders;
    private static List<Founder> foundersList;
    private AdpFounders adpfounders;

    int rotateImage;

    private BottomSheetDialog bottomSheetDialog;

    public static ImageView img_basic_profile_profile_pic;

    static final int REQUEST_STORAGE_READ_ACCESS_PERMISSION = 6;
    private static final int REQUEST_SELECT_PICTURE = 100;
    private static final int PICK_FROM_CAMERA = 101;
    private static final int REQUEST_SELECT_ADDFOUNDERS = 102;
    int PLACE_PICKER_REQUEST = 103;

    public static File file;
    private Uri fileUri;

    public static Bitmap selectedImage = null;
    private String editString = "";
    private int editPos = -1;
    private SocialMedium editsocialMedium = null;
    private TextView tv_counter_udbp;
    public static int mYear, mMonth, mDay;
    public static Calendar c;
    public static CheckBox chk_forms;
    private CountryCodePicker ccp;
    private ImageView img_create_profile_close;
    private String blockCharacterSet = "~#^|$%*!<>/%";

    private InputFilter filter = new InputFilter() {
        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
            if (source != null && blockCharacterSet.contains(("" + source))) {
                return "";
            }
            return null;
        }
    };
    private LinearLayout rlimgupload;

    public static FragProfInfo getInstance(Bundle bundle) {
        FragProfInfo fragInformation = new FragProfInfo();
        fragInformation.setArguments(bundle);
        return fragInformation;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.frag_profile_information, null);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        context = getActivity();

        selectedImage = null;

        c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        initWidget();

        return rootView;
    }

    public void initWidget() {

        btn_save_final_data = rootView.findViewById(R.id.btn_save_final_data);

        edt_about = rootView.findViewById(R.id.edt_about);
        edt_address = rootView.findViewById(R.id.edt_address);
        edt_teamsize = rootView.findViewById(R.id.edt_teamsize);
        edt_doestablishment = rootView.findViewById(R.id.edt_doestablishment);
        edt_dob = rootView.findViewById(R.id.edt_dob);
        edt_currentceo = rootView.findViewById(R.id.edt_currentceo);
        //edt_currentcfo = rootView.findViewById(R.id.edt_currentcfo);
        tv_counter_udbp = rootView.findViewById(R.id.tv_counter_udbp);
        img_create_profile_close = rootView.findViewById(R.id.img_create_profile_close);

        Utilities.TextWatcher(edt_about, tv_counter_udbp, "400");
        edt_about.setFilters(new InputFilter[]{new InputFilter.LengthFilter(400)});

        input_about = rootView.findViewById(R.id.input_about);
        input_address = rootView.findViewById(R.id.input_address);
        input_teamsize = rootView.findViewById(R.id.input_teamsize);
        input_doe = rootView.findViewById(R.id.input_doe);
        input_dob = rootView.findViewById(R.id.input_dob);
        input_ceo = rootView.findViewById(R.id.input_ceo);
        //input_cto = rootView.findViewById(R.id.input_cto);

        edt_address.setClickable(true);
        edt_address.setEnabled(true);

        linear_founder = rootView.findViewById(R.id.linear_founder);
        rel_check_box = rootView.findViewById(R.id.rel_check_box);

        edt_currentceo.setFilters(new InputFilter[]{filter});
        edt_about.setFilters(new InputFilter[]{filter, new InputFilter.LengthFilter(400)});
        //edt_currentcfo.setFilters(new InputFilter[]{filter});

        rlimgupload = rootView.findViewById(R.id.rlimgupload);

        tv_oa_add_info = rootView.findViewById(R.id.tv_oa_add_info);
        tv_add_other_address = rootView.findViewById(R.id.tv_add_other_address);
        tv_founders_add_info = rootView.findViewById(R.id.tv_founder_add_info);
        tv_socialmedia_add_info = rootView.findViewById(R.id.tv_socialmedia_add_info);
        tv_weblink_add_info = rootView.findViewById(R.id.tv_weblink_add_info);
        tv_phone_add_info = rootView.findViewById(R.id.tv_phone_add_info);
        tv_email_add_info = rootView.findViewById(R.id.tv_email_add_info);

        tv_add_founders = rootView.findViewById(R.id.tv_add_founder);
        tv_add_socialmedia = rootView.findViewById(R.id.tv_add_socialmedia);
        tv_add_weblink = rootView.findViewById(R.id.tv_add_weblink);
        tv_add_phone = rootView.findViewById(R.id.tv_add_phone);
        tv_add_email = rootView.findViewById(R.id.tv_add_email);

        divider_socialmedia = rootView.findViewById(R.id.divider_socialmedia);
        divider_founders = rootView.findViewById(R.id.divider_founder);
        divider_weblink = rootView.findViewById(R.id.divider_weblink);

        recyclerView_oa = rootView.findViewById(R.id.recyclerView_oa);
        recyclerView_founders = rootView.findViewById(R.id.recyclerView_founders);
        recyclerView_socialmedia = rootView.findViewById(R.id.recyclerView_socialmedia);
        recyclerView_weblink = rootView.findViewById(R.id.recyclerView_weblink);
        recyclerView_email = rootView.findViewById(R.id.recyclerView_email);
        recyclerView_phone = rootView.findViewById(R.id.recyclerView_phone);

        edt_profile_name = rootView.findViewById(R.id.edt_profile_name);
        chk_forms = rootView.findViewById(R.id.chk_forms);

        img_basic_profile_profile_pic = rootView.findViewById(R.id.img_basic_profile_profile_pic);


        // ********* submit data  to create profile activity

        btn_save_final_data.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    ((CreateProfileActivity) getActivity()).saveFragProfinfo();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        img_create_profile_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    ((CreateProfileActivity) getActivity()).CloseActivity();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        //**********************************

        try {
            Picasso.with(context)
                    .load(R.drawable.ic_logo_svg)
                    .error(R.drawable.ic_logo_svg)
                    .placeholder(R.drawable.ic_logo_svg)
                    .transform(new CircleTransform())
                    .fit()
                    .into(img_basic_profile_profile_pic);
        } catch (Resources.NotFoundException e) {
            e.printStackTrace();
            Picasso.with(context)
                    .load(R.drawable.ic_logo)
                    .error(R.drawable.ic_logo)
                    .placeholder(R.drawable.ic_logo)
                    .transform(new CircleTransform())
                    .fit()
                    .into(img_basic_profile_profile_pic);
        }
        /*Picasso.with(context)
                .load(R.drawable.ic_logo_svg)
                .error(R.drawable.ic_logo_svg)
                .placeholder(R.drawable.ic_logo_svg)
                .transform(new CircleTransform())
                .fit()
                .into(img_basic_profile_profile_pic);*/

        rlimgupload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                bottomSheetDialog = new BottomSheetDialog(context);

                View view1 = getLayoutInflater().inflate(R.layout.custom_bottom_sheet_profile_pic, null);

                bottomSheetDialog.setContentView(view1);

                Button btnCancel = view1.findViewById(R.id.buttonSheet1);
                btnCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        bottomSheetDialog.dismiss();
                    }
                });

                TextView tvViewPhoto = view1.findViewById(R.id.TextViewSheet1);
                tvViewPhoto.setVisibility(View.GONE);
                view1.findViewById(R.id.divider_viewphoto).setVisibility(View.GONE);

                TextView tvUploadPhoto = view1.findViewById(R.id.TextViewSheet2);
                tvUploadPhoto.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        bottomSheetDialog.dismiss();
                        pickFromGallery();
                    }
                });

                TextView tvTakePhoto = view1.findViewById(R.id.TextViewSheet3);
                tvTakePhoto.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        bottomSheetDialog.dismiss();
                        try {
                            if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED)
                                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CAMERA}, 1596);
                            else
                                launchCamera();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
                bottomSheetDialog.show();
            }
        });

        img_basic_profile_profile_pic.setRotation(rotateImage);

        tv_add_email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (view.getTag() != null && view.getTag().equals("1")) {
                    showSingleTextPopup(OnSingleEdit.TYPE_EMAIL, false);
                }
            }
        });

        //////****** Start Other Address ******///////

        tv_oa_add_info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Open_Place_Picker();
            }
        });
        tv_add_other_address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Open_Place_Picker();
            }
        });

        //////****** End Other Address ******///////


        tv_email_add_info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showSingleTextPopup(OnSingleEdit.TYPE_EMAIL, false);
            }
        });

        tv_add_phone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (view.getTag() != null && view.getTag().equals("1")) {
                    showSingleTextPopup(OnSingleEdit.TYPE_PHONE, false);
                }
            }
        });
        tv_phone_add_info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showSingleTextPopup(OnSingleEdit.TYPE_PHONE, false);
            }
        });

        tv_add_weblink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (view.getTag() != null && view.getTag().equals("1")) {
                    showSingleTextPopup(OnSingleEdit.TYPE_WEBLINK, false);
                }
            }
        });
        tv_weblink_add_info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showSingleTextPopup(OnSingleEdit.TYPE_WEBLINK, false);
            }
        });

        tv_add_socialmedia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (view.getTag() != null && view.getTag().equals("1")) {
                    showpopupSociamMedia(false);
                }
            }
        });

        tv_socialmedia_add_info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showpopupSociamMedia(false);
            }
        });

        tv_add_founders.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (view.getTag() != null && view.getTag().equals("1")) {
                    addFounders(-1, null);
                }
            }
        });

        tv_founders_add_info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addFounders(-1, null);
            }
        });

        edt_doestablishment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(),
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {

                                int month = monthOfYear + 1;
                                String fm = "" + month;
                                String fd = "" + dayOfMonth;
                                if (month < 10) {
                                    fm = "0" + month;
                                }
                                if (dayOfMonth < 10) {
                                    fd = "0" + dayOfMonth;
                                }

                                edt_doestablishment.setText(year + "-" + fm + "-" + fd);

                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.getDatePicker().setMaxDate(c.getTimeInMillis());
                datePickerDialog.show();
            }
        });

        edt_dob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(),
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {

                                int month = monthOfYear + 1;
                                String fm = "" + month;
                                String fd = "" + dayOfMonth;
                                if (month < 10) {
                                    fm = "0" + month;
                                }
                                if (dayOfMonth < 10) {
                                    fd = "0" + dayOfMonth;
                                }

                                edt_dob.setText(year + "-" + fm + "-" + fd);

                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.getDatePicker().setMaxDate(c.getTimeInMillis());
                datePickerDialog.show();
            }
        });


        // Email
        //recyclerView_email.addItemDecoration(new SimpleDividerItemDecoration(context));
        recyclerView_email.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(context);
        recyclerView_email.setLayoutManager(mLayoutManager);

        listEmail = new ArrayList<>();
        adpSingleTextEmail = new AdpSingleEmail(context, listEmail, this, true);
        recyclerView_email.setAdapter(adpSingleTextEmail);
        tv_add_email.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        tv_add_email.setTag("0");
        //

        // Phone
        //recyclerView_phone.addItemDecoration(new SimpleDividerItemDecoration(context));
        recyclerView_phone.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(context);
        recyclerView_phone.setLayoutManager(mLayoutManager);

        listPhones = new ArrayList<>();
        adpSingleTextPhone = new AdpSinglePhone(context, listPhones, this, true);
        recyclerView_phone.setAdapter(adpSingleTextPhone);
        tv_add_phone.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        tv_add_phone.setTag("0");
        //

        // Web Link
        //recyclerView_weblink.addItemDecoration(new SimpleDividerItemDecoration(context));
        recyclerView_weblink.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(context);
        recyclerView_weblink.setLayoutManager(mLayoutManager);

        listWeblink = new ArrayList<>();
        adpSingleTextWeblink = new AdpSingleWebLink(context, listWeblink, this, true, "0", "0");
        recyclerView_weblink.setAdapter(adpSingleTextWeblink);
        tv_add_weblink.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        tv_add_weblink.setTag("0");
        //

        // Other Address
        //recyclerView_oa.addItemDecoration(new SimpleDividerItemDecoration(context));
        recyclerView_oa.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(context);
        recyclerView_oa.setLayoutManager(mLayoutManager);

        other_address = new ArrayList<>();
        adpOtherAddress = new AdpOtherAddress(context, other_address, this, true, posadapwet);
        recyclerView_oa.setAdapter(adpOtherAddress);
        tv_add_other_address.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        tv_add_other_address.setTag("0");

        // Social Media
        //recyclerView_socialmedia.addItemDecoration(new SimpleDividerItemDecoration(context));
        recyclerView_socialmedia.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(context);
        recyclerView_socialmedia.setLayoutManager(mLayoutManager);

        socialMediumList = new ArrayList<>();
        adpSocialMedium = new AdpSocialMedium(context, socialMediumList, this, true, "0", "0");
        recyclerView_socialmedia.setAdapter(adpSocialMedium);
        tv_add_socialmedia.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        tv_add_socialmedia.setTag("0");
        //

        // Founders
        //recyclerView_founders.addItemDecoration(new SimpleDividerItemDecoration(context));
        recyclerView_founders.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(context);
        recyclerView_founders.setLayoutManager(mLayoutManager);

        foundersList = new ArrayList<>();
        adpfounders = new AdpFounders(context, foundersList, this, true);
        recyclerView_founders.setAdapter(adpfounders);
        tv_add_founders.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        tv_add_founders.setTag("0");
        //

        edt_address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
                    startActivityForResult(builder.build(getActivity()), PLACE_PICKER_REQUEST);
                    edt_address.setClickable(false);
                    edt_address.setEnabled(false);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    getPosForAddress posadapwet = new getPosForAddress() {
        @Override
        public void getPOsforAddress(int pos, String type) {
            if (type.equalsIgnoreCase("DELETE")) {
                DeleteAddress(pos);
            } else if (type.equalsIgnoreCase("ADDRESS")) {
                Open_Place_Picker(pos);
            }
        }
    };

    private void DeleteAddress(int pos) {
        other_address.remove(pos);
        adpOtherAddress.notifyDataSetChanged();
        if (other_address != null && other_address.size() > 0) {
            recyclerView_oa.setVisibility(View.VISIBLE);
            tv_oa_add_info.setVisibility(View.GONE);
            tv_add_other_address.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_add_black_24dp, 0);
        } else {
            recyclerView_oa.setVisibility(View.GONE);
            tv_oa_add_info.setVisibility(View.VISIBLE);
            //tv_add_other_address.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_add_black_24dp, 0);
        }
    }

    public void Open_Place_Picker(int pos) {
        other_add_pos = pos;
        try {
            PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
            startActivityForResult(builder.build(getActivity()), PLACE_PICKER_REQUEST_ADDRESS);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void Open_Place_Picker() {
        other_add_pos = -1;
        try {
            PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
            startActivityForResult(builder.build(getActivity()), PLACE_PICKER_REQUEST_ADDRESS);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void getEditFounder(int pos, Founder founder, int action) {
        if (action == OnSingleEdit.DELETE) {
            foundersList.remove(pos);
            if (adpfounders != null)
                adpfounders.notifyDataSetChanged();
            if (foundersList != null && foundersList.size() == 0) {
                tv_founders_add_info.setVisibility(View.VISIBLE);
                tv_add_founders.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                tv_add_founders.setTag("0");
            }
        } else if (action == OnSingleEdit.EDIT) {
            addFounders(pos, founder);
        }
    }

    @Override
    public void getEditMedia(int pos, SocialMedium socialMedium, int action) {
        if (action == OnSingleEdit.DELETE) {
            socialMediumList.remove(pos);
            if (adpSocialMedium != null)
                adpSocialMedium.notifyDataSetChanged();
            if (socialMediumList != null && socialMediumList.size() == 0) {
                tv_socialmedia_add_info.setVisibility(View.VISIBLE);
                tv_add_socialmedia.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                tv_add_socialmedia.setTag("0");
            }
        } else if (action == OnSingleEdit.EDIT) {
            editsocialMedium = socialMedium;
            editPos = pos;
            showpopupSociamMedia(true);
        }
    }

    @Override

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == 1596) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                launchCamera();
            } else {
                Toast.makeText(getActivity(), "permission required to open camera", Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void getEditSingle(int pos, String data, int action, int type) {
        if (action == OnSingleEdit.DELETE) {
            if (type == OnSingleEdit.TYPE_EMAIL) {
                listEmail.remove(pos);
                if (adpSingleTextEmail != null)
                    adpSingleTextEmail.notifyDataSetChanged();
                if (listEmail != null && listEmail.size() == 0) {
                    tv_email_add_info.setVisibility(View.VISIBLE);
                    tv_add_email.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                    tv_add_email.setTag("0");
                }

            } else if (type == OnSingleEdit.TYPE_PHONE) {
                listPhones.remove(pos);
                if (adpSingleTextPhone != null)
                    adpSingleTextPhone.notifyDataSetChanged();
                if (listPhones != null && listPhones.size() == 0) {
                    tv_phone_add_info.setVisibility(View.VISIBLE);
                    tv_add_phone.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                    tv_add_phone.setTag("0");
                }

            } else if (type == OnSingleEdit.TYPE_WEBLINK) {
                listWeblink.remove(pos);
                if (adpSingleTextWeblink != null)
                    adpSingleTextWeblink.notifyDataSetChanged();
                if (listWeblink != null && listWeblink.size() == 0) {
                    tv_weblink_add_info.setVisibility(View.VISIBLE);
                    tv_add_weblink.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                    tv_add_weblink.setTag("0");
                }

            }
        } else if (action == OnSingleEdit.EDIT) {
            editString = data;
            editPos = pos;
            showSingleTextPopup(type, true);
        }
    }

    String Text_message = "";

    public void showpopupSociamMedia(final boolean isEdit) {

        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        View view = LinearLayout.inflate(context, R.layout.dialog_addsocialmedia, null);
        dialog.setContentView(view);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;

        dialog.show();
        dialog.getWindow().setAttributes(lp);
        TextView tvAlert = view.findViewById(R.id.tvAlert);
        TextView tvDone = view.findViewById(R.id.tvDone);
        TextView tvCancel = view.findViewById(R.id.tvCancel);

        view.findViewById(R.id.divider_select_socialmedia).setVisibility(View.VISIBLE);

        tvAlert.setText("Add Social Media");

        final SearchableSpinner spinnerSocialmedia = view.findViewById(R.id.spinnerSocialMedia);

        edtSocialmediaLInk = view.findViewById(R.id.edt_socialmedia_link);

        String items[] = {"Facebook", "LinkedIn", "Twitter", "Instagram", "Google Plus"};
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, items);
        spinnerSocialmedia.setTitle("Select Social Media");
        spinnerSocialmedia.setAdapter(spinnerArrayAdapter);

        if (isEdit && editsocialMedium != null) {
            edtSocialmediaLInk.setText(editsocialMedium.getName());
            int media_type = Integer.parseInt(editsocialMedium.getType());
            spinnerSocialmedia.setSelection(media_type - 1);
        }

        spinnerSocialmedia.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                //Toast.makeText(getActivity(), spinnerSocialmedia.getSelectedItem().toString() + "", Toast.LENGTH_SHORT).show();
                int pos = spinnerSocialmedia.getSelectedItemPosition();
                Text_message = getSocialMediaText(pos);
                edtSocialmediaLInk.setText(Text_message + "");
                Selection.setSelection(edtSocialmediaLInk.getText(), edtSocialmediaLInk.getText().length());
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        int pos = spinnerSocialmedia.getSelectedItemPosition();
        Text_message = getSocialMediaText(pos);
        edtSocialmediaLInk.setText(Text_message + "");
        Selection.setSelection(edtSocialmediaLInk.getText(), edtSocialmediaLInk.getText().length());

        edtSocialmediaLInk.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!s.toString().startsWith(Text_message + "")) {
                    edtSocialmediaLInk.setText(Text_message + "");
                    Selection.setSelection(edtSocialmediaLInk.getText(), edtSocialmediaLInk.getText().length());
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        tvDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                Utilities.hidekeyboard(context, getView());
                if (!edtSocialmediaLInk.getText().toString().isEmpty() && isValidUrl(edtSocialmediaLInk.getText().toString())) {
                    int pos = spinnerSocialmedia.getSelectedItemPosition();
                    if (edtSocialmediaLInk != null && !Utilities.isEmpty(edtSocialmediaLInk.getText().toString())) {
                        Utilities.showToast(context, "Social media link should not empty");
                    } else if (!isEdit && isAlreadySocialAdded("" + (pos + 1))) {
                        String message = "Social Media ";
                        switch (pos) {
                            case 0:
                                message = message + "Facebook Already Added";
                                break;
                            case 1:
                                message = message + "LinkedIn Already Added";
                                break;
                            case 2:
                                message = message + "Twitter Already Added";
                                break;
                            case 3:
                                message = message + "Instagram Already Added";
                                break;
                            case 4:
                                message = message + "Google Plus Already Added";
                                break;
                        }
                        Utilities.showToast(context, message);
                    } else {
                        SocialMedium socialMedium = new SocialMedium();
                        socialMedium.setName(edtSocialmediaLInk.getText().toString());
                        switch (pos) {
                            case 0:
                                socialMedium.setType(Constants.SOCIAL_FACEBOOK_ID);
                                break;
                            case 1:
                                socialMedium.setType(Constants.SOCIAL_LINKEDIN_ID);
                                break;
                            case 2:
                                socialMedium.setType(Constants.SOCIAL_TWITTER_ID);
                                break;
                            case 3:
                                socialMedium.setType(Constants.SOCIAL_INSTAGRAM_ID);
                                break;
                            case 4:
                                socialMedium.setType(Constants.SOCIAL_GOOGLE_PLUS);
                                break;
                        }

                        if (isEdit) {
                            socialMediumList.set(editPos, socialMedium);
                        } else {
                            socialMediumList.add(socialMedium);
                        }
                        tv_socialmedia_add_info.setVisibility(View.GONE);
                        tv_add_socialmedia.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_add_black_24dp, 0);
                        tv_add_socialmedia.setTag("1");
                        recyclerView_socialmedia.setVisibility(View.VISIBLE);
                        if (adpSocialMedium != null)
                            adpSocialMedium.notifyDataSetChanged();
                    }
                } else {
                    Utilities.showToast(context, "Please enter valid URL");
                }
            }
        });

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private String getSocialMediaText(int pos) {
        String Text_message = "";
        switch (pos) {
            case 0:
                // Facebook
                Text_message = Constants.FACEBOOK_URL;
                break;
            case 1:
                // Linkedin
                Text_message = Constants.LINKEDIN_URL;
                break;
            case 2:
                //  Twitter
                Text_message = Constants.TWITTER_URL;
                break;
            case 3:
                //  Instagram
                Text_message = Constants.INSTA_URL;
                break;
            case 4:
                //  Google Plus
                Text_message = Constants.GOOGLE_PLUS_URL;
                break;
        }
        return Text_message;
    }

    public boolean isAlreadySocialAdded(String type) {
        if (socialMediumList != null && socialMediumList.size() > 0) {

            for (SocialMedium socialMedium : socialMediumList) {
                if (socialMedium.getType().equals(type)) {
                    return true;
                }
            }
        }

        return false;
    }

    public void addFounders(int pos, Founder founder) {
        if (founder != null) {
            startActivityForResult(new Intent(context, AddFoundersActivity.class)
                    .putExtra("data", founder)
                    .putExtra("pos", pos), REQUEST_SELECT_ADDFOUNDERS);
        } else {
            startActivityForResult(new Intent(context, AddFoundersActivity.class), REQUEST_SELECT_ADDFOUNDERS);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Utilities.hidekeyboard(context, getView());
        if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_SELECT_ADDFOUNDERS) {
            int index = data.getExtras().getInt("index");
            if (index != -1) {
                // EditData
                Founder founder = data.getParcelableExtra("data");
                foundersList.set(index, founder);
                tv_founders_add_info.setVisibility(View.GONE);
                tv_add_founders.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_add_black_24dp, 0);
                tv_add_founders.setTag("1");
                recyclerView_founders.setVisibility(View.VISIBLE);
                if (adpfounders != null)
                    adpfounders.notifyDataSetChanged();
            } else {
                // Add CreateProfileData
                Founder founder = data.getParcelableExtra("data");
                foundersList.add(founder);
                tv_founders_add_info.setVisibility(View.GONE);
                tv_add_founders.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_add_black_24dp, 0);
                tv_add_founders.setTag("1");
                recyclerView_founders.setVisibility(View.VISIBLE);
                if (adpfounders != null)
                    adpfounders.notifyDataSetChanged();
            }
        } else if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_SELECT_PICTURE) {
            fileUri = data.getData();
            file = new File(fileUri.getPath());
            if (fileUri != null) {
                try {
                    String compressImg = new CompressImage(context).compressImage("" + fileUri);
                    Uri compressUri = Uri.fromFile(new File(compressImg));
                    startCropActivity(compressUri);
                } catch (Exception e) {
                    startCropActivity(fileUri);
                    e.printStackTrace();
                }
            } else {
                Utilities.showToast(context, "" + R.string.toast_cannot_retrieve_selected_image);
            }
        } else if (resultCode == Activity.RESULT_OK && requestCode == PICK_FROM_CAMERA) {
            if (fileUri != null) {
                startCropActivity(fileUri);
            } else {
                Utilities.showToast(context, "" + R.string.toast_cannot_retrieve_selected_image);
            }

        } else if (requestCode == UCrop.REQUEST_CROP) {
            handleCropResult(data);
        } else if (requestCode == PLACE_PICKER_REQUEST) {
            edt_address.setClickable(true);
            edt_address.setEnabled(true);
            if (resultCode == Activity.RESULT_OK) {
                Place place = PlacePicker.getPlace(context, data);
                String toastMsg = "";
                if (place.getAddress() != null && !place.getAddress().toString().equalsIgnoreCase("") && !place.getAddress().toString().equalsIgnoreCase("null"))
                    toastMsg = String.format("%s", place.getAddress());
                else
                    toastMsg = "Unknown";
                edt_address.setText(toastMsg);
                double latitude = place.getLatLng().latitude;
                double longtitude = place.getLatLng().longitude;
                edt_address.setTag("" + latitude + "," + longtitude);
            }
        } else if (requestCode == PLACE_PICKER_REQUEST_ADDRESS) {
            if (resultCode == Activity.RESULT_OK) {
                if (other_add_pos == -1) {
                    OtherAddresModel otherAddresModel = new OtherAddresModel();
                    Place place = PlacePicker.getPlace(context, data);
                    String toastMsg = "";
                    if (place.getAddress() != null && !place.getAddress().toString().equalsIgnoreCase("") && !place.getAddress().toString().equalsIgnoreCase("null"))
                        toastMsg = String.format("%s", place.getAddress());
                    else
                        toastMsg = "Unknown";
                    double latitude = place.getLatLng().latitude;
                    double longtitude = place.getLatLng().longitude;
                    otherAddresModel.setAddress(toastMsg);
                    if (place.getAddress() != null) {
                        otherAddresModel.setSubadd(place.getAddress().toString());
                    }
                    otherAddresModel.setLati(latitude);
                    otherAddresModel.setLongi(longtitude);
                    other_address.add(otherAddresModel);
                    adpOtherAddress.notifyDataSetChanged();
                    recyclerView_oa.setVisibility(View.VISIBLE);
                    tv_oa_add_info.setVisibility(View.GONE);
                    tv_add_other_address.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_add_black_24dp, 0);
                } else {
                    OtherAddresModel otherAddresModel = new OtherAddresModel();
                    Place place = PlacePicker.getPlace(context, data);
                    String toastMsg = "";
                    if (place.getAddress() != null && !place.getAddress().toString().equalsIgnoreCase("") && !place.getAddress().toString().equalsIgnoreCase("null"))
                        toastMsg = String.format("%s", place.getAddress());
                    else
                        toastMsg = "Unknown";
                    double latitude = place.getLatLng().latitude;
                    double longtitude = place.getLatLng().longitude;
                    otherAddresModel.setAddress(toastMsg);
                    if (place.getAddress() != null) {
                        otherAddresModel.setSubadd(place.getAddress().toString());
                    }
                    otherAddresModel.setLati(latitude);
                    otherAddresModel.setLongi(longtitude);
                    other_address.set(other_add_pos, otherAddresModel);
                    adpOtherAddress.notifyDataSetChanged();
                    recyclerView_oa.setVisibility(View.VISIBLE);
                    tv_oa_add_info.setVisibility(View.GONE);
                    tv_add_other_address.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_add_black_24dp, 0);
                }
            }
        } else if (requestCode == UCrop.REQUEST_CROP) {
        }
    }

    private void pickFromGallery() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN
                && ActivityCompat.checkSelfPermission(context, android.Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE},
                    REQUEST_STORAGE_READ_ACCESS_PERMISSION);

        } else {
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            intent.addCategory(Intent.CATEGORY_OPENABLE);
            startActivityForResult(Intent.createChooser(intent, getString(R.string.label_select_picture)), REQUEST_SELECT_PICTURE);
        }
    }

    private void handleCropResult(@NonNull Intent result) {
        if (result != null) {
            final Uri resultUri = UCrop.getOutput(result);
            if (resultUri != null) {
                try {
                    final InputStream imageStream = CreateProfileActivity.context.getContentResolver().openInputStream(resultUri);
                    selectedImage = BitmapFactory.decodeStream(imageStream);
                    int orientation = Utilities.getCameraPhotoOrientation(CreateProfileActivity.context, resultUri, file.getPath());
                    img_basic_profile_profile_pic.setImageBitmap(selectedImage);
                    img_basic_profile_profile_pic.setRotation(orientation);

                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                Utilities.showToast(getActivity(), "" + R.string.toast_cannot_retrieve_cropped_image);
            }
        }
    }

    private void startCropActivity(@NonNull Uri uri) {
        String destinationFileName = "" + System.currentTimeMillis();
        destinationFileName += ".png";
        UCrop uCrop = UCrop.of(uri, Uri.fromFile(new File(context.getCacheDir(), destinationFileName)));
        uCrop.withAspectRatio(1, 1);
        uCrop.start(getActivity());
    }

    public void launchCamera() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        file = new File(context.getExternalCacheDir(), String.valueOf(System.currentTimeMillis()) + ".jpg");

        fileUri = Uri.fromFile(file);

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
            intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
        } else {
            file = new File(file.getPath());
            Uri photoUri = FileProvider.getUriForFile(context.getApplicationContext(), context.getApplicationContext().getPackageName() + ".provider", file);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
        }
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        if (intent.resolveActivity(context.getApplicationContext().getPackageManager()) != null) {
            startActivityForResult(intent, PICK_FROM_CAMERA);
        }
    }

    public static void HideChkbox() {
        rel_check_box.setVisibility(View.GONE);
    }

    public static void ViewChkbox() {
        rel_check_box.setVisibility(View.VISIBLE);
    }

    public CreateProfileData getProfileInfo(CreateProfileData createProfileData) {

        String str_about = edt_about.getText().toString();
        String str_address = edt_address.getText().toString();
        String str_teamsize = edt_teamsize.getText().toString();
        String str_establishdate = edt_doestablishment.getText().toString();
        String str_currentceo = edt_currentceo.getText().toString();
        //String str_currentcfo = edt_currentcfo.getText().toString();
        String str_dob = edt_dob.getText().toString();

        if (selectedImage != null) {
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            selectedImage.compress(Bitmap.CompressFormat.JPEG, 100, stream);
            byte[] byteArray = stream.toByteArray();
            String strBase64 = Base64.encodeToString(byteArray, 0);
            Log.d("ss", strBase64);
            createProfileData.setLogo(strBase64);
        }

        if (str_about != null && Utilities.isEmpty(str_about)) {
            createProfileData.setAbout(str_about);
        }

        if (chk_forms != null) {
            createProfileData.setIscheckedBox(chk_forms.isChecked());
        } else {
            createProfileData.setIscheckedBox(false);
        }

        if (str_address != null && Utilities.isEmpty(str_address)) {
            createProfileData.setAddress(str_address);

            if (edt_address.getTag() != null && Utilities.isEmpty(edt_address.getTag().toString())) {
                createProfileData.setLatitude(edt_address.getTag().toString().split(",")[0]);
                createProfileData.setLongitude(edt_address.getTag().toString().split(",")[1]);
            }
        }

        if (str_establishdate != null && Utilities.isEmpty(str_establishdate)) {
            createProfileData.setEstablishDate(str_establishdate);
        }

        if (str_teamsize != null && Utilities.isEmpty(str_teamsize)) {
            createProfileData.setCompanyTeamSize(Integer.parseInt(str_teamsize));
        }

        if (str_currentceo != null && Utilities.isEmpty(str_currentceo)) {
            createProfileData.setCurrent_ceo(str_currentceo);
        }

        /*if (str_currentcfo != null && Utilities.isEmpty(str_currentcfo)) {
            createProfileData.setCurrent_cfo(str_currentcfo);
        }*/

        if (str_dob != null && Utilities.isEmpty(str_dob)) {
            createProfileData.setDob(str_dob);
        }

        if (listEmail != null && listEmail.size() > 0) {
            createProfileData.setEmail(listEmail);
        } else {
            createProfileData.setEmail(new ArrayList<Email>());
        }

        if (listPhones != null && listPhones.size() > 0) {
            createProfileData.setPhone(listPhones);
        } else {
            createProfileData.setPhone(new ArrayList<Phone>());
        }

        if (listWeblink != null && listWeblink.size() > 0) {
            createProfileData.setWeblink(listWeblink);
        } else {
            createProfileData.setWeblink(new ArrayList<Weblink>());
        }

        if (other_address != null && other_address.size() > 0) {
            createProfileData.setOtherAddress(other_address);
        } else {
            createProfileData.setOtherAddress(new ArrayList<OtherAddresModel>());
        }

        if (socialMediumList != null && socialMediumList.size() > 0) {
            createProfileData.setSocialMedia(socialMediumList);
        } else {
            createProfileData.setSocialMedia(new ArrayList<SocialMedium>());
        }

        if (foundersList != null && foundersList.size() > 0) {
            createProfileData.setFounders(foundersList);
        } else {
            createProfileData.setFounders(new ArrayList<Founder>());
        }

        return createProfileData;
    }

    String message = "";

    public void showSingleTextPopup(int type, final boolean isEdit) {

        final int field_type = type;
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        View view = LinearLayout.inflate(context, R.layout.dialog_addsocialmedia, null);
        dialog.setContentView(view);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;

        dialog.show();
        dialog.getWindow().setAttributes(lp);
        TextView tvAlert = view.findViewById(R.id.tvAlert);
        TextView tvDone = view.findViewById(R.id.tvDone);
        TextView tvCancel = view.findViewById(R.id.tvCancel);
        ccp = (CountryCodePicker) view.findViewById(R.id.ccp);
        LinearLayout lin_cpp = view.findViewById(R.id.lin_cpp);

        view.findViewById(R.id.divider_select_socialmedia).setVisibility(View.GONE);

        ccp.enablePhoneAutoFormatter(true);
        /* ccp.registerPhoneNumberTextView(edtSocialmediaLInk);*/

        ccp.setOnCountryChangeListener(new CountryCodePicker.OnCountryChangeListener() {
            @Override
            public void onCountrySelected(Country country) {

            }
        });

        final EditText edtSocialmediaLInk = view.findViewById(R.id.edt_socialmedia_link);

        if (field_type == 2) {
            edtSocialmediaLInk.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    if (edtSocialmediaLInk.getText().toString().length() == 1 && edtSocialmediaLInk.getText().toString().equalsIgnoreCase("0")) {
                        edtSocialmediaLInk.setText("");
                    } else {
                        edtSocialmediaLInk.setFilters(new InputFilter[]{filter1, new InputFilter.LengthFilter(15)});
                    }
                }

                @Override
                public void afterTextChanged(Editable editable) {
                }
            });
        }

        if (isEdit && editString != null && Utilities.isEmpty(editString)) {
            edtSocialmediaLInk.setText(editString);
            if (field_type == 2) {
                String CurrentString = editString;
                String[] separated = CurrentString.split(" ");
                ccp.setCountryForPhoneCode(Integer.parseInt(separated[0]));
                edtSocialmediaLInk.setText(separated[1]);
            }
        }

        if (field_type == 1) {
            message = "Email should not empty";
            tvAlert.setText("Add Email");
            edtSocialmediaLInk.setHint("Enter Email");
            edtSocialmediaLInk.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
            ccp.setVisibility(View.GONE);
            lin_cpp.setVisibility(View.GONE);
        } else if (field_type == 2) {
            ccp.setVisibility(View.VISIBLE);
            lin_cpp.setVisibility(View.VISIBLE);
            message = "Phone should not empty";
            tvAlert.setText("Add Phone");
            edtSocialmediaLInk.setHint("Enter Phone");
            edtSocialmediaLInk.setInputType(InputType.TYPE_CLASS_NUMBER);
        } else if (field_type == 3) {
            message = "Web link should not empty";
            tvAlert.setText("Add Web Link");
            edtSocialmediaLInk.setHint("Enter Web Link");
            edtSocialmediaLInk.setInputType(InputType.TYPE_CLASS_TEXT);
            ccp.setVisibility(View.GONE);
            lin_cpp.setVisibility(View.GONE);
        }

        view.findViewById(R.id.spinnerSocialMedia).setVisibility(View.GONE);

        tvDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Utilities.hidekeyboard(context, view);
                if (edtSocialmediaLInk != null && !Utilities.isEmpty(edtSocialmediaLInk.getText().toString())) {
                    dialog.dismiss();
                    Utilities.showToast(context, message);
                } else {
                    if (field_type == 1) {
                        dialog.dismiss();
/*
                        if (Utilities.isValidEmail(edtSocialmediaLInk.getText().toString())) {
*/

                        if (Utilities.isValidEmail(edtSocialmediaLInk.getText().toString())) {
                            Email email = new Email();
                            email.setName(edtSocialmediaLInk.getText().toString());
                            if (isEdit && editPos != -1)
                                listEmail.set(editPos, email);
                            else
                                listEmail.add(email);
                            tv_email_add_info.setVisibility(View.GONE);
                            if (listEmail != null && listEmail.size() == 5) {
                                tv_add_email.setTag("0");
                                tv_add_email.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                            } else {
                                tv_add_email.setTag("1");
                                tv_add_email.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_add_black_24dp, 0);
                            }
                            recyclerView_email.setVisibility(View.VISIBLE);
                            if (adpSingleTextEmail != null)
                                adpSingleTextEmail.notifyDataSetChanged();
                        } else Utilities.showToast(context, "Please enter valid email address");
                    } else if (field_type == 2) {
                        if (!isValidPhoneNumber(edtSocialmediaLInk.getText().toString())) {
                            Toast.makeText(getActivity(), "Please enter valid phone number", Toast.LENGTH_SHORT).show();
                        } else {
                            dialog.dismiss();
                            Phone email = new Phone();
                            email.setName("+" + ccp.getSelectedCountryCode().toString() + " " + edtSocialmediaLInk.getText().toString());
                            if (isEdit && editPos != -1)
                                listPhones.set(editPos, email);
                            else
                                listPhones.add(email);
                            tv_phone_add_info.setVisibility(View.GONE);
                            if (listPhones != null && listPhones.size() == 5) {
                                tv_add_phone.setTag("0");
                                tv_add_phone.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                            } else {
                                tv_add_phone.setTag("1");
                                tv_add_phone.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_add_black_24dp, 0);
                            }
                            recyclerView_phone.setVisibility(View.VISIBLE);
                            if (adpSingleTextPhone != null)
                                adpSingleTextPhone.notifyDataSetChanged();
                        }
                    } else if (field_type == 3) {
                        dialog.dismiss();
                        if (!edtSocialmediaLInk.getText().toString().isEmpty() && isValidUrl(edtSocialmediaLInk.getText().toString())) {
                            Weblink email = new Weblink();
                            email.setName(edtSocialmediaLInk.getText().toString());
                            if (isEdit && editPos != -1)
                                listWeblink.set(editPos, email);
                            else
                                listWeblink.add(email);
                            tv_weblink_add_info.setVisibility(View.GONE);
                            if (listWeblink != null && listWeblink.size() == 5) {
                                tv_add_weblink.setTag("0");
                                tv_add_weblink.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                            } else {
                                tv_add_weblink.setTag("1");
                                tv_add_weblink.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_add_black_24dp, 0);
                            }
                            recyclerView_weblink.setVisibility(View.VISIBLE);
                            if (adpSingleTextWeblink != null)
                                adpSingleTextWeblink.notifyDataSetChanged();
                        } else {
                            Utilities.showToast(context, "Please enter valid WebLink");
                        }
                    }
                }
            }
        });

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    InputFilter filter1 = new InputFilter() {
        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
            for (int i = start; i < end; ++i) {
                if (!Pattern.compile("[0123456789]*").matcher(String.valueOf(source.charAt(i))).matches()) {
                    return "";
                }
            }
            return null;
        }
    };

    private boolean isValidPhoneNumber(CharSequence phoneNumber) {
        String num = phoneNumber.toString();
        if (num.startsWith("1") || num.startsWith("2") || num.startsWith("3") || num.startsWith("4") || num.startsWith("5") || num.startsWith("6") || num.startsWith("7") || num.startsWith("8") || num.startsWith("9")) {
            return true;
        } else {
            return false;
        }
    }

    private boolean isValidUrl(String url) {
        Pattern p = Patterns.WEB_URL;
        Matcher m = p.matcher(url.toLowerCase());
        if (m.matches())
            return true;
        else
            return false;
    }
}
