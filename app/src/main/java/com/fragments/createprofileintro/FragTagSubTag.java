package com.fragments.createprofileintro;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.models.createprofile.CreateProfileData;
import com.models.tagsdata.ModelTagData;
import com.models.tagsdata.ModelTagDetail;
import com.quickkonnect.R;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;
import com.utilities.Utilities;
import com.volleyapirequest.VolleyApiRequest;
import com.volleyapirequest.VolleyCallBack;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ZTLAB-10 on 14-02-2018.
 */

public class FragTagSubTag extends Fragment {

    private static Context context;
    private View rootView;
    public static List<String> listCategory, listSubcategory;
    public static List<ModelTagDetail> modelTagDetailList, modelSubTagDetailList;
    static ArrayAdapter<String> spinnerArrayAdapterCate, spinnerArrayAdapterSubCate;
    public static int type = 0, sub_tag_type = 0, parentId, id;
    public static SearchableSpinner autotv_tag_title;
    public static AutoCompleteTextView autotv_subtag_title;

    public static FragTagSubTag getInstance(Bundle bundle) {
        FragTagSubTag fragInformation = new FragTagSubTag();
        fragInformation.setArguments(bundle);
        return fragInformation;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.item_createprofilecategory, null);

        context = getActivity();

        initWidget();

        return rootView;
    }

    public void initWidget() {

        autotv_tag_title = rootView.findViewById(R.id.autotv_tag_title);
        autotv_subtag_title = rootView.findViewById(R.id.autotv_subtag_title);

        listCategory = new ArrayList<>();
        modelTagDetailList = new ArrayList<>();
        spinnerArrayAdapterCate = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, listCategory);
        autotv_tag_title.setTitle("Select Profile Category");
        autotv_tag_title.setAdapter(spinnerArrayAdapterCate);
        autotv_tag_title.setSelection(0, true);
        autotv_tag_title.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Log.e("OnItemSelected", "" + i);
                ModelTagDetail modelTagDetail = modelTagDetailList.get(i);
                if (modelTagDetail != null && modelTagDetail.getId() != null) {
                    autotv_tag_title.setTag(modelTagDetail.getId());
                    parentId = modelTagDetail.getId();
                    getTags();
                } else {
                    parentId = 0;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        listSubcategory = new ArrayList<>();
        modelSubTagDetailList = new ArrayList<>();
        spinnerArrayAdapterSubCate = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, listSubcategory);
        autotv_subtag_title.setHint("Select Profile Subcategory");
        autotv_subtag_title.setAdapter(spinnerArrayAdapterSubCate);
        autotv_subtag_title.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                ModelTagDetail modelTagDetail = modelSubTagDetailList.get(i);
                if (modelTagDetail != null && modelTagDetail.getId() != null) {
                    autotv_subtag_title.setTag(modelTagDetail.getId());
                    sub_tag_type = modelTagDetail.getId();
                }
            }
        });

        autotv_subtag_title.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Log.e("OnItemSelected", "" + i);
                ModelTagDetail modelTagDetail = modelSubTagDetailList.get(i);
                if (modelTagDetail != null && modelTagDetail.getId() != null) {
                    autotv_subtag_title.setTag(modelTagDetail.getId());
                    sub_tag_type = modelTagDetail.getId();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

//    public void setTags() {
//
//        listCategory = new ArrayList<>();
//        modelTagDetailList = new ArrayList<>();
//
//        RealmResults<TagData> tagslist = RealmController.with(getActivity()).getAllTags();
//        if (tagslist != null && tagslist.size() > 0) {
//            for (TagData adpTagsData : tagslist) {
//                ModelTagDetail modelTagDetail = new ModelTagDetail();
//                modelTagDetail.setId(adpTagsData.getId());
//                modelTagDetail.setName(adpTagsData.getName());
//                modelTagDetailList.add(modelTagDetail);
//
//                listCategory.add(adpTagsData.getName());
//            }
//
//            ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, listCategory);
//            autotv_tag_title.setTitle("Select Profile Category");
//            autotv_tag_title.setAdapter(spinnerArrayAdapter);
//            autotv_tag_title.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//                @Override
//                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
//                    Log.e("OnItemSelected", "" + i);
//                    ModelTagDetail modelTagDetail = modelTagDetailList.get(i);
//                    if (modelTagDetail != null && modelTagDetail.getId() != null) {
//                        autotv_tag_title.setTag(modelTagDetail.getId());
//                        //getTags(modelTagDetail.getId());
//                    }
//                }
//
//                @Override
//                public void onNothingSelected(AdapterView<?> adapterView) {
//
//                }
//            });
//        }
//    }

//    public void getTags() {
//        try {
//            CreateProfileData modelCreateProfile = new CreateProfileData();
//            JSONObject jsonObject = new JSONObject();
//            jsonObject.put("type", modelCreateProfile.getType());
//            jsonObject.put("parent_id", "0");
//            new VolleyApiRequest(context, true,
//                    VolleyApiRequest.REQUEST_BASEURL + VolleyApiRequest.API_TAGLIST,
//                    new VolleyCallBack() {
//                        @Override
//                        public void onResponse(JSONObject response) {
//                            try {
//                                if (response != null && response.length() > 0) {
//                                    ModelTagData modelTagData = new Gson().fromJson(response.toString(), ModelTagData.class);
//                                    if (modelTagData.getStatus() == 200) {
//                                        if (modelTagData.getData() != null && modelTagData.getData().size() > 0) {
//
//                                            RealmController.getInstance().clearAllTags();
//
//                                            for (ModelTagDetail modelTagDetail : modelTagData.getData()) {
//
//                                                TagData tagData = new TagData();
//                                                tagData.setId(modelTagDetail.getId());
//                                                tagData.setName(modelTagDetail.getName());
//
//                                                Realm realm = Realm.getDefaultInstance();
//                                                realm.beginTransaction();
//                                                realm.copyToRealm(tagData);
//                                                realm.commitTransaction();
//                                            }
//                                        }
//                                    } else
//                                        Utilities.showToast(context, modelTagData.getMsg());
//
//                                }
//                            } catch (Exception e) {
//                                e.printStackTrace();
//                            }
//                        }
//
//                        @Override
//                        public void onErrorResponse(VolleyError error) {
//                            VolleyApiRequest.showVolleyError(context, error);
//                        }
//                    }).enqueRequest(jsonObject, Request.Method.POST);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }

    public void fillCategories(List<ModelTagDetail> modelTagDetails, Integer tp) {
        type = tp;
        listCategory.clear();
        modelTagDetailList.clear();
        for (ModelTagDetail adpTagsData : modelTagDetails) {
            modelTagDetailList.add(adpTagsData);
            if (adpTagsData.getName() != null) {
                String name = adpTagsData.getName();
                if (name.substring(0, 1).equalsIgnoreCase(" "))
                    name = name.substring(1);
                listCategory.add(name);
            }
        }
        if (spinnerArrayAdapterCate != null) {
            spinnerArrayAdapterCate.notifyDataSetChanged();
            autotv_tag_title.setSelection(0);
        }

    }

    public void getTags() {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("parent_id", parentId);
            jsonObject.put("type", type);
            new VolleyApiRequest(context, false,
                    VolleyApiRequest.REQUEST_BASEURL + VolleyApiRequest.API_TAGLIST,
                    new VolleyCallBack() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                if (response != null && response.length() > 0) {
                                    ModelTagData modelTagData = new Gson().fromJson(response.toString(), ModelTagData.class);
                                    if (modelTagData.getStatus() == 200) {
                                        if (modelTagData.getData() != null && modelTagData.getData().size() > 0) {

                                            listSubcategory = new ArrayList<>();
                                            modelSubTagDetailList = new ArrayList<>();
                                            List<ModelTagDetail> modelTagDetail = modelTagData.getData();
                                            for (ModelTagDetail adpTagsData : modelTagDetail) {
                                                modelSubTagDetailList.add(adpTagsData);
                                                listSubcategory.add(adpTagsData.getName().replace(" ", ""));
                                            }

                                            ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1, listSubcategory);
                                            autotv_subtag_title.setHint("Select Profile Subcategory");
                                            autotv_subtag_title.setAdapter(spinnerArrayAdapter);
                                            autotv_subtag_title.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                                @Override
                                                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                                    Log.e("OnItemSelected", "" + i);
                                                    ModelTagDetail modelTagDetail = modelSubTagDetailList.get(i);
                                                    if (modelTagDetail != null && modelTagDetail.getId() != null) {
                                                        sub_tag_type = modelTagDetail.getId();
                                                        autotv_subtag_title.setTag(modelTagDetail.getId());
                                                    }
                                                }

                                                @Override
                                                public void onNothingSelected(AdapterView<?> adapterView) {

                                                }
                                            });
                                        }
                                    } else
                                        Utilities.showToast(context, modelTagData.getMsg());
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onErrorResponse(VolleyError error) {
                            VolleyApiRequest.showVolleyError(context, error);
                        }
                    }).enqueRequest(jsonObject, Request.Method.POST);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    // TODO Check it once, not getting id....

    public CreateProfileData getTagSubtag(CreateProfileData modelCreateProfile) {
        if (autotv_tag_title != null && autotv_subtag_title != null && autotv_subtag_title.getText() != null) {

            if (autotv_tag_title.getTag() != null && Utilities.isEmpty(autotv_tag_title.getTag().toString())) {
                modelCreateProfile.setTag(Integer.parseInt(autotv_tag_title.getTag().toString()));
            }

            if (autotv_subtag_title.getTag() != null) {
                modelCreateProfile.setSubTag(Integer.parseInt(autotv_subtag_title.getTag().toString()));
            } else {
                modelCreateProfile.setSubTag(-1);
                modelCreateProfile.setSubtagText(autotv_subtag_title.getText().toString());
            }
        }
        return modelCreateProfile;
    }
}

    /*private Integer getNew_Tag_Type(String s) {
        final int[] id = {0};
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("parent_id", parentId);
            jsonObject.put("type", type);
            jsonObject.put("name", s + "");
            new VolleyApiRequest(context, true,
                    VolleyApiRequest.REQUEST_BASEURL + VolleyApiRequest.API_CREATE_NEW_SUBCAT,
                    new VolleyCallBack() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                if (response != null && response.length() > 0) {
                                    JSONObject jobj = response.getJSONObject("data");
                                    id[0] = jobj.getInt("sub_tag_id");
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onErrorResponse(VolleyError error) {
                            VolleyApiRequest.showVolleyError(context, error);
                        }
                    }).enqueRequest(jsonObject, Request.Method.POST);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return id[0];
    }*/
