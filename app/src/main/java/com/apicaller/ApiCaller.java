package com.apicaller;

import android.content.Context;

public class ApiCaller {

    public Context context;
    public static ApiCaller apicaller;

    public ApiCaller(Context context) {
        this.context = context;
    }

    public ApiCaller getInstance(Context context) {
        if (apicaller == null)
            apicaller = new ApiCaller(context);
        return apicaller;
    }
}
