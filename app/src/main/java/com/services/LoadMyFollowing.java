package com.services;

import android.app.IntentService;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.realmtable.Following_contact;
import com.retrofitservice.ApiUtils;
import com.retrofitservice.SOService;
import com.utilities.QKPreferences;
import com.utilities.Utilities;
import com.volleyapirequest.VolleyApiRequest;
import com.volleyapirequest.VolleyCallBack;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import io.realm.Realm;

/**
 * Created by ZTLAB-10 on 13-05-2018.
 */

public class LoadMyFollowing extends IntentService {

    private SOService mService;
    private QKPreferences qkPreferences;
    RealmManager realmManager;

    public LoadMyFollowing() {
        super("SynUserData");
        realmManager = new RealmManager();
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        Log.e("200", "onHandleIntent");
        getFollowing();
    }

    public void getFollowing() {
        mService = ApiUtils.getSOService();
        qkPreferences = new QKPreferences(getApplicationContext());
        try {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("user_id", qkPreferences.getValuesInt(QKPreferences.USER_ID) + "");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            new VolleyApiRequest(getApplicationContext(), false, VolleyApiRequest.REQUEST_BASEURL + "api/profile/getAllfollowing",
                    new VolleyCallBack() {
                        @Override
                        public void onResponse(JSONObject response) {
                            parseFollowing(response);
                        }

                        @Override
                        public void onErrorResponse(VolleyError error) {
                        }
                    }).enqueRequest(jsonObject, Request.Method.POST);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void parseFollowing(JSONObject response) {
        try {
            String responseStatus = response.getString("status");
            if (responseStatus.equals("200")) {
                realmManager.realm.beginTransaction();
                realmManager.realm.clear(Following_contact.class);
                realmManager.realm.commitTransaction();
                JSONArray jsnArr = response.getJSONArray("data");
                if (jsnArr != null && jsnArr.length() > 0) {
                    for (int i = 0; i < jsnArr.length(); i++) {
                        JSONObject obj = jsnArr.getJSONObject(i);
                        Following_contact contacts = new Following_contact();
                        contacts.setId(obj.getString("id"));
                        contacts.setName(obj.getString("name"));
                        contacts.setStatus(obj.getString("status"));
                        contacts.setReason(obj.getString("reason"));
                        contacts.setType(obj.getString("type"));
                        contacts.setLogo(obj.getString("logo"));
                        contacts.setRole(obj.getString("role"));
                        contacts.setUnique_code(obj.getString("unique_code"));

                        JSONObject qktag = obj.getJSONObject("qk_tag_info");
                        contacts.setBorder_color(qktag.getString("border_color"));
                        contacts.setPattern_color(qktag.getString("pattern_color"));
                        contacts.setBackground_color(qktag.getString("background_color"));
                        contacts.setMiddle_tag(qktag.getString("middle_tag"));
                        contacts.setQr_code(qktag.getString("qr_code"));
                        contacts.setBackground_gradient(qktag.getString("background_gradient"));

                        contacts.setFollowed_at(obj.getString("followed_at"));
                        try {
                            String scanDate = obj.getString("followed_on");
                            if (Utilities.isEmpty(scanDate))
                                contacts.setFollowed_on(Utilities.changeDateFormat("yyyy-MM-dd hh:mm:ss", "EEE, dd MMM yyyy hh:mm", scanDate));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        updateFollowing_Contact(contacts);
                    }

                } else {
                    try {
                        realmManager.realm.beginTransaction();
                        realmManager.realm.clear(Following_contact.class);
                        realmManager.realm.commitTransaction();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        Intent intent = new Intent("BROADCAST_FROM_SCAN_QK");
        sendBroadcast(intent);

    }

    public void updateFollowing_Contact(Following_contact card) {

        boolean isUpdate = true;

        Following_contact toEdit = realmManager.realm.where(Following_contact.class).equalTo("id", card.getId()).findFirst();

        if (toEdit == null) {
            isUpdate = false;
            toEdit = new Following_contact();
        }
        realmManager.realm.beginTransaction();

        toEdit.setId(card.getId());

        if (card.getName() != null)
            toEdit.setName(card.getName());

        if (card.getStatus() != null)
            toEdit.setStatus(card.getStatus());

        if (card.getReason() != null)
            toEdit.setReason(card.getReason());

        if (card.getType() != null)
            toEdit.setType(card.getType());

        if (card.getUnique_code() != null)
            toEdit.setUnique_code(card.getUnique_code());

        if (card.getLogo() != null)
            toEdit.setLogo(card.getLogo());

        if (card.getRole() != null)
            toEdit.setRole(card.getRole());

        if (card.getBorder_color() != null)
            toEdit.setBorder_color(card.getBorder_color());

        if (card.getPattern_color() != null)
            toEdit.setPattern_color(card.getPattern_color());

        if (card.getBackground_color() != null)
            toEdit.setBackground_color(card.getBackground_color());

        if (card.getMiddle_tag() != null)
            toEdit.setMiddle_tag(card.getMiddle_tag());

        if (card.getQr_code() != null)
            toEdit.setQr_code(card.getQr_code());

        if (card.getBackground_gradient() != null)
            toEdit.setBackground_gradient(card.getBackground_gradient());

        if (card.getFollowed_on() != null)
            toEdit.setFollowed_on(card.getFollowed_on());

        if (card.getFollowed_at() != null)
            toEdit.setFollowed_at(card.getFollowed_at());

        if (!isUpdate)
            realmManager.realm.copyToRealm(toEdit);

        realmManager.realm.commitTransaction();
    }

    public class RealmManager {
        private Realm realm;

        public RealmManager() {
            realm = Realm.getDefaultInstance();
        }
    }
}
