package com.services;

import android.app.AlertDialog;
import android.app.IntentService;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.models.logindata.ModelLoggedUser;
import com.quickkonnect.R;
import com.quickkonnect.UpdateQkTagActivity;
import com.realmtable.MyProfiles;
import com.realmtable.RealmController;
import com.utilities.Constants;
import com.utilities.QKPreferences;
import com.utilities.Utilities;
import com.volleyapirequest.VolleyApiRequest;
import com.volleyapirequest.VolleyCallBack;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by ZTLAB-12 on 11-06-2018.
 */

public class CreateEmployeeProfileService extends IntentService {


    public CreateEmployeeProfileService() {
        super("");
    }

    QKPreferences qkPreferences;

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {

        qkPreferences = new QKPreferences(getApplicationContext());

        String qr_code = intent.getExtras().getString("QR_CODE");
        String user_id = intent.getExtras().getString("USER_ID");
        String profileid = intent.getExtras().getString("PROFILE_ID");

        functionGenerateQuicklyCOde(qr_code + "", user_id + "", profileid + "");

    }

    public void functionGenerateQuicklyCOde(final String qr_code, final String user_id, final String profileid) {

        String borderColor = "0xff000000";
        String backgroundColor = "0xffFFFFFF";
        String dataColor = "0xff191919";

        /*borderColor = borderColor.replace("#", "0xff");
        backgroundColor = backgroundColor.replace("#", "0xff");
        dataColor = dataColor.replace("#", "0xff");*/
        JSONObject obj = new JSONObject();
        try {
            obj.put("middle_tag", "A");
            obj.put("qr_code", qr_code);
            obj.put("border_color", borderColor);
            obj.put("background_color", backgroundColor);
            obj.put("pattern_color", dataColor);
            obj.put("user_id", "" + user_id.replace(".0", ""));
            obj.put("profile_id", "" + profileid.replace(".0", ""));
            obj.put("type", "3");
            obj.put("background_gradient", "#2ABBBE");

        } catch (JSONException e) {
            e.printStackTrace();
        }

        String url = VolleyApiRequest.REQUEST_BASEURL + "api/user_qr";
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, url, obj, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("Response", response + "");
                try {
                    String status = response.getString("status");
                    if (status.equals("200")) {
                        parseQKTagGenerateResponse(response);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //handle the error
                Log.d("Response", error.getLocalizedMessage() + "");
                error.printStackTrace();

            }
        }) {
            @Override
            public Map<String, String> getHeaders() {
                String token = "";
                if (qkPreferences != null) {
                    token = qkPreferences.getApiToken() + "";
                }

                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "Bearer " + token + "");
                params.put("Accept", "application/json");
                params.put("QK_ACCESS_KEY", getResources().getString(R.string.QK_ACCESS_KEY) + "");
                return params;
            }
        };
        Volley.newRequestQueue(this).add(jsonRequest);

       /* JSONObject obj = new JSONObject();
        try {
            obj.put("middle_tag", "A");
            obj.put("qr_code", qr_code);
            obj.put("border_color", borderColor);
            obj.put("background_color", backgroundColor);
            obj.put("pattern_color", dataColor);
            obj.put("user_id", "" + user_id.replace(".0", ""));
            obj.put("profile_id", "" + profileid.replace(".0", ""));
            obj.put("type", "3");
            obj.put("background_gradient", "#2ABBBE");

        } catch (JSONException e) {
            e.printStackTrace();
        }
        new VolleyApiRequest(getApplicationContext(), true, VolleyApiRequest.REQUEST_BASEURL + "api/user_qr",
                new VolleyCallBack() {
                    @Override
                    public void onResponse(JSONObject response) {
                        startService(new Intent(getApplicationContext(), LoadProfiles.class));
                        try {
                            String status = response.getString("status");
                            if (status.equals("200")) {
                                parseQKTagGenerateResponse(response);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onErrorResponse(VolleyError error) {
                    }
                }).enqueRequest(obj, Request.Method.POST);*/
    }

    public void parseQKTagGenerateResponse(JSONObject response) {

        try {
            String status = response.getString("status");
            if (status.equals("200")) {
                startService(new Intent(getApplicationContext(), LoadProfiles.class));

                JSONObject data = response.getJSONObject("data");

                String background_color = data.getString("background_color");
                String border_color = data.getString("border_color");
                String pattern_color = data.getString("pattern_color");
                String qr_code_url = data.getString("url");
                String qr_middle_tag = data.getString("middle_tag");
                String background_gradient = data.getString("background_gradient");

                String p_id = data.getString("profile_id");
                String name = data.getString("name");
                String type = data.getString("type");
                String logo = data.getString("logo");
                String unique_code = data.getString("unique_code");

                JSONObject jsonObject = new JSONObject();

                MyProfiles updateData = new MyProfiles();
                updateData.setId(p_id + "");
                updateData.setName(name + "");
                updateData.setBackground_gradient(background_gradient);
                updateData.setType(type + "");
                updateData.setLogo(logo + "");
                updateData.setUnique_code(unique_code + "");

                jsonObject.put("border_color", border_color);
                jsonObject.put("pattern_color", pattern_color);
                jsonObject.put("background_color", background_color);
                if (qr_middle_tag != null && Utilities.isEmpty(qr_code_url))
                    jsonObject.put("middle_tag", qr_middle_tag);
                else
                    jsonObject.put("middle_tag", "");
                jsonObject.put("qr_code", qr_code_url);
                jsonObject.put("background_gradient", background_gradient);

                updateData.setQktaginfo(jsonObject.toString());
                RealmController.with(getApplication()).updateMyProfiles(updateData);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
