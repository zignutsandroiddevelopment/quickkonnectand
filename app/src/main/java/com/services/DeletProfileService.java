package com.services;

import android.app.IntentService;
import android.content.Intent;
import android.support.annotation.Nullable;

import com.realmtable.RealmController;

/**
 * Created by ZTLAB-12 on 03-07-2018.
 */

public class DeletProfileService extends IntentService {

    public DeletProfileService() {
        super("");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        try {
            String unique_code = intent.getExtras().getString("UNIQUECODE");
            RealmController.with(getApplication()).deleteProfile(unique_code);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
