package com.services;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;

import com.OpenFire.XmppConnectionUtil;
import com.OpenFire.interfaces.XmppConnetionListener;
import com.OpenFire.interfaces.XmppLoginListener;
import com.OpenFire.entry.ContactEntry;
import com.utilities.QKPreferences;

import java.util.ArrayList;

/**
 * Created by ZTLAB-12 on 14-07-2018.
 */

public class BackgroundService extends Service {

    private QKPreferences qkPreferences;
    private ArrayList<ContactEntry> userContactEntries;
    XmppConnectionUtil xmppConnectionUtil;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        try {
            qkPreferences = new QKPreferences(getApplicationContext());

            xmppConnectionUtil = new XmppConnectionUtil(getApplicationContext());
            xmppConnectionUtil.Connect(xmppConnetionListener);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return START_STICKY;
    }


    XmppConnetionListener xmppConnetionListener = new XmppConnetionListener() {
        @Override
        public void onConnectionSuccessfull(String message) {
            qkPreferences.IsXmppConnected("1");
            String Uniquecode = qkPreferences.getValues(QKPreferences.USER_UNIQUECODE);
            xmppConnectionUtil.Login(Uniquecode, xmppLoginListener);
        }

        @Override
        public void onConnectionFailure(String message) {
            qkPreferences.IsXmppConnected("0");
        }
    };

    XmppLoginListener xmppLoginListener = new XmppLoginListener() {
        @Override
        public void onLoginSuccess(String message) {
            qkPreferences.IsXmppLogin("1");
        }

        @Override
        public void onloginFail(String message) {
            qkPreferences.IsXmppLogin("0");
        }
    };

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        //Toast.makeText(this, "ok done...!", Toast.LENGTH_SHORT).show();
        try {
            qkPreferences.IsXmppLogin("0");
            qkPreferences.IsXmppConnected("0");
            xmppConnectionUtil.Disconnect();
            Intent intent = new Intent(getApplicationContext(), BackgroundService.class);
            stopService(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onTaskRemoved(rootIntent);
    }


}



























































 /* public void doConnectAndLogin() {
        try {
            loginHandler = new Handler() {
                @Override
                public void handleMessage(Message msg) {
                    switch (msg.what) {
                        case CONNECTION_FAIL:
                            qkPreferences.IsXmppConnected("0");
                            break;
                        case CONNECTION_COMPLETE:
                            qkPreferences.IsXmppConnected("1");
                            break;
                        case LOGIN_FAIL:
                            qkPreferences.IsXmppLogin("0");
                            break;
                        case LOGIN_COMPLETE:
                            try {
                                qkPreferences.IsXmppLogin("1");
                                if (xmppConnection.isConnected()) {
                                    xmppConnection.addPacketListener(new PingPacketListener(xmppConnection), new PingPacketFilter());
                                }
                                // Send package to server to my status to "Available" (Online)
                                Presence userPresence = new Presence(Presence.Type.available);
                                xmppConnection.sendPacket(userPresence);

                                *//*VCard vcard = new VCard();
                                String Uniquecode = qkPreferences.getValues(QKPreferences.USER_UNIQUECODE);
                                String name = qkPreferences.getValues(QKPreferences.USER_FULLNAME);
                                String image_path = qkPreferences.getValues(QKPreferences.USER_PROFILEIMG);
                                URL url = new URL(image_path);
                                //ProviderManager.addIQProvider("vCard", "vcard-temp", new VCardProvider());
                                vcard.load(xmppConnection, Uniquecode + "@quickkonnect.com");
                                vcard.setNickName(name + "");
                                *//**//*vcard.setEmailHome("" + email);
                                vcard.setMiddleName("" + middleName);
                                vcard.setNickName("" + nickName);
                                vcard.setPhoneHome("Voice", "" + phoneNumber);
                                vcard.setLastName("" + lastName);
                                vcard.setOrganization("" + orginiZation);*//**//*
                                vcard.setAvatar(url); //Image Path should be URL or Can be Byte Array etc.
                                vcard.save(xmppConnection);*//*

                          *//*  // Get user information
                            // - user profile
                            userVCard = new VCard();
                            try {
                                userVCard.load(xmppConnection);
                                userNickName = userVCard.getNickName();
                                userAvatarByte = userVCard.getAvatar();
                                if (userNickName == null) {
                                    userDisplayNameText.setText(userJID);
                                } else {
                                    userDisplayNameText.setText(userNickName);
                                }
                                if (userAvatarByte != null) {
                                    userAvatarImage.setImageBitmap(BitmapUtility.convertByteArrayToBitmap(userAvatarByte));
                                } else {
                                    Toast.makeText(context, "You don't have avatar.", Toast.LENGTH_SHORT).show();
                                }
                            } catch (XMPPException e) {
                                Log.e(LOG_TAG, "Unable to load user VCard: " + e.getMessage());
                            }*//*

                                // - user's friends (Roster)
                                Roster.setDefaultSubscriptionMode(Roster.SubscriptionMode.accept_all);
                                userRoster = xmppConnection.getRoster();
                                // Store roster to collection

                                Collection<RosterEntry> rosterEntries = userRoster.getEntries();

                                if (rosterEntries.size() != 0) {
                                    for (RosterEntry rosterEntry : rosterEntries) {
                                        final VCard fVCard = new VCard();
                                        try {
                                            Log.i(LOG_TAG, "fJID: " + rosterEntry.getUser());
                                            fVCard.load(xmppConnection, rosterEntry.getUser());
                                            ContactEntry contactEntry = new ContactEntry();
                                            contactEntry.setJid(rosterEntry.getUser());
                                            // Check is friend online?
                                            Presence presence = userRoster.getPresence(rosterEntry.getUser());
                                            if (presence.isAvailable() || presence.isAway()) {
                                                String onlineFrom = presence.getFrom();
                                                String[] onlineFromResource = onlineFrom.split("/");
                                                contactEntry.setOnlineViaResource(onlineFromResource[1]);
                                                contactEntry.setOnline(true);
                                            } else {
                                                contactEntry.setOnline(false);
                                            }

                                            // Display name
                                            if (fVCard.getNickName() == null) {
                                                contactEntry.setNickname(rosterEntry.getUser());
                                            } else {
                                                contactEntry.setNickname(fVCard.getNickName());
                                            }

                                            // Avatar
                                            if (fVCard.getAvatar() == null) {
                                                contactEntry.setAvatarBitmap(null);
                                                contactEntry.setAvatarByte(null);
                                            } else {
                                                *//*contactEntry.setAvatarByte(fVCard.getAvatar());
                                                if (contactEntry.isOnline()) {
                                                    contactEntry.setAvatarBitmap(BitmapUtility.convertByteArrayToBitmap(contactEntry.getAvatarByte()));
                                                } else {
                                                    Bitmap normalAvatar = BitmapUtility.convertByteArrayToBitmap(contactEntry.getAvatarByte());
                                                    contactEntry.setAvatarBitmap(BitmapUtility.convertBitmapToGrayScaleBitmap(normalAvatar));
                                                }*//*
                                            }
                                            userContactEntries.add(contactEntry);
                                            *//*userContactAdapter.setData(userContactEntries);
                                            userContactAdapter.notifyDataSetChanged();*//*
                                        } catch (Exception e) {
                                            Log.e(LOG_TAG, "Unable to load friend VCard: " + e.getMessage());
                                        }
                                    }
                                    try {
                                        if (userContactEntries != null && userContactEntries.size() > 0) {
                                            RealmController.with(getApplication()).clearChatuserList();
                                            for (int i = 0; i < userContactEntries.size(); i++) {
                                                ChatUserList chatUserList = new ChatUserList();
                                                chatUserList.setJid(userContactEntries.get(i).getJid() + "");
                                                chatUserList.setNickname(userContactEntries.get(i).getNickname() + "");
                                                //chatUserList.setAvatarByte(userContactEntries.get(i).getAvatarByte().toString() + "");
                                                chatUserList.setAvatarBitmap(userContactEntries.get(i).getAvatarBitmap() + "");
                                                if (userContactEntries.get(i).isOnline())
                                                    chatUserList.setIsOnline("1");
                                                else
                                                    chatUserList.setIsOnline("0");
                                                chatUserList.setOnlineViaResource(userContactEntries.get(i).getOnlineViaResource() + "");
                                                RealmController.with(getApplication()).updateChatUser(chatUserList);
                                            }
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            break;
                    }
                }
            };

            loginThread = new
                    Thread() {
                        @Override
                        public void run() {
                            boolean isConnected;
                            // Trying connect to server
                            try {
                                // Setup required features
                                configure(ProviderManager.getInstance());
                                // Setup connection configuration
                                ConnectionConfiguration config = new ConnectionConfiguration(Constants.SERVER_IP, Constants.SERVER_PORT);
                                config.setSASLAuthenticationEnabled(true);
                                config.setSendPresence(false);
                                SASLAuthentication.supportSASLMechanism("PLAIN", 0);

                                // Setup XMPP connection from configuration
                                xmppConnection = new XMPPConnection(config);
                                xmppConnection.connect();
                                isConnected = true;
                                Log.i(LOG_TAG, "Connected to connect server.");
                                loginHandler.sendEmptyMessage(CONNECTION_COMPLETE);

                                XMPPLogic.getInstance().setConnection(xmppConnection);

                                try {
                                    String Uniquecode = qkPreferences.getValues(QKPreferences.USER_UNIQUECODE);
                                    if (Uniquecode != null && !Uniquecode.equalsIgnoreCase("") && !Uniquecode.equalsIgnoreCase("null")) {
                                        xmppConnection.login(Uniquecode + "", "123456", "Android");
                                        //userJID = xmppConnection.getUser();
                                        Log.i(LOG_TAG, "Logged in to connect server.");
                                        loginHandler.sendEmptyMessage(LOGIN_COMPLETE);
                                    } else {
                                        loginHandler.sendEmptyMessage(LOGIN_FAIL);
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                            } catch (XMPPException e) {
                                isConnected = false;
                                Log.e(LOG_TAG, "Unable to connect server: " + e.getMessage());
                                loginHandler.sendEmptyMessage(CONNECTION_FAIL);
                            }
                        }
                    };
            loginThread.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/
