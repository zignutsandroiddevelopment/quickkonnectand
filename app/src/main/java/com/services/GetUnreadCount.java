package com.services;

import android.app.IntentService;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.util.Log;

import com.Chat.ChatMessage;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;
import com.utilities.QKPreferences;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;

/**
 * Created by ZTLAB-12 on 06-06-2018.
 */

public class GetUnreadCount extends IntentService {

    Query myTopPostsQuery, newquery;
    int count = 0;
    private QKPreferences qkPreferences;
    String mCurrentUserId = "";

    public GetUnreadCount() {
        super(GetUnreadCount.class.getSimpleName());
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {

        qkPreferences = new QKPreferences(getApplicationContext());
        mCurrentUserId = qkPreferences.getValues(QKPreferences.USER_UNIQUECODE) + "";

        myTopPostsQuery = FirebaseDatabase.getInstance().getReference().child("conversation").orderByChild("time");
        newquery = FirebaseDatabase.getInstance().getReference().child("chat");

        //myTopPostsQuery.addChildEventListener(mChildEventListener);
        newquery.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    try {

                        JSONObject jsonObject = new JSONObject(new Gson().toJson(dataSnapshot.getValue()));
                        Iterator iterator = jsonObject.keys();
                        while (iterator.hasNext()) {

                            String key = (String) iterator.next();

                            String[] separated = key.split("-");

                            if (separated[0].matches(mCurrentUserId) || separated[1].matches(mCurrentUserId)) {
                                try {
                                    boolean isNeedVisible = false;
                                    JSONObject value = new JSONObject(jsonObject.get(key).toString());
                                    Iterator iterator1 = value.keys();
                                    while (iterator1.hasNext()) {
                                        String key1 = (String) iterator1.next();
                                        JSONObject value1 = new JSONObject(value.get(key1).toString());
                                        ChatMessage newMessage = new Gson().fromJson(value1.toString(), ChatMessage.class);
                                        Log.e("Data", "" + value);
                                        if (newMessage != null && newMessage.getFrom_id() != null && newMessage.getTo_id() != null) {
                                            if (newMessage.getFrom_id().equals(mCurrentUserId) || newMessage.getTo_id().equals(mCurrentUserId)) {
                                                //messageChatDatabase.child(dataSnapshot.getKey()).child("read1").setValue("1");
                                                if (newMessage.getRead1().equalsIgnoreCase("0")) {
                                                    count++;
                                                }
//                                                if (count > 0) {
//                                                    //  Visible
//                                                    sendMyBroadCast("1");
//                                                } else {
//                                                    //  Invisible
//                                                    sendMyBroadCast("0");
//                                                }
                                            }
                                        }
                                    }

                                } catch (JSONException e) {
                                    // Something went wrong!
                                }
                            }
                        }
//
                        if (count > 0) {
                            sendMyBroadCast("1");
                            count = 0;
                        } else {
                            sendMyBroadCast("0");
                            count = 0;
                        }

//
//                        ChatMessage newMessage = dataSnapshot.getValue(ChatMessage.class);
//                        if (newMessage != null && newMessage.getFrom_id() != null && newMessage.getTo_id() != null) {
//                            if (newMessage.getFrom_id().equals(mCurrentUserId) || newMessage.getTo_id().equals(mCurrentUserId)) {
//                                //messageChatDatabase.child(dataSnapshot.getKey()).child("read1").setValue("1");
//                                if (newMessage.getRead1().equalsIgnoreCase("0")) {
//                                    count++;
//                                }
//                                if (count > 0) {
//                                    //  Visible
//                                    sendMyBroadCast("1");
//                                } else {
//                                    //  Invisible
//                                    sendMyBroadCast("0");
//                                }
//                            }
//                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }

    private void sendMyBroadCast(String type) {
        try {
            Intent broadCastIntent = new Intent();
            broadCastIntent.setAction("CHATCOUNT");
            broadCastIntent.putExtra("data", type + "");
            sendBroadcast(broadCastIntent);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
