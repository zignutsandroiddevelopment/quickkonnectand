package com.services;

import android.app.IntentService;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.util.Log;

import com.models.contacts.ContactsPojo;
import com.models.contacts.Datum;
import com.models.logindata.ModelLoggedData;
import com.models.logindata.ModelLoggedUser;
import com.realmtable.Contacts;
import com.retrofitservice.ApiUtils;
import com.retrofitservice.SOService;
import com.utilities.Constants;
import com.utilities.QKPreferences;
import com.utilities.Utilities;

import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ZTLAB-10 on 13-05-2018.
 */

public class LoadMyContacts extends IntentService {

    private SOService mService;
    private QKPreferences qkPreferences;
    RealmManager realmManager;

    public LoadMyContacts() {
        super("SynUserData");
        realmManager = new RealmManager();
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        Log.e("200", "onHandleIntent");
        getContactsList();
    }

    public void getContactsList() {
        mService = ApiUtils.getSOService();
        qkPreferences = new QKPreferences(getApplicationContext());

        Map<String, String> headerParameters = Utilities.getHeaderParameter(getApplicationContext());

//        params.put("Authorization", "Bearer " + qkPreferences.getApiToken() + "");
//        params.put("Accept", "application/json");
//        params.put("QK_ACCESS_KEY", getResources().getString(R.string.QK_ACCESS_KEY) + "");

        mService.getContactsList(headerParameters, qkPreferences.getLoggedUserid(), TimeZone.getDefault().getID()).enqueue(new Callback<ContactsPojo>() {
            @Override
            public void onResponse(Call<ContactsPojo> call, Response<ContactsPojo> response) {
                Log.e("Contacts:Success", "" + response.body());
                try {
                    if (response != null && response.body() != null) {
                        ContactsPojo contactsPojo = response.body();
                        if (contactsPojo != null && contactsPojo.getStatus() == 200) {
                            parseContacts(response.body());
                        }
                    }
                } catch (Exception e) {
                }
            }

            @Override
            public void onFailure(Call<ContactsPojo> call, Throwable t) {
                Log.e("Contacts:Error", "" + t.getLocalizedMessage());
            }
        });
    }

    public void parseContacts(ContactsPojo contactsPojo) {
        List<Datum> datumList = contactsPojo.getData();
        if (datumList != null && datumList.size() > 0) {

            for (Datum datum: datumList) {

                Contacts contacts = new Contacts();

                contacts.setUser_id("" + datum.getScanUserId());
                contacts.setUsername(datum.getScanUserName());
                contacts.setProfile_pic(datum.getScanUserProfileUrl());
                contacts.setFirstname(datum.getFirstname());
                contacts.setLastname(datum.getLastname());
                contacts.setUnique_code(datum.getScanUserUniqueCode());
                contacts.setScanned_by(datum.getScannedBy());
                contacts.setPhone(datum.getPhone());
                contacts.setEmail(datum.getEmail());
                contacts.setProfile_id("" + datum.getProfileId());
                contacts.setProfile_name("" + datum.getProfileName());
                contacts.setContact_type(Constants.TYPE_CONTACTS);

                try {
                    String scanDate = datum.getScanDate();
                    if (Utilities.isEmpty(scanDate))
                        contacts.setScan_date(Utilities.changeDateFormat("yyyy-MM-dd hh:mm:ss", "dd-MM-yyyy", scanDate));

                } catch (Exception e) {
                    e.printStackTrace();
                }
                updateContacts(contacts);
            }
        } else {
            try {
                realmManager.realm.beginTransaction();
                realmManager.realm.clear(Contacts.class);
                realmManager.realm.commitTransaction();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        try {

            ModelLoggedUser modelLoggedUser = qkPreferences.getLoggedUser();
            ModelLoggedData modelLoggedData = modelLoggedUser.getData();
            if (contactsPojo.getContactCount().getContactAll() != null)
                modelLoggedData.getContactCount().setContactAll(contactsPojo.getContactCount().getContactAll());

            if (contactsPojo.getContactCount().getContactWeek() != null)
                modelLoggedData.getContactCount().setContactWeek(contactsPojo.getContactCount().getContactWeek());

            if (contactsPojo.getContactCount().getContactMonth() != null)
                modelLoggedData.getContactCount().setContactMonth(contactsPojo.getContactCount().getContactMonth());

            modelLoggedUser.setData(modelLoggedData);
            qkPreferences.storeLoggedUser(modelLoggedUser);

        } catch (Exception e) {
            e.printStackTrace();
        }

        Intent intent = new Intent("BROADCAST_FROM_SCAN_QK");
        sendBroadcast(intent);

    }

    public void updateContacts(Contacts card) {

        boolean isUpdate = true;

        Contacts toEdit = realmManager.realm.where(Contacts.class).equalTo("user_id", card.getUser_id()).findFirst();

        if (toEdit == null) {
            isUpdate = false;
            toEdit = new Contacts();
        }

        realmManager.realm.beginTransaction();

        toEdit.setUser_id(card.getUser_id());

        if (card.getUsername() != null)
            toEdit.setUsername(card.getUsername());

        if (card.getEmail() != null)
            toEdit.setEmail(card.getEmail());

        if (card.getFirstname() != null)
            toEdit.setFirstname(card.getFirstname());

        if (card.getLastname() != null)
            toEdit.setLastname(card.getLastname());

        if (card.getProfile_pic() != null)
            toEdit.setProfile_pic(card.getProfile_pic());

        if (card.getBirthdate() != null)
            toEdit.setBirthdate(card.getBirthdate());

        if (card.getHometown() != null)
            toEdit.setHometown(card.getHometown());

        if (card.getCountry() != null)
            toEdit.setCountry(card.getCountry());

        if (card.getState() != null)
            toEdit.setState(card.getState());

        if (card.getCity() != null)
            toEdit.setCity(card.getCity());

        if (card.getQk_story() != null)
            toEdit.setQk_story(card.getQk_story());

        if (card.getUnique_code() != null)
            toEdit.setUnique_code(card.getUnique_code());

        if (card.getScan_date() != null)
            toEdit.setScan_date(card.getScan_date());

        if (card.getIsBlocked() != null)
            toEdit.setIsBlocked(card.getIsBlocked());

        if (card.getContact_detail() != null)
            toEdit.setContact_detail(card.getContact_detail());

        if (card.getEducation_detail() != null)
            toEdit.setEducation_detail(card.getEducation_detail());

        if (card.getExperience_detail() != null)
            toEdit.setExperience_detail(card.getExperience_detail());

        if (card.getPublication_detail() != null)
            toEdit.setPublication_detail(card.getPublication_detail());

        if (card.getLanguage_detail() != null)
            toEdit.setLanguage_detail(card.getLanguage_detail());

        if (card.getApproved_contacts() != null)
            toEdit.setApproved_contacts(card.getApproved_contacts());

        if (card.getConnected_social_media() != null)
            toEdit.setConnected_social_media(card.getConnected_social_media());

        if (card.getScanned_by() != null)
            toEdit.setScanned_by(card.getScanned_by());

        if (card.getPhone() != null)
            toEdit.setPhone(card.getPhone());

        if (card.getEmail() != null)
            toEdit.setEmail(card.getEmail());

        if (card.getProfile_id() != null)
            toEdit.setProfile_id(card.getProfile_id());

        if (card.getProfile_name() != null)
            toEdit.setProfile_name(card.getProfile_name());

        if (card.getContact_type() != null)
            toEdit.setContact_type(card.getContact_type());

        if (card.getIsContacts() != null) {
            toEdit.setIsContacts(card.getIsContacts());
        }

        if (!isUpdate)
            realmManager.realm.copyToRealm(toEdit);

        realmManager.realm.commitTransaction();
    }

    public class RealmManager {
        private Realm realm;

        public RealmManager() {
            realm = Realm.getDefaultInstance();
        }
    }
}
