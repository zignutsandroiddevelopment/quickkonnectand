package com.services;

import android.app.IntentService;
import android.content.Intent;
import android.provider.Telephony;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;
import com.models.notification.Alert;
import com.models.notification.Aps;
import com.models.notification.Data;
import com.models.notification.Notification;
import com.models.notification.NotificationJson;
import com.utilities.QKPreferences;
import com.utilities.Utilities;
import com.volleyapirequest.VolleyApiRequest;
import com.volleyapirequest.VolleyCallBack;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.activities.CreateProfileActivity.context;

/**
 * Created by ZTLAB-12 on 04-06-2018.
 */

public class SendPushService extends IntentService {

    private QKPreferences qkPreferences;
    private DatabaseReference StatusDatabase;
    private boolean isonline = false;

    String user_message = "",
            from_name = "",
            to_name = "",
            status = "",
            token = "",
            chatRef = "",
            from_user_id = "",
            to_user_id = "",
            fromtoken = "",
            totoken = "",
            profile = "",
            users = "",
            mRecipientId = "",
            user_id = "",
            mCurrentUserId = "",
            to_id = "";

    public SendPushService() {
        super("SendPushService");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {

        user_message = intent.getExtras().getString("user_message") + "";
        from_name = intent.getExtras().getString("from_name") + "";
        to_name = intent.getExtras().getString("to_name") + "";
        token = intent.getExtras().getString("token") + "";
        chatRef = intent.getExtras().getString("chatRef") + "";
        from_user_id = intent.getExtras().getString("from_user_id") + "";
        to_user_id = intent.getExtras().getString("to_user_id") + "";
        fromtoken = intent.getExtras().getString("fromtoken") + "";
        totoken = intent.getExtras().getString("totoken") + "";
        profile = intent.getExtras().getString("profile") + "";
        users = intent.getExtras().getString("users") + "";
        to_id = intent.getExtras().getString("to_id") + "";
        user_id = intent.getExtras().getString("useris") + "";
        mCurrentUserId = intent.getExtras().getString("mcuserid") + "";
        mRecipientId = intent.getExtras().getString("receipient_id") + "";
        status = intent.getExtras().getString("status") + "";


       /* StatusDatabase = FirebaseDatabase.getInstance().getReference().child("activity").child(chatRef).child("online");
        StatusDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) { //something changed!
                for (DataSnapshot locationSnapshot : dataSnapshot.getChildren()) {
                    String data = locationSnapshot.getValue() + "";
                    String key = locationSnapshot.getKey() + "";
                    if (key.equalsIgnoreCase(mRecipientId + "")) {
                        if (!data.equalsIgnoreCase("true")) {
                            //online
                            isonline = true;
                            SendPush();
                        }
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });*/
        /*if (isonline)*/
        SendPush();
    }

    private void SendPush() {
        try {
            qkPreferences = new QKPreferences(getApplicationContext());
            String FormUid = qkPreferences.getValuesInt(QKPreferences.USER_ID) + "";

            /*JSONObject jsonObject = new JSONObject();
            jsonObject.put("sender_id", FormUid + "");
            jsonObject.put("receiver_unique_code", mRecipientId + "");
            jsonObject.put("message", user_message + "");
            jsonObject.put("chat_reference", chatRef + "");
            new VolleyApiRequest(getApplicationContext(), false, VolleyApiRequest.REQUEST_BASEURL + "api/user_chat_notification",
                    new VolleyCallBack() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                Log.d("response_notification", response + "");
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onErrorResponse(VolleyError error) {
                            VolleyApiRequest.showVolleyError(context, error);
                        }
                    }).enqueRequest(jsonObject, Request.Method.POST);*/


            JSONObject jsonObject = new JSONObject();
            jsonObject.put("sender_id", FormUid + "");
            jsonObject.put("receiver_unique_code", mRecipientId + "");
            jsonObject.put("message", user_message + "");
            jsonObject.put("chat_reference", chatRef + "");
            jsonObject.put("status", status + "");

            String url = VolleyApiRequest.REQUEST_BASEURL + "api/user_chat_notification";
            JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.d("Response", response + "");
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    //handle the error
                    Log.d("Response", error.getLocalizedMessage() + "");
                    error.printStackTrace();

                }
            }) {    //this is the part, that adds the header to the request
                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> params = new HashMap<String, String>();
                    return params;
                }
            };
            Volley.newRequestQueue(this).add(jsonRequest);

            /*NotificationJson notification = new NotificationJson();

            Alert alert = new Alert();
            alert.setBody(user_message + "");
            alert.setTitle(from_name + "");

            Aps aps = new Aps();
            aps.setAlert(alert);
            aps.setBadge("0");
            aps.setSound("default");

            Data data = new Data();
            data.setBody(user_message + "");
            data.setDate(date + "");
            data.setFromName(to_name + "");
            data.setMsgType("text ");
            data.setNotificationType("0");
            data.setTitle(from_name + "");
            data.setToName(from_name + "");
            data.setConversation(chatRef + "");
            data.setFrom_id(mCurrentUserId + "");
            data.setTo_id(mRecipientId + "");
            data.setFrom_token(totoken + "");
            data.setTo_token(fromtoken + "");
            data.setProfile_image(profile + "");
            data.setUsers(users + "");
            data.setFrom_user_id(FormUid + "");
            data.setTo_user_id(user_id + "");

           *//* Notification notifi = new Notification();
            notifi.setBody(user_message + "");
            notifi.setDate(date + "");
            notifi.setFromName(to_name + "");
            notifi.setMsgType("text ");
            notifi.setNotificationType("0");
            notifi.setTitle(from_name + "");
            notifi.setToName(from_name + "");
            notifi.setConversation(chatRef + "");
            notifi.setFrom_id(mCurrentUserId + "");
            notifi.setTo_id(mRecipientId + "");
            notifi.setFrom_token(totoken + "");
            notifi.setTo_token(fromtoken + "");
            notifi.setProfile_image(profile + "");
            notifi.setUsers(users + "");
            notifi.setFrom_user_id(FormUid + "");
            notifi.setTo_user_id(user_id + "");*//*

            notification.setAps(aps);
            notification.setData(data);
            //notification.setNotification(notifi);
            notification.setTo(token + "");

            Gson gson = new Gson();
            String json = gson.toJson(notification);
            JSONObject jsonObject = new JSONObject(json);
            String url = "https://fcm.googleapis.com/fcm/send";
            JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.d("Response", response + "");
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    //handle the error
                    Log.d("Response", error.getLocalizedMessage() + "");
                    error.printStackTrace();

                }
            }) {    //this is the part, that adds the header to the request
                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("Authorization", "key = AIzaSyDe5Ed2rb3KbVHK-wMXt-MVfoooxchnHtA");
                    params.put("Content-Type", "application/json");
                    params.put("content_available", "true");
                    params.put("priority", "high");
                    return params;
                }
            };
            Volley.newRequestQueue(this).add(jsonRequest);
*/
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}


   /*mService.sendPush(notification).enqueue(new Callback<StausMessage>() {
                    @Override
                    public void onResponse(Call<StausMessage> call, retrofit2.Response<StausMessage> response) {
                        Log.d("response_notification", response + "");
                    }

                    @Override
                    public void onFailure(Call<StausMessage> call, Throwable t) {
                        Log.d("response_noti_error", t.getLocalizedMessage() + "");
                    }
                });*/
                /*Gson gson = new Gson();
                String json = gson.toJson(notification);
                JSONObject jsonObject = new JSONObject(json);
                new VolleyApiRequest(context, true, "https://fcm.googleapis.com/fcm/send",
                        new VolleyCallBack() {
                            @Override
                            public void onResponse(JSONObject response) {
                                try {
                                    Log.d("response_notification", response + "");
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                VolleyApiRequest.showVolleyError(context, error);
                            }

                        }).enqueRequest(jsonObject, Request.Method.POST);*/

                /*RequestQueue queue = Volley.newRequestQueue(NewChatActivity.this);
                StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                // response
                                Log.d("Response", response);
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // TODO Auto-generated method stub
                                Log.d("ERROR", "error => " + error.toString());
                            }
                        }
                ) {
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("User-Agent", "Nintendo Gameboy");
                        params.put("Accept-Language", "fr");
                        return params;
                    }
                };
                queue.add(postRequest);*/