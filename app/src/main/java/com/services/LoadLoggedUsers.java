package com.services;

import android.app.IntentService;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.gson.Gson;
import com.models.login.LoginPojo;
import com.models.logindata.ModelLoggedUser;
import com.quickkonnect.R;
import com.retrofitservice.ApiUtils;
import com.retrofitservice.SOService;
import com.utilities.Constants;
import com.utilities.QKPreferences;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ZTLAB-10 on 13-05-2018.
 */

public class LoadLoggedUsers extends IntentService {

    public static final String PARAM_IN_MSG = "imsg";
    private SOService mService;
    private QKPreferences qkPreferences;

    public LoadLoggedUsers() {
        super("SynUserData");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        Log.e("200", "onHandleIntent");
        getLoginuserData();
    }

    public void getLoginuserData() {
        mService = ApiUtils.getSOService();
        qkPreferences = new QKPreferences(getApplicationContext());

        Map<String, String> params = new HashMap<>();
        params.put("Authorization", "Bearer " + qkPreferences.getApiToken() + "");
        params.put("Accept", "application/json");
        params.put("QK_ACCESS_KEY", getResources().getString(R.string.QK_ACCESS_KEY) + "");

        mService.getLoginUsersData(params, qkPreferences.getValuesInt(QKPreferences.USER_ID)).enqueue(new Callback<LoginPojo>() {
            @Override
            public void onResponse(Call<LoginPojo> call, Response<LoginPojo> response) {
                Log.e("200-Success", "" + response.body());
                try {

                    if (response != null && response.body() != null) {
                        LoginPojo loginPojo = response.body();
                        if (loginPojo.getStatus() == 200) {

                            JSONObject jsonObject = new JSONObject(new Gson().toJson(loginPojo));
                            ModelLoggedUser modelLoggedUser = new Gson().fromJson(jsonObject.toString(), ModelLoggedUser.class);
                            qkPreferences.storeLoggedUser(modelLoggedUser);

                            LocalBroadcastManager localBroadcastManager = LocalBroadcastManager.getInstance(getApplicationContext());
                            Intent localIntent = new Intent(Constants.ACTION_REFRESH_USERDATA);
                            localBroadcastManager.sendBroadcast(localIntent);

                        }
                    }

                } catch (Exception e) {
                    Log.e("200-Error", "" + e.getMessage());
                }
            }

            @Override
            public void onFailure(Call<LoginPojo> call, Throwable t) {
                Log.e("200-Error", "" + t.getLocalizedMessage());
            }
        });
    }
}
