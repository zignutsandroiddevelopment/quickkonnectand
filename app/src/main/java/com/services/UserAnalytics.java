package com.services;

import android.app.IntentService;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.Nullable;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.quickkonnect.R;
import com.utilities.QKPreferences;
import com.volleyapirequest.VolleyApiRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by ZTLAB-12 on 03-07-2018.
 */

public class UserAnalytics extends IntentService {

    String login_user_id = "", startTime = "", endTime = "";
    QKPreferences qkPreferences;

    public UserAnalytics() {
        super("");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        try {
            qkPreferences = new QKPreferences(getApplicationContext());
            login_user_id = "" + qkPreferences.getValuesInt(QKPreferences.USER_ID) + "";
            startTime = "" + qkPreferences.getStartTime() + "";

            String date = Calendar.getInstance().getTimeInMillis() + "";
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            endTime = formatter.format(new Date(Long.parseLong(date + "")));

            CallApi();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void CallApi() {
        try {
            JSONObject obj = new JSONObject();
            try {
                obj.put("device_type", "Android");
                obj.put("starttime", startTime + "");
                obj.put("endtime", endTime + "");
                obj.put("user_id", "" + login_user_id.replace(".0", ""));
                int version = Build.VERSION.SDK_INT;
                String model = Build.MODEL;
                if (model.isEmpty())
                    obj.put("model", "z-404");
                else
                    obj.put("model", model + "");
                obj.put("version", version + "");
            } catch (JSONException e) {
                e.printStackTrace();
            }

            String url = VolleyApiRequest.REQUEST_BASEURL + "api/analytics/user_activity";
            JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, url, obj, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.d("Response", response + "");
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    //handle the error
                    Log.d("Response", error.getLocalizedMessage() + "");
                    error.printStackTrace();

                }
            }) {
                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("Authorization", "Bearer " + qkPreferences.getApiToken() + "");
                    params.put("Accept", "application/json");
                    params.put("QK_ACCESS_KEY", getResources().getString(R.string.QK_ACCESS_KEY) + "");
                    return params;
                }
            };
            Volley.newRequestQueue(this).add(jsonRequest);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
