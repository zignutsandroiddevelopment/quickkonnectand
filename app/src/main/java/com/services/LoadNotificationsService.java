package com.services;

import android.app.IntentService;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.view.View;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.fragments.FragNotifications;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.models.ModelNotifications;
import com.realmtable.Notifications;
import com.realmtable.RealmController;
import com.utilities.QKPreferences;
import com.volleyapirequest.VolleyApiRequest;
import com.volleyapirequest.VolleyCallBack;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;

/**
 * Created by ZTLAB-12 on 23-06-2018.
 */

public class LoadNotificationsService extends IntentService {

    private QKPreferences qkPreferences;
    String mCurrentUserId = "";

    public LoadNotificationsService() {
        super(LoadNotificationsService.class.getSimpleName());
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {

        qkPreferences = new QKPreferences(getApplicationContext());
        mCurrentUserId = qkPreferences.getValues(QKPreferences.USER_UNIQUECODE) + "";

        sendMyBroadCast();

       /* try {
            String timezone = "?timezone=" + TimeZone.getDefault().getID();
            new VolleyApiRequest(getApplicationContext(), false, VolleyApiRequest.REQUEST_BASEURL + "api/notification_list/" + mCurrentUserId + "/" + "1" + timezone,
                    new VolleyCallBack() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                parseResponse(response);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onErrorResponse(VolleyError error) {
                        }
                    }).enqueRequest(null, Request.Method.GET);
            sendMyBroadCast();
        } catch (Exception e) {
            e.printStackTrace();
        }*/

    }

/*    public void parseResponse(JSONObject response) {
        try {
            String status = response.getString("status");
            String message = response.getString("msg");

            if (status.equals("200")) {

                JSONArray notilist = response.getJSONArray("data");

                if (notilist != null && notilist.length() > 0) {

                    Type type = new TypeToken<List<ModelNotifications>>() {
                    }.getType();

                    ArrayList<ModelNotifications> modelNotifications = new Gson().fromJson(notilist.toString(), type);
                    RealmController.with(getApplication()).clearNotifications();
                    for (ModelNotifications notifications : notificationsArrayList) {
                        RealmController.with(getActivity()).createUpdateNotifications(notifications);
                    }

                    List<Notifications> notificationsList = RealmController.with(getActivity()).getNotifications();


                    if (notificationsList != null && notificationsList.size() > 0) {
                        for (Notifications notifications : notificationsList) {

                            ModelNotifications modelNotifications = new ModelNotifications();

                            modelNotifications.setNotificationId(Integer.parseInt(notifications.getNotification_id()));
                            modelNotifications.setNotification_type(notifications.getNotification_type());
                            modelNotifications.setSenderId(Integer.parseInt(notifications.getSender_id()));
                            modelNotifications.setProfile_id(notifications.getProfile_id());
                            modelNotifications.setUsername(notifications.getUsername());
                            modelNotifications.setUserEmail(notifications.getUser_email());
                            modelNotifications.setProfile_pic(notifications.getProfile_pic());
                            modelNotifications.setStatus(notifications.getStatus());
                            modelNotifications.setIsRead(Integer.parseInt(notifications.getIs_read()));
                            modelNotifications.setCreatedTime(notifications.getCreated_time());
                            modelNotifications.setUniqueCode(notifications.getUnique_code());
                            modelNotifications.setMessage(notifications.getMessage());
                            modelNotifications.setProfile_name(notifications.getProfile_name());

                            modelNotifications1.add(modelNotifications);
                        }

                        if (notificationAdapter != null) {
                            notificationAdapter.notifyDataSetChanged();
                        }
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/

    private void sendMyBroadCast() {
        try {
            Intent broadCastIntent = new Intent();
            broadCastIntent.setAction("NOTIFICATION_CHANGE");
            sendBroadcast(broadCastIntent);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
