package com.services;

import android.app.IntentService;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.util.Log;

import com.models.contacts.ContactsPojo;
import com.models.contacts.Datum;
import com.models.logindata.ModelLoggedData;
import com.models.logindata.ModelLoggedUser;
import com.quickkonnect.R;
import com.realmtable.Contacts;
import com.realmtable.Follower_contact;
import com.realmtable.RealmController;
import com.retrofitservice.ApiUtils;
import com.retrofitservice.SOService;
import com.utilities.Constants;
import com.utilities.QKPreferences;
import com.utilities.Utilities;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ZTLAB-10 on 13-05-2018.
 */

public class LoadMyFollowers extends IntentService {

    private SOService mService;
    private QKPreferences qkPreferences;
    RealmManager realmManager;

    public LoadMyFollowers() {
        super("SynUserData");
        realmManager = new RealmManager();
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        Log.e("200", "onHandleIntent");
        getFollower();
    }

    public void getFollower() {
        mService = ApiUtils.getSOService();
        qkPreferences = new QKPreferences(getApplicationContext());

        Map<String, String> params = new HashMap<>();
        params.put("Authorization", "Bearer " + qkPreferences.getApiToken() + "");
        params.put("Accept", "application/json");
        params.put("QK_ACCESS_KEY", getResources().getString(R.string.QK_ACCESS_KEY)+"");

        mService.getFollowersList(params,"follower", qkPreferences.getValuesInt(QKPreferences.USER_ID), 1).enqueue(new Callback<ContactsPojo>() {
            @Override
            public void onResponse(Call<ContactsPojo> call, Response<ContactsPojo> response) {
                Log.e("Follower:Success", "" + response.body());
                try {
                    if (response != null && response.body() != null) {
                        ContactsPojo contactsPojo = response.body();
                        if (contactsPojo != null && contactsPojo.getStatus() == 200) {
                            parseFollowers(contactsPojo);
                        }

                    }

                } catch (Exception e) {

                }
            }

            @Override
            public void onFailure(Call<ContactsPojo> call, Throwable t) {
            }
        });
    }

    public void parseFollowers(ContactsPojo contactsPojo) {

        List<Datum> datumList = contactsPojo.getData();

        if (datumList != null && datumList.size() > 0) {

            realmManager.realm.beginTransaction();
            realmManager.realm.clear(Follower_contact.class);
            realmManager.realm.commitTransaction();

            for (Datum datum : datumList) {

                Follower_contact contacts = new Follower_contact();

                contacts.setUser_id("" + datum.getScanUserId());
                contacts.setUsername(datum.getScanUserName());
                contacts.setProfile_pic(datum.getScanUserProfileUrl());
                contacts.setFirstname(datum.getFirstname());
                contacts.setLastname(datum.getLastname());
                contacts.setUnique_code(datum.getScanUserUniqueCode());
                contacts.setScan_date(datum.getScannedBy());
                contacts.setPhone(datum.getPhone());
                contacts.setEmail(datum.getEmail());
                contacts.setProfile_id("" + datum.getProfileId());
                contacts.setProfile_name("" + datum.getProfileName());

                try {
                    String scanDate = datum.getScanDate();
                    if (Utilities.isEmpty(scanDate))
                        contacts.setScan_date(Utilities.changeDateFormat("yyyy-MM-dd hh:mm:ss", "dd-MM-yyyy", scanDate));

                } catch (Exception e) {
                    e.printStackTrace();
                }

                updateFollower_Contact(contacts);
            }
        }

        Intent intent = new Intent("BROADCAST_FROM_SCAN_QK");
        sendBroadcast(intent);
    }

    public void updateFollower_Contact(Follower_contact card) {

        boolean isUpdate = true;

        Follower_contact toEdit = new Follower_contact();

        //= realmManager.realm.where(Follower_contact.class).equalTo("user_id", card.getUser_id()).findFirst();

//        if (toEdit == null) {
        isUpdate = false;
        toEdit = new Follower_contact();
//        }

        realmManager.realm.beginTransaction();

        toEdit.setUser_id(card.getUser_id());

        if (card.getName() != null)
            toEdit.setName(card.getName());

        if (card.getUsername() != null)
            toEdit.setUsername(card.getUsername());

        if (card.getEmail() != null)
            toEdit.setEmail(card.getEmail());

        if (card.getUnique_code() != null)
            toEdit.setUnique_code(card.getUnique_code());

        if (card.getProfile_pic() != null)
            toEdit.setProfile_pic(card.getProfile_pic());

        if (card.getProfile_id() != null)
            toEdit.setProfile_id(card.getProfile_id());

        if (card.getProfile_name() != null)
            toEdit.setProfile_name(card.getProfile_name());

        if (card.getPhone() != null)
            toEdit.setPhone(card.getPhone());

        if (card.getUsername() != null)
            toEdit.setPhone(card.getUsername());

        if (card.getFirstname() != null)
            toEdit.setPhone(card.getFirstname());

        if (card.getLastname() != null)
            toEdit.setPhone(card.getLastname());

        if (card.getScan_date() != null)
            toEdit.setScan_date(card.getScan_date());

        if (!isUpdate)
            realmManager.realm.copyToRealm(toEdit);

        realmManager.realm.commitTransaction();
    }

    public class RealmManager {
        private Realm realm;

        public RealmManager() {
            realm = Realm.getDefaultInstance();
        }
    }
}
