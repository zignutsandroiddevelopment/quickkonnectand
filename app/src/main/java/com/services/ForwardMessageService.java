package com.services;

import android.app.IntentService;
import android.content.Intent;
import android.support.annotation.Nullable;

import com.Chat.ChatMessage;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.models.Conversation;
import com.models.contacts.Datum;

import com.realmtable.RealmController;
import com.utilities.QKPreferences;

import java.util.ArrayList;

/**
 * Created by ZTLAB-12 on 04-06-2018.
 */
public class ForwardMessageService extends IntentService {

    private QKPreferences qkPreferences;
    private ArrayList<String> selectedmessages;
    public ArrayList<Datum> mlist_multiselect;
    private DatabaseReference messageChatDatabase, conversationDB;


    public ForwardMessageService() {
        super("ForwardMessageService");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {

        selectedmessages = new ArrayList<>();
        mlist_multiselect = new ArrayList<>();
        if (intent != null) {
            selectedmessages = (ArrayList<String>) intent.getSerializableExtra("selected_messages");
            mlist_multiselect = (ArrayList<Datum>) intent.getSerializableExtra("mlist_multiselect");
        }

        qkPreferences = new QKPreferences(getApplicationContext());
        String user_username = qkPreferences.getValues(QKPreferences.USER_FULLNAME) + "";
        String user_uniqueid = qkPreferences.getValues(QKPreferences.USER_UNIQUECODE) + "";

        if (selectedmessages != null && selectedmessages.size() > 0 && mlist_multiselect != null && mlist_multiselect.size() > 0) {

            for (int i = 0; i < mlist_multiselect.size(); i++) {
                String username = mlist_multiselect.get(i).getScanUserName();
                String profileUrl = mlist_multiselect.get(i).getScanUserProfileUrl();
                String uniquecode = mlist_multiselect.get(i).getScanUserUniqueCode();
                String chatRef = "";
                Long currentUserId = null;
                Long ReceipientID = null;

                try {
                    currentUserId = Long.parseLong(user_uniqueid);
                    ReceipientID = Long.parseLong(uniquecode);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    if (currentUserId != null && ReceipientID != null) {
                        if (currentUserId > ReceipientID) {
                            chatRef = ReceipientID + "-" + currentUserId;
                        } else {
                            chatRef = currentUserId + "-" + ReceipientID;
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                messageChatDatabase = FirebaseDatabase.getInstance().getReference().child("chat").child(chatRef + "");
                conversationDB = FirebaseDatabase.getInstance().getReference().child("conversation").child(chatRef);

                for (int j = 0; j < selectedmessages.size(); j++) {
                    Conversation conversation = new Conversation(selectedmessages.get(j) + "", username, profileUrl, "", "", "", "", "");
                    conversationDB.setValue(conversation);
                    ChatMessage newMessage = new ChatMessage(selectedmessages.get(j) + "", user_uniqueid, user_username, "0", uniquecode, username, "", "", "", "");
                    messageChatDatabase.push().setValue(newMessage);
                    RealmController realmController = new RealmController(getApplication());
                    realmController.UpdateTanentListTime(username, selectedmessages.get(j) + "");
                }
            }
        }

    }
}
