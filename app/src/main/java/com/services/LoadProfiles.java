package com.services;

import android.app.IntentService;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.util.Log;

import com.google.gson.Gson;
import com.models.profiles.ModelProfileList;
import com.models.profiles.ProfilesList;
import com.quickkonnect.R;
import com.realmtable.MyProfiles;
import com.retrofitservice.ApiUtils;
import com.retrofitservice.SOService;
import com.utilities.QKPreferences;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ZTLAB-10 on 13-05-2018.
 */

public class LoadProfiles extends IntentService {

    private SOService mService;
    private QKPreferences qkPreferences;
    RealmManager realmManager;

    public LoadProfiles() {
        super("SynUserData");
        realmManager = new RealmManager();
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        Log.e("200", "onHandleIntent");
        getList();
    }

    public void getList() {
        mService = ApiUtils.getSOService();
        qkPreferences = new QKPreferences(getApplicationContext());
        Map<String, String> params = new HashMap<>();
        params.put("Authorization", "Bearer " + qkPreferences.getApiToken() + "");
        params.put("Accept", "application/json");
        params.put("QK_ACCESS_KEY", getResources().getString(R.string.QK_ACCESS_KEY)+"");
        mService.getProfileList(params,qkPreferences.getValuesInt(QKPreferences.USER_ID)).enqueue(new Callback<ModelProfileList>() {
            @Override
            public void onResponse(Call<ModelProfileList> call, Response<ModelProfileList> response) {
                Log.e("Profiles-Success", "" + response.body());
                try {
                    if (response != null && response.body() != null) {
                        ModelProfileList modelProfileList = response.body();
                        if (modelProfileList != null)
                            parseResponse(modelProfileList);
                    }
                } catch (Exception e) {
                    Log.e("Profiles-Error", "" + e.getMessage());
                }
            }

            @Override
            public void onFailure(Call<ModelProfileList> call, Throwable t) {
                Log.e("Profiles-Error", "" + t.getLocalizedMessage());
            }
        });
    }

    private void parseResponse(ModelProfileList modelProfileList) {
        try {

            if (modelProfileList.getStatus() == 200) {

                List<ProfilesList> profilesLists = modelProfileList.getData();

                if (profilesLists != null && profilesLists.size() > 0) {

                    Log.e("Profile-Data", "" + modelProfileList.getData().size());

                    List<String> myprofiles = new ArrayList<>();

                    for (ProfilesList profilesList : profilesLists) {

                        final MyProfiles myProfiles = new MyProfiles();

                        myProfiles.setId("" + profilesList.getId());
                        myProfiles.setName(profilesList.getName());
                        myProfiles.setStatus(profilesList.getStatus());
                        myProfiles.setType("" + profilesList.getType());
                        myProfiles.setLogo("" + profilesList.getLogo());
                        myProfiles.setBackground_gradient("" + profilesList.getQkTagInfo().getBackground_gradient());
                        myProfiles.setReason(profilesList.getResson() + "");
                        myProfiles.setRole_name(profilesList.getRole_name() + "");
                        myProfiles.setRole(profilesList.getRole() + "");
                        myProfiles.setUnique_code("" + profilesList.getUniqueCode());
                        String qktaginfo = new Gson().toJson(profilesList.getQkTagInfo());
                        myProfiles.setQktaginfo(qktaginfo);
                        myProfiles.setIsOwnProfile("true");

                        myprofiles.add(profilesList.getUniqueCode());

                        updateMyProfiles(myProfiles);

                    }
                    qkPreferences.storeMyProfiles(myprofiles);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateMyProfiles(MyProfiles myProfiles) {

        boolean isUpdate = true;

        MyProfiles toEdit = realmManager.realm.where(MyProfiles.class).equalTo("unique_code", myProfiles.getUnique_code()).findFirst();

        if (toEdit == null) {
            isUpdate = false;
            toEdit = new MyProfiles();
        }

        realmManager.realm.beginTransaction();

        toEdit.setId("" + myProfiles.getId());

        if (myProfiles.getUser_id() != null)
            toEdit.setUser_id("" + myProfiles.getUser_id());

        if (myProfiles.getType() != null)
            toEdit.setType("" + myProfiles.getType());

        if (myProfiles.getName() != null)
            toEdit.setName("" + myProfiles.getName());

        if (myProfiles.getAddress() != null)
            toEdit.setAddress("" + myProfiles.getAddress());

        if (myProfiles.getLatitude() != null)
            toEdit.setLatitude("" + myProfiles.getLatitude());

        if (myProfiles.getLongitude() != null)
            toEdit.setLongitude("" + myProfiles.getLongitude());

        if (myProfiles.getAbout() != null)
            toEdit.setAbout("" + myProfiles.getAbout());

        if (myProfiles.getLogo() != null)
            toEdit.setLogo("" + myProfiles.getLogo());

        if (myProfiles.getTag() != null)
            toEdit.setTag("" + myProfiles.getTag());

        if (myProfiles.getSub_tag() != null)
            toEdit.setSub_tag("" + myProfiles.getSub_tag());

        if (myProfiles.getCompany_team_size() != null)
            toEdit.setCompany_team_size("" + myProfiles.getCompany_team_size());

        if (myProfiles.getBackground_gradient() != null)
            toEdit.setBackground_gradient("" + myProfiles.getBackground_gradient());

        if (myProfiles.getEstablish_date() != null)
            toEdit.setEstablish_date("" + myProfiles.getEstablish_date());

        if (myProfiles.getUnique_code() != null)
            toEdit.setUnique_code("" + myProfiles.getUnique_code());

        if (myProfiles.getStatus() != null)
            toEdit.setStatus("" + myProfiles.getStatus());

        if (myProfiles.getSocial_media() != null)
            toEdit.setSocial_media("" + myProfiles.getSocial_media());

        if (myProfiles.getFounders() != null)
            toEdit.setFounders("" + myProfiles.getFounders());

        if (myProfiles.getEmail() != null)
            toEdit.setEmail("" + myProfiles.getEmail());

        if (myProfiles.getPhone() != null)
            toEdit.setPhone("" + myProfiles.getPhone());

        if (myProfiles.getWeblink() != null)
            toEdit.setWeblink("" + myProfiles.getWeblink());

        if (myProfiles.getQktaginfo() != null)
            toEdit.setQktaginfo("" + myProfiles.getQktaginfo());

        if (myProfiles.getReason() != null)
            toEdit.setReason("" + myProfiles.getReason());

        if (myProfiles.getOtheraddress() != null)
            toEdit.setOtheraddress("" + myProfiles.getOtheraddress());

        if (myProfiles.getCurrent_ceo() != null)
            toEdit.setCurrent_ceo("" + myProfiles.getCurrent_ceo());

        if (myProfiles.getCurrent_cfo() != null)
            toEdit.setCurrent_cfo("" + myProfiles.getCurrent_cfo());

        if (myProfiles.getIs_transfer() != null)
            toEdit.setIs_transfer("" + myProfiles.getIs_transfer());
        else
            toEdit.setIs_transfer("0");

        if (myProfiles.getTransfer_status() != null)
            toEdit.setTransfer_status("" + myProfiles.getTransfer_status());
        else
            toEdit.setTransfer_status("0");

        if (myProfiles.getDob() != null)
            toEdit.setDob("" + myProfiles.getDob());

        if (myProfiles.getRole() != null)
            toEdit.setRole("" + myProfiles.getRole());

        if (myProfiles.getRole_name() != null)
            toEdit.setRole_name("" + myProfiles.getRole_name());

        if (myProfiles.getIsOwnProfile() != null)
            toEdit.setIsOwnProfile(myProfiles.getIsOwnProfile());

        if (!isUpdate)
            realmManager.realm.copyToRealm(toEdit);

        realmManager.realm.commitTransaction();
    }

    public class RealmManager {
        private Realm realm;

        public RealmManager() {
            realm = Realm.getDefaultInstance();
        }
    }
}
