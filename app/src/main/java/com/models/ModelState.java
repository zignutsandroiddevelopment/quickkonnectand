package com.models;

/**
 * Created by ZTLAB03 on 02-01-2018.
 */

public class ModelState {

    int id;
    String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
