package com.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ZTLAB-12 on 20-07-2018.
 */

public class ChatUserListModel {

    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("JID")
    @Expose
    private String jID;
    @SerializedName("profile_pic")
    @Expose
    private String profilePic;
    @SerializedName("unique_code")
    @Expose
    private String uniqueCode;
    @SerializedName("last_message")
    @Expose
    private String lastMessage;
    @SerializedName("unread_count")
    @Expose
    private String unreadCount;
    @SerializedName("conversationID")
    @Expose
    private String conversationID;
    @SerializedName("date")
    @Expose
    private String date;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getjID() {
        return jID;
    }

    public void setjID(String jID) {
        this.jID = jID;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    public String getUniqueCode() {
        return uniqueCode;
    }

    public void setUniqueCode(String uniqueCode) {
        this.uniqueCode = uniqueCode;
    }

    public String getLastMessage() {
        return lastMessage;
    }

    public void setLastMessage(String lastMessage) {
        this.lastMessage = lastMessage;
    }

    public String getUnreadCount() {
        return unreadCount;
    }

    public void setUnreadCount(String unreadCount) {
        this.unreadCount = unreadCount;
    }

    public String getConversationID() {
        return conversationID;
    }

    public void setConversationID(String conversationID) {
        this.conversationID = conversationID;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
