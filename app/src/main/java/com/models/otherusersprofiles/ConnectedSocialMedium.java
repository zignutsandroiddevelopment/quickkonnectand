
package com.models.otherusersprofiles;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ConnectedSocialMedium {

    @SerializedName("social_media_id")
    @Expose
    private Integer socialMediaId;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("about")
    @Expose
    private String about;
    @SerializedName("religion")
    @Expose
    private String religion;
    @SerializedName("politics")
    @Expose
    private String politics;
    @SerializedName("media_count")
    @Expose
    private String mediaCount;
    @SerializedName("followers_count")
    @Expose
    private String followersCount;
    @SerializedName("followed_by_count")
    @Expose
    private String followedByCount;
    @SerializedName("friend_list_count")
    @Expose
    private String friendListCount;
    @SerializedName("likes_count")
    @Expose
    private String likesCount;
    @SerializedName("website")
    @Expose
    private String website;
    @SerializedName("publicProfileUrl")
    @Expose
    private String publicProfileUrl;
    @SerializedName("social_media_user_id")
    @Expose
    private String socialMediaUserId;

    public Integer getSocialMediaId() {
        return socialMediaId;
    }

    public void setSocialMediaId(Integer socialMediaId) {
        this.socialMediaId = socialMediaId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public String getReligion() {
        return religion;
    }

    public void setReligion(String religion) {
        this.religion = religion;
    }

    public String getPolitics() {
        return politics;
    }

    public void setPolitics(String politics) {
        this.politics = politics;
    }

    public String getMediaCount() {
        return mediaCount;
    }

    public void setMediaCount(String mediaCount) {
        this.mediaCount = mediaCount;
    }

    public String getFollowersCount() {
        return followersCount;
    }

    public void setFollowersCount(String followersCount) {
        this.followersCount = followersCount;
    }

    public String getFollowedByCount() {
        return followedByCount;
    }

    public void setFollowedByCount(String followedByCount) {
        this.followedByCount = followedByCount;
    }

    public String getFriendListCount() {
        return friendListCount;
    }

    public void setFriendListCount(String friendListCount) {
        this.friendListCount = friendListCount;
    }

    public String getLikesCount() {
        return likesCount;
    }

    public void setLikesCount(String likesCount) {
        this.likesCount = likesCount;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getPublicProfileUrl() {
        return publicProfileUrl;
    }

    public void setPublicProfileUrl(String publicProfileUrl) {
        this.publicProfileUrl = publicProfileUrl;
    }

    public String getSocialMediaUserId() {
        return socialMediaUserId;
    }

    public void setSocialMediaUserId(String socialMediaUserId) {
        this.socialMediaUserId = socialMediaUserId;
    }

}
