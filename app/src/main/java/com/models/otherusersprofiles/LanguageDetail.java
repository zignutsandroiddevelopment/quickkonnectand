
package com.models.otherusersprofiles;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LanguageDetail {

    @SerializedName("langauge_id")
    @Expose
    private Integer langaugeId;
    @SerializedName("langauge_name")
    @Expose
    private String langaugeName;
    @SerializedName("rating")
    @Expose
    private String rating;

    public Integer getLangaugeId() {
        return langaugeId;
    }

    public void setLangaugeId(Integer langaugeId) {
        this.langaugeId = langaugeId;
    }

    public String getLangaugeName() {
        return langaugeName;
    }

    public void setLangaugeName(String langaugeName) {
        this.langaugeName = langaugeName;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

}
