
package com.models.otherusersprofiles;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ContactCount {

    @SerializedName("contact_all")
    @Expose
    private String contactAll;
    @SerializedName("contact_week")
    @Expose
    private String contactWeek;
    @SerializedName("contact_month")
    @Expose
    private String contactMonth;

    public String getContactAll() {
        return contactAll;
    }

    public void setContactAll(String contactAll) {
        this.contactAll = contactAll;
    }

    public String getContactWeek() {
        return contactWeek;
    }

    public void setContactWeek(String contactWeek) {
        this.contactWeek = contactWeek;
    }

    public String getContactMonth() {
        return contactMonth;
    }

    public void setContactMonth(String contactMonth) {
        this.contactMonth = contactMonth;
    }

}
