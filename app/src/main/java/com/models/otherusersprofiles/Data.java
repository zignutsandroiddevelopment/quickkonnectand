
package com.models.otherusersprofiles;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data {

    @SerializedName("basic_detail")
    @Expose
    private BasicDetail basicDetail;
    @SerializedName("contact_detail")
    @Expose
    private List<ContactDetail> contactDetail = null;
    @SerializedName("education_detail")
    @Expose
    private List<EducationDetail> educationDetail = null;
    @SerializedName("experience_detail")
    @Expose
    private List<ExperienceDetail> experienceDetail = null;
    @SerializedName("publication_detail")
    @Expose
    private List<PublicationDetail> publicationDetail = null;
    @SerializedName("language_detail")
    @Expose
    private List<LanguageDetail> languageDetail = null;
    @SerializedName("connected_social_media")
    @Expose
    private List<ConnectedSocialMedium> connectedSocialMedia = null;
    @SerializedName("approved_contacts")
    @Expose
    private List<Object> approvedContacts = null;
    @SerializedName("is_blocked")
    @Expose
    private Integer isBlocked;
    @SerializedName("scan_date")
    @Expose
    private String scanDate;
    @SerializedName("main_plan_detail")
    @Expose
    private String mainPlanDetail;
    @SerializedName("extraa_plan_detail")
    @Expose
    private String extraaPlanDetail;
    @SerializedName("invoices")
    @Expose
    private List<Object> invoices = null;
    @SerializedName("latest_invoice")
    @Expose
    private List<Object> latestInvoice = null;
    @SerializedName("download_request_sent")
    @Expose
    private String download_request_sent;
    @SerializedName("scan_request")
    @Expose
    private String scan_request;

    public BasicDetail getBasicDetail() {
        return basicDetail;
    }

    public void setBasicDetail(BasicDetail basicDetail) {
        this.basicDetail = basicDetail;
    }

    public List<ContactDetail> getContactDetail() {
        return contactDetail;
    }

    public void setContactDetail(List<ContactDetail> contactDetail) {
        this.contactDetail = contactDetail;
    }

    public List<EducationDetail> getEducationDetail() {
        return educationDetail;
    }

    public void setEducationDetail(List<EducationDetail> educationDetail) {
        this.educationDetail = educationDetail;
    }

    public List<ExperienceDetail> getExperienceDetail() {
        return experienceDetail;
    }

    public void setExperienceDetail(List<ExperienceDetail> experienceDetail) {
        this.experienceDetail = experienceDetail;
    }

    public List<PublicationDetail> getPublicationDetail() {
        return publicationDetail;
    }

    public void setPublicationDetail(List<PublicationDetail> publicationDetail) {
        this.publicationDetail = publicationDetail;
    }

    public List<LanguageDetail> getLanguageDetail() {
        return languageDetail;
    }

    public void setLanguageDetail(List<LanguageDetail> languageDetail) {
        this.languageDetail = languageDetail;
    }

    public List<ConnectedSocialMedium> getConnectedSocialMedia() {
        return connectedSocialMedia;
    }

    public void setConnectedSocialMedia(List<ConnectedSocialMedium> connectedSocialMedia) {
        this.connectedSocialMedia = connectedSocialMedia;
    }

    public List<Object> getApprovedContacts() {
        return approvedContacts;
    }

    public void setApprovedContacts(List<Object> approvedContacts) {
        this.approvedContacts = approvedContacts;
    }

    public Integer getIsBlocked() {
        return isBlocked;
    }

    public void setIsBlocked(Integer isBlocked) {
        this.isBlocked = isBlocked;
    }

    public String getScanDate() {
        return scanDate;
    }

    public void setScanDate(String scanDate) {
        this.scanDate = scanDate;
    }

    public String getMainPlanDetail() {
        return mainPlanDetail;
    }

    public void setMainPlanDetail(String mainPlanDetail) {
        this.mainPlanDetail = mainPlanDetail;
    }

    public String getExtraaPlanDetail() {
        return extraaPlanDetail;
    }

    public void setExtraaPlanDetail(String extraaPlanDetail) {
        this.extraaPlanDetail = extraaPlanDetail;
    }

    public List<Object> getInvoices() {
        return invoices;
    }

    public void setInvoices(List<Object> invoices) {
        this.invoices = invoices;
    }

    public List<Object> getLatestInvoice() {
        return latestInvoice;
    }

    public void setLatestInvoice(List<Object> latestInvoice) {
        this.latestInvoice = latestInvoice;
    }

    public String getDownload_request_sent() {
        return download_request_sent;
    }

    public void setDownload_request_sent(String download_request_sent) {
        this.download_request_sent = download_request_sent;
    }

    public String getScan_request() {
        return scan_request;
    }

    public void setScan_request(String scan_request) {
        this.scan_request = scan_request;
    }
}
