package com.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ZTLAB-12 on 19-07-2018.
 */

public class ChatConversationItems {
    @SerializedName("messageID")
    @Expose
    private String messageID;
    @SerializedName("conversationID")
    @Expose
    private String conversationID;
    @SerializedName("fromJID")
    @Expose
    private String fromJID;
    @SerializedName("fromJIDResource")
    @Expose
    private String fromJIDResource;
    @SerializedName("toJID")
    @Expose
    private String toJID;
    @SerializedName("toJIDResource")
    @Expose
    private String toJIDResource;
    @SerializedName("sentDate")
    @Expose
    private String sentDate;
    @SerializedName("stanza")
    @Expose
    private String stanza;
    @SerializedName("body")
    @Expose
    private String body;


    public String getMessageID() {
        return messageID;
    }

    public void setMessageID(String messageID) {
        this.messageID = messageID;
    }

    public String getConversationID() {
        return conversationID;
    }

    public void setConversationID(String conversationID) {
        this.conversationID = conversationID;
    }

    public String getFromJID() {
        return fromJID;
    }

    public void setFromJID(String fromJID) {
        this.fromJID = fromJID;
    }

    public String getFromJIDResource() {
        return fromJIDResource;
    }

    public void setFromJIDResource(String fromJIDResource) {
        this.fromJIDResource = fromJIDResource;
    }

    public String getToJID() {
        return toJID;
    }

    public void setToJID(String toJID) {
        this.toJID = toJID;
    }

    public String getToJIDResource() {
        return toJIDResource;
    }

    public void setToJIDResource(String toJIDResource) {
        this.toJIDResource = toJIDResource;
    }

    public String getSentDate() {
        return sentDate;
    }

    public void setSentDate(String sentDate) {
        this.sentDate = sentDate;
    }

    public String getStanza() {
        return stanza;
    }

    public void setStanza(String stanza) {
        this.stanza = stanza;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }
}
