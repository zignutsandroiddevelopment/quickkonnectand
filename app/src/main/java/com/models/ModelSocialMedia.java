package com.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ModelSocialMedia implements Parcelable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("icon")
    @Expose
    private String icon;

    int ic_social_icon;
    String userName;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public int getIc_social_icon() {
        return ic_social_icon;
    }

    public void setIc_social_icon(int ic_social_icon) {
        this.ic_social_icon = ic_social_icon;
    }

    public final static Parcelable.Creator<ModelSocialMedia> CREATOR = new Creator<ModelSocialMedia>() {


        @SuppressWarnings({
                "unchecked"
        })
        public ModelSocialMedia createFromParcel(Parcel in) {
            return new ModelSocialMedia(in);
        }

        public ModelSocialMedia[] newArray(int size) {
            return (new ModelSocialMedia[size]);
        }

    };

    protected ModelSocialMedia(Parcel in) {
        this.id = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.name = ((String) in.readValue((String.class.getClassLoader())));
        this.status = ((String) in.readValue((String.class.getClassLoader())));
        this.icon = ((String) in.readValue((String.class.getClassLoader())));
    }

    public ModelSocialMedia() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(id);
        dest.writeValue(name);
        dest.writeValue(status);
        dest.writeValue(icon);
    }

    public int describeContents() {
        return 0;
    }

}