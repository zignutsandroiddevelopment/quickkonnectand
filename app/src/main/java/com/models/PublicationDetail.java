
package com.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PublicationDetail implements Parcelable {

    @SerializedName("publication_id")
    @Expose
    private Integer publicationId;
    @SerializedName("publication_title")
    @Expose
    private String publicationTitle;
    @SerializedName("publisher_name")
    @Expose
    private String publisherName;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("publication_url")
    @Expose
    private String publication_url;
    public final static Parcelable.Creator<PublicationDetail> CREATOR = new Creator<PublicationDetail>() {


        @SuppressWarnings({
                "unchecked"
        })
        public PublicationDetail createFromParcel(Parcel in) {
            return new PublicationDetail(in);
        }

        public PublicationDetail[] newArray(int size) {
            return (new PublicationDetail[size]);
        }

    };

    protected PublicationDetail(Parcel in) {
        this.publicationId = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.publicationTitle = ((String) in.readValue((String.class.getClassLoader())));
        this.publisherName = ((String) in.readValue((String.class.getClassLoader())));
        this.date = ((String) in.readValue((String.class.getClassLoader())));
        this.publication_url = ((String) in.readValue((String.class.getClassLoader())));
    }

    public PublicationDetail() {
    }

    public Integer getPublicationId() {
        return publicationId;
    }

    public void setPublicationId(Integer publicationId) {
        this.publicationId = publicationId;
    }

    public String getPublicationTitle() {
        return publicationTitle;
    }

    public void setPublicationTitle(String publicationTitle) {
        this.publicationTitle = publicationTitle;
    }

    public String getPublisherName() {
        return publisherName;
    }

    public void setPublisherName(String publisherName) {
        this.publisherName = publisherName;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getPublication_url() {
        return publication_url;
    }

    public void setPublication_url(String publication_url) {
        this.publication_url = publication_url;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(publicationId);
        dest.writeValue(publicationTitle);
        dest.writeValue(publisherName);
        dest.writeValue(date);
        dest.writeValue(publication_url);
    }

    public int describeContents() {
        return 0;
    }

}
