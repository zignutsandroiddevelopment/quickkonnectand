
package com.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Google implements Parcelable
{

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("data")
    @Expose
    private SocialData data;
    public final static Creator<Google> CREATOR = new Creator<Google>() {


        @SuppressWarnings({
            "unchecked"
        })
        public Google createFromParcel(Parcel in) {
            return new Google(in);
        }

        public Google[] newArray(int size) {
            return (new Google[size]);
        }

    }
    ;

    protected Google(Parcel in) {
        this.status = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.data = ((SocialData) in.readValue((SocialData.class.getClassLoader())));
    }

    public Google() {
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public SocialData getData() {
        return data;
    }

    public void setData(SocialData data) {
        this.data = data;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(status);
        dest.writeValue(data);
    }

    public int describeContents() {
        return  0;
    }

}
