
package com.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BasicDetail implements Parcelable
{

    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("firstname")
    @Expose
    private String firstname;
    @SerializedName("lastname")
    @Expose
    private String lastname;
    @SerializedName("profile_pic")
    @Expose
    private String profilePic;
    @SerializedName("birthdate")
    @Expose
    private String birthdate;
    @SerializedName("hometown")
    @Expose
    private String hometown;
    @SerializedName("country_id")
    @Expose
    private String countryId;
    @SerializedName("country")
    @Expose
    private String country;
    @SerializedName("city_id")
    @Expose
    private String cityId;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("state_id")
    @Expose
    private String stateId;
    @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("qk_story")
    @Expose
    private String qkStory;
    @SerializedName("unique_code")
    @Expose
    private String uniqueCode;
    public final static Parcelable.Creator<BasicDetail> CREATOR = new Creator<BasicDetail>() {


        @SuppressWarnings({
            "unchecked"
        })
        public BasicDetail createFromParcel(Parcel in) {
            return new BasicDetail(in);
        }

        public BasicDetail[] newArray(int size) {
            return (new BasicDetail[size]);
        }

    }
    ;

    protected BasicDetail(Parcel in) {
        this.userId = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.username = ((String) in.readValue((String.class.getClassLoader())));
        this.firstname = ((String) in.readValue((String.class.getClassLoader())));
        this.lastname = ((String) in.readValue((String.class.getClassLoader())));
        this.profilePic = ((String) in.readValue((String.class.getClassLoader())));
        this.birthdate = ((String) in.readValue((String.class.getClassLoader())));
        this.hometown = ((String) in.readValue((String.class.getClassLoader())));
        this.countryId = ((String) in.readValue((Integer.class.getClassLoader())));
        this.country = ((String) in.readValue((String.class.getClassLoader())));
        this.cityId = ((String) in.readValue((Integer.class.getClassLoader())));
        this.city = ((String) in.readValue((String.class.getClassLoader())));
        this.stateId = ((String) in.readValue((Integer.class.getClassLoader())));
        this.state = ((String) in.readValue((String.class.getClassLoader())));
        this.qkStory = ((String) in.readValue((String.class.getClassLoader())));
        this.uniqueCode = ((String) in.readValue((String.class.getClassLoader())));
        this.email = ((String) in.readValue((String.class.getClassLoader())));
    }

    public BasicDetail() {
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public String getHometown() {
        return hometown;
    }

    public void setHometown(String hometown) {
        this.hometown = hometown;
    }

    public String getCountryId() {
        return countryId;
    }

    public void setCountryId(String countryId) {
        this.countryId = countryId;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStateId() {
        return stateId;
    }

    public void setStateId(String stateId) {
        this.stateId = stateId;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getQkStory() {
        return qkStory;
    }

    public void setQkStory(String qkStory) {
        this.qkStory = qkStory;
    }

    public String getUniqueCode() {
        return uniqueCode;
    }

    public void setUniqueCode(String uniqueCode) {
        this.uniqueCode = uniqueCode;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(userId);
        dest.writeValue(username);
        dest.writeValue(firstname);
        dest.writeValue(lastname);
        dest.writeValue(profilePic);
        dest.writeValue(birthdate);
        dest.writeValue(hometown);
        dest.writeValue(countryId);
        dest.writeValue(country);
        dest.writeValue(cityId);
        dest.writeValue(city);
        dest.writeValue(stateId);
        dest.writeValue(state);
        dest.writeValue(qkStory);
        dest.writeValue(uniqueCode);
        dest.writeValue(email);
    }

    public int describeContents() {
        return  0;
    }

}
