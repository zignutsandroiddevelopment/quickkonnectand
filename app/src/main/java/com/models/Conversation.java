package com.models;

import java.util.Date;

/**
 * Created by ZTLAB-12 on 24-05-2018.
 */

public class Conversation {

    public long time;

    String blockuserid;
    String profile_image;
    String lastmessage;
    String users;

    String from_token;
    String from_user_id;
    String to_token;
    String to_user_id;

   /* public Conversation(String blockuserid, String profile_image, String lastmessage, String users, String from_token, String from_user_id, String to_token, String to_user_id) {

        this.blockuserid = blockuserid;
        this.profile_image = profile_image;
        this.lastmessage = lastmessage;
        this.users = users;
        this.from_token = from_token;
        this.from_user_id = from_user_id;
        this.to_token = to_token;
        this.to_user_id = to_user_id;

        time = new Date().getTime() / 1000;
    }*/

    public Conversation(String lastmessage, String users, String profile_image, String blockuserid, String from_token, String from_user_id, String to_token, String to_user_id) {
        this.lastmessage = lastmessage;
        this.users = users;
        this.profile_image = profile_image;
        this.blockuserid = blockuserid;

        this.from_token = from_token;
        this.from_user_id = from_user_id;
        this.to_token = to_token;
        this.to_user_id = to_user_id;

        time = new Date().getTime() / 1000;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public String getBlockuserid() {
        return blockuserid;
    }

    public void setBlockuserid(String blockuserid) {
        this.blockuserid = blockuserid;
    }

    public String getProfile_image() {
        return profile_image;
    }

    public void setProfile_image(String profile_image) {
        this.profile_image = profile_image;
    }

    public String getLastmessage() {
        return lastmessage;
    }

    public void setLastmessage(String lastmessage) {
        this.lastmessage = lastmessage;
    }

    public String getUsers() {
        return users;
    }

    public void setUsers(String users) {
        this.users = users;
    }


    public String getFrom_token() {
        return from_token;
    }

    public void setFrom_token(String from_token) {
        this.from_token = from_token;
    }

    public String getFrom_user_id() {
        return from_user_id;
    }

    public void setFrom_user_id(String from_user_id) {
        this.from_user_id = from_user_id;
    }

    public String getTo_token() {
        return to_token;
    }

    public void setTo_token(String to_token) {
        this.to_token = to_token;
    }

    public String getTo_user_id() {
        return to_user_id;
    }

    public void setTo_user_id(String to_user_id) {
        this.to_user_id = to_user_id;
    }
}
