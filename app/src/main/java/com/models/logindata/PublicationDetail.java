
package com.models.logindata;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PublicationDetail implements Parcelable
{

    @SerializedName("publication_id")
    @Expose
    private Integer publicationId;
    @SerializedName("publication_title")
    @Expose
    private String publicationTitle;
    @SerializedName("publisher_name")
    @Expose
    private String publisherName;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("publication_url")
    @Expose
    private String publicationUrl;
    public final static Creator<PublicationDetail> CREATOR = new Creator<PublicationDetail>() {


        @SuppressWarnings({
            "unchecked"
        })
        public PublicationDetail createFromParcel(Parcel in) {
            return new PublicationDetail(in);
        }

        public PublicationDetail[] newArray(int size) {
            return (new PublicationDetail[size]);
        }

    }
    ;

    protected PublicationDetail(Parcel in) {
        this.publicationId = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.publicationTitle = ((String) in.readValue((String.class.getClassLoader())));
        this.publisherName = ((String) in.readValue((String.class.getClassLoader())));
        this.date = ((String) in.readValue((String.class.getClassLoader())));
        this.publicationUrl = ((String) in.readValue((String.class.getClassLoader())));
    }

    public PublicationDetail() {
    }

    public Integer getPublicationId() {
        return publicationId;
    }

    public void setPublicationId(Integer publicationId) {
        this.publicationId = publicationId;
    }

    public String getPublicationTitle() {
        return publicationTitle;
    }

    public void setPublicationTitle(String publicationTitle) {
        this.publicationTitle = publicationTitle;
    }

    public String getPublisherName() {
        return publisherName;
    }

    public void setPublisherName(String publisherName) {
        this.publisherName = publisherName;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getPublicationUrl() {
        return publicationUrl;
    }

    public void setPublicationUrl(String publicationUrl) {
        this.publicationUrl = publicationUrl;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(publicationId);
        dest.writeValue(publicationTitle);
        dest.writeValue(publisherName);
        dest.writeValue(date);
        dest.writeValue(publicationUrl);
    }

    public int describeContents() {
        return  0;
    }

}
