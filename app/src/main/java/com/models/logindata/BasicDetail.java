
package com.models.logindata;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BasicDetail implements Parcelable {

    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("firstname")
    @Expose
    private String firstname;
    @SerializedName("lastname")
    @Expose
    private String lastname;
    @SerializedName("profile_pic")
    @Expose
    private Object profilePic;
    @SerializedName("birthdate")
    @Expose
    private String birthdate;
    @SerializedName("hometown")
    @Expose
    private String hometown;
    @SerializedName("country_id")
    @Expose
    private Integer countryId;
    @SerializedName("country")
    @Expose
    private String country;
    @SerializedName("city_id")
    @Expose
    private Integer cityId;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("state_id")
    @Expose
    private Integer stateId;
    @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("qk_story")
    @Expose
    private Object qkStory;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("unique_code")
    @Expose
    private String uniqueCode;
    @SerializedName("ishide_year")
    @Expose
    private String ishide_year;

    public final static Creator<BasicDetail> CREATOR = new Creator<BasicDetail>() {


        @SuppressWarnings({
                "unchecked"
        })
        public BasicDetail createFromParcel(Parcel in) {
            return new BasicDetail(in);
        }

        public BasicDetail[] newArray(int size) {
            return (new BasicDetail[size]);
        }

    };

    protected BasicDetail(Parcel in) {
        this.userId = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.username = ((String) in.readValue((String.class.getClassLoader())));
        this.email = ((String) in.readValue((String.class.getClassLoader())));
        this.firstname = ((String) in.readValue((String.class.getClassLoader())));
        this.lastname = ((String) in.readValue((String.class.getClassLoader())));
        this.profilePic = ((Object) in.readValue((Object.class.getClassLoader())));
        this.birthdate = ((String) in.readValue((String.class.getClassLoader())));
        this.hometown = ((String) in.readValue((String.class.getClassLoader())));
        this.countryId = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.country = ((String) in.readValue((String.class.getClassLoader())));
        this.cityId = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.city = ((String) in.readValue((String.class.getClassLoader())));
        this.stateId = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.state = ((String) in.readValue((String.class.getClassLoader())));
        this.qkStory = ((Object) in.readValue((Object.class.getClassLoader())));
        this.uniqueCode = ((String) in.readValue((String.class.getClassLoader())));
        this.gender = ((String) in.readValue((String.class.getClassLoader())));
        this.ishide_year = ((String) in.readValue((String.class.getClassLoader())));
    }

    public BasicDetail() {
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public Object getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(Object profilePic) {
        this.profilePic = profilePic;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public String getHometown() {
        return hometown;
    }

    public void setHometown(String hometown) {
        this.hometown = hometown;
    }

    public Integer getCountryId() {
        return countryId;
    }

    public void setCountryId(Integer countryId) {
        this.countryId = countryId;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Integer getCityId() {
        return cityId;
    }

    public void setCityId(Integer cityId) {
        this.cityId = cityId;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Integer getStateId() {
        return stateId;
    }

    public void setStateId(Integer stateId) {
        this.stateId = stateId;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Object getQkStory() {
        return qkStory;
    }

    public void setQkStory(Object qkStory) {
        this.qkStory = qkStory;
    }

    public String getUniqueCode() {
        return uniqueCode;
    }

    public void setUniqueCode(String uniqueCode) {
        this.uniqueCode = uniqueCode;
    }

    public String getIshide_year() {
        return ishide_year;
    }

    public void setIshide_year(String ishide_year) {
        this.ishide_year = ishide_year;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(userId);
        dest.writeValue(username);
        dest.writeValue(email);
        dest.writeValue(firstname);
        dest.writeValue(lastname);
        dest.writeValue(profilePic);
        dest.writeValue(birthdate);
        dest.writeValue(hometown);
        dest.writeValue(countryId);
        dest.writeValue(country);
        dest.writeValue(cityId);
        dest.writeValue(city);
        dest.writeValue(stateId);
        dest.writeValue(state);
        dest.writeValue(qkStory);
        dest.writeValue(uniqueCode);
        dest.writeValue(gender);
        dest.writeValue(ishide_year);
    }

    public int describeContents() {
        return 0;
    }

}
