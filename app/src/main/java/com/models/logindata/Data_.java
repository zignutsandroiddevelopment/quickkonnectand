
package com.models.logindata;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data_ implements Parcelable {

    @SerializedName("profile_id")
    @Expose
    private String profileId;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("firstname")
    @Expose
    private String firstname;
    @SerializedName("lastName")
    @Expose
    private String lastName;
    @SerializedName("email")
    @Expose
    private String email;

    @SerializedName("publicProfileUrl")
    @Expose
    private String publicProfileUrl;

    @SerializedName("media_count")
    @Expose
    private String mediaCount;
    @SerializedName("followers_count")
    @Expose
    private String followersCount;
    @SerializedName("followed_by_count")
    @Expose
    private String followedByCount;
    @SerializedName("friend_list_count")
    @Expose
    private String friendListCount;
    @SerializedName("likes_count")
    @Expose
    private String likesCount;
    public final static Creator<Data_> CREATOR = new Creator<Data_>() {


        @SuppressWarnings({
                "unchecked"
        })
        public Data_ createFromParcel(Parcel in) {
            return new Data_(in);
        }

        public Data_[] newArray(int size) {
            return (new Data_[size]);
        }

    };

    protected Data_(Parcel in) {
        this.profileId = ((String) in.readValue((String.class.getClassLoader())));
        this.username = ((String) in.readValue((String.class.getClassLoader())));
        this.firstname = ((String) in.readValue((String.class.getClassLoader())));
        this.lastName = ((String) in.readValue((String.class.getClassLoader())));
        this.email = ((String) in.readValue((String.class.getClassLoader())));
        this.mediaCount = ((String) in.readValue((Object.class.getClassLoader())));
        this.followersCount = ((String) in.readValue((Object.class.getClassLoader())));
        this.followedByCount = ((String) in.readValue((Object.class.getClassLoader())));
        this.friendListCount = ((String) in.readValue((Integer.class.getClassLoader())));
        this.likesCount = ((String) in.readValue((Object.class.getClassLoader())));
        this.publicProfileUrl = ((String) in.readValue((Object.class.getClassLoader())));
    }

    public Data_() {
    }

    public String getProfileId() {
        return profileId;
    }

    public void setProfileId(String profileId) {
        this.profileId = profileId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMediaCount() {
        return mediaCount;
    }

    public void setMediaCount(String mediaCount) {
        this.mediaCount = mediaCount;
    }

    public String getFollowersCount() {
        return followersCount;
    }

    public void setFollowersCount(String followersCount) {
        this.followersCount = followersCount;
    }

    public String getFollowedByCount() {
        return followedByCount;
    }

    public void setFollowedByCount(String followedByCount) {
        this.followedByCount = followedByCount;
    }

    public String getFriendListCount() {
        return friendListCount;
    }

    public void setFriendListCount(String friendListCount) {
        this.friendListCount = friendListCount;
    }

    public String getPublicProfileUrl() {
        return publicProfileUrl;
    }

    public void setPublicProfileUrl(String publicProfileUrl) {
        this.publicProfileUrl = publicProfileUrl;
    }

    public String getLikesCount() {
        return likesCount;
    }

    public void setLikesCount(String likesCount) {
        this.likesCount = likesCount;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(profileId);
        dest.writeValue(username);
        dest.writeValue(firstname);
        dest.writeValue(lastName);
        dest.writeValue(email);
        dest.writeValue(mediaCount);
        dest.writeValue(followersCount);
        dest.writeValue(followedByCount);
        dest.writeValue(friendListCount);
        dest.writeValue(likesCount);
        dest.writeValue(publicProfileUrl);
    }

    public int describeContents() {
        return 0;
    }

}
