
package com.models.logindata;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ModelLoggedUser implements Parcelable
{

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("msg")
    @Expose
    private String msg;
    @SerializedName("data")
    @Expose
    private ModelLoggedData data;
    @SerializedName("api_token")
    @Expose
    private String apiToken;

    public final static Creator<ModelLoggedUser> CREATOR = new Creator<ModelLoggedUser>() {


        @SuppressWarnings({
            "unchecked"
        })
        public ModelLoggedUser createFromParcel(Parcel in) {
            return new ModelLoggedUser(in);
        }

        public ModelLoggedUser[] newArray(int size) {
            return (new ModelLoggedUser[size]);
        }

    };

    protected ModelLoggedUser(Parcel in) {
        this.status = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.msg = ((String) in.readValue((String.class.getClassLoader())));
        this.data = ((ModelLoggedData) in.readValue((ModelLoggedData.class.getClassLoader())));
        this.apiToken = ((String) in.readValue((String.class.getClassLoader())));
    }

    public ModelLoggedUser() {
    }

    public String getApiToken() {
        return apiToken;
    }

    public void setApiToken(String apiToken) {
        this.apiToken = apiToken;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ModelLoggedData getData() {
        return data;
    }

    public void setData(ModelLoggedData data) {
        this.data = data;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(status);
        dest.writeValue(msg);
        dest.writeValue(data);
        dest.writeValue(apiToken);
    }

    public int describeContents() {
        return  0;
    }

}
