
package com.models.logindata;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Twitter implements Parcelable
{

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("data")
    @Expose
    private SocialData data;
    public final static Creator<Twitter> CREATOR = new Creator<Twitter>() {


        @SuppressWarnings({
            "unchecked"
        })
        public Twitter createFromParcel(Parcel in) {
            return new Twitter(in);
        }

        public Twitter[] newArray(int size) {
            return (new Twitter[size]);
        }

    }
    ;

    protected Twitter(Parcel in) {
        this.status = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.data = ((SocialData) in.readValue((SocialData.class.getClassLoader())));
    }

    public Twitter() {
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public SocialData getData() {
        return data;
    }

    public void setData(SocialData data) {
        this.data = data;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(status);
        dest.writeValue(data);
    }

    public int describeContents() {
        return  0;
    }

}
