
package com.models.logindata;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SocialData implements Parcelable
{

    @SerializedName("profile_id")
    @Expose
    private String profileId;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("firstname")
    @Expose
    private String firstname;
    @SerializedName("lastName")
    @Expose
    private String lastName;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("publicProfileUrl")
    @Expose
    private String publicProfileUrl;
    @SerializedName("media_count")
    @Expose
    private Object mediaCount;
    @SerializedName("followers_count")
    @Expose
    private Object followersCount;
    @SerializedName("followed_by_count")
    @Expose
    private Object followedByCount;
    @SerializedName("friend_list_count")
    @Expose
    private Integer friendListCount;
    @SerializedName("likes_count")
    @Expose
    private Object likesCount;
    public final static Creator<SocialData> CREATOR = new Creator<SocialData>() {


        @SuppressWarnings({
            "unchecked"
        })
        public SocialData createFromParcel(Parcel in) {
            return new SocialData(in);
        }

        public SocialData[] newArray(int size) {
            return (new SocialData[size]);
        }

    }
    ;

    protected SocialData(Parcel in) {
        this.profileId = ((String) in.readValue((String.class.getClassLoader())));
        this.username = ((String) in.readValue((String.class.getClassLoader())));
        this.firstname = ((String) in.readValue((String.class.getClassLoader())));
        this.lastName = ((String) in.readValue((String.class.getClassLoader())));
        this.email = ((String) in.readValue((String.class.getClassLoader())));
        this.mediaCount = ((Object) in.readValue((Object.class.getClassLoader())));
        this.followersCount = ((Object) in.readValue((Object.class.getClassLoader())));
        this.followedByCount = ((Object) in.readValue((Object.class.getClassLoader())));
        this.friendListCount = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.likesCount = ((Object) in.readValue((Object.class.getClassLoader())));
        this.publicProfileUrl = ((String) in.readValue((String.class.getClassLoader())));
    }

    public SocialData() {
    }

    public String getProfileId() {
        return profileId;
    }

    public void setProfileId(String profileId) {
        this.profileId = profileId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Object getMediaCount() {
        return mediaCount;
    }

    public void setMediaCount(Object mediaCount) {
        this.mediaCount = mediaCount;
    }

    public Object getFollowersCount() {
        return followersCount;
    }

    public void setFollowersCount(Object followersCount) {
        this.followersCount = followersCount;
    }

    public Object getFollowedByCount() {
        return followedByCount;
    }

    public void setFollowedByCount(Object followedByCount) {
        this.followedByCount = followedByCount;
    }

    public Integer getFriendListCount() {
        return friendListCount;
    }

    public void setFriendListCount(Integer friendListCount) {
        this.friendListCount = friendListCount;
    }

    public String getPublicProfileUrl() {
        return publicProfileUrl;
    }

    public void setPublicProfileUrl(String publicProfileUrl) {
        this.publicProfileUrl = publicProfileUrl;
    }

    public Object getLikesCount() {
        return likesCount;
    }

    public void setLikesCount(Object likesCount) {
        this.likesCount = likesCount;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(profileId);
        dest.writeValue(username);
        dest.writeValue(firstname);
        dest.writeValue(lastName);
        dest.writeValue(email);
        dest.writeValue(mediaCount);
        dest.writeValue(followersCount);
        dest.writeValue(followedByCount);
        dest.writeValue(friendListCount);
        dest.writeValue(likesCount);
        dest.writeValue(publicProfileUrl);
    }

    public int describeContents() {
        return  0;
    }

}
