
package com.models.logindata;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ModelUserProfile implements Parcelable
{

    @SerializedName("basic_detail")
    @Expose
    private BasicDetail basicDetail;
    @SerializedName("contact_detail")
    @Expose
    private List<ContactDetail> contactDetail = null;
    @SerializedName("education_detail")
    @Expose
    private List<EducationDetail> educationDetail = null;
    @SerializedName("experience_detail")
    @Expose
    private List<ExperienceDetail> experienceDetail = null;
    @SerializedName("publication_detail")
    @Expose
    private List<PublicationDetail> publicationDetail = null;
    @SerializedName("language_detail")
    @Expose
    private List<LanguageDetail> languageDetail = null;
    public final static Creator<ModelUserProfile> CREATOR = new Creator<ModelUserProfile>() {


        @SuppressWarnings({
            "unchecked"
        })
        public ModelUserProfile createFromParcel(Parcel in) {
            return new ModelUserProfile(in);
        }

        public ModelUserProfile[] newArray(int size) {
            return (new ModelUserProfile[size]);
        }

    }
    ;

    protected ModelUserProfile(Parcel in) {
        this.basicDetail = ((BasicDetail) in.readValue((BasicDetail.class.getClassLoader())));
        in.readList(this.contactDetail, (ContactDetail.class.getClassLoader()));
        in.readList(this.educationDetail, (EducationDetail.class.getClassLoader()));
        in.readList(this.experienceDetail, (ExperienceDetail.class.getClassLoader()));
        in.readList(this.publicationDetail, (PublicationDetail.class.getClassLoader()));
        in.readList(this.languageDetail, (LanguageDetail.class.getClassLoader()));
    }

    public ModelUserProfile() {
    }

    public BasicDetail getBasicDetail() {
        return basicDetail;
    }

    public void setBasicDetail(BasicDetail basicDetail) {
        this.basicDetail = basicDetail;
    }

    public List<ContactDetail> getContactDetail() {
        return contactDetail;
    }

    public void setContactDetail(List<ContactDetail> contactDetail) {
        this.contactDetail = contactDetail;
    }

    public List<EducationDetail> getEducationDetail() {
        return educationDetail;
    }

    public void setEducationDetail(List<EducationDetail> educationDetail) {
        this.educationDetail = educationDetail;
    }

    public List<ExperienceDetail> getExperienceDetail() {
        return experienceDetail;
    }

    public void setExperienceDetail(List<ExperienceDetail> experienceDetail) {
        this.experienceDetail = experienceDetail;
    }

    public List<PublicationDetail> getPublicationDetail() {
        return publicationDetail;
    }

    public void setPublicationDetail(List<PublicationDetail> publicationDetail) {
        this.publicationDetail = publicationDetail;
    }

    public List<LanguageDetail> getLanguageDetail() {
        return languageDetail;
    }

    public void setLanguageDetail(List<LanguageDetail> languageDetail) {
        this.languageDetail = languageDetail;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(basicDetail);
        dest.writeList(contactDetail);
        dest.writeList(educationDetail);
        dest.writeList(experienceDetail);
        dest.writeList(publicationDetail);
        dest.writeList(languageDetail);
    }

    public int describeContents() {
        return  0;
    }

}
