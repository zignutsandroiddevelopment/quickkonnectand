package com.models;

/**
 * Created by ZTLAB03 on 13-03-2018.
 */

class NavModelItems {

    int icon;
    String title;

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
