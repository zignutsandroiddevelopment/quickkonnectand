
package com.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ModelLoggedData implements Parcelable
{

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("firstname")
    @Expose
    private String firstname;
    @SerializedName("lastname")
    @Expose
    private String lastname;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("user_detail")
    @Expose
    private String userDetail;
    @SerializedName("unique_code")
    @Expose
    private String uniqueCode;
    @SerializedName("register")
    @Expose
    private Integer register;
    @SerializedName("firsttime")
    @Expose
    private Integer firsttime;
    @SerializedName("qr_code")
    @Expose
    private Object qrCode;
    @SerializedName("middle_tag")
    @Expose
    private Object middleTag;
    @SerializedName("background_color")
    @Expose
    private Object backgroundColor;
    @SerializedName("border_color")
    @Expose
    private Object borderColor;
    @SerializedName("pattern_color")
    @Expose
    private Object patternColor;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("profile_pic")
    @Expose
    private Object profilePic;
    @SerializedName("notification_status")
    @Expose
    private Integer notificationStatus;
    @SerializedName("facebook")
    @Expose
    private Facebook facebook;
    @SerializedName("linkedin")
    @Expose
    private Linkedin linkedin;
    @SerializedName("twitter")
    @Expose
    private Twitter twitter;
    @SerializedName("instagram")
    @Expose
    private Instagram instagram;
    @SerializedName("google")
    @Expose
    private Google google;
    public final static Creator<ModelLoggedData> CREATOR = new Creator<ModelLoggedData>() {


        @SuppressWarnings({
                "unchecked"
        })
        public ModelLoggedData createFromParcel(Parcel in) {
            return new ModelLoggedData(in);
        }

        public ModelLoggedData[] newArray(int size) {
            return (new ModelLoggedData[size]);
        }

    }
            ;

    protected ModelLoggedData(Parcel in) {
        this.id = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.name = ((String) in.readValue((String.class.getClassLoader())));
        this.firstname = ((String) in.readValue((String.class.getClassLoader())));
        this.lastname = ((String) in.readValue((String.class.getClassLoader())));
        this.email = ((String) in.readValue((String.class.getClassLoader())));
        this.userDetail = ((String) in.readValue((String.class.getClassLoader())));
        this.uniqueCode = ((String) in.readValue((String.class.getClassLoader())));
        this.register = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.firsttime = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.qrCode = ((Object) in.readValue((Object.class.getClassLoader())));
        this.middleTag = ((Object) in.readValue((Object.class.getClassLoader())));
        this.backgroundColor = ((Object) in.readValue((Object.class.getClassLoader())));
        this.borderColor = ((Object) in.readValue((Object.class.getClassLoader())));
        this.patternColor = ((Object) in.readValue((Object.class.getClassLoader())));
        this.status = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.profilePic = ((Object) in.readValue((Object.class.getClassLoader())));
        this.notificationStatus = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.facebook = ((Facebook) in.readValue((Facebook.class.getClassLoader())));
        this.linkedin = ((Linkedin) in.readValue((Linkedin.class.getClassLoader())));
        this.twitter = ((Twitter) in.readValue((Twitter.class.getClassLoader())));
        this.instagram = ((Instagram) in.readValue((Instagram.class.getClassLoader())));
        this.google = ((Google) in.readValue((Google.class.getClassLoader())));
    }

    public ModelLoggedData() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUserDetail() {
        return userDetail;
    }

    public void setUserDetail(String userDetail) {
        this.userDetail = userDetail;
    }

    public String getUniqueCode() {
        return uniqueCode;
    }

    public void setUniqueCode(String uniqueCode) {
        this.uniqueCode = uniqueCode;
    }

    public Integer getRegister() {
        return register;
    }

    public void setRegister(Integer register) {
        this.register = register;
    }

    public Integer getFirsttime() {
        return firsttime;
    }

    public void setFirsttime(Integer firsttime) {
        this.firsttime = firsttime;
    }

    public Object getQrCode() {
        return qrCode;
    }

    public void setQrCode(Object qrCode) {
        this.qrCode = qrCode;
    }

    public Object getMiddleTag() {
        return middleTag;
    }

    public void setMiddleTag(Object middleTag) {
        this.middleTag = middleTag;
    }

    public Object getBackgroundColor() {
        return backgroundColor;
    }

    public void setBackgroundColor(Object backgroundColor) {
        this.backgroundColor = backgroundColor;
    }

    public Object getBorderColor() {
        return borderColor;
    }

    public void setBorderColor(Object borderColor) {
        this.borderColor = borderColor;
    }

    public Object getPatternColor() {
        return patternColor;
    }

    public void setPatternColor(Object patternColor) {
        this.patternColor = patternColor;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Object getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(Object profilePic) {
        this.profilePic = profilePic;
    }

    public Integer getNotificationStatus() {
        return notificationStatus;
    }

    public void setNotificationStatus(Integer notificationStatus) {
        this.notificationStatus = notificationStatus;
    }

    public Facebook getFacebook() {
        return facebook;
    }

    public void setFacebook(Facebook facebook) {
        this.facebook = facebook;
    }

    public Linkedin getLinkedin() {
        return linkedin;
    }

    public void setLinkedin(Linkedin linkedin) {
        this.linkedin = linkedin;
    }

    public Twitter getTwitter() {
        return twitter;
    }

    public void setTwitter(Twitter twitter) {
        this.twitter = twitter;
    }

    public Instagram getInstagram() {
        return instagram;
    }

    public void setInstagram(Instagram instagram) {
        this.instagram = instagram;
    }

    public Google getGoogle() {
        return google;
    }

    public void setGoogle(Google google) {
        this.google = google;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(id);
        dest.writeValue(name);
        dest.writeValue(firstname);
        dest.writeValue(lastname);
        dest.writeValue(email);
        dest.writeValue(userDetail);
        dest.writeValue(uniqueCode);
        dest.writeValue(register);
        dest.writeValue(firsttime);
        dest.writeValue(qrCode);
        dest.writeValue(middleTag);
        dest.writeValue(backgroundColor);
        dest.writeValue(borderColor);
        dest.writeValue(patternColor);
        dest.writeValue(status);
        dest.writeValue(profilePic);
        dest.writeValue(notificationStatus);
        dest.writeValue(facebook);
        dest.writeValue(linkedin);
        dest.writeValue(twitter);
        dest.writeValue(instagram);
        dest.writeValue(google);
    }

    public int describeContents() {
        return  0;
    }

}
