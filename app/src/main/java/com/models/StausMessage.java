package com.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.models.profilesemployee.Datum;

import java.util.List;

/**
 * Created by ZTLAB-12 on 14-05-2018.
 */

public class StausMessage implements Parcelable {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("msg")
    @Expose
    private String msg;
    @SerializedName("success")
    @Expose
    private String success;
    @SerializedName("data")
    @Expose
    private Data data;
    public final static Parcelable.Creator<StausMessage> CREATOR = new Creator<StausMessage>() {


        @SuppressWarnings({
                "unchecked"
        })
        public StausMessage createFromParcel(Parcel in) {
            return new StausMessage(in);
        }

        public StausMessage[] newArray(int size) {
            return (new StausMessage[size]);
        }

    };

    protected StausMessage(Parcel in) {
        this.status = ((String) in.readValue((String.class.getClassLoader())));
        this.msg = ((String) in.readValue((String.class.getClassLoader())));
        this.data = ((Data) in.readValue((Data.class.getClassLoader())));
        this.success = ((String) in.readValue((String.class.getClassLoader())));
    }

    public StausMessage() {
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(status);
        dest.writeValue(msg);
        dest.writeValue(data);
        dest.writeValue(success);
    }

    public int describeContents() {
        return 0;
    }

    public class Data implements Parcelable {

        @SerializedName("unique_code")
        @Expose
        private String uniqueCode;
        @SerializedName("profile_id")
        @Expose
        private String profile_id;

        public final Parcelable.Creator<Data> CREATOR = new Creator<Data>() {

            @SuppressWarnings({
                    "unchecked"
            })
            public Data createFromParcel(Parcel in) {
                return new Data(in);
            }

            public Data[] newArray(int size) {
                return (new Data[size]);
            }

        };

        protected Data(Parcel in) {
            this.uniqueCode = ((String) in.readValue((String.class.getClassLoader())));
            this.profile_id = ((String) in.readValue((String.class.getClassLoader())));
        }

        public Data() {
        }

        public String getUniqueCode() {
            return uniqueCode;
        }

        public void setUniqueCode(String uniqueCode) {
            this.uniqueCode = uniqueCode;
        }

        public void writeToParcel(Parcel dest, int flags) {
            dest.writeValue(uniqueCode);
        }

        public int describeContents() {
            return 0;
        }

        public String getProfile_id() {
            return profile_id;
        }

        public void setProfile_id(String profile_id) {
            this.profile_id = profile_id;
        }
    }


}
