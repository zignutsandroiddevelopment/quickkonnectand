package com.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by ZTLAB-12 on 13-06-2018.
 */

public class UpgradePlan {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("msg")
    @Expose
    private String msg;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }


    public static class Datum {

        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("description")
        @Expose
        private String description;
        @SerializedName("amount")
        @Expose
        private String amount;
        @SerializedName("min_employee_limit")
        @Expose
        private String minEmployeeLimit;
        @SerializedName("max_employee_limit")
        @Expose
        private String maxEmployeeLimit;
        @SerializedName("cost_per_employee")
        @Expose
        private String costPerEmployee;
        @SerializedName("max_employee_cost")
        @Expose
        private String max_employee_cost;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getAmount() {
            return amount;
        }

        public void setAmount(String amount) {
            this.amount = amount;
        }

        public String getMinEmployeeLimit() {
            return minEmployeeLimit;
        }

        public void setMinEmployeeLimit(String minEmployeeLimit) {
            this.minEmployeeLimit = minEmployeeLimit;
        }

        public String getMaxEmployeeLimit() {
            return maxEmployeeLimit;
        }

        public void setMaxEmployeeLimit(String maxEmployeeLimit) {
            this.maxEmployeeLimit = maxEmployeeLimit;
        }

        public String getCostPerEmployee() {
            return costPerEmployee;
        }

        public void setCostPerEmployee(String costPerEmployee) {
            this.costPerEmployee = costPerEmployee;
        }

        public String getMax_employee_cost() {
            return max_employee_cost;
        }

        public void setMax_employee_cost(String max_employee_cost) {
            this.max_employee_cost = max_employee_cost;
        }
    }

}
