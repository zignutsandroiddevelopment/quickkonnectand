package com.models;

import java.util.Date;

/**
 * Created by ZTLAB-12 on 24-05-2018.
 */

public class Attachments {

    private String doc;
    private String users;
    private long time;

    public Attachments() {
    }

    public Attachments(String url_file, String users) {
        this.doc = url_file;
        this.users = users;
        time = new Date().getTime();
    }

    public String getDoc() {
        return doc;
    }

    public void setDoc(String doc) {
        this.doc = doc;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public String getUsers() {
        return users;
    }

    public void setUsers(String users) {
        this.users = users;
    }
}
