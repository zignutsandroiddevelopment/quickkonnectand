
package com.models.EmployeeView;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("type")
    @Expose
    private Integer type;
    @SerializedName("employee_no")
    @Expose
    private String employeeNo;
    @SerializedName("department")
    @Expose
    private String department;
    @SerializedName("designation")
    @Expose
    private String designation;
    @SerializedName("valid_from")
    @Expose
    private String validFrom;
    @SerializedName("valid_to")
    @Expose
    private String validTo;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("profile_pic")
    @Expose
    private String profilePic;
    @SerializedName("unique_code")
    @Expose
    private String uniqueCode;
    @SerializedName("qk_tag_info")
    @Expose
    private QkTagInfo qkTagInfo;
    @SerializedName("fax")
    @Expose
    private List<Fax> fax = null;
    @SerializedName("phone")
    @Expose
    private List<Phone> phone = null;
    @SerializedName("email")
    @Expose
    private List<Email> email = null;
    @SerializedName("weblink")
    @Expose
    private List<Weblink> weblink = null;
    @SerializedName("profile")
    @Expose
    private Profile profile;
    @SerializedName("role")
    @Expose
    private Integer role;
    @SerializedName("role_name")
    @Expose
    private String roleName;

    @SerializedName("about")
    @Expose
    private String about;

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getEmployeeNo() {
        return employeeNo;
    }

    public void setEmployeeNo(String employeeNo) {
        this.employeeNo = employeeNo;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(String validFrom) {
        this.validFrom = validFrom;
    }

    public String getValidTo() {
        return validTo;
    }

    public void setValidTo(String validTo) {
        this.validTo = validTo;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    public String getUniqueCode() {
        return uniqueCode;
    }

    public void setUniqueCode(String uniqueCode) {
        this.uniqueCode = uniqueCode;
    }

    public QkTagInfo getQkTagInfo() {
        return qkTagInfo;
    }

    public void setQkTagInfo(QkTagInfo qkTagInfo) {
        this.qkTagInfo = qkTagInfo;
    }

    public List<Fax> getFax() {
        return fax;
    }

    public void setFax(List<Fax> fax) {
        this.fax = fax;
    }

    public List<Phone> getPhone() {
        return phone;
    }

    public void setPhone(List<Phone> phone) {
        this.phone = phone;
    }

    public List<Email> getEmail() {
        return email;
    }

    public void setEmail(List<Email> email) {
        this.email = email;
    }

    public List<Weblink> getWeblink() {
        return weblink;
    }

    public void setWeblink(List<Weblink> weblink) {
        this.weblink = weblink;
    }

    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }

    public Integer getRole() {
        return role;
    }

    public void setRole(Integer role) {
        this.role = role;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

}
