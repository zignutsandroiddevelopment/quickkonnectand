
package com.models.EmployeeView;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class QkTagInfo {

    @SerializedName("border_color")
    @Expose
    private String borderColor;
    @SerializedName("pattern_color")
    @Expose
    private String patternColor;
    @SerializedName("background_color")
    @Expose
    private String backgroundColor;
    @SerializedName("middle_tag")
    @Expose
    private String middleTag;
    @SerializedName("qr_code")
    @Expose
    private String qrCode;
    @SerializedName("background_gradient")
    @Expose
    private String backgroundGradient;

    public String getBorderColor() {
        return borderColor;
    }

    public void setBorderColor(String borderColor) {
        this.borderColor = borderColor;
    }

    public String getPatternColor() {
        return patternColor;
    }

    public void setPatternColor(String patternColor) {
        this.patternColor = patternColor;
    }

    public String getBackgroundColor() {
        return backgroundColor;
    }

    public void setBackgroundColor(String backgroundColor) {
        this.backgroundColor = backgroundColor;
    }

    public String getMiddleTag() {
        return middleTag;
    }

    public void setMiddleTag(String middleTag) {
        this.middleTag = middleTag;
    }

    public String getQrCode() {
        return qrCode;
    }

    public void setQrCode(String qrCode) {
        this.qrCode = qrCode;
    }

    public String getBackgroundGradient() {
        return backgroundGradient;
    }

    public void setBackgroundGradient(String backgroundGradient) {
        this.backgroundGradient = backgroundGradient;
    }

}
