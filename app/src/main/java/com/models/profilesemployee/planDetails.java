package com.models.profilesemployee;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ZTLAB-12 on 26-06-2018.
 */

public class planDetails {


    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("max_employee")
    @Expose
    private String max_employee;

    @SerializedName("added_employees")
    @Expose
    private String added_employees;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMax_employee() {
        return max_employee;
    }

    public void setMax_employee(String max_employee) {
        this.max_employee = max_employee;
    }

    public String getAdded_employees() {
        return added_employees;
    }

    public void setAdded_employees(String added_employees) {
        this.added_employees = added_employees;
    }
}
