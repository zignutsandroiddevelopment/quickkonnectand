
package com.models.notification;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Aps {

    @SerializedName("alert")
    @Expose
    private Alert alert;
    @SerializedName("badge")
    @Expose
    private String badge;
    @SerializedName("sound")
    @Expose
    private String sound;

    public Alert getAlert() {
        return alert;
    }

    public void setAlert(Alert alert) {
        this.alert = alert;
    }

    public String getBadge() {
        return badge;
    }

    public void setBadge(String badge) {
        this.badge = badge;
    }

    public String getSound() {
        return sound;
    }

    public void setSound(String sound) {
        this.sound = sound;
    }

}
