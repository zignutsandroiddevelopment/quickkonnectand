
package com.models;

import java.util.List;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ModelApprovedContact implements Parcelable
{

    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("contact_detail")
    @Expose
    private List<ContactDetail> contactDetail = null;
    public final static Creator<ModelApprovedContact> CREATOR = new Creator<ModelApprovedContact>() {


        @SuppressWarnings({
            "unchecked"
        })
        public ModelApprovedContact createFromParcel(Parcel in) {
            return new ModelApprovedContact(in);
        }

        public ModelApprovedContact[] newArray(int size) {
            return (new ModelApprovedContact[size]);
        }

    }
    ;

    protected ModelApprovedContact(Parcel in) {
        this.userId = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.username = ((String) in.readValue((String.class.getClassLoader())));
        this.email = ((String) in.readValue((String.class.getClassLoader())));
        in.readList(this.contactDetail, (ContactDetail.class.getClassLoader()));
    }

    public ModelApprovedContact() {
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<ContactDetail> getContactDetail() {
        return contactDetail;
    }

    public void setContactDetail(List<ContactDetail> contactDetail) {
        this.contactDetail = contactDetail;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(userId);
        dest.writeValue(username);
        dest.writeValue(email);
        dest.writeList(contactDetail);
    }

    public int describeContents() {
        return  0;
    }

}
