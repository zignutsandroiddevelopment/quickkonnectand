
package com.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ModelLoggedUser implements Parcelable
{

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("msg")
    @Expose
    private String msg;
    @SerializedName("data")
    @Expose
    private ModelLoggedData data;
    public final static Creator<ModelLoggedUser> CREATOR = new Creator<ModelLoggedUser>() {


        @SuppressWarnings({
            "unchecked"
        })
        public ModelLoggedUser createFromParcel(Parcel in) {
            return new ModelLoggedUser(in);
        }

        public ModelLoggedUser[] newArray(int size) {
            return (new ModelLoggedUser[size]);
        }

    }
    ;

    protected ModelLoggedUser(Parcel in) {
        this.status = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.msg = ((String) in.readValue((String.class.getClassLoader())));
        this.data = ((ModelLoggedData) in.readValue((ModelLoggedData.class.getClassLoader())));
    }

    public ModelLoggedUser() {
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ModelLoggedData getData() {
        return data;
    }

    public void setData(ModelLoggedData data) {
        this.data = data;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(status);
        dest.writeValue(msg);
        dest.writeValue(data);
    }

    public int describeContents() {
        return  0;
    }

}
