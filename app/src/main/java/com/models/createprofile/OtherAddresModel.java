package com.models.createprofile;

/**
 * Created by ZTLAB03 on 15-03-2018.
 */

public class OtherAddresModel {

    String name;

    public String getSubadd() {
        return subadd;
    }

    public void setSubadd(String subadd) {
        this.subadd = subadd;
    }

    String subadd;
    int id;
    double lati;
    double longi;

    public double getLati() {
        return lati;
    }

    public void setLati(double lati) {
        this.lati = lati;
    }

    public double getLongi() {
        return longi;
    }

    public void setLongi(double longi) {
        this.longi = longi;
    }

    public String getAddress() {
        return name;
    }

    public void setAddress(String address) {
        this.name = address;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
