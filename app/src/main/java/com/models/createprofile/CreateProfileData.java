
package com.models.createprofile;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.models.tagsdata.ModelTagDetail;

import java.util.List;

public class CreateProfileData implements Parcelable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("type")
    @Expose
    private Integer type;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("longitude")
    @Expose
    private String longitude;
    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("about")
    @Expose
    private String about;
    @SerializedName("logo")
    @Expose
    private String logo;
    @SerializedName("tag")
    @Expose
    private Integer tag;
    @SerializedName("sub_tag")
    @Expose
    private Integer subTag;
    @SerializedName("company_team_size")
    @Expose
    private Integer companyTeamSize;
    @SerializedName("establish_date")
    @Expose
    private String establishDate;
    @SerializedName("unique_code")
    @Expose
    private String uniqueCode;
    @SerializedName("status")
    @Expose
    private String status;

    @SerializedName("employee_unique_code")
    @Expose
    private String employee_unique_code;


    @SerializedName("dob")
    @Expose
    private String dob;
    @SerializedName("current_ceo")
    @Expose
    private String current_ceo;
    @SerializedName("current_cfo")
    @Expose
    private String current_cfo;

    @SerializedName("transfer_status")
    @Expose
    private String transfer_status;
    @SerializedName("is_transfer")
    @Expose
    private String is_transfer;
    @SerializedName("role")
    @Expose
    private String role;
    @SerializedName("role_name")
    @Expose
    private String role_name;
    @SerializedName("is_follower")
    @Expose
    private String is_follower;
    @SerializedName("profile_view_code")
    @Expose
    private String profile_view_code;

    public String getTransfer_status() {
        return transfer_status;
    }

    public void setTransfer_status(String transfer_status) {
        this.transfer_status = transfer_status;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getRole_name() {
        return role_name;
    }

    public void setRole_name(String role_name) {
        this.role_name = role_name;
    }

    public String getIs_transfer() {
        return is_transfer;
    }

    public void setIs_transfer(String is_transfer) {
        this.is_transfer = is_transfer;
    }

    @SerializedName("social_media")
    @Expose
    private List<SocialMedium> socialMedia = null;

    @SerializedName("otheraddress")
    @Expose
    private List<OtherAddresModel> otherAddress = null;

    @SerializedName("founders")
    @Expose
    private List<Founder> founders = null;
    @SerializedName("phone")
    @Expose
    private List<Phone> phone = null;
    @SerializedName("email")
    @Expose
    private List<Email> email = null;
    @SerializedName("weblink")
    @Expose
    private List<Weblink> weblink = null;

    private Boolean ischeckedBox;

    private String subtagText;

    public Boolean getIscheckedBox() {
        return ischeckedBox;
    }

    public void setIscheckedBox(Boolean ischeckedBox) {
        this.ischeckedBox = ischeckedBox;
    }
    //List<ModelTagDetail> modelTagDetails;

    public final static Creator<CreateProfileData> CREATOR = new Creator<CreateProfileData>() {


        @SuppressWarnings({
                "unchecked"
        })
        public CreateProfileData createFromParcel(Parcel in) {
            return new CreateProfileData(in);
        }

        public CreateProfileData[] newArray(int size) {
            return (new CreateProfileData[size]);
        }

    };

    protected CreateProfileData(Parcel in) {
        this.id = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.userId = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.type = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.name = ((String) in.readValue((String.class.getClassLoader())));
        this.address = ((String) in.readValue((String.class.getClassLoader())));
        this.longitude = ((String) in.readValue((String.class.getClassLoader())));
        this.latitude = ((String) in.readValue((String.class.getClassLoader())));
        this.about = ((String) in.readValue((String.class.getClassLoader())));
        this.logo = ((String) in.readValue((String.class.getClassLoader())));
        this.tag = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.subTag = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.companyTeamSize = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.establishDate = ((String) in.readValue((String.class.getClassLoader())));
        this.uniqueCode = ((String) in.readValue((String.class.getClassLoader())));
        this.status = ((String) in.readValue((String.class.getClassLoader())));
        this.dob = ((String) in.readValue((String.class.getClassLoader())));
        this.current_ceo = ((String) in.readValue((String.class.getClassLoader())));
        this.current_cfo = ((String) in.readValue((String.class.getClassLoader())));
        this.is_transfer = ((String) in.readValue((String.class.getClassLoader())));
        this.transfer_status = ((String) in.readValue((String.class.getClassLoader())));
        this.role = ((String) in.readValue((String.class.getClassLoader())));
        this.role_name = ((String) in.readValue((String.class.getClassLoader())));
        this.profile_view_code = ((String) in.readValue((String.class.getClassLoader())));
        this.employee_unique_code = ((String) in.readValue((String.class.getClassLoader())));
        this.is_follower = ((String) in.readValue((String.class.getClassLoader())));
        in.readList(this.socialMedia, (SocialMedium.class.getClassLoader()));
        in.readList(this.founders, (Founder.class.getClassLoader()));
        in.readList(this.phone, (com.models.createprofile.Phone.class.getClassLoader()));
        in.readList(this.email, (com.models.createprofile.Email.class.getClassLoader()));
        in.readList(this.weblink, (com.models.createprofile.Weblink.class.getClassLoader()));
        in.readList(this.otherAddress, (ModelTagDetail.class.getClassLoader()));
    }

    public CreateProfileData() {
    }

    public List<OtherAddresModel> getOtherAddress() {
        return otherAddress;
    }

    public void setOtherAddress(List<OtherAddresModel> otherAddress) {
        this.otherAddress = otherAddress;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public Integer getTag() {
        return tag;
    }

    public void setTag(Integer tag) {
        this.tag = tag;
    }

    public Integer getSubTag() {
        return subTag;
    }

    public void setSubTag(Integer subTag) {
        this.subTag = subTag;
    }

    public Integer getCompanyTeamSize() {
        return companyTeamSize;
    }

    public void setCompanyTeamSize(Integer companyTeamSize) {
        this.companyTeamSize = companyTeamSize;
    }

    public String getEmployee_unique_code() {
        return employee_unique_code;
    }

    public void setEmployee_unique_code(String employee_unique_code) {
        this.employee_unique_code = employee_unique_code;
    }

    public String getEstablishDate() {
        return establishDate;
    }

    public void setEstablishDate(String establishDate) {
        this.establishDate = establishDate;
    }

    public String getUniqueCode() {
        return uniqueCode;
    }

    public void setUniqueCode(String uniqueCode) {
        this.uniqueCode = uniqueCode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<SocialMedium> getSocialMedia() {
        return socialMedia;
    }

    public void setSocialMedia(List<SocialMedium> socialMedia) {
        this.socialMedia = socialMedia;
    }

    public List<Founder> getFounders() {
        return founders;
    }

    public void setFounders(List<Founder> founders) {
        this.founders = founders;
    }

    public List<Phone> getPhone() {
        return phone;
    }

    public void setPhone(List<Phone> phone) {
        this.phone = phone;
    }

    public List<Email> getEmail() {
        return email;
    }

    public void setEmail(List<Email> email) {
        this.email = email;
    }

    public List<Weblink> getWeblink() {
        return weblink;
    }

    public void setWeblink(List<Weblink> weblink) {
        this.weblink = weblink;
    }

    public String getSubtagText() {
        return subtagText;
    }

    public void setSubtagText(String subtagText) {
        this.subtagText = subtagText;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getCurrent_ceo() {
        return current_ceo;
    }

    public void setCurrent_ceo(String current_ceo) {
        this.current_ceo = current_ceo;
    }

    public String getCurrent_cfo() {
        return current_cfo;
    }

    public void setCurrent_cfo(String current_cfo) {
        this.current_cfo = current_cfo;
    }

    public String getProfile_view_code() {
        return profile_view_code;
    }

    public void setProfile_view_code(String profile_view_code) {
        this.profile_view_code = profile_view_code;
    }

    public String getIs_follower() {
        return is_follower;
    }

    public void setIs_follower(String is_follower) {
        this.is_follower = is_follower;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(id);
        dest.writeValue(userId);
        dest.writeValue(type);
        dest.writeValue(name);
        dest.writeValue(address);
        dest.writeValue(longitude);
        dest.writeValue(latitude);
        dest.writeValue(about);
        dest.writeValue(logo);
        dest.writeValue(tag);
        dest.writeValue(subTag);
        dest.writeValue(companyTeamSize);
        dest.writeValue(establishDate);
        dest.writeValue(uniqueCode);
        dest.writeValue(status);
        dest.writeValue(dob);
        dest.writeValue(current_ceo);
        dest.writeValue(current_cfo);
        dest.writeList(socialMedia);
        dest.writeList(founders);
        dest.writeList(phone);
        dest.writeList(email);
        dest.writeList(weblink);
        dest.writeList(otherAddress);
        dest.writeValue(is_transfer);
        dest.writeValue(transfer_status);
        dest.writeValue(role);
        dest.writeValue(role_name);
        dest.writeValue(employee_unique_code);
        dest.writeValue(profile_view_code);
        dest.writeValue(is_follower);
    }

    public int describeContents() {
        return 0;
    }

}
