
package com.models.createprofile;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Phone implements Parcelable
{

    @SerializedName("name")
    @Expose
    private String name;
    public final static Creator<Phone> CREATOR = new Creator<Phone>() {


        @SuppressWarnings({
            "unchecked"
        })
        public Phone createFromParcel(Parcel in) {
            return new Phone(in);
        }

        public Phone[] newArray(int size) {
            return (new Phone[size]);
        }

    }
    ;

    protected Phone(Parcel in) {
        this.name = ((String) in.readValue((String.class.getClassLoader())));
    }

    public Phone() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(name);
    }

    public int describeContents() {
        return  0;
    }

}
