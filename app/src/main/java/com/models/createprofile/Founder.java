
package com.models.createprofile;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Founder implements Parcelable
{

    @SerializedName("firstname")
    @Expose
    private String firstname;
    @SerializedName("lastname")
    @Expose
    private String lastname;
    @SerializedName("designation")
    @Expose
    private String designation;
    @SerializedName("email")
    @Expose
    private String email;


    public final static Creator<Founder> CREATOR = new Creator<Founder>() {


        @SuppressWarnings({
            "unchecked"
        })
        public Founder createFromParcel(Parcel in) {
            return new Founder(in);
        }

        public Founder[] newArray(int size) {
            return (new Founder[size]);
        }

    }
    ;

    protected Founder(Parcel in) {
        this.firstname = ((String) in.readValue((String.class.getClassLoader())));
        this.lastname = ((String) in.readValue((String.class.getClassLoader())));
        this.designation = ((String) in.readValue((String.class.getClassLoader())));
        this.email = ((String) in.readValue((String.class.getClassLoader())));
    }

    public Founder() {
    }
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(firstname);
        dest.writeValue(lastname);
        dest.writeValue(designation);
        dest.writeValue(email);
    }

    public int describeContents() {
        return  0;
    }

}
