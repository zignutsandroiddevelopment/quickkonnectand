
package com.models.createprofile;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Email implements Parcelable
{

    @SerializedName("name")
    @Expose
    private String name;
    public final static Creator<Email> CREATOR = new Creator<Email>() {


        @SuppressWarnings({
            "unchecked"
        })
        public Email createFromParcel(Parcel in) {
            return new Email(in);
        }

        public Email[] newArray(int size) {
            return (new Email[size]);
        }

    }
    ;

    protected Email(Parcel in) {
        this.name = ((String) in.readValue((String.class.getClassLoader())));
    }

    public Email() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(name);
    }

    public int describeContents() {
        return  0;
    }

}
