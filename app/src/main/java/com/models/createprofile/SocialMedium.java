
package com.models.createprofile;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SocialMedium implements Parcelable {

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("type")
    @Expose
    private String type="1";

    public final static Creator<SocialMedium> CREATOR = new Creator<SocialMedium>() {


        @SuppressWarnings({
                "unchecked"
        })
        public SocialMedium createFromParcel(Parcel in) {
            return new SocialMedium(in);
        }

        public SocialMedium[] newArray(int size) {
            return (new SocialMedium[size]);
        }

    };

    protected SocialMedium(Parcel in) {
        this.name = ((String) in.readValue((String.class.getClassLoader())));
        this.type = ((String) in.readValue((String.class.getClassLoader())));
    }

    public SocialMedium() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(name);
        dest.writeValue(type);
    }

    public int describeContents() {
        return 0;
    }

}
