
package com.models.createprofile;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Weblink implements Parcelable {

    @SerializedName("name")
    @Expose
    private String name;
    public final static Creator<Weblink> CREATOR = new Creator<Weblink>() {


        @SuppressWarnings({
                "unchecked"
        })
        public Weblink createFromParcel(Parcel in) {
            return new Weblink(in);
        }

        public Weblink[] newArray(int size) {
            return (new Weblink[size]);
        }

    };

    protected Weblink(Parcel in) {
        this.name = ((String) in.readValue((String.class.getClassLoader())));
    }

    public Weblink() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(name);
    }

    public int describeContents() {
        return 0;
    }

}
