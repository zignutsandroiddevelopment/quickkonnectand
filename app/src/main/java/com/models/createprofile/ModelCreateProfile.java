
package com.models.createprofile;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ModelCreateProfile implements Parcelable
{

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("msg")
    @Expose
    private String msg;
    @SerializedName("data")
    @Expose
    private CreateProfileData data;
    public final static Creator<ModelCreateProfile> CREATOR = new Creator<ModelCreateProfile>() {

        @SuppressWarnings({
            "unchecked"
        })
        public ModelCreateProfile createFromParcel(Parcel in) {
            return new ModelCreateProfile(in);
        }

        public ModelCreateProfile[] newArray(int size) {
            return (new ModelCreateProfile[size]);
        }

    }
    ;

    protected ModelCreateProfile(Parcel in) {
        this.status = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.msg = ((String) in.readValue((String.class.getClassLoader())));
        this.data = ((CreateProfileData) in.readValue((CreateProfileData.class.getClassLoader())));
    }

    public ModelCreateProfile() {
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public CreateProfileData getData() {
        return data;
    }

    public void setData(CreateProfileData data) {
        this.data = data;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(status);
        dest.writeValue(msg);
        dest.writeValue(data);
    }

    public int describeContents() {
        return  0;
    }

}
