
package com.models.tagsdata;

import java.util.List;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ModelTagData implements Parcelable
{

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("msg")
    @Expose
    private String msg;
    @SerializedName("data")
    @Expose
    private List<ModelTagDetail> data = null;
    public final static Creator<ModelTagData> CREATOR = new Creator<ModelTagData>() {


        @SuppressWarnings({
            "unchecked"
        })
        public ModelTagData createFromParcel(Parcel in) {
            return new ModelTagData(in);
        }

        public ModelTagData[] newArray(int size) {
            return (new ModelTagData[size]);
        }

    }
    ;

    protected ModelTagData(Parcel in) {
        this.status = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.msg = ((String) in.readValue((String.class.getClassLoader())));
        in.readList(this.data, (ModelTagDetail.class.getClassLoader()));
    }

    public ModelTagData() {
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<ModelTagDetail> getData() {
        return data;
    }

    public void setData(List<ModelTagDetail> data) {
        this.data = data;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(status);
        dest.writeValue(msg);
        dest.writeList(data);
    }

    public int describeContents() {
        return  0;
    }

}
