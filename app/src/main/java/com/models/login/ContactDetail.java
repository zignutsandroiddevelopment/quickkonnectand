
package com.models.login;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ContactDetail implements Parcelable
{

    @SerializedName("contact_id")
    @Expose
    private Integer contactId;
    @SerializedName("contact_type")
    @Expose
    private String contactType;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("phone")
    @Expose
    private String phone;
    public final static Creator<ContactDetail> CREATOR = new Creator<ContactDetail>() {


        @SuppressWarnings({
            "unchecked"
        })
        public ContactDetail createFromParcel(Parcel in) {
            return new ContactDetail(in);
        }

        public ContactDetail[] newArray(int size) {
            return (new ContactDetail[size]);
        }

    }
    ;

    protected ContactDetail(Parcel in) {
        this.contactId = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.contactType = ((String) in.readValue((String.class.getClassLoader())));
        this.email = ((String) in.readValue((String.class.getClassLoader())));
        this.phone = ((String) in.readValue((String.class.getClassLoader())));
    }

    public ContactDetail() {
    }

    public Integer getContactId() {
        return contactId;
    }

    public void setContactId(Integer contactId) {
        this.contactId = contactId;
    }

    public String getContactType() {
        return contactType;
    }

    public void setContactType(String contactType) {
        this.contactType = contactType;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(contactId);
        dest.writeValue(contactType);
        dest.writeValue(email);
        dest.writeValue(phone);
    }

    public int describeContents() {
        return  0;
    }

}
