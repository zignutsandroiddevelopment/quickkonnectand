
package com.models.login;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LanguageDetail implements Parcelable
{

    @SerializedName("langauge_id")
    @Expose
    private Integer langaugeId;
    @SerializedName("langauge_name")
    @Expose
    private String langaugeName;
    @SerializedName("rating")
    @Expose
    private String rating;
    public final static Creator<LanguageDetail> CREATOR = new Creator<LanguageDetail>() {


        @SuppressWarnings({
            "unchecked"
        })
        public LanguageDetail createFromParcel(Parcel in) {
            return new LanguageDetail(in);
        }

        public LanguageDetail[] newArray(int size) {
            return (new LanguageDetail[size]);
        }

    }
    ;

    protected LanguageDetail(Parcel in) {
        this.langaugeId = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.langaugeName = ((String) in.readValue((String.class.getClassLoader())));
        this.rating = ((String) in.readValue((String.class.getClassLoader())));
    }

    public LanguageDetail() {
    }

    public Integer getLangaugeId() {
        return langaugeId;
    }

    public void setLangaugeId(Integer langaugeId) {
        this.langaugeId = langaugeId;
    }

    public String getLangaugeName() {
        return langaugeName;
    }

    public void setLangaugeName(String langaugeName) {
        this.langaugeName = langaugeName;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(langaugeId);
        dest.writeValue(langaugeName);
        dest.writeValue(rating);
    }

    public int describeContents() {
        return  0;
    }

}
