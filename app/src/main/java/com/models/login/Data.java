
package com.models.login;

import java.util.List;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data implements Parcelable
{

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("firstname")
    @Expose
    private String firstname;
    @SerializedName("lastname")
    @Expose
    private String lastname;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("firsttime")
    @Expose
    private Integer firsttime;
    @SerializedName("register")
    @Expose
    private Integer register;
    @SerializedName("user_detail")
    @Expose
    private String userDetail;
    @SerializedName("unique_code")
    @Expose
    private String uniqueCode;
    @SerializedName("qr_code")
    @Expose
    private String qrCode;
    @SerializedName("middle_tag")
    @Expose
    private String middleTag;
    @SerializedName("background_color")
    @Expose
    private String backgroundColor;
    @SerializedName("border_color")
    @Expose
    private String borderColor;
    @SerializedName("pattern_color")
    @Expose
    private String patternColor;
    @SerializedName("background_gradient")
    @Expose
    private String backgroundGradient;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("profile_pic")
    @Expose
    private String profilePic;
    @SerializedName("notification_status")
    @Expose
    private Integer notificationStatus;
    @SerializedName("facebook")
    @Expose
    private Facebook facebook;
    @SerializedName("linkedin")
    @Expose
    private Linkedin linkedin;
    @SerializedName("twitter")
    @Expose
    private Twitter twitter;
    @SerializedName("instagram")
    @Expose
    private Instagram instagram;
    @SerializedName("google")
    @Expose
    private Google google;
    @SerializedName("snapchat")
    @Expose
    private Snapchat snapchat;
    @SerializedName("basic_detail")
    @Expose
    private BasicDetail basicDetail;
    @SerializedName("contact_detail")
    @Expose
    private List<ContactDetail> contactDetail = null;
    @SerializedName("education_detail")
    @Expose
    private List<EducationDetail> educationDetail = null;
    @SerializedName("experience_detail")
    @Expose
    private List<ExperienceDetail> experienceDetail = null;
    @SerializedName("publication_detail")
    @Expose
    private List<Object> publicationDetail = null;
    @SerializedName("language_detail")
    @Expose
    private List<LanguageDetail> languageDetail = null;
    @SerializedName("contact_count")
    @Expose
    private ContactCount contactCount;
    @SerializedName("subscription")
    @Expose
    private Integer subscription;

    public final static Creator<Data> CREATOR = new Creator<Data>() {

        @SuppressWarnings({
            "unchecked"
        })
        public Data createFromParcel(Parcel in) {
            return new Data(in);
        }

        public Data[] newArray(int size) {
            return (new Data[size]);
        }

    }
    ;

    protected Data(Parcel in) {
        this.id = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.name = ((String) in.readValue((String.class.getClassLoader())));
        this.firstname = ((String) in.readValue((String.class.getClassLoader())));
        this.lastname = ((String) in.readValue((String.class.getClassLoader())));
        this.email = ((String) in.readValue((String.class.getClassLoader())));
        this.firsttime = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.register = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.userDetail = ((String) in.readValue((String.class.getClassLoader())));
        this.uniqueCode = ((String) in.readValue((String.class.getClassLoader())));
        this.qrCode = ((String) in.readValue((String.class.getClassLoader())));
        this.middleTag = ((String) in.readValue((String.class.getClassLoader())));
        this.backgroundColor = ((String) in.readValue((String.class.getClassLoader())));
        this.borderColor = ((String) in.readValue((String.class.getClassLoader())));
        this.patternColor = ((String) in.readValue((String.class.getClassLoader())));
        this.backgroundGradient = ((String) in.readValue((String.class.getClassLoader())));
        this.status = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.profilePic = ((String) in.readValue((String.class.getClassLoader())));
        this.notificationStatus = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.facebook = ((Facebook) in.readValue((Facebook.class.getClassLoader())));
        this.linkedin = ((Linkedin) in.readValue((Linkedin.class.getClassLoader())));
        this.twitter = ((Twitter) in.readValue((Twitter.class.getClassLoader())));
        this.instagram = ((Instagram) in.readValue((Instagram.class.getClassLoader())));
        this.google = ((Google) in.readValue((Google.class.getClassLoader())));
        this.snapchat = ((Snapchat) in.readValue((Snapchat.class.getClassLoader())));
        this.basicDetail = ((BasicDetail) in.readValue((BasicDetail.class.getClassLoader())));
        in.readList(this.contactDetail, (ContactDetail.class.getClassLoader()));
        in.readList(this.educationDetail, (EducationDetail.class.getClassLoader()));
        in.readList(this.experienceDetail, (ExperienceDetail.class.getClassLoader()));
        in.readList(this.publicationDetail, (Object.class.getClassLoader()));
        in.readList(this.languageDetail, (LanguageDetail.class.getClassLoader()));
        this.contactCount = ((ContactCount) in.readValue((ContactCount.class.getClassLoader())));
        this.subscription = ((Integer) in.readValue((Integer.class.getClassLoader())));
    }

    public Data() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getFirsttime() {
        return firsttime;
    }

    public void setFirsttime(Integer firsttime) {
        this.firsttime = firsttime;
    }

    public Integer getRegister() {
        return register;
    }

    public void setRegister(Integer register) {
        this.register = register;
    }

    public String getUserDetail() {
        return userDetail;
    }

    public void setUserDetail(String userDetail) {
        this.userDetail = userDetail;
    }

    public String getUniqueCode() {
        return uniqueCode;
    }

    public void setUniqueCode(String uniqueCode) {
        this.uniqueCode = uniqueCode;
    }

    public String getQrCode() {
        return qrCode;
    }

    public void setQrCode(String qrCode) {
        this.qrCode = qrCode;
    }

    public String getMiddleTag() {
        return middleTag;
    }

    public void setMiddleTag(String middleTag) {
        this.middleTag = middleTag;
    }

    public String getBackgroundColor() {
        return backgroundColor;
    }

    public void setBackgroundColor(String backgroundColor) {
        this.backgroundColor = backgroundColor;
    }

    public String getBorderColor() {
        return borderColor;
    }

    public void setBorderColor(String borderColor) {
        this.borderColor = borderColor;
    }

    public String getPatternColor() {
        return patternColor;
    }

    public void setPatternColor(String patternColor) {
        this.patternColor = patternColor;
    }

    public String getBackgroundGradient() {
        return backgroundGradient;
    }

    public void setBackgroundGradient(String backgroundGradient) {
        this.backgroundGradient = backgroundGradient;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    public Integer getNotificationStatus() {
        return notificationStatus;
    }

    public void setNotificationStatus(Integer notificationStatus) {
        this.notificationStatus = notificationStatus;
    }


    public Snapchat getSnapchat() {
        return snapchat;
    }

    public void setSnapchat(Snapchat snapchat) {
        this.snapchat = snapchat;
    }

    public Facebook getFacebook() {
        return facebook;
    }

    public void setFacebook(Facebook facebook) {
        this.facebook = facebook;
    }

    public Linkedin getLinkedin() {
        return linkedin;
    }

    public void setLinkedin(Linkedin linkedin) {
        this.linkedin = linkedin;
    }

    public Twitter getTwitter() {
        return twitter;
    }

    public void setTwitter(Twitter twitter) {
        this.twitter = twitter;
    }

    public Instagram getInstagram() {
        return instagram;
    }

    public void setInstagram(Instagram instagram) {
        this.instagram = instagram;
    }

    public Google getGoogle() {
        return google;
    }

    public void setGoogle(Google google) {
        this.google = google;
    }

    public BasicDetail getBasicDetail() {
        return basicDetail;
    }

    public void setBasicDetail(BasicDetail basicDetail) {
        this.basicDetail = basicDetail;
    }

    public List<ContactDetail> getContactDetail() {
        return contactDetail;
    }

    public void setContactDetail(List<ContactDetail> contactDetail) {
        this.contactDetail = contactDetail;
    }

    public List<EducationDetail> getEducationDetail() {
        return educationDetail;
    }

    public void setEducationDetail(List<EducationDetail> educationDetail) {
        this.educationDetail = educationDetail;
    }

    public List<ExperienceDetail> getExperienceDetail() {
        return experienceDetail;
    }

    public void setExperienceDetail(List<ExperienceDetail> experienceDetail) {
        this.experienceDetail = experienceDetail;
    }

    public List<Object> getPublicationDetail() {
        return publicationDetail;
    }

    public void setPublicationDetail(List<Object> publicationDetail) {
        this.publicationDetail = publicationDetail;
    }

    public List<LanguageDetail> getLanguageDetail() {
        return languageDetail;
    }

    public void setLanguageDetail(List<LanguageDetail> languageDetail) {
        this.languageDetail = languageDetail;
    }

    public ContactCount getContactCount() {
        return contactCount;
    }

    public void setContactCount(ContactCount contactCount) {
        this.contactCount = contactCount;
    }

    public Integer getSubscription() {
        return subscription;
    }

    public void setSubscription(Integer subscription) {
        this.subscription = subscription;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(id);
        dest.writeValue(name);
        dest.writeValue(firstname);
        dest.writeValue(lastname);
        dest.writeValue(email);
        dest.writeValue(firsttime);
        dest.writeValue(register);
        dest.writeValue(userDetail);
        dest.writeValue(uniqueCode);
        dest.writeValue(qrCode);
        dest.writeValue(middleTag);
        dest.writeValue(backgroundColor);
        dest.writeValue(borderColor);
        dest.writeValue(patternColor);
        dest.writeValue(backgroundGradient);
        dest.writeValue(status);
        dest.writeValue(profilePic);
        dest.writeValue(notificationStatus);
        dest.writeValue(facebook);
        dest.writeValue(linkedin);
        dest.writeValue(twitter);
        dest.writeValue(instagram);
        dest.writeValue(snapchat);
        dest.writeValue(google);
        dest.writeValue(basicDetail);
        dest.writeList(contactDetail);
        dest.writeList(educationDetail);
        dest.writeList(experienceDetail);
        dest.writeList(publicationDetail);
        dest.writeList(languageDetail);
        dest.writeValue(contactCount);
        dest.writeValue(subscription);
    }

    public int describeContents() {
        return  0;
    }

}
