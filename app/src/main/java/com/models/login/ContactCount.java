
package com.models.login;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ContactCount implements Parcelable
{

    @SerializedName("contact_all")
    @Expose
    private Integer contactAll;
    @SerializedName("contact_week")
    @Expose
    private Integer contactWeek;
    @SerializedName("contact_month")
    @Expose
    private Integer contactMonth;
    public final static Creator<ContactCount> CREATOR = new Creator<ContactCount>() {


        @SuppressWarnings({
            "unchecked"
        })
        public ContactCount createFromParcel(Parcel in) {
            return new ContactCount(in);
        }

        public ContactCount[] newArray(int size) {
            return (new ContactCount[size]);
        }

    }
    ;

    protected ContactCount(Parcel in) {
        this.contactAll = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.contactWeek = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.contactMonth = ((Integer) in.readValue((Integer.class.getClassLoader())));
    }

    public ContactCount() {
    }

    public Integer getContactAll() {
        return contactAll;
    }

    public void setContactAll(Integer contactAll) {
        this.contactAll = contactAll;
    }

    public Integer getContactWeek() {
        return contactWeek;
    }

    public void setContactWeek(Integer contactWeek) {
        this.contactWeek = contactWeek;
    }

    public Integer getContactMonth() {
        return contactMonth;
    }

    public void setContactMonth(Integer contactMonth) {
        this.contactMonth = contactMonth;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(contactAll);
        dest.writeValue(contactWeek);
        dest.writeValue(contactMonth);
    }

    public int describeContents() {
        return  0;
    }

}
