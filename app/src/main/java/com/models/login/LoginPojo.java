
package com.models.login;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LoginPojo implements Parcelable {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("msg")
    @Expose
    private String msg;
    @SerializedName("data")
    @Expose
    private Data data;
    @SerializedName("api_token")
    @Expose
    private String apiToken;
    public final static Creator<LoginPojo> CREATOR = new Creator<LoginPojo>() {


        @SuppressWarnings({
                "unchecked"
        })
        public LoginPojo createFromParcel(Parcel in) {
            return new LoginPojo(in);
        }

        public LoginPojo[] newArray(int size) {
            return (new LoginPojo[size]);
        }

    };

    protected LoginPojo(Parcel in) {
        this.status = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.msg = ((String) in.readValue((String.class.getClassLoader())));
        this.data = ((Data) in.readValue((Data.class.getClassLoader())));
        this.apiToken = ((String) in.readValue((String.class.getClassLoader())));
    }

    public LoginPojo() {
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getApiToken() {
        return apiToken;
    }

    public void setApiToken(String apiToken) {
        this.apiToken = apiToken;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(status);
        dest.writeValue(msg);
        dest.writeValue(data);
        dest.writeValue(apiToken);
    }

    public int describeContents() {
        return 0;
    }

}
