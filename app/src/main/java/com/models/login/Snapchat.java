package com.models.login;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ZTLAB-12 on 12-07-2018.
 */

public class Snapchat implements Parcelable {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("data")
    @Expose
    private Object data;
    public final static Creator<Snapchat> CREATOR = new Creator<Snapchat>() {


        @SuppressWarnings({
                "unchecked"
        })
        public Snapchat createFromParcel(Parcel in) {
            return new Snapchat(in);
        }

        public Snapchat[] newArray(int size) {
            return (new Snapchat[size]);
        }

    };

    protected Snapchat(Parcel in) {
        this.status = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.data = ((Object) in.readValue((Object.class.getClassLoader())));
    }

    public Snapchat() {
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(status);
        dest.writeValue(data);
    }

    public int describeContents() {
        return 0;
    }

}
