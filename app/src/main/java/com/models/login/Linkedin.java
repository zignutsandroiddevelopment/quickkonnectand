
package com.models.login;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Linkedin implements Parcelable
{

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("data")
    @Expose
    private Object data;
    public final static Creator<Linkedin> CREATOR = new Creator<Linkedin>() {


        @SuppressWarnings({
            "unchecked"
        })
        public Linkedin createFromParcel(Parcel in) {
            return new Linkedin(in);
        }

        public Linkedin[] newArray(int size) {
            return (new Linkedin[size]);
        }

    }
    ;

    protected Linkedin(Parcel in) {
        this.status = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.data = ((Object) in.readValue((Object.class.getClassLoader())));
    }

    public Linkedin() {
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(status);
        dest.writeValue(data);
    }

    public int describeContents() {
        return  0;
    }

}
