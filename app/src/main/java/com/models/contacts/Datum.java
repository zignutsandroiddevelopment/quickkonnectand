
package com.models.contacts;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Datum implements Serializable {

    @SerializedName("scan_user_id")
    @Expose
    private Integer scanUserId;
    @SerializedName("scan_user_name")
    @Expose
    private String scanUserName;
    @SerializedName("scan_user_device_id")
    @Expose
    private String scan_user_device_id;
    @SerializedName("scan_user_profile_url")
    @Expose
    private String scanUserProfileUrl;
    @SerializedName("scan_user_unique_code")
    @Expose
    private String scanUserUniqueCode;
    @SerializedName("scan_date")
    @Expose
    private String scanDate;
    @SerializedName("firstname")
    @Expose
    private String firstname;
    @SerializedName("lastname")
    @Expose
    private String lastname;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("scanned_by")
    @Expose
    private String scannedBy;

    @SerializedName("is_employer")
    @Expose
    private String is_employer;
    @SerializedName("is_personal")
    @Expose
    private String is_personal;


    @SerializedName("profile_id")
    @Expose
    private Object profileId;
    @SerializedName("profile_name")
    @Expose
    private Object profileName;

    public Integer getScanUserId() {
        return scanUserId;
    }

    public void setScanUserId(Integer scanUserId) {
        this.scanUserId = scanUserId;
    }

    public String getScanUserName() {
        return scanUserName;
    }

    public void setScanUserName(String scanUserName) {
        this.scanUserName = scanUserName;
    }

    public String getScanUserProfileUrl() {
        return scanUserProfileUrl;
    }

    public void setScanUserProfileUrl(String scanUserProfileUrl) {
        this.scanUserProfileUrl = scanUserProfileUrl;
    }

    public String getScanUserUniqueCode() {
        return scanUserUniqueCode;
    }

    public void setScanUserUniqueCode(String scanUserUniqueCode) {
        this.scanUserUniqueCode = scanUserUniqueCode;
    }

    public String getScanDate() {
        return scanDate;
    }

    public void setScanDate(String scanDate) {
        this.scanDate = scanDate;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getScannedBy() {
        return scannedBy;
    }

    public void setScannedBy(String scannedBy) {
        this.scannedBy = scannedBy;
    }

    public Object getProfileId() {
        return profileId;
    }

    public void setProfileId(Object profileId) {
        this.profileId = profileId;
    }

    public Object getProfileName() {
        return profileName;
    }

    public void setProfileName(Object profileName) {
        this.profileName = profileName;
    }

    public String getScan_user_device_id() {
        return scan_user_device_id;
    }

    public void setScan_user_device_id(String scan_user_device_id) {
        this.scan_user_device_id = scan_user_device_id;
    }

    public String getIs_employer() {
        return is_employer;
    }

    public void setIs_employer(String is_employer) {
        this.is_employer = is_employer;
    }

    public String getIs_personal() {
        return is_personal;
    }

    public void setIs_personal(String is_personal) {
        this.is_personal = is_personal;
    }
}
