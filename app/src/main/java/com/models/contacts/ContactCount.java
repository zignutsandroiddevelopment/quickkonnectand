
package com.models.contacts;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ContactCount {

    @SerializedName("contact_all")
    @Expose
    private Integer contactAll;
    @SerializedName("contact_week")
    @Expose
    private Integer contactWeek;
    @SerializedName("contact_month")
    @Expose
    private Integer contactMonth;

    public Integer getContactAll() {
        return contactAll;
    }

    public void setContactAll(Integer contactAll) {
        this.contactAll = contactAll;
    }

    public Integer getContactWeek() {
        return contactWeek;
    }

    public void setContactWeek(Integer contactWeek) {
        this.contactWeek = contactWeek;
    }

    public Integer getContactMonth() {
        return contactMonth;
    }

    public void setContactMonth(Integer contactMonth) {
        this.contactMonth = contactMonth;
    }

}
