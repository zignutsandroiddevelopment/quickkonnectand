
package com.models.contacts;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.models.otherusersprofiles.*;

public class ContactsPojo {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("msg")
    @Expose
    private String msg;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;
    @SerializedName("contact_count")
    @Expose
    private ContactCount contactCount;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public ContactCount getContactCount() {
        return contactCount;
    }

    public void setContactCount(ContactCount contactCount) {
        this.contactCount = contactCount;
    }

}
