package com.models.globalsearch;

import com.realmtable.Country;

import java.util.ArrayList;
import java.util.List;

public class Continent {

    private String name;
    private ArrayList<ModelGlobalSearch> countryList = new ArrayList<>();

    public Continent(String name, ArrayList<ModelGlobalSearch> countryList) {
        super();
        this.name = name;
        this.countryList = countryList;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<ModelGlobalSearch> getCountryList() {
        return countryList;
    }

    public void setCountryList(ArrayList<ModelGlobalSearch> countryList) {
        this.countryList = countryList;
    }
}