package com.models.globalsearch;

/**
 * Created by ZTLAB-10 on 04-05-2018.
 */

public class ModelGlobalSearch {

    private String username;
    private String companyname;
    private String key;
    private int getItemtype;
    private String id;
    private String profile_pic;
    private String unique_code;
    private String type;
    private String role;
    private String user_id;
    private String is_followed;
    private String profileId;
    private String scan_request;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getCompanyname() {
        return companyname;
    }

    public void setCompanyname(String companyname) {
        this.companyname = companyname;
    }

    public int getGetItemtype() {
        return getItemtype;
    }

    public void setGetItemtype(int getItemtype) {
        this.getItemtype = getItemtype;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProfile_pic() {
        return profile_pic;
    }

    public void setProfile_pic(String profile_pic) {
        this.profile_pic = profile_pic;
    }

    public String getUnique_code() {
        return unique_code;
    }

    public void setUnique_code(String unique_code) {
        this.unique_code = unique_code;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getIs_followed() {
        return is_followed;
    }

    public void setIs_followed(String is_followed) {
        this.is_followed = is_followed;
    }

    public String getProfileId() {
        return profileId;
    }

    public void setProfileId(String profileId) {
        this.profileId = profileId;
    }

    public String getScan_request() {
        return scan_request;
    }

    public void setScan_request(String scan_request) {
        this.scan_request = scan_request;
    }
}