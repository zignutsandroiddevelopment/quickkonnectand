package com.models.globalsearch;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Datum {

    @SerializedName("key")
    @Expose
    private String key;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("profile_pic")
    @Expose
    private Object profilePic;
    @SerializedName("unique_code")
    @Expose
    private String uniqueCode;
    @SerializedName("type")
    @Expose
    private Integer type;
    @SerializedName("role")
    @Expose
    private Integer role;
    @SerializedName("user_id")
    @Expose
    private Integer user_id;
    @SerializedName("is_followed")
    @Expose
    private Integer is_followed;
    @SerializedName("scan_request")
    @Expose
    private String scan_request;


    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Object getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(Object profilePic) {
        this.profilePic = profilePic;
    }

    public String getUniqueCode() {
        return uniqueCode;
    }

    public void setUniqueCode(String uniqueCode) {
        this.uniqueCode = uniqueCode;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getRole() {
        return role;
    }

    public void setRole(Integer role) {
        this.role = role;
    }

    public Integer getUser_id() {
        return user_id;
    }

    public void setUser_id(Integer user_id) {
        this.user_id = user_id;
    }

    public Integer getIs_followed() {
        return is_followed;
    }

    public void setIs_followed(Integer is_followed) {
        this.is_followed = is_followed;
    }

    public String getScan_request() {
        return scan_request;
    }

    public void setScan_request(String scan_request) {
        this.scan_request = scan_request;
    }
}