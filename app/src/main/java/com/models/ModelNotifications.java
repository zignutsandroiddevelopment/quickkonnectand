
package com.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ModelNotifications implements Parcelable {

    @SerializedName("notification_id")
    @Expose
    private Integer notificationId;
    @SerializedName("sender_id")
    @Expose
    private Integer senderId;
    @SerializedName("username")
    @Expose
    private String username;

    public String getNotification_type() {
        return notification_type;
    }

    public void setNotification_type(String notification_type) {
        this.notification_type = notification_type;
    }

    @SerializedName("notification_type")
    @Expose

    private String notification_type;
    @SerializedName("profile_pic")
    @Expose
    private String profile_pic;

    public String getProfile_pic() {
        return profile_pic;
    }

    public void setProfile_pic(String profile_pic) {
        this.profile_pic = profile_pic;
    }

    @SerializedName("user_email")
    @Expose
    private String userEmail;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("is_read")
    @Expose
    private Integer isRead;
    @SerializedName("created_time")
    @Expose
    private String createdTime;
    @SerializedName("unique_code")
    @Expose
    private String uniqueCode;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("profile_name")
    @Expose
    private String profile_name;
    @SerializedName("profile_id")
    @Expose
    private String profile_id;

    public final static Creator<ModelNotifications> CREATOR = new Creator<ModelNotifications>() {


        @SuppressWarnings({
                "unchecked"
        })
        public ModelNotifications createFromParcel(Parcel in) {
            return new ModelNotifications(in);
        }

        public ModelNotifications[] newArray(int size) {
            return (new ModelNotifications[size]);
        }

    };

    protected ModelNotifications(Parcel in) {
        this.notificationId = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.senderId = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.username = ((String) in.readValue((String.class.getClassLoader())));
        this.userEmail = ((String) in.readValue((String.class.getClassLoader())));
        this.status = ((String) in.readValue((String.class.getClassLoader())));
        this.isRead = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.createdTime = ((String) in.readValue((String.class.getClassLoader())));
        this.uniqueCode = ((String) in.readValue((String.class.getClassLoader())));
        this.message = ((String) in.readValue((String.class.getClassLoader())));
        this.profile_name = ((String) in.readValue((String.class.getClassLoader())));
        this.profile_id = ((String) in.readValue((String.class.getClassLoader())));
    }

    public ModelNotifications() {
    }

    public Integer getNotificationId() {
        return notificationId;
    }

    public void setNotificationId(Integer notificationId) {
        this.notificationId = notificationId;
    }

    public Integer getSenderId() {
        return senderId;
    }

    public String getProfile_id() {
        return profile_id;
    }

    public void setProfile_id(String profile_id) {
        this.profile_id = profile_id;
    }

    public void setSenderId(Integer senderId) {
        this.senderId = senderId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getIsRead() {
        return isRead;
    }

    public void setIsRead(Integer isRead) {
        this.isRead = isRead;
    }

    public String getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(String createdTime) {
        this.createdTime = createdTime;
    }

    public String getUniqueCode() {
        return uniqueCode;
    }

    public void setUniqueCode(String uniqueCode) {
        this.uniqueCode = uniqueCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getProfile_name() {
        return profile_name;
    }

    public void setProfile_name(String profile_name) {
        this.profile_name = profile_name;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(notificationId);
        dest.writeValue(senderId);
        dest.writeValue(username);
        dest.writeValue(userEmail);
        dest.writeValue(status);
        dest.writeValue(isRead);
        dest.writeValue(createdTime);
        dest.writeValue(uniqueCode);
        dest.writeValue(message);
        dest.writeValue(profile_name);
        dest.writeValue(profile_id);
    }

    public int describeContents() {
        return 0;
    }

}
