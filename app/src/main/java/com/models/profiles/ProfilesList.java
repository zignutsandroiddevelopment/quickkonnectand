
package com.models.profiles;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProfilesList implements Parcelable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("reason")
    @Expose
    private String resson;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("type")
    @Expose
    private Integer type;
    @SerializedName("logo")
    @Expose
    private Object logo;
    @SerializedName("unique_code")
    @Expose
    private String uniqueCode;
    @SerializedName("qk_tag_info")
    @Expose
    private QkTagInfo qkTagInfo;

    @SerializedName("is_transfer")
    @Expose
    private String is_transfer;
    @SerializedName("transfer_status")
    @Expose
    private String transfer_status;

    @SerializedName("role")
    @Expose
    private String role;

    @SerializedName("role_name")
    @Expose
    private String role_name;

    private String user_id;
    private String profile_id;
    private String company_profile;

    public String getIs_transfer() {
        return is_transfer;
    }

    public void setIs_transfer(String is_transfer) {
        this.is_transfer = is_transfer;
    }

    public String getTransfer_status() {
        return transfer_status;
    }

    public void setTransfer_status(String transfer_status) {
        this.transfer_status = transfer_status;
    }

    public final static Parcelable.Creator<ProfilesList> CREATOR = new Creator<ProfilesList>() {


        @SuppressWarnings({
                "unchecked"
        })
        public ProfilesList createFromParcel(Parcel in) {
            return new ProfilesList(in);
        }

        public ProfilesList[] newArray(int size) {
            return (new ProfilesList[size]);
        }

    };


    protected ProfilesList(Parcel in) {
        this.id = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.name = ((String) in.readValue((String.class.getClassLoader())));
        this.status = ((String) in.readValue((String.class.getClassLoader())));
        this.type = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.logo = ((Object) in.readValue((Object.class.getClassLoader())));
        this.uniqueCode = ((String) in.readValue((String.class.getClassLoader())));
        this.qkTagInfo = ((QkTagInfo) in.readValue((QkTagInfo.class.getClassLoader())));
        this.is_transfer = ((String) in.readValue((String.class.getClassLoader())));
        this.transfer_status = ((String) in.readValue((String.class.getClassLoader())));
        this.role = ((String) in.readValue((String.class.getClassLoader())));
        this.role_name = ((String) in.readValue((String.class.getClassLoader())));
        this.user_id = ((String) in.readValue((String.class.getClassLoader())));
        this.profile_id = ((String) in.readValue((String.class.getClassLoader())));
        this.company_profile = ((String) in.readValue((String.class.getClassLoader())));
    }

    public ProfilesList() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getRole_name() {
        return role_name;
    }

    public void setRole_name(String role_name) {
        this.role_name = role_name;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Object getLogo() {
        return logo;
    }

    public void setLogo(Object logo) {
        this.logo = logo;
    }

    public String getUniqueCode() {
        return uniqueCode;
    }

    public void setUniqueCode(String uniqueCode) {
        this.uniqueCode = uniqueCode;
    }

    public QkTagInfo getQkTagInfo() {
        return qkTagInfo;
    }

    public void setQkTagInfo(QkTagInfo qkTagInfo) {
        this.qkTagInfo = qkTagInfo;
    }

    public String getResson() {
        return resson;
    }

    public void setResson(String resson) {
        this.resson = resson;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getProfile_id() {
        return profile_id;
    }

    public void setProfile_id(String profile_id) {
        this.profile_id = profile_id;
    }

    public String getCompany_profile() {
        return company_profile;
    }

    public void setCompany_profile(String company_profile) {
        this.company_profile = company_profile;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(id);
        dest.writeValue(name);
        dest.writeValue(status);
        dest.writeValue(type);
        dest.writeValue(logo);
        dest.writeValue(uniqueCode);
        dest.writeValue(qkTagInfo);
        dest.writeValue(is_transfer);
        dest.writeValue(transfer_status);
        dest.writeValue(role);
        dest.writeValue(role_name);
        dest.writeValue(user_id);
        dest.writeValue(profile_id);
        dest.writeValue(company_profile);
    }

    public int describeContents() {
        return 0;
    }

}
