package com.models.profiles;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PlanDetail implements Parcelable {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("max_employee")
    @Expose
    private String maxEmployee;
    public final static Parcelable.Creator<PlanDetail> CREATOR = new Creator<PlanDetail>() {


        @SuppressWarnings({
                "unchecked"
        })
        public PlanDetail createFromParcel(Parcel in) {
            return new PlanDetail(in);
        }

        public PlanDetail[] newArray(int size) {
            return (new PlanDetail[size]);
        }

    };

    protected PlanDetail(Parcel in) {
        this.name = ((String) in.readValue((String.class.getClassLoader())));
        this.id = ((String) in.readValue((String.class.getClassLoader())));
        this.maxEmployee = ((String) in.readValue((Integer.class.getClassLoader())));
    }

    public PlanDetail() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMaxEmployee() {
        return maxEmployee;
    }

    public void setMaxEmployee(String maxEmployee) {
        this.maxEmployee = maxEmployee;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(name);
        dest.writeValue(id);
        dest.writeValue(maxEmployee);
    }

    public int describeContents() {
        return 0;
    }

}