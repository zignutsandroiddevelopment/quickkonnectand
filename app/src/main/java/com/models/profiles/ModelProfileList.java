
package com.models.profiles;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ModelProfileList implements Parcelable {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("msg")
    @Expose
    private String msg;
    @SerializedName("data")
    @Expose
    private List<ProfilesList> data = null;
    @SerializedName("plan_detail")
    @Expose
    private PlanDetail planDetail;
    @SerializedName("is_public")
    @Expose
    private boolean is_public;

    public final static Parcelable.Creator<ModelProfileList> CREATOR = new Creator<ModelProfileList>() {


        @SuppressWarnings({
                "unchecked"
        })
        public ModelProfileList createFromParcel(Parcel in) {
            return new ModelProfileList(in);
        }

        public ModelProfileList[] newArray(int size) {
            return (new ModelProfileList[size]);
        }

    };

    protected ModelProfileList(Parcel in) {
        this.status = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.msg = ((String) in.readValue((String.class.getClassLoader())));
        in.readList(this.data, (ProfilesList.class.getClassLoader()));
        this.planDetail = ((PlanDetail) in.readValue((PlanDetail.class.getClassLoader())));
        this.is_public = ((Boolean) in.readValue((Boolean.class.getClassLoader())));
    }

    public ModelProfileList() {
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<ProfilesList> getData() {
        return data;
    }

    public void setData(List<ProfilesList> data) {
        this.data = data;
    }

    public PlanDetail getPlanDetail() {
        return planDetail;
    }

    public void setPlanDetail(PlanDetail planDetail) {
        this.planDetail = planDetail;
    }

    public boolean isIs_public() {
        return is_public;
    }

    public void setIs_public(boolean is_public) {
        this.is_public = is_public;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(status);
        dest.writeValue(msg);
        dest.writeList(data);
        dest.writeValue(planDetail);
        dest.writeValue(is_public);
    }

    public int describeContents() {
        return 0;
    }
}
