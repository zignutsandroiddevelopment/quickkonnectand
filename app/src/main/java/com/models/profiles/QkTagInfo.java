package com.models.profiles;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class QkTagInfo implements Parcelable {

    @SerializedName("border_color")
    @Expose
    private Object borderColor;
    @SerializedName("pattern_color")
    @Expose
    private Object patternColor;
    @SerializedName("background_color")
    @Expose
    private Object backgroundColor;
    @SerializedName("middle_tag")
    @Expose
    private Object middleTag;
    @SerializedName("qr_code")
    @Expose
    private Object qrCode;

    @SerializedName("background_gradient")
    @Expose
    private Object background_gradient;

    public Object getBackground_gradient() {
        return background_gradient;
    }

    public void setBackground_gradient(Object background_gradient) {
        this.background_gradient = background_gradient;
    }

    public final static Parcelable.Creator<QkTagInfo> CREATOR = new Creator<QkTagInfo>() {


        @SuppressWarnings({
                "unchecked"
        })
        public QkTagInfo createFromParcel(Parcel in) {
            return new QkTagInfo(in);
        }

        public QkTagInfo[] newArray(int size) {
            return (new QkTagInfo[size]);
        }

    };

    protected QkTagInfo(Parcel in) {
        this.borderColor = ((Object) in.readValue((Object.class.getClassLoader())));
        this.patternColor = ((Object) in.readValue((Object.class.getClassLoader())));
        this.backgroundColor = ((Object) in.readValue((Object.class.getClassLoader())));
        this.middleTag = ((Object) in.readValue((Object.class.getClassLoader())));
        this.qrCode = ((Object) in.readValue((Object.class.getClassLoader())));
    }

    public QkTagInfo() {
    }

    public Object getBorderColor() {
        return borderColor;
    }

    public void setBorderColor(Object borderColor) {
        this.borderColor = borderColor;
    }

    public Object getPatternColor() {
        return patternColor;
    }

    public void setPatternColor(Object patternColor) {
        this.patternColor = patternColor;
    }

    public Object getBackgroundColor() {
        return backgroundColor;
    }

    public void setBackgroundColor(Object backgroundColor) {
        this.backgroundColor = backgroundColor;
    }

    public Object getMiddleTag() {
        return middleTag;
    }

    public void setMiddleTag(Object middleTag) {
        this.middleTag = middleTag;
    }

    public Object getQrCode() {
        return qrCode;
    }

    public void setQrCode(Object qrCode) {
        this.qrCode = qrCode;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(borderColor);
        dest.writeValue(patternColor);
        dest.writeValue(backgroundColor);
        dest.writeValue(middleTag);
        dest.writeValue(qrCode);
    }

    public int describeContents() {
        return 0;
    }

}