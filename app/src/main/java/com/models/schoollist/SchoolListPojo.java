
package com.models.schoollist;

import java.util.List;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SchoolListPojo implements Parcelable
{

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("msg")
    @Expose
    private String msg;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;
    public final static Creator<SchoolListPojo> CREATOR = new Creator<SchoolListPojo>() {


        @SuppressWarnings({
            "unchecked"
        })
        public SchoolListPojo createFromParcel(Parcel in) {
            return new SchoolListPojo(in);
        }

        public SchoolListPojo[] newArray(int size) {
            return (new SchoolListPojo[size]);
        }

    }
    ;

    protected SchoolListPojo(Parcel in) {
        this.status = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.msg = ((String) in.readValue((String.class.getClassLoader())));
        in.readList(this.data, (Datum.class.getClassLoader()));
    }

    public SchoolListPojo() {
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(status);
        dest.writeValue(msg);
        dest.writeList(data);
    }

    public int describeContents() {
        return  0;
    }

}
