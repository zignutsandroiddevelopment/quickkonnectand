
package com.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class EducationDetail implements Parcelable
{

    @SerializedName("education_id")
    @Expose
    private Integer educationId;
    @SerializedName("school_name")
    @Expose
    private String schoolName;
    @SerializedName("degree")
    @Expose
    private String degree;
    @SerializedName("field_of_study")
    @Expose
    private String fieldOfStudy;
    @SerializedName("start_date")
    @Expose
    private String startDate;
    @SerializedName("end_date")
    @Expose
    private String endDate;
    @SerializedName("activities")
    @Expose
    private String activities;
    public final static Parcelable.Creator<EducationDetail> CREATOR = new Creator<EducationDetail>() {


        @SuppressWarnings({
            "unchecked"
        })
        public EducationDetail createFromParcel(Parcel in) {
            return new EducationDetail(in);
        }

        public EducationDetail[] newArray(int size) {
            return (new EducationDetail[size]);
        }

    }
    ;

    protected EducationDetail(Parcel in) {
        this.educationId = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.schoolName = ((String) in.readValue((String.class.getClassLoader())));
        this.degree = ((String) in.readValue((String.class.getClassLoader())));
        this.fieldOfStudy = ((String) in.readValue((String.class.getClassLoader())));
        this.startDate = ((String) in.readValue((String.class.getClassLoader())));
        this.endDate = ((String) in.readValue((String.class.getClassLoader())));
        this.activities = ((String) in.readValue((String.class.getClassLoader())));
    }

    public EducationDetail() {
    }

    public Integer getEducationId() {
        return educationId;
    }

    public void setEducationId(Integer educationId) {
        this.educationId = educationId;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public String getDegree() {
        return degree;
    }

    public void setDegree(String degree) {
        this.degree = degree;
    }

    public String getFieldOfStudy() {
        return fieldOfStudy;
    }

    public void setFieldOfStudy(String fieldOfStudy) {
        this.fieldOfStudy = fieldOfStudy;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getActivities() {
        return activities;
    }

    public void setActivities(String activities) {
        this.activities = activities;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(educationId);
        dest.writeValue(schoolName);
        dest.writeValue(degree);
        dest.writeValue(fieldOfStudy);
        dest.writeValue(startDate);
        dest.writeValue(endDate);
        dest.writeValue(activities);
    }

    public int describeContents() {
        return  0;
    }

}
