
package com.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ContactDetail implements Parcelable {

    @SerializedName("contact_id")
    @Expose
    private String contactId;
    @SerializedName("contact_type")
    @Expose
    private String contactType;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("contactname")
    @Expose
    private String contactname;
    @SerializedName("is_approved")
    @Expose
    private Integer is_approved;
    @SerializedName("is_update")
    @Expose
    private Integer is_update;

    private Integer isContactExist = 0;
    private Integer currentPos;

    public final static Creator<ContactDetail> CREATOR = new Creator<ContactDetail>() {


        @SuppressWarnings({
                "unchecked"
        })
        public ContactDetail createFromParcel(Parcel in) {
            return new ContactDetail(in);
        }

        public ContactDetail[] newArray(int size) {
            return (new ContactDetail[size]);
        }

    };

    protected ContactDetail(Parcel in) {
        this.contactId = ((String) in.readValue((String.class.getClassLoader())));
        this.contactType = ((String) in.readValue((String.class.getClassLoader())));
        this.phone = ((String) in.readValue((String.class.getClassLoader())));
        this.email = ((String) in.readValue((String.class.getClassLoader())));
        this.contactname = ((String) in.readValue((String.class.getClassLoader())));
        this.is_approved = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.is_update = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.isContactExist = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.currentPos = ((Integer) in.readValue((Integer.class.getClassLoader())));
    }

    public ContactDetail() {
    }

    public String getContactId() {
        return contactId;
    }

    public void setContactId(String contactId) {
        this.contactId = contactId;
    }

    public String getContactType() {
        return contactType;
    }

    public void setContactType(String contactType) {
        this.contactType = contactType;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContactname() {
        return contactname;
    }

    public void setContactname(String contactname) {
        this.contactname = contactname;
    }

    public Integer getIs_approved() {
        return is_approved;
    }

    public void setIs_approved(Integer is_approved) {
        this.is_approved = is_approved;
    }

    public Integer getIs_update() {
        return is_update;
    }

    public void setIs_update(Integer is_update) {
        this.is_update = is_update;
    }

    public Integer getIsContactExist() {
        return isContactExist;
    }

    public void setIsContactExist(Integer isContactExist) {
        this.isContactExist = isContactExist;
    }

    public Integer getCurrentPos() {
        return currentPos;
    }

    public void setCurrentPos(Integer currentPos) {
        this.currentPos = currentPos;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(contactId);
        dest.writeValue(contactType);
        dest.writeValue(phone);
        dest.writeValue(email);
        dest.writeValue(contactname);
        dest.writeInt(is_approved);
        dest.writeInt(is_update);
        dest.writeInt(isContactExist);
        dest.writeInt(currentPos);
    }

    public int describeContents() {
        return 0;
    }

}
