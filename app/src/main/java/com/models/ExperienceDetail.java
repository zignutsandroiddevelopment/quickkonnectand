
package com.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ExperienceDetail implements Parcelable
{

    @SerializedName("experience_id")
    @Expose
    private Integer experienceId;
    @SerializedName("company_name")
    @Expose
    private String companyName;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("location")
    @Expose
    private String location;
    @SerializedName("start_date")
    @Expose
    private String startDate;
    @SerializedName("end_date")
    @Expose
    private String endDate;
    public final static Parcelable.Creator<ExperienceDetail> CREATOR = new Creator<ExperienceDetail>() {


        @SuppressWarnings({
            "unchecked"
        })
        public ExperienceDetail createFromParcel(Parcel in) {
            return new ExperienceDetail(in);
        }

        public ExperienceDetail[] newArray(int size) {
            return (new ExperienceDetail[size]);
        }

    }
    ;

    protected ExperienceDetail(Parcel in) {
        this.experienceId = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.companyName = ((String) in.readValue((String.class.getClassLoader())));
        this.title = ((String) in.readValue((String.class.getClassLoader())));
        this.location = ((String) in.readValue((String.class.getClassLoader())));
        this.startDate = ((String) in.readValue((String.class.getClassLoader())));
        this.endDate = ((String) in.readValue((String.class.getClassLoader())));
    }

    public ExperienceDetail() {
    }

    public Integer getExperienceId() {
        return experienceId;
    }

    public void setExperienceId(Integer experienceId) {
        this.experienceId = experienceId;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(experienceId);
        dest.writeValue(companyName);
        dest.writeValue(title);
        dest.writeValue(location);
        dest.writeValue(startDate);
        dest.writeValue(endDate);
    }

    public int describeContents() {
        return  0;
    }

}
