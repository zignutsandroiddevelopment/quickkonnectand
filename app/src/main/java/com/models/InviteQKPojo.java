
package com.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class InviteQKPojo implements Parcelable
{

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("msg")
    @Expose
    private String msg;
    public final static Creator<InviteQKPojo> CREATOR = new Creator<InviteQKPojo>() {


        @SuppressWarnings({
            "unchecked"
        })
        public InviteQKPojo createFromParcel(Parcel in) {
            return new InviteQKPojo(in);
        }

        public InviteQKPojo[] newArray(int size) {
            return (new InviteQKPojo[size]);
        }

    }
    ;

    protected InviteQKPojo(Parcel in) {
        this.status = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.msg = ((String) in.readValue((String.class.getClassLoader())));
    }

    public InviteQKPojo() {
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(status);
        dest.writeValue(msg);
    }

    public int describeContents() {
        return  0;
    }

}
