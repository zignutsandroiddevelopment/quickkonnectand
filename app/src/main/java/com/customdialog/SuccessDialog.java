package com.customdialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cunoraz.gifview.library.GifView;
import com.interfaces.OnSuccessDialog;
import com.quickkonnect.R;

public class SuccessDialog {

    private Context context;
    private Dialog dialog;
    private OnSuccessDialog onSuccessDialog;

    public SuccessDialog(Context context, OnSuccessDialog onSuccessDialog) {
        this.context = context;
        this.onSuccessDialog = onSuccessDialog;
    }

    public void showSuccessDialog(String message) {

        dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View view = LinearLayout.inflate(context, R.layout.layout_sucess_dialog_employee, null);
        dialog.setContentView(view);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        dialog.show();
        dialog.getWindow().setAttributes(lp);

        TextView tvmessage = dialog.findViewById(R.id.tvmessage);

        final GifView gifView1 = (GifView) view.findViewById(R.id.image_suc_gif);
        gifView1.setVisibility(View.VISIBLE);
        gifView1.play();
        gifView1.setGifResource(R.drawable.done);
        gifView1.getGifResource();
        try {
            Handler h = new Handler();
            h.postDelayed(new Runnable() {
                @Override
                public void run() {
                    gifView1.pause();
                }
            }, 2000);
        } catch (Exception e) {
            e.printStackTrace();
        }

        tvmessage.setText(message + "");
        TextView textView = dialog.findViewById(R.id.tv_ok);
        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                }
                onSuccessDialog.onSuccess();
            }
        });
    }
}
