package com.customdialog;

import android.app.Activity;
import android.content.Context;
import android.support.design.widget.BottomSheetDialog;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.interfaces.OnImagePick;
import com.quickkonnect.R;

public class DialogUploadProfilePic {

    private Context context;
    private BottomSheetDialog bottomSheetDialog;
    private OnImagePick onImagePick;

    public DialogUploadProfilePic(Context context, OnImagePick onImagePick) {
        this.context = context;
        this.onImagePick = onImagePick;
    }

    public void showImagePickDialog() {

        bottomSheetDialog = new BottomSheetDialog(context);

        View view1 = ((Activity) context).getLayoutInflater().inflate(R.layout.custom_bottom_sheet_profile_pic, null);

        bottomSheetDialog.setContentView(view1);

        Button btnCancel = view1.findViewById(R.id.buttonSheet1);

        TextView tvViewPhoto = view1.findViewById(R.id.TextViewSheet1);
        TextView tvUploadphoto = view1.findViewById(R.id.TextViewSheet2);
        TextView tvTakephoto = view1.findViewById(R.id.TextViewSheet3);
        TextView tvRemovePhoto = view1.findViewById(R.id.TextViewSheet4);

        View divider_viewphoto = view1.findViewById(R.id.divider_viewphoto);

        tvViewPhoto.setVisibility(View.GONE);
        divider_viewphoto.setVisibility(View.GONE);

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialog.dismiss();
            }
        });

        tvUploadphoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialog.dismiss();
                onImagePick.onUploadPhoto();
            }
        });

        tvTakephoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialog.dismiss();
                onImagePick.onTakePhoto();
            }
        });

        tvRemovePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialog.dismiss();
                onImagePick.removePhoto();
            }
        });

        bottomSheetDialog.show();
    }
}
