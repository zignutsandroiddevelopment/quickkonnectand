package com.Chat;

import com.google.firebase.database.Exclude;

import java.util.Date;

/**
 * Created by Marcel on 11/7/2015.
 */
public class ChatMessage {

    private String from_id;
    String from_name;
    private String read1;
    private String text;
    public long timeStamp;
    private String to_id;
    String to_name;

    String from_token;
    String from_user_id;
    String to_token;
    String to_user_id;

    private int mRecipientOrSenderStatus;

    private boolean isSelected = false;


    public ChatMessage() {
    }

    public ChatMessage(String text, String from_id, String from_name, String read1, String to_id, String to_name, String from_token, String from_user_id, String to_token, String to_user_id) {

        this.text = text;
        this.to_id = to_id;
        this.from_id = from_id;
        this.from_name = from_name;
        this.to_name = to_name;
        this.read1 = read1;
        this.from_token = from_token;
        this.from_user_id = from_user_id;
        this.to_token = to_token;
        this.to_user_id = to_user_id;

        timeStamp = new Date().getTime() / 1000;

    }

    /*public ChatMessage(String from_id, String from_name, String read1, String text, long timeStamp, String to_id, String to_name,, boolean isSelected) {
        this.from_id = from_id;
        this.from_name = from_name;
        this.read1 = read1;
        this.text = text;
        this.timeStamp = timeStamp;
        this.to_id = to_id;
        this.to_name = to_name;
        this.isSelected = isSelected;
    }*/

    public String getFrom_token() {
        return from_token;
    }

    public void setFrom_token(String from_token) {
        this.from_token = from_token;
    }

    public String getFrom_user_id() {
        return from_user_id;
    }

    public void setFrom_user_id(String from_user_id) {
        this.from_user_id = from_user_id;
    }

    public String getTo_token() {
        return to_token;
    }

    public void setTo_token(String to_token) {
        this.to_token = to_token;
    }

    public String getTo_user_id() {
        return to_user_id;
    }

    public void setTo_user_id(String to_user_id) {
        this.to_user_id = to_user_id;
    }

    public int getmRecipientOrSenderStatus() {
        return mRecipientOrSenderStatus;
    }

    public void setmRecipientOrSenderStatus(int mRecipientOrSenderStatus) {
        this.mRecipientOrSenderStatus = mRecipientOrSenderStatus;
    }

    @Exclude
    public int getRecipientOrSenderStatus() {
        return mRecipientOrSenderStatus;
    }

    public void setRecipientOrSenderStatus(int recipientOrSenderStatus) {
        this.mRecipientOrSenderStatus = recipientOrSenderStatus;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getFrom_id() {
        return from_id;
    }

    public void setFrom_id(String from_id) {
        this.from_id = from_id;
    }

    public String getTo_id() {
        return to_id;
    }

    public void setTo_id(String to_id) {
        this.to_id = to_id;
    }

    public long getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(long timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getFrom_name() {
        return from_name;
    }

    public void setFrom_name(String from_name) {
        this.from_name = from_name;
    }

    public String getTo_name() {
        return to_name;
    }

    public void setTo_name(String to_name) {
        this.to_name = to_name;
    }

    public String getRead1() {
        return read1;
    }

    public void setRead1(String read1) {
        this.read1 = read1;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
