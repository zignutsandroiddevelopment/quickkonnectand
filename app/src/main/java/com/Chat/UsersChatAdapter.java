package com.Chat;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.OpenFire.entry.ContactEntry;
import com.interfaces.getUserOnClick;
import com.quickkonnect.CircleTransform;
import com.quickkonnect.R;
import com.squareup.picasso.Picasso;
import com.swiperefershlayout.SwipeRevealLayout;
import com.swiperefershlayout.ViewBinderHelper;
import com.utilities.Constants;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Zignuts Technolab PVT. LTD on 03-03-2018.
 * Edited by Navin Panchal 03/11/2018 - Date format issue resolved, Code formatting
 */

public class UsersChatAdapter extends RecyclerView.Adapter<UsersChatAdapter.ViewHolderUsers> {

    private ArrayList<ContactEntry> userContactEntries;
    private Context mContext;
    private getUserOnClick getUserOnClick;
    private final ViewBinderHelper binderHelper = new ViewBinderHelper();

    public UsersChatAdapter(Context context, ArrayList<ContactEntry> userContactEntries, getUserOnClick getUserOnClick) {
        this.userContactEntries = userContactEntries;
        this.getUserOnClick = getUserOnClick;
        mContext = context;
    }

    @NonNull
    @Override
    public ViewHolderUsers onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolderUsers(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_chatnew, parent, false), getUserOnClick);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderUsers holder, int position) {

        ContactEntry contactEntry = userContactEntries.get(position);

        binderHelper.bind(holder.swipeRevealLayout, String.valueOf(position));
        binderHelper.setOpenOnlyOne(true);
        if (holder.swipeRevealLayout.isOpened())
            binderHelper.closeLayout(String.valueOf(position));

        holder.mUserDisplayName.setText(contactEntry.getUsername() + "");

        if (contactEntry.getLast_message() != null && !contactEntry.getLast_message().equalsIgnoreCase("") && !contactEntry.getLast_message().equalsIgnoreCase("null")) {
            holder.tvLastMessage.setText(contactEntry.getLast_message() + "");

        } else holder.tvLastMessage.setText("");

        if (contactEntry.getProfile_pic() != null && !contactEntry.getProfile_pic().isEmpty() && !contactEntry.getProfile_pic().equals("null")) {
            Picasso.with(mContext)
                    .load(contactEntry.getProfile_pic() + "")
                    .error(R.drawable.ic_default_profile_photo)
                    .placeholder(R.drawable.ic_default_profile_photo)
                    .transform(new CircleTransform())
                    .fit()
                    .into(holder.UserProfile);

        } else holder.UserProfile.setImageResource(R.drawable.ic_default_profile_photo);

        if (contactEntry.getUnread_count() != null && !contactEntry.getUnread_count().equalsIgnoreCase("0") && !contactEntry.getUnread_count().equalsIgnoreCase("null") && !contactEntry.getUnread_count().equalsIgnoreCase("")) {
            holder.tvUnreadMessageCounter.setVisibility(View.VISIBLE);
            holder.tvUnreadMessageCounter.setText(contactEntry.getUnread_count());

        } else holder.tvUnreadMessageCounter.setVisibility(View.GONE);

        if (contactEntry.getDate() != null) {
            try {
                long date = Long.parseLong(contactEntry.getDate());
                String formattedDate = formatToYesterdayOrToday(new Date(date));
                holder.time.setText(formattedDate);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    @Override
    public int getItemCount() {
        if (userContactEntries != null && userContactEntries.size() > 0)
            return userContactEntries.size();
        else
            return 0;
    }

    public void clear() {
        userContactEntries.clear();
    }

    public void refillData(ArrayList<ContactEntry> userContactEntries) {
        this.userContactEntries = userContactEntries;
        notifyDataSetChanged();
    }

    public class ViewHolderUsers extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ImageView UserProfile;
        private TextView mUserDisplayName, tvLastMessage, tvUnreadMessageCounter, time;
        private LinearLayout linearRoot;
        private getUserOnClick getUserOnClick;
        private FrameLayout frame_delete;
        private SwipeRevealLayout swipeRevealLayout;

        public ViewHolderUsers(View itemView, getUserOnClick getUserOnClick) {
            super(itemView);
            this.getUserOnClick = getUserOnClick;
            UserProfile = itemView.findViewById(R.id.img_user_profile);
            mUserDisplayName = itemView.findViewById(R.id.tvUsername);
            tvUnreadMessageCounter = itemView.findViewById(R.id.tv_recent_msg_count);
            tvLastMessage = itemView.findViewById(R.id.tvMessage);
            time = itemView.findViewById(R.id.tvTime);
            linearRoot = itemView.findViewById(R.id.linearRoot);
            frame_delete = itemView.findViewById(R.id.delete_layout);
            swipeRevealLayout = itemView.findViewById(R.id.swipe_chat_con);

            swipeRevealLayout.close(true);

            UserProfile.setOnClickListener(this);
            linearRoot.setOnClickListener(this);
            frame_delete.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (view == linearRoot) {
                getUserOnClick.getUserclick(Constants.TYPE_CLICK_ON_OTHER, getAdapterPosition());
            } else if (view == frame_delete) {
                getUserOnClick.getUserclick(Constants.TYPE_CHAT_DELETE, getAdapterPosition());
            }
        }
    }

    public static String formatToYesterdayOrToday(Date dateTime) throws ParseException {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(dateTime);
        Calendar today = Calendar.getInstance();
        Calendar yesterday = Calendar.getInstance();
        yesterday.add(Calendar.DATE, -1);
        DateFormat timeFormatter = new SimpleDateFormat("hh:mma");
        if (calendar.get(Calendar.YEAR) == today.get(Calendar.YEAR) && calendar.get(Calendar.DAY_OF_YEAR) == today.get(Calendar.DAY_OF_YEAR)) {
            return "Today " + timeFormatter.format(dateTime);
        } else if (calendar.get(Calendar.YEAR) == yesterday.get(Calendar.YEAR) && calendar.get(Calendar.DAY_OF_YEAR) == yesterday.get(Calendar.DAY_OF_YEAR)) {
            return "Yesterday " + timeFormatter.format(dateTime);
        } else {
            String formatedDate = new SimpleDateFormat("dd/MM/yyyy").format(dateTime);
            return formatedDate;
        }
    }
}