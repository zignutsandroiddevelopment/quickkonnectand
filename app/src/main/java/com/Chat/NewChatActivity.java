package com.Chat;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.OpenFire.XmppConnectionUtil;
import com.OpenFire.entry.ChatEntry;
import com.OpenFire.entry.RoasterLogic;
import com.OpenFire.entry.XMPPLogic;
import com.OpenFire.interfaces.PresenceChangesListener;
import com.OpenFire.interfaces.ReconnectListener;
import com.OpenFire.network.PingPacketFilter;
import com.OpenFire.network.PingPacketListener;
import com.OpenFire.network.PingProvider;
import com.activities.MatchProfileActivity;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.models.ChatConcersationModel;
import com.models.ChatConversationItems;
import com.quickkonnect.CircleTransform;
import com.quickkonnect.R;
import com.realmtable.ChatConversations;
import com.realmtable.ChatUserList;
import com.realmtable.RealmController;
import com.squareup.picasso.Picasso;
import com.utilities.Constants;
import com.utilities.QKPreferences;
import com.utilities.Utilities;
import com.volleyapirequest.VolleyApiRequest;
import com.volleyapirequest.VolleyCallBack;

import org.jivesoftware.smack.Chat;
import org.jivesoftware.smack.ChatManager;
import org.jivesoftware.smack.ChatManagerListener;
import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.MessageListener;
import org.jivesoftware.smack.Roster;
import org.jivesoftware.smack.SASLAuthentication;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smack.provider.ProviderManager;
import org.jivesoftware.smackx.GroupChatInvitation;
import org.jivesoftware.smackx.PrivateDataManager;
import org.jivesoftware.smackx.ServiceDiscoveryManager;
import org.jivesoftware.smackx.filetransfer.FileTransferNegotiator;
import org.jivesoftware.smackx.packet.ChatStateExtension;
import org.jivesoftware.smackx.packet.LastActivity;
import org.jivesoftware.smackx.packet.OfflineMessageInfo;
import org.jivesoftware.smackx.packet.OfflineMessageRequest;
import org.jivesoftware.smackx.packet.SharedGroupsInfo;
import org.jivesoftware.smackx.provider.BytestreamsProvider;
import org.jivesoftware.smackx.provider.DataFormProvider;
import org.jivesoftware.smackx.provider.DelayInformationProvider;
import org.jivesoftware.smackx.provider.DiscoverInfoProvider;
import org.jivesoftware.smackx.provider.DiscoverItemsProvider;
import org.jivesoftware.smackx.provider.IBBProviders;
import org.jivesoftware.smackx.provider.MUCAdminProvider;
import org.jivesoftware.smackx.provider.MUCOwnerProvider;
import org.jivesoftware.smackx.provider.MUCUserProvider;
import org.jivesoftware.smackx.provider.MessageEventProvider;
import org.jivesoftware.smackx.provider.MultipleAddressesProvider;
import org.jivesoftware.smackx.provider.RosterExchangeProvider;
import org.jivesoftware.smackx.provider.StreamInitiationProvider;
import org.jivesoftware.smackx.provider.VCardProvider;
import org.jivesoftware.smackx.provider.XHTMLExtensionProvider;
import org.jivesoftware.smackx.search.UserSearch;
import org.json.JSONObject;

import io.realm.RealmResults;

public class NewChatActivity extends Activity implements PresenceChangesListener {

    ProgressDialog progressDialog;
    private static String LOG_TAG = "- Chat Activity -";
    private Context context;
    private RecyclerView chatRecycler;
    private ImageView sendButton, back, toolbar_profile;
    private EditText chatMessageEditText;
    private TextView toolbarName, toolbar_tv_typing;
    private MessageChatAdapter chatAdapter;
    private QKPreferences qkPreferences;
    private String fJID, fNickName, UniqueId = "", UserUniqueId = "", ConversationID = "";
    private String mJID, mNickName;
    private String fAvatarByte;
    private boolean is_RosterCreated = false;
    int index = 0;
    RealmResults<ChatConversations> conversationlist;

    private XMPPConnection xmppConnection;
    XmppConnectionUtil xmppConnectionUtil;

    private ChatManager chatManager;
    private Chat chat;
    Handler chatHandler;

    LinearLayout toolbar, ll_back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.layout_chat);
        Utilities.changeStatusbar(getApplicationContext(), getWindow());
        qkPreferences = new QKPreferences(NewChatActivity.this);

        xmppConnectionUtil = new XmppConnectionUtil(getApplicationContext());
        UserUniqueId = qkPreferences.getValues(QKPreferences.USER_UNIQUECODE) + "";

        progressDialog = new ProgressDialog(NewChatActivity.this);

        initView();
        XmppConnection();
    }

    private void XmppConnection() {
        try {
            if (progressDialog != null && !progressDialog.isShowing()) {
                progressDialog.setMessage("Loading...");
                progressDialog.setCancelable(true);
                progressDialog.show();
            }
            NotificationManager notificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.cancelAll();
        } catch (Exception e) {
            e.printStackTrace();
        }
        xmppConnection = XMPPLogic.getInstance().getConnection();
        if (xmppConnection != null) {
            if (!xmppConnection.isConnected()) {
                ConnectToXmpp();
            } else if (!xmppConnection.isAuthenticated()) {
                LoginToXmpp();
            } else {
                if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();

                xmppConnectionUtil.setPresenceChangesListener(NewChatActivity.this);
                //xmppConnection = XMPPLogic.getInstance().getConnection();
                FileTransferNegotiator.IBB_ONLY = true;
                ServiceDiscoveryManager sdm = new ServiceDiscoveryManager(xmppConnection);
                sdm.addFeature("http://jabber.org/protocol/disco#info");
                sdm.addFeature("http://jabber.org/protocol/disco#item");
                sdm.addFeature("jabber:iq:privacy");

                // Create Chat Manager
                chatManager = xmppConnection.getChatManager();

                try {
                    Roster roster = xmppConnection.getRoster();
                    // jid: String, user: String, groups: String[]
                    String Name = getIntent().getExtras().getString("fNickName") + "";
                    roster.createEntry(fJID, Name + "", null);
                    roster.setSubscriptionMode(Roster.SubscriptionMode.accept_all);
                    Presence subscribed = new Presence(Presence.Type.subscribed);
                    subscribed.setTo(fJID + "");
                    xmppConnection.sendPacket(subscribed);
                    is_RosterCreated = true;

                    Presence availability = roster.getPresence(UniqueId + "@quickkonnect.com");
                    retrieveState_mode(availability.getMode(), availability.isAvailable());

                } catch (XMPPException e) {
                    e.printStackTrace();
                    is_RosterCreated = false;
                }

                // Add Chat Listener
                chatManager.addChatListener(new ChatManagerListener() {
                    public void chatCreated(Chat chat, boolean isCreated) {
                        Log.i(LOG_TAG, "Chat was created.");
                    }
                });

                // Create chat channel with friend
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        chatHandler = new Handler() {
                            @Override
                            public void handleMessage(Message msg) {
                                switch (msg.what) {
                                    case 1:
                                        org.jivesoftware.smack.packet.Message chatMessage;
                                        chatMessage = (org.jivesoftware.smack.packet.Message) msg.obj;

                                        StoreSingleMessage(chatMessage.getBody() + "", mJID, fJID);
                                        break;
                                }
                            }
                        };
                    }
                });

                chat = chatManager.createChat(fJID, new MessageListener() {
                    public void processMessage(Chat chat, final org.jivesoftware.smack.packet.Message chatMessage) {
                        if (chatMessage.getBody() != null) {
                            new Thread() {
                                public void run() {
                                    Message msg = new Message();
                                    msg.what = 1;
                                    msg.obj = chatMessage;
                                    chatHandler.sendMessage(msg);
                                }
                            }.start();
                            Log.i(LOG_TAG, "Received message from: " + chatMessage.getFrom() + ": " + chatMessage.getBody());
                        }
                    }
                });
            }
        } else {
            ConnectToXmpp();
        }
    }

    private void LoginToXmpp() {
        try {
            @SuppressLint("StaticFieldLeak")
            AsyncTask<Void, Void, Boolean> loginThread = new AsyncTask<Void, Void, Boolean>() {
                @Override
                protected synchronized Boolean doInBackground(Void... arg0) {
                    try {
                        if (xmppConnection.isConnected()) {
                            if (UserUniqueId != null && !UserUniqueId.equalsIgnoreCase("") && !UserUniqueId.equalsIgnoreCase("null")) {
                                xmppConnection.login(UserUniqueId + "", XmppConnectionUtil.USER_PASSWORD + "", XmppConnectionUtil.USER_RESOURCE + "");
                                Log.i(LOG_TAG, "Logged in to connect server.");
                                if (xmppConnection.isConnected()) {
                                    xmppConnection.addPacketListener(new PingPacketListener(xmppConnection), new PingPacketFilter());
                                }
                                // Send package to server to my status to "Available" (Online)
                                Presence userPresence = new Presence(Presence.Type.available);
                                xmppConnection.sendPacket(userPresence);
                                XmppConnection();
                            } else {
                                XmppConnection();
                            }
                        } else {
                            XmppConnection();
                        }
                    } catch (Exception e) {
                        XmppConnection();
                        Log.e(LOG_TAG, "Unable to connect server: " + e.getMessage());
                    }
                    return null;
                }
            };
            loginThread.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void ConnectToXmpp() {
        try {
            @SuppressLint("StaticFieldLeak")
            AsyncTask<Void, Void, Boolean> connectionThread = new AsyncTask<Void, Void, Boolean>() {
                @Override
                protected synchronized Boolean doInBackground(Void... arg0) {
                    try {
                        // Setup required features
                        configure(ProviderManager.getInstance());
                        // Setup connection configuration
                        ConnectionConfiguration config = new ConnectionConfiguration(XmppConnectionUtil.SERVER_IP, XmppConnectionUtil.SERVER_PORT);
                        config.setSASLAuthenticationEnabled(true);
                        config.setSendPresence(false);
                        SASLAuthentication.supportSASLMechanism("PLAIN", 0);

                        // Setup XMPP connection from configuration
                        xmppConnection = new XMPPConnection(config);
                        xmppConnection.connect();
                        Log.i(LOG_TAG, "Connected to server.");
                        XMPPLogic.getInstance().setConnection(xmppConnection);
                        XmppConnection();
                    } catch (Exception e) {
                        XmppConnection();
                        Log.e(LOG_TAG, "Unable to connect server: " + e.getMessage());
                    }
                    return null;
                }
            };
            connectionThread.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initView() {

        toolbar = findViewById(R.id.tool_bar);
        back = toolbar.findViewById(R.id.back);
        toolbar_profile = toolbar.findViewById(R.id.img_chat_profile_image);
        toolbarName = toolbar.findViewById(R.id.toolbarName);
        toolbar_tv_typing = toolbar.findViewById(R.id.toolbar_tv_typing);
        ll_back = toolbar.findViewById(R.id.ll_back);
        ll_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    setUnreadCount();
                    Utilities.hideKeyboard(NewChatActivity.this);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                setResult(RESULT_OK);
                NewChatActivity.this.finish();
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    setUnreadCount();
                    Utilities.hideKeyboard(NewChatActivity.this);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                setResult(RESULT_OK);
                NewChatActivity.this.finish();
            }
        });

        // View matching
        chatRecycler = findViewById(R.id.chat_message_listview);
        sendButton = findViewById(R.id.chat_send_button);
        chatMessageEditText = findViewById(R.id.chat_message_edit_text);

        // Event for send message button
        sendButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                SendMessage();
            }
        });

        // Extras
        Bundle bundle = getIntent().getExtras();
        if (bundle == null) {
            NewChatActivity.this.finish();
        } else {
            // Get active connection
            fJID = bundle.getString("fJID");
            try {
                String fjsplit[] = fJID.split("@");
                if (fjsplit[0] != null) {
                    UniqueId = fjsplit[0];
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            fNickName = bundle.getString("fNickName");
            fAvatarByte = bundle.getString("fAvatarByte");
            mJID = UserUniqueId + "@quickkonnect.com";

            setprofilepicAndName();

        }
        getConversation(true, false);
        setUnreadCount();
    }

    private void setprofilepicAndName() {
        try {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (fAvatarByte != null && !fAvatarByte.equalsIgnoreCase("") && !fAvatarByte.equalsIgnoreCase("null")) {

                        Picasso.with(NewChatActivity.this)
                                .load(fAvatarByte + "")
                                .error(R.drawable.ic_default_profile_photo)
                                .transform(new CircleTransform())
                                .placeholder(R.drawable.ic_default_profile_photo)
                                .fit()
                                .into(toolbar_profile);
                    } else {
                        toolbar_profile.setImageResource(R.drawable.ic_default_profile_photo);
                    }

                    toolbar_profile.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (UniqueId != null && !UniqueId.equalsIgnoreCase("")) {
                                Intent i = new Intent(context, MatchProfileActivity.class);
                                i.putExtra("unique_code", UniqueId + "");
                                i.putExtra(Constants.KEY, Constants.KEY_OTHER);
                                startActivity(i);
                            }
                        }
                    });

                    if (fNickName != null) {
                        toolbarName.setText(fNickName + "");
                    }
                    //Initialize
                    LinearLayoutManager mLayoutManager = new LinearLayoutManager(NewChatActivity.this);
                    //mLayoutManager.setReverseLayout(true);
                    mLayoutManager.setStackFromEnd(true);
                    chatRecycler.setLayoutManager(mLayoutManager);
                    chatRecycler.setHasFixedSize(true);
                    //ScrolltoLast();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getConversation(boolean isRefresh, boolean isScroll) {

        boolean isShowprogressbar = true;
        conversationlist = RealmController.with(NewChatActivity.this).getChatConversationbyId(UserUniqueId + "|" + UniqueId);

        if (conversationlist != null && conversationlist.size() > 0) {

            try {
                ConversationID = conversationlist.get(0).getConversationID();
                index = conversationlist.size();
            } catch (Exception e) {
                e.printStackTrace();
            }
            isShowprogressbar = false;
            setAdapter(conversationlist);
            if (isScroll)
                ScrolltoLast();
        }
        setAdapter(conversationlist);
        if (isRefresh)
            getConversationfromApi(isShowprogressbar);
    }

    private void setAdapter(RealmResults<ChatConversations> conversationlist) {
        chatAdapter = new MessageChatAdapter(context, conversationlist, mJID);
        chatRecycler.setAdapter(chatAdapter);
    }

    private void setUnreadCount() {
        try {
            ChatUserList contactEntry = new ChatUserList();
            contactEntry.setJID(UniqueId + "@quickkonnect.com");
            contactEntry.setUnread_count("0");
            RealmController.with(NewChatActivity.this).updateChatUser(contactEntry);
            context = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void refillAdapter() {
        conversationlist = RealmController.with(NewChatActivity.this).getChatConversationbyId(UserUniqueId + "|" + UniqueId);
        chatAdapter.refillAdapter(conversationlist);
    }

    private void StroreToRealm(final ChatEntry chatEntry) {
       /* try {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    String mid = "";
                    int pos = 0;
                    try {
                        if (chatEntries != null && chatEntries.size() > 0) {
                            pos = chatEntries.size() - 1;
                            //int mimid = Integer.parseInt(chatEntries.get(pos).getId()) + 1;
                            mid = pos + "";
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (mid != null && !mid.equalsIgnoreCase("") & ConversationID != null && !ConversationID.equalsIgnoreCase("")) {
                        ChatConversations chatConversations = new ChatConversations();
                        chatConversations.setMessageID(UniqueId + UserUniqueId + ConversationID + mid);
                        chatConversations.setChetReference(UserUniqueId + "|" + UniqueId);
                        chatConversations.setMsgchtid(mid);
                        chatConversations.setSentDate(String.valueOf(chatEntry.getWhen()));
                        chatConversations.setBody(chatEntry.getMessage());
                        chatConversations.setToJID(chatEntry.getReceiverJID());
                        chatConversations.setFromJID(chatEntry.getSenderJID());
                        chatConversations.setConversationID(ConversationID);

                        *//*ChatEntry chatEntry1 = new ChatEntry();
                        chatEntry1.setId(mid);
                        chatEntry1.setSenderJID(chatEntry.getSenderJID());
                        chatEntry1.setReceiverJID(chatEntry.getReceiverJID());
                        chatEntry1.setWhen(chatEntry.getWhen());
                        chatEntry1.setMessage(chatEntry.getMessage());*//*

                        RealmController.with(ChatConversation.this).updateChatConversation(chatConversations);
                        //chatEntries.set(pos, chatEntry1);
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }*/
    }

    private void getonlineOfflinPreferences() {
        Roster roster = RoasterLogic.getInstance().getRoster();
        Presence availability = roster.getPresence(UniqueId + "@quickkonnect.com");
        retrieveState_mode(availability.getMode(), availability.isAvailable());
    }

    public void retrieveState_mode(final Presence.Mode userMode, final boolean isOnline) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                /* 0 for offline, 1 for online, 2 for away,3 for busy*/
                if (userMode == Presence.Mode.dnd) {
                    updateStatusOfUser("Offline");
                } else if (userMode == Presence.Mode.away || userMode == Presence.Mode.xa) {
                    updateStatusOfUser("Offline");
                } else if (isOnline) {
                    updateStatusOfUser("Online");
                } else {
                    updateStatusOfUser("Offline");
                }

            }
        });
    }

    private void updateStatusOfUser(final String s) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (s != null && !s.equalsIgnoreCase("") && !s.equalsIgnoreCase("null")) {
                    toolbar_tv_typing.setText(s + "");
                    toolbar_tv_typing.setVisibility(View.VISIBLE);
                    if (s.equalsIgnoreCase("Offline")) {
                        toolbar_tv_typing.setVisibility(View.GONE);
                    }
                } else {
                    toolbar_tv_typing.setVisibility(View.GONE);
                }
            }
        });

    }

    private void ScrolltoLast() {
        try {
            if (chatRecycler != null && chatAdapter != null && chatAdapter.getItemCount() > 2) {
                //chatRecycler.smoothScrollToPosition(chatAdapter.getItemCount() - 1);
                chatRecycler.smoothScrollToPosition(chatAdapter.getItemCount() - 1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void SendMessage() {
        if (Utilities.isNetworkConnected(NewChatActivity.this)) {
            String message = chatMessageEditText.getText().toString().trim();
            if (message.trim().length() != 0) {
                try {
                    chatMessageEditText.setText(null);
                    StoreSingleMessage(message + "", fJID, mJID);
                    chat.sendMessage(message);
                } catch (Exception e) {
                    Log.e(LOG_TAG, "Unable to send message: " + e.getMessage());
                   /* try {
                        // Already started in Splash therefor have to stop
                        stopService(new Intent(getApplicationContext(), BackgroundService.class));
                        // start again for connection
                        startService(new Intent(getApplicationContext(), BackgroundService.class));
                    } catch (Exception e1) {
                        e1.printStackTrace();
                        startService(new Intent(getApplicationContext(), BackgroundService.class));
                    }*/
                    XmppConnection();
                }
            }
        } else {
            Toast.makeText(context, "There is no internet connection.", Toast.LENGTH_SHORT).show();
        }
    }

    private void getConversationfromApi(final boolean isProgressbarLoad) {
        try {
            JSONObject jsonObject = null;
            try {
                jsonObject = new JSONObject();
                jsonObject.put("fromJID", mJID + "");
                jsonObject.put("toJID", fJID + "");
            } catch (Exception e) {
                e.printStackTrace();
            }
            new VolleyApiRequest(NewChatActivity.this, isProgressbarLoad, VolleyApiRequest.REQUEST_BASEURL + "api/openfire/user_conversion",
                    new VolleyCallBack() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                String status = response.getString("status");
                                String message = response.getString("msg");

                                if (status.equals("200")) {
                                    ChatConcersationModel gson = new Gson().fromJson(String.valueOf(response), ChatConcersationModel.class);
                                    StoreDatailRealm(gson);
                                } else {
                                    Utilities.showToast(context, message);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onErrorResponse(VolleyError error) {
                            // VolleyApiRequest.showVolleyError(context, error);
                        }
                    }).enqueRequest(jsonObject, Request.Method.POST);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        context = this;
        Utilities.precenceAvailable(NewChatActivity.this);
    }

    public void settoOnline() {

    }

    public void settoOffline() {

    }

    private void StoreSingleMessage(final String message, final String mJID, final String fJID) {
        try {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    int newindex = index++;
                    ChatConversations chatConversations = new ChatConversations();
                    chatConversations.setMessageID(UniqueId + UserUniqueId + ConversationID + newindex);
                    chatConversations.setChetReference(UserUniqueId + "|" + UniqueId);
                    chatConversations.setMsgchtid(newindex + "");
                    chatConversations.setSentDate(System.currentTimeMillis() + "");
                    chatConversations.setBody(message + "");
                    chatConversations.setToJID(mJID);
                    chatConversations.setFromJID(fJID);
                    chatConversations.setConversationID(ConversationID);

                    RealmController.with(NewChatActivity.this).updateChatConversation(chatConversations);
                    refillAdapter();
                    ScrolltoLast();

                    if (context != null && context instanceof ChatConversation) {
                        MediaPlayer mp = MediaPlayer.create(context, R.raw.plucky);
                        mp.start();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onPause() {
        context = null;
        Utilities.precenceNotAvailable(NewChatActivity.this);
        super.onPause();
    }

    private void StoreDatailRealm(final ChatConcersationModel gson) {
        try {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (gson != null && gson.getData().size() > 0) {
                        RealmController.with(NewChatActivity.this).clearChatConversationbyid(UserUniqueId + "|" + UniqueId);
                        for (int i = 0; gson.getData().size() > i; i++) {
                            ChatConversationItems chatConversationItems = gson.getData().get(i);

                            ChatConversations chatConversations = new ChatConversations();
                            chatConversations.setMessageID(UniqueId + UserUniqueId + chatConversationItems.getConversationID() + i);
                            chatConversations.setChetReference(UserUniqueId + "|" + UniqueId);
                            //chatConversations.setMsgchtid(chatConversationItems.getMessageID());
                            chatConversations.setMsgchtid(i + "");
                            chatConversations.setSentDate(chatConversationItems.getSentDate());
                            chatConversations.setBody(chatConversationItems.getBody());
                            chatConversations.setToJID(chatConversationItems.getToJID());
                            chatConversations.setFromJID(chatConversationItems.getFromJID());
                            chatConversations.setConversationID(chatConversationItems.getConversationID());

                            RealmController.with(NewChatActivity.this).updateChatConversation(chatConversations);
                        }
                        getConversation(false, false);
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void presenceChanged(Presence var1) {
        try {
            if (var1 != null && var1.getTo().equalsIgnoreCase(mJID)) {
                        /*available,
                        unavailable,
                        subscribe,
                        subscribed,
                        unsubscribe,
                        unsubscribed,
                        error;*/
                if (var1.getType().toString().equalsIgnoreCase("available")) {
                    updateStatusOfUser("Online");
                } else if (var1.getType().toString().equalsIgnoreCase("unavailable")) {
                    updateStatusOfUser("Offline");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        setUnreadCount();
        setResult(RESULT_OK);
        NewChatActivity.this.finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        context = null;
        // xmppConnectionUtil.removePresenceListener();
    }

    private void ConnectXmpp() {
        try {
            //From notification
            xmppConnectionUtil.reconnectToXmpp(new ReconnectListener() {
                @Override
                public void onCalBack(int type) {
                    HandleCallBack(type);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void HandleCallBack(final int type) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();
                if (type == 1) {
                    //Success
                    initView();
                } else {
                    //Error
                    Toast.makeText(context, "Please try again", Toast.LENGTH_SHORT).show();
                    //ChatConversation.this.finish();
                }
            }
        });
    }

    private void HandleCallBackChat(final int type, final String msg) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();
                if (type == 1) {
                    chatMessageEditText.setText(null);
                    StoreSingleMessage(msg + "", fJID, mJID);
                    //Success
                    // sendMessage(msg + "");
                } else {
                    //Error
                    Toast.makeText(context, "Please try Again", Toast.LENGTH_SHORT).show();
                    //ChatConversation.this.finish();
                }
            }
        });
    }

    private void reConnectAndMessage(final String messag) {
        try {
            if (progressDialog != null && !progressDialog.isShowing()) {
                progressDialog.setMessage("Loading...");
                progressDialog.setCancelable(true);
                progressDialog.show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            xmppConnectionUtil.reconnectToXmppAndSendMessage(new ReconnectListener() {
                @Override
                public void onCalBack(int type) {
                    HandleCallBackChat(type, messag + "");
                }
            }, "", "", "");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void configure(ProviderManager pm) {
        // Add Ping-Pong Feature
        pm.addIQProvider("ping", "urn:xmpp:ping", new PingProvider());

        // Enable VCard feature
        pm.addIQProvider("vCard", "vcard-temp", new VCardProvider());

        // User Search
        pm.addIQProvider("query", "jabber:iq:search", new UserSearch.Provider());

        //  Private Data Storage
        pm.addIQProvider("query", "jabber:iq:private", new PrivateDataManager.PrivateDataIQProvider());

        //  Time
        try {
            pm.addIQProvider("query", "jabber:iq:time", Class.forName("org.jivesoftware.smackx.packet.Time"));
        } catch (ClassNotFoundException e) {
            Log.e(LOG_TAG, "Can't load class for org.jivesoftware.smackx.packet.Time");
        }

        //  XHTML
        pm.addExtensionProvider("html", "http://jabber.org/protocol/xhtml-im", new XHTMLExtensionProvider());

        //  Roster Exchange
        pm.addExtensionProvider("x", "jabber:x:roster", new RosterExchangeProvider());

        //  Message Events
        pm.addExtensionProvider("x", "jabber:x:event", new MessageEventProvider());

        //  Chat State
        pm.addExtensionProvider("active", "http://jabber.org/protocol/chatstates", new ChatStateExtension.Provider());
        pm.addExtensionProvider("composing", "http://jabber.org/protocol/chatstates", new ChatStateExtension.Provider());
        pm.addExtensionProvider("paused", "http://jabber.org/protocol/chatstates", new ChatStateExtension.Provider());
        pm.addExtensionProvider("inactive", "http://jabber.org/protocol/chatstates", new ChatStateExtension.Provider());
        pm.addExtensionProvider("gone", "http://jabber.org/protocol/chatstates", new ChatStateExtension.Provider());

        //  File Transfer - with ASMACK-ANDROID-7
//      pm.addIQProvider("si","http://jabber.org/protocol/si", new StreamInitiationProvider());
//		pm.addIQProvider("query","http://jabber.org/protocol/bytestreams", new BytestreamsProvider());

        //  File Transfer - WITH ASMACK-ANDDROID-7-BEEM
        pm.addIQProvider("si", "http://jabber.org/protocol/si", new StreamInitiationProvider());
        pm.addIQProvider("query", "http://jabber.org/protocol/bytestreams", new BytestreamsProvider());
        pm.addIQProvider("open", "http://jabber.org/protocol/ibb", new IBBProviders.Open());
        pm.addIQProvider("close", "http://jabber.org/protocol/ibb", new IBBProviders.Close());
        pm.addExtensionProvider("data", "http://jabber.org/protocol/ibb", new IBBProviders.Data());

        //  Group Chat Invitations
        pm.addExtensionProvider("x", "jabber:x:conference", new GroupChatInvitation.Provider());

        //  Service Discovery # Items
        pm.addIQProvider("query", "http://jabber.org/protocol/disco#items", new DiscoverItemsProvider());

        //  Service Discovery # Info
        pm.addIQProvider("query", "http://jabber.org/protocol/disco#info", new DiscoverInfoProvider());

        //  Data Forms
        pm.addExtensionProvider("x", "jabber:x:data", new DataFormProvider());

        //  MUC User
        pm.addExtensionProvider("x", "http://jabber.org/protocol/muc#user", new MUCUserProvider());

        //  MUC Admin
        pm.addIQProvider("query", "http://jabber.org/protocol/muc#admin", new MUCAdminProvider());

        //  MUC Owner
        pm.addIQProvider("query", "http://jabber.org/protocol/muc#owner", new MUCOwnerProvider());

        //  Delayed Delivery
        pm.addExtensionProvider("x", "jabber:x:delay", new DelayInformationProvider());

        //  Version
        try {
            pm.addIQProvider("query", "jabber:iq:version", Class.forName("org.jivesoftware.smackx.packet.Version"));
        } catch (ClassNotFoundException e) {
            Log.w(LOG_TAG, "Can't load class for org.jivesoftware.smackx.packet.Version");
        }

        //  Offline Message Requests
        pm.addIQProvider("offline", "http://jabber.org/protocol/offline", new OfflineMessageRequest.Provider());

        //  Offline Message Indicator
        pm.addExtensionProvider("offline", "http://jabber.org/protocol/offline", new OfflineMessageInfo.Provider());

        //  Last Activity
        pm.addIQProvider("query", "jabber:iq:last", new LastActivity.Provider());

        //  SharedGroupsInfo
        pm.addIQProvider("sharedgroup", "http://www.jivesoftware.org/protocol/sharedgroup", new SharedGroupsInfo.Provider());

        //  JEP-33: Extended Stanza Addressing
        pm.addExtensionProvider("addresses", "http://jabber.org/protocol/address", new MultipleAddressesProvider());
    }
}

/* private void sendMessage(String msg) {
        try {
            if (msg != null && !msg.equalsIgnoreCase("") && !msg.equalsIgnoreCase("null")) {
                try {
                    if (!isRestricted()) {
                        if (xmppConnectionUtil == null)
                            xmppConnection = XMPPLogic.getInstance().getConnection();
                        Roster roster = xmppConnection.getRoster();
                        // jid: String, user: String, groups: String[]
                        String Name = getIntent().getExtras().getString("fNickName") + "";
                        roster.createEntry(fJID, Name + "", null);
                        roster.setSubscriptionMode(Roster.SubscriptionMode.accept_all);
                        Presence subscribed = new Presence(Presence.Type.subscribed);
                        subscribed.setTo(fJID + "");
                        xmppConnection.sendPacket(subscribed);
                        is_RosterCreated = true;
                    }
                } catch (XMPPException e) {
                    e.printStackTrace();
                }
                if (chat == null) {
                    chatManager = xmppConnection.getChatManager();
                    chat = chatManager.createChat(fJID, new MessageListener() {
                        public void processMessage(Chat chat, final org.jivesoftware.smack.packet.Message chatMessage) {
                            if (chatMessage.getBody() != null) {
                                new Thread() {
                                    public void run() {
                                        Message msg = new Message();
                                        msg.what = 1;
                                        msg.obj = chatMessage;
                                    }
                                }.start();
                                Log.i(LOG_TAG, "Received message from: " + chatMessage.getFrom() + ": " + chatMessage.getBody());
                            }
                        }
                    });
                }
                chat.sendMessage(msg);
                chatMessageEditText.setText(null);
                StoreSingleMessage(msg + "", fJID, mJID);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
}*/




