package com.Chat;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.OpenFire.entry.ChatEntry;
import com.bumptech.glide.request.RequestOptions;
import com.github.curioustechizen.ago.RelativeTimeTextView;
import com.quickkonnect.R;
import com.bumptech.glide.Glide;
import com.realmtable.ChatConversations;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import io.realm.RealmResults;

class MessageChatAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    static final int RECIPIENT = 1;
    static final int SENDER = 0;
    private Context context;
    private String myJID;
    RealmResults<ChatConversations> conversationlist;

    MessageChatAdapter(Context context, RealmResults<ChatConversations> conversationlist, String myJID) {
        this.context = context;
        this.conversationlist = conversationlist;
        this.myJID = myJID;
    }

    @Override
    public int getItemViewType(int position) {
        if (conversationlist.get(position).getFromJID().equalsIgnoreCase(myJID)) {
            return SENDER;
        } else {
            return RECIPIENT;
        }
    }

    public void refillAdapter(RealmResults<ChatConversations> conversationlist) {
        this.conversationlist = conversationlist;
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        RecyclerView.ViewHolder viewHolder;
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());

        switch (viewType) {
            case SENDER:
                View viewSender = inflater.inflate(R.layout.layout_sender_message, viewGroup, false);
                viewHolder = new ViewHolderSender(viewSender);
                break;
            case RECIPIENT:
                View viewRecipient = inflater.inflate(R.layout.layout_recipient_message, viewGroup, false);
                viewHolder = new ViewHolderRecipient(viewRecipient);
                break;
            default:
                View viewSenderDefault = inflater.inflate(R.layout.layout_sender_message, viewGroup, false);
                viewHolder = new ViewHolderSender(viewSenderDefault);
                break;
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {

        switch (viewHolder.getItemViewType()) {
            case SENDER:
                ViewHolderSender viewHolderSender = (ViewHolderSender) viewHolder;
                configureSenderView(viewHolderSender, position);
                break;
            case RECIPIENT:
                ViewHolderRecipient viewHolderRecipient = (ViewHolderRecipient) viewHolder;
                configureRecipientView(viewHolderRecipient, position);
                break;
        }
    }

    private void configureSenderView(ViewHolderSender viewHolderSender, int position) {

        ChatConversations senderFireMessage = conversationlist.get(position);

        viewHolderSender.mSenderMessageTextView.setText(senderFireMessage.getBody());

        try {
            long datelong = Long.parseLong(senderFireMessage.getSentDate());
            String times = new SimpleDateFormat("MMM, dd hh:mm aa").format(new Date(datelong));
            String hours = new SimpleDateFormat("hh:mm aa").format(new Date(datelong));
            String date = new SimpleDateFormat("MMM, dd").format(new Date(datelong));
            String todaydate = new SimpleDateFormat("MMM, dd").format(Calendar.getInstance().getTime());
            if (times != null && date.equalsIgnoreCase(todaydate + "")) {
                viewHolderSender.time.setText(hours + "");
            } else {
                viewHolderSender.time.setText(times + "");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void configureRecipientView(ViewHolderRecipient viewHolderRecipient, int position) {

        ChatConversations recipientFireMessage = conversationlist.get(position);

        viewHolderRecipient.mRecipientMessageTextView.setText(recipientFireMessage.getBody());

        try {
            long datelong = Long.parseLong(recipientFireMessage.getSentDate());

            String times = new SimpleDateFormat("MMM, dd hh:mm aa").format(new Date(datelong));
            String hours = new SimpleDateFormat("hh:mm aa").format(new Date(datelong));
            String date = new SimpleDateFormat("MMM, dd").format(new Date(datelong));
            String todaydate = new SimpleDateFormat("MMM, dd").format(Calendar.getInstance().getTime());
            if (times != null && date.equalsIgnoreCase(todaydate + "")) {
                viewHolderRecipient.time.setText(hours + "");
            } else {
                viewHolderRecipient.time.setText(times + "");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return conversationlist.size();
    }


    /*==============ViewHolder===========*/

    /*ViewHolder for Sender*/
    private class ViewHolderSender extends RecyclerView.ViewHolder {

        private TextView mSenderMessageTextView, time;
        RelativeLayout root_sender;

        ViewHolderSender(View itemView) {
            super(itemView);
            mSenderMessageTextView = itemView.findViewById(R.id.text_view_sender_message);
            root_sender = itemView.findViewById(R.id.root_relativ_sender);
            time = itemView.findViewById(R.id.time);
        }
    }

    /*ViewHolder for Recipient*/
    private class ViewHolderRecipient extends RecyclerView.ViewHolder {

        private TextView mRecipientMessageTextView, time;
        private RelativeLayout root_recipient;

        ViewHolderRecipient(View itemView) {
            super(itemView);
            mRecipientMessageTextView = itemView.findViewById(R.id.text_view_recipient_message);
            root_recipient = itemView.findViewById(R.id.root_relativ_recipient);
            time = itemView.findViewById(R.id.time);
        }
    }

    private String getDate(long time) {
        Calendar cal = Calendar.getInstance();
        TimeZone tz = cal.getTimeZone();//get your local time zone.
        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a", Locale.getDefault());
        sdf.setTimeZone(tz);//set time zone.
        String localTime;
        if (String.valueOf(time).length() > 10) {
            localTime = sdf.format(new Date(time));
        } else {
            localTime = sdf.format(new Date(time * 1000));
        }
        return localTime;
    }
}