package com.utilities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;

import com.interfaces.CallBackImageDownload;
import com.quickkonnect.R;

import java.io.InputStream;

/**
 * Created by ZTLAB09 on 15-12-2017.
 */

public class DownloadImage extends AsyncTask<String, Void, Bitmap> {

    private ProgressDialog mDialog;
    Context context;
    CallBackImageDownload callBackImageDownload;

    public DownloadImage(Context context, CallBackImageDownload callBackImageDownload) {
        this.context = context;
        this.callBackImageDownload = callBackImageDownload;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        mDialog = new ProgressDialog(context, R.style.NewDialog);
        // pDialog.setMessage("Please wait...");
        mDialog.setCancelable(false);
        //pDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.parseColor("0f7e66")));
        mDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
        mDialog.show();
        Utilities.changeStatusbar((Activity) context, mDialog.getWindow());
    }

    @Override
    protected Bitmap doInBackground(String... URL) {

        String imageURL = URL[0];

        Bitmap bitmap = null;
        try {
            // Download Image from URL
            InputStream input = new java.net.URL(imageURL).openStream();
            // Decode Bitmap
            bitmap = BitmapFactory.decodeStream(input);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bitmap;
    }

    @Override
    protected void onPostExecute(Bitmap result) {
        // Set the bitmap into ImageView
        // Close progressdialog
        if (context != null && mDialog != null && mDialog.isShowing())
            mDialog.dismiss();
        if (result != null) {
            callBackImageDownload.getBase64(Utilities.encodeTobase64(result));
            callBackImageDownload.getBitmap(result);
        }

    }
}
