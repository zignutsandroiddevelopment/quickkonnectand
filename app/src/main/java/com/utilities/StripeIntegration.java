package com.utilities;

import android.content.Context;

import com.stripe.android.Stripe;
import com.stripe.android.TokenCallback;
import com.stripe.android.model.Card;

/*import com.stripe.android.Stripe;
import com.stripe.android.TokenCallback;
import com.stripe.android.model.Card;*/

/**
 * Created by ZTLAB-12 on 14-06-2018.
 */

public class StripeIntegration {
    private static String PUBLISH_KEY = "pk_live_qpaYldVHpbcn1OYmwcYyPPOn";
    //    private static String PUBLISH_KEY = "pk_test_g9AdFl2BPmF5jZZEN8th7wu9";
    public static StripeIntegration stripeIntegration;
    private Stripe stripe;
    private Context context;

    private StripeIntegration(Context context) {
        this.context = context;
    }

    public static StripeIntegration getInstance(Context context) {

        if (stripeIntegration == null) {
            stripeIntegration = new StripeIntegration(context);
        }
        return stripeIntegration;
    }

    public static String getPublishKey() {
        return PUBLISH_KEY;
    }

    public static void setPublishKey(String publishKey) {
        PUBLISH_KEY = publishKey;
    }

    public void initStripe() {
        stripe = new Stripe(context, PUBLISH_KEY);
    }

    public Stripe getStripe() {
        return stripe;
    }

    public void generateStripeToken(Card card, TokenCallback tokenCallback) {
        stripe.createToken(card, tokenCallback);

    }

}
