package com.utilities;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.models.logindata.ModelLoggedUser;

import java.lang.reflect.Type;
import java.util.HashSet;
import java.util.List;

/**
 * Created by ZTLAB09 on 04-12-2017.
 */

public class QKPreferences {

    private String loginUserId;
    private String loginUniqueCode;

    public String getLoginUserId() {
        return loginUserId;
    }

    public void setLoginUserId(String loginUserId) {
        this.loginUserId = loginUserId;
    }

    public String getLoginUniqueCode() {
        return loginUniqueCode;
    }

    public void setLoginUniqueCode(String loginUniqueCode) {
        this.loginUniqueCode = loginUniqueCode;
    }

    public static final String USER_ID = "user_id";
    public static final String USER_FULLNAME = "user_fname";
    public static final String USER_PROFILEIMG = "user_lname";
    public static final String USER_UNIQUECODE = "user_uniquecode";

    public static final String USER_CONT_ALL = "user_cont_all";
    public static final String USER_CONT_WEEK = "user_cont_week";
    public static final String USER_CONT_MONTH = "user_cont_month";

    private static final String QKPREF_NAME = "QuickConnectApp";
    private static final String PREFDATA_LOGGEDUSER = "loggeduserdata";
    //    private static final String PREFDATA_SCANCONTACTS = "scancontacts";
//    private static final String PREFDATA_USERPROFILE = "userprofile";
    public static final String QKTAGINFORMATION = "qk_tag_info";
    public static final String API_TOKEN = "API_TOKEN";
    public static final String NOTIFICATION_INFO = "notification_unread";
    public static final String FB_USERNAME = "name";
    //    public static final String LINKEDIN_UNAME = "firstName";
    public static final String TWITTER_UNAME = "tuname";
    public static final String INSTA_UNAME = "insta_uname";
    public static final String PROFILE_ID = "PROFILE_ID";
    public static final String START_TIME = "START_TIME";
    public static final String LOGIN_FROM = "LOGIN_FROM";
    public static final String IS_TRANSFER = "IS_TRANSFER";
    public static final String TRANSFER_STATUS = "TRANSFER_STATUS";
    public static final String STATUS = "STATUS";
    public static final String MY_PROFILE_UNIQUEIDS = "my_profiles_uniqueids";
    public static final String UPGRADE_PLAN = "UPGRADE_PLAN";
    public static final String UNREAD_COUNT = "UNREAD_COUNT";

    public static final String MAX_EMPLOYEE = "max_employee";
    public static final String MAX_NEW_EMPLOYEE = "max_new_employee";
    public static final String TOTAL_EMPLOYEE = "total_employee";
    public static final String USER_STRIPE_TOKEN = "USER_STRIPE_TOKEN";

    public static final String IS_XMPP_CONNECTED = "IS_XMPP_CONNECTED";
    public static final String IS_XMPP_LOGEDIN = "IS_XMPP_LOGEDIN";

    public static final String IS_SHOWBIRTHYEAR = "IS_SHOWBIRTHYEAR";

    private SharedPreferences sharedPreference;
    private static QKPreferences qkPreferences;
    private Context context;

    public QKPreferences(Context context) {
        this.context = context;
        sharedPreference = context.getSharedPreferences(QKPREF_NAME, 0);

        ModelLoggedUser modelLoggedUser = getLoggedUser();

        if (modelLoggedUser != null && modelLoggedUser.getData() != null) {

            String userID = String.valueOf(modelLoggedUser.getData().getId());
            setLoginUserId(userID);

            String userUniquecode = modelLoggedUser.getData().getUniqueCode();
            setLoginUniqueCode(userUniquecode);
        }
    }

    public static QKPreferences getInstance(Context context) {
        if (qkPreferences == null)
            qkPreferences = new QKPreferences(context);
        return qkPreferences;
    }

    public void storeLoggedUser(ModelLoggedUser modelLoggedUser) {

        SharedPreferences.Editor editor = sharedPreference.edit();

        editor.putInt(USER_ID, modelLoggedUser.getData().getId());
        editor.putString(USER_FULLNAME, modelLoggedUser.getData().getName());
        editor.putString(USER_PROFILEIMG, "" + modelLoggedUser.getData().getProfilePic());
        editor.putString(USER_UNIQUECODE, "" + modelLoggedUser.getData().getUniqueCode());

        try {

            int contactn_all = modelLoggedUser.getData().getContactCount().getContactAll();
            int contactn_week = modelLoggedUser.getData().getContactCount().getContactWeek();
            int contactn_month = modelLoggedUser.getData().getContactCount().getContactMonth();

            editor.putInt(USER_CONT_ALL, contactn_all);
            editor.putInt(USER_CONT_WEEK, contactn_week);
            editor.putInt(USER_CONT_MONTH, contactn_month);

        } catch (Exception e) {
            e.printStackTrace();
        }

        editor.putString(PREFDATA_LOGGEDUSER, new Gson().toJson(modelLoggedUser));
        editor.commit();
    }

    public void storeProfileID(String id) {
        SharedPreferences.Editor editor = sharedPreference.edit();
        editor.putString(PROFILE_ID, id);
        editor.commit();
    }

    public String getProfileID() {
        if (sharedPreference != null)
            return sharedPreference.getString(PROFILE_ID, "");
        else
            return "";
    }

    public void storeStartTime(String time) {
        SharedPreferences.Editor editor = sharedPreference.edit();
        editor.putString(START_TIME, time + "");
        editor.commit();
    }

    public String getStartTime() {
        if (sharedPreference != null)
            return sharedPreference.getString(START_TIME, "");
        else
            return "";
    }

    public void StoreUnreadCount(int time) {
        SharedPreferences.Editor editor = sharedPreference.edit();
        editor.putInt(UNREAD_COUNT, time);
        editor.commit();
    }

    public int getUnreadCount() {
        if (sharedPreference != null)
            return sharedPreference.getInt(UNREAD_COUNT, 0);
        else
            return 0;
    }

    public void IsXmppConnected(String type) {
        SharedPreferences.Editor editor = sharedPreference.edit();
        editor.putString(IS_XMPP_CONNECTED, type + "");
        editor.commit();
    }

    public String getIsXmppConnected() {
        if (sharedPreference != null)
            return sharedPreference.getString(IS_XMPP_CONNECTED, "");
        else
            return "";
    }

    public void IsXmppLogin(String type) {
        SharedPreferences.Editor editor = sharedPreference.edit();
        editor.putString(IS_XMPP_LOGEDIN, type + "");
        editor.commit();
    }

    public String getIsXmpplogin() {
        if (sharedPreference != null)
            return sharedPreference.getString(IS_XMPP_LOGEDIN, "");
        else
            return "";
    }

    public void storeIsTransfer(String id) {
        SharedPreferences.Editor editor = sharedPreference.edit();
        editor.putString(IS_TRANSFER, id);
        editor.commit();
    }

    public String getIsTransfer() {
        if (sharedPreference != null)
            return sharedPreference.getString(IS_TRANSFER, "");
        else
            return "";
    }

    public void storeTransfer_status(String id) {
        SharedPreferences.Editor editor = sharedPreference.edit();
        editor.putString(TRANSFER_STATUS, id);
        editor.commit();
    }

    public String getTransfer_status() {
        if (sharedPreference != null)
            return sharedPreference.getString(TRANSFER_STATUS, "");
        else
            return "";
    }

    public void storeStatus(String id) {
        SharedPreferences.Editor editor = sharedPreference.edit();
        editor.putString(STATUS, id);
        editor.commit();
    }

    public String getStatus() {
        if (sharedPreference != null)
            return sharedPreference.getString(STATUS, "");
        else
            return "";
    }

    public String getUpgradePlan() {
        if (sharedPreference != null)
            return sharedPreference.getString(UPGRADE_PLAN, "");
        else
            return "";
    }

    public void storeLoginFrom(String from) {
        SharedPreferences.Editor editor = sharedPreference.edit();
        editor.putString(LOGIN_FROM, from);
        editor.commit();
    }

    public String getLoginFrom() {
        if (sharedPreference != null)
            return sharedPreference.getString(LOGIN_FROM, "");
        else
            return "";
    }

    public ModelLoggedUser getLoggedUser() {
        String loggedUserData = sharedPreference.getString(PREFDATA_LOGGEDUSER, null);
        if (loggedUserData != null && loggedUserData.length() > 0) {
            ModelLoggedUser modelLoggedUser = new Gson().fromJson(loggedUserData, ModelLoggedUser.class);
            return modelLoggedUser;
        } else
            return null;
    }

    public String getLoggedUserid() {
        if (getLoggedUser() != null && getLoggedUser().getData() != null && getLoggedUser().getData().getId() != null)
            return "" + getLoggedUser().getData().getId();
        return null;
    }

    public void storeQKInfo(String qkinfo) {
        try {
            SharedPreferences.Editor editor = sharedPreference.edit();
            editor.putString(QKTAGINFORMATION, "" + qkinfo);
            editor.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getQKInfo() {
        return sharedPreference.getString(QKTAGINFORMATION, null);
    }

    public void storeApiToken(String token) {
        try {
            SharedPreferences.Editor editor = sharedPreference.edit();
            editor.putString(API_TOKEN, "" + token);
            editor.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getApiToken() {
        return sharedPreference.getString(API_TOKEN, "");
    }

    public void clearQKPreference() {
        SharedPreferences.Editor editor = sharedPreference.edit();
        editor.clear();
        editor.commit();
        editor.apply();
    }


    public void storeUnreadNotification(String notiinfo) {
        try {
            SharedPreferences.Editor editor = sharedPreference.edit();
            editor.putString(NOTIFICATION_INFO, notiinfo);
            editor.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getNotificationInfo() {
        return sharedPreference.getString(NOTIFICATION_INFO, null);
    }

    public void storeFbUname(String fbuname) {
        try {
            SharedPreferences.Editor editor = sharedPreference.edit();
            editor.putString(FB_USERNAME, fbuname);
            editor.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void storeUpgradePlan(String jsonString) {
        try {
            SharedPreferences.Editor editor = sharedPreference.edit();
            editor.putString(UPGRADE_PLAN, jsonString);
            editor.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void storeTwitter_Uname(String twitter_uname) {
        try {
            SharedPreferences.Editor editor = sharedPreference.edit();
            editor.putString(TWITTER_UNAME, twitter_uname);
            editor.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void storeInsta_Uname(String insta_uname) {
        try {
            SharedPreferences.Editor editor = sharedPreference.edit();
            editor.putString(INSTA_UNAME, insta_uname);
            editor.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getInstaUname() {
        return sharedPreference.getString(INSTA_UNAME, null);
    }

    public void storeValues(String key, String value) {
        SharedPreferences.Editor editor = sharedPreference.edit();
        editor.putString(key, value);
        editor.apply();
    }

    public String getValues(String key) {
        return sharedPreference.getString(key, null);
    }

    public boolean getValuesBoolean(String key) {
        return sharedPreference.getBoolean(key, false);
    }

    public int getValuesInt(String key) {
        return sharedPreference.getInt(key, -1);
    }

    public int getValuesIntDefault0(String key) {
        return sharedPreference.getInt(key, 0);
    }

    public void storeLatLong(String lati, String longi) {
        try {
            SharedPreferences.Editor editor = sharedPreference.edit();
            editor.putString("lati", lati);
            editor.putString("longi", longi);
            editor.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void storeUserStripeToken(String token) {
        try {
            SharedPreferences.Editor editor = sharedPreference.edit();
            editor.putString(USER_STRIPE_TOKEN, token);
            editor.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getUserStripeToken() {
        return sharedPreference.getString(USER_STRIPE_TOKEN, "");
    }


    public String getLati() {
        return sharedPreference.getString("lati", null);
    }

    public String getLongi() {
        return sharedPreference.getString("longi", null);
    }

    public void storeMyProfiles(List<String> listMyProfiles) {
        try {
            SharedPreferences.Editor editor = sharedPreference.edit();
            editor.putString(MY_PROFILE_UNIQUEIDS, new Gson().toJson(listMyProfiles));
            editor.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<String> getMyProfilesIds() {
        Type type = new TypeToken<List<String>>() {
        }.getType();
        String listIds = sharedPreference.getString(MY_PROFILE_UNIQUEIDS, null);
        List<String> stringList = new Gson().fromJson(listIds, type);
        return stringList;
    }

    public void setValues(String key, String value) {
        SharedPreferences.Editor editor = sharedPreference.edit();
        editor.putString(key, value);
        editor.apply();
        editor.commit();
    }

    public void setValues(String key, boolean value) {
        SharedPreferences.Editor editor = sharedPreference.edit();
        editor.putBoolean(key, value);
        editor.apply();
        editor.commit();
    }

    public static String LIST_DOWNLOADREQUEST = "list_downloadrequest";

    public void saveDownloadRequest(HashSet<String> listDownloads) {
        SharedPreferences.Editor editor = sharedPreference.edit();
        editor.putString(LIST_DOWNLOADREQUEST, new Gson().toJson(listDownloads));
        editor.apply();
        editor.commit();
    }

    public HashSet<String> getDownloadRequest() {
        HashSet<String> listDownloads = new HashSet<String>();
        String downloadlist = sharedPreference.getString(LIST_DOWNLOADREQUEST, "");
        if (downloadlist != null && !downloadlist.isEmpty()) {
            Type type = new TypeToken<HashSet<String>>() {
            }.getType();
            listDownloads = new Gson().fromJson(downloadlist, type);
        }
        return listDownloads;
    }
}
