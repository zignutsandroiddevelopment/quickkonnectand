package com.utilities;

/**
 * Created by ZTLAB09 on 04-12-2017.
 */

public class Constants {

    public static String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

    public static String CONTACTS_TYPE_FOLLOWERS = "follower";
    public static String CONTACTS_TYPE_FOLLOWING = "followings";

    public static final String SOCIAL_FACEBOOK_ID = "1";
    public static final String SOCIAL_LINKEDIN_ID = "2";
    public static final String SOCIAL_TWITTER_ID = "3";
    public static final String SOCIAL_INSTAGRAM_ID = "4";
    public static final String SOCIAL_GOOGLE_PLUS = "5";
    public static final String SOCIAL_SNAPCHAT = "6";

    public static final String QUICKKONNECT_DIRECTORY = "Quickkonnect/Images";

    public static final int RCODE_UPDATEEDUCATION = 122;
    public static final int RCODE_UPDATEQKTAG = 121;
    public static final int DELETE_EMPLOYEE_PROFILE = 122;

    public static final String URL_TERMSOFUSE = "https://quickkonnect.com/terms-of-use";
    public static final String URL_PRIVACYCONTENT = "https://quickkonnect.com/privacy-contract";
    public static final String URL_UG = "https://quickkonnect.com/copyright-quickkonnect";
    public static final String URL_PRESS = "https://quickkonnect.com/press";
    public static final String URL_HELP = "https://quickkonnect.com/#faq";

    public static final String CONTENT_ALER_PRIVACY = "<html>\n" +
            "<body>\n" +
            "By accepting and continuing, Quickkonnect assumes that you willingly agree to both the <a href=\"https://quickkonnect.com/terms-of-use\">Terms and Conditions</a> and <a href=\"https://quickkonnect.com/privacy-contract\"> Privacy Contract</a>. If this is not the case, you can delete your account and may\n" +
            "leave this application.\n" +
            "</body>\n" +
            "</html>";

    public static final String NO_INTERNET_CONNECTION = "No internet connection, please try after some time.";

    public static final String QKTAGURL = "url";
    public static final String QKMIDDLETAGURL = "middle_tag";
    public static final String QKTAGLOCALURL = "qktag_localurl";
    public static final String QKBORDERCOLOR = "border_color";
    public static final String QKBACKCOLOR = "background_color";
    public static final String QKPATTERNCOLOR = "pattern_color";

    public static final String NOTI_PENDING = "Pending";
    public static final String NOTI_APPRVOED = "Approve";
    public static final String NOTI_REJECTED = "Reject";

    public static final String NOTIFICATION_TYPE_SCAN = "1";
    public static final String NOTIFICATION_TYPE_CONTREQ = "2";
    public static final String NOTIFICATION_TYPE_APPROVED = "3";
    public static final String NOTIFICATION_TYPE_PROFAPPROVED = "5";
    public static final String NOTIFICATION_TYPE_PROFREJECT = "6";
    public static final String NOTIFICATION_TYPE_FOLLOW = "7";
    public static final String NOTIFICATION_TYPE_TRANSFER = "8";
    public static final String NOTIFICATION_TYPE_TRANSFER_APPROVE = "9";
    public static final String NOTIFICATION_TYPE_TRANSFER_REJECT = "10";
    public static final String NOTIFICATION_TYPE_EMPLOYY_REQUEST = "11";
    public static final String NOTIFICATION_TYPE_EMPLOYY_ACCEPT = "12";
    public static final String NOTIFICATION_TYPE_EMPLOYY_REJECT = "13";
    public static final String NOTIFICATION_TYPE_CHANGE_ROLE = "14";
    public static final String NOTIFICATION_TYPE_TRIAL_EXPIRATION = "15";
    public static final String NOTIFICATION_TYPE_CHANGE_PLAN_CANCEL = "16";
    public static final String NOTIFICATION_TYPE_CHANGE_PAYMENT_FAIL = "17";
    public static final String NOTIFICATION_TYPE_CHANGE_EMPLOYEE_DELETE = "18";
    public static final String NOTIFICATION_TYPE_PROFILE_DELETE = "19";
    public static final String NOTIFICATION_CHAT = "20";
    public static final String NOTIFICATION_SCAN_CONTREQ = "22";
    public static final String NOTIFICATION_SCAN_APRROVED = "23";
    public static final String NOTIFICATION_SCAN_REJECTED = "24";


   /* 15
    Trial Expiration Alert
     16
    Plan Cancel Notification
    17
    Payment Failed
    18
    Employee Delete Admin Notify*/

    public static final String KEY_SCANQK = "key_scanqk";
    public static final String KEY_CONTACT_EMPLOYEER = "key_contact_employeer";
    public static final String KEY_SCANQK_CONTACT = "key_scanqk_contact";
    public static final String KEY_SCANQK_GLOBAL_SEARCH = "KEY_SCANQK_GLOBAL_SEARCH";
    public static final String KEY_GLOBAL_SEARCH = "KEY_GLOBAL_SEARCH";
    public static final String KEY_OTHER = "key_other";
    public static final String KEY_OTHER_WITH_EMPLOYEE = "key_other_with_employee";
    public static final String KEY_FOLLOWER = "key_follower";
    public static final String KEY_FOLLOWER_UNBlock = "key_follower_unblock";
    public static final String KEY_BLOCKED = "key_blocked";
    public static final String KEY_SCANREQUEST = "key_scanrequest";
    public static final String KEY = "key";
    public static final String KEY_UPDATE = "key_update";
    public static final String ADD_DATA = "add";
    public static final String EDIT_DATA = "edit";
    public static final String KEY_QKTAGINFO = "qktag_info";
    public static final String KEY_CREATEPROFILEID = "cretae_profile_id";
    public static final String KEY_PROFILE_TYPE = "cretae_profile_type";
    public static final String KEY_CREATE_COMPONY_PROFILE = "KEY_CREATE_COMPONY_PROFILE";
    public static final String KEY_CREATE_COMPONY_PROFILE_IMAGE = "KEY_CREATE_COMPONY_PROFILE_IMAGE";
    public static final String KEY_CREATE_COMPONY_PROFILE_NAME = "KEY_CREATE_COMPONY_PROFILE_NAME";
    public static final String KEY_CREATE_COMPONY_PROFILE_ID = "KEY_CREATE_COMPONY_PROFILE_ID";
    public static final String KEY_CREATE_COMPONY_PROFILE_ABOUT = "KEY_CREATE_COMPONY_PROFILE_ABOUT";


    public static final int TYPE_PROFILE_DEFAULT = 0;
    public static final int TYPE_PROFILE_COMPANY = 1;
    public static final int TYPE_PROFILE_ENTERPRISE = 2;
    public static final int TYPE_PROFILE_CELEBRITY = 3;
    public static final int TYPE_PROFILE_EMPLOYEE = 4;

    public static final int TYPE_CLICK_ON_PROFILE_PIC = 1;
    public static final int TYPE_CLICK_ON_OTHER = 2;
    public static final int TYPE_SEARCH_CONTACT = 3;
    public static final int TYPE_CHAT_DELETE = 4;

    public static final String KEY_FROM_DASHBOARD = "dashboard";
    public static final String KEY_FROM_CREATEPROFILE = "CreateProfile";
    public static final String KEY_FROM_UPDATEPROFILE = "UpdateProfile";
    public static final String KEY_FROM_MANAGEPROFILES = "ManageProfile";
    public static final String KEY_FROM_NOTIFICATIONS = "NOTIFICATIONS";
    public static final String KEY_FROM_BLOCK_PROFILE = "KEY_FROM_BLOCK_PROFILE";

    public static final int STATUS_1 = 1;
    public static final int STATUS_0 = 0;

    public static final String KEY_FROM = "from";
    public static final String KEY_PROFILEID = "profile_id";
    public static final String KEY_PROFILETYPE = "profile_type";
    public static final String KEY_UNQUECODE = "profile_uniquecode";
    public static final String KEY_EMPLOYEE_UNIQUE_CODE = "KEY_EMPLOYEE_UNIQUE_CODE";
    public static final String KEY_MIDDLETAG = "profile_middletag";
    public static final String KEY_BACKGROUND_GRADIENT = "background_gradient";
    public static final String KEY_PROFILELEUSERID = "profile_userid";

    public static final int RESULT_ON_UPDATEUSERPROFILE = 120;

    public static final String LOG_REQUESTDATA = "log_requestdata";
    public static final String LOG_RESPONSEDATA = "log_responsedata";
    public static final String KEY_TITLE = "KEY_TITLE";

    public static final String SHARE_USER_QRCODE = "share_user_qrcode";
    public static final String SHARE_USER_PUBLIC_URL = "share_user_public_url";

    /*
        Constants for Social Facebook
     */


    /*
        Constants for Social LinkedIn
     */


    /*
        Constants for Social Twitter
     */


    /*
        Constants for Social Instagram
     */
    public static final String INSTA_SELF_URL = "https://api.instagram.com/v1/users/self/?access_token=";


    public static final String TYPE_CONTACTS = "contacts";
    public static final String TYPE_FOLLOWERS = "followers";

    public static final String ACTION_REFRESH_USERDATA = "quickkonnect.refreshdata";


    public static final String EXTRA_CHAT_REF = "EXTRA_CHAT_REF";
    public static final String EXTRA_CURRENT_USER_ID = "EXTRA_CURRENT_USER_ID";
    public static final String EXTRA_RECIPIENT_ID = "EXTRA_RECIPIENT_ID";
    public static final String EXTRA_BLOCK_USER_ID = "EXTRA_BLOCK_USER_ID";
    public static final String EXTRA_CURRENT_USER_NAME = "EXTRA_CURRENT_USER_NAME";
    public static final String EXTRA_RECIPIENT_NAME = "EXTRA_RECIPIENT_NAME";
    public static final String EXTRA_RECIPIENT_IMAGE = "EXTRA_RECIPIENT_IMAGE";
    public static final String EXTRA_IS_FORWARD = "EXTRA_IS_FORWARD"; // 1 = yes , 0 = no
    public static final String EXTRA_FORWARD_MESSAGE = "EXTRA_FORWARD_MESSAGE";
    public static final String EXTRA_USER_ID = "EXTRA_USER_ID";
    public static final String EXTRA_SENDER_ID = "EXTRA_SENDER_ID";
    public static final String EXTRA_SCAN_USER_DEVICE_ID = "EXTRA_SCAN_USER_DEVICE_ID";

    public static final String EXTRA_FROM_NOTIFICATION = "EXTRA_FROM_NOTIFICATION";
    public static final String EXTRA_USERS = "EXTRA_USERS";


    public static final String FACEBOOK_URL = "https://www.facebook.com/";
    public static final String LINKEDIN_URL = "https://www.linkedin.com/";
    public static final String TWITTER_URL = "https://twitter.com/";
    public static final String INSTA_URL = "https://www.instagram.com/";
    public static final String GOOGLE_PLUS_URL = "https://plus.google.com/";

    public static final String PRIVACY_STATUS_PUBLIC = "PUB";
    public static final String PRIVACY_STATUS_PRIVATE = "PRV";

    public static final String CONTACT_APPROVED = "A";
    public static final String CONTACT_REJECTED = "R";
    public static final String CONTACT_PENDING = "P";

    public static String KEY_GENERAL_NOTIFICATION = "general_notification";
}
