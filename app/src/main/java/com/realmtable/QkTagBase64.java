package com.realmtable;

import io.realm.RealmObject;

/**
 * Created by ZTLAB-10 on 05-03-2018.
 */

public class QkTagBase64 extends RealmObject {

    private String profile_id;
    private String base64Data;
    private String middletag_url;

    public String getProfile_id() {
        return profile_id;
    }

    public void setProfile_id(String profile_id) {
        this.profile_id = profile_id;
    }

    public String getBase64Data() {
        return base64Data;
    }

    public void setBase64Data(String base64Data) {
        this.base64Data = base64Data;
    }

    public String getMiddletag_url() {
        return middletag_url;
    }

    public void setMiddletag_url(String middletag_url) {
        this.middletag_url = middletag_url;
    }
}
