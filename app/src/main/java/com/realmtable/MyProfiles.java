package com.realmtable;

import io.realm.RealmObject;

/**
 * Created by ZTLAB-10 on 23-02-2018.
 */

public class MyProfiles extends RealmObject {

    private String id;
    private String user_id;
    private String type;
    private String name;
    private String address;
    private String longitude;
    private String latitude;
    private String about;
    private String logo;
    private String tag;
    private String sub_tag;
    private String company_team_size;
    private String establish_date;
    private String unique_code;
    private String status;
    private String social_media;
    private String founders;
    private String phone;
    private String email;
    private String weblink;
    private String otheraddress;
    private String current_ceo;
    private String current_cfo;
    private String dob;
    private String transfer_status;
    private String is_transfer;
    private String role;
    private String isOwnProfile;
    private String employee_unique_code;
    private String profile_view_code;
    private String is_follower;

    //for type =4 employeedata
    private String employee_no;
    private String department;
    private String designation;
    private String valid_from;
    private String valid_to;
    private String fax;
    private String profile;

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getRole_name() {
        return role_name;
    }

    public void setRole_name(String role_name) {
        this.role_name = role_name;
    }

    private String role_name;

    private String background_gradient;

    public String getBackground_gradient() {
        return background_gradient;
    }

    public void setBackground_gradient(String background_gradient) {
        this.background_gradient = background_gradient;
    }

    public String getTransfer_status() {
        return transfer_status;
    }

    public void setTransfer_status(String transfer_status) {
        this.transfer_status = transfer_status;
    }

    public String getIs_transfer() {
        return is_transfer;
    }

    public void setIs_transfer(String is_transfer) {
        this.is_transfer = is_transfer;
    }

    public String getOtheraddress() {
        return otheraddress;
    }

    public void setOtheraddress(String otheraddress) {
        this.otheraddress = otheraddress;
    }

    private String qktaginfo;

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    private String reason;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLongitude() {
        return longitude;
    }

    public String getEmployee_unique_code() {
        return employee_unique_code;
    }

    public void setEmployee_unique_code(String employee_unique_code) {
        this.employee_unique_code = employee_unique_code;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getSub_tag() {
        return sub_tag;
    }

    public void setSub_tag(String sub_tag) {
        this.sub_tag = sub_tag;
    }

    public String getCompany_team_size() {
        return company_team_size;
    }

    public void setCompany_team_size(String company_team_size) {
        this.company_team_size = company_team_size;
    }

    public String getEstablish_date() {
        return establish_date;
    }

    public void setEstablish_date(String establish_date) {
        this.establish_date = establish_date;
    }

    public String getUnique_code() {
        return unique_code;
    }

    public void setUnique_code(String unique_code) {
        this.unique_code = unique_code;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSocial_media() {
        return social_media;
    }

    public void setSocial_media(String social_media) {
        this.social_media = social_media;
    }

    public String getFounders() {
        return founders;
    }

    public void setFounders(String founders) {
        this.founders = founders;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getWeblink() {
        return weblink;
    }

    public void setWeblink(String weblink) {
        this.weblink = weblink;
    }

    public String getQktaginfo() {
        return qktaginfo;
    }

    public void setQktaginfo(String qktaginfo) {
        this.qktaginfo = qktaginfo;
    }

    public String getCurrent_ceo() {
        return current_ceo;
    }

    public void setCurrent_ceo(String current_ceo) {
        this.current_ceo = current_ceo;
    }

    public String getCurrent_cfo() {
        return current_cfo;
    }

    public void setCurrent_cfo(String current_cfo) {
        this.current_cfo = current_cfo;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getIsOwnProfile() {
        return isOwnProfile;
    }

    public void setIsOwnProfile(String isOwnProfile) {
        this.isOwnProfile = isOwnProfile;
    }

    public String getEmployee_no() {
        return employee_no;
    }

    public void setEmployee_no(String employee_no) {
        this.employee_no = employee_no;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getValid_from() {
        return valid_from;
    }

    public void setValid_from(String valid_from) {
        this.valid_from = valid_from;
    }

    public String getValid_to() {
        return valid_to;
    }

    public void setValid_to(String valid_to) {
        this.valid_to = valid_to;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }

    public String getProfile_view_code() {
        return profile_view_code;
    }

    public void setProfile_view_code(String profile_view_code) {
        this.profile_view_code = profile_view_code;
    }

    public String getIs_follower() {
        return is_follower;
    }

    public void setIs_follower(String is_follower) {
        this.is_follower = is_follower;
    }
}
