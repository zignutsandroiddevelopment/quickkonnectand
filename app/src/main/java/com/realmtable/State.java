package com.realmtable;

import io.realm.RealmObject;

/**
 * Created by ZTLAB03 on 02-01-2018.
 */

public class State extends RealmObject {
     private int id;
     private String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
