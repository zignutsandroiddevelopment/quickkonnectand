package com.realmtable;

import io.realm.RealmObject;

/**
 * Created by ZTLAB03 on 30-03-2018.
 */

public class TransferUser extends RealmObject {

    private String id;
    private String name;
    private String email;
    private String profile_pic;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getProfile_pic() {
        return profile_pic;
    }

    public void setProfile_pic(String profile_pic) {
        this.profile_pic = profile_pic;
    }
}
