package com.realmtable;

import io.realm.RealmObject;

/**
 * Created by ZTLAB03 on 17-04-2018.
 */

public class Roles extends RealmObject {
    private String id;
    private String name;
    private String description;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
