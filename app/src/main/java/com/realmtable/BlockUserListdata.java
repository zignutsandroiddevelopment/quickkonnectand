package com.realmtable;

import io.realm.Realm;
import io.realm.RealmObject;

/**
 * Created by ZTLAB03 on 04-05-2018.
 */

public class BlockUserListdata extends RealmObject {

    private String user_id;
    private String name;
    private String unique_code;
    private String email;
    private String profile_pic;

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUnique_code() {
        return unique_code;
    }

    public void setUnique_code(String unique_code) {
        this.unique_code = unique_code;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getProfile_pic() {
        return profile_pic;
    }

    public void setProfile_pic(String profile_pic) {
        this.profile_pic = profile_pic;
    }
}
