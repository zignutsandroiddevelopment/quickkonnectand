package com.realmtable;

import io.realm.RealmObject;

/**
 * Created by ZTLAB-12 on 19-07-2018.
 */

public class ChatConversations extends RealmObject {

    private String ChetReference;
    private String messageID;
    private String conversationID;
    private String fromJID;
    private String toJID;
    private String sentDate;
    private String body;
    private String msgchtid;

    public String getChetReference() {
        return ChetReference;
    }

    public void setChetReference(String chetReference) {
        ChetReference = chetReference;
    }

    public String getMessageID() {
        return messageID;
    }

    public void setMessageID(String messageID) {
        this.messageID = messageID;
    }

    public String getConversationID() {
        return conversationID;
    }

    public void setConversationID(String conversationID) {
        this.conversationID = conversationID;
    }

    public String getFromJID() {
        return fromJID;
    }

    public void setFromJID(String fromJID) {
        this.fromJID = fromJID;
    }

    public String getToJID() {
        return toJID;
    }

    public void setToJID(String toJID) {
        this.toJID = toJID;
    }

    public String getSentDate() {
        return sentDate;
    }

    public void setSentDate(String sentDate) {
        this.sentDate = sentDate;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getMsgchtid() {
        return msgchtid;
    }

    public void setMsgchtid(String msgchtid) {
        this.msgchtid = msgchtid;
    }
}
