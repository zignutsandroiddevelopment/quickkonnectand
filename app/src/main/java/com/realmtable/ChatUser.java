package com.realmtable;

import io.realm.RealmObject;

/**
 * Created by ZTLAB-12 on 28-05-2018.
 */

public class ChatUser extends RealmObject {

    private String blockuserid;
    private String lastmessage;
    private String profile_image;
    private String time;
    private String users;
    private String unradCounter;
    private String receiverId;
    private String userUid;
    private String chatref;

    private String from_token;
    private String from_user_id;
    private String to_token;
    private String to_user_id;

    public String getBlockuserid() {
        return blockuserid;
    }

    public void setBlockuserid(String blockuserid) {
        this.blockuserid = blockuserid;
    }

    public String getLastmessage() {
        return lastmessage;
    }

    public void setLastmessage(String lastmessage) {
        this.lastmessage = lastmessage;
    }

    public String getProfile_image() {
        return profile_image;
    }

    public void setProfile_image(String profile_image) {
        this.profile_image = profile_image;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getUsers() {
        return users;
    }

    public void setUsers(String users) {
        this.users = users;
    }

    public String getUnradCounter() {
        return unradCounter;
    }

    public void setUnradCounter(String unradCounter) {
        this.unradCounter = unradCounter;
    }

    public String getReceiverId() {
        return receiverId;
    }

    public void setReceiverId(String receiverId) {
        this.receiverId = receiverId;
    }

    public String getUserUid() {
        return userUid;
    }

    public void setUserUid(String userUid) {
        this.userUid = userUid;
    }

    public String getChatref() {
        return chatref;
    }

    public void setChatref(String chatref) {
        this.chatref = chatref;
    }

    public String getFrom_token() {
        return from_token;
    }

    public void setFrom_token(String from_token) {
        this.from_token = from_token;
    }

    public String getFrom_user_id() {
        return from_user_id;
    }

    public void setFrom_user_id(String from_user_id) {
        this.from_user_id = from_user_id;
    }

    public String getTo_token() {
        return to_token;
    }

    public void setTo_token(String to_token) {
        this.to_token = to_token;
    }

    public String getTo_user_id() {
        return to_user_id;
    }

    public void setTo_user_id(String to_user_id) {
        this.to_user_id = to_user_id;
    }
}