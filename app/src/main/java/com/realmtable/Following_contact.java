package com.realmtable;

import io.realm.RealmObject;

/**
 * Created by ZTLAB03 on 02-05-2018.
 */

public class Following_contact extends RealmObject {

    private String id;
    private String name;
    private String status;
    private String reason;
    private String type;
    private String logo;
    private String role;
    private String unique_code;
    private String border_color;
    private String pattern_color;
    private String background_color;
    private String middle_tag;
    private String qr_code;
    private String background_gradient;
    private String followed_on;
    private String followed_at;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getUnique_code() {
        return unique_code;
    }

    public void setUnique_code(String unique_code) {
        this.unique_code = unique_code;
    }

    public String getBorder_color() {
        return border_color;
    }

    public void setBorder_color(String border_color) {
        this.border_color = border_color;
    }

    public String getPattern_color() {
        return pattern_color;
    }

    public void setPattern_color(String pattern_color) {
        this.pattern_color = pattern_color;
    }

    public String getBackground_color() {
        return background_color;
    }

    public void setBackground_color(String background_color) {
        this.background_color = background_color;
    }

    public String getMiddle_tag() {
        return middle_tag;
    }

    public void setMiddle_tag(String middle_tag) {
        this.middle_tag = middle_tag;
    }

    public String getQr_code() {
        return qr_code;
    }

    public void setQr_code(String qr_code) {
        this.qr_code = qr_code;
    }

    public String getBackground_gradient() {
        return background_gradient;
    }

    public void setBackground_gradient(String background_gradient) {
        this.background_gradient = background_gradient;
    }

    public String getFollowed_on() {
        return followed_on;
    }

    public void setFollowed_on(String followed_on) {
        this.followed_on = followed_on;
    }

    public String getFollowed_at() {
        return followed_at;
    }

    public void setFollowed_at(String followed_at) {
        this.followed_at = followed_at;
    }
}
