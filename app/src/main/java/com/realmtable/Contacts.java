package com.realmtable;

import io.realm.RealmObject;

/**
 * Created by ZTLAB-10 on 12-02-2018.
 */

public class Contacts extends RealmObject {

    private String user_id;
    private String username;
    private String email;
    private String firstname;
    private String lastname;
    private String profile_pic;
    private String birthdate;
    private String hometown;
    private String country_id;
    private String country;
    private String city_id;
    private String city;
    private String state_id;
    private String state;
    private String qk_story;
    private String unique_code;
    private String scan_date;
    private String profile_id;
    private String profile_name;
    private String contact_type;
    private String scan_user_device_id;
    private String ishide_year;

    private String contact_detail;
    private String education_detail;
    private String experience_detail;
    private String publication_detail;
    private String language_detail;
    private String connected_social_media;
    private String approved_contacts;
    private String phone;
    private String scanned_by;

    private String isBlocked;
    private String isMatched;
    private String is_personal;
    private String is_employer;

    private String isContacts;

    private String download_request_sent;

    public String getIsBlocked() {
        return isBlocked;
    }

    public void setIsBlocked(String isBlocked) {
        this.isBlocked = isBlocked;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getScan_date() {
        return scan_date;
    }

    public void setScan_date(String scan_date) {
        this.scan_date = scan_date;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getScanned_by() {
        return scanned_by;
    }

    public void setScanned_by(String scanned_by) {
        this.scanned_by = scanned_by;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getProfile_pic() {
        return profile_pic;
    }

    public void setProfile_pic(String profile_pic) {
        this.profile_pic = profile_pic;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public String getHometown() {
        return hometown;
    }

    public void setHometown(String hometown) {
        this.hometown = hometown;
    }

    public String getCountry_id() {
        return country_id;
    }

    public void setCountry_id(String country_id) {
        this.country_id = country_id;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity_id() {
        return city_id;
    }

    public void setCity_id(String city_id) {
        this.city_id = city_id;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState_id() {
        return state_id;
    }

    public void setState_id(String state_id) {
        this.state_id = state_id;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getQk_story() {
        return qk_story;
    }

    public void setQk_story(String qk_story) {
        this.qk_story = qk_story;
    }

    public String getUnique_code() {
        return unique_code;
    }

    public void setUnique_code(String unique_code) {
        this.unique_code = unique_code;
    }

    public String getContact_detail() {
        return contact_detail;
    }

    public void setContact_detail(String contact_detail) {
        this.contact_detail = contact_detail;
    }

    public String getEducation_detail() {
        return education_detail;
    }

    public void setEducation_detail(String education_detail) {
        this.education_detail = education_detail;
    }

    public String getExperience_detail() {
        return experience_detail;
    }

    public void setExperience_detail(String experience_detail) {
        this.experience_detail = experience_detail;
    }

    public String getPublication_detail() {
        return publication_detail;
    }

    public void setPublication_detail(String publication_detail) {
        this.publication_detail = publication_detail;
    }

    public String getLanguage_detail() {
        return language_detail;
    }

    public void setLanguage_detail(String language_detail) {
        this.language_detail = language_detail;
    }

    public String getConnected_social_media() {
        return connected_social_media;
    }

    public void setConnected_social_media(String connected_social_media) {
        this.connected_social_media = connected_social_media;
    }

    public String getApproved_contacts() {
        return approved_contacts;
    }

    public void setApproved_contacts(String approved_contacts) {
        this.approved_contacts = approved_contacts;
    }

    public String getProfile_id() {
        return profile_id;
    }

    public void setProfile_id(String profile_id) {
        this.profile_id = profile_id;
    }

    public String getProfile_name() {
        return profile_name;
    }

    public void setProfile_name(String profile_name) {
        this.profile_name = profile_name;
    }

    public String getContact_type() {
        return contact_type;
    }

    public void setContact_type(String contact_type) {
        this.contact_type = contact_type;
    }

    public String getIsMatched() {
        return isMatched;
    }

    public void setIsMatched(String isMatched) {
        this.isMatched = isMatched;
    }

    public String getIsContacts() {
        return isContacts;
    }

    public void setIsContacts(String isContacts) {
        this.isContacts = isContacts;
    }

    public String getScan_user_device_id() {
        return scan_user_device_id;
    }

    public void setScan_user_device_id(String scan_user_device_id) {
        this.scan_user_device_id = scan_user_device_id;
    }

    public String getIs_personal() {
        return is_personal;
    }

    public void setIs_personal(String is_personal) {
        this.is_personal = is_personal;
    }

    public String getIs_employer() {
        return is_employer;
    }

    public void setIs_employer(String is_employer) {
        this.is_employer = is_employer;
    }

    public String getIshide_year() {
        return ishide_year;
    }

    public void setIshide_year(String ishide_year) {
        this.ishide_year = ishide_year;
    }

    public String getDownload_request_sent() {
        return download_request_sent;
    }

    public void setDownload_request_sent(String download_request_sent) {
        this.download_request_sent = download_request_sent;
    }
}
