package com.realmtable;

import io.realm.RealmObject;

/**
 * Created by ZTLAB-12 on 14-05-2018.
 */

public class SchoolListTbl extends RealmObject {
    private String id;
    private String name;
    private String logo;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }
}