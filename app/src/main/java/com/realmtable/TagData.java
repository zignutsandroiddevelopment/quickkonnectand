package com.realmtable;

import io.realm.RealmObject;

/**
 * Created by ZTLAB-10 on 21-02-2018.
 */

public class TagData extends RealmObject {

    private int id;
    private String name;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    private String type;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
