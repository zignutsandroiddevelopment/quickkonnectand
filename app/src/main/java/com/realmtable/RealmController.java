package com.realmtable;

import android.app.Activity;
import android.app.Application;
import android.app.Fragment;
import android.util.Log;

import com.google.gson.Gson;
import com.models.ModelNotifications;
import com.models.profiles.ProfilesList;
import com.models.profiles.QkTagInfo;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;

/**
 * Created by ZTLAB03 on 01-01-2018.
 */

public class RealmController {

    private static RealmController instance;
    private final Realm realm;

    public RealmController(Application application) {
        realm = Realm.getDefaultInstance();
    }

    public static RealmController with(Fragment fragment) {

        if (instance == null) {
            instance = new RealmController(fragment.getActivity().getApplication());
        }
        return instance;
    }

    public static RealmController with(Activity activity) {

        if (instance == null) {
            instance = new RealmController(activity.getApplication());
        }
        return instance;
    }

    public static RealmController with(Application application) {

        if (instance == null) {
            instance = new RealmController(application);
        }
        return instance;
    }

    public static RealmController getInstance() {
        return instance;
    }

    public Realm getRealm() {
        return realm;
    }

    public void refresh() {
        realm.refresh();
    }

    public void clearAllTables() {
        realm.beginTransaction();
        realm.clear(Country.class);
        realm.clear(BlockList.class);
        realm.clear(Contacts.class);
        realm.clear(MyProfiles.class);
        realm.clear(MyProfielData.class);
        realm.clear(Notifications.class);
        realm.clear(QkTagBase64.class);
        realm.clear(State.class);
        realm.clear(TagData.class);
        realm.clear(Followers.class);
        realm.clear(TransferUser.class);
        realm.clear(Employees.class);
        realm.clear(BlockEmpList.class);
        realm.clear(Following_contact.class);
        realm.clear(Follower_contact.class);
        realm.clear(SchoolListTbl.class);
        realm.clear(ChatUser.class);
        realm.clear(ChatUserList.class);
        realm.clear(ChatConversations.class);
        realm.commitTransaction();
    }

    public void clearFollowers() {
        realm.beginTransaction();
        realm.clear(Follower_contact.class);
        realm.commitTransaction();
    }

    public void clearFollowing() {
        realm.beginTransaction();
        realm.clear(Following_contact.class);
        realm.commitTransaction();
    }

    public Contacts clearContacts() {
        realm.beginTransaction();
        realm.clear(Contacts.class);
        realm.commitTransaction();
        return null;
    }

    public ChatUser clearChatUser() {
        realm.beginTransaction();
        realm.clear(ChatUser.class);
        realm.commitTransaction();
        return null;
    }

    public RealmController clearNotifications() {
        realm.beginTransaction();
        realm.clear(Notifications.class);
        realm.commitTransaction();
        return null;
    }

    public RealmController clearSchoolList() {
        realm.beginTransaction();
        realm.clear(SchoolListTbl.class);
        realm.commitTransaction();
        return null;
    }

    public RealmController clearAllTags() {
        realm.beginTransaction();
        realm.clear(TagData.class);
        realm.commitTransaction();
        return null;
    }

    public RealmController clearEmployee() {
        realm.beginTransaction();
        realm.clear(Employees.class);
        realm.commitTransaction();
        return null;
    }

    public RealmController clearRoles() {
        realm.beginTransaction();
        realm.clear(Roles.class);
        realm.commitTransaction();
        return null;
    }

    public RealmController clearChatuserList() {
        realm.beginTransaction();
        realm.clear(ChatUserList.class);
        realm.commitTransaction();
        return null;
    }

    public RealmController deleteSingleConversation(final String JID) {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                RealmResults<ChatUserList> rows = realm.where(ChatUserList.class).equalTo("JID", JID + "").findAll();
                rows.clear();
            }
        });
        return null;
    }

    public RealmController clearChatConversation() {
        realm.beginTransaction();
        realm.clear(ChatConversations.class);
        realm.commitTransaction();
        return null;
    }

    public void clearChatConversationbyid(final String ChetReference) {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                RealmResults<ChatConversations> rows = realm.where(ChatConversations.class).equalTo("ChetReference", ChetReference).findAll();
                rows.clear();
            }
        });
    }

    public RealmController clearAllBlockedContacts() {
        realm.beginTransaction();
        realm.clear(BlockList.class);
        realm.commitTransaction();
        return null;
    }

    public RealmController clearAllBlockedEmpoyee() {
        realm.beginTransaction();
        realm.clear(BlockEmpList.class);
        realm.commitTransaction();
        return null;
    }

    public RealmController clearAllBlockedUserdata() {
        realm.beginTransaction();
        realm.clear(BlockUserListdata.class);
        realm.commitTransaction();
        return null;
    }

    public RealmController clearQKTags() {
        realm.beginTransaction();
        realm.clear(QkTagBase64.class);
        realm.commitTransaction();
        return null;
    }

    public RealmResults<Country> getCountry() {
        return realm.where(Country.class).findAll();
    }

    public RealmResults<SchoolListTbl> getSchoolList() {
        return realm.where(SchoolListTbl.class).findAll();
    }

    public RealmResults<Contacts> getContacts() {
        return realm.where(Contacts.class).findAll();
    }

    public RealmResults<ChatUser> getChatUsers() {
        return realm.where(ChatUser.class).findAll();
    }

    public RealmResults<ChatUserList> getChatUserlist() {
        return realm.where(ChatUserList.class).findAll();
    }

    public RealmResults<Follower_contact> getFollower_contact() {
        return realm.where(Follower_contact.class).findAll();
    }

    public RealmResults<Following_contact> getFollowing_contact() {
        return realm.where(Following_contact.class).findAll();
    }

    public RealmResults<Followers> getFollowers() {
        return realm.where(Followers.class).findAll();
    }

    public RealmResults<Followers> getFollowersbyProfileId(String profile_id) {
        return realm.where(Followers.class).equalTo("profile_id", profile_id + "").findAll();
    }

    public RealmResults<ChatConversations> getChatConversationbyId(String mJid) {
        return realm.where(ChatConversations.class).equalTo("ChetReference", mJid + "").findAllSorted("sentDate", true);
    }

    public RealmResults<ChatConversations> getChatConversation() {
        return realm.where(ChatConversations.class).findAll();
    }

    public RealmResults<Employees> getEmployeeByProfileId(String profile_id) {
        return realm.where(Employees.class).equalTo("profile_id", profile_id + "").findAll();
    }

    public RealmResults<Employees> getEmployee() {
        return realm.where(Employees.class).findAll();
    }

    public RealmResults<Roles> getRoles() {
        return realm.where(Roles.class).findAll();
    }

    public RealmResults<TransferUser> getTransferUser() {
        return realm.where(TransferUser.class).findAll();
    }

    public RealmResults<BlockList> getBlockList() {
        return realm.where(BlockList.class).findAll();
    }

    public RealmResults<BlockEmpList> getBlockEmp() {
        return realm.where(BlockEmpList.class).findAll();
    }

    public RealmResults<BlockUserListdata> getBlockUserData() {
        return realm.where(BlockUserListdata.class).findAll();
    }

    public RealmResults<Notifications> getNotifications() {
        return realm.where(Notifications.class).findAll();
    }

    public RealmResults<TagData> getAllTags() {
        return realm.where(TagData.class).findAll();
    }

    public RealmResults<MyProfiles> getMyProfiles() {
        return realm.where(MyProfiles.class).findAll();
    }

    public RealmResults<MyProfiles> getMyProfiles(String ProfileId) {
        return realm.where(MyProfiles.class).equalTo("id", ProfileId + "").findAll();
    }

    public MyProfiles getMyProfielDataByProfileId(String unique_code) {
        return realm.where(MyProfiles.class).equalTo("unique_code", unique_code).findFirst();
    }

    public MyProfiles getMyProfielDataByid(String profileid) {
        return realm.where(MyProfiles.class).equalTo("id", profileid).findFirst();
    }

    public Contacts getUniqueContacts(String uniquecode) {
        Contacts contacts = realm.where(Contacts.class)
                .equalTo("unique_code", uniquecode).findFirst();
        return contacts;
    }

    public Contacts getUserIDContacts(String user_id) {
        Contacts contacts = realm.where(Contacts.class)
                .equalTo("user_id", user_id).findFirst();
        return contacts;
    }

    public void deleteTag(final String user_id) {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                RealmResults<QkTagBase64> rows = realm.where(QkTagBase64.class).equalTo("profile_id", user_id).findAll();
                rows.clear();
            }
        });
    }


    // Single Object Delete From Contacts
    public void deleteContact(final String unique_code) {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.where(Contacts.class).equalTo("unique_code", unique_code).findFirst().removeFromRealm();
                Log.e("Deleted", "Deleted");
            }
        });
    }

    // Single Object Delete From Profiles
    public void deleteProfile(final String unique_code) {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.where(MyProfiles.class).equalTo("unique_code", unique_code).findFirst().removeFromRealm();
                Log.e("Deleted", "deleteProfile");
            }
        });
    }

    // Single Object Delete From Followers
    public void deleteFollower(final String unique_code) {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.where(Follower_contact.class).equalTo("unique_code", unique_code).findFirst().removeFromRealm();
                Log.e("Deleted", "deleteFollower");
            }
        });
    }

    public MyProfiles getProfileByID(String user_id) {
        MyProfiles myProfiles = realm.where(MyProfiles.class).equalTo("id", user_id).findFirst();
        return myProfiles;
    }

    public MyProfiles getProfileByUniqeCode(String uniqueCode) {
        MyProfiles myProfiles = realm.where(MyProfiles.class).equalTo("unique_code", uniqueCode).findFirst();
        return myProfiles;
    }

    public QkTagBase64 getQKTagsbyID(String profile_id) {
        QkTagBase64 qkTagBase64 = realm.where(QkTagBase64.class).equalTo("profile_id", profile_id).findFirst();
        return qkTagBase64;
    }

    public void updateContacts(Contacts card) {

        boolean isUpdate = true;

        Contacts toEdit = realm.where(Contacts.class).equalTo("user_id", card.getUser_id()).findFirst();

        if (toEdit == null) {
            isUpdate = false;
            toEdit = new Contacts();
        }

        realm.beginTransaction();

        toEdit.setUser_id(card.getUser_id());

        if (card.getUsername() != null)
            toEdit.setUsername(card.getUsername());

        if (card.getEmail() != null)
            toEdit.setEmail(card.getEmail());

        if (card.getFirstname() != null)
            toEdit.setFirstname(card.getFirstname());

        if (card.getLastname() != null)
            toEdit.setLastname(card.getLastname());

        if (card.getProfile_pic() != null)
            toEdit.setProfile_pic(card.getProfile_pic());

        if (card.getBirthdate() != null)
            toEdit.setBirthdate(card.getBirthdate());

        if (card.getHometown() != null)
            toEdit.setHometown(card.getHometown());

        if (card.getCountry() != null)
            toEdit.setCountry(card.getCountry());

        if (card.getState() != null)
            toEdit.setState(card.getState());

        if (card.getIshide_year() != null)
            toEdit.setIshide_year(card.getIshide_year());

        if (card.getCity() != null)
            toEdit.setCity(card.getCity());

        if (card.getQk_story() != null)
            toEdit.setQk_story(card.getQk_story());

        if (card.getUnique_code() != null)
            toEdit.setUnique_code(card.getUnique_code());

        if (card.getScan_date() != null)
            toEdit.setScan_date(card.getScan_date());

        if (card.getIsBlocked() != null)
            toEdit.setIsBlocked(card.getIsBlocked());

        if (card.getContact_detail() != null)
            toEdit.setContact_detail(card.getContact_detail());

        if (card.getEducation_detail() != null)
            toEdit.setEducation_detail(card.getEducation_detail());

        if (card.getExperience_detail() != null)
            toEdit.setExperience_detail(card.getExperience_detail());

        if (card.getPublication_detail() != null)
            toEdit.setPublication_detail(card.getPublication_detail());

        if (card.getLanguage_detail() != null)
            toEdit.setLanguage_detail(card.getLanguage_detail());

        if (card.getApproved_contacts() != null)
            toEdit.setApproved_contacts(card.getApproved_contacts());

        if (card.getConnected_social_media() != null)
            toEdit.setConnected_social_media(card.getConnected_social_media());

        if (card.getScanned_by() != null)
            toEdit.setScanned_by(card.getScanned_by());

        if (card.getPhone() != null)
            toEdit.setPhone(card.getPhone());

        if (card.getEmail() != null)
            toEdit.setEmail(card.getEmail());

        if (card.getProfile_id() != null)
            toEdit.setProfile_id(card.getProfile_id());

        if (card.getProfile_name() != null)
            toEdit.setProfile_name(card.getProfile_name());

        if (card.getContact_type() != null)
            toEdit.setContact_type(card.getContact_type());

        if (card.getIsContacts() != null) {
            toEdit.setIsContacts(card.getIsContacts());
        }
        if (card.getScan_user_device_id() != null) {
            toEdit.setScan_user_device_id(card.getScan_user_device_id());
        }
        if (card.getIs_employer() != null) {
            toEdit.setIs_employer(card.getIs_employer());
        }
        if (card.getIs_personal() != null) {
            toEdit.setIs_personal(card.getIs_personal());
        }
        if (card.getDownload_request_sent() != null) {
            toEdit.setDownload_request_sent(card.getDownload_request_sent());
        }

        if (!isUpdate)
            realm.copyToRealm(toEdit);

        realm.commitTransaction();
    }


    public void updateChatUser(ChatUser card) {

        boolean isUpdate = true;

        ChatUser toEdit = realm.where(ChatUser.class).equalTo("chatref", card.getChatref()).findFirst();

        if (toEdit == null) {
            isUpdate = false;
            toEdit = new ChatUser();
        }

        realm.beginTransaction();

        if (card.getChatref() != null)
            toEdit.setChatref(card.getChatref());

        if (card.getBlockuserid() != null)
            toEdit.setBlockuserid(card.getBlockuserid());

        if (card.getLastmessage() != null)
            toEdit.setLastmessage(card.getLastmessage());

        if (card.getProfile_image() != null)
            toEdit.setProfile_image(card.getProfile_image());

        if (card.getTime() != null)
            toEdit.setTime(card.getTime());

        if (card.getUsers() != null)
            toEdit.setUsers(card.getUsers());

        if (card.getUnradCounter() != null)
            toEdit.setUnradCounter(card.getUnradCounter());

        if (card.getReceiverId() != null)
            toEdit.setReceiverId(card.getReceiverId());

        if (card.getUserUid() != null)
            toEdit.setUserUid(card.getUserUid());

        if (card.getFrom_token() != null)
            toEdit.setFrom_token(card.getFrom_token());

        if (card.getTo_token() != null)
            toEdit.setTo_token(card.getTo_token());

        if (card.getTo_user_id() != null)
            toEdit.setTo_user_id(card.getTo_user_id());

        if (card.getFrom_user_id() != null)
            toEdit.setFrom_user_id(card.getFrom_user_id());

        if (!isUpdate)
            realm.copyToRealm(toEdit);

        realm.commitTransaction();
    }

    public void updateFollower_Contact(Follower_contact card) {

        boolean isUpdate = true;

        Follower_contact toEdit = realm.where(Follower_contact.class).equalTo("user_id", card.getUser_id()).findFirst();

//        if (toEdit == null) {
        isUpdate = false;
        toEdit = new Follower_contact();
//        }

        realm.beginTransaction();

        toEdit.setUser_id(card.getUser_id());

        if (card.getName() != null)
            toEdit.setName(card.getName());

        if (card.getUsername() != null)
            toEdit.setUsername(card.getUsername());

        if (card.getEmail() != null)
            toEdit.setEmail(card.getEmail());

        if (card.getUnique_code() != null)
            toEdit.setUnique_code(card.getUnique_code());

        if (card.getProfile_pic() != null)
            toEdit.setProfile_pic(card.getProfile_pic());

        if (card.getProfile_id() != null)
            toEdit.setProfile_id(card.getProfile_id());

        if (card.getProfile_name() != null)
            toEdit.setProfile_name(card.getProfile_name());

        if (card.getPhone() != null)
            toEdit.setPhone(card.getPhone());

        if (card.getUsername() != null)
            toEdit.setPhone(card.getUsername());

        if (card.getFirstname() != null)
            toEdit.setPhone(card.getFirstname());

        if (card.getLastname() != null)
            toEdit.setPhone(card.getLastname());

        if (card.getScan_date() != null)
            toEdit.setScan_date(card.getScan_date());

        if (!isUpdate)
            realm.copyToRealm(toEdit);

        realm.commitTransaction();
    }

    public void updateFollowing_Contact(Following_contact card) {

        boolean isUpdate = true;

        Following_contact toEdit = realm.where(Following_contact.class).equalTo("id", card.getId()).findFirst();

        if (toEdit == null) {
            isUpdate = false;
            toEdit = new Following_contact();
        }
        realm.beginTransaction();

        toEdit.setId(card.getId());

        if (card.getName() != null)
            toEdit.setName(card.getName());

        if (card.getStatus() != null)
            toEdit.setStatus(card.getStatus());

        if (card.getReason() != null)
            toEdit.setReason(card.getReason());

        if (card.getType() != null)
            toEdit.setType(card.getType());

        if (card.getUnique_code() != null)
            toEdit.setUnique_code(card.getUnique_code());

        if (card.getLogo() != null)
            toEdit.setLogo(card.getLogo());

        if (card.getRole() != null)
            toEdit.setRole(card.getRole());

        if (card.getBorder_color() != null)
            toEdit.setBorder_color(card.getBorder_color());

        if (card.getPattern_color() != null)
            toEdit.setPattern_color(card.getPattern_color());

        if (card.getBackground_color() != null)
            toEdit.setBackground_color(card.getBackground_color());

        if (card.getMiddle_tag() != null)
            toEdit.setMiddle_tag(card.getMiddle_tag());

        if (card.getQr_code() != null)
            toEdit.setQr_code(card.getQr_code());

        if (card.getBackground_gradient() != null)
            toEdit.setBackground_gradient(card.getBackground_gradient());

        if (card.getFollowed_on() != null)
            toEdit.setFollowed_on(card.getFollowed_on());

        if (card.getFollowed_at() != null)
            toEdit.setFollowed_at(card.getFollowed_at());

        if (!isUpdate)
            realm.copyToRealm(toEdit);

        realm.commitTransaction();
    }

    public void updateFollowers(Followers card) {

        boolean isUpdate = true;

        Followers toEdit = realm.where(Followers.class).equalTo("user_id", card.getUser_id()).findFirst();

        if (toEdit == null) {
            isUpdate = false;
            toEdit = new Followers();
        }
        realm.beginTransaction();

        toEdit.setUser_id(card.getUser_id());

        if (card.getName() != null)
            toEdit.setName(card.getName());

        if (card.getEmail() != null)
            toEdit.setEmail(card.getEmail());

        if (card.getProfile_pic() != null)
            toEdit.setProfile_pic(card.getProfile_pic());

        if (card.getUnique_code() != null)
            toEdit.setUnique_code(card.getUnique_code());

        if (card.getProfile_id() != null)
            toEdit.setProfile_id(card.getProfile_id());

        if (!isUpdate)
            realm.copyToRealm(toEdit);

        realm.commitTransaction();
    }

    // todo check it once
    public void DeleteEmployeeFromProfileId(final String profile_id) {
        try {
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    RealmResults<Employees> rows = realm.where(Employees.class).equalTo("profile_id", profile_id + "").findAll();
                    rows.clear();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateEmployee(Employees card) {

        boolean isUpdate = true;

        Employees toEdit = realm.where(Employees.class).equalTo("user_id", card.getUser_id()).findFirst();

        if (toEdit == null) {
            isUpdate = false;
            toEdit = new Employees();
        }
        realm.beginTransaction();

        if (card.getId() != null)
            toEdit.setId(card.getId());

        if (card.getUsername() != null)
            toEdit.setUsername(card.getUsername());

        if (card.getProfile_id() != null)
            toEdit.setProfile_id(card.getProfile_id());

        if (card.getUser_id() != null)
            toEdit.setUser_id(card.getUser_id());

        if (card.getEmployee_no() != null)
            toEdit.setEmployee_no(card.getEmployee_no());

        if (card.getValid_to() != null)
            toEdit.setValid_to(card.getValid_to());

        if (card.getValid_from() != null)
            toEdit.setValid_from(card.getValid_from());

        if (card.getDesignation() != null)
            toEdit.setDesignation(card.getDesignation());

        if (card.getDepartment() != null)
            toEdit.setDepartment(card.getDepartment());

        if (card.getStatus() != null)
            toEdit.setStatus(card.getStatus());

        if (card.getReason() != null)
            toEdit.setReason(card.getReason());

        if (card.getUser1() != null)
            toEdit.setUser1(card.getUser1());

        if (card.getRole() != null)
            toEdit.setRole(card.getRole());

        if (card.getRole_name() != null)
            toEdit.setRole_name(card.getRole_name());

        /*if (card.getU_id() != null)
            toEdit.setU_id(card.getU_id());

        if (card.getU_name() != null)
            toEdit.setU_name(card.getU_name());

        if (card.getU_email() != null)
            toEdit.setU_email(card.getU_email());

        if (card.getU_profile_pic() != null)
            toEdit.setU_profile_pic(card.getU_profile_pic());

        if (card.getU_unique_code() != null)
            toEdit.setU_unique_code(card.getU_unique_code());*/

        if (!isUpdate)
            realm.copyToRealm(toEdit);

        realm.commitTransaction();
    }

    public void updateRoles(Roles card) {

        boolean isUpdate = true;

        Roles toEdit = realm.where(Roles.class).equalTo("id", card.getId()).findFirst();

        if (toEdit == null) {
            isUpdate = false;
            toEdit = new Roles();
        }
        realm.beginTransaction();

        if (card.getId() != null)
            toEdit.setId(card.getId());

        if (card.getName() != null)
            toEdit.setName(card.getName());

        if (card.getDescription() != null)
            toEdit.setDescription(card.getDescription());

        if (!isUpdate)
            realm.copyToRealm(toEdit);

        realm.commitTransaction();
    }

    public void updateChatUser(ChatUserList chatUserList) {

        boolean isUpdate = true;

        ChatUserList toEdit = realm.where(ChatUserList.class).equalTo("JID", chatUserList.getJID()).findFirst();

        if (toEdit == null) {
            isUpdate = false;
            toEdit = new ChatUserList();
        }

        realm.beginTransaction();

        if (chatUserList.getJID() != null)
            toEdit.setJID(chatUserList.getJID());

        if (chatUserList.getUsername() != null)
            toEdit.setUsername(chatUserList.getUsername());

        if (chatUserList.getProfile_pic() != null)
            toEdit.setProfile_pic(chatUserList.getProfile_pic());

        if (chatUserList.getUnique_code() != null)
            toEdit.setUnique_code(chatUserList.getUnique_code());

        if (chatUserList.getIsOnline() != null)
            toEdit.setIsOnline(chatUserList.getIsOnline());

        if (chatUserList.getUnread_count() != null)
            toEdit.setUnread_count(chatUserList.getUnread_count());

        if (chatUserList.getUser_id() != null)
            toEdit.setUser_id(chatUserList.getUser_id());

        if (chatUserList.getLast_message() != null)
            toEdit.setLast_message(chatUserList.getLast_message());

        if (chatUserList.getConversationID() != null)
            toEdit.setConversationID(chatUserList.getConversationID());

        if (chatUserList.getDate() != null)
            toEdit.setDate(chatUserList.getDate());

        if (!isUpdate)
            realm.copyToRealm(toEdit);

        realm.commitTransaction();
    }

    public void updateChatConversation(ChatConversations chatConversations) {

        boolean isUpdate = true;

        ChatConversations toEdit = realm.where(ChatConversations.class).equalTo("messageID", chatConversations.getMessageID()).findFirst();

        if (toEdit == null) {
            isUpdate = false;
            toEdit = new ChatConversations();
        }
        realm.beginTransaction();

        if (chatConversations.getMessageID() != null)
            toEdit.setMessageID(chatConversations.getMessageID());

        if (chatConversations.getConversationID() != null)
            toEdit.setConversationID(chatConversations.getConversationID());

        if (chatConversations.getFromJID() != null)
            toEdit.setFromJID(chatConversations.getFromJID());

        if (chatConversations.getToJID() != null)
            toEdit.setToJID(chatConversations.getToJID());

        if (chatConversations.getSentDate() != null)
            toEdit.setSentDate(chatConversations.getSentDate());

        if (chatConversations.getBody() != null)
            toEdit.setBody(chatConversations.getBody());

        if (chatConversations.getChetReference() != null)
            toEdit.setChetReference(chatConversations.getChetReference());

        if (chatConversations.getMsgchtid() != null)
            toEdit.setMsgchtid(chatConversations.getMsgchtid());

        if (!isUpdate)
            realm.copyToRealm(toEdit);

        realm.commitTransaction();
    }

    public void updateTrsnsferUserList(TransferUser card) {

        boolean isUpdate = true;

        TransferUser toEdit = realm.where(TransferUser.class).equalTo("id", card.getId()).findFirst();

        if (toEdit == null) {
            isUpdate = false;
            toEdit = new TransferUser();
        }
        realm.beginTransaction();

        toEdit.setId(card.getId());

        if (card.getName() != null)
            toEdit.setName(card.getName());

        if (card.getEmail() != null)
            toEdit.setEmail(card.getEmail());

        if (card.getProfile_pic() != null)
            toEdit.setProfile_pic(card.getProfile_pic());

        if (!isUpdate)
            realm.copyToRealm(toEdit);

        realm.commitTransaction();
    }

    public void createUpdateBlockedContacts(BlockList blockList) {

        boolean isUpdate = true;

        BlockList toEdit = realm.where(BlockList.class).equalTo("user_id", blockList.getUser_id()).findFirst();

        if (toEdit == null) {
            isUpdate = false;
            toEdit = new BlockList();
        }

        realm.beginTransaction();

        if (blockList.getUser_id() != null)
            toEdit.setUser_id(blockList.getUser_id());

        if (blockList.getProfile_pic() != null)
            toEdit.setProfile_pic(blockList.getProfile_pic());

        if (blockList.getUsername() != null)
            toEdit.setUsername(blockList.getUsername());

        if (blockList.getFirstname() != null)
            toEdit.setFirstname(blockList.getFirstname());

        if (blockList.getLastname() != null)
            toEdit.setLastname(blockList.getLastname());

        if (blockList.getScan_date() != null)
            toEdit.setScan_date(blockList.getScan_date());

        if (blockList.getUnique_code() != null)
            toEdit.setUnique_code(blockList.getUnique_code());

        if (!isUpdate)
            realm.copyToRealm(toEdit);

        realm.commitTransaction();
    }

    public void createUpdateBlockedEmp(BlockEmpList blockList) {

        boolean isUpdate = true;

        BlockEmpList toEdit = realm.where(BlockEmpList.class).equalTo("id", blockList.getId()).findFirst();

        if (toEdit == null) {
            isUpdate = false;
            toEdit = new BlockEmpList();
        }

        realm.beginTransaction();

        if (blockList.getId() != null)
            toEdit.setId(blockList.getId());

        if (blockList.getProfile_id() != null)
            toEdit.setProfile_id(blockList.getProfile_id());

        if (blockList.getUser_id() != null)
            toEdit.setUser_id(blockList.getUser_id());

        if (blockList.getEmployee_no() != null)
            toEdit.setEmployee_no(blockList.getEmployee_no());

        if (blockList.getValid_to() != null)
            toEdit.setValid_to(blockList.getValid_to());

        if (blockList.getValid_from() != null)
            toEdit.setValid_from(blockList.getValid_from());

        if (blockList.getDesignation() != null)
            toEdit.setDesignation(blockList.getDesignation());

        if (blockList.getDepartment() != null)
            toEdit.setDepartment(blockList.getDepartment());

        if (blockList.getStatus() != null)
            toEdit.setStatus(blockList.getStatus());

        if (blockList.getReason() != null)
            toEdit.setReason(blockList.getReason());

        if (blockList.getUser1() != null)
            toEdit.setUser1(blockList.getUser1());

        if (blockList.getProfile1() != null)
            toEdit.setProfile1(blockList.getProfile1());

        if (!isUpdate)
            realm.copyToRealm(toEdit);

        realm.commitTransaction();
    }

    public void createUpdateBlockeduser(BlockUserListdata blockList) {

        boolean isUpdate = true;

        BlockUserListdata toEdit = realm.where(BlockUserListdata.class).equalTo("user_id", blockList.getUser_id()).findFirst();

        if (toEdit == null) {
            isUpdate = false;
            toEdit = new BlockUserListdata();
        }

        realm.beginTransaction();

        if (blockList.getUser_id() != null)
            toEdit.setUser_id(blockList.getUser_id());

        if (blockList.getName() != null)
            toEdit.setName(blockList.getName());

        if (blockList.getUnique_code() != null)
            toEdit.setUnique_code(blockList.getUnique_code());

        if (blockList.getEmail() != null)
            toEdit.setEmail(blockList.getEmail());

        if (blockList.getProfile_pic() != null)
            toEdit.setProfile_pic(blockList.getProfile_pic());

        if (!isUpdate)
            realm.copyToRealm(toEdit);

        realm.commitTransaction();
    }

    public RealmResults getFilturedTagList(String profiletype) {

        //boolean isUpdate = true;
        RealmQuery<TagData> realmQuery = realm.where(TagData.class).equalTo("type", profiletype + "");
        RealmResults<TagData> finalList = realmQuery.findAll();
        /*TagData toEdit = realm.where(TagData.class).equalTo("type", profiletype).findFirst();
        if (toEdit == null) {
            isUpdate = false;
            toEdit = new TagData();
        }

        realm.beginTransaction();

        if (taglist.getName() != null)
            toEdit.setName(taglist.getName());

        if (taglist.getType() != null)
            toEdit.setType(taglist.getType());

            toEdit.setId(taglist.getId());

        if (!isUpdate)
            realm.copyToRealm(toEdit);

        realm.commitTransaction();*/

        return finalList;
    }

    public void updateMyProfiles(MyProfiles myProfiles) {

        boolean isUpdate = true;

        MyProfiles toEdit = realm.where(MyProfiles.class).equalTo("unique_code", myProfiles.getUnique_code()).findFirst();

        if (toEdit == null) {
            isUpdate = false;
            toEdit = new MyProfiles();
        }

        realm.beginTransaction();

        toEdit.setId("" + myProfiles.getId());

        if (myProfiles.getUser_id() != null)
            toEdit.setUser_id("" + myProfiles.getUser_id());

        if (myProfiles.getType() != null)
            toEdit.setType("" + myProfiles.getType());

        if (myProfiles.getName() != null)
            toEdit.setName("" + myProfiles.getName());

        if (myProfiles.getAddress() != null)
            toEdit.setAddress("" + myProfiles.getAddress());

        if (myProfiles.getLatitude() != null)
            toEdit.setLatitude("" + myProfiles.getLatitude());

        if (myProfiles.getLongitude() != null)
            toEdit.setLongitude("" + myProfiles.getLongitude());

        if (myProfiles.getAbout() != null)
            toEdit.setAbout("" + myProfiles.getAbout());

        if (myProfiles.getLogo() != null)
            toEdit.setLogo("" + myProfiles.getLogo());

        if (myProfiles.getTag() != null)
            toEdit.setTag("" + myProfiles.getTag());

        if (myProfiles.getSub_tag() != null)
            toEdit.setSub_tag("" + myProfiles.getSub_tag());

        if (myProfiles.getCompany_team_size() != null)
            toEdit.setCompany_team_size("" + myProfiles.getCompany_team_size());

        if (myProfiles.getBackground_gradient() != null)
            toEdit.setBackground_gradient("" + myProfiles.getBackground_gradient());

        if (myProfiles.getEstablish_date() != null)
            toEdit.setEstablish_date("" + myProfiles.getEstablish_date());

        if (myProfiles.getUnique_code() != null)
            toEdit.setUnique_code("" + myProfiles.getUnique_code());

        if (myProfiles.getStatus() != null)
            toEdit.setStatus("" + myProfiles.getStatus());

        if (myProfiles.getSocial_media() != null)
            toEdit.setSocial_media("" + myProfiles.getSocial_media());

        if (myProfiles.getFounders() != null)
            toEdit.setFounders("" + myProfiles.getFounders());

        if (myProfiles.getEmail() != null)
            toEdit.setEmail("" + myProfiles.getEmail());

        if (myProfiles.getPhone() != null)
            toEdit.setPhone("" + myProfiles.getPhone());

        if (myProfiles.getWeblink() != null)
            toEdit.setWeblink("" + myProfiles.getWeblink());

        if (myProfiles.getQktaginfo() != null)
            toEdit.setQktaginfo("" + myProfiles.getQktaginfo());

        if (myProfiles.getReason() != null)
            toEdit.setReason("" + myProfiles.getReason());

        if (myProfiles.getOtheraddress() != null)
            toEdit.setOtheraddress("" + myProfiles.getOtheraddress());

        if (myProfiles.getCurrent_ceo() != null)
            toEdit.setCurrent_ceo("" + myProfiles.getCurrent_ceo());

        if (myProfiles.getCurrent_cfo() != null)
            toEdit.setCurrent_cfo("" + myProfiles.getCurrent_cfo());

        if (myProfiles.getIs_transfer() != null)
            toEdit.setIs_transfer("" + myProfiles.getIs_transfer());
        else
            toEdit.setIs_transfer("0");

        if (myProfiles.getTransfer_status() != null)
            toEdit.setTransfer_status("" + myProfiles.getTransfer_status());
        else
            toEdit.setTransfer_status("0");

        if (myProfiles.getDob() != null)
            toEdit.setDob("" + myProfiles.getDob());

        if (myProfiles.getRole() != null)
            toEdit.setRole("" + myProfiles.getRole());

        if (myProfiles.getRole_name() != null)
            toEdit.setRole_name("" + myProfiles.getRole_name());

        if (myProfiles.getIsOwnProfile() != null)
            toEdit.setIsOwnProfile(myProfiles.getIsOwnProfile());

        if (myProfiles.getEmployee_no() != null)
            toEdit.setEmployee_no(myProfiles.getEmployee_no());

        if (myProfiles.getDepartment() != null)
            toEdit.setDepartment(myProfiles.getDepartment());

        if (myProfiles.getDesignation() != null)
            toEdit.setDesignation(myProfiles.getDesignation());

        if (myProfiles.getValid_from() != null)
            toEdit.setValid_from(myProfiles.getValid_from());

        if (myProfiles.getValid_to() != null)
            toEdit.setValid_to(myProfiles.getValid_to());

        if (myProfiles.getFax() != null)
            toEdit.setFax(myProfiles.getFax());

        if (myProfiles.getProfile() != null)
            toEdit.setProfile(myProfiles.getProfile());

        if (myProfiles.getEmployee_unique_code() != null)
            toEdit.setEmployee_unique_code(myProfiles.getEmployee_unique_code());

        if (myProfiles.getProfile_view_code() != null)
            toEdit.setProfile_view_code(myProfiles.getProfile_view_code());

        if (myProfiles.getIs_follower() != null)
            toEdit.setIs_follower(myProfiles.getIs_follower());

        if (!isUpdate)
            realm.copyToRealm(toEdit);

        realm.commitTransaction();
    }


//    public void updateMyProfielData(MyProfielData myProfiles) {
//
//        boolean isUpdate = true;
//
//        // here id = profile Id
//
//        MyProfielData toEdit = realm.where(MyProfielData.class).equalTo("id", myProfiles.getId()).findFirst();
//
//        if (toEdit == null) {
//            isUpdate = false;
//            toEdit = new MyProfielData();
//        }
//
//        realm.beginTransaction();
//
//        toEdit.setId("" + myProfiles.getId());
//
//        if (myProfiles.getUser_id() != null)
//            toEdit.setUser_id("" + myProfiles.getUser_id());
//
//        if (myProfiles.getType() != null)
//            toEdit.setType("" + myProfiles.getType());
//
//        if (myProfiles.getName() != null)
//            toEdit.setName("" + myProfiles.getName());
//
//        if (myProfiles.getAddress() != null)
//            toEdit.setAddress("" + myProfiles.getAddress());
//
//        if (myProfiles.getLatitude() != null)
//            toEdit.setLatitude("" + myProfiles.getLatitude());
//
//        if (myProfiles.getLongitude() != null)
//            toEdit.setLongitude("" + myProfiles.getLongitude());
//
//        if (myProfiles.getAbout() != null)
//            toEdit.setAbout("" + myProfiles.getAbout());
//        else
//            toEdit.setAbout("");
//
//        if (myProfiles.getLogo() != null)
//            toEdit.setLogo("" + myProfiles.getLogo());
//
//        if (myProfiles.getTag() != null)
//            toEdit.setTag("" + myProfiles.getTag());
//
//        if (myProfiles.getSub_tag() != null)
//            toEdit.setSub_tag("" + myProfiles.getSub_tag());
//
//        if (myProfiles.getCompany_team_size() != null)
//            toEdit.setCompany_team_size("" + myProfiles.getCompany_team_size());
//        else
//            toEdit.setCompany_team_size("");
//
//        if (myProfiles.getBackground_gradient() != null)
//            toEdit.setBackground_gradient("" + myProfiles.getBackground_gradient());
//
//        if (myProfiles.getEstablish_date() != null)
//            toEdit.setEstablish_date("" + myProfiles.getEstablish_date());
//
//        if (myProfiles.getUnique_code() != null)
//            toEdit.setUnique_code("" + myProfiles.getUnique_code());
//
//        if (myProfiles.getStatus() != null)
//            toEdit.setStatus("" + myProfiles.getStatus());
//
//        if (myProfiles.getSocial_media() != null)
//            toEdit.setSocial_media("" + myProfiles.getSocial_media());
//
//        if (myProfiles.getFounders() != null)
//            toEdit.setFounders("" + myProfiles.getFounders());
//
//        if (myProfiles.getEmail() != null)
//            toEdit.setEmail("" + myProfiles.getEmail());
//
//        if (myProfiles.getPhone() != null)
//            toEdit.setPhone("" + myProfiles.getPhone());
//
//        if (myProfiles.getWeblink() != null)
//            toEdit.setWeblink("" + myProfiles.getWeblink());
//
//        if (myProfiles.getQktaginfo() != null)
//            toEdit.setQktaginfo("" + myProfiles.getQktaginfo());
//
//        if (myProfiles.getReason() != null)
//            toEdit.setReason("" + myProfiles.getReason());
//
//        if (myProfiles.getOtheraddress() != null)
//            toEdit.setOtheraddress("" + myProfiles.getOtheraddress());
//
//        if (myProfiles.getCurrent_ceo() != null)
//            toEdit.setCurrent_ceo("" + myProfiles.getCurrent_ceo());
//        else
//            toEdit.setCurrent_ceo("");
//
//
//        if (myProfiles.getCurrent_cfo() != null)
//            toEdit.setCurrent_cfo("" + myProfiles.getCurrent_cfo());
//        else
//            toEdit.setCurrent_cfo("");
//
//        if (myProfiles.getIs_transfer() != null)
//            toEdit.setIs_transfer("" + myProfiles.getIs_transfer());
//        else
//            toEdit.setIs_transfer("0");
//
//        if (myProfiles.getTransfer_status() != null)
//            toEdit.setTransfer_status("" + myProfiles.getTransfer_status());
//        else
//            toEdit.setTransfer_status("0");
//
//        if (myProfiles.getDob() != null)
//            toEdit.setDob("" + myProfiles.getDob());
//
//        if (myProfiles.getRole() != null)
//            toEdit.setRole("" + myProfiles.getRole());
//
//        if (myProfiles.getRole_name() != null)
//            toEdit.setRole_name("" + myProfiles.getRole_name());
//
//        if (!isUpdate)
//            realm.copyToRealm(toEdit);
//
//        realm.commitTransaction();
//    }

    public void updateQKTags(QkTagBase64 qkTagBase64) {

        boolean isUpdate = true;

        QkTagBase64 toEdit = realm.where(QkTagBase64.class).equalTo("profile_id", qkTagBase64.getProfile_id()).findFirst();

        if (toEdit == null) {
            isUpdate = false;
            toEdit = new QkTagBase64();
        }

        realm.beginTransaction();

        toEdit.setProfile_id("" + qkTagBase64.getProfile_id());

        if (qkTagBase64.getBase64Data() != null)
            toEdit.setBase64Data("" + qkTagBase64.getBase64Data());

        if (qkTagBase64.getMiddletag_url() != null)
            toEdit.setMiddletag_url("" + qkTagBase64.getMiddletag_url());


        if (!isUpdate)
            realm.copyToRealm(toEdit);

        realm.commitTransaction();
    }

    public void createUpdateNotifications(ModelNotifications notifications) {

        boolean isUpdate = true;

        Notifications toEdit = realm.where(Notifications.class)
                .equalTo("notification_id", "" + notifications.getNotificationId()).findFirst();

        if (toEdit == null) {
            isUpdate = false;
            toEdit = new Notifications();
        }

        realm.beginTransaction();

        toEdit.setNotification_id("" + notifications.getNotificationId());

        if (notifications.getNotification_type() != null)
            toEdit.setNotification_type(notifications.getNotification_type());

        if (notifications.getSenderId() != null)
            toEdit.setSender_id("" + notifications.getSenderId());

        if (notifications.getUsername() != null)
            toEdit.setUsername(notifications.getUsername());

        if (notifications.getUserEmail() != null)
            toEdit.setUser_email(notifications.getUserEmail());

        if (notifications.getProfile_pic() != null)
            toEdit.setProfile_pic(notifications.getProfile_pic());

        if (notifications.getProfile_id() != null)
            toEdit.setProfile_id(notifications.getProfile_id());

        if (notifications.getStatus() != null)
            toEdit.setStatus(notifications.getStatus());

        if (notifications.getIsRead() != null)
            toEdit.setIs_read("" + notifications.getIsRead());

        if (notifications.getCreatedTime() != null)
            toEdit.setCreated_time("" + notifications.getCreatedTime());

        if (notifications.getUniqueCode() != null)
            toEdit.setUnique_code("" + notifications.getUniqueCode());

        if (notifications.getMessage() != null)
            toEdit.setMessage("" + notifications.getMessage());

        if (notifications.getProfile_name() != null)
            toEdit.setProfile_name("" + notifications.getProfile_name());

        if (!isUpdate)
            realm.copyToRealm(toEdit);

        realm.commitTransaction();
    }

    public static ProfilesList getRealmtoPojo(MyProfiles createProfileData) {

        ProfilesList myProfiles = new ProfilesList();

        myProfiles.setId(Integer.parseInt(createProfileData.getId()));
        myProfiles.setResson(createProfileData.getReason());
        myProfiles.setName(createProfileData.getName());
        myProfiles.setStatus(createProfileData.getStatus());
        myProfiles.setType(Integer.parseInt(createProfileData.getType()));
        myProfiles.setLogo(createProfileData.getLogo());
        myProfiles.setUniqueCode(createProfileData.getUnique_code());
        QkTagInfo qkTagInfo = new Gson().fromJson(createProfileData.getQktaginfo(), QkTagInfo.class);
        myProfiles.setQkTagInfo(qkTagInfo);
        myProfiles.setIs_transfer(createProfileData.getIs_transfer());
        myProfiles.setTransfer_status(createProfileData.getTransfer_status());
        myProfiles.setRole(createProfileData.getRole());
        myProfiles.setRole_name(createProfileData.getRole_name());
        myProfiles.setUser_id(createProfileData.getUser_id());
        myProfiles.setCompany_profile(createProfileData.getProfile());

        return myProfiles;
    }

    public static Contacts getRealmtoRealmContacts(Contacts card) {

        Contacts toEdit = new Contacts();

        toEdit.setUser_id(card.getUser_id());

        if (card.getUsername() != null)
            toEdit.setUsername(card.getUsername());

        if (card.getEmail() != null)
            toEdit.setEmail(card.getEmail());

        if (card.getFirstname() != null)
            toEdit.setFirstname(card.getFirstname());

        if (card.getLastname() != null)
            toEdit.setLastname(card.getLastname());

        if (card.getProfile_pic() != null)
            toEdit.setProfile_pic(card.getProfile_pic());

        if (card.getBirthdate() != null)
            toEdit.setBirthdate(card.getBirthdate());

        if (card.getHometown() != null)
            toEdit.setHometown(card.getHometown());

        if (card.getCountry() != null)
            toEdit.setCountry(card.getCountry());

        if (card.getState() != null)
            toEdit.setState(card.getState());

        if (card.getCity() != null)
            toEdit.setCity(card.getCity());

        if (card.getQk_story() != null)
            toEdit.setQk_story(card.getQk_story());

        if (card.getUnique_code() != null)
            toEdit.setUnique_code(card.getUnique_code());

        if (card.getScan_date() != null)
            toEdit.setScan_date(card.getScan_date());

        if (card.getIsBlocked() != null)
            toEdit.setIsBlocked(card.getIsBlocked());

        if (card.getContact_detail() != null)
            toEdit.setContact_detail(card.getContact_detail());

        if (card.getEducation_detail() != null)
            toEdit.setEducation_detail(card.getEducation_detail());

        if (card.getExperience_detail() != null)
            toEdit.setExperience_detail(card.getExperience_detail());

        if (card.getPublication_detail() != null)
            toEdit.setPublication_detail(card.getPublication_detail());

        if (card.getLanguage_detail() != null)
            toEdit.setLanguage_detail(card.getLanguage_detail());

        if (card.getApproved_contacts() != null)
            toEdit.setApproved_contacts(card.getApproved_contacts());

        if (card.getConnected_social_media() != null)
            toEdit.setConnected_social_media(card.getConnected_social_media());

        if (card.getScanned_by() != null)
            toEdit.setScanned_by(card.getScanned_by());

        if (card.getPhone() != null)
            toEdit.setPhone(card.getPhone());

        if (card.getEmail() != null)
            toEdit.setEmail(card.getEmail());

        if (card.getProfile_id() != null)
            toEdit.setProfile_id(card.getProfile_id());

        if (card.getProfile_name() != null)
            toEdit.setProfile_name(card.getProfile_name());

        if (card.getContact_type() != null)
            toEdit.setContact_type(card.getContact_type());

        if (card.getIsContacts() != null) {
            toEdit.setIsContacts(card.getIsContacts());
        }

        return toEdit;
    }

    public static MyProfiles getRealmtoRealmMyProfiles(MyProfiles myProfiles) {

        boolean isUpdate = true;

        MyProfiles toEdit = new MyProfiles();

        toEdit.setId("" + myProfiles.getId());

        if (myProfiles.getUser_id() != null)
            toEdit.setUser_id("" + myProfiles.getUser_id());

        if (myProfiles.getType() != null)
            toEdit.setType("" + myProfiles.getType());

        if (myProfiles.getName() != null)
            toEdit.setName("" + myProfiles.getName());

        if (myProfiles.getAddress() != null)
            toEdit.setAddress("" + myProfiles.getAddress());

        if (myProfiles.getLatitude() != null)
            toEdit.setLatitude("" + myProfiles.getLatitude());

        if (myProfiles.getLongitude() != null)
            toEdit.setLongitude("" + myProfiles.getLongitude());

        if (myProfiles.getAbout() != null)
            toEdit.setAbout("" + myProfiles.getAbout());

        if (myProfiles.getLogo() != null)
            toEdit.setLogo("" + myProfiles.getLogo());

        if (myProfiles.getTag() != null)
            toEdit.setTag("" + myProfiles.getTag());

        if (myProfiles.getSub_tag() != null)
            toEdit.setSub_tag("" + myProfiles.getSub_tag());

        if (myProfiles.getCompany_team_size() != null)
            toEdit.setCompany_team_size("" + myProfiles.getCompany_team_size());

        if (myProfiles.getBackground_gradient() != null)
            toEdit.setBackground_gradient("" + myProfiles.getBackground_gradient());

        if (myProfiles.getEstablish_date() != null)
            toEdit.setEstablish_date("" + myProfiles.getEstablish_date());

        if (myProfiles.getUnique_code() != null)
            toEdit.setUnique_code("" + myProfiles.getUnique_code());

        if (myProfiles.getStatus() != null)
            toEdit.setStatus("" + myProfiles.getStatus());

        if (myProfiles.getSocial_media() != null)
            toEdit.setSocial_media("" + myProfiles.getSocial_media());

        if (myProfiles.getFounders() != null)
            toEdit.setFounders("" + myProfiles.getFounders());

        if (myProfiles.getEmail() != null)
            toEdit.setEmail("" + myProfiles.getEmail());

        if (myProfiles.getPhone() != null)
            toEdit.setPhone("" + myProfiles.getPhone());

        if (myProfiles.getWeblink() != null)
            toEdit.setWeblink("" + myProfiles.getWeblink());

        if (myProfiles.getQktaginfo() != null)
            toEdit.setQktaginfo("" + myProfiles.getQktaginfo());

        if (myProfiles.getReason() != null)
            toEdit.setReason("" + myProfiles.getReason());

        if (myProfiles.getOtheraddress() != null)
            toEdit.setOtheraddress("" + myProfiles.getOtheraddress());

        if (myProfiles.getCurrent_ceo() != null)
            toEdit.setCurrent_ceo("" + myProfiles.getCurrent_ceo());

        if (myProfiles.getCurrent_cfo() != null)
            toEdit.setCurrent_cfo("" + myProfiles.getCurrent_cfo());

        if (myProfiles.getIs_transfer() != null)
            toEdit.setIs_transfer("" + myProfiles.getIs_transfer());
        else
            toEdit.setIs_transfer("0");

        if (myProfiles.getTransfer_status() != null)
            toEdit.setTransfer_status("" + myProfiles.getTransfer_status());
        else
            toEdit.setTransfer_status("0");

        if (myProfiles.getDob() != null)
            toEdit.setDob("" + myProfiles.getDob());

        if (myProfiles.getRole() != null)
            toEdit.setRole("" + myProfiles.getRole());

        if (myProfiles.getRole_name() != null)
            toEdit.setRole_name("" + myProfiles.getRole_name());

        if (myProfiles.getIsOwnProfile() != null)
            toEdit.setIsOwnProfile(myProfiles.getIsOwnProfile());

        return toEdit;
    }

    public void UpdateTanentListTime(String id, String message) {

        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy hh:mm:ss");
        String formattedDate = df.format(c);
        boolean isUpdate = true;
        ChatlistModel toEdit = realm.where(ChatlistModel.class).equalTo("id", id).findFirst();
        if (toEdit == null) {
            isUpdate = false;
            toEdit = new ChatlistModel();
        }
        realm.beginTransaction();
        if (toEdit.getTime() != null)
            toEdit.setTime(formattedDate);
        if (message != null)
            toEdit.setLastMsg(message);

        toEdit.setChatinitiate(true);


        if (!isUpdate)
            realm.copyToRealm(toEdit);
        realm.commitTransaction();
    }

}