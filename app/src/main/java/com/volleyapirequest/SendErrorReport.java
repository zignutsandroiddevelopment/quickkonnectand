package com.volleyapirequest;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.quickkonnect.R;
import com.utilities.Utilities;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Navin Panchal on 22-12-2017
 */

public class SendErrorReport {

    private Context context;
    private boolean isShowProgress = false;
    private ProgressDialog mDialog;
    private String url = null;
    private VolleyCallBack volleyCallBack;

    public SendErrorReport(Context context, String url, VolleyCallBack volleyCallBack) {
        this.context = context;
        this.url = url;
        this.volleyCallBack = volleyCallBack;
    }

    public void enqueRequest(final JSONObject jsonObject, int method) {

        if (isShowProgress) {
            mDialog = new ProgressDialog(context, R.style.NewDialog);
            // pDialog.setMessage("Please wait...");
            mDialog.setCancelable(false);
            //pDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.parseColor("0f7e66")));
            mDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
            mDialog.show();
            Utilities.changeStatusbar((Activity) context, mDialog.getWindow());
        }
        JsonObjectRequest jsObjRequest = new JsonObjectRequest(method, url, jsonObject, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                // TODO Auto-generated method stub
                if (isShowProgress && mDialog != null && mDialog.isShowing())
                    mDialog.dismiss();
                try {
                    Log.e("Error Report", "" + response);
                    String status = response.optString("status");
                    if (status.equals("200")) {
                        JSONObject jsonObject1 = response.getJSONObject("data");
                        //Utilities.showToast(context, "Your Error id is : " + jsonObject1.optString("log_id"));
                        volleyCallBack.onResponse(response);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                // TODO Auto-generated method stub
                if (isShowProgress && mDialog != null && mDialog.isShowing())
                    mDialog.dismiss();
                volleyCallBack.onErrorResponse(error);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("QK_ACCESS_KEY", context.getResources().getString(R.string.QK_ACCESS_KEY));
                return params;
            }
        };
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(
                30000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Volley.newRequestQueue(context).add(jsObjRequest);
    }

    public static void showVolleyError(Context context, VolleyError error) {

        if (error instanceof TimeoutError) {
            Toast.makeText(context, "Time Out,Network is slow.", Toast.LENGTH_SHORT).show();
        } else if (error instanceof NoConnectionError) {
            Toast.makeText(context, "Please Check Your Internet Connection.", Toast.LENGTH_SHORT).show();
        } else if (error instanceof AuthFailureError) {
            Toast.makeText(context, "Authentication Error.", Toast.LENGTH_SHORT).show();
        } else if (error instanceof ServerError) {
            Toast.makeText(context, "" + error.getMessage(), Toast.LENGTH_SHORT).show();
        } else if (error instanceof NetworkError) {
            Toast.makeText(context, "Network Error.", Toast.LENGTH_SHORT).show();
        } else if (error instanceof ParseError) {
            Toast.makeText(context, "Parse Error.", Toast.LENGTH_SHORT).show();
        }
    }
}
