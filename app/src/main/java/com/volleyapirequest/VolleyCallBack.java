package com.volleyapirequest;

import com.android.volley.VolleyError;

import org.json.JSONObject;

/**
 * Created by ZTLAB-10 on 22-12-2017.
 */

public interface VolleyCallBack {

    void onResponse(JSONObject response);
    void onErrorResponse(VolleyError error);

}
