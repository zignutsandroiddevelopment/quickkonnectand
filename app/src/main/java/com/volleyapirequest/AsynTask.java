package com.volleyapirequest;

import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.interfaces.OnSuccessError;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by ZTLAB-10 on 12-03-2018.
 */

public class AsynTask {

    Context context;
    OnSuccessError backGetResult;

    public AsynTask(Context context, OnSuccessError backGetResult) {
        this.context = context;
        this.backGetResult = backGetResult;
    }

    public void fnRejectContact(String userid, String sender_id, final int notification_id) {

        try {
            JSONObject obj = new JSONObject();
            try {
                obj.put("user_id", userid);
                obj.put("sender_id", sender_id);
                obj.put("notification_id", notification_id);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.e("Object -> ", obj.toString());
            new VolleyApiRequest(context, true, VolleyApiRequest.REQUEST_BASEURL + "api/reject_contact",
                    new VolleyCallBack() {
                        @Override
                        public void onResponse(JSONObject response) {
                            backGetResult.onSuccess(response);
                        }

                        @Override
                        public void onErrorResponse(VolleyError error) {
                            backGetResult.onCancel(error.getMessage());
                        }
                    }).enqueRequest(obj, Request.Method.POST);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void functionGetContactList(String userid, String oppositeuserid) {
        try {
            new VolleyApiRequest(context, true, VolleyApiRequest.REQUEST_BASEURL + "api/user_contact_list/" + userid + "/" + oppositeuserid,
                    new VolleyCallBack() {
                        @Override
                        public void onResponse(JSONObject response) {
                            backGetResult.onSuccess(response);
                        }

                        @Override
                        public void onErrorResponse(VolleyError error) {
                            backGetResult.onCancel(error.getMessage());
                        }
                    }).enqueRequest(null, Request.Method.GET);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // Approve Contact
    public void fnApproveContact(JSONObject jsonObject) {
        try {
            Log.e("Object -> ", jsonObject.toString());
            new VolleyApiRequest(context, true, VolleyApiRequest.REQUEST_BASEURL + "api/user_approved_contact",
                    new VolleyCallBack() {
                        @Override
                        public void onResponse(JSONObject response) {
                            backGetResult.onSuccess(response);
                        }

                        @Override
                        public void onErrorResponse(VolleyError error) {
                            backGetResult.onCancel(error.getMessage());
                        }
                    }).enqueRequest(jsonObject, Request.Method.POST);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}