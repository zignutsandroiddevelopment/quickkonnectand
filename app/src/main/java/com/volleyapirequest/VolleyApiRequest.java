package com.volleyapirequest;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.quickkonnect.R;
import com.utilities.QKPreferences;
import com.utilities.Utilities;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class VolleyApiRequest {

    private Context context;
    private boolean isShowProgress = false;
    private ProgressDialog mDialog;
    private String url = null;
    private VolleyCallBack volleyCallBack;

    public static String REQUEST_BASEURL = "https://quickkonnect.com/app/";
//    public static String REQUEST_BASEURL = "http://35.165.10.205/quickkonnect/";

    //this is user dp in chat notifications
    public static String REQUEST_BASEURL_PROFILE_PIC = "https://quickkonnect-docs.s3.eu-central-1.amazonaws.com/";
    //public static String REQUEST_BASEURL_PROFILE_PIC = "https://quickkonnect-test-docs.s3.eu-central-1.amazonaws.com/";

    // test  "https://quickkonnect-test-docs.s3.eu-central-1.amazonaws.com/";
    // live  "https://quickkonnect-docs.s3.eu-central-1.amazonaws.com/";

    public static final String API_CHECK_EMAIL_EXIST = "api/check_email_exist";
    public static final String API_CREATE_USER = "api/create_user";
    public static final String API_CREATE_NEW_SUBCAT = "api/profile/add_sub_tag";
    // Manage Profiles
    public static final String API_CREATE_PROFILE = "api/profile/create";
    public static final String API_TAGLIST = "api/profile/tag_list";
    public static final String API_PROFILELIST = "api/profile/profile_list";

    QKPreferences qkPreferences;

    public VolleyApiRequest(Context context, boolean isShowProgress, String url, VolleyCallBack volleyCallBack) {
        this.context = context;
        this.isShowProgress = isShowProgress;
        this.url = url;
        this.volleyCallBack = volleyCallBack;
        qkPreferences = new QKPreferences(context);
    }

    public void enqueRequest(final JSONObject jsonObject, int method) {
        if (isShowProgress) {
            mDialog = new ProgressDialog(context, R.style.NewDialog);
            // pDialog.setMessage("Please wait...");
            mDialog.setCancelable(false);
            //pDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.parseColor("0f7e66")));
            mDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
            mDialog.show();
            Utilities.changeStatusbar((Activity) context, mDialog.getWindow());
        }
        JsonObjectRequest jsObjRequest = new JsonObjectRequest(method, url, jsonObject, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                // TODO Auto-generated method stub
                //mDialog.dismiss();
                if (isShowProgress && mDialog != null && mDialog.isShowing())
                    mDialog.dismiss();
                volleyCallBack.onResponse(response);
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(final VolleyError error) {
                // TODO Auto-generated method stub
                try {
                    JSONObject jsonObject1 = Utilities.getDeviceInfo(context);
                    JSONObject message = new JSONObject();
                    message.put("message", "" + error.getMessage());
                    message.put("url", url);
                    jsonObject1.put("message", message.toString());
                    new SendErrorReport(context, REQUEST_BASEURL + "api/create_log", new VolleyCallBack() {
                        @Override
                        public void onResponse(JSONObject response) {
                            if (isShowProgress && mDialog != null && mDialog.isShowing())
                                volleyCallBack.onErrorResponse(error);
                            mDialog.dismiss();
                        }

                        @Override
                        public void onErrorResponse(VolleyError error) {
                            volleyCallBack.onErrorResponse(error);
                        }
                    }).enqueRequest(jsonObject1, Request.Method.POST);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                String token = "";
                if (qkPreferences != null) {
                    token = qkPreferences.getApiToken() + "";
                }

                Map<String, String> params = new HashMap<>();
                params.put("Authorization", "Bearer " + token + "");
                params.put("Accept", "application/json");
                //params.put("QK_ACCESS_KEY", context.getResources().getString(R.string.QK_ACCESS_KEY));
                return params;
            }
        };
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(
                15000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Volley.newRequestQueue(context).add(jsObjRequest);
    }

    public static void showVolleyError(Context context, VolleyError error) {

        if (error instanceof TimeoutError) {
            Toast.makeText(context, "Time out,Network is slow.", Toast.LENGTH_SHORT).show();
        } else if (error instanceof NoConnectionError) {
            Toast.makeText(context, "Please check your internet connection.", Toast.LENGTH_SHORT).show();
        } else if (error instanceof AuthFailureError) {
            Toast.makeText(context, "Authentication error.", Toast.LENGTH_SHORT).show();
        } else if (error instanceof ServerError) {
            Toast.makeText(context, "" + error.getMessage(), Toast.LENGTH_SHORT).show();
        } else if (error instanceof NetworkError) {
            Toast.makeText(context, "Network error.", Toast.LENGTH_SHORT).show();
        } else if (error instanceof ParseError) {
            Toast.makeText(context, "Parse error.", Toast.LENGTH_SHORT).show();
        }
    }
}
