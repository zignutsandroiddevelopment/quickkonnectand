package com.retrofitservice;

import com.models.EmployeeView.EmployeeDetailView;
import com.models.InviteQKPojo;
import com.models.StausMessage;
import com.models.SynchContactsPojo;
import com.models.contacts.ContactsPojo;
import com.models.countries.CountryPojo;
import com.models.globalsearch.ModelSearhWebResult;
import com.models.login.LoginPojo;
import com.models.otherusersprofiles.OtherUserProfilesPojo;
import com.models.profiles.ModelProfileList;
import com.models.profilesemployee.ProfileEmpPojo;
import com.models.profilesfollower.ProfileFollowerPojo;
import com.models.schoollist.SchoolListPojo;

import org.json.JSONArray;

import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.HeaderMap;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.Url;

public interface SOService {

    @Headers("QK_ACCESS_KEY: " + ApiUtils.QK_ACCESS_KEY)
    @GET("api/get_countries")
    Call<CountryPojo> getCountries();

    @FormUrlEncoded
    @Headers("QK_ACCESS_KEY: " + ApiUtils.QK_ACCESS_KEY)
    @POST("api/login")
    Call<LoginPojo> getLogin(
            @Field("email") String email,
            @Field("password") String password,
            @Field("device_type") String device_type,
            @Field("device_id") String device_id);

    @Headers("QK_ACCESS_KEY: " + ApiUtils.QK_ACCESS_KEY)
    @GET("api/get_login_user/{user_id}")
    Call<LoginPojo> getLoginUsersData(@HeaderMap Map<String, String> headers, @Path("user_id") int user_id);

    @FormUrlEncoded
    @POST("api/profile/profile_list")
    Call<ModelProfileList> getProfileList(
            @HeaderMap Map<String, String> headers,
            @Field("user_id") int user_id);

    @FormUrlEncoded
    @POST("api/get_user_detail_by_unique_code")
    Call<OtherUserProfilesPojo> getOtherUserProfile(
            @HeaderMap Map<String, String> headers,
            @Field("unique_code") String unique_code,
            @Field("opposite_unique_code") String opposite_unique_code,
            @Field("timezone") String timezone);

    @GET("api/scan_user_list/{user_id}")
    Call<ContactsPojo> getContactsList(
            @HeaderMap Map<String, String> headers,
            @Path("user_id") String user_id,
            @Query("timezone") String timezone);

    @FormUrlEncoded
    @POST("api/profile/followers_list")
    Call<ContactsPojo> getFollowersList(
            @HeaderMap Map<String, String> headers,
            @Field("type") String type,
            @Field("user_id") int user_id,
            @Field("page") int page);

    @FormUrlEncoded
    @POST("api/profile/followers_list")
    Call<ProfileFollowerPojo> getCompanyFollowersList(
            @HeaderMap Map<String, String> headers,
            @Field("type") String type,
            @Field("profile_id") String user_id,
            @Field("user_id") int page);

    @FormUrlEncoded
    @POST("api/member/member_list")
    Call<ProfileEmpPojo> getCompanyEmployeeList(
            @HeaderMap Map<String, String> headers,
            @Field("profile_id") String profile_id,
            @Field("user_id") String user_id,
            @Field("page") int page);


    @POST("api/profile/education_profile_list")
    Call<SchoolListPojo> getSchoolList(@HeaderMap Map<String, String> headers);

    @FormUrlEncoded
    @POST("api/member/status")
    Call<StausMessage> AcceptEmployee(
            @HeaderMap Map<String, String> headers,
            @Field("status") String status,
            @Field("user_id") String user_id,
            @Field("profile_id") String profileId,
            @Field("phone") JSONArray phone,
            @Field("email") JSONArray email,
            @Field("weblink") JSONArray web,
            @Field("fax") JSONArray fax);

    @FormUrlEncoded
    @POST("api/profile/profile_view")
    Call<EmployeeDetailView> getEmployeeInfo(
            @HeaderMap Map<String, String> headers,
            @Field("profile_id") String profile_id,
            @Field("user_id") String user_id,
            @Field("type") String type);

    @FormUrlEncoded
    @POST("api/web/invite_employee")
    Call<InviteQKPojo> invite_employee(
            @HeaderMap Map<String, String> headers,
            @Field("email") String email,
            @Field("message") String message);

    @FormUrlEncoded
    @POST("api/user_contact_sync")
    Call<SynchContactsPojo> synchContact(@HeaderMap Map<String, String> headers,
                                         @Field("contact_id") String contact_id,
                                         @Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("api/notification_toggle")
    Call<ResponseBody> changNotificationStatus(@HeaderMap Map<String, String> headers,
                                               @Field("user_id") String user_id,
                                               @Field("notification_status") String notification_status);

    @FormUrlEncoded
    @POST("api/user_privacy_setting")
    Call<ResponseBody> updatePrivacyStatus(@HeaderMap Map<String, String> headers,
                                           @Field("user_id") String user_id,
                                           @Field("privacy") String privacy);

    @FormUrlEncoded
    @POST("api/global_search")
    Call<ModelSearhWebResult> searchGlobalData(@HeaderMap Map<String, String> headers,
                                               @Field("user_id") String user_id,
                                               @Field("search") String search);

    @FormUrlEncoded
    @POST("api/contact_request/send_request")
    Call<ResponseBody> sendContactRequest(@HeaderMap Map<String, String> headers,
                                          @Field("user_id") String user_id,
                                          @Field("opposite_user_id") String opposite_user_id);

    @FormUrlEncoded
    @POST("api/contact_request/reject_request")
    Call<ResponseBody> rejectScanRequest(@HeaderMap Map<String, String> headers,
                                         @Field("user_id") String user_id,
                                         @Field("opposite_user_id") int opposite_user_id,
                                         @Field("notification_id") int notification_id);

    @FormUrlEncoded
    @POST("api/contact_request/approved_request")
    Call<ResponseBody> approveScanRequest(@HeaderMap Map<String, String> headers,
                                          @Field("user_id") String user_id,
                                          @Field("opposite_user_id") int opposite_user_id,
                                          @Field("notification_id") int notification_id);

    @FormUrlEncoded
    @POST("api/scan_user_store")
    Call<ResponseBody> approveScanRequest(@HeaderMap Map<String, String> headers,
                                          @Field("user_id") String user_id,
                                          @Field("unique_code") String opposite_user_id,
                                          @Field("timezone") String timezone,
                                          @Field("latitude") String latitude,
                                          @Field("longitude") String longitude,
                                          @Field("device_type") String device_type,
                                          @Field("manual_scan") String manual_scan);

    @GET
    Call<ResponseBody> getEmailVerify(@Url String url);

    @GET
    Call<ResponseBody> getUserQRCode(@Url String url);

    @GET("api/user_contact_block_list/{user_id}")
    Call<ResponseBody> getBlockedContacts(@HeaderMap Map<String, String> headers,
                                          @Path("user_id") String user_id,
                                          @Query("timezone") String timezone);

    @FormUrlEncoded
    @POST("api/user_feedback")
    Call<ResponseBody> sendReport(@HeaderMap Map<String, String> headers,
                                  @Field("user_id") String user_id,
                                  @Field("msg") String msg,
                                  @Field("name") String name,
                                  @Field("email") String email,
                                  @Field("file") String file);

    @FormUrlEncoded
    @POST("api/profile/getAllfollowing")
    Call<ResponseBody> getAllFollowing(@HeaderMap Map<String, String> headers,
                                       @Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("api/user_publication_profile_add")
    Call<ResponseBody> addPublication(@HeaderMap Map<String, String> headers,
                                      @Field("user_id") String user_id,
                                      @Field("publication_title") String publication_title,
                                      @Field("publisher_name") String publisher_name,
                                      @Field("date") String date,
                                      @Field("url") String url);


    @FormUrlEncoded
    @POST("api/user_publication_profile_edit")
    Call<ResponseBody> editPublication(@HeaderMap Map<String, String> headers,
                                       @Field("publication_id") int publication_id,
                                       @Field("publication_title") String publication_title,
                                       @Field("publisher_name") String publisher_name,
                                       @Field("date") String date,
                                       @Field("url") String url);


    @GET("api/delete_publication_detail/{publication_id}")
    Call<ResponseBody> deletePublication(@HeaderMap Map<String, String> headers,
                                         @Path("publication_id") int publication_id);


    @FormUrlEncoded
    @POST("api/user_langauge_detail_add")
    Call<ResponseBody> addLanguage(@HeaderMap Map<String, String> headers,
                                   @Field("user_id") String user_id,
                                   @Field("langauge_name") String langauge_name,
                                   @Field("rating") String rating);

    @FormUrlEncoded
    @POST("api/user_langauge_detail_edit")
    Call<ResponseBody> editLanguage(@HeaderMap Map<String, String> headers,
                                    @Field("langauge_id") int langauge_id,
                                    @Field("langauge_name") String langauge_name,
                                    @Field("rating") String rating);

    @GET("api/delete_language_detail/{language_id}")
    Call<ResponseBody> deleteLanguage(@HeaderMap Map<String, String> headers,
                                      @Path("language_id") int language_id);
}